<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script src="resources/reporting/js/base64.js"></script>
<!-- <script src="resources/reporting/bootstrap.min.js"></script>  -->
<script src="resources/reporting/js/jquery.min.js"></script>
<script src="resources/reporting/js/jquery.popup.js"></script>

<link href="resources/reporting/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/reporting/css/components.css" rel="stylesheet">
<link href="resources/reporting/css/core.css" rel="stylesheet">
<link href="resources/reporting/css/floating.css" rel="stylesheet">
<link href="resources/reporting/css/icons.css" rel="stylesheet">
<link href="resources/reporting/css/jquery.popup.css" rel="stylesheet">
<link href="resources/reporting/css/keyboard.css" rel="stylesheet">
<link href="resources/reporting/css/menu.css" rel="stylesheet">
<link href="resources/reporting/css/pages.css" rel="stylesheet">
<link href="resources/reporting/css/print.css" rel="stylesheet">
<link href="resources/reporting/css/responsive.css" rel="stylesheet">
<link href="resources/reporting/css/Custom/custom.css" rel="stylesheet">
<link href="resources/reporting/css/Custom/font.css" rel="stylesheet">
<link href="resources/reporting/css/Custom/login.css" rel="stylesheet">
</head>
<body class="bg">



	
	<div class="wrapper">
		<div class="container">
		
			<div class='row FeesSubMenu'>
				<div class="col-vm-12">
					<!-- <div class="col-vm-2 FessMaster">
						<div class="Rounded-Rectangle menubutton">
							<img src="../resources/images/Teacher.png" /> <label
								class="menu-name">ફી માસ્ટર</label>
						</div>
					</div> -->
					<!-- 	<div class="col-vm-2 PaymentMethod">
						<div class="Rounded-Rectangle menubutton">
							<img src="../resources/images/Student.png" class="img-white" />
							<label class="menu-name">પેમૅન્ટ પ્રકાર</label>
						</div>
					</div> -->
					<div class="col-vm-2 PurchaseMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Total Purchase</label>
						</div>
					</div>
					<div class="col-vm-2 SalesMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Total Sales</label>
						</div>
					</div>
					<div class="col-vm-2 ExpenseMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Expense</label>
						</div>
					</div>
					<div class="col-vm-2 RecieptMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Reciept</label>
						</div>
					</div>
					<div class="col-vm-2 PaymentsMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Payments</label>
						</div>
					</div>
					<div class="col-vm-2 ReceivableMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Receivables</label>
						</div>
					</div>
						<div class="col-vm-2 PayableMenu">
						<div class="Rounded-Rectangle menubutton">
							 <label
								class="menu-name">Payables</label>
						</div>
					</div>
				</div>
			</div>

			

		<div class="row PurchaseDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						Purchase Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<!-- onclick="searchUser()"
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a>  -->
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="purchasemonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="purchasedayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px  purchaseDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="purchaseday" id="purchasedayselectId" onchange="displaypurchasedata();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_purchaseDaywise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px  purchasemonthwiseDetail"
						style="display: none;">
						month Wise :<select name="purchasemonth"  id="monthWiseSelectId"  onchange="monthWisePurchase();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_purchasemonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!--Sale  Start-->
			<div class="row saleDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						sale Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<!-- onclick="searchUser()"
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a>  -->
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="salemonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="saledayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px  saleDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="saleday" id="saledayselectId" onchange="saledayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_saleDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px  salemonthwiseDetail"
						style="display: none;">
						month Wise :<select name="saeMonthWise"  id="saleMonthWiseSelected"  onchange="monthWiseSale();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_SaleMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>	
		<!-- Sale End -->	
	
			<!--Expense Report  Start-->
			<div class="row expenseDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						Expense Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							onclick="searchUser()"
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a> 
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="expensemonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="expensedayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px expenseDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="expenseday" id="expensedayselectId" onchange="expensedayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_expenseDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px  expensemonthwiseDetail"
						style="display: none;">
						month Wise :<select name="expenseMonthWise"  id="expenseMonthWiseSelected"  onchange="monthWiseexpense();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_expenseMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!-- Create Report  End -->		
		
		<!-- Reciept report -->
		<div class="row recieptDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						Reciept Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a> 
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="recieptmonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="recieptdayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px recieptDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="recieptday" id="recieptdayselectId" onchange="recieptdayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_recieptDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px recieptmonthwiseDetail"
						style="display: none;">
						month Wise :<select name="recieptMonthWise"  id="recieptMonthWiseSelected"  onchange="recieptmonthWise();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_recieptMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!-- end -->
		
		<!-- Reciept report -->
		<div class="row paymentDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						payment Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a> 
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="paymentmonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="paymentdayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px paymentDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="paymentday" id="paymentdayselectId" onchange="paymentdayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_paymentDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px paymentmonthwiseDetail"
						style="display: none;">
						month Wise :<select name="paymentMonthWise"  id="paymentMonthWiseSelected"  onchange="paymentmonthWise();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_paymentMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!-- end -->	
			
			
			<!-- paybles report -->
		<div class="row payablesDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						payables Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a> 
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="payablesmonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="payablesdayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px payablesDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="payablesday" id="payablesdayselectId" onchange="payablesdayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_payablesDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px payablesmonthwiseDetail"
						style="display: none;">
						month Wise :<select name="payablesMonthWise"  id="payablesMonthWiseSelected"  onchange="payablesmonthWise();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_payablesMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!-- end -->	
		
		<!-- recievables report -->
		<div class="row recievablesDivList padding-top-12-5px"
				style="display: none;">
				<hr />
				<div class="col-md-12">
					<div class="col-md-6 stud-info-title">
						recievables Report<b class="stud-info"></b>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"
						style="Padding-left: 0px; Padding-right: 0px; padding-top: 5px;">
						<div id="SearchForm">
							<input type="text" placeholder="Search Here" class="searchTable"
								id="tblusersearch" value="">
							<a href=""
								class="SearchFormTablea"> <img
								src="../resources/images/Search.png" class="img-responsive"
								alt="setting" /></a> 
						</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="col-md-12 padding-top-12-5px">
					<div class="submenuhr">
						<div class="pos-relative pull-left">
							<div class="submenubutton" id="recievablesmonthId">
								<div>Month Wise</div>
							</div>
							<div class="submenubutton" id="recievablesdayId">
								<div>Day Wise</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 padding-top-12-5px recievablesDaywiseDetail"
						style="display: none;">
							Day Wise :<select name="recievablesday" id="recievablesdayselectId" onchange="recievablesdayWise();">
							    <option value="">select</option>
							    <option value="7">7</option>
							    <option value="15">15</option>
							    <option value="30">30</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_recievablesDayWise">
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12 padding-top-12-5px recievablesmonthwiseDetail"
						style="display: none;">
						month Wise :<select name="recievablesMonthWise"  id="recievablesMonthWiseSelected"  onchange="recievablesmonthWise();">
							    <option value="">select</option>
							    <option value="3">3</option>
							    <option value="6">6</option>
							    <option value="12">12</option>
	  							</select>
						<div class="table-responsive list" data-pattern="priority-columns">
							<table id='tech-companies-1' class='table'>
								<thead>
									<tr>
										<td>Bill Number</td>
										<td>Purchase Date</td>
										<td>Total Bill Amount</td>
										<td>Company name</td>
										<td>Payment Status</td>
									</tr>
								</thead>
								<tbody id="tbl_recievablesMonthwise">
								</tbody>
							</table>
						</div>
					</div>
			</div>
			</div>
		<!-- end -->	
<script src="resources/reporting/reportmain.js"></script>
</body>
</html>
