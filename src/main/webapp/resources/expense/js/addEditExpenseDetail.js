angular.module('addExpenseData', ['fileUploadDirective', 'toaster'])
.controller('addExpenseDataController', function($http, fileUploadService, toaster, manageExpenseDataService, billInkDashboardService) {
	var vm = this;
	vm.manageExpenseDataService = manageExpenseDataService;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.expense = {};
	vm.expense.imageURLList = [];
	vm.billInkDashboardService = billInkDashboardService;

	vm.init = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		if (vm.manageExpenseDataService.requestType === "Edit") {
			$http.post('../../expenseData/getExpenseDetailsById', { expenseId: vm.manageExpenseDataService.editExpenseId, companyId : vm.manageExpenseDataService.selectedCompanyId})
			.then(vm.setExpenseDetail, vm.errorFetchData);			
		} else {
			vm.expense.addedBy = {};
			vm.expense.addedBy.userId = vm.billInkDashboardService.userId;
		}
	};

	vm.setExpenseDetail = function(response) {
		vm.expense = angular.copy(response.data.data);
		//vm.searchLedgerNameText = vm.expense.ledgerData.ledgerName;
		vm.searchExpenseNameText = vm.expense.expenseData.ledgerName;
		vm.expense.expenseInvoiceDateObj = new Date(vm.expense.expenseInvoiceDate);
		vm.expense.modifiedBy = {};
		vm.expense.modifiedBy.userId = vm.billInkDashboardService.userId;
	};

	vm.errorFetchData = function() {
	};

	vm.closeAddEditExpenseScreen = function() {
		vm.manageExpenseDataService.getExpenseList(vm.manageExpenseDataService.selectedCompanyId);
		vm.billInkDashboardService.currentViewUrl = "../expense/view/viewExpenseList.html";		
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			vm.closeAddEditExpenseScreen();
		} else if (response.data.status === "error") {
			alert(response.data.errMsg);
		}
	}

	function errorCallback(response) {
		alert(response.data.errMsg);
	}

	vm.saveExpenseData = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		vm.expense.expenseInvoiceDate = moment(vm.expense.expenseInvoiceDateObj).format("YYYY-MM-DD");
		$http.post('../../expenseData/saveExpenseData', vm.expense).then(successCallback, errorCallback);
	};

	vm.publishExpenseData = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId
		vm.expense.expenseInvoiceDate = moment(vm.expense.expenseInvoiceDateObj).format("YYYY-MM-DD");
		$http.post('../../expenseData/publishExpenseData', vm.expense).then(successCallback, errorCallback);		
	};

	function validateExpenseSaveUpdate() {
		if (!vm.manageExpenseDataService.selectedCompanyId || vm.manageExpenseDataService.selectedCompanyId < 1) {
			toaster.showMessage('error', '', 'Company data is missing to save expense details');
			return false;
		}

		if (!vm.expense.ledgerData || vm.expense.ledgerData.ledgerId < 1) {
			toaster.showMessage('error', '', 'Kindly select ledger data to save expense details');
			return false;
		}

		if (!vm.expense.expenseData || vm.expense.ledgerData.ledgerId < 1) {
			toaster.showMessage('error', '', 'Kindly select expesne ledger data to save expense details');
			return false;
		}

		return true;
	}

	vm.uploadFileAndSaveExpenseData = function (action) {
		if (!validateExpenseSaveUpdate()) {
			return false;
		}
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'expense').then(function(result){
				if (result.data.status === "success") {				
					vm.expense.imageURLList = vm.expense.imageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.expense.imageURLList && vm.expense.imageURLList.length > 0) {						
						vm.expense.imageURL = vm.expense.imageURLList[0].imageURL;
					}
					if (action === "Save") {
						vm.saveExpenseData();						
					} else if (action === "Publish") {						
						vm.publishExpenseData();
					}
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.expense.imageURL = null;
			if (vm.expense.imageURLList && vm.expense.imageURLList.length > 0) {						
				vm.expense.imageURL = vm.expense.imageURLList[0].imageURL;
			}
			if (action === "Save") {
				vm.saveExpenseData();						
			} else if (action === "Publish") {						
				vm.publishExpenseData();
			}
		}
	};

	vm.updateSavedExpenseData = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		vm.expense.expenseInvoiceDate = moment(vm.expense.expenseInvoiceDateObj).format("YYYY-MM-DD");
		$http.post('../../expenseData/updateSavedExpenseData', vm.expense).then(successCallback, errorCallback);
	};

	vm.updatePublishExpenseData = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		vm.expense.expenseInvoiceDate = moment(vm.expense.expenseInvoiceDateObj).format("YYYY-MM-DD");
		$http.post('../../expenseData/updatePublishedExpenseData', vm.expense).then(successCallback, errorCallback);
	};

	vm.uploadFileAndUpdateExpenseData = function(action) {
		if (!validateExpenseSaveUpdate()) {
			return false;
		}
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'expense').then(function(result){
				if (result.data.status === "success") {
					vm.expense.imageURLList = vm.expense.imageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.expense.imageURLList && vm.expense.imageURLList.length > 0) {						
						vm.expense.imageURL = vm.expense.imageURLList[0].imageURL;
					}
					if (action === "Save") {
						vm.updateSavedExpenseData();						
					} else if (action === "Publish") {						
						vm.updatePublishExpenseData();
					}
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.expense.imageURL = null;
			if (vm.expense.imageURLList && vm.expense.imageURLList.length > 0) {						
				vm.expense.imageURL = vm.expense.imageURLList[0].imageURL;
			}
			if (action === "Save") {
				vm.updateSavedExpenseData();						
			} else if (action === "Publish") {						
				vm.updatePublishExpenseData();
			}
		}
	};

	function getImageURLListObj(imageURLList) {
		var expenseImageURLList = [];
		var expenseImageURLObj = {};

		for (var count = 0; count < imageURLList.length; count++) {
			expenseImageURLObj = {};
			expenseImageURLObj.imageURL = imageURLList[count];
			expenseImageURLList.push(expenseImageURLObj);
		}

		return expenseImageURLList;
	}

	vm.markExpenseAsVerified = function() {
		vm.expense.verifiedBy = {};
		vm.expense.verifiedBy.userId = vm.billInkDashboardService.userId;
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		$http.post('../../expenseData/markExpenseAsVerified', vm.expense).then(successCallback, errorCallback);
	};

	vm.publishSavedExpense = function() {
		vm.expense.companyId = vm.manageExpenseDataService.selectedCompanyId;
		$http.post('../../expenseData/publishSavedExpense', vm.expense).then(successCallback, errorCallback);
	};

	vm.searchLedger = function() {

		if (vm.expense.ledgerData && vm.expense.ledgerData.ledgerName === vm.searchLedgerNameText) {

			vm.searchLedgerList = [];
			return;
 		}

		return $http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageExpenseDataService.selectedCompanyId })
		.then(function(response) {
			vm.searchLedgerList = response.data.data;
			return vm.searchLedgerList; 
		}, errorCallback);
	};
	
	vm.searchExpense = function() {
		if (vm.expense.expenseData && vm.expense.expenseData.ledgerName === vm.searchExpenseNameText) {
 			
			vm.searchExpenseList = [];
			return;
 		}
		return $http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchExpenseNameText, companyId: vm.manageExpenseDataService.selectedCompanyId })
				.then(function(response) {
					vm.searchExpenseList = response.data.data;
					return vm.searchExpenseList; 
				}, errorCallback);
	};

	function getExpenseDataSuccessCallBack(response) {
		vm.expense.expenseData = {};
		vm.searchExpenseList = response.data.data;
	}

	vm.selectExpense = function(ledgerObj) {
		//		vm.searchExpenseNameText = ledgerObj.ledgerName;
		//		vm.expense.expenseData = {};
		
				if (!ledgerObj || !ledgerObj.ledgerName) {
					return;
				}
		
				vm.expense.expenseData = ledgerObj;
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.expense.ledgerData = {};
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		if (!ledgerObj || !ledgerObj.ledgerName) {
					return;
		}
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		vm.expense.ledgerData = {};
		vm.expense.ledgerData.ledgerId = ledgerObj.ledgerId;
	};

	vm.openImageModel = function(index) {
		if (index < 0 || index > (vm.expense.imageURLList - 1)) {
			return;
		}

		vm.expenseImageIndex = index;
		$("#expense-image-show-model .modal").modal('show');
		vm.selectedPhotoURL = vm.expense.imageURLList[index].imageURL;
	};

	vm.fetchImage = function(position) {
		vm.expenseImageIndex += position;

		if (vm.expenseImageIndex < 0) {
			vm.expenseImageIndex = vm.expense.imageURLList.length - 1;
		} else if (vm.expenseImageIndex >= vm.expense.imageURLList.length) {
			vm.expenseImageIndex = 0;
		}
		vm.selectedPhotoURL = vm.expense.imageURLList[vm.expenseImageIndex].imageURL;
	};

	vm.deleteImage = function(index) {
		vm.expense.imageURLList.splice(index, 1);
	};

});