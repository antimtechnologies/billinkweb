angular.module('manageExpenseData', ['infinite-scroll'])
.service("manageExpenseDataService", function($http) {
	
	var vm = this;

	vm.expenseList = [];
	vm.startOfExpense = 0;
	vm.noOfRecordForExpense = 12;
	vm.expenseFilterData = {};
	vm.hasMoreExpenseData = true;

	vm.companyCountRecordList = [];
	vm.startOfCompanyCount = 0;
	vm.noOfRecordForCompanyCount = 12;
	vm.hasMoreCompanyCountData = true;

	vm.getExpenseList = function(companyId) {
		$http.post('../../expenseData/getExpenseListData', {
			companyId: companyId, 
			start: vm.startOfExpense, 
			noOfRecord: vm.noOfRecordForExpense,
			filterData : vm.expenseFilterData
		}).then(successCallback, errorCallback);	
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfExpense === 0) {
				vm.expenseList = response.data.data;
				if (vm.expenseList.length < vm.noOfRecordForExpense) {
					vm.hasMoreExpenseData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForExpense) {				
				vm.hasMoreExpenseData = false;
			}

			vm.expenseList = vm.expenseList.concat(response.data.data);
		}
	}

	function errorCallback() {
			
	}

	vm.getCompanyCountData = function() {
		$http.post('../../companydata/getCompanyRequestCountDetails', {start: vm.startOfCompanyCount, noOfRecord: vm.noOfRecordForCompanyCount, sortParam: 'expensePendingCount' }).
		then(companyCountSuccessCallBack, errorCallback);
	};

	function companyCountSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompanyCount === 0) {
				vm.companyCountRecordList = response.data.data;
				if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
					vm.hasMoreCompanyCountData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCompanyCount) {				
				vm.hasMoreCompanyCountData = false;
			}

			vm.companyCountRecordList = vm.companyCountRecordList.concat(response.data.data);
		}
	}

})
.controller('manageExpenseDataController', function($http, $ocLazyLoad, manageExpenseDataService, billInkDashboardService) {
	var vm = this;
	vm.manageExpenseDataService = manageExpenseDataService;
	vm.billInkDashboardService = billInkDashboardService;

	if (vm.billInkDashboardService.userRole !== "Vendor") {
		vm.statusList = ['New', 'Modified', 'Deleted'];
	} else {		
		vm.statusList = ['New', 'Modified', 'Deleted', 'UnPublished'];
	}

	vm.manageExpenseDataService.expenseFilterData = {};
	vm.manageExpenseDataService.expenseFilterData.statusList = [];

	vm.openExpenseList = function(companyObj) {

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageExpenseDataService.expenseFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageExpenseDataService.expenseFilterData.isVerified = 0;
		}

		vm.billInkDashboardService.currentCompanyObj = companyObj;
		vm.manageExpenseDataService.selectedCompanyId = companyObj.companyId;
		vm.manageExpenseDataService.startOfExpense = 0;
		vm.billInkDashboardService.currentViewUrl = "../expense/view/viewExpenseList.html";
		vm.manageExpenseDataService.getExpenseList(vm.manageExpenseDataService.selectedCompanyId);
	};

	vm.init = function() {
		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.billInkDashboardService.currentViewUrl = "../expense/view/companyWiseExpenseCount.html";
			vm.manageExpenseDataService.getCompanyCountData();
		} else {			
			vm.openExpenseList(vm.billInkDashboardService.selectedCompany);
		}
	};
	
	vm.backToCompanyWiseExpenseData = function() {
			vm.billInkDashboardService.currentViewUrl = "../expense/view/companyWiseExpenseCount.html";
			vm.manageExpenseDataService.getCompanyCountData();
	};

	vm.loadMoreExpense = function() {
		if (vm.manageExpenseDataService.hasMoreExpenseData) {
			vm.manageExpenseDataService.startOfExpense +=  vm.manageExpenseDataService.noOfRecordForExpense;
			vm.manageExpenseDataService.getExpenseList(vm.manageExpenseDataService.selectedCompanyId);
		}
	};
	
	vm.loadMoreCompanyCountData = function() {
		if (vm.manageExpenseDataService.hasMoreCompanyCountData) {
			vm.manageExpenseDataService.startOfCompanyCount +=  vm.manageExpenseDataService.noOfRecordForExpense;
			vm.manageExpenseDataService.getCompanyCountData();
		}
	};

	vm.showAddExpenseDataForm = function(requestFor) {
		vm.manageExpenseDataService.requestType = "Add";
		vm.manageExpenseDataService.requestFor = requestFor;
		vm.manageExpenseDataService.startOfExpense = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../expense/js/addEditExpenseDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../expense/view/addEditExpenseDetail.html";
		});
	};

	vm.showDeletePrompt = function(expenseId, statusName) {
		vm.selectedDeleteExpenseId = expenseId;
		vm.selectedDeleteExpenseStatus = statusName;
		$("#expense-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteSavedExpenseData = function() {
		$http.post('../../expenseData/deleteSavedExpenseData', {
			expenseId: vm.selectedDeleteExpenseId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageExpenseDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	}

	vm.deletePublishExpenseData = function() {
		$http.post('../../expenseData/deletePublishedExpenseData', {
			expenseId: vm.selectedDeleteExpenseId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageExpenseDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	};

	vm.deleteExpenseData = function() {
		if (vm.selectedDeleteExpenseStatus === "UnPublished") {
			vm.deleteSavedExpenseData();
		} else {
			vm.deletePublishExpenseData();
		}
	};

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteExpenseId = 0;
		vm.selectedDeleteExpenseStatus = "";
		$("#expense-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
	}

	vm.editExpenseData = function(expenseId, imageURL) {
		vm.manageExpenseDataService.requestType = "Edit";
		vm.manageExpenseDataService.requestFor = imageURL && imageURL !== "" ? "uploadImage" : "enterManually";
		vm.manageExpenseDataService.editExpenseId = expenseId;
		vm.manageExpenseDataService.startOfExpense = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../expense/js/addEditExpenseDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../expense/view/addEditExpenseDetail.html";
		});
	};

	// filter related function start
	vm.launchFilter = function() {
		$("#expense-filter-dialog .modal").modal('show');
	};

	vm.closeFilterModal = function() {
		$("#expense-filter-dialog .modal").modal('hide');
	};

	vm.applyFilter = function() {
		vm.searchFilterData();
		$("#expense-filter-dialog .modal").modal('hide');
	};

	vm.searchFilterData = function() {
		vm.manageExpenseDataService.startOfExpense = 0;
		vm.manageExpenseDataService.getExpenseList(vm.manageExpenseDataService.selectedCompanyId);
	};

	vm.resetFilter = function() {
		vm.manageExpenseDataService.expenseFilterData = {};
		vm.manageExpenseDataService.expenseFilterData.statusList = [];

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageExpenseDataService.expenseFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageExpenseDataService.expenseFilterData.isVerified = 0;
		}

		vm.manageExpenseDataService.startOfExpense = 0;
		vm.manageExpenseDataService.getExpenseList(vm.manageExpenseDataService.selectedCompanyId);
		$("#expense-filter-dialog .modal").modal('hide');
	};

	vm.toggleSelection = function toggleSelection(status) {
		var idx = vm.manageExpenseDataService.expenseFilterData.statusList.indexOf(status);
	
		if (idx > -1) {
			vm.manageExpenseDataService.expenseFilterData.statusList.splice(idx, 1);
		} else {
			vm.manageExpenseDataService.expenseFilterData.statusList.push(status);
		}
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageExpenseDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.manageExpenseDataService.expenseFilterData.ledgerId = 0;
			vm.searchLedgerList = [];
		}
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.manageExpenseDataService.expenseFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
	};
	// filter related function end

	vm.$onDestroy = function() {
		vm.manageExpenseDataService.expenseList = [];
		vm.manageExpenseDataService.startOfExpense = 0;
		vm.manageExpenseDataService.hasMoreExpenseData = true;

		vm.manageExpenseDataService.companyCountRecordList = [];
		vm.manageExpenseDataService.startOfCompanyCount = 0;
		vm.manageExpenseDataService.hasMoreCompanyCountData = true;

		vm.manageExpenseDataService.selectedCompanyId = 0;
	}
});
