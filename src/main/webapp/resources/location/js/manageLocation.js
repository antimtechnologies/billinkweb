var managelocationDataApp = angular.module('manageLocation', ['infiniteScroll', 'infinite-scroll']);

managelocationDataApp.service('manageLocationDataService', function($http){
	var svc 					= this;
	
	svc.selectedCompanyId 		= 0;
	svc.startOfItem				= 0;
	svc.noOfRecordForItem		= 30;
	
	svc.totalItemCount 			= 0;
	svc.locationListObj = [];
	
	svc.stateList = [];
	
	svc.getCompanyLocationList=function(companyId){
		svc.selectedCompanyId=companyId;
		$http.post('../../locationData/getLocationList', {start: svc.startOfItem, noOfRecord:svc.noOfRecordForItem,companyId: svc.selectedCompanyId}).then(successCallbackLocationList, errorCallbackLocationList);
		
	};
	function successCallbackLocationList(response) {
		
		svc.locationListObj = angular.copy(response.data.data);
	}
	
	
	

	function errorCallbackLocationList(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}
	
});



managelocationDataApp.controller('manageLocationDataController', function($http, $ocLazyLoad, manageLocationDataService, billInkDashboardService) {
	var vm 							= this;
	vm.manageLocationDataService =  manageLocationDataService;
	vm.manageLocationDataService.locationContainerURL = "../location/view/viewLocationList.html";
	vm.manageLocationDataService.selectedCompanyId=0;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.billInkDashboardService 		= billInkDashboardService;
	vm.selectedCompanyId =  0;
	vm.locationListObj=[];
	
	vm.init = function() {
		
		
		vm.getCompanyList();
		vm.getLocationListObj();
		$http.post('../../stateData/getStateList', vm.billInkDashboardService.ledger).then(vm.setStateList, vm.errorFetchData);
		
		//console.log("company id :"+vm.selectedCompanyId);
//		$http.get('../../locationData/findbycompanyId?companyId='+vm.selectedCompanyId)
//		.then(getlocationSuccessCallBack, errorCallback);
	};
		
	vm.getLocationListObj=function(){
		console.log("Chekc");
		vm.manageLocationDataService.getCompanyLocationList(vm.selectedCompanyId);
//		$http.post('../../locationData/getLocationList', {start: vm.startOfCompany, noOfRecord:noOfRecordForCompany,companyId: vm.selectedCompanyId}).then(successCallbackLocationList, errorCallbackLocationList);
		
	};
	
	
	
	
	
	function successCallbackLocationList(response) {
		vm.locationListObj = angular.copy(response.data.data);
	}
	
	
	

	function errorCallbackLocationList(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}
	
	
	
	
	vm.addLocationDataSave=function(){
		vm.manageLocationDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../location/js/addEditLocationData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageLocationDataService.locationContainerURL = "../location/view/addEditLocationData.html";
		});
		
	}

	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

//	function getlocationSuccessCallBack(response){
//		vm.manageLocationDataService.getLocationList = response.data.result;
//		console.log("location name :"+vm.manageLocationDataService.getLocationList[0].locationName);
//	}
	
	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					vm.manageLocationDataService.selectedCompanyId = vm.companyList[0].companyId;
					vm.selectedCompanyId = vm.companyList[0].companyId;
					vm.getLocationListObj()
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
		}
	}

	function errorCallback() {
		
	}

	vm.setStateList = function(response) {
		vm.billInkDashboardService.stateList = response.data.data;
	};
	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};

	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
			
		}
	};



	vm.loadMoreItem = function() {
		if (vm.manageLocationDataService.totalItemCount > vm.manageLocationDataService.startOfItem) {			
			vm.manageLocationDataService.startOfItem += vm.manageLocationDataService.noOfRecordForItem;
		
		}
	};
// add location 

//	vm.uploadFileAndSaveLocationData = function () {
//		vm.addLocation();
//
//	};
//	
//	vm.addLocation = function() {
//		if (!vm.billInkDashboardService.location.locationName || vm.billInkDashboardService.location.locationName === "") {
//			toaster.showMessage('error', '', 'Kindly enter Location name.');
//			return false;
//		}
//		vm.billInkDashboardService.location.companyId = vm.manageLocationDataService.selectedCompanyId;
//		
//		if(vm.getLocationList.length > 0){
//			for (var i = 0; i < vm.getLocationList.length; i++) {
//				if (vm.billInkDashboardService.location.locationName === vm.getLocationList[i].locationName) {
//					toaster.showMessage('error', '', 'Location name already exits.');
//					return false;
//			}
//		}
//		}
//		$http.post('../../locationData/savelocation', vm.billInkDashboardService.location).then(successCallback, errorCallback);
//		
//		
//	};
//	
//	
//	function successCallback(response) {
//		if (response.data.status === "success") {
//			vm.closeItemDataSave();
//		} else {
//			toaster.showMessage('error', '', response.data.errMsg);
//		}
//	}
//	
//	vm.closeItemDataSave = function() {
//		vm.manageLocationDataService.editItemId = 0;
//		vm.manageLocationDataService.startOfItem = 0;
//	}
	
	// end location add
	vm.addLocationData = function() {
		
		vm.manageLocationDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../location/js/addEditLocationData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageLocationDataService.locationContainerURL = "../location/view/addEditLocationData.html";
		});
	};

	
	vm.getCompayItemDetail = function(companyId) {
		vm.selectedCompanyId = companyId;
		vm.manageLocationDataService.selectedCompanyId = companyId;
		vm.manageLocationDataService.startOfItem = 0;
		vm.manageLocationDataService.itemList = [];
		vm.getLocationListObj();
	};

	function deleteItemSuccessCallback() {
		vm.closeDeleteModal();
		vm.getCompayLocationDetail(vm.manageLocationDataService.selectedCompanyId);
	}

	vm.getCompayLocationDetail = function(companyId) {
		vm.selectedCompanyId=companyId;
		vm.manageLocationDataService.selectedCompanyId = companyId;
		vm.manageLocationDataService.startOfItem = 0;
		vm.manageLocationDataService.itemList = [];
		vm.getCompanyList();
		vm.getLocationListObj();
	};
	vm.closeDeleteModal = function() {
		vm.selectedDeleteItemId = 0;
		$("#item-delete-confirmation-dialog .modal").modal('hide');
	};

	
	vm.deleteLocation=function(deleteId){
		$http.post('../../locationData/deleteLocation', {locationId:deleteId,companyId : vm.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
		.then(deleteLocationsuccesscallback, errorCallback);
	}
	
	function deleteLocationsuccesscallback(){
		toaster.showMessage('success', '', "Delete Records Successfully.");
		vm.getLocationListObj();
	}
	
	
	function errorCallback() {
		
	}

	vm.editItemData = function(itemId) {
		vm.manageLocationDataService.requestType = "Edit";
		vm.manageLocationDataService.editItemId = itemId;
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../item/js/addEditItemData.js"],
				{serie: true}
		)
		.then(function () {
			vm.manageLocationDataService.itemContainerURL = "../item/view/addEditItemData.html";
		});
	};

});

