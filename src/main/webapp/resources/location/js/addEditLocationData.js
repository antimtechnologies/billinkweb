var aaddLocationDataApp = angular.module('addEditLocationData', ['fileUploadDirective', 'toaster']);

aaddLocationDataApp.controller('addEditLocationDataController', function($http,$ocLazyLoad, fileUploadService, toaster, manageLocationDataService, billInkDashboardService) {
	var vm = this;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.taxCodeList = [];
	vm.manageLocationDataService = manageLocationDataService;
	vm.manageLocationDataService.startOfItem=0;
	vm.manageLocationDataService.noOfRecordForItem=30;
	vm.manageLocationDataService.locationListObj=[];
	vm.billInkDashboardService = billInkDashboardService;
	vm.billInkDashboardService.location = {};
	vm.billInkDashboardService.location.locationDocumentList = [];

	vm.init = function() {
		$http.post('../../stateData/getStateList', vm.billInkDashboardService.ledger).then(vm.setStateList, vm.errorFetchData);
		
		//console.log("company Id ==:"+vm.billInkDashboardService.location.companyId);
	};


	
	
	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}

	vm.setStateList = function(response) {
		vm.billInkDashboardService.stateList = response.data.data;
	};

//	vm.updateItem = function() {
//		
//		if (!vm.billInkDashboardService.item.itemName && vm.billInkDashboardService.item.itemName === "") {
//			toaster.showMessage('error', '', 'Kindly enter item name.');
//			return false;
//		}
//
//		$http.post('../../itemData/updateItemData', vm.billInkDashboardService.item).then(successCallback, errorCallback);
//	};
//
//	vm.uploadFileAndUpdateItemData = function() {
//		var file = vm.profilePhotoFile;
//		if (vm.profilePhotoFile) {			
//			fileUploadService.uploadFileToUrl(file, 'item').then(function(result){
//				if (result.data.status === "success") {				
//					vm.billInkDashboardService.item.profilePhotoURL = result.data.imagePath;
//					vm.updateItem();
//				} else {
//					toaster.showMessage('error', '', 'error while uploading document');
//				}
//			}, function(error) {
//				toaster.showMessage('error', '', 'error while uploading document');
//			});
//		} else {
//			vm.updateItem();
//		}
//	};



	vm.addDocument = function() {
		angular.element("input[type='file']").val(null);
		vm.locationDocumentObj = {};
		vm.locationDocumentFile = "";
		$("#add-document-dialog .modal").modal('show');
	};

	vm.closedDocumentModal = function() {
		$("#add-document-dialog .modal").modal('hide');
	};

	vm.saveDocument = function() {
		var file = vm.locationDocumentFile;
		if (vm.locationDocumentFile) {			
			fileUploadService.uploadFileToUrl(file, 'company').then(function(result){
				if (result.data.status === "success") {				
					vm.locationDocumentObj.documentUrl = result.data.imagePath;
					vm.billInkDashboardService.location.locationDocumentList.push(vm.locationDocumentObj);
					toaster.showMessage('success', '', 'Records Added.');
				} else {
					toaster.showMessage('error', '', 'error while uploading document');
				}
				vm.locationDocumentObj = {};
				vm.locationDocumentFile = undefined;
				$("#add-document-dialog .modal").modal('hide');
				
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading document');
			});
		}
	};

	vm.deleteDocument = function(index) {
		vm.billInkDashboardService.location.locationDocumentList.splice(index, 1);
	};

	vm.$onDestroy = function() {
		vm.billInkDashboardService.categoryList = [];
		vm.billInkDashboardService.brandList = [];
		vm.billInkDashboardService.taxCodeList = [];
	}

	vm.uploadFileAndSaveLocationData = function () {
		vm.addLocation();

	};
	
	vm.addLocation = function() {
		if (!vm.billInkDashboardService.location.locationName || vm.billInkDashboardService.location.locationName === "") {
			toaster.showMessage('error', '', 'Kindly enter Location name.');
			return false;
		}
		vm.billInkDashboardService.location.companyId = vm.manageLocationDataService.selectedCompanyId;
		
		$http.post('../../locationData/checklocationunique', vm.billInkDashboardService.location).then(function(response){
							if(response.data.data=="exists"){
								toaster.showMessage('error', '', 'Location Name is already there. Please change it.');
								return false;
							}else{
//								alert("vishal");
//								$http.get('../../locationData/findbycompanyId?companyId='+vm.manageLocationDataService.selectedCompanyId)
//								.then(function (response){
//									vm.manageLocationDataService.locationListObj = response.data.result;
//									console.log("location name :"+vm.manageLocationDataService.locationListObj[0].locationName);
//								}, errorCallback);
								
								console.log("company Id :"+vm.manageLocationDataService.selectedCompanyId);
								console.log("length :"+vm.manageLocationDataService.locationListObj.length);
								if(vm.manageLocationDataService.locationListObj.length > 0){
									for (var i = 0; i < vm.manageLocationDataService.locationListObj.length; i++) {
										console.log("Added :"+vm.billInkDashboardService.location.locationName);
										console.log("serach :"+vm.manageLocationDataService.locationListObj[i].locationName);
										if (vm.billInkDashboardService.location.locationName == vm.manageLocationDataService.locationListObj[i].locationName) {
											//toaster.showMessage('error', '', 'Location name already exits.');
											//return false;
									   }
								    }
								}
								$http.post('../../locationData/savelocation', vm.billInkDashboardService.location).then(successCallback, errorCallback);
								
							}
		},errorCallback)
		
		
		
	};
	
//	function getlocationSuccessCallBack(response){
//		vm.manageLocationDataService.getLocationList = response.data.result;
//		console.log("location name :"+vm.manageLocationDataService.getLocationList[0].locationName);
//	}
	
	function successCallback(response) {
		if (response.data.status === "success") {
			vm.closeLocationDataSave();
			toaster.showMessage('success', '', 'data saved successfully.');
//			vm.manageLocationDataService.selectedCompanyId=vm.billInkDashboardService.location.companyId;
//			vm.manageLocationDataService.getLocationListObj
//			vm.manageLocationDataService.getCompanyLocationList();
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}
	
	
	vm.closeLocationDataSave = function() {
	
		vm.manageLocationDataService.startOfItem = 0;
		
		$ocLazyLoad
		.load(
				["../location/js/manageLocation.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageLocationDataService.locationContainerURL = "../location/view/viewLocationList.html";
		});
		vm.manageLocationDataService.selectedCompanyId=vm.billInkDashboardService.location.companyId;
		console.log(vm.billInkDashboardService.location.companyId);
//		vm.manageLocationDataService.locationContainerURL = "../location/view/viewLocationList.html";
		vm.manageLocationDataService.getCompanyLocationList(vm.billInkDashboardService.location.companyId);
		
		
	}
	
//	vm.closeItemDataSave = function() {
//		vm.manageLocationDataService.editItemId = 0;
//		vm.manageLocationDataService.startOfItem = 0;
//	}
	
});
