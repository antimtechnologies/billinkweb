angular
		.module('manageReport', [ 'infinite-scroll' ])
		.service(
				"manageReportService",
				function($http) {

					var vm = this;
// for purchase report	
					vm.PurchaseRegisterAvaliableList = [
							[ 'Billink id', 'purchaseId', true ],
							[ 'Invoice Date', 'billDate', true ],
							[ 'Purchase Date', 'purchaseDate', true ],
						/*	[ 'Comapny Name', 'comapnyName', true ],*/
							[ 'Ledger Name', 'ledgerName', true ],
							[ 'billNumber', 'billNumber', true ],
							/*[ 'Naration', 'description', true ],*/
							[ 'Ref No 1', 'referenceNumber1', true ],
							[ 'Ref No 2', 'referenceNumber2', true ],
							[ 'Credit Period', 'creditPeriod', true ],
							[ 'GSTN', 'gstNumber', true ],
							[ 'Item Name', 'itemName', true ],
							[ 'SKU', 'sku', true ],
							[ 'EAN', 'eanNo', true ],
							[ 'HSN', 'hsnCode', true ],
							[ 'LocationName', 'locationName', true ],
							[ 'Batch No', 'batchNo', true ],
							[ 'Mfg. Date', 'mfgDate', true ],
							[ 'Shelf life', 'shelfLife', true ],
							[ 'Exp. Date', 'expDate', true ],
							[ 'Qty', 'item_quantity', true ],
							[ 'Rate', 'item_purchaseRate', true ],
							/*[ 'Item Tax Rate', 'item_taxRate', true ],
							[ 'Item Amount', 'item_amount', true ],*/
							[ 'Discount', 'itemDiscount', true ],
							[ 'Additional Discount', 'additionalitemDiscount', true ],
							[ 'Tax Rate', 'item_taxRate', true ],
							[ 'IGST', 'item_iGST', true ],
							[ 'SGST', 'item_sGST', true ],
							[ 'CGST', 'item_cGST', true ],
							[ 'Total Amount', 'item_total_amount', true ],
							
							[ 'Charge LedgerName', 'chargeledgerName', true ],
							[ 'SAC', 'sacCode', true ],
							[ 'Rate', 'charge_purchase', true ],
							[ 'Discount', 'chargeDiscount', true ],	
							[ 'TaxRate', 'charge_taxRate', true ],
							[ 'IGST', 'charge_iGST', true ],
							[ 'SGST', 'charge_sGST', true ],
							[ 'CGST', 'charge_cGST', true ],
							[ 'TaxableAmount', '	', true ],
							
							[ 'Total Qty', 'item_quantity', true ],
							[ 'Total Taxable Value', 'total_taxable_amt', true ],
							[ 'IGST', 'total_igst', true ],
							[ 'CGST', 'total_cgst', true ],
							[ 'SGST', 'total_sgst', true ],
							[ 'Gross Total', 'grossTotalAmount', true ],
							[ 'Discount', 'discount', true ],
							[ 'Total', 'totalBillAmount', true ],
							[ 'Narration', 'description', true ]];
							
					// for sale report		
					
					vm.saleRegisterAvaliableList = [
						[ 'Billink id', 'sellId', true ],
						[ 'Sale Date', 'sellBillDate', true ],
						/*[ 'Comapny Name', 'comapnyName', true ],*/
						[ 'Ledger Name', 'ledgerName', true ],
						[ 'BillNo.', 'billNumber', true ],
						/*[ 'Naration', 'description', true ],*/
						[ 'Ref No 1', 'referenceNumber1', true ],
						[ 'Ref No 2', 'referenceNumber2', true ],
						[ 'Credit Period', 'creditPeriod', true ],
						[ 'GSTN', 'gstNumber', true ],
						
						[ 'Item Name', 'itemName', true ],
						[ 'SKU', 'sku', true ],
						[ 'EAN', 'eanNo', true ],
						[ 'HSN', 'hsnCode', true ],
						[ 'LocationName', 'locationName', true ],
						[ 'Batch No', 'batchNo', true ],
						[ 'Mfg. Date', 'mfgDate', true ],
						[ 'Shelf life', 'shelfLife', true ],
						[ 'Exp. Date', 'expDate', true ],
						[ 'Qty', 'item_quantity', true ],
						[ 'Rate', 'item_saleRate', true ],
						[ 'Item Tax Rate', 'item_taxRate', true ],
						/*[ 'Item Amount', 'item_amount', true ],*/
						[ 'Discount', 'itemDiscount', true ],
						[ 'Additional Discount', 'additionalitemDiscount', true ],
						[ 'IGST', 'item_iGST', true ],
						[ 'SGST', 'item_sGST', true ],
						[ 'CGST', 'item_cGST', true ],
						[ 'Taxable Amount', 'item_total_amount', true ],
						
						
						[ 'Charge LedgerName', 'chargeledgerName', true ],
						[ 'SAC', 'sacCode', true ],
						[ 'RATE', 'charge_sale', true ],
						[ 'Discount', 'chargeDiscount', true ],
						[ 'TaxRate', 'charge_taxRate', true ],
						[ 'IGST', 'charge_iGST', true ],
						[ 'SGST', 'charge_sGST', true ],
						[ 'CGST', 'charge_cGST', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total Charge Amount', 'chargeAmount', true ],
						
						
						[ 'Total Qty', 'item_quantity', true ],
						[ 'Total Taxable Value', 'total_taxable_amt', true ],
						[ 'IGST', 'total_igst', true ],
						[ 'CGST', 'total_cgst', true ],
						[ 'SGST', 'total_sgst', true ],
						[ 'Gross Total', 'grossTotalAmount', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total', 'totalBillAmount', true ],
						[ 'Narration', 'description', true ]];
						
					
					
					// for credit note 
					
					vm.creditnoteRegisterAvaliableList = [
						[ 'Billink id', 'creditNoteId', true ],
						[ 'Credit Date', 'creditNoteDate', true ],
						/*[ 'Comapny Name', 'comapnyName', true ],*/
						[ 'Ledger Name', 'ledgerName', true ],
						[ 'Creadit No.', 'billNumber', true ],
						[ 'Naration', 'description', true ],
						[ 'Ref No 1', 'referenceNumber1', true ],
						[ 'Ref No 2', 'referenceNumber2', true ],
						[ 'Original Invoice No.', 'billNumber', true ],
						[ 'Original Invoice Date', 'invoiceDate', true ],
						[ 'GSTN', 'gstNumber', true ],
						
						[ 'Item Name', 'itemName', true ],
						[ 'SKU', 'sku', true ],
						[ 'EAN', 'eanNo', true ],
						[ 'HSN', 'hsnCode', true ],
						[ 'LocationName', 'locationName', true ],
						[ 'Batch No', 'batchNo', true ],
						[ 'Mfg. Date', 'mfgDate', true ],
						[ 'Shelf life', 'shelfLife', true ],
						[ 'Exp. Date', 'expDate', true ],
						[ 'Qty', 'item_quantity', true ],
						[ 'Rate', 'item_saleRate', true ],
						[ 'Discount', 'itemDiscount', true ],
						[ 'Additional Discount', 'additionaldiscount', true ],
						[ 'FreeItem Qty', 'freequantity', true ],
						[ 'Tax Rate', 'item_taxRate', true ],
						[ 'IGST', 'item_iGST', true ],
						[ 'SGST', 'item_sGST', true ],
						[ 'CGST', 'item_cGST', true ],
						[ 'Taxable Amount', 'item_total_amount', true ],
					
						
						[ 'Charge LedgerName', 'chargeledgerName', true ],
						[ 'SAC', 'sacCode', true ],
						[ 'RATE', 'charge_sale', true ],
						[ 'Discount', 'chargeDiscount', true ],
						[ 'TaxRate', 'charge_taxRate', true ],
						[ 'IGST', 'charge_iGST', true ],
						[ 'SGST', 'charge_sGST', true ],
						[ 'CGST', 'charge_cGST', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total Charge Amount', 'chargeAmount', true ],
						
						
						[ 'Total Qty', 'item_quantity', true ],
						[ 'Total Taxable Value', 'total_taxable_amt', true ],
						[ 'IGST', 'total_igst', true ],
						[ 'CGST', 'total_cgst', true ],
						[ 'SGST', 'total_sgst', true ],
						[ 'Gross Total', 'grossTotalAmount', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total', 'totalBillAmount', true ],
						[ 'Narration', 'description', true ]];
					
					// for debit note 
					
					vm.debitnoteRegisterAvaliableList = [
						[ 'Billink id', 'debitNoteId', true ],
						[ 'Debit Date', 'debitNoteDate', true ],
						/*[ 'Comapny Name', 'comapnyName', true ],*/
						[ 'Ledger Name', 'ledgerName', true ],
						[ 'DebitNote No.', 'billNumber', true ],
						/*[ 'Naration', 'description', true ],*/
						[ 'Ref No 1', 'referenceNumber1', true ],
						[ 'Ref No 2', 'referenceNumber2', true ],
						[ 'Original Invoice No.', 'originalInvoiceNumber', true ],
						[ 'Original Invoice Date', 'originalInvoiceDate', true ],
						
						
						[ 'Item Name', 'itemName', true ],
						[ 'SKU', 'sku', true ],
						[ 'EAN', 'eanNo', true ],
						[ 'HSN', 'hsnCode', true ],
						[ 'LocationName', 'locationName', true ],
						[ 'Batch No', 'batchNo', true ],
						[ 'Mfg. Date', 'mfgDate', true ],
						[ 'Shelf life', 'shelfLife', true ],
						[ 'Exp. Date', 'expDate', true ],
						[ 'Qty', 'item_quantity', true ],
						[ 'Item Purchase Rate', 'item_purchaserate', true ],
						[ 'Discount', 'itemDiscount', true ],
						[ 'Additional Discount', 'additionaldiscount', true ],
						/*[ 'FreeItem Qty', 'freequantity', true ],*/
						[ 'Tax Rate', 'item_taxRate', true ],
						[ 'IGST', 'item_iGST', true ],
						[ 'SGST', 'item_sGST', true ],
						[ 'CGST', 'item_cGST', true ],
						[ 'Taxable Amount', 'item_total_amount', true ],
						
						[ 'Charge LedgerName', 'chargeledgerName', true ],
						[ 'SAC', 'sacCode', true ],
						[ 'RATE', 'charge_sale', true ],
						[ 'Discount', 'chargeDiscount', true ],
						[ 'TaxRate', 'charge_taxRate', true ],
						[ 'IGST', 'charge_iGST', true ],
						[ 'SGST', 'charge_sGST', true ],
						[ 'CGST', 'charge_cGST', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total Charge Amount', 'chargeAmount', true ],
						
						
						[ 'Total Qty', 'item_quantity', true ],
						[ 'Total Taxable Value', 'total_taxable_amt', true ],
						[ 'IGST', 'total_igst', true ],
						[ 'CGST', 'total_cgst', true ],
						[ 'SGST', 'total_sgst', true ],
						[ 'Gross Total', 'grossTotalAmount', true ],
						[ 'Discount', 'discount', true ],
						[ 'Total', 'totalBillAmount', true ],
						[ 'Narration', 'description', true ]];
						
						
					
					vm.LedgerRegisterAvaliableList = [
						[ 'Credit', 'Credit', false ],
						[ 'Debit', 'Debit', false ] ];
				
					
					
					vm.DebitnoteRegisterDisplayList = [
						[ 'Billink id', 'debitNoteId'],
						[ 'Debit Date', 'debitNoteDate'],
					/*	[ 'Comapny Name', 'comapnyName' ],*/
						[ 'Ledger Name', 'ledgerName' ],
						[ 'billNumber', 'billNumber'],
						[ 'Naration', 'description'],
						[ 'Ref No 1', 'referenceNumber1'],
						[ 'Ref No 2', 'referenceNumber2'],
						[ 'Original Invoice No.', 'originalInvoiceNumber'],
						[ 'Original Invoice Date', 'originalInvoiceDate'],
						[ 'Item Name', 'itemName'],
						[ 'SKU', 'sku'],
						[ 'EAN', 'eanNo'],
						[ 'HSN', 'hsnCode'],
						[ 'LocationName', 'locationName'],
						[ 'Batch No', 'batchNo'],
						[ 'Mfg. Date', 'mfgDate'],
						[ 'Shelf life', 'shelfLife'],
						[ 'Exp. Date', 'expDate' ],
						[ 'Qty', 'item_quantity'],
						[ 'Rate', 'item_purchaserate'],
						/*[ 'Item Tax Rate', 'item_taxRate'],*/
						/*[ 'Item Amount', 'item_amount'],*/
						[ 'Discount', 'itemDiscount'],
						[ 'Additional Discount', 'additionaldiscount'],
						[ 'IGST', 'item_iGST'],
						[ 'SGST', 'item_sGST'],
						[ 'CGST', 'item_cGST'],
						[ 'Taxable Amount', 'item_total_amount'],
						
						
						[ 'Charge LedgerName', 'chargeledgerName'],
						[ 'SAC', 'sacCode'],
						[ 'RATE', 'charge_sale'],
						[ 'TaxRate', 'charge_taxRate'],
						[ 'Discount', 'chargeDiscount'],
						[ 'IGST', 'charge_iGST'],
						[ 'SGST', 'charge_sGST'],
						[ 'CGST', 'charge_cGST'],
						/*[ 'Discount', 'discount'],*/
						[ 'Taxable Amount', 'chargeAmount'],
						[ 'Total Qty', 'item_quantity'],
						[ 'Total Taxable Value', 'total_taxable_amt'],
						[ 'IGST', 'total_igst'],
						[ 'CGST', 'total_cgst'],
						[ 'SGST', 'total_sgst'],
						[ 'Gross Total', 'grossTotalAmount'],
						[ 'Discount', 'discount'],
						[ 'Total', 'totalBillAmount'],
						[ 'Narration', 'description']];
					
					vm.CreditnoteRegisterDisplayList = [
						[ 'Billink id', 'creditNoteId'],
						[ 'Credit Date', 'creditNoteDate'],
						/*[ 'Comapny Name', 'comapnyName' ],*/
						[ 'Ledger Name', 'ledgerName' ],
						[ 'Creadit No.', 'billNumber'],
						/*[ 'Naration', 'description'],*/
						[ 'Ref No 1', 'referenceNumber1'],
						[ 'Ref No 2', 'referenceNumber2'],
						[ 'Original Invoice No.', 'billNumber'],
						[ 'Original Invoice Date', 'invoiceDate'],
						
						
						[ 'Item Name', 'itemName'],
						[ 'EAN', 'eanNo'],
						[ 'HSN', 'hsnCode'],
						[ 'LocationName', 'locationName'],
						[ 'Batch No', 'batchNo'],
						[ 'Mfg. Date', 'mfgDate'],
						[ 'Shelf life', 'shelfLife'],
						[ 'Exp. Date', 'expDate'],
						[ 'Qty', 'item_quantity'],
						[ 'Rate', 'item_saleRate'],
						[ 'Discount', 'itemDiscount'],
						[ 'Additional Discount', 'additionaldiscount'],
						[ 'FreeItem Qty', 'freequantity'],
						[ 'Tax Rate', 'item_taxRate'],
						[ 'IGST', 'item_iGST'],
						[ 'SGST', 'item_sGST'],
						[ 'CGST', 'item_cGST'],
						[ 'Taxable Amount', 'item_total_amount'],
					
						
						[ 'Charge LedgerName', 'chargeledgerName'],
						[ 'SAC', 'sacCode'],
						[ 'RATE', 'charge_sale'],
						[ 'Discount', 'chargeDiscount'],
						[ 'TaxRate', 'charge_taxRate'],
						[ 'IGST', 'charge_iGST'],
						[ 'SGST', 'charge_sGST'],
						[ 'CGST', 'charge_cGST'],
						[ 'Discount', 'discount'],
						[ 'Total Charge Amount', 'chargeAmount'],
						
						
						[ 'Total Qty', 'item_quantity'],
						[ 'Total Taxable Value', 'total_taxable_amt'],
						[ 'IGST', 'total_igst'],
						[ 'CGST', 'total_cgst'],
						[ 'SGST', 'total_sgst'],
						[ 'Gross Total', 'grossTotalAmount'],
						[ 'Discount', 'discount'],
						[ 'Total', 'totalBillAmount'],
						[ 'Narration', 'description'] ];
					
					vm.ledgerRegisterDisplayList  = [
						[ 'Billink ID ', 'noteId'],
						[ 'Opening Balance ', 'openingBalance'],
						[ 'Debit Sales ', 'saleGrantAmt'],
						[ 'DebitNote Amount ', 'amount'],
						[ 'Sale Receipt Amount ', 'saleReceiptAmt'],
						[ 'Credit Purchase ', 'purchaseGrantAmt'],
						[ 'CreditNote Amount ', 'amount'],
						[ 'Purchase Amount ', 'purchasePaymentAmt'],
						[ 'BillNumber', 'billNumber'],
						[ 'Purchase Ref1.', 'referenceNumber1'],
						[ 'Sale Ref1.', 'referenceNumber2'],
						[ 'Original Invoice No.', 'originalInvoiceNo'],
						[ 'Purchase Payment.', 'purchaseInvoiceNo'],
						[ 'Sale Receipt No.', 'saleReceiptNo'],
						[ 'Narration.', 'description']];
						
						
//						[ 'Note Id ', 'noteId'],
//						[ 'Note Date', 'noteDate'],
//						[ 'Ledger Name', 'ledgerName' ],
//						[ 'billNumber', 'billNumber'],
//						[ 'Naration', 'description'],
//						[ 'Ref No 1', 'referenceNumber1'],
//						[ 'Ref No 2', 'referenceNumber2'],
//						[ 'Amount', 'amount'] ];
					
				vm.dayBookDisplayList = [
					
					[ 'Billink ID ', 'noteId'],
					[ 'Purchase Total ', 'purchaseGrantAmount'],
					[ 'Sale Total ', 'sellGrantAmount'],
					[ 'Sale Total ', 'sellGrantAmount'],
					[ 'Debit Amount ', 'amount'],
					[ 'Creadit Amount ', 'amount'],
					[ 'Creadit Amount ', 'amount'],
					[ 'Purchase Payment ', 'purchasePayment'],
					[ 'Sales Receipt ', 'saleReceipt'],
					[ 'Expense ', 'expense'],
					[ 'Bill No.', 'billNumber'],
					[ 'Purchase Ref1', 'referenceNumber1'],
					[ 'Sale Ref2', 'referenceNumber2'],
					[ 'Original Invoice No.', 'originalInvoiceNumber'],
					[ 'Original Invoice No.', 'originalInvoiceNumber'],
					[ 'Payment Invoice No.', 'invoiceNoForRef'],
					[ 'Sale Recepit No.', 'saleReceiptForRef'],
					[ 'Narration.', 'description'],
						];
					
					
//						[ 'Note Id ', 'noteId'],
//						[ 'Note Date', 'noteDate'],
//						[ 'Ledger Name', 'ledgerName' ],
//						[ 'billNumber', 'billNumber'],
//						[ 'Naration', 'description'],
//						[ 'Ref No 1', 'referenceNumber1'],
//						[ 'Ref No 2', 'referenceNumber2'],
//						[ 'Amount', 'amount'] ];
				
				
			vm.taxableledgerReport = [
					
					[ 'Billink ID ', 'noteId'],
					[ 'Openning Balance ', 'amount'],
					[ 'IGST ', 'igst'],
					[ 'CGST ', 'cgst'],
					[ 'SGST ', 'sgst'],
					[ 'Bill No. ', 'billNumber'],
					[ 'Purchase Ref1 ', 'referenceNumber1'],
					[ 'sale Ref2 ', 'referenceNumber2'],
					[ 'Narration ', 'description'],
						];
				
					
				vm.stockDisplayList = [
					
					[ 'ItemName', 'itemName'],
					[ 'Quantity', 'stock'],
					[ 'Rate', 'purchaseRate'],
					[ 'Amount', '(stock*purchaseRate)'],
					[ 'Quantity', 'qty'],
					[ 'Rate', 'purchaseRate'],
					[ 'Amount', '(qty*purchaseRate)'],
					[ 'InWard Date', 'inwarDate'],
					[ 'Quantity', '(inqty-qty)'],
					[ 'Rate', 'purchaseRate'],
					[ 'Amount', '((inqty-qty)*purchaseRate)'],
					[ 'Outward Date', 'outWardDate'],
					[ 'Quantity', 'outqty'],
					[ 'Rate', 'purchaseRate'],
					[ 'Amount', '(outqty*purchaseRate)'],
					[ 'Quantity', 'stock'],
					[ 'Rate', 'purchaseRate'],
					[ 'Amount', '(stock*purchaseRate)']

				];
//					[ 'Stock Id', 'stockId'],
//					[ 'Date', 'date'],
//					[ 'Amount', 'amount' ],
//					[ 'Item Quantity', 'item_quantity'],
//					[ 'Rate', 'rate'],
//					[ 'Name', 'name']];
					
					vm.SaleRegisterDisplayList = [
						[ 'Billink id', 'sellId'],
						[ 'Sale Date', 'sellBillDate'],
						/*[ 'Comapny Name', 'comapnyName' ],*/
						[ 'Ledger Name', 'ledgerName' ],
						[ 'Bill Bo.', 'billNumber'],
						/*[ 'Naration', 'description'],*/
						[ 'Ref No 1', 'referenceNumber1'],
						[ 'Ref No 2', 'referenceNumber2'],
						[ 'Credit Period', 'creditPeriod'],
						[ 'GSTN', 'gstNumber'],
						
						[ 'Item Name', 'itemName'],
						[ 'SKU', 'sku'],
						[ 'EAN', 'eanNo'],
						[ 'HSN', 'hsnCode'],
						[ 'LocationName', 'locationName'],
						[ 'Batch No', 'batchNo'],
						[ 'Mfg. Date', 'mfgDate'],
						[ 'Shelf life', 'shelfLife'],
						[ 'Exp. Date', 'expDate' ],
						[ 'Qty', 'item_quantity'],
						[ 'Rate', 'item_saleRate'],
						[ 'Discount', 'itemDiscount'],
						[ 'Additional Discount', 'additionalitemDiscount'],
						[ 'Free Qty', 'freeItemQty'],
						[ 'Tax Rate', 'item_taxRate'],
						/*[ 'Item Amount', 'item_amount'],*/
						[ 'IGST', 'item_iGST'],
						[ 'SGST', 'item_sGST'],
						[ 'CGST', 'item_cGST'],
						[ 'Taxable Amount', 'item_total_amount'],
						
						
						[ 'Charge LedgerName', 'chargeledgerName'],
						[ 'SAC', 'sacCode'],
						[ 'RATE', 'charge_sale'],
						[ 'TaxRate', 'charge_taxRate'],
						[ 'Discount', 'chargeDiscount'],
						[ 'IGST', 'charge_iGST'],
						[ 'SGST', 'charge_sGST'],
						[ 'CGST', 'charge_cGST'],
						/*[ 'Discount', 'discount'],*/
						[ 'Taxable Amount', 'chargeAmount'],
						[ 'Total Qty', 'item_quantity'],
						[ 'Total Taxable Value', 'total_taxable_amt'],
						[ 'IGST', 'total_igst'],
						[ 'CGST', 'total_cgst'],
						[ 'SGST', 'total_sgst'],
						[ 'Gross Total', 'grossTotalAmount'],
						[ 'Discount', 'discount'],
						[ 'Total', 'totalBillAmount'],
						[ 'Narration', 'description']];
						
					
					vm.PurchaseRegisterDisplayList = [
						[ 'Billink id', 'purchaseId'],
						[ 'Invoice Date', 'billDate'],
						[ 'Purchase Date', 'purchaseDate'],
						
						/*[ 'Comapny Name', 'comapnyName' ],*/
						[ 'Ledger Name', 'ledgerName' ],
						[ 'billNumber', 'billNumber'],
						/*[ 'Naration', 'description'],*/
						[ 'Ref No 1', 'referenceNumber1'],
						[ 'Ref No 2', 'referenceNumber2'],
						[ 'Credit Period', 'creditPeriod'],
						[ 'Item Name', 'itemName'],
						[ 'SKU', 'sku'],
						[ 'EAN', 'eanNo'],
						[ 'HSN', 'hsnCode'],
						[ 'LocationName', 'locationName'],
						[ 'Batch No', 'batchNo'],
						[ 'Mfg. Date', 'mfgDate'],
						[ 'Shelf life', 'shelfLife'],
						[ 'Exp. Date', 'expDate' ],
						[ 'Qty', 'item_quantity'],
						[ 'Rate', 'item_purchaseRate'],
						/*[ 'Item Qty', 'item_quantity'],
						[ 'Item Amount', 'item_amount'],*/
						[ 'Discount', 'itemDiscount'],
						[ 'Additional Discount', 'additionalitemDiscount'],
						[ 'Tax Rate', 'item_taxRate'],
						[ ' IGST', 'item_iGST'],
						[ ' SGST', 'item_sGST'],
						[ ' CGST', 'item_cGST'],
						[ 'Total Amount', 'item_total_amount'],
						
						[ 'Charge LedgerName', 'chargeledgerName'],
						[ 'SAC', 'sacCode'],
						[ 'Rate', 'charge_purchase'],
						[ 'Discount', 'chargeDiscount'],
						[ 'TaxableAmount', 'charge_item_amount'],
						[ 'TaxRate', 'charge_taxRate'],
						
						[ 'IGST', 'charge_iGST'],
						[ 'SGST', 'charge_sGST'],
						[ 'CGST', 'charge_cGST'],
						
						[ 'Total Qty', 'item_quantity'],
						[ 'Total Taxable Value', 'total_taxable_amt'],
						[ 'IGST', 'total_igst'],
						[ 'CGST', 'total_cgst'],
						[ 'SGST', 'total_sgst'],
						[ 'Gross Total', 'grossTotalAmount'],
						[ 'Discount', 'discount'],
						[ 'Total', 'totalBillAmount'],
						[ 'Narration', 'description']];
//						[ 'Charge Purchase', 'charge_purchase'],
//						[ 'Charge TaxRate', 'charge_taxRate'],
//						[ 'Charge Discount', 'chargeDiscount'],
//						[ 'Charge IGST', 'charge_iGST'],
//						[ 'Charge SGST', 'charge_sGST'],
//						[ 'Charge CGST', 'charge_cGST'],
//						[ 'Discount', 'discount'],
//						[ 'Total Bill Amount', 'totalBillAmount'] ];

					vm.PurchaseRegisterList = [];
					vm.SaleRegisterList = [];
					vm.creditnoteRegisterList = [];
					vm.DebitnoteRegisterList = [];
					vm.allLedgerList = [];
					vm.taxAbleLedgerList = [];
					vm.dayBookList = [];
					vm.stockList =[];
					//vm.companyid
					vm.getLedgerNameList = [];
					
					vm.flag = 0;
					
					
					
					vm.getLedgerData = function() {
						$http.get(
								'../../report/getAllLedgerName').then(
								successLedgerdataCallback, errorCallback);
					};
					function successLedgerdataCallback(response){
						//console.log(response);
						vm.getLedgerNameList =  response.data.result;
					//	console.log("array :"+vm.getLedgerNameList);
						
					}
					vm.getInit = function(fromDate, toDate) {
						$http.get(
								'../../report/purchase?startDate=' + fromDate
										+ '&endDate=' + toDate).then(
								successCallback, errorCallback);
					};

					vm.getsales = function(fromDate, toDate) {
						$http.get(
								'../../report/sale?startDate=' + fromDate
										+ '&endDate=' + toDate).then(
								successsellCallback, errorsellCallback);
					};
					
					vm.getCreditnote = function(fromDate, toDate) {
						$http.get(
								'../../report/credit?startDate=' + fromDate
										+ '&endDate=' + toDate).then(
								successCreditCallback, errorCreditCallback);
					};
					
					vm.getDebitnote = function(fromDate, toDate) {
						$http.get(
								'../../report/debit?startDate=' + fromDate
										+ '&endDate=' + toDate).then(
								successDebitCallback, errorDebitCallback);
					};
					
					vm.getCreditDebitLedger = function(fromDate, toDate,ledgerId) {
						$http.get(
								'../../report/getledgercreditdebitdetail?startDate=' + fromDate
										+ '&endDate=' + toDate +'&ledgerId='+ledgerId).then(
								successCreditLedgerCallback, errorCreditLedgerCallback);
					};
					
					vm.gettaxableLedger = function(fromDate, toDate,ledgerId) {
						$http.get(
								'../../report/gettaxablereport?startDate=' + fromDate
										+ '&endDate=' + toDate +'&ledgerId='+ledgerId).then(
												successtaxAbleLedgerCallback, errorCreditLedgerCallback);
					};
				
//					vm.getDebitLedger = function(fromDate, toDate) {
//						$http.get(
//								'../../report/alldebitledger?startDate=' + fromDate
//										+ '&endDate=' + toDate).then(
//							successCreditLedgerCallback, errorCreditLedgerCallback);
//					};
					
//					vm.getdebitToday = function() {
//						$http.get(
//								'../../report/alldebitledger/today').then(
//							successCreditLedgerCallback, errorCreditLedgerCallback);
//					};
//					
//					vm.getcreditToday = function() {
//						$http.get(
//								'../../report/allcreditledger/today').then(
//							successCreditLedgerCallback, errorCreditLedgerCallback);
//					};
					
					
					vm.currentDayBook = function(ledgerId){
						$http.get(
						'../../report/gettodaycreditdebitdetail?ledgerId='+ledgerId		
						).then(
					successDaybookCallback, errorDayBookCallback);
					}
					
					vm.stockDetails = function(fromDate, toDate,companyId){
					
						$http.get(
								'../../report/getstockdetails?startDate=' + fromDate
										+ '&endDate=' + toDate+'&companyId='+companyId).then(
												successStockCallback, errorStockCallback);
					}
					function successStockCallback(response){
						console.log("response=======>>>>"+response.data.result);
						vm.stockList = response.data.result;
					}
					function errorStockCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
					function successDaybookCallback(response){
						vm.dayBookList = response.data.result;
					}
					function errorDayBookCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
					function successCallback(response) {
						vm.PurchaseRegisterList = response.data.result;
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					function successsellCallback(response){
						vm.SaleRegisterList = response.data.result;
					}
					
					function errorsellCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					function successCreditCallback(response){
						vm.creditnoteRegisterList = response.data.result;
					}
					
					function errorCreditCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					function successDebitCallback(response){
						vm.DebitnoteRegisterList = response.data.result;
					}
					function errorDebitCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					function successCreditLedgerCallback(response){
						console.log("response :"+response.data.result);
						vm.allLedgerList = response.data.result;
					}

					function successtaxAbleLedgerCallback(response){
						console.log("response :"+response.data.result);
						vm.taxAbleLedgerList = response.data.result;
					}
					
					function errorCreditLedgerCallback(){
						toaster.showMessage('error', '', response.data.errMsg);
					}
				})
		.controller(
				'manageReportController',
				function($http, $ocLazyLoad, manageReportService,
						billInkDashboardService) {
					var vm = this;

					vm.maxDate = moment().format('YYYY-MM-DD');
					let date = moment().format('MM/DD/YYYY');
					vm.toDate = new Date(date);
					vm.fromDate = new Date(date);
					vm.manageReportService = manageReportService;
					vm.billInkDashboardService = billInkDashboardService;
					vm.companyId = 0 ;
					vm.ledgerList = [];
					
					//vm.companyId = "";
					vm.init = function() {
						vm.flag = 0;
						vm.manageReportService.getLedgerData();
						vm.manageReportService.getInit(formatDate(vm.fromDate),
								formatDate(vm.toDate));
						//vm.companyId = "";
						//vm.manageReportService.companyId = vm.billInkDashboardService.companyId;
						
						vm.companyId = vm.billInkDashboardService.companyId;
						//vm.companyId = vm.billInkDashboardService.companyId;
						//console.log("Company Id :"+vm.billInkDashboardService.companyId);
						//vm.billInkDashboardService.currentCompanyObj = companyObj;
						//vm.manageReportService.selectedCompanyId = companyObj.companyId;
					};

					vm.openPurchaseList = function(companyObj) {
						
						vm.billInkDashboardService.currentCompanyObj = companyObj;
						vm.manageReportService.selectedCompanyId = companyObj.companyId;
						//vm.manageReportService.startOfPurchase = 0;
						//vm.billInkDashboardService.currentViewUrl = "../purchase/view/viewPurchaseList.html";
						//vm.managePurchaseDataService.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
					};
					
				
					vm.resetreportdetail = function(){
						$(".purchasereportdiv").hide();
						$(".salereportdiv").hide();
						$(".creditnotereportdiv").hide();
						$(".debitnotereportdiv").hide();
						$(".taxablereportdiv").hide();
						$("#PurchaseId").removeClass("active");
						$("#SaleId").removeClass("active");
						$("#CreditnoteId").removeClass("active");
						$("#DebitnoteId").removeClass("active");
						$("#LedgerreportId").removeClass("active");
						$("#DaybookId").removeClass("active");
						$("#bankId").removeClass("active");
						$("#TaxesledgerId").removeClass("active");
						$("#stockreportId").removeClass("active");
					}
					
					
					vm.change = function(val){
						
						vm.flag = val;
						if(val == 0){
							vm.resetreportdetail();
							$(".purchasereportdiv").show();
							$("#PurchaseId").addClass("active");
						}else if(val == 1){
							vm.resetreportdetail();
							$("#SaleId").addClass("active");
						} else if(val == 2){
							vm.resetreportdetail();
							$("#CreditnoteId").addClass("active");
						}else if(val == 3){
							vm.resetreportdetail();
							$("#DebitnoteId").addClass("active");
						}else if(val == 4){
							vm.resetreportdetail();
							console.log("inside ledger");
							$("#LedgerreportId").addClass("active");
						}else if(val == 5){
							vm.resetreportdetail();
							$("#DaybookId").addClass("active");
						}else if(val == 6){
							vm.resetreportdetail();
							$("#bankId").addClass("active");
						}else if(val == 7){
							vm.resetreportdetail();
							$("#TaxesledgerId").addClass("active");
						}else if(val == 8){
							vm.resetreportdetail();
							$("#stockreportId").addClass("active");
						}
					}
					vm.init1 = function() {
						vm.manageReportService.getInit();
					};

					vm.checkDate = function() {
						var fd = new Date(vm.fromDate).getTime();
						var td = new Date(vm.toDate).getTime();
						if (fd > td) {
							vm.toDate = vm.fromDate;
							document.getElementById("toDate").value = vm.fromDate;
						}
						var dt = formatDate(vm.fromDate);
						document.getElementById("toDate").min = dt;
						vm.manageReportService.getInit(formatDate(vm.fromDate),
								formatDate(vm.toDate));
					};

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					vm.$onDestroy = function() {
						vm.PurchaseRegisterList = [];
					}

					vm.PurchaseRegisterDetail = function() {
						vm.manageReportService.getInit(formatDate(vm.fromDate),formatDate(vm.toDate));
						$("#PurchaseRegister-dialog .modal").modal('show');
					}

					vm.PurchaseRegistercloseModal = function() {
						$("#PurchaseRegister-dialog .modal").modal('hide');
					}
					
					vm.PurchaseRegisterCheckBoxChange= function(val){
						vm.manageReportService.PurchaseRegisterAvaliableList[val][2] = document.getElementById(val).checked;
					}

					vm.PurchaseRegistercloseModalapplyFilter = function() {
						vm.manageReportService.PurchaseRegisterDisplayList = [];
						for(let i=0;i<vm.manageReportService.PurchaseRegisterAvaliableList.length;i++){
							if(vm.manageReportService.PurchaseRegisterAvaliableList[i][2])
								vm.manageReportService.PurchaseRegisterDisplayList.push(vm.manageReportService.PurchaseRegisterAvaliableList[i]);
						}
						$("#PurchaseRegister-dialog .modal").modal('hide');
						$("#tbl_purchase_Details").show();
					}
					
					
					
					// for sales 

					vm.saleRegisterDetail = function(){
						vm.manageReportService.getsales(formatDate(vm.fromDate),formatDate(vm.toDate));
						$("#SaleRegister-dialog .modal").modal('show');
					}
					
					vm.saleRegistercloseModal = function(){
						$("#SaleRegister-dialog .modal").modal('hide');
					}


					vm.SaleRegisterCheckBoxChange= function(val){
						vm.manageReportService.saleRegisterAvaliableList[val][2] = document.getElementById(val).checked;
					}
					
					vm.SaleRegistercloseModalapplyFilter = function() {
						vm.manageReportService.SaleRegisterDisplayList = [];
						for(let i=0;i<vm.manageReportService.saleRegisterAvaliableList.length;i++){
							if(vm.manageReportService.saleRegisterAvaliableList[i][2])
								vm.manageReportService.SaleRegisterDisplayList.push(vm.manageReportService.saleRegisterAvaliableList[i]);
						}
						$("#SaleRegister-dialog .modal").modal('hide');
						$("#tbl_sale_Details").show();
					}
					
					
					// for credit note 
					
					vm.creditnoteRegisterDetail = function(){
						vm.manageReportService.getCreditnote(formatDate(vm.fromDate),formatDate(vm.toDate));
						$("#creditnoteRegister-dialog .modal").modal('show');
					}
					
					vm.creditnoteRegistercloseModal = function(){
						$("#creditnoteRegister-dialog .modal").modal('hide');
					}
					
					vm.creditnoteRegisterCheckBoxChange= function(val){
						vm.manageReportService.creditnoteRegisterAvaliableList[val][2] = document.getElementById(val).checked;
					}
					
					vm.creditnoteRegisterapplyFilter = function() {
						vm.manageReportService.CreditnoteRegisterDisplayList = [];
						for(let i=0;i<vm.manageReportService.creditnoteRegisterAvaliableList.length;i++){
							if(vm.manageReportService.creditnoteRegisterAvaliableList[i][2])
								vm.manageReportService.CreditnoteRegisterDisplayList.push(vm.manageReportService.creditnoteRegisterAvaliableList[i]);
						}
						$("#creditnoteRegister-dialog .modal").modal('hide');
						$("#tbl_creditnote_Details").show();
					}
					
					// for debit note 
					
					vm.debitnoteRegisterDetail = function(){
						vm.manageReportService.getDebitnote(formatDate(vm.fromDate),formatDate(vm.toDate));
						$("#debitnoteRegister-dialog .modal").modal('show');
					}
					
					vm.debitnoteRegistercloseModal = function(){
						$("#debitnoteRegister-dialog .modal").modal('hide');
					}
					
					vm.debitnoteRegisterCheckBoxChange= function(val){
						vm.manageReportService.debitnoteRegisterAvaliableList[val][2] = document.getElementById(val).checked;
					}
					
					vm.debitnoteRegisterapplyFilter = function() {
						vm.manageReportService.DebitnoteRegisterDisplayList = [];
						for(let i=0;i<vm.manageReportService.debitnoteRegisterAvaliableList.length;i++){
							if(vm.manageReportService.debitnoteRegisterAvaliableList[i][2])
								vm.manageReportService.DebitnoteRegisterDisplayList.push(vm.manageReportService.debitnoteRegisterAvaliableList[i]);
						}
						$("#debitnoteRegister-dialog .modal").modal('hide');
						$("#tbl_debitnotes").show();
					}
				
					// ledger 
					
					vm.ledgerRegisterDetail  = function(){
						$("#ledgerRegister-dialog .modal").modal('show');
						 vm.ledgerId = $( "#creditdebitLedgerId option:selected" ).val();
							vm.manageReportService.getCreditDebitLedger(formatDate(vm.fromDate),formatDate(vm.toDate),vm.ledgerId);
							$("#tbl_ledgerDetail").show();
					}
					
					vm.ledgerRegistercloseModal = function(){
						$("#ledgerRegister-dialog .modal").modal('hide');
					}
	
				    //  Day book 
					
					vm.ledgerDetailsListBycompanyId = function(){
						$http
						.post(
								'../../ledgerData/getLedgerListDataForReport',
								{
									companyId : vm.billInkDashboardService.companyId
								}).then(function(response) {
									vm.ledgerList = response.data.data;
								}, vm.errorFetchData);
						
					}
					
					vm.getDayBookDetail = function(){
						//vm.currentDayBook = ()
						 vm.ledgerId = $( "#daybookLedgerId option:selected" ).val();
						// console.log("id :"+vm.ledgerId);
						vm.manageReportService.currentDayBook(vm.ledgerId);
						$("#tbl_Daybook_Details").show();
					}
				    
					//taxable ledger
					
					vm.taxableledgerDetail  = function(){
						$("#ledgerRegister-dialog .modal").modal('show');
						 vm.ledgerId = $( "#taxableLedgerId option:selected" ).val();
							vm.manageReportService.gettaxableLedger(formatDate(vm.fromDate),formatDate(vm.toDate),vm.ledgerId);
							$("#tbl_taxableledgerDetail").show();
					}
					
					// stock 
					
					vm.StockRegisterDetail = function(){
						//console.log("fun com :"+vm.companyId);
						vm.manageReportService.stockDetails(formatDate(vm.fromDate),formatDate(vm.toDate),vm.companyId);
						$("#tbl_Stock_Details").show();
					}
				    
					// serach 
					
					
					vm.searchPurchaseDetail = function(){
						var inputValue = vm.searchPurchaseDetailText;
						$("#tbl_purchase_Details  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
						});
						$("#purchaseheaderId").show();
					}
					
					vm.searchSaleDetail = function(){
						var inputValue = vm.searchSaleDetailText;
						$("#tbl_sale_Details  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
								});
						$("#saleheaderId").show();
					}
					
					vm.searchCreditnoteDetail = function(){
						var inputValue = vm.searchCreditnoteDetailText;
						$("#tbl_creditnote_Details  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
								});
						$("#creditnoteheaderId").show();
					}
					
					vm.searchledgerDetail = function(){
						var inputValue = vm.searchledgerDetailText;
						$("#tbl_ledgerDetail  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
								});
						$("#ledgerheaderId").show();
					}
					
					vm.searchdayBookDetail = function(){
						var inputValue = vm.searchDaybookDetailText;
						$("#tbl_Daybook_Details  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
								});
						$("#daybookheaderId").show();
					}
					
					vm.searchStockDetail = function(){
						var inputValue = vm.searchStockDetailText;
						$("#tbl_Stock_Details  tr").filter(
								function() {
									$(this).toggle(
											$(this).text().toLowerCase().indexOf(
													inputValue) > -1)
								});
						$("#stockheaderId").show();
					}
					
					
				});



