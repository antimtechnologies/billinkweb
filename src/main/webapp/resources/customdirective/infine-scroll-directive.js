var infiniteScrollApp = angular.module('infiniteScroll', []);

infiniteScrollApp.directive('infineScrollDirective', function() {
	return {
	  	restrict: 'A',
	  	scope: {
			loadMoreData: '&'
		},
    	link: function(scope, element, attr){
    		element.on('scroll', function (e) {
    			if ( $(e.currentTarget).scrollTop() + $(e.currentTarget).innerHeight() >= ( $(e.currentTarget)[0].scrollHeight - 200 ) ) {
    	            scope.loadMoreData();
    	        }
    		});
    	}
	};
});