	angular
		.module('addCreditNoteData', [ 'fileUploadDirective', 'toaster' ])
		.controller(
				'addCreditNoteDataController',
				function($http, fileUploadService, toaster,
						manageCreditNoteDataService, billInkDashboardService) {
					var vm = this;
					vm.manageCreditNoteDataService = manageCreditNoteDataService;
					vm.profilePhotoFile = null;
					vm.stateList = [];
					vm.creditNote = {};
					vm.creditNote.imageURLList = [];
					vm.numberOfAddedItem = 1;
					vm.creditNote.creditNoteItemMappingList = [];
					vm.creditNote.creditNoteItemMappingList[vm.numberOfAddedItem - 1] = {};
					vm.numberOfItem = new Array(vm.numberOfAddedItem);
					vm.creditNote.creditNoteSellMappingList = [];
					var saleId = "";
					vm.numberOfAddedCreditNoteCharge = 1;
					vm.numberOfCreditNoteCharge = new Array(
							vm.numberOfAddedCreditNoteCharge);
					vm.creditNote.creditNoteChargeModelList = [];
					vm.creditNote.creditNoteChargeModelList[vm.numberOfAddedCreditNoteCharge - 1] = {};
					
					vm.billInkDashboardService = billInkDashboardService;
					vm.taxCodeList = [];

					vm.locationList = [];

					vm.calculateIGST = false;
					vm.calculateSGST = false;
					vm.calculateCGST = false;

					vm.init = function() {
						$http.post('../../taxcodedata/getTaxCodeValueList', {})
								.then(vm.setTaxCodeList, vm.errorFetchData);
						$http
								.post(
										'../../companydata/getCompanyDetails',
										{
											companyId : vm.manageCreditNoteDataService.selectedCompanyId
										}).then(vm.setCompanyDetail,
										vm.errorFetchData);
						vm.getLocationList();
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						if (vm.manageCreditNoteDataService.requestType === "Edit") {
							$http
									.post(
											'../../creditNoteData/getCreditNoteDetailsById',
											{
												creditNoteId : vm.manageCreditNoteDataService.editCreditNoteId,
												companyId : vm.manageCreditNoteDataService.selectedCompanyId
											}).then(vm.setCreditNoteDetail,
											vm.errorFetchData);
						} else {
							vm.creditNote.addedBy = {};
							vm.creditNote.addedBy.userId = vm.billInkDashboardService.userId;
							$http
									.post(
											'../../companydata/getBillPrefixAndLastNumber',
											{
												companyId : vm.manageCreditNoteDataService.selectedCompanyId
											})
									.then(
											function(response) {
												vm.creditNote.billNumberPrefix = response.data.data.billNumberPrefix;
												vm.creditNote.currentBillNumber = response.data.data.lastCreditNoteNumber + 1;
											}, vm.errorFetchData);
						}

						$http
								.post(
										'../../companydata/getCompanyDetails',
										{
											companyId : vm.manageCreditNoteDataService.selectedCompanyId
										}).then(vm.setCompanyDetail,
										vm.errorFetchData);

						$http
								.get(
										'../../locationData/findbycompanyId?companyId='
												+ vm.manageCreditNoteDataService.selectedCompanyId)
								.then(function(response) {
									vm.locationList = response.data.result;
								}, vm.errorFetchData);

					};
					vm.setCompanyDetail = function(response) {
						vm.companyDataForSell = angular
								.copy(response.data.data);
						vm.selectedStateForSell = vm.companyDataForSell.state.stateName;
					};
					vm.setTaxCodeList = function(response) {
						vm.taxCodeList = response.data.data;
					};

					vm.addLedgerData = function() {
						billInkDashboardService
								.loadMenuPage({

									iconImgURL : "../images/manage_ledger.png",
									menuName : "Manage Ledger",
									url : "../ledger/view/manageLedger.html",
									js : [
											'../customdirective/infine-scroll-directive.js',
											'../ledger/js/manageLedger.js' ]

								});

					};

					vm.getOneBatch = function(index) {
						console.log("getOneFucntion call");
						console.log("Fucntion Index:" + index);
						console
								.log("BatchId :"
										+ vm.creditNote.creditNoteItemMappingList[index].batchno);
						if (vm.creditNote.creditNoteItemMappingList[index].batchId
								&& vm.creditNote.creditNoteItemMappingList[index].batchId != undefined) {
							console
									.log("If batchID :"
											+ vm.creditNote.creditNoteItemMappingList[index].batchId);
							$http
									.post(
											'../../itemData/getoneBatch',
											{
												batchId : vm.creditNote.creditNoteItemMappingList[index].batchId
											})
									.then(
											function(res) {
												var dt = formatDateForSale(res.data.data.mfgDate);
												var pt = formatDateForSale(res.data.data.expDate);

												var dtp = formatDate(res.data.data.mfgDate);
												var ptp = formatDate(res.data.data.expDate);

												vm.creditNote.creditNoteItemMappingList[index].mfgdate = dt;
												vm.creditNote.creditNoteItemMappingList[index].expdate = pt;
												// vm.sell.sellItemModel[index].batchId
												// = res.data.data.batchId;
												vm.creditNote.creditNoteItemMappingList[index].mfgdateForPrint = dtp;
												vm.creditNote.creditNoteItemMappingList[index].expdateForPrint = ptp;

											}, errorCallback);

						}
					}

					vm.getBatchnolist = function(index) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : vm.creditNote.creditNoteItemMappingList[index].item.itemId
										})
								.then(
										function(batchres) {
											vm.creditNote.creditNoteItemMappingList[index].batchNoList = batchres.data.data;
											vm.creditNote.creditNoteItemMappingList[index].batchId = batchres.data.data.batchNo;

										}, errorCallback);

					}

					vm.getLocationList = function() {
						$http
								.get(
										'../../locationData/findbycompanyId?companyId='
												+ vm.creditNote.companyId)
								.then(
										function(response) {
											vm.locationList = response.data.result;
										}, vm.errorFetchData);

					};

					vm.addCreditNoteCharge = function() {
						vm.numberOfAddedCreditNoteCharge += 1;
						vm.creditNote.creditNoteChargeModelList[vm.numberOfAddedCreditNoteCharge - 1] = {}
						vm.numberOfCreditNoteCharge = new Array(
								vm.numberOfAddedCreditNoteCharge);
					};

					vm.deleteCreditNoteCharge = function(index) {
						if (vm.numberOfAddedCreditNoteCharge === 1) {
							return;
						}
						vm.numberOfAddedCreditNoteCharge -= 1;
						vm.creditNote.creditNoteChargeModelLists
								.splice(index, 1);
						vm.numberOfCreditNoteCharge = new Array(
								vm.numberOfAddedCreditNoteCharge);
					};

					vm.AddItem = function() {
						vm.numberOfAddedItem += 1;
						vm.creditNote.creditNoteItemMappingList[vm.numberOfAddedItem - 1] = {}
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.deleteCreditNoteItem = function(index) {
						vm.numberOfAddedItem -= 1;
						vm.creditNote.creditNoteItemMappingList
								.splice(index, 1);
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.setCompanyDetail = function(response) {
						vm.companyDataForCreaditNotes = angular
								.copy(response.data.data);

					};

					vm.setCreditNoteDetail = function(response) {
						vm.creditNote = angular.copy(response.data.data);
						vm.searchLedgerNameText = vm.creditNote.ledgerData.ledgerName;
						vm.creditNote.creditNoteDateObj = new Date(
								vm.creditNote.creditNoteDate);
						vm.creditNote.modifiedBy = {};
						vm.creditNote.modifiedBy.userId = vm.billInkDashboardService.userId;
						var dt = formatDateForSale(vm.creditNote.originalInvoiceDate);
						vm.creditNote.originalInvoiceDate = dt;
						setGSTCalculationFlags();
						if (vm.creditNote.creditNoteItemMappingList
								&& vm.creditNote.creditNoteItemMappingList.length > 0) {
							vm.numberOfAddedItem = vm.creditNote.creditNoteItemMappingList.length;
							vm.numberOfItem = new Array(vm.numberOfAddedItem);
						}

						if (!vm.creditNote.creditNoteItemMappingList
								|| vm.creditNote.creditNoteItemMappingList.length === 0) {
							vm.creditNote.creditNoteItemMappingList = [];
							vm.creditNote.creditNoteItemMappingList[vm.numberOfAddedItem - 1] = {};
						}

						if (vm.creditNote.creditNoteChargeModelList
								&& vm.creditNote.creditNoteChargeModelList.length > 0) {
							vm.numberOfAddedCreditNoteCharge = vm.creditNote.creditNoteChargeModelList.length;
							vm.numberOfCreditNoteCharge = new Array(
									vm.numberOfAddedCreditNoteCharge);
						}

						if (!vm.creditNote.creditNoteChargeModelList
								|| vm.creditNote.creditNoteChargeModelList.length === 0) {
							vm.creditNote.creditNoteChargeModelList = [];
							vm.creditNote.creditNoteChargeModelList[vm.numberOfAddedCreditNoteCharge - 1] = {};
						}

						vm.ledgerId = vm.creditNote.ledgerData.ledgerId;
						$http
								.post(
										'../../ledgerData/getMultipleGstDataspcndn',
										{
											ledgerId : vm.creditNote.ledgerData.ledgerId
										}).then(function(response) {
									vm.ledgerCityList = response.data.data;
									vm.getLedgerCityList();
								}, errorCallback);

						$http.post('../../sellData/getLedgerSellDetails', {
							ledgerId : vm.creditNote.ledgerData.ledgerId
						}).then(function(response) {
							vm.ledgerSellList = response.data.data;
						}, errorCallback);

						for (var j = 0; j < vm.numberOfItem.length; j++) {
							var myVar = setInterval(
									getBatchItemDetails(
											j,
											vm.creditNote.creditNoteItemMappingList[j].item.itemId),
									1000);
						}
					};

					function getBatchItemDetails(i, itemId) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : itemId
										})
								.then(
										function(batchres) {

											vm.creditNote.creditNoteItemMappingList[i].batchNoList = batchres.data.data;
											vm.creditNote.creditNoteItemMappingList[i].batchId = batchres.data.data[0].batchId;
											vm.creditNote.creditNoteItemMappingList[i].batchno = batchres.data.data[0].batchno;
											console
													.log("batch Id :"
															+ vm.creditNote.creditNoteItemMappingList[i].batchId);
											$http
													.post(
															'../../itemData/getoneBatch',
															{
																batchId : vm.creditNote.creditNoteItemMappingList[i].batchId
															})
													.then(
															function(res) {
																var dt = formatDateForSale(res.data.data.mfgDate);
																var pt = formatDateForSale(res.data.data.expDate);

																vm.creditNote.creditNoteItemMappingList[i].mfgdate = dt;
																vm.creditNote.creditNoteItemMappingList[i].expdate = pt;
																// vm.sell.sellItemModel[k].batchId
																// =
																// res.data.data.batchId;

																vm.creditNote.creditNoteItemMappingList[i].batchno = res.data.data.batchNo;

																var dtp = formatDate(res.data.data.mfgDate);
																var ptp = formatDate(res.data.data.expDate);
																vm.creditNote.creditNoteItemMappingList[i].mfgdateForPrint = dtp;
																vm.creditNote.creditNoteItemMappingList[i].expdateForPrint = ptp;

															}, errorCallback);

										}, errorCallback);

					}

					vm.errorFetchData = function() {
					};

					vm.closeAddEditCreditNoteScreen = function() {
						vm.manageCreditNoteDataService
								.getCreditNoteList(vm.manageCreditNoteDataService.selectedCompanyId);
						vm.billInkDashboardService.currentViewUrl = "../creditnote/view/viewCreditNoteList.html";
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeAddEditCreditNoteScreen();
						} else if (response.data.status === "error") {
							alert(response.data.errMsg);
						}
					}

					function errorCallback(response) {
						alert(response.data.errMsg);
					}

					vm.saveCreditNoteData = function() {
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						vm.creditNote.creditNoteDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"YYYY-MM-DD");
						vm.creditNote.billNumberPrefix = vm.creditNote.billNumberPrefix
								|| "";
						vm.creditNote.billNumber = vm.creditNote.billNumberPrefix
								.concat(vm.creditNote.currentBillNumber);
						vm.creditNote.billDate = moment(vm.creditNote.billDate)
								.format("YYYY-MM-DD");
						// console.log("After :"+vm.creditNote.billDate);
						console.log("cityName :" + vm.creditNote.cityName);
						vm.creditNote.originalInvoiceDate = moment(
								vm.creditNote.originalInvoiceDate).format(
								"YYYY-MM-DD");
						$http.post('../../creditNoteData/saveCreditNoteData',
								vm.creditNote).then(successCallback,
								errorCallback);
					};

					vm.publishCreditNoteData = function() {
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId
						vm.creditNote.creditNoteDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"YYYY-MM-DD");
						vm.creditNote.billNumberPrefix = vm.creditNote.billNumberPrefix
								|| "";
						vm.creditNote.billNumber = vm.creditNote.billNumberPrefix
								.concat(vm.creditNote.currentBillNumber);
						$http.post(
								'../../creditNoteData/publishCreditNoteData',
								vm.creditNote).then(successCallback,
								errorCallback);
					};

					function validateCreditNoteSaveUpdate() {
						if (!vm.manageCreditNoteDataService.selectedCompanyId
								|| vm.manageCreditNoteDataService.selectedCompanyId < 1) {
							toaster
									.showMessage('error', '',
											'Company data is missing to save expense details');
							return false;
						}

						return true;
					}

					vm.uploadFileAndSaveCreditNoteData = function(action) {
						if (!validateCreditNoteSaveUpdate()) {
							return;
						}
						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'creditNote')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.creditNote.imageURLList = vm.creditNote.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.creditNote.imageURLList
															&& vm.creditNote.imageURLList.length > 0) {
														vm.creditNote.imageURL = vm.creditNote.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm.saveCreditNoteData();
													} else if (action === "Publish") {
														vm
																.publishCreditNoteData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.creditNote.imageURL = null;
							if (vm.creditNote.imageURLList
									&& vm.creditNote.imageURLList.length > 0) {
								vm.creditNote.imageURL = vm.creditNote.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.saveCreditNoteData();
							} else if (action === "Publish") {
								vm.publishCreditNoteData();
							}
						}
					};

					vm.updateSavedCreditNoteData = function() {
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						vm.creditNote.creditNoteDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"YYYY-MM-DD");
						vm.creditNote.billNumberPrefix = vm.creditNote.billNumberPrefix
								|| "";
						vm.creditNote.billNumber = vm.creditNote.billNumberPrefix
								.concat(vm.creditNote.currentBillNumber);
						$http
								.post(
										'../../creditNoteData/updateSavedCreditNoteData',
										vm.creditNote).then(successCallback,
										errorCallback);
					};

					vm.updatePublishCreditNoteData = function() {
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						vm.creditNote.creditNoteDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"YYYY-MM-DD");
						vm.creditNote.billNumberPrefix = vm.creditNote.billNumberPrefix
								|| "";
						vm.creditNote.billNumber = vm.creditNote.billNumberPrefix
								.concat(vm.creditNote.currentBillNumber);
						$http
								.post(
										'../../creditNoteData/updatePublishedCreditNoteData',
										vm.creditNote).then(successCallback,
										errorCallback);
					};

					vm.uploadFileAndUpdateCreditNoteData = function(action) {
						if (!validateCreditNoteSaveUpdate()) {
							return;
						}
						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'creditNote')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.creditNote.imageURLList = vm.creditNote.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.creditNote.imageURLList
															&& vm.creditNote.imageURLList.length > 0) {
														vm.creditNote.imageURL = vm.creditNote.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.updateSavedCreditNoteData();
													} else if (action === "Publish") {
														vm
																.updatePublishCreditNoteData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.creditNote.imageURL = null;
							if (vm.creditNote.imageURLList
									&& vm.creditNote.imageURLList.length > 0) {
								vm.creditNote.imageURL = vm.creditNote.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.updateSavedCreditNoteData();
							} else if (action === "Publish") {
								vm.updatePublishCreditNoteData();
							}
						}
					};

					vm.markCreditNoteAsVerified = function() {
						vm.creditNote.verifiedBy = {};
						vm.creditNote.verifiedBy.userId = vm.billInkDashboardService.userId;
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						$http
								.post(
										'../../creditNoteData/markCreditNoteAsVerified',
										vm.creditNote).then(successCallback,
										errorCallback);
					};

					vm.publishSavedCreditNote = function() {
						vm.creditNote.companyId = vm.manageCreditNoteDataService.selectedCompanyId;
						$http.post(
								'../../creditNoteData/publishSavedCreditNote',
								vm.creditNote).then(successCallback,
								errorCallback);
					};

					function getsaleIdForSelectedBill() {
						console.log("getsaleIdForSelectedBill call");
						if (vm.creditNote.creditNoteSellMappingList.length != 0) {
							console.log("not null ")
							for (var i = 0; i < vm.creditNote.creditNoteSellMappingList.length; i++) {
								console
										.log(vm.creditNote.creditNoteSellMappingList);
								console
										.log(vm.creditNote.creditNoteSellMappingList[i]);
								saleId = saleId
										.concat(+vm.creditNote.creditNoteSellMappingList[i].sell.sellId
												+ ",");
							}
						}
					}

					vm.selectSellBill = function(sellObj) {
						if (indexOfSellObj(sellObj,
								vm.creditNote.creditNoteSellMappingList) === -1) {
							var creditNoteSellMapping = {};
							creditNoteSellMapping.sell = sellObj;
							vm.creditNote.creditNoteSellMappingList
									.push(creditNoteSellMapping);
							vm.searchBillNumber = null;
							vm.searchSellBillList = [];
							getsaleIdForSelectedBill();
						}
					};

					vm.deleteFromSeletedSell = function(sellObj) {
						var sellObjIndex = indexOfSellObj(sellObj,
								vm.creditNote.creditNoteSellMappingList);
						vm.creditNote.creditNoteSellMappingList.splice(
								sellObjIndex, 1);
					};

					vm.searchSellBill = function() {
						var sellFilterData = {};
						sellFilterData.sellId = vm.searchBillNumber;
						sellFilterData.ledgerId = vm.creditNote.ledgerData.ledgerId;

						if (vm.searchBillNumber) {
							$http
									.post(
											'../../sellData/getSellListData',
											{
												companyId : vm.manageCreditNoteDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 20,
												filterData : sellFilterData
											}).then(getSellBillDetail,
											errorCallback);
						} else {
							vm.searchSellBillList = [];
						}
					};

					function indexOfSellObj(sellObj, creditNoteSellList) {
						for (var count = 0; count < creditNoteSellList.length; count++) {
							if (sellObj.sellId === creditNoteSellList[count].sell.sellId) {
								return count;
							}
						}
						return -1;
					}

					function getSellBillDetail(response) {
						vm.searchSellBillList = response.data.data;
					}

					vm.searchLedger = function() {
						if (vm.creditNote.ledgerData
								&& vm.creditNote.ledgerData.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];
							return;
						}
						return $http
								.post(
										'../../ledgerData/getLedgerNameIdListData',
										{
											ledgerName : vm.searchLedgerNameText,
											companyId : vm.manageCreditNoteDataService.selectedCompanyId
										}).then(function(response) {
									vm.calculateIGST = false;
									vm.calculateSGST = false;
									vm.calculateCGST = false;
									vm.searchLedgerList = response.data.data;
									return vm.searchLedgerList;
								}, errorCallback);
					}

					vm.selectLedger = function(ledgerObj) {
						if (!ledgerObj || !ledgerObj.ledgerName) {
								return;
						}
						vm.ledgerId = ledgerObj.ledgerId;
						vm.searchLedgerNameText = ledgerObj.ledgerName;
						vm.creditNote.ledgerData = ledgerObj;
						vm.ledgerId = ledgerObj.ledgerId;
						$http.post('../../sellData/getLedgerSellDetails', {
							ledgerId : ledgerObj.ledgerId
						}).then(function(response) {
							vm.ledgerSellList = response.data.data;
						}, errorCallback);

						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : ledgerObj.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);

						setGSTCalculationFlags();
					};

					vm.getLedgerCityList = function() {
						var flag = true;
						angular
								.forEach(
										vm.ledgerCityList,
										function(ledgerList) {

											if (flag == true && vm.creditNote.cityName && vm.creditNote.cityName.toLowerCase() === ledgerList.city.toLowerCase()) {

												console.log(ledgerList);
												vm.creditNote.gstNumber = ledgerList.multipleGst;
												flag = false;
												return;
											}
										});

					};

					vm.getLedgerSellDetails = function() {
						console.log("getLedgerSellDetails call");
						if (vm.creditNote.InvoiceNumber != undefined
								&& vm.creditNote.InvoiceNumber != "") {
							var debitNoteDate = vm.creditNote.InvoiceNumber
									.split("@");
							console
									.log("debitNoteDate[0] :"
											+ debitNoteDate[0]);
							var dt = formatDateForSale(debitNoteDate[1]);
							vm.creditNote.originalInvoiceNumber = debitNoteDate[0];
							vm.creditNote.originalInvoiceDate = dt;
							vm.BillDate = moment(debitNoteDate[1]).format(
									"YYYY-MM-DD");
							console.log("SellBillDate :" + vm.BillDate);

							$http
									.post(
											'../../sellData/getledgerItemSellList',
											{
												sellId : debitNoteDate[8]
											})
									.then(
											function(response) {

												vm.ledgerSell = angular
														.copy(response.data.data);

												if (vm.ledgerSell
														&& vm.ledgerSell.length > 0) {
													vm.numberOfAddedItem = vm.ledgerSell.length;
													vm.numberOfItem = new Array(
															vm.numberOfAddedItem);
												}

												for (var i = 0; i < vm.numberOfItem.length; i++) {
													vm.creditNote.creditNoteItemMappingList[i].item = {};
													vm.creditNote.creditNoteItemMappingList[i].item.itemName = vm.ledgerSell[i].item.itemName;
													vm.creditNote.creditNoteItemMappingList[i].quantity = vm.ledgerSell[i].quantity;
													vm.creditNote.creditNoteItemMappingList[i].item.itemId = vm.ledgerSell[i].item.itemId;
													vm.creditNote.creditNoteItemMappingList[i].taxRate = 0;
													vm.creditNote.creditNoteItemMappingList[i].item.hsnCode = 0;
													vm.creditNote.creditNoteItemMappingList[i].taxRatefreeItem = 0;
													// vm.getAmount(i);
													$http
															.post(
																	'../../schemeData/getBatchItemDetailsByItemId',
																	{
																		itemId : vm.ledgerSell[i].item.itemId
																	})
															.then(
																	function(
																			batchres) {

																		var j = i - 1;

																		var dt = formatDateForSale(batchres.data.data[j].mfgDate);
																		var pt = formatDateForSale(batchres.data.data[j].expDate);

																		vm.creditNote.creditNoteItemMappingList[j].mfgdate = dt;
																		vm.creditNote.creditNoteItemMappingList[j].expdate = pt;

																		vm.creditNote.creditNoteItemMappingList[j].batchno = batchres.data.data[j].batchNo;
																		vm.creditNote.creditNoteItemMappingList[j].batchId = batchres.data.data[j].batchId;

																	},
																	errorCallback);
												}

											}, errorCallback);

							$http
									.post('../../itemData/getItemMrpByItemId',
											{
												itemId : debitNoteDate[7],
												wetDate : vm.BillDate
											})
									.then(
											function(response) {
												for (var i = 0; i < vm.numberOfItem.length; i++) {
													vm.creditNote.creditNoteItemMappingList[i].sellRate = response.data.data[0].itemMrp;
												}

											}, errorCallback);

						}
					};

					function setGSTCalculationFlags() {
						if (vm.creditNote.ledgerData && vm.billInkDashboardService.igstTaxCalGSTType && vm.billInkDashboardService.igstTaxCalGSTType.indexOf(vm.creditNote.ledgerData.gstType) > -1) {
							vm.calculateIGST = true;
						} else {
							if (vm.creditNote.ledgerData
									&& vm.billInkDashboardService.currentCompanyObj
									&& vm.creditNote.ledgerData.state
									&& vm.creditNote.ledgerData.state.stateId === vm.billInkDashboardService.currentCompanyObj.state.stateId) {
								vm.calculateSGST = true;
								vm.calculateCGST = true;
							} else {
								vm.calculateIGST = true;
							}
						}
					}

					vm.selectItem = function(index, itemObj) {
						if (!itemObj) {
							return;
						}
						if (vm.creditNote.creditNoteItemMappingList[index] && vm.creditNote.creditNoteItemMappingList[index].creditNoteItemMappingId > 0) {
										vm.creditNote.creditNoteItemMappingList[index].creditNoteItemMappingId = 0;
										return;
						}
						vm.creditNote.creditNoteItemMappingList[index].item = {};
						vm.creditNote.creditNoteItemMappingList[index].item.itemName = itemObj.itemName;
						vm.creditNote.creditNoteItemMappingList[index].item.itemId = itemObj.itemId;
						vm.creditNote.creditNoteItemMappingList[index].item.hsnCode = itemObj.hsnCode;
						vm.creditNote.creditNoteItemMappingList[index].sellRate = itemObj.sellRate;
						vm.creditNote.creditNoteItemMappingList[index].taxRate = itemObj.taxCode;

						vm.creditNote.creditNoteItemMappingList[index].taxRatefreeItem = itemObj.taxCode;

						vm.creditNote.creditNoteItemMappingList[index].quantity = itemObj.quantity;
						vm.creditNote.creditNoteItemMappingList[index].discount = itemObj.discount;
						vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount = itemObj.measureDiscountInAmount;
						vm.creditNote.creditNoteItemMappingList[index].additionaldiscount = itemObj.additionaldiscount;
						vm.creditNote.creditNoteItemMappingList[index].addmeasureDiscountInAmount = itemObj.addmeasureDiscountInAmount;
						vm.creditNote.creditNoteItemMappingList[index].locationId = itemObj.locationId;
						vm.creditNote.creditNoteItemMappingList[index].batchno = itemObj.batchId;
						vm.ItemId = vm.creditNote.creditNoteItemMappingList[index].item.itemId;

						console
								.log("ItemId :"
										+ vm.creditNote.creditNoteItemMappingList[index].item.itemId);
						// if
						// (vm.billInkDashboardService.noTaxCodeGSTType.indexOf(vm.creditNote.ledgerData.gstType)
						// > -1) {
						// vm.creditNote.creditNoteItemMappingList[index].taxRate
						// = 0;
						// }

						vm.creditNote.billDate = moment(vm.creditNote.billDate)
								.format("YYYY-MM-DD");
						console.log("Creadit Note Billdate :"
								+ vm.creditNote.billDate);
						$http
								.post(
										'../../itemData/getItemMrpByItemId',
										{
											itemId : vm.creditNote.creditNoteItemMappingList[index].item.itemId,
											wetDate : vm.creditNote.billDate
										})
								.then(
										function(response) {
											if (response.data.data[0]) {
												vm.creditNote.creditNoteItemMappingList[index].sellRate = response.data.data[0].itemMrp;
											}

										}, errorCallback);

						console
								.log("item.itemId ::"
										+ vm.creditNote.creditNoteItemMappingList[index].item.itemId);
						$http
								.post(
										'../../schemeData/getFreeItemByItemId',
										{
											itemId : vm.creditNote.creditNoteItemMappingList[index].item.itemId
										})
								.then(
										function(response) {
											vm.creditNote.creditNoteItemMappingList[index].freequantity = response.data.data.totalQtyOfFreeItem;
											// vm.creditNote.creditNoteItemMappingList[$index].Freequantity
											console
													.log("TotalQtyOfItem =====::"
															+ response.data.data.totalQtyOfItem);

											console
													.log("freeItemId=== :"
															+ response.data.data.freeItemId);
											console
													.log("Total Free Item == :"
															+ response.data.data.totalQtyOfFreeItem);
											vm.creditNote.creditNoteItemMappingList[index].freeItemId = response.data.data.freeItemId;
											vm.totalQtyOfFreeItems = response.data.data.totalQtyOfItem;

											vm.creditNote.creditNoteItemMappingList[index].Totalquantity = response.data.data.totalQtyOfFreeItem;
											$http
													.post(
															'../../schemeData/getFreeItemNameByItemId',
															{
																itemId : response.data.data.freeItemId
															})
													.then(
															function(res) {
																console
																		.log("Free ItemName ==== :"
																				+ res.data.data.itemName);
																if(res.data.data){
																vm.creditNote.creditNoteItemMappingList[index].FreeitemName = res.data.data.itemName;
																}
															}, errorCallback);
										}, errorCallback);

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : vm.creditNote.creditNoteItemMappingList[index].item.itemId
										})
								.then(
										function(batchres) {
											vm.creditNote.creditNoteItemMappingList[index].batchNoList = batchres.data.data;
											vm.creditNote.creditNoteItemMappingList[index].batchId = batchres.data.data[0].batchId;
											console
													.log("select Item batch no :"
															+ batchres.data.data[0].batchNo);
											vm.creditNote.creditNoteItemMappingList[index].batchno = batchres.data.data[0].batchNo;
											var dt = formatDateForSale(batchres.data.data[0].mfgDate);
											var pt = formatDateForSale(batchres.data.data[0].expDate);

											var dtp = formatDate(batchres.data.data[0].mfgDate);
											var ptp = formatDate(batchres.data.data[0].expDate);

											vm.creditNote.creditNoteItemMappingList[index].mfgdate = dt;
											vm.creditNote.creditNoteItemMappingList[index].expdate = pt;

											vm.creditNote.creditNoteItemMappingList[index].mfgdateForPrint = dtp;
											vm.creditNote.creditNoteItemMappingList[index].expdateForPrint = ptp;

										}, errorCallback);

					};

					function formatDateForSale(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						console.log("date of d:" + d);
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					vm.fetchItemList = function(index) {
						// var itemSearchText =
						// vm.creditNote.creditNoteItemMappingList[index].item.itemName;
						// //if (itemSearchText.length > 2) {
						// $http.post('../../itemData/getItemBasicDetailList',
						// {itemName: itemSearchText, companyId:
						// vm.manageCreditNoteDataService.selectedCompanyId,
						// start: 0, noOfRecord: 30})
						// .then(function(response) {
						// vm.itemList = [];
						// vm.itemList[index] = {};
						// vm.itemList[index].itemData = response.data.data;
						// vm.getBatchnolist(index);
						// }, errorCallback);
						// }
// var itemSearchText =
// vm.creditNote.creditNoteItemMappingList[index].item.itemName;
						var itemSearchText = vm.creditNote.creditNoteItemMappingList[index].item.itemName;
						if (itemSearchText.length > 0) {
							var itemSearchText = vm.creditNote.creditNoteItemMappingList[index].item.itemName;
							console.log("getItemList :" + saleId);
							return $http.post('../../sellData/getItemNameForCredit', {
								itemName : vm.itemSearchText,
								sellId : saleId
							}).then(function(res) {

								// vm.debitData = angular.copy(res.data.data);
								vm.itemList = [];
								vm.itemList[index] = {};
								vm.itemList[index].itemData = res.data.data;
								vm.getBatchnolist(index);
								// console.log("length res
								// :"+vm.debitData.length);
								// vm.itemList = {};
								// vm.itemListDebit = [];
								return vm.itemList[index].itemData;

							}, errorCallback);
						}
					};

					vm.fetchBatchNoItemList = function(index) {
						var itemBatchNoSearchText = vm.creditNote.creditNoteItemMappingList[index].item.batchNo;
						console.log("ItemBatch No :" + itemBatchNoSearchText);
						// if (itemSearchText.length > 2) {
						$http
								.post(
										'../../schemeData/getBatchItemDetails',
										{
											batchNo : itemBatchNoSearchText,
											itemId : vm.creditNote.creditNoteItemMappingList[index].item.itemId
										})
								.then(
										function(response) {
											vm.itemBatchNoList = [];
											vm.itemBatchNoList[index] = {};
											vm.itemBatchNoList[index].itemBatchData = response.data.data;

										}, errorCallback);
						// }
					};
					vm.getAmount = function(index) {
						vm.creditNote.creditNoteItemMappingList[index].totalItemAmount = 0;
						if (vm.creditNote.creditNoteItemMappingList[index].item
								&& vm.creditNote.creditNoteItemMappingList[index].item.itemId
								&& vm.creditNote.creditNoteItemMappingList[index].quantity
								&& vm.creditNote.creditNoteItemMappingList[index].sellRate) {
							vm.creditNote.creditNoteItemMappingList[index].totalItemAmount = vm.creditNote.creditNoteItemMappingList[index].sellRate
									* vm.creditNote.creditNoteItemMappingList[index].quantity;
							vm.creditNote.creditNoteItemMappingList[index].totalItemAmount += (vm.creditNote.creditNoteItemMappingList[index].totalItemAmount
									* vm.creditNote.creditNoteItemMappingList[index].taxRate / 100);

							if (vm.creditNote.creditNoteItemMappingList[index].discount) {
								if (vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount) {
									vm.creditNote.creditNoteItemMappingList[index].totalItemAmount -= vm.creditNote.creditNoteItemMappingList[index].discount;
								} else {
									vm.creditNote.creditNoteItemMappingList[index].totalItemAmount -= (vm.creditNote.creditNoteItemMappingList[index].totalItemAmount
											* vm.creditNote.creditNoteItemMappingList[index].discount / 100);
								}
							}
						}
						vm.creditNote.creditNoteItemMappingList[index].totalItemAmount = vm.creditNote.creditNoteItemMappingList[index].totalItemAmount
								.toFixed(2);
						return vm.creditNote.creditNoteItemMappingList[index].totalItemAmount;
					};

					vm.getItemLevelAmount = function(index) {
						vm.creditNote.creditNoteItemMappingList[index].itemAmount = 0;
						if (vm.creditNote.creditNoteItemMappingList[index].item
								&& vm.creditNote.creditNoteItemMappingList[index].item.itemId
								&& vm.creditNote.creditNoteItemMappingList[index].quantity
								&& vm.creditNote.creditNoteItemMappingList[index].sellRate) {
							var itemAmount = vm.creditNote.creditNoteItemMappingList[index].sellRate
									* vm.creditNote.creditNoteItemMappingList[index].quantity;

							if (vm.creditNote.creditNoteItemMappingList[index].discount) {

								if (vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount) {
									itemAmount -= vm.creditNote.creditNoteItemMappingList[index].discount;
								} else {
									itemAmount -= (itemAmount
											* vm.creditNote.creditNoteItemMappingList[index].discount / 100);
								}
							}

							vm.creditNote.creditNoteItemMappingList[index].itemAmount = parseFloat(
									itemAmount).toFixed(2);
						}
						return vm.creditNote.creditNoteItemMappingList[index].itemAmount;
					};

					vm.getSGSTAmount = function(index) {

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteItemMappingList[index].sgst;
						}

						vm.creditNote.creditNoteItemMappingList[index].sgst = 0;

						if (vm.calculateSGST) {
							if (vm.creditNote.creditNoteItemMappingList[index].item
									&& vm.creditNote.creditNoteItemMappingList[index].item.itemId
									&& vm.creditNote.creditNoteItemMappingList[index].quantity
									&& vm.creditNote.creditNoteItemMappingList[index].sellRate) {
								var itemAmount = vm.creditNote.creditNoteItemMappingList[index].sellRate
										* vm.creditNote.creditNoteItemMappingList[index].quantity;

								if (vm.creditNote.creditNoteItemMappingList[index].discount) {

									if (vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteItemMappingList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteItemMappingList[index].discount / 100);
									}
								}

								var taxAmount = (itemAmount
										* vm.creditNote.creditNoteItemMappingList[index].taxRate / 100);
								vm.creditNote.creditNoteItemMappingList[index].sgst = taxAmount / 2;
							}
						}
						vm.creditNote.creditNoteItemMappingList[index].sgst = parseFloat(
								vm.creditNote.creditNoteItemMappingList[index].sgst)
								.toFixed(2);
						return vm.creditNote.creditNoteItemMappingList[index].sgst;
					};

					vm.getCGSTAmount = function(index) {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteItemMappingList[index].cgst;
						}

						vm.creditNote.creditNoteItemMappingList[index].cgst = 0;
						if (vm.calculateCGST) {
							if (vm.creditNote.creditNoteItemMappingList[index].item
									&& vm.creditNote.creditNoteItemMappingList[index].item.itemId
									&& vm.creditNote.creditNoteItemMappingList[index].quantity
									&& vm.creditNote.creditNoteItemMappingList[index].sellRate) {
								var itemAmount = vm.creditNote.creditNoteItemMappingList[index].sellRate
										* vm.creditNote.creditNoteItemMappingList[index].quantity;

								if (vm.creditNote.creditNoteItemMappingList[index].discount) {

									if (vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteItemMappingList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteItemMappingList[index].discount / 100);
									}
								}
								var taxAmount = (itemAmount
										* vm.creditNote.creditNoteItemMappingList[index].taxRate / 100);
								vm.creditNote.creditNoteItemMappingList[index].cgst = taxAmount / 2;
							}
						}
						vm.creditNote.creditNoteItemMappingList[index].cgst = parseFloat(
								vm.creditNote.creditNoteItemMappingList[index].cgst)
								.toFixed(2);
						return vm.creditNote.creditNoteItemMappingList[index].cgst;
					};

					vm.getIGSTAmount = function(index) {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteItemMappingList[index].igst;
						}

						vm.creditNote.creditNoteItemMappingList[index].igst = 0;

						if (vm.calculateIGST) {

							if (vm.creditNote.creditNoteItemMappingList[index].item
									&& vm.creditNote.creditNoteItemMappingList[index].item.itemId
									&& vm.creditNote.creditNoteItemMappingList[index].quantity
									&& vm.creditNote.creditNoteItemMappingList[index].sellRate) {
								var itemAmount = vm.creditNote.creditNoteItemMappingList[index].sellRate
										* vm.creditNote.creditNoteItemMappingList[index].quantity;

								if (vm.creditNote.creditNoteItemMappingList[index].discount) {

									if (vm.creditNote.creditNoteItemMappingList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteItemMappingList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteItemMappingList[index].discount / 100);
									}
								}
								var taxAmount = (itemAmount
										* vm.creditNote.creditNoteItemMappingList[index].taxRate / 100);
								vm.creditNote.creditNoteItemMappingList[index].igst = taxAmount;
							}
						}
						vm.creditNote.creditNoteItemMappingList[index].igst = parseFloat(
								vm.creditNote.creditNoteItemMappingList[index].igst)
								.toFixed(2);
						return vm.creditNote.creditNoteItemMappingList[index].igst;
					};

					vm.getGrossTotalAmount = function() {
						vm.creditNote.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.creditNote.creditNoteItemMappingList.length; counter++) {
							if (vm.creditNote.creditNoteItemMappingList[counter].totalItemAmount
									&& vm.creditNote.creditNoteItemMappingList[counter].totalItemAmount > 0) {
								vm.creditNote.grossTotalAmount += parseFloat(vm.creditNote.creditNoteItemMappingList[counter].totalItemAmount);
							}
						}

						if (vm.creditNote.creditNoteChargeModelList.length > 0) {
							for (var count = 0; count < vm.creditNote.creditNoteChargeModelList.length; count++) {
								if (vm.creditNote.creditNoteChargeModelList[count].totalItemAmount)
									vm.creditNote.grossTotalAmount += parseFloat(vm.creditNote.creditNoteChargeModelList[count].totalItemAmount);
							}
						}

						vm.creditNote.grossTotalAmount = parseFloat(
								vm.creditNote.grossTotalAmount).toFixed(2);
						return vm.creditNote.grossTotalAmount;
					};

					vm.getTotalBillAmount = function() {
						vm.creditNote.amount = parseFloat(vm.creditNote.grossTotalAmount);

						if (vm.creditNote.discount
								&& vm.creditNote.discount > 0) {
							if (vm.creditNote.sellMeasureDiscountInAmount) {
								vm.creditNote.amount -= vm.creditNote.discount;
							} else {
								vm.creditNote.amount -= (vm.creditNote.amount
										* vm.creditNote.discount / 100);
							}
						}

						vm.creditNote.amount = parseFloat(vm.creditNote.amount)
								.toFixed(2);
						return vm.creditNote.amount;
					};

					vm.openImageModel = function(index) {
						if (index < 0
								|| index > (vm.creditNote.imageURLList - 1)) {
							return;
						}

						vm.creditNoteImageIndex = index;
						$("#creditNote-image-show-model .modal").modal('show');
						vm.selectedPhotoURL = vm.creditNote.imageURLList[index].imageURL;
					};

					vm.batchAddItemModel = function(index) {
						console.log("call this index for urcjase controller :"
								+ index);
						console
								.log("Purchase Item Id :"
										+ vm.creditNote.creditNoteItemMappingList[index].item.itemId);
						vm.creditNote.itemId = vm.creditNote.creditNoteItemMappingList[index].item.itemId;
						vm.index = index;
						console.log("batchAddItemModel Index :" + vm.index);
						// $("#add-batch-sell-dialog .modal").modal('show');
						$("#new-batch-creditNote-dialog .modal").modal('show');

					}

					vm.saveBatchSell = function() {
						console.log("sell Function call :"
								+ vm.creditNote.batchNo);
						if (!vm.creditNote.batchNo
								|| vm.creditNote.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.creditNote.mfgDate
								|| vm.creditNote.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.creditNote.mfgDate);
							var month = date.getMonth()
									+ vm.creditNote.shelfLifes;
							date.setMonth(month);
							vm.creditNote.expDate = moment(date).format(
									"YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							vm.creditNote.expDate = moment(
									vm.creditNote.mfgDate).format("YYYY-MM-DD");
						}

						vm.creditNote.mfgDate = moment(vm.creditNote.mfgDate)
								.format("YYYY-MM-DD");
						// vm.purchase.expDate =
						// moment(vm.purchase.expDate).format("YYYY-MM-DD");
						// vm.billInkDashboardService.item.push(vm.purchase);
						// console.log("shelflife :" + vm.purchase.shelfLifes);
						vm.creditNote.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.creditNote.shelfLifes;
						console.log("itemID :" + vm.creditNote.itemId);
						vm.batchNo = vm.creditNote.batchNo;
						vm.mfgDate = vm.creditNote.mfgDate;
						vm.shelfLife = vm.creditNote.shelfLife;
						vm.id = vm.ItemId;
						$http
								.post('../../itemData/saveBatch', {
									batchNo : vm.batchNo,
									mfgDate : vm.mfgDate,
									expDate : vm.creditNote.expDate,
									shelfLife : vm.shelfLife,
									itemId : vm.id
								})
								.then(
										function(response) {
											toaster.showMessage('success', '',
													'data saved successfully.');
											vm.creditNote.creditNoteItemMappingList[vm.index].batchNoList
													.push(response.data.data);
											vm.creditNote.creditNoteItemMappingList[vm.index].batchId = response.data.data.batchId;
											var dt = formatDateForSale(response.data.data.mfgDate);
											var pt = formatDateForSale(response.data.data.expDate);
											vm.creditNote.creditNoteItemMappingList[vm.index].mfgdate = dt;
											vm.creditNote.creditNoteItemMappingList[vm.index].expdate = pt;
											$(
													"#new-batch-creditNote-dialog .modal")
													.modal('hide');
										}, errorCallback);
						vm.creditNote.mfgDate = "";
						vm.creditNote.batchNo = "";
						vm.creditNote.shelfLife = "";
						// $("#new-batch-sell-dialog .modal").modal('hide');
					};

					vm.closedBatch = function() {
						$("#new-batch-creditNote-dialog .modal").modal('hide');
					}

					vm.fetchImage = function(position) {
						vm.creditNoteImageIndex += position;

						if (vm.creditNoteImageIndex < 0) {
							vm.creditNoteImageIndex = vm.creditNote.imageURLList.length - 1;
						} else if (vm.creditNoteImageIndex >= vm.creditNote.imageURLList.length) {
							vm.creditNoteImageIndex = 0;
						}
						vm.selectedPhotoURL = vm.creditNote.imageURLList[vm.creditNoteImageIndex].imageURL;
					};

					vm.deleteImage = function(index) {
						vm.creditNote.imageURLList.splice(index, 1);
					};

					function getImageURLListObj(imageURLList) {
						var creaditImageURLList = [];
						var imageURLObj = {};

						for (var count = 0; count < imageURLList.length; count++) {
							imageURLObj = {};
							imageURLObj.imageURL = imageURLList[count];
							creaditImageURLList.push(imageURLObj);
						}

						return creaditImageURLList;
					}

					// CreditNoteCharge related method.
					vm.fetchCreditNoteChargeLedgerList = function(index) {
						var ledgerSearchText = vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerName;
						if (ledgerSearchText.length > 2) {
							
						$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: ledgerSearchTextw, companyId: vm.manageCreditNoteDataService.selectedCompanyId, start: 0, noOfRecord: 30})
							.then(function(response) {
								vm.ledgerList = [];
								
									vm.ledgerList[index] = {};
									vm.ledgerList[index].ledgerData = response.data.data;
									vm.ledgerList[index].ledgerData; 
								}, errorCallback);
						}
					};

					vm.selectCreditNoteChargeLedger = function(index, ledgerObj) {
						
						if (!ledgerObj) {
							return;
						}
							
						if (vm.creditNote.creditNoteChargeModelList[index] && vm.creditNote.creditNoteChargeModelList[index].creditNoteChargeMappingId > 0) {
							vm.creditNote.creditNoteChargeModelList[index].creditNoteChargeMappingId = 0;
							return;
							}
						vm.creditNote.creditNoteChargeModelList[index].ledger = {};
						vm.creditNote.creditNoteChargeModelList[index].ledger = ledgerObj;
						vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge = ledgerObj.ledgerCharge;
						vm.creditNote.creditNoteChargeModelList[index].taxRate = ledgerObj.taxCode;

						if (vm.billInkDashboardService.noTaxCodeGSTType
								.indexOf(vm.creditNote.ledgerData.gstType) > -1) {
							vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge = 0;
						}
					};

					vm.getCreditNoteChargeAmount = function(index) {
						vm.creditNote.creditNoteChargeModelList[index].totalItemAmount = 0;
						vm.creditNote.creditNoteChargeModelList[index].quantity = 1;
						if (vm.creditNote.creditNoteChargeModelList[index].ledger
								&& vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerId
								&& vm.creditNote.creditNoteChargeModelList[index].quantity
								&& vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge) {
							vm.creditNote.creditNoteChargeModelList[index].totalItemAmount = vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge
									* vm.creditNote.creditNoteChargeModelList[index].quantity;

							if (vm.creditNote.creditNoteChargeModelList[index].taxRate) {
								vm.creditNote.creditNoteChargeModelList[index].totalItemAmount += (vm.creditNote.creditNoteChargeModelList[index].totalItemAmount
										* vm.creditNote.creditNoteChargeModelList[index].taxRate / 100);
							}

							if (vm.creditNote.creditNoteChargeModelList[index].discount) {
								if (vm.creditNote.creditNoteChargeModelList[index].measureDiscountInAmount) {
									vm.creditNote.creditNoteChargeModelList[index].totalItemAmount -= vm.creditNote.creditNoteChargeModelList[index].discount;
								} else {
									vm.creditNote.creditNoteChargeModelList[index].totalItemAmount -= (vm.creditNote.creditNoteChargeModelList[index].totalItemAmount
											* vm.creditNote.creditNoteChargeModelList[index].discount / 100);
								}
							}
						}
						vm.creditNote.creditNoteChargeModelList[index].totalItemAmount = vm.creditNote.creditNoteChargeModelList[index].totalItemAmount
								.toFixed(2);
						return vm.creditNote.creditNoteChargeModelList[index].totalItemAmount;
					};

					vm.getCreditNoteChargeLevelAmount = function(index) {
						vm.creditNote.creditNoteChargeModelList[index].itemAmount = 0;
						vm.creditNote.creditNoteChargeModelList[index].quantity = 1;
						if (vm.creditNote.creditNoteChargeModelList[index].ledger
								&& vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerId
								&& vm.creditNote.creditNoteChargeModelList[index].quantity
								&& vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge) {
							var itemAmount = vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge
									* vm.creditNote.creditNoteChargeModelList[index].quantity;

							if (vm.creditNote.creditNoteChargeModelList[index].discount) {

								if (vm.creditNote.creditNoteChargeModelList[index].measureDiscountInAmount) {
									itemAmount -= vm.creditNote.creditNoteChargeModelList[index].discount;
								} else {
									itemAmount -= (itemAmount
											* vm.creditNote.creditNoteChargeModelList[index].discount / 100);
								}
							}

							vm.creditNote.creditNoteChargeModelList[index].itemAmount = parseFloat(
									itemAmount).toFixed(2);
						}
						return vm.creditNote.creditNoteChargeModelList[index].itemAmount;
					};

					vm.getCreditNoteChargeSGSTAmount = function(index) {

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteChargeModelList[index].sgst;
						}

						vm.creditNote.creditNoteChargeModelList[index].sgst = 0;
						vm.creditNote.creditNoteChargeModelList[index].quantity = 1;
						if (vm.calculateSGST) {
							if (vm.creditNote.creditNoteChargeModelList[index].ledger
									&& vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerId
									&& vm.creditNote.creditNoteChargeModelList[index].quantity
									&& vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge) {
								var itemAmount = vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge
										* vm.creditNote.creditNoteChargeModelList[index].quantity;

								if (vm.creditNote.creditNoteChargeModelList[index].discount) {

									if (vm.creditNote.creditNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteChargeModelList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.creditNote.creditNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.creditNote.creditNoteChargeModelList[index].taxRate / 100);
								}
								vm.creditNote.creditNoteChargeModelList[index].sgst = taxAmount / 2;
							}
						}
						vm.creditNote.creditNoteChargeModelList[index].sgst = parseFloat(
								vm.creditNote.creditNoteChargeModelList[index].sgst)
								.toFixed(2);
						return vm.creditNote.creditNoteChargeModelList[index].sgst;
					};

					vm.getCreditNoteChargeCGSTAmount = function(index) {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteChargeModelList[index].cgst;
						}

						vm.creditNote.creditNoteChargeModelList[index].cgst = 0;
						vm.creditNote.creditNoteChargeModelList[index].quantity = 1;
						if (vm.calculateCGST) {
							if (vm.creditNote.creditNoteChargeModelList[index].ledger
									&& vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerId
									&& vm.creditNote.creditNoteChargeModelList[index].quantity
									&& vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge) {
								var itemAmount = vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge
										* vm.creditNote.creditNoteChargeModelList[index].quantity;

								if (vm.creditNote.creditNoteChargeModelList[index].discount) {

									if (vm.creditNote.creditNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteChargeModelList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.creditNote.creditNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.creditNote.creditNoteChargeModelList[index].taxRate / 100);
								}

								vm.creditNote.creditNoteChargeModelList[index].cgst = taxAmount / 2;
							}
						}
						vm.creditNote.creditNoteChargeModelList[index].cgst = parseFloat(
								vm.creditNote.creditNoteChargeModelList[index].cgst)
								.toFixed(2);
						return vm.creditNote.creditNoteChargeModelList[index].cgst;
					};

					vm.getCreditNoteChargeIGSTAmount = function(index) {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							return vm.creditNote.creditNoteChargeModelList[index].igst;
						}

						vm.creditNote.creditNoteChargeModelList[index].igst = 0;
						vm.creditNote.creditNoteChargeModelList[index].quantity = 1;
						if (vm.calculateIGST) {

							if (vm.creditNote.creditNoteChargeModelList[index].ledger
									&& vm.creditNote.creditNoteChargeModelList[index].ledger.ledgerId
									&& vm.creditNote.creditNoteChargeModelList[index].quantity
									&& vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge) {
								var itemAmount = vm.creditNote.creditNoteChargeModelList[index].creditNoteCharge
										* vm.creditNote.creditNoteChargeModelList[index].quantity;

								if (vm.creditNote.creditNoteChargeModelList[index].discount) {

									if (vm.creditNote.creditNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.creditNote.creditNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.creditNote.creditNoteChargeModelList[index].discount / 100);
									}
								}

								var taxAmount = 0;
								if (vm.creditNote.creditNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.creditNote.creditNoteChargeModelList[index].taxRate / 100);
								}

								vm.creditNote.creditNoteChargeModelList[index].igst = taxAmount;
							}
						}
						vm.creditNote.creditNoteChargeModelList[index].igst = parseFloat(
								vm.creditNote.creditNoteChargeModelList[index].igst)
								.toFixed(2);
						return vm.creditNote.creditNoteChargeModelList[index].igst;
					};

					vm.getCreditNoteChargeGrossTotalAmount = function() {
						vm.creditNote.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.creditNote.creditNoteChargeModelList.length; counter++) {
							if (vm.creditNote.creditNoteChargeModelList[counter].totalItemAmount
									&& vm.creditNote.creditNoteChargeModelList[counter].totalItemAmount > 0) {
								vm.creditNote.grossTotalAmount += parseFloat(vm.creditNote.creditNoteChargeModelList[counter].totalItemAmount);
							}
						}
						vm.creditNote.grossTotalAmount = parseFloat(
								vm.creditNote.grossTotalAmount).toFixed(2);
						return vm.creditNote.grossTotalAmount;
					};

					// 24-05-2019 Start
					vm.batchAddItemModel = function(index) {
						// console.log("call this index for urcjase controller
						// :"
						// + index);
						// console
						// .log("Purchase Item Id :"
						// + vm.purchase.purchaseItemModel[index].item.itemId);
						vm.index = index;
						vm.creditNote.itemId = vm.creditNote.creditNoteItemMappingList[index].item.itemId;
						vm.creditNote.creditNoteItemMappingList[vm.index].batchNoList = [];
						$("#new-batch-credit-dialog .modal").modal('show');
					}

					vm.saveBatchCreditNote = function() {
						console.log("vm.purchase.mfgDate :"
								+ vm.creditNote.mfgDate)
						if (!vm.creditNote.batchNo
								|| vm.creditNote.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.creditNote.mfgDate
								|| vm.creditNote.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.creditNote.mfgDate);
							var month = date.getMonth()
									+ vm.creditNote.shelfLifes;
							date.setMonth(month);
							vm.creditNote.expDate = moment(date).format(
									"YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							vm.creditNote.expDate = moment(
									vm.creditNote.mfgDate).format("YYYY-MM-DD");
						}

						vm.creditNote.mfgDate = moment(vm.creditNote.mfgDate)
								.format("YYYY-MM-DD");
						// vm.purchase.expDate =
						// moment(vm.purchase.expDate).format("YYYY-MM-DD");
						// vm.billInkDashboardService.item.push(vm.purchase);
						console.log("shelflife :" + vm.creditNote.shelfLifes);
						vm.creditNote.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.creditNote.shelfLifes;

						vm.batchNo = vm.creditNote.batchNo;
						vm.mfgDate = vm.creditNote.mfgDate;
						vm.shelfLife = vm.creditNote.shelfLife;
						vm.id = vm.ItemId;

						$http
								.post('../../itemData/saveBatch', {
									batchNo : vm.batchNo,
									mfgDate : vm.mfgDate,
									expDate : vm.creditNote.expDate,
									shelfLife : vm.shelfLife,
									itemId : vm.id
								})
								.then(
										function(response) {
											vm.creditNote.creditNoteItemMappingList[vm.index].batchNoList
													.push(response.data.data);
											// vm.debitNote.debitNoteItemMappingList[vm.index].batchNoList.push(response.data.data);
											vm.creditNote.creditNoteItemMappingList[vm.index].batchId = response.data.data.batchId;
											// vm.debitNote.debitNoteItemMappingList[$index].batchNoList
											// vm.sell.sellItemModel[vm.index].batchId
											// =
											// response.data.data.batchId;
											var dt = formatDateForPurchase(response.data.data.mfgDate);
											var pt = formatDateForPurchase(response.data.data.expDate);
											vm.creditNote.creditNoteItemMappingList[vm.index].mfgdate = dt;
											vm.creditNote.creditNoteItemMappingList[vm.index].expdate = pt;
											$("#new-batch-credit-dialog .modal")
													.modal('hide');
											toaster.showMessage('success', '',
													'data saved successfully.');
										}, errorCallback);
						vm.creditNote.mfgDate = "";
						vm.creditNote.batchNo = "";
						vm.creditNote.shelfLife = "";
						// $("#new-batch-purchase-dialog .modal").modal('hide');
					};

					vm.closedBatch = function() {
						$("#new-batch-credit-dialog .modal").modal('hide');
					}

					function formatDateForPurchase(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						console.log("date of d:" + d);
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					vm.prepareHSNCodeWiseTaxList = function() {
						vm.hsnTaxCodeList = {};
						vm.totalIGST = 0;
						vm.totalSGST = 0;
						vm.totalCGST = 0;

						for ( var i in vm.creditNote.creditNoteItemMappingList) {
							var debitNoteItemObj = vm.creditNote.creditNoteItemMappingList[i];
							if (debitNoteItemObj.item) {

								debitNoteItemObj.item.hsnCode = debitNoteItemObj.item.hsnCode
										|| "";
								vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode] = vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode]
										|| {};
								if (vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate]) {
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].igst += parseFloat(debitNoteItemObj.igst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].cgst += parseFloat(debitNoteItemObj.cgst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].sgst += parseFloat(debitNoteItemObj.sgst);
								} else {
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate] = {};
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].igst = parseFloat(debitNoteItemObj.igst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].cgst = parseFloat(debitNoteItemObj.cgst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].sgst = parseFloat(debitNoteItemObj.sgst);
								}
								vm.totalIGST += parseFloat(debitNoteItemObj.igst);
								vm.totalCGST += parseFloat(debitNoteItemObj.cgst);
								vm.totalSGST += parseFloat(debitNoteItemObj.sgst);
							}
						}

						for ( var i in vm.creditNote.creditNoteItemMappingList) {
							var debitNoteFreightObj = vm.creditNote.creditNoteItemMappingList[i];
							if (debitNoteFreightObj.ledger) {

								debitNoteFreightObj.ledger.sacCode = debitNoteFreightObj.ledger.sacCode
										|| "";
								vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode] = vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode]
										|| {};
								if (vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate]) {
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].igst += parseFloat(debitNoteFreightObj.igst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].cgst += parseFloat(debitNoteFreightObj.cgst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].sgst += parseFloat(debitNoteFreightObj.sgst);
								} else {
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate] = {};
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].igst = parseFloat(debitNoteFreightObj.igst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].cgst = parseFloat(debitNoteFreightObj.cgst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].sgst = parseFloat(debitNoteFreightObj.sgst);
								}
								vm.totalIGST += parseFloat(debitNoteFreightObj.igst);
								vm.totalCGST += parseFloat(debitNoteFreightObj.cgst);
								vm.totalSGST += parseFloat(debitNoteFreightObj.sgst);
							}
						}

						vm.hsnList = [];
						if (Object.keys(vm.hsnTaxCodeList).length > 0) {
							for ( var hsnCode in vm.hsnTaxCodeList) {
								for ( var taxRate in vm.hsnTaxCodeList[hsnCode]) {
									var hsnTaxCodeObj = {};
									hsnTaxCodeObj.hsnCode = hsnCode;
									hsnTaxCodeObj.taxRate = taxRate;
									hsnTaxCodeObj.igst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].igst)
											.toFixed(2);
									hsnTaxCodeObj.cgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].cgst)
											.toFixed(2);
									hsnTaxCodeObj.sgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].sgst)
											.toFixed(2);
									vm.hsnList.push(hsnTaxCodeObj);
								}
							}
						}
						vm.totalIGST = parseFloat(vm.totalIGST).toFixed(2);
						vm.totalCGST = parseFloat(vm.totalCGST).toFixed(2);
						vm.totalSGST = parseFloat(vm.totalSGST).toFixed(2);
						vm.totalItemLevelAmountWithoutTax = parseFloat(vm.creditNote.grossTotalAmount)
								- parseFloat(vm.totalSGST)
								- parseFloat(vm.totalCGST)
								- parseFloat(vm.totalIGST);
						vm.totalItemLevelAmountWithoutTax = parseFloat(
								vm.totalItemLevelAmountWithoutTax).toFixed(2);
					};

					// 24-05-2019 End

					// printDiv Function start
					vm.printDiv = function() {
						vm.creditNote.sellPrintDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second
							var printContent = document
									.getElementById('printSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);
					}

					vm.withOutFreeQtyPrintDiv = function() {
						vm.creditNote.sellPrintDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second
									var printContent = document
											.getElementById('printWithOutFeeQtySellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									// WinPrint.document.left(100);
									// WinPrint.document.top(150);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.withOutBatchPrintDiv = function() {
						vm.creditNote.sellPrintDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second

									var printContent = document
											.getElementById('printWithOutbatchNoSellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.allFieldsPrintDiv = function() {
						vm.creditNote.sellPrintDate = moment(
								vm.creditNote.creditNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second

							var printContent = document
									.getElementById('printAllFieldSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);

					}

					// PrintDiv Fucntion End
					var one = [ "", "one ", "two ", "three ", "four ", "five ",
							"six ", "seven ", "eight ", "nine ", "ten ",
							"eleven ", "twelve ", "thirteen ", "fourteen ",
							"fifteen ", "sixteen ", "seventeen ", "eighteen ",
							"nineteen " ];
					var ten = [ "", "", "twenty ", "thirty ", "forty ",
							"fifty ", "sixty ", "seventy ", "eighty ",
							"ninety " ];
					function numToWords(n, s) {
						var str = "";
						// if n is more than 19, divide it
						if (n > 19) {
							str += ten[parseInt(n / 10)] + one[n % 10];
						} else {
							str += one[n];
						}

						// if n is non-zero
						if (n != 0) {
							str += s;
						}
						return str;
					}
					function convertToWords(n) {
						// stores word representation of given number n
						var out = "";

						// handles digits at ten millions and hundred
						// millions places (if any)
						out += numToWords(parseInt((n / 10000000)), "crore ");

						// handles digits at hundred thousands and one
						// millions places (if any)
						out += numToWords(parseInt(((n / 100000) % 100)),
								"lakh ");

						// handles digits at thousands and tens thousands
						// places (if any)
						out += numToWords(parseInt(((n / 1000) % 100)),
								"thousand ");

						// handles digit at hundreds places (if any)
						out += numToWords(parseInt(((n / 100) % 10)),
								"hundred ");

						if (n > 100 && n % 100 > 0) {
							out += "and ";
						}

						// handles digits at ones and tens places (if any)
						out += numToWords(parseInt((n % 100)), "");

						return out;
					}
					vm.convertToWordsString = function(number) {
						var sArray = number.split(".");
						var paisePart = "";
						var integer = parseInt(sArray[0]);
						if (sArray.length == 2) {
							var s = parseInt(sArray[1]);
							paisePart = convertToWords(s);
						}
						var words = convertToWords(integer)
								+ (paisePart.length > 0 ? ("and " + paisePart + "Paise")
										: "") + " Only";
						return words;

					}
				});