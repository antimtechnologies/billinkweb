angular.module('manageCreditNoteData', ['infinite-scroll'])
.service("manageCreditNoteDataService", function($http) {
	
	var vm = this;

	vm.creditNoteList = [];
	vm.startOfCreditNote = 0;
	vm.noOfRecordForCreditNote = 12;
	vm.creditNoteFilterData = {};
	vm.hasMoreCreditNoteData = true;

	vm.companyCountRecordList = [];
	vm.startOfCompanyCount = 0;
	vm.noOfRecordForCompanyCount = 12;
	vm.hasMoreCompanyCountData = true;

	vm.getCreditNoteList = function(companyId) {
		$http.post('../../creditNoteData/getCreditNoteListData', {
			companyId: companyId, 
			start: vm.startOfCreditNote, 
			noOfRecord: vm.noOfRecordForCreditNote,
			filterData : vm.creditNoteFilterData
		}).then(successCallback, errorCallback);	
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfCreditNote === 0) {
				vm.creditNoteList = response.data.data;
				if (vm.creditNoteList.length < vm.noOfRecordForCreditNote) {
					vm.hasMoreCreditNoteData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCreditNote) {				
				vm.hasMoreCreditNoteData = false;
			}

			vm.creditNoteList = vm.creditNoteList.concat(response.data.data);
		}
	}

	function errorCallback() {
			
	}

	vm.getCompanyCountData = function() {
		$http.post('../../companydata/getCompanyRequestCountDetails', {start: vm.startOfCompanyCount, noOfRecord: vm.noOfRecordForCompanyCount, sortParam: 'creditNotePendingCount' }).
		then(companyCountSuccessCallBack, errorCallback);
	};

	function companyCountSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompanyCount === 0) {
				vm.companyCountRecordList = response.data.data;
				if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
					vm.hasMoreCompanyCountData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCompanyCount) {				
				vm.hasMoreCompanyCountData = false;
			}

			vm.companyCountRecordList = vm.companyCountRecordList.concat(response.data.data);
		}
	}

})
.controller('manageCreditNoteDataController', function($http, $ocLazyLoad, manageCreditNoteDataService, billInkDashboardService) {
	var vm = this;
	vm.manageCreditNoteDataService = manageCreditNoteDataService;
	vm.billInkDashboardService = billInkDashboardService;

	if (vm.billInkDashboardService.userRole !== "Vendor") {
		vm.statusList = ['New', 'Modified', 'Deleted'];
	} else {		
		vm.statusList = ['New', 'Modified', 'Deleted', 'UnPublished'];
	}

	vm.manageCreditNoteDataService.creditNoteFilterData = {};
	vm.manageCreditNoteDataService.creditNoteFilterData.statusList = [];

	vm.openCreditNoteList = function(companyObj) {

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageCreditNoteDataService.creditNoteFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageCreditNoteDataService.creditNoteFilterData.isVerified = 0;
		}

		vm.billInkDashboardService.currentCompanyObj = companyObj;
		vm.manageCreditNoteDataService.selectedCompanyId = companyObj.companyId;
		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		vm.billInkDashboardService.currentViewUrl = "../creditnote/view/viewCreditNoteList.html";
		vm.manageCreditNoteDataService.getCreditNoteList(vm.manageCreditNoteDataService.selectedCompanyId);
	};
	
	vm.backToCompanyWiseCreditNoteData = function() {
		vm.billInkDashboardService.currentViewUrl = "../creditnote/view/companyWiseCreditNoteCount.html";
		vm.manageCreditNoteDataService.getCompanyCountData();
	};

	vm.init = function() {
		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.billInkDashboardService.currentViewUrl = "../creditnote/view/companyWiseCreditNoteCount.html";
			vm.manageCreditNoteDataService.getCompanyCountData();
		} else {			
			vm.openCreditNoteList(vm.billInkDashboardService.selectedCompany);
		}
	};

	vm.loadMoreCreditNote = function() {
		if (vm.manageCreditNoteDataService.hasMoreCreditNoteData) {
			vm.manageCreditNoteDataService.startOfCreditNote +=  vm.manageCreditNoteDataService.noOfRecordForCreditNote;
			vm.manageCreditNoteDataService.getCreditNoteList(vm.manageCreditNoteDataService.selectedCompanyId);
		}
	};
	
	vm.loadMoreCompanyCountData = function() {
		if (vm.manageCreditNoteDataService.hasMoreCompanyCountData) {
			vm.manageCreditNoteDataService.startOfCompanyCount +=  vm.manageCreditNoteDataService.noOfRecordForCreditNote;
			vm.manageCreditNoteDataService.getCompanyCountData();
		}
	};

	vm.showAddCreditNoteDataForm = function(requestFor) {
		vm.manageCreditNoteDataService.requestType = "Add";
		vm.manageCreditNoteDataService.requestFor = requestFor;
		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../creditnote/js/addEditCreditNoteDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../creditnote/view/addEditCreditNoteDetail.html";
		});
	};

	vm.showDeletePrompt = function(creditNoteId, statusName) {
		vm.selectedDeleteCreditNoteId = creditNoteId;
		vm.selectedDeleteCreditNoteStatus = statusName;
		$("#creditNote-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteSavedCreditNoteData = function() {
		$http.post('../../creditNoteData/deleteSavedCreditNoteData', {
			creditNoteId: vm.selectedDeleteCreditNoteId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageCreditNoteDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	}

	vm.deletePublishCreditNoteData = function() {
		$http.post('../../creditNoteData/deletePublishedCreditNoteData', {
			creditNoteId: vm.selectedDeleteCreditNoteId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageCreditNoteDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	};

	vm.deleteCreditNoteData = function() {
		if (vm.selectedDeleteCreditNoteStatus === "UnPublished") {
			vm.deleteSavedCreditNoteData();
		} else {
			vm.deletePublishCreditNoteData();
		}
	};

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteCreditNoteId = 0;
		vm.selectedDeleteCreditNoteStatus = "";
		$("#creditNote-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
	}

	vm.editCreditNoteData = function(creditNoteId, imageURL) {
		vm.manageCreditNoteDataService.requestType = "Edit";
		vm.manageCreditNoteDataService.requestFor = imageURL && imageURL !== "" ? "uploadImage" : "enterManually";
		vm.manageCreditNoteDataService.editCreditNoteId = creditNoteId;
		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../creditnote/js/addEditCreditNoteDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../creditnote/view/addEditCreditNoteDetail.html";
		});
	};

	// filter related function start
	vm.launchFilter = function() {
		$("#creditNote-filter-dialog .modal").modal('show');
	};

	vm.closeFilterModal = function() {
		$("#creditNote-filter-dialog .modal").modal('hide');
	};

	vm.applyFilter = function() {
		vm.searchFilterData();
		$("#creditNote-filter-dialog .modal").modal('hide');
	};

	vm.searchFilterData = function() {
		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		vm.manageCreditNoteDataService.getCreditNoteList(vm.manageCreditNoteDataService.selectedCompanyId);		
	};

	vm.resetFilter = function() {
		vm.manageCreditNoteDataService.creditNoteFilterData = {};
		vm.manageCreditNoteDataService.creditNoteFilterData.statusList = [];

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageCreditNoteDataService.creditNoteFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageCreditNoteDataService.creditNoteFilterData.isVerified = 0;
		}

		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		vm.manageCreditNoteDataService.getCreditNoteList(vm.manageCreditNoteDataService.selectedCompanyId);
		$("#creditNote-filter-dialog .modal").modal('hide');
	};

	vm.toggleSelection = function toggleSelection(status) {
		var idx = vm.manageCreditNoteDataService.creditNoteFilterData.statusList.indexOf(status);
	
		if (idx > -1) {
			vm.manageCreditNoteDataService.creditNoteFilterData.statusList.splice(idx, 1);
		} else {
			vm.manageCreditNoteDataService.creditNoteFilterData.statusList.push(status);
		}
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageCreditNoteDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.manageCreditNoteDataService.creditNoteFilterData.ledgerId = 0;
			vm.searchLedgerList = [];
		}
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.manageCreditNoteDataService.creditNoteFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
	};
	// filter related function end

	vm.$onDestroy = function() {
		vm.manageCreditNoteDataService.creditNoteList = [];
		vm.manageCreditNoteDataService.startOfCreditNote = 0;
		vm.manageCreditNoteDataService.hasMoreCreditNoteData = true;

		vm.manageCreditNoteDataService.companyCountRecordList = [];
		vm.manageCreditNoteDataService.startOfCompanyCount = 0;
		vm.manageCreditNoteDataService.hasMoreCompanyCountData = true;
	};

});
