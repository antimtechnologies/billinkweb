var addCustomerTypeDataApp = angular.module('manageCustomerType', [ 'infiniteScroll',
		'infinite-scroll','toaster' ]);

addCustomerTypeDataApp.service('manageCustomerTypeDataService', function($http) {

});

addCustomerTypeDataApp
		.controller(
				'addEditCustomerTypeDataController',
				function($http, $ocLazyLoad, manageCustomerTypeDataService,
						billInkDashboardService,toaster) {

					var vm = this;
					vm.manageCustomerTypeDataService = manageCustomerTypeDataService;
					vm.billInkDashboardService = billInkDashboardService;
					vm.billInkDashboardService.customer = {};
					
					vm.customerDiscountListObj = [];
					vm.startOfCompany=0;
					const noOfRecord=30;
					vm.selectedCompanyId =0;
					vm.addCustomer = function() {
						if (!vm.billInkDashboardService.customer.customerType
								|| vm.billInkDashboardService.customer.customerType === "") {
							toaster.showMessage('error', '',
									'Kindly enter Cutomer Type name.');
							return false;
						}

						$http.post('../../customerTypeData/saveCutomerData',
								{customerType:vm.billInkDashboardService.customer.customerType,isDeleted:0,companyId:vm.selectedCompanyId}).then(
								successCallback, errorCallback);
					};
					
					vm.init = function() {
						vm.getCompanyList();
						
					};
					
					vm.getCustomerDiscountList = function() {
						$http.post('../../customerTypeData/getCustomerTypeListView', {companyId:vm.selectedCompanyId,start: vm.startOfCompany, noOfRecord: noOfRecord})
						.then(successCallbackCompanyDiscountList, errorCallbackCustomerDiscountList);	
					};

					function successCallbackCompanyDiscountList(response) {
						vm.customerDiscountListObj = angular.copy(response.data.result);
						
					}

					function errorCallbackCustomerDiscountList(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.uploadFileAndSaveCustomerData = function() {
						console.log("Save Btn click");
						vm.addCustomer();
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage("success","","Record Added/Updated.");
							vm.getCustomerDiscountList();
							// vm.closeItemDataSave();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					

					vm.getCompayItemDetail = function(companyId) {
						vm.selectedCompanyId = companyId;
						vm.selectedCompanyId = companyId;
						vm.startOfCompany = 0;
						vm.customerDiscountListObj = [];
						vm.getCustomerDiscountList();
					};
					

					function getCompanyDataSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompany === 0) {
								vm.companyList = response.data.data;
								vm.totalCount = response.data.totalCount;
								if (vm.companyList.length > 0) {
									vm.selectedCompanyId = vm.companyList[0].companyId;
									vm.selectedCompanyId = vm.companyList[0].companyId;
									vm.getCustomerDiscountList();
								}
								return;
							}
							vm.companyList = vm.companyList.concat(response.data.data);
						}
					}
					
					
					vm.getCompanyList = function() {
						$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecord, companyName: vm.searchCompanyNameText })
						.then(getCompanyDataSuccessCallBack, errorCallback);
					};
					
					function errorCallback() {
						
					}

					vm.loadMoreCompanyList = function() {
						if (vm.totalCount > vm.startOfCompany) {			
							vm.startOfCompany += noOfRecordForCompany;
							vm.getCompanyList();
							
						}
					};
					
					vm.getCompanyListOnSearch = function() {
						vm.startOfCompany = 0;
						vm.getCompanyList();
					};
					
					
					vm.deletecustomer=function(deleteid){
						$http.post('../../customerTypeData/deleteCustomerType', {customerTypeId:deleteid,companyId : vm.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
						.then(deletesuccessCallbackCompanyDiscountList, errorCallbackCustomerDiscountList);	
					}
					
					function deletesuccessCallbackCompanyDiscountList(){
						toaster.showMessage('success', '',"Record deleted successfully.");
						vm.getCustomerDiscountList();
					}

});


