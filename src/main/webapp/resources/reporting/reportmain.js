//console.log("call this at load time....");
$(".submenubutton")
		.click(
				function() {
				
					var id = this.id;
					if (id == 'purchasedayId') {
						if (!$(".purchaseDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".purchaseDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}else if (id == 'purchasemonthId') {
						if (!$(".purchasemonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".purchasemonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'saledayId') {
						if (!$(".saleDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".saleDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'salemonthId') {
						if (!$(".salemonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".salemonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'expensedayId') {
						if (!$(".expenseDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".expenseDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'expensemonthId') {
						if (!$(".expensemonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".expensemonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'recieptdayId') {
						if (!$(".recieptDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".recieptDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'recieptmonthId') {
						if (!$(".recieptmonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".recieptmonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'paymentdayId') {
						if (!$(".paymentDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".paymentDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'paymentmonthId') {
						if (!$(".paymentmonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".paymentmonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					
					else if (id == 'recievablesdayId') {
						if (!$(".recievablesDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".recievablesDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'recievablesmonthId') {
						if (!$(".recievablesmonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".recievablesmonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'payablesdayId') {
						if (!$(".payablesDaywiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".payablesDaywiseDetail").animate({
								height : 'toggle'
							});
						}
					}
					else if (id == 'payablesmonthId') {
						if (!$(".payablesmonthwiseDetail").is(":visible")) {
							resetDetail();
							$("#" + id).addClass("submenubutton-selected");
							$(".payablesmonthwiseDetail").animate({
								height : 'toggle'
							});
						}
					}
				});
				


$(".PurchaseMenu").click(function() {
//	resetDetail();
	$(".PurchaseDivList").show();
});


$(".SalesMenu").click(function() {
	$(".saleDivList").show();
});
$(".ExpenseMenu").click(function() {
	$(".DivList").show();
});
$(".RecieptMenu").click(function() {
	$(".recieptDivList").show();
});
$(".PaymentsMenu").click(function() {
	$(".paymentDivList").show();
});
$(".ReceivableMenu").click(function() {
	$(".recievablesDivList").show();
});
$(".PayableMenu").click(function() {
	$(".payablesDivList").show();
});

//$(".CreditMenu").click(function() {
////	resetDetail();
//	$(".saleDivList").show();
//});



function resetDetail(){
	$(".purchaseDaywiseDetail").hide();
	$(".purchasemonthwiseDetail").hide();
	$(".salemonthwiseDetail").hide();
	$(".saleDaywiseDetail").hide();
}



function displaypurchasedata()
{
	var day = document.getElementById("purchasedayselectId").value;
	$.ajax({
		type : "GET",
		url :"./purchaseData/findbyday?day="+day,
		dataType :"json",
		success : function(response)
		{
			alert("success");
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.billNumber
				        + "</td><td>"
						+ p.purchaseDate
						+ "</td><td>"
						+ p.totalBillAmount
				        + "</td><td>"
				        + p.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_purchaseDaywise").html(data);
			}
		},
		error : function() {
		}
	});
}



function monthWisePurchase()
{
	var month = document.getElementById("monthWiseSelectId").value;
	$.ajax({
		type : "GET",
		url :"./purchaseData/findbymonth?month="+month,
		dataType :"json",
		success : function(response)
		{
			alert("success");
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.billNumber
				        + "</td><td>"
						+ p.purchaseDate
						+ "</td><td>"
						+ p.totalBillAmount
				        + "</td><td>"
				        + p.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_purchasemonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}



function saledayWise(){
	
	var saleDay = document.getElementById("saledayselectId").value;
	$.ajax({
		type : "GET",
		url :"./sellData/findbyday?day="+saleDay,
		dataType :"json",
		success : function(response)
		{
			alert("success");
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.billNo
				        + "</td><td>"
						+ p.sellBillDate
						+ "</td><td>"
						+ p.totalBillAmount
				        + "</td><td>"
				        + p.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_saleDayWise").html(data);
			}
		},
		error : function() {
		}
	});
}



function monthWiseSale(){
	
	var saleMonth = document.getElementById("saleMonthWiseSelected").value;
	$.ajax({
		type : "GET",
		url :"./sellData/findbymonth?month="+saleMonth,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.billNo
				        + "</td><td>"
						+ p.sellBillDate
						+ "</td><td>"
						+ p.totalBillAmount
				        + "</td><td>"
				        + p.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_SaleMonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}
/////===================expense ==

//function expensedayWise(){
//	
//	var expenseDay = document.getElementById("expensedayselectId").value;
//	$.ajax({
//		type : "GET",
//		url :"./expenseData/findAllExpensebycompanyandday?companyId=50&day="+expenseDay,
//		dataType :"json",
//		success : function(response)
//		{
//			alert("success");
//			var data = "";
//			if (response.result == null) {
//				
//			} else {
//				for (i = 0; i < response.result.length; i++)
//				{
//					var p = response.result[i];
//					
//					data += "<tr>"
//                        +"<td>"
//						+ p.billNo
//				        + "</td><td>"
//						+ p.sellBillDate
//						+ "</td><td>"
//						+ p.totalBillAmount
//				        + "</td><td>"
//				        + p.comapyTableEntity.companyName
//						+ "</td><td>"
//						+ p.paymentStatus
//						+ "</td></tr>"
//				}
//				$("#tbl_saleDayWise").html(data);
//			}
//		},
//		error : function() {
//		}
//	});
//}
//
//
//
//function monthWiseSale(){
//	
//	var saleMonth = document.getElementById("saleMonthWiseSelected").value;
//	$.ajax({
//		type : "GET",
//		url :"./sellData/findbymonth?month="+saleMonth,
//		dataType :"json",
//		success : function(response)
//		{
//			var data = "";
//			if (response.result == null) {
//				
//			} else {
//				for (i = 0; i < response.result.length; i++)
//				{
//					var p = response.result[i];
//					
//					data += "<tr>"
//                        +"<td>"
//						+ p.billNo
//				        + "</td><td>"
//						+ p.sellBillDate
//						+ "</td><td>"
//						+ p.totalBillAmount
//				        + "</td><td>"
//				        + p.comapyTableEntity.companyName
//						+ "</td><td>"
//						+ p.paymentStatus
//						+ "</td></tr>"
//				}
//				$("#tbl_SaleMonthwise").html(data);
//			}
//		},
//		error : function() {
//		}
//	});
//}

// ===========================reciept ===========

function recieptdayWise(){
	
	var day = document.getElementById("recieptdayselectId").value;
	$.ajax({
		type : "GET",
		url :"./sellPaymentData/getallsalesPaymentByDay?ispaymentdone=1&day="+day,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.salesTableEntity.billNo
				        + "</td><td>"
						+ p.salesTableEntity.sellBillDate
						+ "</td><td>"
						+ p.salesTableEntity.totalBillAmount
				        + "</td><td>"
				        + p.salesTableEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.salesTableEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_recieptDayWise").html(data);
			}
		},
		error : function() {
		}
	});
}



function recieptmonthWise(){
	
	var month = document.getElementById("recieptMonthWiseSelected").value;
	$.ajax({
		type : "GET",
		url :"./sellPaymentData/getallsalesPaymentByMonth?ispaymentdone=1&month="+month,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.salesTableEntity.billNo
				        + "</td><td>"
						+ p.salesTableEntity.sellBillDate
						+ "</td><td>"
						+ p.salesTableEntity.totalBillAmount
				        + "</td><td>"
				        + p.salesTableEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.salesTableEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_recieptMonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}
//===================== payment =  

function paymentdayWise(){
	
	var day = document.getElementById("paymentdayselectId").value;
	$.ajax({
		type : "GET",
		url :"./purchasePaymentData/getallpurchasePaymentByDay?ispaymentdone=1&day="+day,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.purchaseEntity.billNumber
				        + "</td><td>"
						+ p.purchaseEntity.purchaseDate
						+ "</td><td>"
						+ p.purchaseEntity.totalBillAmount
				        + "</td><td>"
				        + p.purchaseEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.purchaseEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_paymentDayWise").html(data);
			}
		},
		error : function() {
		}
	});
}

function paymentmonthWise(){
	
	var month = document.getElementById("paymentMonthWiseSelected").value;
	$.ajax({
		type : "GET",
		url :"./purchasePaymentData/getallpurchasePaymentByMonth?ispaymentdone=1&month="+month,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.purchaseEntity.billNumber
				        + "</td><td>"
						+ p.purchaseEntity.purchaseDate
						+ "</td><td>"
						+ p.purchaseEntity.totalBillAmount
				        + "</td><td>"
				        + p.purchaseEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.purchaseEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_paymentMonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}

//====================== receivables

function recievablesdayWise(){
	
	var day = document.getElementById("recievablesdayselectId").value;
	$.ajax({
		type : "GET",
		url :"./sellPaymentData/getallsalesPaymentByDay?ispaymentdone=0&day="+day,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.salesTableEntity.billNo
				        + "</td><td>"
						+ p.salesTableEntity.sellBillDate
						+ "</td><td>"
						+ p.salesTableEntity.totalBillAmount
				        + "</td><td>"
				        + p.salesTableEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.salesTableEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_recievablesDayWise").html(data);
			}
		},
		error : function() {
		}
	});
}



function recievablesmonthWise(){
	
	var month = document.getElementById("recievablesMonthWiseSelected").value;
	$.ajax({
		type : "GET",
		url :"./sellPaymentData/getallsalesPaymentByMonth?ispaymentdone=1&month="+month,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.salesTableEntity.billNo
				        + "</td><td>"
						+ p.salesTableEntity.sellBillDate
						+ "</td><td>"
						+ p.salesTableEntity.totalBillAmount
				        + "</td><td>"
				        + p.salesTableEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.salesTableEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_recievablesMonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}

//================== payables
function payablesdayWise(){
	
	var day = document.getElementById("payablesdayselectId").value;
	$.ajax({
		type : "GET",
		url :"./purchasePaymentData/getallpurchasePaymentByDay?ispaymentdone=1&day="+day,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.purchaseEntity.billNumber
				        + "</td><td>"
						+ p.purchaseEntity.purchaseDate
						+ "</td><td>"
						+ p.purchaseEntity.totalBillAmount
				        + "</td><td>"
				        + p.purchaseEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.purchaseEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_payablesDayWise").html(data);
			}
		},
		error : function() {
		}
	});
}

function payablesmonthWise(){
	
	var month = document.getElementById("payablesMonthWiseSelected").value;
	$.ajax({
		type : "GET",
		url :"./purchasePaymentData/getallpurchasePaymentByMonth?ispaymentdone=1&month="+month,
		dataType :"json",
		success : function(response)
		{
			var data = "";
			if (response.result == null) {
				
			} else {
				for (i = 0; i < response.result.length; i++)
				{
					var p = response.result[i];
					
					data += "<tr>"
                        +"<td>"
						+ p.purchaseEntity.billNumber
				        + "</td><td>"
						+ p.purchaseEntity.purchaseDate
						+ "</td><td>"
						+ p.purchaseEntity.totalBillAmount
				        + "</td><td>"
				        + p.purchaseEntity.comapyTableEntity.companyName
						+ "</td><td>"
						+ p.purchaseEntity.paymentStatus
						+ "</td></tr>"
				}
				$("#tbl_payablesMonthwise").html(data);
			}
		},
		error : function() {
		}
	});
}