angular.module('addExcelUploadData', ['fileUploadDirective', 'toaster'])
.controller('addExcelUploadDataController', function($http, fileUploadService, toaster, manageExcelUploadDataService, billInkDashboardService) {
	var vm = this;
	vm.manageExcelUploadDataService = manageExcelUploadDataService;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.excelUpload = {};
	vm.excelUpload.otherDocImageURLList = [];
	vm.billInkDashboardService = billInkDashboardService;

	vm.init = function() {
		vm.excelUpload.companyId = vm.manageExcelUploadDataService.selectedCompanyId;
		if (vm.manageExcelUploadDataService.requestType === "Edit") {
			$http.post('../../excelUploadData/getExcelUploadDetailsById', { documentId: vm.manageExcelUploadDataService.editExcelUploadId, companyId : vm.manageExcelUploadDataService.selectedCompanyId})
			.then(vm.setExcelUploadDetail, vm.errorFetchData);			
		} else {
			vm.excelUpload.addedBy = {};
			vm.excelUpload.addedBy.userId = vm.billInkDashboardService.userId;
			vm.excelUpload.documentType = "Sell";
		}
	};

	vm.setExcelUploadDetail = function(response) {
		vm.excelUpload = angular.copy(response.data.data);
	};

	vm.errorFetchData = function() {
	};

	vm.closeAddEditExcelUploadScreen = function() {
		vm.manageExcelUploadDataService.getCompanyExcelUploadList(vm.manageExcelUploadDataService.selectedCompanyId);
		vm.manageExcelUploadDataService.excelUploadContainerURL = "../excelupload/view/viewExcelUploadList.html";		
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			vm.closeAddEditExcelUploadScreen();
		} else if (response.data.status === "error") {
			alert(response.data.errMsg);
		}
	}

	function errorCallback(response) {
		alert(response.data.errMsg);
	}

	vm.saveExcelUploadData = function() {
		vm.excelUpload.companyId = vm.manageExcelUploadDataService.selectedCompanyId;
		$http.post('../../excelUploadData/saveExcelDocumentData', vm.excelUpload).then(successCallback, errorCallback);
	};

	vm.uploadFileAndSaveExcelUploadData = function (action) {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'excelUpload').then(function(result){
				if (result.data.status === "success") {				
					vm.excelUpload.otherDocImageURLList = vm.excelUpload.otherDocImageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.excelUpload.otherDocImageURLList && vm.excelUpload.otherDocImageURLList.length > 0) {						
						vm.excelUpload.documentURL = vm.excelUpload.otherDocImageURLList[0].documentUrl;
					}
					vm.saveExcelUploadData();						
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.excelUpload.documentUrl = null;
			if (vm.excelUpload.otherDocImageURLList && vm.excelUpload.otherDocImageURLList.length > 0) {						
				vm.excelUpload.documentUrl = vm.excelUpload.otherDocImageURLList[0].documentUrl;
			}
			vm.saveExcelUploadData();						
		}
	};

	function getImageURLListObj(otherDocImageURLList) {
		var excelUploadImageURLList = [];
		var excelUploadImageURLObj = {};

		for (var count = 0; count < otherDocImageURLList.length; count++) {
			excelUploadImageURLObj = {};
			excelUploadImageURLObj.documentUrl = otherDocImageURLList[count];
			excelUploadImageURLList.push(excelUploadImageURLObj);
		}
		return excelUploadImageURLList;
	}

	vm.markExcelUploadAsVerified = function() {
		vm.excelUpload.verifiedBy = {};
		vm.excelUpload.verifiedBy.userId = vm.billInkDashboardService.userId;
		vm.excelUpload.companyId = vm.manageExcelUploadDataService.selectedCompanyId;
		$http.post('../../excelUploadData/markExcelUploadAsVerified', vm.excelUpload).then(successCallback, errorCallback);
	};

	vm.publishSavedExcelUpload = function() {
		vm.excelUpload.companyId = vm.manageExcelUploadDataService.selectedCompanyId;
		$http.post('../../excelUploadData/publishSavedExcelUpload', vm.excelUpload).then(successCallback, errorCallback);
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageExcelUploadDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.searchLedgerList = [];
			vm.excelUpload.ledgerData = {};
		}
	};

	vm.searchExcelUpload = function() {
		if (vm.searchExcelUploadNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchExcelUploadNameText, companyId: vm.manageExcelUploadDataService.selectedCompanyId })
			.then(getExcelUploadDataSuccessCallBack, errorCallback);
		} else {
			vm.searchExcelUploadList = [];
			vm.excelUpload.excelUploadData = {};
		}
	};

	function getExcelUploadDataSuccessCallBack(response) {
		vm.excelUpload.excelUploadData = {};
		vm.searchExcelUploadList = response.data.data;
	}

	vm.selectExcelUpload = function(ledgerObj) {
		vm.searchExcelUploadNameText = ledgerObj.ledgerName;
		vm.excelUpload.excelUploadData = {};
		vm.excelUpload.excelUploadData.ledgerId = ledgerObj.ledgerId;
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.excelUpload.ledgerData = {};
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		vm.excelUpload.ledgerData = {};
		vm.excelUpload.ledgerData.ledgerId = ledgerObj.ledgerId;
	};

	vm.openImageModel = function(index) {
		if (index < 0 || index > (vm.excelUpload.otherDocImageURLList - 1)) {
			return;
		}

		vm.excelUploadImageIndex = index;
		$("#excelUpload-image-show-model .modal").modal('show');
		vm.selectedPhotoURL = vm.excelUpload.otherDocImageURLList[index].documentUrl;
	};

	vm.fetchImage = function(position) {
		vm.excelUploadImageIndex += position;

		if (vm.excelUploadImageIndex < 0) {
			vm.excelUploadImageIndex = vm.excelUpload.otherDocImageURLList.length - 1;
		} else if (vm.excelUploadImageIndex >= vm.excelUpload.otherDocImageURLList.length) {
			vm.excelUploadImageIndex = 0;
		}
		vm.selectedPhotoURL = vm.excelUpload.otherDocImageURLList[vm.excelUploadImageIndex].documentUrl;
	};

	vm.deleteImage = function(index) {
		vm.excelUpload.otherDocImageURLList.splice(index, 1);
	};

});