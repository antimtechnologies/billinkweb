var manageExcelUploadDataApp = angular.module('manageExcelUpload', ['infiniteScroll', 'infinite-scroll'])

manageExcelUploadDataApp.service('manageExcelUploadDataService', function($http){
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfExcelUpload			= 0;
	svc.noOfRecordForExcelUpload		= 12;
	svc.totalExcelUploadCount 		= 0;

	svc.getCompanyExcelUploadList = function() {
		$http.post('../../excelUploadData/getExcelDocumentList', {companyId: svc.selectedCompanyId, start: svc.startOfExcelUpload, noOfRecord: svc.noOfRecordForExcelUpload }).
		then(svc.successCallback, svc.errorCallback);
	};

	svc.successCallback = function(response) {
		if (response.data.status === "success") {
			if (svc.startOfExcelUpload === 0) {
				svc.totalExcelUploadCount = response.data.totalCount;
				svc.excelUploadList = response.data.data;
				return;
			}
			svc.excelUploadList = svc.excelUploadList.concat(response.data.data);
		}
	};

	svc.errorCallback = function() {
		svc.excelUploadList = [];
	};

});

manageExcelUploadDataApp.controller('manageExcelUploadController', function($http, $ocLazyLoad, manageExcelUploadDataService, billInkDashboardService) {
	var vm 							= this;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.manageExcelUploadDataService	= manageExcelUploadDataService;
	vm.manageExcelUploadDataService.excelUploadContainerURL = "../excelupload/view/viewExcelUploadList.html";
	vm.billInkDashboardService		= billInkDashboardService;
	vm.sampleDocURL					= "../excelupload/sample_document.xlsx";
	vm.init = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};

	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
		}
	};

	vm.loadMoreExcelUpload = function() {
		if (vm.manageExcelUploadDataService.totalExcelUploadCount > vm.manageExcelUploadDataService.startOfExcelUpload) {			
			vm.manageExcelUploadDataService.startOfExcelUpload += vm.manageExcelUploadDataService.noOfRecordForExcelUpload;
			vm.manageExcelUploadDataService.getCompanyExcelUploadList();
		}
	};

	vm.getCompayExcelUploadDetail = function(companyId) {
		vm.manageExcelUploadDataService.excelUploadContainerURL = "../excelupload/view/viewExcelUploadList.html";
		vm.manageExcelUploadDataService.selectedCompanyId = companyId;
		vm.manageExcelUploadDataService.startOfExcelUpload = 0;
		vm.manageExcelUploadDataService.getCompanyExcelUploadList();
	}

	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					vm.manageExcelUploadDataService.selectedCompanyId = vm.companyList[0].companyId;
					vm.manageExcelUploadDataService.startOfExcelUpload = 0;
					vm.manageExcelUploadDataService.getCompanyExcelUploadList();
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
		}
	}

	function errorCallback() {
		
	}

	vm.addExcelUploadData = function() {
		vm.manageExcelUploadDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../excelupload/js/addEditExcelUpload.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageExcelUploadDataService.excelUploadContainerURL = "../excelupload/view/addEditExcelUpload.html";
		});
	};

	vm.showDeletePrompt = function(excelUploadId) {
		vm.selectedDeleteExcelUploadId = excelUploadId;
		$("#excelUpload-delete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteExcelUploadData = function() {
		$http.post('../../excelUploadData/deleteExcelDocumentData', {excelUploadId: vm.selectedDeleteExcelUploadId, companyId : vm.manageExcelUploadDataService.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId }).
		then(deleteExcelUploadSuccessCallback, errorCallback);	
	};

	function deleteExcelUploadSuccessCallback() {
		vm.closeDeleteModal();
		vm.getCompayExcelUploadDetail(vm.manageExcelUploadDataService.selectedCompanyId);
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteItemId = 0;
		$("#excelUpload-delete-confirmation-dialog .modal").modal('hide');
	};

	vm.editExcelUploadData = function(excelUploadId) {
		vm.manageExcelUploadDataService.requestType = "Edit";
		vm.manageExcelUploadDataService.editExcelUploadId = excelUploadId;
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../excelupload/js/addEditExcelUpload.js"],
				{serie: true}
		)
		.then(function () {
			vm.manageExcelUploadDataService.excelUploadContainerURL = "../excelupload/view/addEditExcelUpload.html";
		});
	};

});

