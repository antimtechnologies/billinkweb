angular.module('manageTaxCode', ['infinite-scroll', 'ui.bootstrap'])
.service("manageTaxCodeService", function($http) {
	
	var vm = this;
	vm.taxCodeList = [];
	vm.getTaxCodeList = function() {
		$http.post('../../taxcodedata/getTaxCodeList', {}).then(successCallback, errorCallback);	
	};

	function successCallback(response) {
		vm.taxCodeList = angular.copy(response.data.data);
	}

	function errorCallback() {

	}

})
.controller('manageTaxCodeController', function($http, $ocLazyLoad, manageTaxCodeService, billInkDashboardService) {
	var vm = this;
	vm.manageTaxCodeService = manageTaxCodeService;
	vm.manageTaxCodeService.currentViewUrl = "../taxcode/view/viewTaxCodeList.html";
	vm.billInkDashboardService = billInkDashboardService;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.taxCodeData = {};

	vm.init = function() {
		vm.manageTaxCodeService.taxCodeList = [];
		vm.manageTaxCodeService.getTaxCodeList();
	};
	
	vm.deleteTaxCode = function(taxCodeId) {
		vm.selectedDeleteTaxCodeId = taxCodeId;
		$("#taxCode-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteTaxCodeData = function() {
		$http.post('../../taxcodedata/deleteTaxCodeData', {taxCodeId: vm.selectedDeleteTaxCodeId, loggedInUserId: vm.billInkDashboardService.userId }).then(deleteTaxCodeSuccessCallback, errorCallback);	
	}

	function deleteTaxCodeSuccessCallback() {
		vm.closeDeleteModal();
		vm.manageTaxCodeService.getTaxCodeList();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteTaxCodeId = 0;
		$("#taxCode-delelete-confirmation-dialog .modal").modal('hide');
	};

	vm.addTaxCodeData = function() {
		vm.taxCodeData = {};
		$("#taxCode-add-confirmation-dialog .modal").modal('show');
	};

	vm.saveTaxCodeData = function() {
		$http.post('../../taxcodedata/saveTaxCodeData', vm.taxCodeData).then(saveTaxCodeSuccessCallback, errorCallback);	
	}

	function saveTaxCodeSuccessCallback() {
		vm.closeAddModal();
		vm.manageTaxCodeService.getTaxCodeList();
	}

	vm.closeAddModal = function() {
		vm.taxCodeData = {};
		$("#taxCode-add-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
		
	}
	vm.$onDestroy = function() {		
		vm.taxCodeData = {};
		vm.manageTaxCodeService.totalCount = 0;
		vm.manageTaxCodeService.taxCodeList = [];
	};

});

