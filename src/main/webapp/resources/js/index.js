var app = angular.module('login', ['toaster']);

app.controller('loginController', function($http, $window, toaster) {
	var vm = this;

	vm.login = function() {
		
		if (!vm.billinkusername || vm.billinkusername === "") {
			vm.errorMessage = "Enter user name.";
			return false;
		} 

		if (!vm.billinkpassword || vm.billinkpassword === "") {
			vm.errorMessage = "Enter password.";
			return false;
		}

		var data = {
				userName : vm.billinkusername,
				password : vm.billinkpassword
		};

		$http.post('userData/validateUserCredential', data).then(
				successCallback, errorCallback);
		
		return true;
	}

	function successCallback(response) {
		
		if (response.data.status === "success") {
			$window.location.href = 'resources/dashboard/billInkDashboard.html';
		} else {
			vm.errorMessage = "Not able to validate user credential data.";
		}
		return true;
	}

	function errorCallback(response) {
		vm.errorMessage = response.data.errMsg;
		return false;
	}

});
