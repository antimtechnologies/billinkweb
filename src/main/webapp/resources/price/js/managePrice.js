angular
		.module('managePrice', [ 'infinite-scroll', 'ui.bootstrap', 'toaster','toaster'])
		.service(
				"managePriceService",
				function($http,toaster) {

					var vm = this;
					vm.customerDiscountListObj = [];
					vm.startOfCompany=0;
					vm.selectedCompanyId=0;
					const noOfRecord=30;
					vm.getCustomerDiscountList = function() {
						$http.post('../../pricemaster/getCustomerPriceList', {start: vm.startOfCompany, noOfRecord: noOfRecord,companyId:vm.selectedCompanyId})
						.then(successCallbackCompanyDiscountList, errorCallbackCustomerDiscountList);	
					};

					function successCallbackCompanyDiscountList(response) {
						vm.customerDiscountListObj = angular.copy(response.data.data);
						
					}

					function errorCallbackCustomerDiscountList(response) {
						toaster.showMessage('error', '', response.data.errMsg);
						
					}
					vm.getCustomerTypeList = [];
					

					vm.getcustomerlist = function() {
					
						$http.post('../../customerTypeData/getCustomerTypeListView', {companyId:vm.selectedCompanyId,start: vm.startOfCompany, noOfRecord: noOfRecord})
						.then(successCallback, errorCallback);
					};

					vm.savePriceMaster = function(discount,customerTypeId){
						$http.post('../../pricemaster/savepricedata', {discount: discount,customerTypeId:customerTypeId,isDeleted:0,companyId:vm.selectedCompanyId}).then(addpriceSuccessCallback, errorCallback);
					}
					function addpriceSuccessCallback(response) {
						if (response.data.status === "Success") {
							toaster.showMessage('success', '', 'data saved/updated.');
							
							vm.getCustomerDiscountList();
						//	vm.closeEditModal();
						//	vm.manageBrandService.getBrandList();
						} else {
							toaster.showMessage('error', '', response.data.errMsg);
							
						}
					}
					function successCallback(response) {
						vm.getCustomerTypeList = response.data.result;
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
		})
		.controller(
				'managePriceController',
				function($http, $ocLazyLoad, managePriceService,
						billInkDashboardService,toaster) {
					var vm = this;
					
//					vm.maxDate = moment().format('YYYY-MM-DD');
//					let date = moment().format('MM/DD/YYYY');
//					vm.toDate = new Date(date);
//					vm.fromDate = new Date(date);
					vm.billInkDashboardService=billInkDashboardService;
					vm.managePriceService = managePriceService;
					vm.managePriceService.customerDiscountListObj=[];
					vm.startOfCompany=0;
					const noOfRecord=30;

					vm.addPriceMasterDiscount = function(){
						$("#pricemaster-add-confirmation-dialog .modal").modal('show');
					 }
					vm.closepricemasterModal = function(){
						$("#pricemaster-add-confirmation-dialog .modal").modal('hide');
					}
					
					vm.savePriceMasterData = function(){
						vm.customerTypeId = $( "#customerTypeId option:selected" ).val();
						   discount = vm.priceMaster.discount;
						
					vm.managePriceService.savePriceMaster(discount,vm.customerTypeId);
					$("#pricemaster-add-confirmation-dialog .modal").modal('hide');
						
					}
					vm.init = function() {
						vm.flag = 0;
						vm.managePriceService.getCustomerDiscountList();
						vm.getCompanyList();
						
						
					};
					
					
					vm.getCompayItemDetail = function(companyId) {
						vm.selectedCompanyId = companyId;
						vm.managePriceService.selectedCompanyId = companyId;
						vm.managePriceService.startOfCompany = 0;
						vm.managePriceService.customerDiscountListObj = [];
						vm.managePriceService.getcustomerlist();
						vm.managePriceService.getCustomerDiscountList();
					};
					

					function getCompanyDataSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompany === 0) {
								vm.companyList = response.data.data;
								vm.totalCount = response.data.totalCount;
								if (vm.companyList.length > 0) {
									vm.managePriceService.selectedCompanyId = vm.companyList[0].companyId;
									vm.selectedCompanyId = vm.companyList[0].companyId;
									vm.managePriceService.getcustomerlist();
									vm.managePriceService.getCustomerDiscountList();
								}
								return;
							}
							vm.companyList = vm.companyList.concat(response.data.data);
						}
					}
					
					
					vm.getCompanyList = function() {
						$http.post('../../companydata/getCompanyNameIdList', {start: vm.managePriceService.startOfCompany, noOfRecord: noOfRecord, companyName: vm.searchCompanyNameText })
						.then(getCompanyDataSuccessCallBack, errorCallback);
					};
					
					function errorCallback() {
						
					}

					vm.loadMoreCompanyList = function() {
						if (vm.totalCount > vm.startOfCompany) {			
							vm.startOfCompany += noOfRecordForCompany;
							vm.getCompanyList();
							
						}
					};
					
					vm.getCompanyListOnSearch = function() {
						vm.startOfCompany = 0;
						vm.getCompanyList();
					};
					
					
					function deletesuccesscallback(){
						toaster.showMessage('success', '',"Delete SuccessFully");
						vm.managePriceService.getcustomerlist();
						vm.managePriceService.getCustomerDiscountList();
					}
					
					function deleteerrorCallback(){
						toaster.showMessage('error', '',"Not Deleted.Please try again after sometime." );
					}
					
					vm.deleteprice=function (deleteid){
						$http.post('../../pricemaster/deleteCustomerPrice', {priceId:deleteid,companyId : vm.managePriceService.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
						.then(deletesuccesscallback, deleteerrorCallback);
						
					}

});



