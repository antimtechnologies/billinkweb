/**
 * 
 */
angular.module('manageUser', ['infinite-scroll'])
.controller('manageUserDataController', function($http, $ocLazyLoad,$modalinstance) {
	var vm = this;
	
	vm.oldPassword;
	vm.newPassword;
	vm.userName;
	
	vm.changePassword = function ()
	{
		var data = {
				userName: vm.userName,
				oldPassword: vm.oldPassword,
				newPassword: vm.newPassword
		};
		$http.post('../../userData/changeUserPassword', data).then(
				successCallback, errorCallback);
	}
	
	vm.close = funcction ()
	{
		$modalinstance.close();
	}
});
