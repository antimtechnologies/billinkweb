angular.module('manageUser', ['infinite-scroll'])
.service("manageUserDataService", function($http) {
	
	var vm = this;
	vm.startOfUser = 0;
	vm.noOfRecordForUser = 12;
	vm.totalCount = 0;
	vm.userList = [];
	vm.getUserList = function() {
		$http.post('../../userData/getUserDetails', {
						start: vm.startOfUser, 
						noOfRecord: vm.noOfRecordForUser,
						userName: vm.searchUserNameText
					}).then(successCallback, errorCallback);
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfUser === 0) {
				vm.userList = angular.copy(response.data.data);
				vm.totalCount = response.data.totalCount;
				return;
			}
			vm.userList = vm.userList.concat(response.data.data);
		}
	}

	function errorCallback() {

	}

})
.controller('manageUserDataController', function($http, $ocLazyLoad, manageUserDataService, billInkDashboardService,toaster) {
	var vm = this;
	vm.manageUserDataService = manageUserDataService;
	vm.manageUserDataService.currentViewUrl = "../user/view/viewUserList.html";
	vm.billInkDashboardService = billInkDashboardService;
	vm.init = function() {
		vm.manageUserDataService.startOfUser = 0;
		vm.manageUserDataService.userList = [];
		vm.manageUserDataService.getUserList();
	};

	vm.loadMoreUser = function() {
		if (vm.manageUserDataService.totalCount > vm.manageUserDataService.startOfUser) {
			vm.manageUserDataService.startOfUser +=  vm.manageUserDataService.noOfRecordForUser;
			vm.manageUserDataService.getUserList();
		}
	};
	
	
	vm.searchUserData = function() {
				vm.manageUserDataService.startOfUser = 0;
				vm.manageUserDataService.userList = [];
				vm.manageUserDataService.getUserList();		
			};
		

	vm.showAddUserDataForm = function() {
		vm.manageUserDataService.requestType = "Add";
		vm.manageUserDataService.startOfUser = 0;
		vm.manageUserDataService.userList = [];
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../user/js/addEditUserData.js"])
		.then(function () {
			console.log(vm.manageUserDataService.currentViewUrl);
			vm.manageUserDataService.currentViewUrl = "../user/view/addEditUserData.html";
			console.log(vm.manageUserDataService.currentViewUrl);
		});
	};

	vm.showDeletePrompt = function(userId) {
		vm.selectedDeleteUserId = userId;
		$("#user-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteUserData = function() {
		$http.post('../../userData/deleteUserData', {userId: vm.selectedDeleteUserId, loggedInUserId: vm.billInkDashboardService.userId }).then(deleteUserSuccessCallback, errorCallback);	
	}

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteUserId = 0;
		$("#user-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
		
	}

	vm.editUserData = function(userId) {
		vm.manageUserDataService.requestType = "Edit";
		vm.manageUserDataService.editUserId = userId;
		vm.manageUserDataService.startOfUser = 0;
		vm.manageUserDataService.userList = [];

		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../user/js/addEditUserData.js"])
		.then(function () {
			console.log(vm.manageUserDataService.currentViewUrl);
			vm.manageUserDataService.currentViewUrl = "../user/view/addEditUserData.html";
			console.log(vm.manageUserDataService.currentViewUrl);
		});
	};

	vm.$onDestroy = function() {
		vm.manageUserDataService.editUserId = 0;
		vm.manageUserDataService.startOfUser = 0;
		vm.manageUserDataService.userList = [];
	};

});
