angular.module('editUserProfile', ['fileUploadDirective', 'toaster'])
.controller('editUserProfileController', function($window, $http, fileUploadService, billInkDashboardService, toaster) {
	var vm = this;
	vm.manageUserDataService
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.user = {};
	vm.user.companyList = [];
	vm.vendorDesignationList = ['Proprietor', 'Partner', 'Director', 'Managing Director', 'Member', 'Accountant', 'Admin'];
	vm.billInkDashboardService = billInkDashboardService;
	vm.user.userDocumentList = [];
	vm.init = function() {
		vm.user.userDocumentList = [];
		$http.post('../../stateData/getStateList', vm.user).then(vm.setStateList, errorFetchStateList);

		$http.post('../../userData/getUserDetailsById', {userId: billInkDashboardService.userId}).then(vm.setUserDetail, vm.errorFetchData);
	};

	function errorFetchStateList(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}

	vm.setUserDetail = function(response) {
		if (response.data.status === "success") {
			vm.user = angular.copy(response.data.data);
			
			if (vm.user.userLicenceExpiryDate && vm.user.userLicenceExpiryDate != "") {			
				vm.user.userLicenceExpiryDateObj = new Date(vm.user.userLicenceExpiryDate);
			}
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	};

	vm.errorFetchData = function(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	};

	vm.setStateList = function(response) {
		vm.stateList = response.data.data;
	};

	function successCallback(response) {
		if (response.data.status === "success") {
//			$window.location.reload();
			toaster.showMessage('success', '', 'User profile updated');
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	function changePasswordSuccessCallback(response) {
		if (response.data.status === "success") {
			if (response.data.updateCount == 1) {				
				toaster.showMessage('success', '', 'Password changed');
				vm.closeChangePasswordModal();
			} else {
				toaster.showMessage('success', '', 'Invalid old password.');
			}
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	function errorCallback() {
		toaster.showMessage('error', '', 'Something went wrong please try again');
	}

	vm.updateUser = function() {
		if (vm.user.userLicenceExpiryDateObj) {	
			vm.user.userLicenceExpiryDate = moment(vm.user.userLicenceExpiryDateObj).format("YYYY-MM-DD");
		}
		$http.post('../../userData/updateUserData', vm.user).then(successCallback, errorCallback);
	};

	vm.uploadFileAndUpdateUserData = function() {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'user').then(function(result){
				if (result.data.status === "success") {		
					vm.user.profilePhotoURL = result.data.imagePath;
					vm.updateUser();
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.updateUser();
		}
	};
	
	vm.showChangePasswordPrompt = function() {
		$("#user-change-password-dialog .modal").modal('show');
	};
	
	vm.closeChangePasswordModal = function() {
		$("#user-change-password-dialog .modal").modal('hide');
	};
	
	vm.changePassword = function() {
		
		if (vm.newPassword !== vm.confirmNewPassword) {
			toaster.showMessage('error', '', 'new password and confirm password does not match.');
			return false;
		}

		$http.post('../../userData/changeUserPassword', {
			userName:vm.user.userName,
			userId:vm.userId,
			oldPassword:vm.oldPassword,
			newPassword:vm.newPassword,
			confirmNewPassword:vm.confirmNewPassword
		}).then(changePasswordSuccessCallback, errorCallback);
	};


	vm.addDocument = function() {
		angular.element("input[type='file']").val(null);
		vm.userDocumentObj = {};
		vm.userDocumentFile = "";
		$("#add-document-dialog .modal").modal('show');
	};

	vm.closedDocumentModal = function() {
		$("#add-document-dialog .modal").modal('hide');
	};

	vm.saveDocument = function() {
		var file = vm.userDocumentFile;
		if (vm.userDocumentFile) {			
			fileUploadService.uploadFileToUrl(file, 'user').then(function(result){
				if (result.data.status === "success") {				
					vm.userDocumentObj.documentUrl = result.data.imagePath;
					vm.user.userDocumentList.push(vm.userDocumentObj);
				}
				vm.userDocumentObj = {};
				vm.userDocumentFile = undefined;
				$("#add-document-dialog .modal").modal('hide');
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading file.');
			});
		}
	};

	vm.deleteDocument = function(index) {
		vm.user.userDocumentList.splice(index, 1);
	};

});