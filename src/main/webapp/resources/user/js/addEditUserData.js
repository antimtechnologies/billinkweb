angular.module('addUserData', ['toaster'])
.controller('addUserDataController', function($http, fileUploadService, manageUserDataService, toaster, billInkDashboardService) {
	var vm = this;
	vm.manageUserDataService = manageUserDataService;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.user = {};
	vm.user.state = {};
	vm.user.companyList = [];
	vm.vendorDesignationList = ['Proprietor', 'Partner', 'Director', 'Managing Director', 'Member', 'Accountant', 'Admin'];
	vm.billInkDashboardService = billInkDashboardService;

	vm.init = function() {
		$http.post('../../stateData/getStateList', vm.user).then(vm.setStateList, vm.errorFetchList);
		$http.post('../../roleData/getRoleDetails', vm.user).then(vm.setRoleList, vm.errorFetchList);

		$http.post('../../menuData/getMenuDetails', {})
		.then(function(response) {
			vm.menuList = response.data.data;
		}, function(response) {
		});
		
		if (vm.manageUserDataService.requestType === "Edit") {
			$http.post('../../userData/getUserDetailsById', { userId: vm.manageUserDataService.editUserId}).then(vm.setUserDetail, vm.errorFetchData);
		} else {
			vm.user = {};
			vm.user.addedBy = {};
			vm.user.addedBy.userId = vm.billInkDashboardService.userId;
		}
	};

	vm.errorFetchList = function(response) {
		toaster.showMessage('error', "", response.data.errMsg);
	};

	vm.setRoleList = function(response) {
		vm.roleList = response.data.data;
	};

	vm.setUserDetail = function(response) {
		
		if (response.data.status === "success") {			
			vm.user = angular.copy(response.data.data);
			
			if (vm.user.userLicenceExpiryDate && vm.user.userLicenceExpiryDate != "") {
				vm.user.userLicenceExpiryDateObj = new Date(vm.user.userLicenceExpiryDate);
			}
		} else {
			toaster.showMessage('error', "", "Error while fetching user details.");
		}
	};

	vm.errorFetchData = function(response) {
		toaster.showMessage('error', "", response.data.errMsg);
	};

	vm.setStateList = function(response) {
		vm.stateList = response.data.data;
	};

	vm.setSeletedRole = function() {
		vm.user.role = vm.roleList[vm.SeletedRole];
	};

	vm.addUser = function() {
		if (!vm.user.role || !vm.user.role.roleId || vm.user.role.roleId === 0) {
			toaster.showMessage('error', "", 'Kindly select user role.');
			return false;
		}

		if (!vm.user.userName || vm.user.userName === "") {
			toaster.showMessage('error', "", 'Kindly enter user name.');
			return false;
		}
		
		if (!vm.user.state || !vm.user.state.stateId && vm.user.state.stateId == 0) {
			toaster.showMessage('error', "", 'Kindly select state.');
			return false;
		}
		
		if (!vm.user.password || vm.user.password === "") {
			toaster.showMessage('error', "", 'Kindly enter password.');
			return false;
		}

		if (vm.user.password !== vm.user.confirmPassword) {
			toaster.showMessage('error', "", 'password does not match.');
			return false;
		}

 		if (vm.user.userLicenceExpiryDateObj) {
			vm.user.userLicenceExpiryDate = moment(vm.user.userLicenceExpiryDateObj).format("YYYY-MM-DD");
		}
		$http.post('../../userData/saveUserData', vm.user).then(addUserSuccessCallback, errorCallback);
	};

	vm.closeAddEditUserScreen = function() {
		//vm.manageUserDataService.editUserId = 0;
		//vm.manageUserDataService.startOfUser = 0;
		//vm.manageUserDataService.requestType = "";
		vm.manageUserDataService.getUserList();
		vm.manageUserDataService.currentViewUrl = "../user/view/viewUserList.html";
	};

	function addUserSuccessCallback(response) {
		if (response.data.status === "success") {
			toaster.showMessage('success', "", "User added.");
			vm.closeAddEditUserScreen();
		} else {
			toaster.showMessage('error', "", response.data.errMsg);
		}
	}

	function updateUserSuccessCallback(response) {
		if (response.data.status === "success") {
			toaster.showMessage('success', "", "User updated");
			vm.closeAddEditUserScreen();
		} else {
			toaster.showMessage('error', "", response.data.errMsg);
		}
	}

	function errorCallback(response) {
		toaster.showMessage('error', "", response.data.errMsg);
	}

	vm.uploadFileAndSaveUserData = function () {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {
			fileUploadService.uploadFileToUrl(file, 'user').then(function(result){
				if (result.data.status === "success") {
					vm.user.profilePhotoURL = result.data.imagePath;
					vm.addUser();
				} else {
					toaster.showMessage('error', '', 'error while saving user data.');
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading file');
			});
		} else {
			vm.user.profilePhotoURL = null;
			vm.addUser();
		}
	};

	vm.updateUser = function() {
		
		if (!vm.user.role || !vm.user.role.roleId || vm.user.role.roleId === 0) {
			toaster.showMessage('error', "", 'Kindly select user role.');
			return false;
		}

		if (!vm.user.userName || vm.user.userName === "") {
			toaster.showMessage('error', "", 'Kindly enter user name.');
			return false;
		}
		
		if (!vm.user.state || !vm.user.state.stateId && vm.user.state.stateId == 0) {
			toaster.showMessage('error', "", 'Kindly select state.');
			return false;
		}

		if (vm.user.userLicenceExpiryDateObj) {
			vm.user.userLicenceExpiryDate = moment(vm.user.userLicenceExpiryDateObj).format("YYYY-MM-DD");
		}
		$http.post('../../userData/updateUserData', vm.user).then(updateUserSuccessCallback, errorCallback);
	};

	vm.uploadFileAndUpdateUserData = function() {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {
			fileUploadService.uploadFileToUrl(file, 'user').then(function(result){
				if (result.data.status === "success") {
					vm.user.profilePhotoURL = result.data.imagePath;
					vm.updateUser();
				} else {
					toaster.showMessage('error', '', 'error while updating user data.');
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading file');
			});
		} else {
			vm.updateUser();
		}
	};

	vm.searchCompany = function() {
		if (vm.searchCompanyNameText.length > 2) {
			$http.post('../../companydata/getCompanyNameIdList', {companyName: vm.searchCompanyNameText })
			.then(getCompanyDataSuccessCallBack, errorCallback);
		} else {
			vm.searchCompanyList = [];
		}
	};

	function getCompanyDataSuccessCallBack(response) {
		vm.searchCompanyList = response.data.data;
	}

	vm.selectCompany = function(companyObj) {
		vm.searchCompanyNameText = "";
		vm.searchCompanyList = [];
		if (vm.user.role.roleName === 'Vendor') {
			companyObj.userCompanyMenuList = {};
			companyObj.userCompanyMenuList = angular.copy(vm.menuList);
		}

		if (!vm.user.companyList) {
			vm.user.companyList = [];
		}

		for(var count = 0; count < vm.user.companyList.length; count++) {
			if (companyObj) {
				if (companyObj.companyId === vm.user.companyList[count].companyId) {
					toaster.showMessage('error', "", "Company already exist in list.");
					return false;
				}
			}
		} 
		vm.user.companyList.push(companyObj);
	};

	vm.deleteFromSeletedCompany = function(index) {
		vm.user.companyList.splice(index, 1);
	};

	vm.showChangePasswordPrompt = function() {
		$("#user-change-password-dialog .modal").modal('show');
	};

	vm.closeChangePasswordModal = function() {
		$("#user-change-password-dialog .modal").modal('hide');
	};

	vm.changePassword = function() {
		if (vm.newPassword !== vm.confirmNewPassword) {
			toaster.showMessage('error', "", "Password does not match.");
			return false;
		}

		$http.post('../../userData/resetUserPassword', {userName:vm.user.userName,userId:vm.userId, password:vm.newPassword})
		.then(function(response) {
			toaster.showMessage('success', "", "Password reset successfully.");
			vm.closeChangePasswordModal();
		}, function(response) {
			toaster.showMessage('error', "", "error while reseting password.");
		});
	};

});
