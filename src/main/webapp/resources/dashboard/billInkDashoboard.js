var app = angular.module('billInkDashboard', ['oc.lazyLoad', 'ui.bootstrap', 'toaster', 'ngMaterial', 'ngMessages'])

app.service(
		"dashBoardDataService",
		function($http) {

			var vm = this;
			vm.dataPassList = [ [ 1, 'day', 7 ], [ 2, 'day', 15 ],
					[ 3, 'day', 30 ], [ 4, 'month', 3 ], [ 5, 'month', 6 ],
					[ 6, 'month', 12 ] ];
			vm.dataList1 = [];
			vm.dataList2 = [];
			vm.dataList3 = [];
			vm.dataList4 = [];
			vm.dataList5 = [];
			vm.dataList6 = [];
			vm.dataList = [];
			vm.currentViewUrl = "";
			vm.flag = 0;

			vm.getInit = function(val) {
				$http.get(
						'../../dasboard/getalltotalamount?type='
								+ vm.dataPassList[val][1] + '&time_period='
								+ vm.dataPassList[val][2] + '&error_code='
								+ vm.dataPassList[val][0]).then(
						successCallback, errorCallback);
			};

			function successCallback(response) {
				if (response.data.error[0].errorcode == 1)
					vm.dataList1 = response.data.result;
				else if (response.data.error[0].errorcode == 2)
					vm.dataList2 = response.data.result;
				else if (response.data.error[0].errorcode == 3)
					vm.dataList3 = response.data.result;
				else if (response.data.error[0].errorcode == 4)
					vm.dataList4 = response.data.result;
				else if (response.data.error[0].errorcode == 5)
					vm.dataList5 = response.data.result;
				else if (response.data.error[0].errorcode == 6)
					vm.dataList6 = response.data.result;
				vm.dataList = [];
				vm.dataList.push(vm.dataList1);
				vm.dataList.push(vm.dataList2);
				vm.dataList.push(vm.dataList3);
				vm.dataList.push(vm.dataList4);
				vm.dataList.push(vm.dataList5);
				vm.dataList.push(vm.dataList6);
			}

			function errorCallback(response) {
				toaster.showMessage('error', '', response.data.errMsg);
				;
			}

		}).controller('dashBoardController',
		function($http, toaster, dashBoardDataService) {
			var vm = this;
			vm.dashBoardDataService = dashBoardDataService;

			vm.init = function() {
				vm.currentViewUrl = "dashboardCount.html";
				vm.flag = 0;
			};

			vm.change = function(val) {
				vm.flag = val;
				if(val==1){
					$("#Overview").removeClass("active");
					$("#Intelligence").addClass("active");
				}
				else{
					$("#Overview").addClass("active");
					$("#Intelligence").removeClass("active");
				}
			};

			vm.init1 = function(val) {
				vm.dashBoardDataService.getInit(val);
			};
		});

app.service('fileUploadService', function($q, $http, toaster) {
	var deffered = $q.defer();
	var responseData;
	var vm = this;

	vm.uploadFileToUrl = function(file, imageType) {

		var fd = new FormData();
		for (var count = 0; count < file.length; count++) {
			fd.append("file[]", file[count]);
		}

		fd.append('imageType', imageType);
		var uploadUrl = "../../uploadFile";
		return $http.post(uploadUrl, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		});
	}

	vm.getResponse = function() {
		return responseData;
	}
});

app.service('billInkDashboardService', function($ocLazyLoad, $http,
		fileUploadService, toaster) {
	var svc = this;
	svc.purchseItemId;
	svc.currentCompanyObj = {};
	svc.documentTypeList = [ 'Personal', 'Professional', 'Return File',
			'Others' ];
	svc.noTaxCodeGSTType = [ 'Import Nil/ Exempt', 'SEZ(Exempt)',
			'Export Nil/ Exempt', 'Embassy / UN Body',
			'Unregistered - Sundry Creditor - Sundry Purchase',
			'Composition -  Sundry Creditor - Sundry Purchase' ];

	svc.igstTaxCalGSTType = [ 'Import Taxable', 'Export Taxable', 'SEZ' ];

	svc.itemMeasureUnit = [ "BAGS", "BALE", "BUNDLES", "BUCKLES",
			"BILLION OF UNITS", "BOX", "BOTTLES", "BUNCHES", "CANS",
			"CUBIC METER", "CUBIC CENTIMETER", "CENTIMETER", "CARTONS",
			"DOZEN", "DRUM", "GREAT GROSS", "GRAMMES", "GROSS", "GROSS YARDS",
			"KILOGRAMS", "KILOLITRE", "KILOMETRE", "MILILITRE", "METERS",
			"NUMBERS", "PACKS", "PIECES", "PAIRS", "QUINTAL", "ROLLS", "SETS",
			"SQUARE FEET", "SQUARE METERS", "SQUARE YARDS", "TABLETS",
			"TEN GROSS", "THOUSANDS", "TONNES", "TUBES", "US GALLONS", "UNITS",
			"YARDS", "OTHERS" ];

	svc.ledgerGroupList = [ "Bank Accounts", "Bank OCC A/c", "Bank OD A/c",
			"Branch / Divisions", "Capital Account", "Cash-in-Hand",
			"Current Assets", "Current Liabilities", "Deposits (Asset)",
			"Direct Expenses", "Direct Incomes", "Duties & Taxes",
			"Fixed Assets", "Indirect Expenses", "Indirect Incomes",
			"Investments", "Loans & Advances (Asset)", "Loans (Liability)",
			"Misc. Expenses (ASSET)", "Provisions", "Purchase Accounts",
			"Reserves & Surplus", "Retained Earnings", "Sales Accounts",
			"Secured Loans", "Stock-in-Hand", "Sundry Creditors",
			"Sundry Debtors", "Suspense A/c", "Unsecured Loans" ];

	svc.gstTypeList = [ "Consumer", "Regular", "Import Nil/ Exempt",
			"Import Taxable", "Export Nil/ Exempt", "Export Taxable", "SEZ",
			"SEZ(Exempt)", "Unregistered - Sundry Creditor - Sundry Purchase",
			"Composition -  Sundry Creditor - Sundry Purchase",
			"Embassy / UN Body" ];

	svc.isPDFDocument = function(fileURL) {
		var ext = /^.+\.([^.]+)$/.exec(fileURL);
		ext = ext == null ? "" : ext[1];

		if (ext === "pdf" || ext === "PDF") {
			return true;
		}

		return false;
	};

	svc.loadMenuPage = function(menu) {

		svc.selectedMenu = {};
		$ocLazyLoad.load(menu.js, {
			serie : true
		}).then(function() {
			svc.selectedMenu = menu;

			if (svc.userRole !== "Vendor") {
				svc.currentCompanyObj = {};
				if (menu.adminEmployeeCurrentViewURL) {
					svc.currentViewUrl = menu.adminEmployeeCurrentViewURL;
				}
			} else {
				if (menu.vendorCurrentViewURL) {
					svc.currentViewUrl = menu.vendorCurrentViewURL;
				}
			}
		});
	};

	svc.downloadURI = function(uri) {
		var urlNameArray = uri.split("/");
		var name = urlNameArray[urlNameArray.length - 1];
		var link = document.createElement("a");
		link.download = name;
		link.href = uri;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
		delete link;
	};

	svc.errorFetchList = function(response) {

	};

	svc.launchAddLedgerModel = function() {
		var parameter = {
			companyId : svc.currentCompanyObj.companyId
		};
		$http.post('../../taxcodedata/getTaxCodeValueList', {}).then(
				svc.setTaxCodeList, svc.errorFetchList);
		$http.post('../../stateData/getStateList', {}).then(svc.setStateList,
				svc.errorFetchList);
//		$ocLazyLoad
//		.load(
//				["../ledger/js/addEditLedgerData.js"],
//				{serie: true}		
//		)
//		.then(function () {
			
			$("#add-ledger-section-model .modal").modal('show');
//		});
	};

	svc.setStateList = function(response) {
		svc.stateList = response.data.data;
	};

	svc.closeLedgerDataSave = function() {
		$("#add-ledger-section-model .modal").modal('hide');
		svc.taxCodeList = [];
		svc.ledger = {};
	};

	svc.addLedger = function() {
		if (!svc.ledger.ledgerName || svc.ledger.ledgerName === "") {
			toaster.showMessage('error', '', 'Kindly enter ledger name.');
			return false;
		}

		svc.ledger.addedBy = {};
		svc.ledger.companyId = svc.currentCompanyObj.companyId;
		svc.ledger.addedBy.userId = svc.userId;
		$http.post('../../ledgerData/saveLedgerData', svc.ledger).then(
				successLedgerCallback, svc.errorFetchList);
	};

	// Buyer Save Api call Start
	svc.buyer = {};
	svc.addBuyer = function() {
		if (!svc.buyer.buyerName || svc.buyer.buyerName === "") {
			toaster.showMessage('error', '', 'Kindly enter Buyer name.');
			return false;
		}
		svc.buyer.stateId = 1;
		 var buyerVal = document.getElementById("addBuyerId").value;
		console.log("buyerVal :"+buyerVal);
		if(buyerVal == "save"){
			$http.post('../../buyerData/saveBuyerData', svc.buyer).then(
					successBuyerCallback, svc.errorFetchList);
		}else{
			console.log("edit byuer call");
			$http.post('../../buyerData/updateBuyerInfoData', svc.buyer).then(
					successBuyerCallback, svc.errorFetchList);
		}
		
	};
	
	svc.closeBuyerDataSave = function() {
		$("#add-buyer-section-model .modal").modal('hide');
//		svc.buyer = {};
	};
	
	function successBuyerCallback() {
		svc.closeBuyerDataSave();
		
		console.log("successFully Add Buyer...");
	}
	
	svc.errorFetchList = function(response) {

	};
//Buyer save Api call End	 add-buyer-section-model
	
	
	
	svc.uploadFileAndSaveLedgerData = function() {
		var file = svc.profilePhotoFile;

		if (svc.profilePhotoFile) {
			fileUploadService.uploadFileToUrl(file, 'ledger').then(
					function(result) {
						if (result.data.status === "success") {
							svc.ledger.profilePhotoURL = result.data.imagePath;
							svc.addLedger();
						} else {
							toaster.showMessage('error', '',
									'error while uploading document');
						}
					},
					function(error) {
						toaster.showMessage('error', '',
								'error while uploading document');
					});
		} else {
			svc.ledger.profilePhotoURL = null;
			svc.addLedger();
		}
	};

	function successLedgerCallback() {
		svc.closeLedgerDataSave();
	}

	svc.launchAddItemModel = function() {
		var parameter = {
			companyId : svc.currentCompanyObj.companyId
		};
		$http.post('../../categorydata/getItemCategoryList', parameter).then(
				svc.setCategoryList, svc.errorFetchList);
		$http.post('../../branddata/getItemBrandList', parameter).then(
				svc.setBrandList, svc.errorFetchList);
		$http.post('../../taxcodedata/getTaxCodeValueList', {}).then(
				svc.setTaxCodeList, svc.errorFetchList);
		$("#add-item-section-model .modal").modal('show');
	}

	
	
	svc.batchAddItemModel = function(index) {
		console.log("call this index :"+index);
		var parameter = {
			companyId : svc.currentCompanyObj.companyId
		};
		$("#add-batch-purchase-dialog .modal").modal('show');
	}
	
	
	
	svc.setCategoryList = function(response) {
		svc.categoryList = response.data.data;
	};

	svc.setBrandList = function(response) {
		svc.brandList = response.data.data;
	};

	svc.setTaxCodeList = function(response) {
		svc.taxCodeList = response.data.data;
	};

	svc.closeItemDataSave = function() {
		$("#add-item-section-model .modal").modal('hide');
		svc.categoryList = [];
		svc.brandList = [];
		svc.taxCodeList = [];
		svc.item = {};
	};

	svc.addItem = function() {
		if (!svc.item.itemName || svc.item.itemName === "") {
			toaster.showMessage('error', '', 'Kindly enter item name.');
			return false;
		}

		
		svc.item.addedBy = {};
		svc.item.companyId = svc.currentCompanyObj.companyId;
		svc.item.addedBy.userId = svc.userId;
		$http.post('../../itemData/saveItemData', svc.item).then(
				successCallback, svc.errorFetchList);
	};

	svc.uploadFileAndSaveItemData = function() {
		var file = svc.profilePhotoFile;

		if (svc.profilePhotoFile) {
			fileUploadService.uploadFileToUrl(file, 'item').then(
					function(result) {
						if (result.data.status === "success") {
							svc.item.profilePhotoURL = result.data.imagePath;
							svc.addItem();
						} else {
							toaster.showMessage('error', '',
									'error while uploading document');
						}
					},
					function(error) {
						toaster.showMessage('error', '',
								'error while uploading document');
					});
		} else {
			svc.item.profilePhotoURL = null;
			svc.addItem();
		}
	};

	function successCallback() {
		svc.closeItemDataSave();
	}

});

app.config(function($httpProvider) {

	$httpProvider.interceptors.push(function($q, $window) {
		return {
			'request' : function(config) {
				$('#processing').show();
				return config;
			},
			'response' : function(response) {
				$('#processing').hide();
				return response;
			},
			'responseError' : function(response) {
				$('#processing').hide();
				switch (response.status) {
				case 403:
					$window.location = '../../../'+window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
					break;
				}
				return response;
			}
		};
	});
});

app
		.controller(
				'billInkDashboardController',
				function($http, $window, $modal, toaster,
						billInkDashboardService) {
					var vm = this;
					vm.purchase = {};
					vm.sell = {};
					var menuItems = [
							{
								iconImgURL : "../images/purchase.png",
								menuName : "DashBoard",
								url : "",
								adminEmployeeCurrentViewURL : ""
							},
							{
								iconImgURL : "../images/purchase.png",
								menuName : "Purchase",
								url : "../purchase/view/managePurchase.html",
								js : "../purchase/js/managePurchase.js",
								vendorCurrentViewURL : "../purchase/view/viewPurchaseList.html",
								adminEmployeeCurrentViewURL : "../purchase/view/companyWisePurchaseCount.html"
							},
							{
								iconImgURL : "../images/sales.png",
								menuName : "Sale",
								url : "../sell/view/manageSell.html",
								js : "../sell/js/manageSell.js",
								vendorCurrentViewURL : "../sell/view/viewSellList.html",
								adminEmployeeCurrentViewURL : "../sell/view/companyWiseSellCount.html"
							},
							{
								iconImgURL : "../images/purchase.png",
								menuName : "Report",
								url : "../report/view/manageReport.html",
								js : "../report/js/manageReport.js",
								vendorCurrentViewURL : "../report/view/manageReport.html",
								adminEmployeeCurrentViewURL : "../report/view/manageReport.html"
							},
							{
								iconImgURL : "../images/purchase_payment.png",
								menuName : "Purchase Payment",
								url : "../purchasepayment/view/managePurchasePayment.html",
								js : "../purchasepayment/js/managePurchasePayment.js",
								vendorCurrentViewURL : "../purchasepayment/view/viewPurchasePaymentList.html",
								adminEmployeeCurrentViewURL : "../purchasepayment/view/companyWisePurchasePaymentCount.html"
							},
							{
								iconImgURL : "../images/sales_receipt.png",
								menuName : "Sale Receipt",
								url : "../sellpayment/view/manageSellPayment.html",
								js : "../sellpayment/js/manageSellPayment.js",
								vendorCurrentViewURL : "../sellpayment/view/viewSellPaymentList.html",
								adminEmployeeCurrentViewURL : "../sellpayment/view/companyWiseSellPaymentCount.html"
							},
							{
								iconImgURL : "../images/debit_note.png",
								menuName : "Debit Note",
								url : "../debitnote/view/manageDebitNote.html",
								js : "../debitnote/js/manageDebitNote.js",
								vendorCurrentViewURL : "../debitnote/view/viewDebitNoteList.html",
								adminEmployeeCurrentViewURL : "../debitnote/view/companyWiseDebitNoteCount.html"
							},
							{
								iconImgURL : "../images/credit_note.png",
								menuName : "Credit Note",
								url : "../creditnote/view/manageCreditNote.html",
								js : "../creditnote/js/manageCreditNote.js",
								vendorCurrentViewURL : "../creditnote/view/viewCreditNoteList.html",
								adminEmployeeCurrentViewURL : "../creditnote/view/companyWiseCreditNoteCount.html"
							},
							{
								iconImgURL : "../images/expenses.png",
								menuName : "Expense",
								url : "../expense/view/manageExpense.html",
								js : "../expense/js/manageExpense.js",
								vendorCurrentViewURL : "../expense/view/viewExpenseList.html",
								adminEmployeeCurrentViewURL : "../expense/view/companyWiseExpenseCount.html"
							},
							{
								iconImgURL : "../images/manage_ledger.png",
								menuName : "Other Docs",
								url : "../otherdocs/view/manageOtherDocs.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../otherdocs/js/manageOtherDocs.js' ],
								vendorCurrentViewURL : "../otherdocs/view/viewOtherDocsList.html",
								adminEmployeeCurrentViewURL : "../otherdocs/view/viewOtherDocsList.html"
							},
							{
								iconImgURL : "../images/manage_ledger.png",
								menuName : "Excel Upload",
								url : "../excelupload/view/manageExcelUpload.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../excelupload/js/manageExcelUpload.js' ],
								vendorCurrentViewURL : "../excelupload/view/viewExcelUploadList.html",
								adminEmployeeCurrentViewURL : "../excelupload/view/viewExcelUploadList.html"
							},
							{
								iconImgURL : "../images/manage_company.png",
								menuName : "Manage User",
								url : "../user/view/manageUser.html",
								js : "../user/js/manageUser.js"
							},
							{
								iconImgURL : "../images/manage_company.png",
								menuName : "Manage Company",
								url : "../company/view/manageCompany.html",
								js : "../company/js/manageCompany.js"
							},
							{
								iconImgURL : "../images/manage_items.png",
								menuName : "Manage Item",
								url : "../item/view/manageItem.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../item/js/manageItem.js' ]
							},
							{
								iconImgURL : "../images/manage_ledger.png",
								menuName : "Manage Ledger",
								url : "../ledger/view/manageLedger.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../ledger/js/manageLedger.js' ]
							}, {
								iconImgURL : "../images/manage_items.png",
								menuName : "Manage TaxCode",
								url : "../taxcode/view/manageTaxCode.html",
								js : "../taxcode/js/manageTaxCode.js"
							}, {
								iconImgURL : "../images/manage_items.png",
								menuName : "Manage Category",
								url : "../category/view/manageCategory.html",
								js : "../category/js/manageCategory.js"
							}, {
								iconImgURL : "../images/manage_items.png",
								menuName : "Manage Brand",
								url : "../brand/view/manageBrand.html",
								js : "../brand/js/manageBrand.js"
							}, {
								iconImgURL : "../images/manage_items.png",
								menuName : "Price Master",
								url : "../price/view/managePrice.html",
								js : "../price/js/managePrice.js"
							},
							{
								iconImgURL : "../images/manage_items.png",
								menuName : "CutomerType",
								url : "../customerType/view/addCutomerType.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../customerType/js/addCutomerType.js' ]
							},
							{
								iconImgURL : "../images/manage_items.png",
								menuName : "Location Master",
								url : "../location/view/manageLocation.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../location/js/manageLocation.js' ]
							},{
								iconImgURL : "../images/manage_items.png",
								menuName : "Scheme Master",
								url : "../scheme/schemeOne/view/manageSchemeOne.html",
								js : [
										'../customdirective/infine-scroll-directive.js',
										'../scheme/schemeOne/js/manageSchemeOne.js' ]
							}
					// {menuName:"Manage DashBoard",
					// url:"../dashboard/view/manageDashBoard.html",
					// js:['../customdirective/infine-scroll-directive.js',
					// '../dashboard/js/manageDashBoard.js' ]},
					];

					vm.billInkDashboardService = billInkDashboardService;
					vm.displayMenuItems = [];

					vm.init = function() {
						$http.post('../../userData/getUserDetailsByUserName',
								{}).then(successCallback, errorCallback);
					}

					vm.logoutUser = function() {
						$http.post('../../userData/logOut', {}).then(
								logoutSuccessCallBack, logoutSuccessCallBack);
					};

					function logoutSuccessCallBack() {
						$window.location.href = '../../..'+window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));;
					}

					function getAllowedMenuListForCompany(companyId,
							userCompanyList) {
						for (var count = 0; count < userCompanyList.length; count++) {
							if (userCompanyList[count].companyId === companyId) {
								return userCompanyList[count].userCompanyMenuList;
							}
						}
					}

					function getCompanyObj(companyId, userCompanyList) {
						for (var count = 0; count < userCompanyList.length; count++) {
							if (userCompanyList[count].companyId === companyId) {
								return userCompanyList[count];
							}
						}
					}

					function successCallback(response) {
						if (response.data && response.data.status === "success") {
							var userData = angular.copy(response.data.data);
							var userCompanyList = userData.companyList;
							var allowedMenuList = [];
							
							
							
							vm.billInkDashboardService.user = angular
									.copy(userData);
							vm.billInkDashboardService.userName = userData.userName;
							vm.billInkDashboardService.userRole = userData.role.roleName;
							vm.billInkDashboardService.userId = userData.userId;
							vm.billInkDashboardService.companyId = userData.defaultCompany.companyId;
							
							if (vm.billInkDashboardService.userRole === "Vendor") {
								if (userData.defaultCompany.companyId) {
									vm.billInkDashboardService.selectedCompany = userData.defaultCompany;
									allowedMenuList = getAllowedMenuListForCompany(
											userData.defaultCompany.companyId,
											userCompanyList);
								} else {
									vm.billInkDashboardService.selectedCompany = userCompanyList[0];
									allowedMenuList = userCompanyList[0].userCompanyMenuList;
								}
								vm.billInkDashboardService.currentCompanyObj = getCompanyObj(
										vm.billInkDashboardService.selectedCompany.companyId,
										userCompanyList);
								vm.billInkDashboardService.selectedCompany = angular
										.copy(vm.billInkDashboardService.currentCompanyObj);
							}

							for ( var menuindex in menuItems) {

								if (vm.billInkDashboardService.userRole === "Admin") {
									menuItems[menuindex].display = true;
									vm.displayMenuItems
											.push(menuItems[menuindex]);
									continue;
								}

								if (vm.billInkDashboardService.userRole === "Employee"
										&& !(menuItems[menuindex].menuName === "Manage User" || menuItems[menuindex].menuName === "Manage Company")) {
									menuItems[menuindex].display = true;
									vm.displayMenuItems
											.push(menuItems[menuindex]);
									continue;
								}

//								if (menuItems[menuindex].menuName === "Manage Item"
//										|| menuItems[menuindex].menuName === "Manage Ledger"
//										|| menuItems[menuindex].menuName === "Manage Brand"
//										|| menuItems[menuindex].menuName === "Manage Category"
//											|| menuItems[menuindex].menuName === "Scheme Master"	
//												|| menuItems[menuindex].menuName === "CutomerType"
//												||	menuItems[menuindex].menuName === "Location Master" ||	menuItems[menuindex].menuName === "Price Master" ) {
//									menuItems[menuindex].display = true;
//									vm.displayMenuItems
//											.push(menuItems[menuindex]);
//									continue;
//								}

								for ( var alwMnuIndex in allowedMenuList) {
									if (allowedMenuList[alwMnuIndex].selected
											&& allowedMenuList[alwMnuIndex].menuName === menuItems[menuindex].menuName) {
										menuItems[menuindex].display = true;
										vm.displayMenuItems
												.push(menuItems[menuindex]);
									}
								}
							}
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					
					vm.saveBatchForPurchase = function() {
						console.log("vm.purchase.mfgDate :"+vm.purchase.mfgDate)
						if (!vm.purchase.batchNo || vm.purchase.batchNo === "") {
							toaster.showMessage('error', '', 'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.purchase.mfgDate || vm.purchase.mfgDate === "") {
							toaster.showMessage('error', '', 'Kindly enter Mfg Date.');
							return false;
						}
//						if (!vm.purchase.expDate || vm.purchase.expDate === "") {
//							toaster.showMessage('error', '', 'Kindly enter Exp Date.');
//							return false;
//						}
						
						if(document.getElementById("dayMonthDD").value == "Month"){
							 var date = new Date(vm.purchase.mfgDate);
							 var month=date.getMonth()+vm.purchase.shelfLifes;  
							 date.setMonth(month);
							 vm.purchase.expDate = moment(date).format("YYYY-MM-DD");
							 //vm.itemBatchObj.expDates = moment(date).format("MM-DD-YYYY");
						}
						else if(document.getElementById("dayMonthDD").value == "days"){
							 vm.purchase.expDate = moment(vm.purchase.mfgDate).format("YYYY-MM-DD");
						}
						
						vm.purchase.mfgDate = moment(vm.purchase.mfgDate).format("YYYY-MM-DD");
						//vm.purchase.expDate = moment(vm.purchase.expDate).format("YYYY-MM-DD");
						//vm.billInkDashboardService.item.push(vm.purchase);
						console.log("shelflife :"+vm.purchase.shelfLifes);
						vm.purchase.shelfLife = document.getElementById("dayMonthDD").value+"@"+vm.purchase.shelfLifes;
						
				
						
						$http.post('../../itemData/savebatchData', vm.purchase).then(successCallBatchback, errorCallback);
						$("#add-batch-purchase-dialog .modal").modal('hide');
					};
					
					//15-05-2019 Start
					vm.saveBatchForSell = function() {
						console.log("sell Function call :"+vm.sell.batchNo);
						if (!vm.sell.batchNo || vm.sell.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.sell.mfgDate || vm.sell.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.sell.mfgDate);
							var month = date.getMonth()
									+ vm.sell.shelfLifes;
							date.setMonth(month);
							vm.sell.expDate = moment(date).format(
									"YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							vm.sell.expDate = moment(vm.sell.mfgDate)
									.format("YYYY-MM-DD");
						}

						vm.sell.mfgDate = moment(vm.sell.mfgDate)
								.format("YYYY-MM-DD");
						// vm.purchase.expDate =
						// moment(vm.purchase.expDate).format("YYYY-MM-DD");
						// vm.billInkDashboardService.item.push(vm.purchase);
						//console.log("shelflife :" + vm.purchase.shelfLifes);
						vm.sell.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.sell.shelfLifes;
						console.log("itemID :"+vm.sell.itemId);
						vm.sell.itemId = 1;
						$http.post('../../itemData/savebatchData', vm.sell,{itemId:5})
								.then(successCallBatchback, errorCallback);
						$("#add-batch-sell-dialog .modal").modal('hide');
					};
					//15-05-2019 End
					
					
					vm.closedBatchModal = function(){
						$("#add-batch-purchase-dialog .modal").modal('hide');
					}
					vm.closedSellBatchModal = function(){
						$("#add-batch-sell-dialog .modal").modal('hide');
					}
					
					
					function successCallBatchback(){
						toaster.showMessage('success', '', 'data saved successfully.');
						$http.post('../../schemeData/getLastBatchItemDetails', {itemId: vm.purchase.purchaseItemModel[index].item.itemId})
						.then(function(response) {
							console.log("Sucess insert :"+response);
							vm.itemBatchNoList = [];
							vm.itemBatchNoList[index] = {};
							vm.itemBatchNoList[index].itemBatchData = response.data.data;
						}, errorCallback);
						
					}
					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.logout = function() {
						var data = {
							userName : vm.billinkusername,
							password : vm.billinkpassword
						};
						$http.post('../../userData/logout', data).then(
								successCallback, errorCallback);
					}

					vm.openCompanySelectionModal = function() {
						if (vm.billInkDashboardService.user
								&& vm.billInkDashboardService.user.companyList
								&& vm.billInkDashboardService.user.companyList.length > 1) {
							vm.userCompanyList = vm.billInkDashboardService.user.companyList;
							$("#user-company-selection-modal .modal").modal(
									'show');
						}
					};

					vm.updateCompany = function(companyObj) {
						var data = {
							companyId : companyObj.companyId
						};
						$http.post('../../userData/updateUserDefaultCompany',
								data).then(updateCompanySuccess,
								updateCompanyError);
					};

					function updateCompanySuccess(response) {
						$window.location.reload();
					}

					function updateCompanyError(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.openUserProfilePage = function() {
						billInkDashboardService
								.loadMenuPage({
									menuName : "User Profile",
									url : "../user/view/userProfile.html",
									js : [
											"../user/js/editUserProfileController.js",
											"../customdirective/fileUploadDirective.js" ]
								});
					};

				});


