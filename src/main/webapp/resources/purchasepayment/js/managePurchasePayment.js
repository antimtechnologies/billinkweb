angular.module('managePurchasePaymentData', ['infinite-scroll'])
.service("managePurchasePaymentDataService", function($http) {
	
	var vm = this;

	vm.purchasePaymentList = [];
	vm.startOfPurchasePayment = 0;
	vm.noOfRecordForPurchasePayment = 12;
	vm.purchasePaymentFilterData = {};
	vm.hasMorePurchasePaymentData = true;

	vm.companyCountRecordList = [];
	vm.startOfCompanyCount = 0;
	vm.noOfRecordForCompanyCount = 12;
	vm.hasMoreCompanyCountData = true;

	vm.getPurchasePaymentList = function(companyId) {
		$http.post('../../purchasePaymentData/getPurchasePaymentListData', {
			companyId: companyId, 
			start: vm.startOfPurchasePayment, 
			noOfRecord: vm.noOfRecordForPurchasePayment,
			filterData : vm.purchasePaymentFilterData
		}).then(successCallback, errorCallback);	
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfPurchasePayment === 0) {
				vm.purchasePaymentList = response.data.data;
				if (vm.purchasePaymentList.length < vm.noOfRecordForPurchasePayment) {
					vm.hasMorePurchasePaymentData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForPurchasePayment) {				
				vm.hasMorePurchasePaymentData = false;
			}

			vm.purchasePaymentList = vm.purchasePaymentList.concat(response.data.data);
		}
	}

	function errorCallback() {
			
	}

	vm.getCompanyCountData = function() {
		$http.post('../../companydata/getCompanyRequestCountDetails', {start: vm.startOfCompanyCount, noOfRecord: vm.noOfRecordForCompanyCount, sortParam: 'purchasePaymentPendingCount' }).
		then(companyCountSuccessCallBack, errorCallback);
	};

	function companyCountSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompanyCount === 0) {
				vm.companyCountRecordList = response.data.data;
				if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
					vm.hasMoreCompanyCountData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCompanyCount) {				
				vm.hasMoreCompanyCountData = false;
			}

			vm.companyCountRecordList = vm.companyCountRecordList.concat(response.data.data);
		}
	}

})
.controller('managePurchasePaymentDataController', function($http, $ocLazyLoad, managePurchasePaymentDataService, billInkDashboardService) {
	var vm = this;
	vm.backToCompanyWisePurchasePaymentData = function() {
			vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/companyWisePurchasePaymentCount.html";
			vm.managePurchasePaymentDataService.getCompanyCountData();
	};
	vm.managePurchasePaymentDataService = managePurchasePaymentDataService;
	vm.billInkDashboardService = billInkDashboardService;
	if (vm.billInkDashboardService.userRole !== "Vendor") {
		vm.statusList = ['New', 'Modified', 'Deleted'];
	} else {		
		vm.statusList = ['New', 'Modified', 'Deleted', 'UnPublished'];
	}

	vm.managePurchasePaymentDataService.purchasePaymentFilterData = {};
	vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList = [];

	vm.openPurchasePaymentList = function(companyObj) {

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.isVerified = 0;
		}

		vm.billInkDashboardService.currentCompanyObj = companyObj; 
		vm.managePurchasePaymentDataService.selectedCompanyId = companyObj.companyId;
		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/viewPurchasePaymentList.html";
		vm.managePurchasePaymentDataService.getPurchasePaymentList(vm.managePurchasePaymentDataService.selectedCompanyId);
	};

	vm.init = function() {
		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/companyWisePurchasePaymentCount.html";
			vm.managePurchasePaymentDataService.getCompanyCountData();
		} else {			
			vm.openPurchasePaymentList(vm.billInkDashboardService.selectedCompany);
		}
	};

	vm.loadMorePurchasePayment = function() {
		if (vm.managePurchasePaymentDataService.hasMorePurchasePaymentData) {
			vm.managePurchasePaymentDataService.startOfPurchasePayment +=  vm.managePurchasePaymentDataService.noOfRecordForPurchasePayment;
			vm.managePurchasePaymentDataService.getPurchasePaymentList(vm.managePurchasePaymentDataService.selectedCompanyId);
		}
	};
	
	vm.loadMoreCompanyCountData = function() {
		if (vm.managePurchasePaymentDataService.hasMoreCompanyCountData) {
			vm.managePurchasePaymentDataService.startOfCompanyCount +=  vm.managePurchasePaymentDataService.noOfRecordForPurchasePayment;
			vm.managePurchasePaymentDataService.getCompanyCountData();
		}
	};

	vm.showAddPurchasePaymentDataForm = function(requestFor) {
		vm.managePurchasePaymentDataService.requestType = "Add";
		vm.managePurchasePaymentDataService.requestFor = requestFor;
		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../purchasepayment/js/addEditPurchasePaymentDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/addEditPurchasePaymentDetail.html";
		});
	};

	vm.showDeletePrompt = function(purchasePaymentId, statusName) {
		vm.selectedDeletePurchasePaymentId = purchasePaymentId;
		vm.selectedDeletePurchasePaymentStatus = statusName;
		$("#purchasePayment-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteSavedPurchasePaymentData = function() {
		$http.post('../../purchasePaymentData/deleteSavedPurchasePaymentData', {
			purchasePaymentId: vm.selectedDeletePurchasePaymentId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.managePurchasePaymentDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	}

	vm.deletePublishPurchasePaymentData = function() {
		$http.post('../../purchasePaymentData/deletePublishedPurchasePaymentData', {
			purchasePaymentId: vm.selectedDeletePurchasePaymentId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.managePurchasePaymentDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	};

	vm.deletePurchasePaymentData = function() {
		if (vm.selectedDeletePurchasePaymentStatus === "UnPublished") {
			vm.deleteSavedPurchasePaymentData();
		} else {
			vm.deletePublishPurchasePaymentData();
		}
	};

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeletePurchasePaymentId = 0;
		vm.selectedDeletePurchasePaymentStatus = "";
		$("#purchasePayment-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
	}

	vm.editPurchasePaymentData = function(purchasePaymentId, imageURL) {
		vm.managePurchasePaymentDataService.requestType = "Edit";
		vm.managePurchasePaymentDataService.requestFor = imageURL && imageURL !== "" ? "uploadImage" : "enterManually";
		vm.managePurchasePaymentDataService.editPurchasePaymentId = purchasePaymentId;
		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../purchasepayment/js/addEditPurchasePaymentDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/addEditPurchasePaymentDetail.html";
		});
	};

	// filter related function start
	vm.launchFilter = function() {
		$("#purchasePayment-filter-dialog .modal").modal('show');
	};

	vm.closeFilterModal = function() {
		$("#purchasePayment-filter-dialog .modal").modal('hide');
	};

	vm.applyFilter = function() {
		vm.searchFilterData();
		$("#purchasePayment-filter-dialog .modal").modal('hide');
	};

	vm.searchFilterData = function() {		
		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		vm.managePurchasePaymentDataService.getPurchasePaymentList(vm.managePurchasePaymentDataService.selectedCompanyId);
	};

	vm.resetFilter = function() {
		vm.managePurchasePaymentDataService.purchasePaymentFilterData = {};
		vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList = [];

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.isVerified = 0;
		}

		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		vm.managePurchasePaymentDataService.getPurchasePaymentList(vm.managePurchasePaymentDataService.selectedCompanyId);
		$("#purchasePayment-filter-dialog .modal").modal('hide');
	};

	vm.toggleSelection = function toggleSelection(status) {
		var idx = vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList.indexOf(status);
	
		if (idx > -1) {
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList.splice(idx, 1);
		} else {
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.statusList.push(status);
		}
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.managePurchasePaymentDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.managePurchasePaymentDataService.purchasePaymentFilterData.ledgerId = 0;
			vm.searchLedgerList = [];
		}
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.managePurchasePaymentDataService.purchasePaymentFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
	};
	// filter related function end

	vm.$onDestroy = function() {
		vm.managePurchasePaymentDataService.purchasePaymentList = [];
		vm.managePurchasePaymentDataService.startOfPurchasePayment = 0;
		vm.managePurchasePaymentDataService.hasMorePurchasePaymentData = true;

		vm.managePurchasePaymentDataService.companyCountRecordList = [];
		vm.managePurchasePaymentDataService.startOfCompanyCount = 0;
		vm.managePurchasePaymentDataService.hasMoreCompanyCountData = true;

		vm.managePurchasePaymentDataService.selectedCompanyId = 0;
	}
});
