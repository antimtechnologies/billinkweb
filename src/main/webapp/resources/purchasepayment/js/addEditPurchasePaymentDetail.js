angular
		.module('addPurchasePaymentData', [ 'fileUploadDirective', 'toaster' ])
		.controller(
				'addPurchasePaymentDataController',
				function($http, fileUploadService, toaster,
						managePurchasePaymentDataService,
						billInkDashboardService) {
					var vm = this;
					vm.managePurchasePaymentDataService = managePurchasePaymentDataService;
					vm.profilePhotoFile = null;
					vm.stateList = [];
					vm.purchasePayment = {};
					vm.purchasePayment.purchaseList = [];
					vm.billInkDashboardService = billInkDashboardService;
					vm.taxCodeList = [ 0, 0.5, 3, 5, 12, 18, 28 ];
					vm.selectedBill = {};
					vm.purchasePayment.imageURLList = [];

					vm.init = function() {
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						if (vm.managePurchasePaymentDataService.requestType === "Edit") {
							$http
									.post(
											'../../purchasePaymentData/getPurchasePaymentDetailsById',
											{
												purchasePaymentId : vm.managePurchasePaymentDataService.editPurchasePaymentId,
												companyId : vm.managePurchasePaymentDataService.selectedCompanyId
											}).then(
											vm.setPurchasePaymentDetail,
											vm.errorFetchData);
						} else {
							vm.purchasePayment.addedBy = {};
							vm.purchasePayment.addedBy.userId = vm.billInkDashboardService.userId;
						}
					};

					vm.setPurchasePaymentDetail = function(response) {
						if (response.data.status === "success") {
							vm.purchasePayment = angular
									.copy(response.data.data);
							// vm.searchLedgerNameText =
							// vm.purchasePayment.ledgerData.ledgerName;
							// vm.searchPaymentFromLedgerNameText =
							// vm.purchasePayment.paymentFromLedger.ledgerName;
							if (!(vm.purchasePayment.paymentFromLedger && vm.purchasePayment.paymentFromLedger.ledgerId > 0)) {
								vm.purchasePayment.paymentFromLedger = null;
							}
							vm.purchasePayment.purchasePaymentDateObj = new Date(
									vm.purchasePayment.purchasePaymentDate);
							vm.purchasePayment.modifiedBy = {};
							vm.purchasePayment.modifiedBy.userId = vm.billInkDashboardService.userId;
							vm.purchasePayment.paymentDateObj = new Date(
									vm.purchasePayment.paymentDate);
						} else if (response.data.status === "error") {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					};

					vm.errorFetchData = function() {
					};

					vm.closeAddEditPurchasePaymentScreen = function() {
						vm.managePurchasePaymentDataService
								.getPurchasePaymentList(vm.managePurchasePaymentDataService.selectedCompanyId);
						vm.billInkDashboardService.currentViewUrl = "../purchasepayment/view/viewPurchasePaymentList.html";
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeAddEditPurchasePaymentScreen();
						} else if (response.data.status === "error") {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.savePurchasePaymentData = function() {
						vm.purchasePayment.paymentDate = moment(
								vm.purchasePayment.paymentDateObj).format(
								"YYYY-MM-DD");
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						vm.purchasePayment.purchasePaymentDate = moment(
								vm.purchasePayment.purchasePaymentDateObj)
								.format("YYYY-MM-DD");
						$http
								.post(
										'../../purchasePaymentData/savePurchasePaymentData',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					vm.publishPurchasePaymentData = function() {
						vm.purchasePayment.paymentDate = moment(
								vm.purchasePayment.paymentDateObj).format(
								"YYYY-MM-DD");
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId
						vm.purchasePayment.purchasePaymentDate = moment(
								vm.purchasePayment.purchasePaymentDateObj)
								.format("YYYY-MM-DD");
						$http
								.post(
										'../../purchasePaymentData/publishPurchasePaymentData',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					function validatePurchasePaymentSaveData() {
						if (!vm.purchasePayment.ledgerData
								|| !vm.purchasePayment.ledgerData.ledgerId
								|| vm.purchasePayment.ledgerData.ledgerId < 1) {
							toaster.showMessage('error', '',
									'kindly select ledger details.');
							return false;
						}
						if (!vm.purchasePayment.paymentFromLedger
								|| !vm.purchasePayment.paymentFromLedger.ledgerId
								|| vm.purchasePayment.paymentFromLedger.ledgerId < 1) {
							toaster.showMessage('error', '',
									'kindly select payment from ledger.');
							return false;
						}

						return true;
					}

					vm.uploadFileAndSavePurchasePaymentData = function(action) {
						if (!validatePurchasePaymentSaveData()) {
							return false;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'purchasePayment')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.purchasePayment.imageURLList = vm.purchasePayment.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.purchasePayment.imageURLList
															&& vm.purchasePayment.imageURLList.length > 0) {
														vm.purchasePayment.imageURL = vm.purchasePayment.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.savePurchasePaymentData();
													} else if (action === "Publish") {
														vm
																.publishPurchasePaymentData();
													}
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading images');
											});
						} else {
							vm.purchasePayment.imageURL = null;
							if (vm.purchasePayment.imageURLList
									&& vm.purchasePayment.imageURLList.length > 0) {
								vm.purchasePayment.imageURL = vm.purchasePayment.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.savePurchasePaymentData();
							} else if (action === "Publish") {
								vm.publishPurchasePaymentData();
							}
						}
					};

					vm.updateSavedPurchasePaymentData = function() {
						vm.purchasePayment.paymentDate = moment(
								vm.purchasePayment.paymentDateObj).format(
								"YYYY-MM-DD");
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						vm.purchasePayment.purchasePaymentDate = moment(
								vm.purchasePayment.purchasePaymentDateObj)
								.format("YYYY-MM-DD");
						$http
								.post(
										'../../purchasePaymentData/updateSavedPurchasePaymentData',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					vm.updatePublishPurchasePaymentData = function() {
						vm.purchasePayment.paymentDate = moment(
								vm.purchasePayment.paymentDateObj).format(
								"YYYY-MM-DD");
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						vm.purchasePayment.purchasePaymentDate = moment(
								vm.purchasePayment.purchasePaymentDateObj)
								.format("YYYY-MM-DD");
						$http
								.post(
										'../../purchasePaymentData/updatePublishedPurchasePaymentData',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					vm.uploadFileAndUpdatePurchasePaymentData = function(action) {
						if (!validatePurchasePaymentSaveData()) {
							return false;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'purchasePayment')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.purchasePayment.imageURLList = vm.purchasePayment.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.purchasePayment.imageURLList
															&& vm.purchasePayment.imageURLList.length > 0) {
														vm.purchasePayment.imageURL = vm.purchasePayment.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.updateSavedPurchasePaymentData();
													} else if (action === "Publish") {
														vm
																.updatePublishPurchasePaymentData();
													}
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading images');
											});
						} else {
							vm.purchasePayment.imageURL = null;
							if (vm.purchasePayment.imageURLList
									&& vm.purchasePayment.imageURLList.length > 0) {
								vm.purchasePayment.imageURL = vm.purchasePayment.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.updateSavedPurchasePaymentData();
							} else if (action === "Publish") {
								vm.updatePublishPurchasePaymentData();
							}
						}
					};

					vm.markPurchasePaymentAsVerified = function() {
						vm.purchasePayment.verifiedBy = {};
						vm.purchasePayment.verifiedBy.userId = vm.billInkDashboardService.userId;
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						$http
								.post(
										'../../purchasePaymentData/markPurchasePaymentAsVerified',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					vm.publishSavedPurchasePayment = function() {
						if (!validatePurchasePaymentSaveData()) {
							return false;
						}
						vm.purchasePayment.companyId = vm.managePurchasePaymentDataService.selectedCompanyId;
						$http
								.post(
										'../../purchasePaymentData/publishSavedPurchasePayment',
										vm.purchasePayment).then(
										successCallback, errorCallback);
					};

					vm.searchLedger = function() {
						if (vm.purchasePayment.ledgerData
								&& vm.purchasePayment.ledgerData.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];
							return;
						}
						if (vm.searchLedgerNameText.length > 2) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchLedgerNameText,
												companyId : vm.managePurchasePaymentDataService.selectedCompanyId
											}).then(
											getLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.searchLedgerList = [];
							vm.purchasePayment.ledgerData = {};
						}
					};

					vm.searchPaymentFromLedger = function() {

						if (vm.purchasePayment.paymentFromLedger
								&& vm.purchasePayment.paymentFromLedger.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];
							return;
						}
						if (vm.searchPaymentFromLedgerNameText.length > 2) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchPaymentFromLedgerNameText,
												companyId : vm.managePurchasePaymentDataService.selectedCompanyId
											})
									.then(
											getPaymentFromLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.searchPaymentFromLedgerList = [];
							vm.purchasePayment.paymentFromLedger = {};
						}
					};

					function getPaymentFromLedgerDataSuccessCallBack(response) {
						vm.purchasePayment.paymentFromLedger = {};
						vm.searchPaymentFromLedgerList = response.data.data;
					}

					vm.selectPaymentFromLedger = function(ledgerObj) {
						vm.searchPaymentFromLedgerNameText = ledgerObj.ledgerName;
						vm.purchasePayment.paymentFromLedger = ledgerObj;
					};

					vm.selectPurchaseBill = function(purchaseObj) {
						if (indexOfPurchaseObj(purchaseObj,
								vm.purchasePayment.purchaseList) === -1) {
							var purchasePaymentPurchaseMapping = {};
							purchasePaymentPurchaseMapping.purchase = purchaseObj;
							vm.purchasePayment.purchaseList
									.push(purchasePaymentPurchaseMapping);
							vm.searchPurchaseBillList = [];
							vm.searchBillNumber = null;
						}
					};

					vm.deleteFromSeletedPurchase = function(purchaseObj) {
						var purchaseObjIndex = indexOfPurchaseObj(purchaseObj,
								vm.purchasePayment.purchaseList);
						vm.purchasePayment.purchaseList.splice(
								purchaseObjIndex, 1);
					};

					vm.searchPurchaseBill = function() {
						var purchaseFilterData = {};
						purchaseFilterData.purchaseId = vm.searchBillNumber;
						purchaseFilterData.ledgerId = vm.purchasePayment.ledgerData.ledgerId;
						purchaseFilterData.paymentStatus = [ 0, 1 ];
						if (vm.searchBillNumber) {
							$http
									.post(
											'../../purchaseData/getPurchaseListData',
											{
												companyId : vm.managePurchasePaymentDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 20,
												filterData : purchaseFilterData
											}).then(getPurchaseBillDetail,
											errorCallback);
						} else {
							vm.searchPurchaseBillList = [];
						}
					};

					function indexOfPurchaseObj(purchaseObj,
							purchasePaymentPurchaseList) {
						for (var count = 0; count < purchasePaymentPurchaseList.length; count++) {
							if (purchaseObj.purchaseId === purchasePaymentPurchaseList[count].purchase.purchaseId) {
								return count;
							}
						}
						return -1;
					}

					function getPurchaseBillDetail(response) {
						vm.searchPurchaseBillList = response.data.data;
					}

					function getLedgerDataSuccessCallBack(response) {

						vm.purchasePayment.ledgerData = {};

						if (response.data.status === "success") {
							vm.searchLedgerList = response.data.data;
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					vm.selectLedger = function(ledgerObj) {
						vm.searchLedgerNameText = ledgerObj.ledgerName;
						vm.purchasePayment.ledgerData = {};
						vm.purchasePayment.ledgerData.ledgerId = ledgerObj.ledgerId;
					};

					vm.fetchItemList = function(index) {
						var itemSearchText = vm.purchasePayment.purchasePaymentItemMappingList[index].item.itemName;
						if (itemSearchText.length > 2) {
							$http
									.post(
											'../../itemData/getItemBasicDetailList',
											{
												itemName : itemSearchText,
												companyId : vm.managePurchasePaymentDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 30
											})
									.then(
											function(response) {
												vm.itemList = [];
												vm.itemList[index] = {};
												vm.itemList[index].itemData = response.data.data;
											}, errorCallback);
						}
					};

					vm.selectLItem = function(index, itemObj) {
						vm.purchasePayment.purchasePaymentItemMappingList[index].item = {};
						vm.purchasePayment.purchasePaymentItemMappingList[index].item.itemName = itemObj.itemName;
						vm.purchasePayment.purchasePaymentItemMappingList[index].item.itemId = itemObj.itemId;
					};

					vm.openImageModel = function(index) {
						if (index < 0
								|| index > (vm.purchasePayment.imageURLList - 1)) {
							return;
						}

						vm.purchasePaymentImageIndex = index;
						$("#purchasePayment-image-show-model .modal").modal(
								'show');
						vm.selectedPhotoURL = vm.purchasePayment.imageURLList[index].imageURL;
					};

					vm.fetchImage = function(position) {
						vm.purchasePaymentImageIndex += position;

						if (vm.purchasePaymentImageIndex < 0) {
							vm.purchasePaymentImageIndex = vm.purchasePayment.imageURLList.length - 1;
						} else if (vm.purchasePaymentImageIndex >= vm.purchasePayment.imageURLList.length) {
							vm.purchasePaymentImageIndex = 0;
						}
						vm.selectedPhotoURL = vm.purchasePayment.imageURLList[vm.purchasePaymentImageIndex].imageURL;
					};

					vm.deleteImage = function(index) {
						vm.purchasePayment.imageURLList.splice(index, 1);
					};

					function getImageURLListObj(imageURLList) {
						var purchasePaymentImageURLList = [];
						var imageURLObj = {};

						for (var count = 0; count < imageURLList.length; count++) {
							imageURLObj = {};
							imageURLObj.imageURL = imageURLList[count];
							purchasePaymentImageURLList.push(imageURLObj);
						}

						return purchasePaymentImageURLList;
					}
				});