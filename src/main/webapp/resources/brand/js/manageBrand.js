angular
		.module('manageBrand', [ 'infinite-scroll', 'ui.bootstrap', 'toaster' ])
		.service("manageBrandService", function($http) {

			var vm = this;
			vm.brandList = [];
			vm.getBrandList = function() {
				$http.post('../../branddata/getItemBrandList', {
					companyId : vm.selectedCompanyId,
					brandName : vm.searchBrandNameText
				}).then(successCallback, errorCallback);

			};

			function successCallback(response) {
				vm.brandList = angular.copy(response.data.data);
			}

			function errorCallback(response) {
				toaster.showMessage('error', '', response.data.errMsg);
			}

		})
		.controller(
				'manageBrandController',
				function($http, $ocLazyLoad, toaster, manageBrandService,
						billInkDashboardService) {
					var vm = this;
					vm.manageBrandService = manageBrandService;
					vm.manageBrandService.currentViewUrl = "../brand/view/viewBrandList.html";
					vm.billInkDashboardService = billInkDashboardService;
					vm.startOfCompany = 0;
					const noOfRecordForCompany = 30;
					vm.brand = {};

					vm.init = function() {
						vm.manageBrandService.brandList = [];
						vm.getCompanyList();
					};

					vm.getCompanyListOnSearch = function() {
						vm.startOfCompany = 0;
						vm.getCompanyList();
					};

					vm.getCompayBrandDetail = function(companyId) {
						vm.manageBrandService.selectedCompanyId = companyId
						vm.manageBrandService.getBrandList();
					}

					vm.searchBrandData = function() {
						vm.manageBrandService.brandList = [];
						vm.manageBrandService.getBrandList();
					};

					vm.getCompanyList = function() {
						$http.post('../../companydata/getCompanyNameIdList', {
							start : vm.startOfCompany,
							noOfRecord : noOfRecordForCompany,
							companyName : vm.searchCompanyNameText
						}).then(getCompanyDataSuccessCallBack, errorCallback);
					};

					vm.loadMoreCompanyList = function() {
						if (vm.totalCount > vm.startOfCompany) {
							vm.startOfCompany += noOfRecordForCompany;
							vm.getCompanyList();
						}
					};

					function getCompanyDataSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompany === 0) {
								vm.companyList = response.data.data;
								vm.totalCount = response.data.totalCount;
								if (vm.companyList.length > 0) {
									vm.manageBrandService.selectedCompanyId = vm.companyList[0].companyId;
									vm.manageBrandService.getBrandList();
								}
								return;
							}
							vm.companyList = vm.companyList
									.concat(response.data.data);
						}
					}

					vm.deleteBrand = function(brandId) {
						vm.selectedDeleteBrandId = brandId;
						$("#brand-delelete-confirmation-dialog .modal").modal(
								'show');
					};

					vm.deleteBrandData = function() {
						$http.post('../../branddata/deleteItemBrandData', {
							brandId : vm.selectedDeleteBrandId,
							loggedInUserId : vm.billInkDashboardService.userId
						}).then(deleteBrandSuccessCallback, errorCallback);
					}

					function deleteBrandSuccessCallback() {
						vm.closeDeleteModal();
						vm.manageBrandService.getBrandList();
					}

					vm.closeDeleteModal = function() {
						vm.selectedDeleteBrandId = 0;
						$("#brand-delelete-confirmation-dialog .modal").modal(
								'hide');
					};

					vm.editBrand = function(brandId, brandName) {
						vm.brand.brandName = brandName;
						vm.brand.brandId = brandId;
						vm.brand.companyId = vm.manageBrandService.selectedCompanyId;
						$("#brand-edit-confirmation-dialog .modal").modal(
								'show');
					};

					vm.UpdateBrandData = function() {
						if (!vm.brand.brandName || vm.brand.brandName === "") {
							toaster.showMessage('error', '',
									'Kindly enter brand name.');
							return false;
						}
						$http.post('../../branddata/updateItemBrandData',
								vm.brand).then(editBrandSuccessCallback,
								errorCallback);
					}

					function editBrandSuccessCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage('success', '',
									'data saved successfully.');
							vm.closeEditModal();
							vm.manageBrandService.getBrandList();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					vm.closeEditModal = function() {
						vm.selectedDeleteBrandId = 0;
						$("#brand-edit-confirmation-dialog .modal").modal(
								'hide');
					};

					vm.addBrandData = function() {
						vm.brand = {};
						vm.brand.companyId = vm.manageBrandService.selectedCompanyId;
						$("#brand-add-confirmation-dialog .modal")
								.modal('show');
					};

					vm.saveBrandData = function() {
						if (!vm.brand.brandName || vm.brand.brandName === "") {
							toaster.showMessage('error', '',
									'Kindly enter brand name.');
							return false;
						}
						$http.post('../../branddata/saveItemBrandData',
								vm.brand).then(saveBrandSuccessCallback,
								errorCallback);
					}

					function saveBrandSuccessCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage('success', '',
									'data saved successfully.');
							vm.closeAddModal();
							vm.manageBrandService.getBrandList();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					vm.closeAddModal = function() {
						vm.selectedDeleteBrandId = 0;
						$("#brand-add-confirmation-dialog .modal")
								.modal('hide');
					};

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.$onDestroy = function() {
						vm.manageBrandService.totalCount = 0;
						vm.manageBrandService.brandList = [];
					};

				});
