var manageItemDataApp = angular.module('manageItem', ['infiniteScroll', 'infinite-scroll']);

manageItemDataApp.service('manageItemDataService', function($http){
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfItem				= 0;
	svc.noOfRecordForItem		= 12;
	svc.itemList				= [];
	svc.totalItemCount 			= 0;

	svc.getCompanyItemList = function() {
		
		$http.post('../../itemData/getItemListData', {
							companyId: svc.selectedCompanyId, 
							start: svc.startOfItem, 
							noOfRecord: svc.noOfRecordForItem,
							itemName: svc.searchItemNameText
					}).then(svc.successCallback, svc.errorCallback);
	};

	svc.successCallback = function(response) {
		if (response.data.status === "success") {
			if (svc.startOfItem === 0) {
				svc.totalItemCount = response.data.totalCount;
				svc.itemList = response.data.data;
				return;
			}
			svc.itemList = svc.itemList.concat(response.data.data);
		}
	};

	svc.errorCallback = function() {
		svc.itemList = [];
	};

});

manageItemDataApp.controller('manageItemController', function($http, $ocLazyLoad, manageItemDataService, billInkDashboardService) {
	var vm 							= this;
	vm.manageItemDataService		= manageItemDataService;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.manageItemDataService.itemContainerURL = "../item/view/viewItemList.html";
	vm.billInkDashboardService 		= billInkDashboardService;
	vm.init = function() {
		vm.startOfCompany = 0;
		
		vm.getCompanyList();
	};

	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					vm.manageItemDataService.selectedCompanyId = vm.companyList[0].companyId;
					vm.manageItemDataService.getCompanyItemList();
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
		}
	}

	function errorCallback() {
		
	}

	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};

	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
		}
	};

	vm.getCompayItemDetail = function(companyId) {
		vm.manageItemDataService.selectedCompanyId = companyId;
		vm.manageItemDataService.startOfItem = 0;
		vm.manageItemDataService.itemList = [];
		vm.manageItemDataService.getCompanyItemList();
	};
	
	vm.searchItemData = function() {
				vm.manageItemDataService.startOfItem = 0;
				vm.manageItemDataService.itemList = [];
				vm.manageItemDataService.getCompanyItemList();
				
			}

	vm.loadMoreItem = function() {
		if (vm.manageItemDataService.totalItemCount > vm.manageItemDataService.startOfItem) {			
			vm.manageItemDataService.startOfItem += vm.manageItemDataService.noOfRecordForItem;
			vm.manageItemDataService.getCompanyItemList();
		}
	};

	vm.addItemData = function() {
		vm.manageItemDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../item/js/addEditItemData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageItemDataService.itemContainerURL = "../item/view/addEditItemData.html";
		});
	};

	vm.showDeletePrompt = function(itemId) {
		vm.selectedDeleteItemId = itemId;
		$("#item-delete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteItemData = function() {
		$http.post('../../itemData/deleteItemData', {itemId: vm.selectedDeleteItemId, companyId : vm.manageItemDataService.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId }).then(deleteItemSuccessCallback, errorCallback);	
	};

	function deleteItemSuccessCallback() {
		vm.closeDeleteModal();
		vm.getCompayItemDetail(vm.manageItemDataService.selectedCompanyId);
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteItemId = 0;
		$("#item-delete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
		
	}

	vm.editItemData = function(itemId) {
		vm.manageItemDataService.requestType = "Edit";
		vm.manageItemDataService.editItemId = itemId;
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../item/js/addEditItemData.js"],
				{serie: true}
		)
		.then(function () {
			vm.manageItemDataService.itemContainerURL = "../item/view/addEditItemData.html";
		});
	};

});

