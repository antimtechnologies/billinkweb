var aaddItemDataApp = angular.module('addEditItemData', [
		'fileUploadDirective', 'toaster' ]);

aaddItemDataApp
		.controller(
				'addEditItemDataController',
				function($http, fileUploadService, toaster,
						manageItemDataService, billInkDashboardService) {
					var vm = this;

					vm.profilePhotoFile = null;
					vm.stateList = [];
					vm.taxCodeList = [];
					vm.manageItemDataService = manageItemDataService;
					vm.billInkDashboardService = billInkDashboardService;
					vm.billInkDashboardService.item = {};
					vm.billInkDashboardService.item.itemDocumentList = [];

					vm.billInkDashboardService.item.itemBatchList = [];
					vm.billInkDashboardService.item.itemAliasList = [];
					vm.billInkDashboardService.item.itemMrpList = [];
					vm.billInkDashboardService.item.itemPurchaseRateList = [];
					vm.maxDate = moment().format('YYYY-MM-DD');

					vm.init = function() {
						vm.manageItemDataService.startOfItem = 0;
						vm.billInkDashboardService.item = {};
						vm.manageItemDataService.itemList = [];
						vm.billInkDashboardService.item.itemDocumentList = [];
						vm.billInkDashboardService.item.itemBatchList = [];
						vm.billInkDashboardService.item.itemMrpList = [];
						vm.billInkDashboardService.item.itemPurchaseRateList = [];
						vm.billInkDashboardService.item.itemAliasList = [];
						var parameter = {
							companyId : manageItemDataService.selectedCompanyId
						};
						$http.post('../../categorydata/getItemCategoryList',
								parameter).then(vm.setCategoryList,
								vm.errorFetchList);
						$http.post('../../branddata/getItemBrandList',
								parameter).then(vm.setBrandList,
								vm.errorFetchList);
						$http.post('../../taxcodedata/getTaxCodeValueList', {})
								.then(vm.setTaxCodeList, vm.errorFetchList);

						vm.billInkDashboardService.item.itemDocumentList = [];
						if (vm.manageItemDataService.requestType == "Edit") {
							// $("#itemSku").hide();
							$http
									.post(
											'../../itemData/getItemDetailsById',
											{
												companyId : vm.manageItemDataService.selectedCompanyId,
												itemId : vm.manageItemDataService.editItemId
											}).then(vm.setItemDetail,
											vm.errorFetchList);

						} else {
							vm.billInkDashboardService.item.addedBy = {};
							vm.billInkDashboardService.item.addedBy.userId = vm.billInkDashboardService.userId;
							vm.billInkDashboardService.item.companyId = manageItemDataService.selectedCompanyId;

							$http
									.post(
											'../../companydata/getBillPrefixAndLastNumber',
											{
												companyId : vm.billInkDashboardService.item.companyId
											})
									.then(
											function(response) {
												vm.billInkDashboardService.item.itemNumberPrefix = response.data.data.skuprefix
														|| "";
												vm.billInkDashboardService.item.currentItemNumber = response.data.data.lastItemNumber ? response.data.data.lastItemNumber + 1
														: 1;
												vm.billInkDashboardService.item.sku = vm.billInkDashboardService.item.itemNumberPrefix
														+ vm.billInkDashboardService.item.currentItemNumber;
											}, vm.errorFetchData);
						}

						/*
						 * $http.post('../../companydata/getBillPrefixAndLastNumber', {
						 * companyId :
						 * vm.billInkDashboardService.item.companyId})
						 * .then(function(response) {
						 * vm.billInkDashboardService.item.itemNumberPrefix =
						 * response.data.data.billNumberPrefix || "";
						 * vm.billInkDashboardService.item.currentItemNumber =
						 * response.data.data.lastItemNumber ?
						 * response.data.data.lastItemNumber + 1 : 1;
						 * vm.billInkDashboardService.item.sku =
						 * vm.billInkDashboardService.item.itemNumberPrefix+vm.billInkDashboardService.item.currentItemNumber; },
						 * vm.errorFetchData);
						 */

					};

					vm.setTaxCodeList = function(response) {
						vm.billInkDashboardService.taxCodeList = response.data.data;
					};

					vm.setItemDetail = function(response) {
						vm.billInkDashboardService.item = angular
								.copy(response.data.data);
						vm.billInkDashboardService.item.companyId = vm.manageItemDataService.selectedCompanyId;
						vm.selectedCategory = vm.billInkDashboardService.item.category.categoryName;

						vm.selectedBrand = vm.billInkDashboardService.item.brand.brandName;
						vm.billInkDashboardService.item.modifiedBy = {};
						vm.billInkDashboardService.item.itemNumberPrefix = "";
						$http
								.post(
										'../../companydata/getBillPrefixAndLastNumber',
										{
											companyId : vm.manageItemDataService.selectedCompanyId
										})
								.then(
										function(response) {
											vm.billInkDashboardService.item.itemNumberPrefix = response.data.data.billNumberPrefix
													|| "";
											// vm.billInkDashboardService.item.currentItemNumber
											// =
											// response.data.data.lastItemNumber
											// ?
											// response.data.data.lastItemNumber
											// + 1 : 1;
											vm.billInkDashboardService.item.sku = vm.billInkDashboardService.item.itemNumberPrefix
													+ vm.billInkDashboardService.item.currentItemNumber;
										}, vm.errorFetchData);
						vm.billInkDashboardService.item.modifiedBy.userId = vm.billInkDashboardService.userId;
						if (vm.billInkDashboardService.item.itemBatchList.length != 0) {
							for (var i = 0; i < vm.billInkDashboardService.item.itemBatchList.length; i++) {
								vm.billInkDashboardService.item.itemBatchList[i].mfgDates = moment(
										vm.billInkDashboardService.item.itemBatchList[i].mfgDate)
										.format("DD-MM-YYYY");
								vm.billInkDashboardService.item.itemBatchList[i].expDates = moment(
										vm.billInkDashboardService.item.itemBatchList[i].expDate)
										.format("DD-MM-YYYY");
							}
						}

						// if
						// (vm.billInkDashboardService.item.itemMrpList.length
						// != 0) {
						// for (var j = 0; j <
						// vm.billInkDashboardService.item.itemMrpList.length;
						// j++) {
						// vm.billInkDashboardService.item.itemMrpList[j].wetDate
						// =
						// moment(vm.billInkDashboardService.item.itemMrpList[j].wetDate).format("MM-DD-YYYY");
						// vm.billInkDashboardService.item.itemMrpList[j].wefDate
						// =
						// moment(vm.billInkDashboardService.item.itemMrpList[j].wefDate).format("MM-DD-YYYY");
						// }
						// }
						//		
						// if
						// (vm.billInkDashboardService.item.itemPurchaseRateList.length
						// != 0) {
						// for (var k = 0; k <
						// vm.billInkDashboardService.item.itemMrpList.length;
						// k++) {
						// vm.billInkDashboardService.item.itemPurchaseRateList[k].wetPurachaseRateDate
						// =
						// moment(vm.billInkDashboardService.item.itemPurchaseRateList[k].wetPurachaseRateDate).format("MM-DD-YYYY");
						// vm.billInkDashboardService.item.itemPurchaseRateList[k].wefPurchaseRateDate
						// =
						// moment(vm.billInkDashboardService.item.itemPurchaseRateList[k].wefPurchaseRateDate).format("MM-DD-YYYY");
						// }
						// }
					};

					vm.errorFetchList = function(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					};

					vm.errorFetchData = function(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					};

					vm.setCategoryList = function(response) {
						vm.billInkDashboardService.categoryList = response.data.data;
					};

					vm.setBrandList = function(response) {
						vm.billInkDashboardService.brandList = response.data.data;
					};

					vm.setSelectedBrand = function(index) {
						vm.billInkDashboardService.item.brand = vm.brandList[index];
						vm.selectedBrand = vm.billInkDashboardService.item.brand.brandName;
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeItemDataSave();
							toaster.showMessage('success', '',
									"Records Saved/updated");
							vm.manageItemDataService.getCompanyItemList();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.addItem = function() {

						if (!vm.billInkDashboardService.item.itemName
								|| vm.billInkDashboardService.item.itemName === "") {
							toaster.showMessage('error', '',
									'Kindly enter item name.');
							return false;
						}
						var re = new RegExp("^[A-Za-z0-9]+$");
						if (vm.billInkDashboardService.item.eanNo
								&& vm.billInkDashboardService.item.eanNo != ""
								&& !re
										.test(vm.billInkDashboardService.item.eanNo)) {
							toaster.showMessage('error', '',
									'Only Alphanuric allow for EANNum.');
							return false;
						}

						// $http.post('../../itemData/saveItemData',
						// vm.billInkDashboardService.item).then(successCallback,
						// errorCallback);
						$http
								.post(
										'../../itemData/getItemList',
										{
											companyId : vm.billInkDashboardService.item.companyId,
											itemName : vm.billInkDashboardService.item.itemName
										}).then(successItemList, errorItemList);

					};

					vm.uploadFileAndSaveItemData = function() {
						var file = vm.profilePhotoFile;

						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'item')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.billInkDashboardService.item.profilePhotoURL = result.data.imagePath;
													vm.addItem();
												} else {
													toaster
															.showMessage(
																	'error',
																	'',
																	'error while uploading document');
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading document');
											});
						} else {
							vm.billInkDashboardService.item.profilePhotoURL = null;
							vm.addItem();
						}
					};

					vm.updateItem = function() {
						//		
						if (!vm.billInkDashboardService.item.itemName
								&& vm.billInkDashboardService.item.itemName === "") {
							toaster.showMessage('error', '',
									'Kindly enter item name.');
							return false;
						}
						var re = new RegExp("^[A-Za-z0-9]+$");
						if (vm.billInkDashboardService.item.eanNo
								&& vm.billInkDashboardService.item.eanNo != ""
								&& !re
										.test(vm.billInkDashboardService.item.eanNo)) {
							toaster.showMessage('error', '',
									'Only Alphanuric allow for EANNum.');
							return false;
						}

						$http.post('../../itemData/updateItemData',
								vm.billInkDashboardService.item).then(
								successCallback, errorCallback);
					};

					vm.uploadFileAndUpdateItemData = function() {
						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'item')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.billInkDashboardService.item.profilePhotoURL = result.data.imagePath;
													vm.updateItem();
												} else {
													toaster
															.showMessage(
																	'error',
																	'',
																	'error while uploading document');
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading document');
											});
						} else {
							vm.updateItem();
						}
					};

					vm.closeItemDataSave = function() {
						vm.manageItemDataService.editItemId = 0;
						vm.manageItemDataService.startOfItem = 0;
						vm.manageItemDataService.getCompanyItemList();
						vm.manageItemDataService.requestType = "";
						vm.manageItemDataService.itemContainerURL = "../item/view/viewItemList.html";
					}

					vm.addDocument = function() {
						angular.element("input[type='file']").val(null);
						vm.itemDocumentObj = {};
						vm.itemDocumentFile = "";
						$("#add-document-dialog .modal").modal('show');
					};

					vm.closedDocumentModal = function() {
						$("#add-document-dialog .modal").modal('hide');
					};

					vm.saveDocument = function() {
						var file = vm.itemDocumentFile;
						if (vm.itemDocumentFile) {
							fileUploadService
									.uploadFileToUrl(file, 'company')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.itemDocumentObj.documentUrl = result.data.imagePath;
													console
															.log(vm.billInkDashboardService.item);
													console
															.log(vm.billInkDashboardService.item.itemDocumentList);
													vm.billInkDashboardService.item.itemDocumentList
															.push(vm.itemDocumentObj);
												} else {
													toaster
															.showMessage(
																	'error',
																	'',
																	'error while uploading document');
												}
												vm.itemDocumentObj = {};
												vm.itemDocumentFile = undefined;
												$("#add-document-dialog .modal")
														.modal('hide');
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading document');
											});
						}
					};

					vm.deleteDocument = function(index) {
						vm.billInkDashboardService.item.itemDocumentList
								.splice(index, 1);
					};

					vm.$onDestroy = function() {
						vm.billInkDashboardService.categoryList = [];
						vm.billInkDashboardService.brandList = [];
						vm.billInkDashboardService.taxCodeList = [];
					}

					vm.addItemAlias = function() {
						vm.itemAliasObj = {};
						$("#add-alias-dialog .modal").modal('show');

					}
					vm.closedAliasModal = function() {
						$("#add-alias-dialog .modal").modal('hide');
					}

					vm.saveItemAlias = function() {

						if (!vm.itemAliasObj.itemAliasName
								|| vm.itemAliasObj.itemAliasName === "") {
							toaster.showMessage('error', '',
									'Kindly enter itemAliasName.');
							return false;
						}

						if (vm.billInkDashboardService.item.itemAliasList.length == 5) {
							toaster.showMessage('error', '',
									'You can nott add more than 5');
							return false;
						}

						console.log("itemAlias Company Id :"
								+ vm.manageItemDataService.selectedCompanyId);
						// $http.post("../../itemData/getItemAliasNameById",{
						// companyId :
						// vm.manageItemDataService.selectedCompanyId})
						// .then(successItemAlias, errorCallback);

						$http
								.post(
										"../../itemData/getItemAliasNameListByCompanyId",
										{
											companyId : vm.manageItemDataService.selectedCompanyId,
											itemAliasName : vm.itemAliasObj.itemAliasName
										})
								.then(successItemAlias, errorCallback);
					}

					function successItemAlias(response) {
						if (response.data.data.length == 0) {
							vm.billInkDashboardService.item.itemAliasList
									.push(vm.itemAliasObj);
							$("#add-alias-dialog .modal").modal('hide');
						} else {
							toaster.showMessage('error', '',
									'this name already exits.');
						}

						// var flag = true;
						// vm.billInkDashboardService.getItemAliasList =
						// response.data.data;
						// if(vm.billInkDashboardService.getItemAliasList.length
						// > 0){
						// for (var i = 0; i <
						// vm.billInkDashboardService.getItemAliasList.length;
						// i++) {
						// if (vm.itemAliasObj.itemAliasName ==
						// vm.billInkDashboardService.getItemAliasList[i].itemAliasName)
						// {
						// toaster.showMessage('error', '', 'this name already
						// exits.');
						// flag = false;
						// break;
						// }
						// }
						// }
						//		
						// if (flag) {
						// vm.billInkDashboardService.item.itemAliasList.push(vm.itemAliasObj);
						// $("#add-alias-dialog .modal").modal('hide');
						// }

					}

					vm.addBatch = function() {
						// angular.element("input[type='file']").val(null);
						vm.itemBatchObj = {};
						$("#add-batch-dialog .modal").modal('show');
					};

					vm.closedBatchModal = function() {
						$("#add-batch-dialog .modal").modal('hide');
					};

					vm.saveBatch = function() {
						if (!vm.itemBatchObj.batchNo
								|| vm.itemBatchObj.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.itemBatchObj.mfgDate
								|| vm.itemBatchObj.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						if (!vm.itemBatchObj.shelfLife
								|| vm.itemBatchObj.shelfLife === "") {
							toaster.showMessage('error', '',
									'Kindly enter Shelft Date.');
							return false;
						}
						console.log("selected type :"
								+ document.getElementById("dayMonthDD").value);

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.itemBatchObj.mfgDate);
							var month = date.getMonth()
									+ vm.itemBatchObj.shelfLife;
							date.setMonth(month);
							vm.itemBatchObj.expDate = moment(date).format(
									"YYYY-MM-DD");
							vm.itemBatchObj.expDates = moment(date).format(
									"DD-MM-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							var date = new Date(vm.itemBatchObj.mfgDate);
							var days = date.getDate()
									+ vm.itemBatchObj.shelfLife;
							date.setDate(days)
							vm.itemBatchObj.expDate = moment(date).format(
									"YYYY-MM-DD");
							vm.itemBatchObj.expDates = moment(date).format(
									"DD-MM-YYYY");
						}
						vm.itemBatchObj.mfgDate = moment(
								vm.itemBatchObj.mfgDate).format("YYYY-MM-DD");
						vm.itemBatchObj.mfgDates = moment(
								vm.itemBatchObj.mfgDate).format("DD-MM-YYYY");

						vm.itemBatchObj.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.itemBatchObj.shelfLife;
						vm.billInkDashboardService.item.itemBatchList
								.push(vm.itemBatchObj);
						$("#add-batch-dialog .modal").modal('hide');
					};

					vm.addItemMrp = function() {
						// angular.element("input[type='file']").val(null);
						vm.itemMrpObj = {};
						$("#add-mrp-dialog .modal").modal('show');
					};

					vm.closedMrpRateModal= function() {
						$("#add-mrp-dialog .modal").modal('hide');
					};

					vm.saveMrp = function() {
						if (!vm.itemMrpObj.itemMrp
								|| vm.itemMrpObj.itemMrp === "") {
							toaster.showMessage('error', '',
									'Kindly enter Item Mrp.');
							return false;
						}
						if (!vm.itemMrpObj.wetDate
								|| vm.itemMrpObj.wetDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter W.E.T Date.');
							return false;
						}
						if (!vm.itemMrpObj.wefDate
								|| vm.itemMrpObj.wefDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter W.E.F Date.');
							return false;
						}
						vm.itemMrpObj.wetDate = moment(vm.itemMrpObj.wetDate)
								.format("YYYY-MM-DD");
						vm.itemMrpObj.wefDate = moment(vm.itemMrpObj.wefDate)
								.format("YYYY-MM-DD");
						vm.billInkDashboardService.item.itemMrpList
								.push(vm.itemMrpObj);
						$("#add-mrp-dialog .modal").modal('hide');
					};

					// 23-05-2019 Start
					function successItemList(response) {
						console.log("responce sc");
						vm.ItemList = response.data.data;
						vm.itemListAb = angular.copy(response.data.data);
						console.log("ItemList :" + vm.ItemList.length);
						console.log("itemListAb :" + vm.itemListAb.length);
						console.log(response.data.data.length);
						if (response.data.data.length == 0) {
							$http
									.post(
											'../../itemData/getItemListByEanNo',
											{
												companyId : vm.billInkDashboardService.item.companyId,
												eanNo : vm.billInkDashboardService.item.eanNo
											}).then(successItemListByEanNo,
											errorItemListByEanNo);
						} else {
							toaster.showMessage('success', '',
									"This Item Already Available....");
						}

					}
					function errorItemList() {
						// $http.post('../../itemData/getItemListByEanNo',
						// {companyId :
						// vm.billInkDashboardService.item.companyId,itemName :
						// vm.billInkDashboardService.item.itemName}).then(successItemListByEanNo,
						// errorItemListByEanNo);
					}

					function successItemListByEanNo(response) {
						console.log("responce sc");
						console.log(response.data.data);
						if (response.data.data.length == 0) {
							// $http.post('../../itemData/getItemListByEanNo',
							// {companyId :
							// vm.billInkDashboardService.item.companyId,itemName
							// :
							// vm.billInkDashboardService.item.itemName}).then(successItemListByEanNo,
							// errorItemListByEanNo);
							$http.post('../../itemData/saveItemData',
									vm.billInkDashboardService.item).then(
									successCallback, errorCallback);
						} else {
							toaster.showMessage('success', '',
									"This EanNo. Already Available....");
						}

					}

					function errorItemListByEanNo() {
						// $http.post('../../itemData/saveItemData',
						// vm.billInkDashboardService.item).then(successCallback,
						// errorCallback);
					}

					vm.checkDate = function() {
						console.log("checkdate call");
						console.log(vm.itemPurchaseRateObj.wefPurachaseRateDate);
						console.log(vm.itemPurchaseRateObj.wetPurachaseRateDate);
						var fd = new Date(
								vm.itemPurchaseRateObj.wefPurchaseRateDate)
								.getTime();
						var td = new Date(
								vm.itemPurchaseRateObj.wetPurachaseRateDate)
								.getTime();
						if (fd > td) {
							console.log("fd greter than td");
							alert("wrong date has been set.Please select correctdate. ");
							vm.itemPurchaseRateObj.wefPurchaseRateDate="";
							vm.itemPurchaseRateObj.wetPurchaseRateDate="";
							
						}
						var dt = formatDate(vm.itemPurchaseRateObj.wefPurchaseRateDate);
						document.getElementById("wetPurachaseRateDate").min = dt;
					};

					vm.checkMrpDate = function() {
						console.log("checkMrpDate call");
						var fd = new Date(vm.itemMrpObj.wefDate).getTime();
						var td = new Date(vm.itemMrpObj.wetDate).getTime();
						if (fd > td) {
							console.log("fd greter than td");
							alert("wrong date has been set.Please select correctdate. ")
							vm.itemMrpObj.wefDate="";
							vm.itemMrpObj.wetDate="";
						}
						var dt = formatDate(vm.itemMrpObj.wefDate);
						document.getElementById("wetDate").min = dt;
					};

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}
					// 23-05-2019 End

					vm.addPurchaseRate = function() {
						// angular.element("input[type='file']").val(null);
						vm.itemPurchaseRateObj = {};
						// let date = moment().format('MM/DD/YYYY');
						// vm.itemPurchaseRateObj.wefPurchaseRateDate = new
						// Date(date);
						// vm.itemPurchaseRateObj.wetPurachaseRateDate = new
						// Date(date);
						$("#add-purchaserate-dialog .modal").modal('show');
					};

					vm.closedMrpModal = function() {
						$("#add-purchaserate-dialog .modal").modal('hide');
					};

					vm.savePurchaseRate = function() {
						if (!vm.itemPurchaseRateObj.itemPurchaseRate
								|| vm.itemPurchaseRateObj.itemPurchaseRate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Purchase Rate.');
							return false;
						}
						if (!vm.itemPurchaseRateObj.wetPurachaseRateDate
								|| vm.itemPurchaseRateObj.wetPurachaseRateDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter W.E.T Date.');
							return false;
						}
						if (!vm.itemPurchaseRateObj.wefPurchaseRateDate
								|| vm.itemPurchaseRateObj.wefPurchaseRateDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter W.E.F Date.');
							return false;
						}
						vm.itemPurchaseRateObj.wetPurachaseRateDate = moment(
								vm.itemPurchaseRateObj.wetPurachaseRateDate)
								.format("YYYY-MM-DD");
						vm.itemPurchaseRateObj.wefPurchaseRateDate = moment(
								vm.itemPurchaseRateObj.wefPurchaseRateDate)
								.format("YYYY-MM-DD");
						vm.billInkDashboardService.item.itemPurchaseRateList
								.push(vm.itemPurchaseRateObj);
						$("#add-purchaserate-dialog .modal").modal('hide');
					};

					vm.deleteBatch = function(index) {
						vm.billInkDashboardService.item.itemBatchList.splice(
								index, 1);
					};
					vm.deleteMrp = function(index) {
						vm.billInkDashboardService.item.itemMrpList.splice(
								index, 1);
					};
					vm.deletePurchaseRate = function(index) {
						vm.billInkDashboardService.item.itemPurchaseRateList
								.splice(index, 1);
					};

					vm.deleteItemAlias = function(index) {
						vm.billInkDashboardService.item.itemAliasList.splice(
								index, 1);
					};

				});
