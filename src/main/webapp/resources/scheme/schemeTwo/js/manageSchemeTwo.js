var manageSchemeTwoDataApp = angular.module('manageSchemeTwo', ['infiniteScroll', 'infinite-scroll','toaster']);

manageSchemeTwoDataApp.service('manageSchemeTwoDataService', function($http){
	
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfItem				= 0;
	svc.noOfRecordForItem		= 12;
	svc.itemList				= [];
	svc.schemaTwoListObj=[];
	svc.getCompanyItemList = function() {
		
		$http.post('../../itemData/getItemListData', {companyId: svc.selectedCompanyId, start: svc.startOfItem, noOfRecord: svc.noOfRecordForItem }).
		then(svc.successCallback, svc.errorCallback);
	};

	svc.successCallback = function(response) {
		if (response.data.status === "success") {
			if (svc.startOfItem === 0) {
				svc.totalItemCount = response.data.totalCount;
				svc.itemList = response.data.data;
				return;
			}
			svc.itemList = svc.itemList.concat(response.data.data);
		}
	};

	svc.errorCallback = function() {
		svc.itemList = [];
	};
	
});

manageSchemeTwoDataApp.controller('manageSchemeTwoController', function($http, $ocLazyLoad, manageSchemeTwoDataService, billInkDashboardService,toaster) {
	
	var vm 							= this;
	vm.manageSchemeTwoDataService		= manageSchemeTwoDataService;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.billInkDashboardService 		= billInkDashboardService;
	vm.billInkDashboardService.item = {};
	vm.manageSchemeTwoDataService.itemList	=[];
	vm.manageSchemeTwoDataService.schemaTwoListObj=[];
	vm.schemaTwoListObj=[];
	vm.schmeonestart=0;
	vm.selectedCompanyId=0;
	const totalrecords=30;
	vm.startOfCompany = 0;
	vm.init = function() {
		
		vm.billInkDashboardService.item = {};
		
		vm.getCompanyList();
		
//		vm.manageSchemeTwoDataService.getCompanyItemList();
//		vm.getSchme2List();
		
	};
	
	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};
	
	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};
	
	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
		}
	};
	
	vm.getSchme2List=function(){
		
		
		
//		alert(vm.manageSchemeTwoDataService.selectedCompanyId);
		$http.post('../../schemeData/getSchemeTwoData',{companyId: vm.selectedCompanyId,start: vm.schmeonestart, noOfRecord: totalrecords})
		.then(successCallbackSchemaTwoList,errorCallbackSchemaTwoList)
		
	}
	
	function successCallbackSchemaTwoList(response) {
		vm.manageSchemeTwoDataService.schemaTwoListObj = angular.copy(response.data.data);
		
	}

	function errorCallbackSchemaTwoList(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}
	
	vm.closeSchemeTwoModel = function() {
		$("#add-schemetwo-section-model .modal").modal('hide');
		
		};
	
	vm.showSchemeTwoModel=function(){
		vm.billInkDashboardService.item.companyId=vm.selectedCompanyId;
		vm.manageSchemeTwoDataService.selectedCompanyId=vm.selectedCompanyId;
		vm.manageSchemeTwoDataService.getCompanyItemList();
		$("#add-schemetwo-section-model .modal").modal('show');
		
		
		};
	
	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					
					vm.selectedCompanyId = vm.companyList[0].companyId;
					vm.manageSchemeTwoDataService.selectedCompanyId=vm.selectedCompanyId;
					vm.getSchme2List();
					vm.manageSchemeTwoDataService.getCompanyItemList();
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
			
		}
	}
	function errorCallback() {
		
	}
	
	vm.getCompayItemDetail = function(companyId) {
		vm.selectedCompanyId = companyId;
		vm.manageSchemeTwoDataService.selectedCompanyId = companyId;
		vm.manageSchemeTwoDataService.startOfItem = 0;
		vm.manageSchemeTwoDataService.itemList = [];
		vm.manageSchemeTwoDataService.getCompanyItemList();
		vm.getSchme2List();
	};
	
	vm.addSchemeTwoData = function() {
		vm.manageSchemeTwoDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../scheme/schemeTwo/js/addEditSchemeTwoData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageSchemeTwoDataService.itemContainerURL = "../scheme/schemeTwo/view/addEditSchemeTwoData.html";
		});
	};
	
	vm.loadMoreItem = function() {
		if (vm.manageSchemeTwoDataService.totalItemCount > vm.manageSchemeTwoDataService.startOfItem) {			
			vm.manageSchemeTwoDataService.startOfItem += vm.manageSchemeTwoDataService.noOfRecordForItem;
			vm.manageSchemeTwoDataService.selectedCompanyId=vm.selectedCompanyId;
			vm.manageSchemeTwoDataService.getCompanyItemList();
		}
	};
	
	
	vm.saveSchemeTwoData = function() {
		vm.billInkDashboardService.item.itemId = document.getElementById("itemId").value;
		vm.billInkDashboardService.item.freeItemId = document.getElementById("freeItemId").value;
		//vm.manageSchemeTwoDataService.selectedCompanyId=vm.selectedCompanyId;
		vm.billInkDashboardService.item.companyId = vm.manageSchemeTwoDataService.selectedCompanyId;
		vm.selectedCompanyId=vm.billInkDashboardService.item.companyId;
		
		$http.post('../../schemeData/overruledSchemaTwo', {companyId: vm.selectedCompanyId,itemId:vm.billInkDashboardService.item.itemId,
			totalQtyOfItem:vm.billInkDashboardService.item.totalQtyOfItem}).then(function(response){
			if(response.data.data=="exists"){
				toaster.showMessage('error', '', 'Same Product with same qty ruled already. ');
				return false;
			}else{

				$http.post('../../schemeData/saveSchemeTwoData', vm.billInkDashboardService.item).then(successCallback, errorCallback);
			}
	});
		
	};
	
	vm.closeSchemeTwoDataSave = function() {
		vm.manageSchemeTwoDataService.startOfItem = 0;
		//vm.getCompanyList();
		//vm.manageSchemeTwoDataService.itemContainerURL = "../scheme/schemeTwo/view/manageSchemeTwo.html";
	};
	
	function successCallback(response) {
		if (response.data.status === "success") {
			
			toaster.showMessage('success','','Record Added successfully.');
			vm.closeSchemeTwoModel();
			vm.getCompayItemDetail(vm.selectedCompanyId);
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}
	
	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}
	
	vm.deleteSchemeTwo=function(deleteId){
		$http.post('../../schemeData/deleteSchemeTwo', {schemeTwoId:deleteId,companyId : vm.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
		.then(deletesuccessCallback, deleteerrorCallback);	
	}
	
	function deletesuccessCallback(response){
		toaster.showMessage('success','','Deleted successfully.');
		vm.manageSchemeTwoDataService.getCompanyItemList();
		vm.getSchme2List();
	}
	
	function deleteerrorCallback(response){
		
	}
	
	
});