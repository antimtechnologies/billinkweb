var addSchemeOneDataApp = angular.module('manageSchemeOne', [ 'infiniteScroll',
		'infinite-scroll','toaster' ]);

addSchemeOneDataApp.service('manageSchemeOneDataService', function($http) {
	
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfItem				= 0;
	svc.noOfRecordForItem		= 12;
	svc.itemList				= [];
	
	
});

addSchemeOneDataApp
		.controller(
				'addEditSchemeOneDataController',
				function($http, $ocLazyLoad, manageSchemeOneDataService,
						billInkDashboardService,toaster) {

					var vm = this;
					vm.schmeonestart=0;
					const totalrecords=30;
					vm.companyList=[];
					vm.manageSchemeOneDataService = manageSchemeOneDataService;
					vm.manageSchemeOneDataService.selectedCompanyId=0;
					vm.billInkDashboardService = billInkDashboardService;
					vm.manageSchemeOneDataService.itemContainerURL="";
					vm.totalInvoiceMrpFrom=0;
					vm.schemaOneListObj=[];
					vm.schemaTwoListObj=[];
					vm.selectedCompanyId=0;
					vm.startOfCompany =0;
					const noOfRecordForCompany 		= 30;
					vm.addSchemeOne = function() {
						vm.billInkDashboardService.schemeone.companyId=vm.selectedCompanyId;
						vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom=document.getElementById("totalInvoiceMrpFrom").value;
						if (vm.billInkDashboardService.schemeone.totalInvoiceMrpTo < vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom){
							toaster.showMessage("error", "", "You don't know that from is always smaller than To. validate your number.");
							return false;
						}
						vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom=vm.totalInvoiceMrpFrom;
						$http.post('../../schemeData/overruledSchemaOne', {companyId: vm.selectedCompanyId,totalInvoiceMrpFrom:vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom}).then(function(response){
							if(response.data.data=="exists"){
								toaster.showMessage('error', '', 'You can not overruled. You have to add more than that '+vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom);
								return false;
							}else{

								$http.post('../../schemeData/saveSchemeOneData',
								vm.billInkDashboardService.schemeone).then(
								successCallback, errorCallback);
							}
					});
					}
					
					
					vm.closeSchemeOneDataSave = function() {
						
						$("#add-schemeone-section-model .modal").modal('hide');
						
						};
					
					vm.showSchemeOneModel=function(){
						console.log(vm.schemaOneListObj[0]);
						if(vm.schemaOneListObj[0]){
								vm.totalInvoiceMrpFrom=vm.schemaOneListObj[0].totalInvoiceMrpTo+1;
						}else{
								vm.totalInvoiceMrpFrom=0;
						}
						console.log(vm.totalInvoiceMrpFrom);
						$("#add-schemeone-section-model .modal").modal('show');
						document.getElementById("totalInvoiceMrpFrom").value=vm.totalInvoiceMrpFrom;
						
						};
					
					
					
					vm.getSchme1List=function(){
						
						
						$http.post('../../schemeData/getSchemeOneData',{companyId: vm.selectedCompanyId,start: vm.schmeonestart, noOfRecord: totalrecords})
						.then(successCallbackSchemaOneList,errorCallbackSchemaOneList)
						
					}
					
					function successCallbackSchemaOneList(response) {
						vm.schemaOneListObj = angular.copy(response.data.data);
						
					}

					function errorCallbackSchemaOneList(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					vm.loadMoreCompanyList = function() {
						if (vm.totalCount > vm.startOfCompany) {			
							vm.startOfCompany += noOfRecordForCompany;
							vm.getCompanyList();
						}
					};
					
					vm.getCompanyListOnSearch = function() {
						vm.startOfCompany = 0;
						vm.getCompanyList();
					};
					
					vm.getCompanyList = function() {
						$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
						.then(getCompanyDataSuccessCallBack, errorCallback);
					};
					
					function getCompanyDataSuccessCallBack(response) {
						if (response.data.status === "success") {
							
							if (vm.startOfCompany === 0) {
								vm.companyList = response.data.data;
								vm.totalCount = response.data.totalCount;
								
								if (vm.companyList.length > 0) {
									vm.selectedCompanyId = vm.companyList[0].companyId;
									vm.manageSchemeOneDataService.selectedCompanyId=vm.selectedCompanyId;
									
								}
								return;
							}
							
							vm.companyList =response.data.data;
						}
					}
					
					
					
					
					
					function successCallbackSchemaTwoList(response) {
						vm.schemaTwoListObj = angular.copy(response.data.data);
					}

					function errorCallbackSchemaTwoList(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.uploadFileAndSaveSchemeData = function() {
						console.log("Save Btn click");
						vm.addSchemeOne();
					};

					vm.init = function(){
						vm.flag = 0;
						
						vm.getCompanyList();
						vm.addSchemeTwoData();
						vm.getSchme1List();
						//vm.getSchme2List();
						
					}
					
					
					vm.getCompayItemDetail = function(companyId) {
						vm.selectedCompanyId = companyId;
						vm.startOfItem = 0;
						vm.schemaOneListObj=[];
						vm.schemaTwoListObj=[];
						vm.getSchme1List();
//						vm.getSchme2List();
					};
					
					vm.gethighestfromamount=function(){
						vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom=0
					}
					
					vm.change = function(val){
						
						vm.flag = val;
						if(val == 0){
							$("#schemeOneId").addClass("active");
							$("#schemeTwoId").removeClass("active");
						}else{
							$("#schemeTwoId").addClass("active");
							$("#schemeOneId").removeClass("active");
						}
					}
					function successCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage('success','','Record Added successfully.');
							
							vm.billInkDashboardService.schemeone.totalInvoiceMrpFrom="";
							vm.billInkDashboardService.schemeone.totalInvoiceMrpTo="";
							vm.billInkDashboardService.schemeone.totalMrpOnDiscount="";
							vm.closeSchemeOneDataSave();
							//vm.getCompanyList();
							vm.getSchme1List();
//							vm.getSchme2List();
							
							// vm.closeItemDataSave();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}
					
					
					

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					
					// changed   
					
					vm.addSchemeTwoData = function() {
						$ocLazyLoad
						.load(
								["../scheme/schemeTwo/js/manageSchemeTwo.js"],
								{serie: true}		
						)
						.then(function () {
							vm.manageSchemeOneDataService.itemContainerURL = "../scheme/schemeTwo/view/addEditSchemeTwoData.html";
						});
					};
					
					
					vm.deleteSchemeOne=function(deleteId){
						$http.post('../../schemeData/deleteSchemeOne', {schemeOneId:deleteId,companyId : vm.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
						.then(deletesuccessCallback, deleteerrorCallback);	
					}
					
					
					vm.deleteSchemeTwo=function(deleteId){
						$http.post('../../schemeData/deleteSchemeTwo', {schemeTwoId:deleteId,companyId : vm.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId})
						.then(deletesuccessCallback, deleteerrorCallback);	
					}
					
					function deletesuccessCallback(response){
						toaster.showMessage('success','','Deleted successfully.');
						vm.getSchme1List();
					}
					
					function deleteerrorCallback(response){
						
					}
});


