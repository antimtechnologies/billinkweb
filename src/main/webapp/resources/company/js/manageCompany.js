angular
		.module('manageCompany', [ 'infinite-scroll', 'ui.bootstrap' ])
		.service("manageCompanyService", function($http) {

			var vm = this;
			vm.startOfCompany = 0;
			vm.companyList = [];
			vm.noOfRecordForCompany = 12;
			vm.totalCount = 0;
			vm.getCompanyList = function() {

				$http.post('../../companydata/getCompanyList', {
					start : vm.startOfCompany,
					noOfRecord : vm.noOfRecordForCompany,
					companyName : vm.searchCompanyNameText
				}).then(successCallback, errorCallback);
			};

			function successCallback(response) {
				if (response.data.status === "success") {
					if (vm.startOfCompany === 0) {
						vm.companyList = angular.copy(response.data.data);
						vm.totalCount = response.data.totalCount;
						return;
					}
					vm.companyList = vm.companyList.concat(response.data.data);
				}
			}

			function errorCallback() {

			}

		})
		.controller(
				'manageCompanyController',
				function($http, $ocLazyLoad, manageCompanyService,
						billInkDashboardService) {
					var vm = this;
					vm.manageCompanyService = manageCompanyService;
					vm.manageCompanyService.currentViewUrl = "../company/view/viewCompanyList.html";
					vm.billInkDashboardService = billInkDashboardService;
					

					vm.init = function() {
						vm.manageCompanyService.startOfCompany = 0;
						vm.manageCompanyService.companyList = [];
						vm.manageCompanyService.getCompanyList();
						vm.manageCompanyService.requestType="manage"
					};

					vm.loadMoreCompany = function() {
						if (vm.manageCompanyService.totalCount > vm.manageCompanyService.startOfCompany) {
							vm.manageCompanyService.startOfCompany += vm.manageCompanyService.noOfRecordForCompany;
							vm.manageCompanyService.getCompanyList();
						}
					};

					vm.searchCompanyData = function() {
						vm.manageCompanyService.startOfCompany = 0;
						vm.manageCompanyService.companyList = [];
						vm.manageCompanyService.getCompanyList();
					};

					vm.launchAddCompanyScreen = function() {
						vm.manageCompanyService.requestType = "Add";
						vm.manageCompanyService.startOfCompany = 0;
						vm.manageCompanyService.companyList = [];
						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../company/js/addEditCompany.js" ])
								.then(
										function() {
											vm.manageCompanyService.currentViewUrl = "../company/view/addEditCompany.html";
										});
					};

					vm.showDeletePrompt = function(companyId) {
						vm.selectedDeleteCompanyId = companyId;
						$("#company-delelete-confirmation-dialog .modal")
								.modal('show');
					};

					vm.deleteCompanyData = function() {
						$http.post('../../companydata/deleteCompanyData', {
							companyId : vm.selectedDeleteCompanyId,
							loggedInUserId : vm.billInkDashboardService.userId
						}).then(deleteCompanySuccessCallback, errorCallback);
					}

					function deleteCompanySuccessCallback() {
						vm.closeDeleteModal();
						vm.init();
					}

					vm.closeDeleteModal = function() {
						vm.selectedDeleteCompanyId = 0;
						$("#company-delelete-confirmation-dialog .modal")
								.modal('hide');
					};

					function errorCallback() {

					}

					vm.editCompanyData = function(companyId) {
						vm.manageCompanyService.requestType = "Edit";
						vm.manageCompanyService.editCompanyId = companyId;
						vm.manageCompanyService.startOfCompany = 0;
						vm.manageCompanyService.companyList = [];

						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../company/js/addEditCompany.js" ],
										{
											serie : true
										})
								.then(
										function() {
											vm.manageCompanyService.currentViewUrl = "../company/view/addEditCompany.html";
										});
					};

					vm.$onDestroy = function() {
						vm.manageCompanyService.startOfCompany = 0;
						vm.manageCompanyService.totalCount = 0;
						vm.manageCompanyService.companyList = [];
					};

				});
