angular.module('addCompany', ['fileUploadDirective', 'toaster'])
.controller('addEditCompanyController', function($http, $window, fileUploadService, toaster, manageCompanyService, billInkDashboardService) {
	var vm = this;
	vm.profilePhotoFile = null;
	vm.manageCompanyService = manageCompanyService;
	vm.stateList = [];
	vm.company = {};
	vm.billInkDashboardService = billInkDashboardService;
	vm.companyBusinessIndustry = [
		"Manufacturer",
		"Super Stockies",
		"Distributor",
		"Retailer",
		"Service Provider",
		"SEZ",
		"Manufacturer - Composition",
		"Super Stockies - Composition",
		"Distributor - Composition",
		"Retailer - Composition",
		"Service Provider - Composition",
	];
	vm.companyTypeList = ['Individual', 'HUF', 'Partenership', 'AOP/BOI', 'Private Limited', 'Public Limited', 'Trust'];
	vm.company.companyDocumentList = [];

	vm.init = function() {
		$http.post('../../stateData/getStateList', vm.company).then(vm.setStateList, vm.errorFetchData);
		if (vm.manageCompanyService.requestType === "Edit") {
			$http.post('../../companydata/getCompanyDetails', { companyId: vm.manageCompanyService.editCompanyId}).then(vm.setCompanyDetail, vm.errorFetchData);			
		} else {
			vm.company = {};
			vm.company.companyDocumentList = [];
			vm.company.addedBy = {};
			vm.company.addedBy.userId = vm.billInkDashboardService.userId;
		}
	};

	vm.setCompanyDetail = function(response) {
		vm.company = angular.copy(response.data.data);
		vm.selectedState = vm.company.state.stateName;
	};

	vm.errorFetchData = function(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	};

	vm.setStateList = function(response) {
		vm.stateList = response.data.data;
	};

	vm.addCompany = function() {
		if (!vm.company.companyName || vm.company.companyName === "") {
			toaster.showMessage('error', '', 'Kindly enter company name.');
			return false;
		}

		$http.post('../../companydata/saveCompanyData', vm.company).then(successCallback, errorCallback);
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			toaster.showMessage('error', '', 'data saved successfully.');
			vm.closeCompanyAddScreen();
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	vm.closeCompanyAddScreen = function() {
		vm.manageCompanyService.editCompanyId = 0;
		vm.manageCompanyService.startOfCompany = 0;
		vm.manageCompanyService.requestType = "";
		vm.manageCompanyService.getCompanyList();
		vm.manageCompanyService.currentViewUrl = "../company/view/viewCompanyList.html";
	};

	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}

	vm.uploadFileAndSaveCompanyData = function () {
		var file = vm.profilePhotoFile;

		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'company').then(function(result){
				if (result.data.status === "success") {				
					vm.company.profilePhotoURL = result.data.imagePath;
					vm.addCompany();
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading document');
			});
		} else {
			vm.company.profilePhotoURL = null;
			vm.addCompany();
		}
	};

	vm.updateCompany = function() {
		if (!vm.company.companyName && vm.company.companyName === "") {
			toaster.showMessage('error', '', 'Kindly enter company name.');
			return false;
		}

		$http.post('../../companydata/updateCompanyData', vm.company).then(updateCompanySuccessCallBack, errorCallback);
	};

	function updateCompanySuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.billInkDashboardService.userRole === "Vendor") {
//				$window.location.reload();
			}
			toaster.showMessage('error', '', 'data saved successfully.');
			vm.closeCompanyAddScreen();
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	vm.uploadFileAndUpdateCompanyData = function() {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'company').then(function(result){
				if (result.data.status === "success") {				
					vm.company.profilePhotoURL = result.data.imagePath;
					vm.updateCompany();
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading document');
			});
		} else {
			vm.updateCompany();
		}
	};

	vm.addDocument = function() {
		angular.element("input[type='file']").val(null);
		vm.companyDocumentObj = {};
		vm.companyDocumentFile = "";
		$("#add-document-dialog .modal").modal('show');
	};

	vm.closedDocumentModal = function() {
		$("#add-document-dialog .modal").modal('hide');
	};

	vm.saveDocument = function() {
		var file = vm.companyDocumentFile;
		if (vm.companyDocumentFile) {			
			fileUploadService.uploadFileToUrl(file, 'company').then(function(result){
				if (result.data.status === "success") {				
					vm.companyDocumentObj.documentUrl = result.data.imagePath;
					vm.company.companyDocumentList.push(vm.companyDocumentObj);
				}
				vm.companyDocumentObj = {};
				vm.companyDocumentFile = undefined;
				$("#add-document-dialog .modal").modal('hide');
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading document');
			});
		}
	};

	vm.deleteDocument = function(index) {
		vm.company.companyDocumentList.splice(index, 1);
	};

});