angular
		.module('addPurchaseData', [ 'fileUploadDirective', 'toaster' ])
		.controller(
				'addPurchaseDataController',
				function($http, $ocLazyLoad, fileUploadService, toaster,
						managePurchaseDataService, billInkDashboardService) {

					var vm = this;

					vm.managePurchaseDataService = managePurchaseDataService;
					vm.managePurchaseDataService.ledgerContainerURL = "";
					vm.profilePhotoFile = null;
					vm.stateList = [];
					vm.purchase = {};
					vm.purchase.imageURLList = [];
					// vm.purchase.purchaseItemModel = [];
					vm.numberOfAddedItem = 1;
					vm.numberOfItem = new Array(vm.numberOfAddedItem);
					vm.purchase.purchaseItemModel = [];
					// vm.purchase.purchaseItemModel[vm.numberOfAddedItem - 1] =
					// {};
					vm.numberOfAddedPurchaseCharge = 1;
					vm.numberOfPurchaseCharge = new Array(vm.numberOfAddedPurchaseCharge);
//					getLedgerCityList=null;
					vm.ledgerCityList=null;
					vm.purchase.purchaseItemModel[vm.numberOfAddedItem - 1] = {};
					vm.purchase.purchaseChargeList = [];
					vm.purchase.purchaseChargeList[vm.numberOfAddedPurchaseCharge - 1] = {};
					// vm.numberOfItem = new Array(vm.numberOfAddedItem);
					vm.billInkDashboardService = billInkDashboardService;
					vm.taxCodeList = [];

					vm.locationList = [];

					vm.calculateIGST = false;
					vm.calculateSGST = false;
					vm.calculateCGST = false;

					vm.init = function() {
						$http.post('../../taxcodedata/getTaxCodeValueList', {})
								.then(vm.setTaxCodeList, vm.errorFetchData);

						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						console.log("company==== :" + vm.purchase.companyId);
						if (vm.managePurchaseDataService.requestType === "Edit") {
							$http
									.post(
											'../../purchaseData/getPurchaseDetailsById',
											{
												purchaseId : vm.managePurchaseDataService.editPurchaseId,
												companyId : vm.managePurchaseDataService.selectedCompanyId
											}).then(vm.setPurchaseDetail,
											vm.errorFetchData);
						} else {
							vm.purchase.addedBy = {};
							vm.purchase.addedBy.userId = vm.billInkDashboardService.userId;
						}
						$http
								.post(
										'../../companydata/getCompanyDetails',
										{
											companyId : vm.managePurchaseDataService.selectedCompanyId
										}).then(vm.setCompanyDetail,
										vm.errorFetchData);
						$http.get(
								'../../locationData/findbycompanyId?companyId='
										+ vm.purchase.companyId).then(
								function(response) {
									vm.locationList = response.data.result;
								}, vm.errorFetchData);
					};

					vm.addLedgerData = function() {
						billInkDashboardService
								.loadMenuPage({

									iconImgURL : "../images/manage_ledger.png",
									menuName : "Manage Ledger",
									url : "../ledger/view/manageLedger.html",
									js : [
											'../customdirective/infine-scroll-directive.js',
											'../ledger/js/manageLedger.js' ]

								});

					};

					vm.setTaxCodeList = function(response) {
						if (response.data.status === "success") {
							vm.taxCodeList = response.data.data;
						} else {

						}
					};

					// vm.getLocationList = function(index) {
					// // console.log("success :"+index);
					// $http
					// .get(
					// '../../locationData/findbycompanyId?companyId='
					// + vm.purchase.companyId)
					// .then(
					// function(response) {
					// console.log("resp :"
					// + response.data.result);
					// vm.locationList[index] = response.data.result;
					// }, vm.errorFetchData);
					//
					// };

					vm.AddItem = function() {
						vm.numberOfAddedItem += 1;
						vm.purchase.purchaseItemModel[vm.numberOfAddedItem - 1] = {}
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.deletePurchaseItem = function(index) {
						vm.numberOfAddedItem -= 1;
						vm.purchase.purchaseItemModel.splice(index, 1);
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.setPurchaseDetail = function(response) {
						vm.purchase = angular.copy(response.data.data);
						vm.searchLedgerNameText = vm.purchase.ledgerData.ledgerName;
						vm.purchase.purchaseDateObj = new Date(
								vm.purchase.purchaseDate);
						vm.purchase.billDate = new Date(vm.purchase.billDate);
						vm.purchase.modifiedBy = {};
						vm.purchase.modifiedBy.userId = vm.billInkDashboardService.userId;
						setGSTCalculationFlag();
						console.log("Checking");
						console.log(vm.purchase.purchaseItemModel);
						if (vm.purchase.purchaseItemModel
								&& vm.purchase.purchaseItemModel.length > 0) {
							vm.numberOfAddedItem = vm.purchase.purchaseItemModel.length;
							vm.numberOfItem = new Array(vm.numberOfAddedItem);
						}
						
						if (!vm.purchase.purchaseItemModel
								|| vm.purchase.purchaseItemModel.length === 0) {
							vm.purchase.purchaseItemModel = [];
							vm.purchase.purchaseItemModel[vm.numberOfAddedItem - 1] = {};
						}
						console.log("Purchase Charge List length :");
						console.log(vm.purchase.purchaseChargeList.length);
						if (vm.purchase.purchaseChargeList
								&& vm.purchase.purchaseChargeList.length > 0) {
							vm.numberOfAddedPurchaseCharge = vm.purchase.purchaseChargeList.length;
							vm.numberOfPurchaseCharge = new Array(
									vm.numberOfAddedPurchaseCharge);
						}
						if (!vm.purchase.purchaseChargeList
								|| vm.purchase.purchaseChargeList.length === 0) {
							vm.purchase.purchaseChargeList = [];
							vm.purchase.purchaseChargeList[vm.numberOfAddedPurchaseCharge - 1] = {};
						}
						
						vm.ledgerId = vm.purchase.ledgerData.ledgerId;
						console.log("vm.purchase.ledgerData.ledgerId :"
								+ vm.purchase.ledgerData.ledgerId);

						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : vm.purchase.ledgerData.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);

						//						
						// $http.post('../../ledgerData/getMultipleGstDataByCityName',
						// {ledgerId: vm.purchase.ledgerData.ledgerId,city:
						// vm.purchase.cityName})
						// .then(function(response) {
						// vm.ledgerGstList = response.data.data;
						// }, errorCallback);
						//		
						
						for (var j = 0; j < vm.numberOfItem.length; j++) {
							var myVar = setInterval(getBatchItemDetails(j, vm.purchase.purchaseItemModel[j].item.itemId), 1000);
							vm.changeAmount(j);
						}
						

					};

					function getBatchItemDetails(i, itemId) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : itemId
										})
								.then(
										function(batchres) {

											vm.purchase.purchaseItemModel[i].batchNoList = batchres.data.data;
											//vm.purchase.purchaseItemModel[i].item.batchno = batchres.data.data.batchNo;

											$http
													.post(
															'../../itemData/getoneBatch',
															{
																batchId : vm.purchase.purchaseItemModel[i].batchId
															})
													.then(
															function(res) {
																var dt = formatDateForPurchase(res.data.data.mfgDate);
																var pt = formatDateForPurchase(res.data.data.expDate);

																vm.purchase.purchaseItemModel[i].item.mfgDate = dt;
																vm.purchase.purchaseItemModel[i].item.expDate = pt;
																// vm.sell.sellItemModel[k].batchId
																// =
																// res.data.data.batchId;

																var dtp = formatDate(res.data.data.mfgDate);
																var ptp = formatDate(res.data.data.expDate);
																vm.purchase.purchaseItemModel[i].batchNo = res.data.data.batchNo;
																vm.purchase.purchaseItemModel[i].mfgdateForPrint = dtp;
																vm.purchase.purchaseItemModel[i].expdateForPrint = ptp;

															}, errorCallback);

										}, errorCallback);

					}

					vm.errorFetchData = function(response) {
						toaster.showMessage('error', "", response.data.errMsg);
					};

					vm.closeAddEditPurchaseScreen = function() {
						vm.managePurchaseDataService
								.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
						vm.billInkDashboardService.currentViewUrl = "../purchase/view/viewPurchaseList.html";
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeAddEditPurchaseScreen();
							toaster.showMessage('success', '',
									'data saved successfully.');
						} else if (response.data.status === "error") {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}

					vm.savePurchaseData = function() {
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						vm.purchase.purchaseDate = moment(
								vm.purchase.purchaseDateObj).format(
								"YYYY-MM-DD");
						vm.purchase.billDate = moment(vm.purchase.billDate)
								.format("YYYY-MM-DD");
						console.log(vm.purchase.billDate);

						console.log("free ItemId :"
								+ vm.purchase.purchaseItemModel[0].freeItemId);
						console
								.log("free qun :"
										+ vm.purchase.purchaseItemModel[0].freequantity);
						$http.post('../../purchaseData/savePurchaseData',
								vm.purchase).then(successCallback,
								errorCallback);
					};

					vm.publishPurchaseData = function() {
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId
						vm.purchase.purchaseDate = moment(
								vm.purchase.purchaseDateObj).format(
								"YYYY-MM-DD");
						$http.post('../../purchaseData/publishPurchaseData',
								vm.purchase).then(successCallback,
								errorCallback);
					};

					vm.getNormalDiscount = function(index) {
						if (vm.purchase.purchaseItemModel[index]) {
							$http
									.post('../../ledgerData/getDiscount', {
										ledgerId : vm.ledgerId
									})
									.then(
											function(response) {
												console.log("getDiscount");
												console.log(response.data.data);
												vm.purchase.purchaseItemModel[index].discount = response.data.data;
											}, errorCallback);

						}
					}

					vm.getpurchaseData = function(index) {
						//						
						var billdateobj = "";
						if (vm.purchase.billDate) {
							billdateobj = ((new Date(vm.purchase.billDate))
									.getTime() / 1000);
						} else {
							billdateobj = 000;
						}
						$http
								.post(
										"../../itemData/getPurchaseRate",
										{
											itemId : vm.purchase.purchaseItemModel[index].item.itemId,
											billDate : billdateobj
										})
								.then(
										function(response) {
											vm.purchase.purchaseItemModel[index].purchaseRate = response.data.data[0].itemPurchaseRate;
											// console.log();
										}, errorCallback);

					}

					function getImageURLListObj(imageURLList) {
						var purchaseImageURLList = [];
						var purchaseImageURL = {};

						for (var count = 0; count < imageURLList.length; count++) {
							purchaseImageURL = {};
							purchaseImageURL.imageURL = imageURLList[count];
							purchaseImageURLList.push(purchaseImageURL);
						}

						return purchaseImageURLList;
					}

					function validatePurchaseSaveUpdate() {
						if (!vm.managePurchaseDataService.selectedCompanyId
								|| vm.managePurchaseDataService.selectedCompanyId < 1) {
							toaster
									.showMessage('error', '',
											'Company data is missing to save purchase details');
							return false;
						}

						// if (!vm.purchase.ledgerData ||
						// vm.purchase.ledgerData.ledgerId < 1) {
						// toaster.showMessage('error', '', 'Kindly select
						// ledger data to save purchase details');
						// return false;
						// }

						return true;
					}

					vm.uploadFileAndSavePurchaseData = function(action) {
						if (!validatePurchaseSaveUpdate()) {
							return;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'purchase')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.purchase.imageURLList = vm.purchase.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.purchase.imageURLList
															&& vm.purchase.imageURLList.length > 0) {
														vm.purchase.imageURL = vm.purchase.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm.savePurchaseData();
													} else if (action === "Publish") {
														vm
																.publishPurchaseData();
													}
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading files');
											});
						} else {
							vm.purchase.imageURL = null;
							if (vm.purchase.imageURLList
									&& vm.purchase.imageURLList.length > 0) {
								vm.purchase.imageURL = vm.purchase.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.savePurchaseData();
							} else if (action === "Publish") {
								vm.publishPurchaseData();
							}
						}
					};

					vm.updateSavedPurchaseData = function() {
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						vm.purchase.purchaseDate = moment(
								vm.purchase.purchaseDateObj).format(
								"YYYY-MM-DD");
						$http.post(
								'../../purchaseData/updateSavedPurchaseData',
								vm.purchase).then(successCallback,
								errorCallback);
					};

					vm.updatePublishPurchaseData = function() {
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						vm.purchase.purchaseDate = moment(
								vm.purchase.purchaseDateObj).format(
								"YYYY-MM-DD");
						$http
								.post(
										'../../purchaseData/updatePublishedPurchaseData',
										vm.purchase).then(successCallback,
										errorCallback);
					};

					vm.uploadFileAndUpdatePurchaseData = function(action) {
						if (!validatePurchaseSaveUpdate()) {
							return;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'purchase')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.purchase.imageURLList = vm.purchase.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.purchase.imageURLList
															&& vm.purchase.imageURLList.length > 0) {
														vm.purchase.imageURL = vm.purchase.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.updateSavedPurchaseData();
													} else if (action === "Publish") {
														vm
																.updatePublishPurchaseData();
													}
												}
											},
											function(error) {
												toaster
														.showMessage('error',
																'',
																'error while uploading files');
											});
						} else {
							vm.purchase.imageURL = null;
							if (vm.purchase.imageURLList
									&& vm.purchase.imageURLList.length > 0) {
								vm.purchase.imageURL = vm.purchase.imageURLList[0].imageURL;
							}
							vm.purchase.imageURLList = []
							if (action === "Save") {
								vm.updateSavedPurchaseData();
							} else if (action === "Publish") {
								vm.updatePublishPurchaseData();
							}
						}
					};

					vm.markPurchaseAsVerified = function() {
						vm.purchase.verifiedBy = {};
						vm.purchase.verifiedBy.userId = vm.billInkDashboardService.userId;
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						$http.post('../../purchaseData/markPurchaseAsVerified',
								vm.purchase).then(successCallback,
								errorCallback);
					};

					vm.publishSavedPurchase = function() {
						vm.purchase.companyId = vm.managePurchaseDataService.selectedCompanyId;
						$http.post('../../purchaseData/publishSavedPurchase',
								vm.purchase).then(successCallback,
								errorCallback);
					};

					vm.searchLedger = function() {
						if (vm.purchase.ledgerData
								&& vm.purchase.ledgerData.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];
							return;
						}
						if (vm.searchLedgerNameText.length > 0) {
							$http.post('../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchLedgerNameText,
												companyId : vm.managePurchaseDataService.selectedCompanyId
											}).then(
											getLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.searchLedgerList = [];
							vm.calculateIGST = false;
							vm.calculateSGST = false;
							vm.calculateCGST = false;
							vm.purchase.ledgerData = {};
						}
					};

					function getLedgerDataSuccessCallBack(response) {
						vm.purchase.ledgerData = {};
						console.log("ledget object:"+response.data.data);
						vm.calculateIGST = false;
						vm.calculateSGST = false;
						vm.calculateCGST = false;
						vm.searchLedgerList = angular.copy(response.data.data);
						return vm.searchLedgerList;

					}

					vm.selectLedger = function(ledgerObj) {
						if (!ledgerObj || !ledgerObj.ledgerName) {
							return;
						}
						vm.searchLedgerNameText = ledgerObj.ledgerName;
						vm.purchase.ledgerData = ledgerObj;
						vm.purchase.cityName = ledgerObj.cityName;
						vm.purchase.gstNumber = ledgerObj.gstNumber;
						vm.ledgerId = ledgerObj.ledgerId;
						console.log("ledgerId :" + ledgerObj.ledgerId);
						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : ledgerObj.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);
						setGSTCalculationFlag();
					};

					vm.getLedgerCityList = function() {
						console.log("Selected LedgerCity call");
						var flag = true;
						console.log("vm.debitNote.cityName :"
								+ vm.ledgerCityList);
						angular
								.forEach(
										vm.ledgerCityList,
										function(ledgerList) {

											if (flag == true
													&& vm.purchase.cityName
															.toLowerCase() === ledgerList.city
															.toLowerCase()) {
												//								
												console.log(ledgerList);
												// console.log(vm.purchase.cityName.toLowerCase());
												vm.purchase.ledgerData.state.stateId=ledgerList.stateid;
												vm.purchase.gstNumber = ledgerList.multipleGst;
												flag = false;
												return;
											}
										});
						setGSTCalculationFlag();

					};

					function setGSTCalculationFlag() {
//						if (vm.billInkDashboardService.igstTaxCalGSTType
//								.indexOf(vm.purchase.ledgerData.gstType) > -1) {
//							vm.calculateIGST = true;
//						} else {
							if (vm.purchase.ledgerData
									&& vm.billInkDashboardService.currentCompanyObj
									&& vm.purchase.ledgerData.state
									&& vm.purchase.ledgerData.state.stateId === vm.billInkDashboardService.currentCompanyObj.state.stateId) {
								vm.calculateSGST = true;
								vm.calculateCGST = true;
								vm.calculateIGST = false;
							} else {
								vm.calculateIGST = true;
								vm.calculateSGST=false;
								vm.calculateCGST=false;
							}
//						}
					}
					vm.fetchItemList = function(index) {
						
//						console.log(vm.searchItemNameText);
						var itemSearchText = vm.purchase.purchaseItemModel[index].item.itemName;
						// if (itemSearchText.length > 2) {
						return $http
								.post(
										'../../itemData/getItemBasicDetailList',
										{
											itemName : itemSearchText,
											companyId : vm.managePurchaseDataService.selectedCompanyId,
											start : 0,
											noOfRecord : 30
										})
								.then(
										function(response) {
											vm.itemList = [];
											vm.itemList[index] = {};
											vm.itemList[index].itemData = response.data.data;
											//vm.getLocationList(index);
											vm.getBatchnolist(index);
											vm.getNormalDiscount();
											return vm.itemList[index].itemData;
										}, errorCallback);
						// }
					};

					vm.selectItem = function(index, itemObj) {
						if (!itemObj) {
							return;
						}

						if (vm.purchase.purchaseItemModel[index]
								&& vm.purchase.purchaseItemModel[index].purchaseItemMappingId > 0) {
							vm.purchase.purchaseItemModel[index].purchaseItemMappingId = 0;
							return;
						}

						vm.purchase.purchaseItemModel[index].item = {};
						vm.purchase.purchaseItemModel[index].item.itemName = itemObj.itemName;
						vm.purchase.purchaseItemModel[index].item.itemId = itemObj.itemId;
						vm.purchase.purchaseItemModel[index].item.hsnCode = itemObj.hsnCode;
						vm.purchase.purchaseItemModel[index].taxRate = itemObj.taxCode;
						vm.purchase.purchaseItemModel[index].purchaseRate = itemObj.purchaseRate;
						vm.purchase.purchaseItemModel[index].taxRatefreeItem = itemObj.taxCode;
						vm.ItemId = vm.purchase.purchaseItemModel[index].item.itemId;
						vm.purchase.purchaseItemModel[index].quantity = 1;

						vm.billInkDashboardService.purchseItemId = vm.purchase.purchaseItemModel[index].item.itemId;

						console
								.log("vm.billInkDashboardService.purchseItemId :"
										+ vm.billInkDashboardService.purchseItemId);

						// if
						// (vm.billInkDashboardService.noTaxCodeGSTType.indexOf(vm.purchase.ledgerData.gstType)
						// > -1) {
						// vm.purchase.purchaseItemModel[index].taxRate = 0;
						// }

						if (vm.purchase.ledgerData
								&& vm.billInkDashboardService.noTaxCodeGSTType
										.indexOf(vm.purchase.ledgerData.gstType) > -1) {
							vm.purchase.purchaseItemModel[index].taxRate = 0;

						}

						vm.getpurchaseData(index);
						vm.getBatchnolist(index);
						// vm.getNormalDiscount(index);
						$http
								.post(
										'../../schemeData/getFreeItemByItemId',
										{
											itemId : vm.purchase.purchaseItemModel[index].item.itemId
										})
								.then(
										function(response) {

											// vm.purchase.purchaseItemModel[index].freeItemrowId
											// =
											// response.data.data.totalQtyOfFreeItem;

											vm.purchase.purchaseItemModel[index].freeItemId = response.data.data.freeItemId;
											vm.totalQtyOfFreeItems = response.data.data.totalQtyOfItem;

											vm.purchase.purchaseItemModel[index].freequantity = response.data.data.totalQtyOfFreeItem;
											$http
													.post(
															'../../schemeData/getFreeItemNameByItemId',
															{
																itemId : response.data.data.freeItemId
															})
													.then(
															function(res) {
																vm.purchase.purchaseItemModel[index].FreeitemName = res.data.data.itemName;
																vm
																		.changeAmount(index);
															}, errorCallback);
										}, errorCallback);

					};

					vm.getBatchnolist = function(index) {
						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : vm.purchase.purchaseItemModel[index].item.itemId
										})
								.then(
										function(batchres) {
											vm.purchase.purchaseItemModel[index].batchNoList = batchres.data.data;
											// vm.purchase.purchaseItemModel[index].item.batchNo=
											// batchres.data.data.batchNo;

										}, errorCallback);

					}

					function formatDateForPurchase(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						console.log("date of d:" + d);
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					// 15-05-2019 Start
					vm.getOneBatch = function(index) {
						console.log("getOneFucntion call");
						console.log("Fucntion Index:" + index);
						console.log("BatchId :"
								+ vm.purchase.purchaseItemModel[index].batchId);
						if (vm.purchase.purchaseItemModel[index].batchId
								&& vm.purchase.purchaseItemModel[index].batchId != undefined) {
							console
									.log("If batchID :"
											+ vm.purchase.purchaseItemModel[index].batchId);
							$http
									.post(
											'../../itemData/getoneBatch',
											{
												batchId : vm.purchase.purchaseItemModel[index].batchId
											})
									.then(
											function(res) {
												var dt = formatDateForPurchase(res.data.data.mfgDate);
												var pt = formatDateForPurchase(res.data.data.expDate);

												var dtp = formatDate(res.data.data.mfgDate);
												var ptp = formatDate(res.data.data.expDate);

												vm.purchase.purchaseItemModel[index].item.mfgDate = dt;
												vm.purchase.purchaseItemModel[index].item.expDate = pt;
												console
														.log("batch No :"
																+ res.data.data.batchNo);
												vm.purchase.purchaseItemModel[index].batchNo = res.data.data.batchNo;
												// vm.purchase.purchaseItemModel[index].batchId
												// = res.data.data.batchId;
												vm.purchase.purchaseItemModel[index].mfgdateForPrint = dtp;
												vm.purchase.purchaseItemModel[index].expdateForPrint = ptp;

											}, errorCallback);

						}

					}
					// 15-05-2019 End

					// item batch No Search Start
					vm.fetchBatchNoItemList = function(index) {
						var itemBatchNoSearchText = vm.purchase.purchaseItemModel[index].item.batchNo;
						console.log("ItemBatch No :" + itemBatchNoSearchText);
						// if (itemSearchText.length > 2) {
						$http
								.post(
										'../../schemeData/getBatchItemDetails',
										{
											batchNo : itemBatchNoSearchText,
											itemId : vm.purchase.purchaseItemModel[index].item.itemId
										})
								.then(
										function(response) {
											vm.itemBatchNoList = [];
											vm.itemBatchNoList[index] = {};
											vm.itemBatchNoList[index].itemBatchData = response.data.data;

										}, errorCallback);
						// }
					};

					vm.changeAmount = function(index) {
						// console.log(index);
						if (index == -1) {
							vm.purchase.totalBillAmount = 0;
							vm.purchase.totalBillAmount = parseFloat(vm.purchase.grossTotalAmount);
							if (vm.purchase.discount
									&& vm.purchase.discount > 0) {

								if (vm.purchase.purchaseMeasureDiscountInAmount) {
									vm.purchase.totalBillAmount -= vm.purchase.discount;
								} else {
									vm.purchase.totalBillAmount -= (vm.purchase.totalBillAmount
											* vm.purchase.discount / 100);
								}

							}
							vm.purchase.totalBillAmount = parseFloat(
									vm.purchase.totalBillAmount).toFixed(2);
						} else {

							vm.purchase.purchaseItemModel[index].itemAmount = 0;
							vm.purchase.purchaseItemModel[index].totalItemAmount = 0;
							vm.purchase.purchaseItemModel[index].sgst = 0;
							vm.purchase.purchaseItemModel[index].cgst = 0;
							vm.purchase.purchaseItemModel[index].igst = 0;
							vm.purchase.grossTotalAmount = 0;
							vm.purchase.totalBillAmount = 0;

							var ItemAmount = 0, ItemAmountWithTax = 0, ItemDiscount = 0, ItemAmountWithTaxDiscount = 0;

							if (vm.purchase.purchaseItemModel[index].item
									&& vm.purchase.purchaseItemModel[index].item.itemId
									&& vm.purchase.purchaseItemModel[index].quantity
									&& vm.purchase.purchaseItemModel[index].purchaseRate) {

								ItemAmount = vm.purchase.purchaseItemModel[index].purchaseRate
										* vm.purchase.purchaseItemModel[index].quantity;

								ItemAmountWithTax = ItemAmount
										+ (ItemAmount
												* vm.purchase.purchaseItemModel[index].taxRate / 100);

								if (vm.purchase.purchaseItemModel[index].discount) {

									if (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
										ItemDiscount = vm.purchase.purchaseItemModel[index].discount;
										ItemAmountWithTaxDiscount = vm.purchase.purchaseItemModel[index].discount;
									} else {
										ItemDiscount = (ItemAmount
												* vm.purchase.purchaseItemModel[index].discount / 100);
										ItemAmountWithTaxDiscount = (ItemAmountWithTax
												* vm.purchase.purchaseItemModel[index].discount / 100);
									}
								}

								if (vm.purchase.purchaseItemModel[index].additionaldiscount) {

									if (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
										ItemAmount -= vm.purchase.purchaseItemModel[index].additionaldiscount;
										ItemAmountWithTax -= vm.purchase.purchaseItemModel[index].additionaldiscount;
									} else {
										ItemAmount -= (ItemAmount
												* vm.purchase.purchaseItemModel[index].additionaldiscount / 100);
										ItemAmountWithTax -= (ItemAmountWithTax
												* vm.purchase.purchaseItemModel[index].additionaldiscount / 100);
									}
								}

								ItemAmount -= ItemDiscount;
								ItemAmountWithTax -= ItemAmountWithTaxDiscount;
							}
							vm.purchase.purchaseItemModel[index].itemAmount = ItemAmount
									.toFixed(2);
							vm.purchase.purchaseItemModel[index].totalItemAmount = ItemAmountWithTax
									.toFixed(2);
							var taxAmount = (ItemAmount * vm.purchase.purchaseItemModel[index].taxRate) / 100;
							if (vm.calculateIGST)
								vm.purchase.purchaseItemModel[index].igst = taxAmount
										.toFixed(2);

							taxAmount = taxAmount / 2;
							if (vm.calculateSGST)
								vm.purchase.purchaseItemModel[index].sgst = taxAmount
										.toFixed(2);

							if (vm.calculateCGST)
								vm.purchase.purchaseItemModel[index].cgst = taxAmount
										.toFixed(2);

							for (var counter = 0; counter < vm.purchase.purchaseItemModel.length; counter++) {
								if (vm.purchase.purchaseItemModel[counter].totalItemAmount
										&& vm.purchase.purchaseItemModel[counter].totalItemAmount > 0) {
									vm.purchase.grossTotalAmount += parseFloat(vm.purchase.purchaseItemModel[counter].totalItemAmount);
								}
							}

							if (vm.purchase.purchaseChargeList.length > 0) {
								for (var count = 0; count < vm.purchase.purchaseChargeList.length; count++) {
									console
											.log("charge Total Amt :"
													+ vm.purchase.purchaseChargeList[count].totalItemAmount);
									if (vm.purchase.purchaseChargeList[count].totalItemAmount)
										vm.purchase.grossTotalAmount += parseFloat(vm.purchase.purchaseChargeList[count].totalItemAmount);
								}
							}

							vm.purchase.grossTotalAmount = parseFloat(
									vm.purchase.grossTotalAmount).toFixed(2);

							vm.purchase.totalBillAmount = parseFloat(vm.purchase.grossTotalAmount);
							if (vm.purchase.discount
									&& vm.purchase.discount > 0) {

								if (vm.purchase.purchaseMeasureDiscountInAmount) {
									vm.purchase.totalBillAmount -= vm.purchase.discount;
								} else {
									vm.purchase.totalBillAmount -= (vm.purchase.totalBillAmount
											* vm.purchase.discount / 100);
								}

							}
							vm.purchase.totalBillAmount = parseFloat(
									vm.purchase.totalBillAmount).toFixed(2);
						}

					}

					/*
					 * vm.getAmount = function(index) { console.log("index");
					 * vm.purchase.purchaseItemModel[index].totalItemAmount = 0;
					 * if (vm.purchase.purchaseItemModel[index].item &&
					 * vm.purchase.purchaseItemModel[index].item.itemId &&
					 * vm.purchase.purchaseItemModel[index].quantity &&
					 * vm.purchase.purchaseItemModel[index].purchaseRate) {
					 * 
					 * vm.purchase.purchaseItemModel[index].totalItemAmount =
					 * vm.purchase.purchaseItemModel[index].purchaseRate
					 * vm.purchase.purchaseItemModel[index].quantity;
					 * vm.purchase.purchaseItemModel[index].totalItemAmount +=
					 * (vm.purchase.purchaseItemModel[index].totalItemAmount
					 * vm.purchase.purchaseItemModel[index].taxRate / 100); var
					 * calculateamout=vm.purchase.purchaseItemModel[index].totalItemAmount;
					 * 
					 * if (vm.purchase.purchaseItemModel[index].discount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
					 * vm.purchase.purchaseItemModel[index].totalItemAmount -=
					 * vm.purchase.purchaseItemModel[index].discount; } else {
					 * vm.purchase.purchaseItemModel[index].totalItemAmount -=
					 * (vm.purchase.purchaseItemModel[index].totalItemAmount
					 * vm.purchase.purchaseItemModel[index].discount / 100); } }
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
					 * vm.purchase.purchaseItemModel[index].totalItemAmount -=
					 * vm.purchase.purchaseItemModel[index].additionaldiscount; }
					 * else {
					 * vm.purchase.purchaseItemModel[index].totalItemAmount -=
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount
					 * calculateamout / 100); } } }
					 * vm.purchase.purchaseItemModel[index].totalItemAmount =
					 * vm.purchase.purchaseItemModel[index].totalItemAmount
					 * .toFixed(2); return
					 * vm.purchase.purchaseItemModel[index].totalItemAmount; };
					 */

					/*
					 * vm.getItemLevelAmount = function(index) {
					 * vm.purchase.purchaseItemModel[index].itemAmount = 0; if
					 * (vm.purchase.purchaseItemModel[index].item &&
					 * vm.purchase.purchaseItemModel[index].item.itemId &&
					 * vm.purchase.purchaseItemModel[index].quantity &&
					 * vm.purchase.purchaseItemModel[index].purchaseRate) { var
					 * itemAmount =
					 * vm.purchase.purchaseItemModel[index].purchaseRate
					 * vm.purchase.purchaseItemModel[index].quantity; var
					 * calculateamount=itemAmount;
					 * 
					 * if (vm.purchase.purchaseItemModel[index].discount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].discount; } else {
					 * itemAmount -= (itemAmount
					 * vm.purchase.purchaseItemModel[index].discount / 100); } }
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].additionaldiscount; }
					 * else { itemAmount -= (calculateamount
					 * vm.purchase.purchaseItemModel[index].additionaldiscount /
					 * 100); } }
					 * 
					 * vm.purchase.purchaseItemModel[index].itemAmount =
					 * parseFloat( itemAmount).toFixed(2); } return
					 * vm.purchase.purchaseItemModel[index].itemAmount; };
					 */

					/*
					 * vm.getSGSTAmount = function(index) {
					 * 
					 * vm.purchase.purchaseItemModel[index].sgst = 0;
					 * 
					 * if (vm.calculateSGST) { if
					 * (vm.purchase.purchaseItemModel[index].item &&
					 * vm.purchase.purchaseItemModel[index].item.itemId &&
					 * vm.purchase.purchaseItemModel[index].quantity &&
					 * vm.purchase.purchaseItemModel[index].purchaseRate) { var
					 * itemAmount =
					 * vm.purchase.purchaseItemModel[index].purchaseRate
					 * vm.purchase.purchaseItemModel[index].quantity; var
					 * calculateamount=itemAmount;
					 * 
					 * if (vm.purchase.purchaseItemModel[index].discount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].discount; } else {
					 * itemAmount -= (itemAmount
					 * vm.purchase.purchaseItemModel[index].discount / 100); } }
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].additionaldiscount; }
					 * else { itemAmount -= (calculateamount
					 * vm.purchase.purchaseItemModel[index].additionaldiscount /
					 * 100); } }
					 * 
					 * var taxAmount = (itemAmount
					 * vm.purchase.purchaseItemModel[index].taxRate / 100);
					 * vm.purchase.purchaseItemModel[index].sgst = taxAmount /
					 * 2; } } vm.purchase.purchaseItemModel[index].sgst =
					 * parseFloat( vm.purchase.purchaseItemModel[index].sgst)
					 * .toFixed(2); return
					 * vm.purchase.purchaseItemModel[index].sgst; };
					 */

					/*
					 * vm.getCGSTAmount = function(index) {
					 * 
					 * vm.purchase.purchaseItemModel[index].cgst = 0; if
					 * (vm.calculateCGST) { if
					 * (vm.purchase.purchaseItemModel[index].item &&
					 * vm.purchase.purchaseItemModel[index].item.itemId &&
					 * vm.purchase.purchaseItemModel[index].quantity &&
					 * vm.purchase.purchaseItemModel[index].purchaseRate) { var
					 * itemAmount =
					 * vm.purchase.purchaseItemModel[index].purchaseRate
					 * vm.purchase.purchaseItemModel[index].quantity;
					 * 
					 * var calculateamount=itemAmount;
					 * 
					 * if (vm.purchase.purchaseItemModel[index].discount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].discount; } else {
					 * itemAmount -= (itemAmount
					 * vm.purchase.purchaseItemModel[index].discount / 100); } }
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].additionaldiscount; }
					 * else { itemAmount -= (calculateamount
					 * vm.purchase.purchaseItemModel[index].additionaldiscount /
					 * 100); } } var taxAmount = (itemAmount
					 * vm.purchase.purchaseItemModel[index].taxRate / 100);
					 * vm.purchase.purchaseItemModel[index].cgst = taxAmount /
					 * 2; } } vm.purchase.purchaseItemModel[index].cgst =
					 * parseFloat( vm.purchase.purchaseItemModel[index].cgst)
					 * .toFixed(2); return
					 * vm.purchase.purchaseItemModel[index].cgst; };
					 */

					/*
					 * vm.getIGSTAmount = function(index) {
					 * vm.purchase.purchaseItemModel[index].igst = 0;
					 * 
					 * if (vm.calculateIGST) {
					 * 
					 * if (vm.purchase.purchaseItemModel[index].item &&
					 * vm.purchase.purchaseItemModel[index].item.itemId &&
					 * vm.purchase.purchaseItemModel[index].quantity &&
					 * vm.purchase.purchaseItemModel[index].purchaseRate) { var
					 * itemAmount =
					 * vm.purchase.purchaseItemModel[index].purchaseRate
					 * vm.purchase.purchaseItemModel[index].quantity; var
					 * calculateamount=itemAmount; if
					 * (vm.purchase.purchaseItemModel[index].discount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].measureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].discount; } else {
					 * itemAmount -= (itemAmount
					 * vm.purchase.purchaseItemModel[index].discount / 100); } }
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].additionaldiscount) {
					 * 
					 * if
					 * (vm.purchase.purchaseItemModel[index].addmeasureDiscountInAmount) {
					 * itemAmount -=
					 * vm.purchase.purchaseItemModel[index].additionaldiscount; }
					 * else { itemAmount -= (calculateamount
					 * vm.purchase.purchaseItemModel[index].additionaldiscount /
					 * 100); } } var taxAmount = (itemAmount
					 * vm.purchase.purchaseItemModel[index].taxRate / 100);
					 * vm.purchase.purchaseItemModel[index].igst = taxAmount; } }
					 * vm.purchase.purchaseItemModel[index].igst = parseFloat(
					 * vm.purchase.purchaseItemModel[index].igst) .toFixed(2);
					 * return vm.purchase.purchaseItemModel[index].igst; };
					 */

					/*
					 * vm.getGrossTotalAmount = function() {
					 * vm.purchase.grossTotalAmount = 0; for (var counter = 0;
					 * counter < vm.purchase.purchaseItemModel.length;
					 * counter++) { if
					 * (vm.purchase.purchaseItemModel[counter].totalItemAmount &&
					 * vm.purchase.purchaseItemModel[counter].totalItemAmount >
					 * 0) { vm.purchase.grossTotalAmount +=
					 * parseFloat(vm.purchase.purchaseItemModel[counter].totalItemAmount); } }
					 * 
					 * if (vm.purchase.purchaseChargeList.length > 0) { for (var
					 * count = 0; count < vm.purchase.purchaseChargeList.length;
					 * count++) { if
					 * (vm.purchase.purchaseChargeList[count].totalItemAmount)
					 * vm.purchase.grossTotalAmount +=
					 * parseFloat(vm.purchase.purchaseChargeList[count].totalItemAmount); } }
					 * 
					 * vm.purchase.grossTotalAmount = parseFloat(
					 * vm.purchase.grossTotalAmount).toFixed(2); return
					 * vm.purchase.grossTotalAmount; };
					 */

					/*
					 * vm.getTotalBillAmount = function() {
					 * vm.purchase.totalBillAmount =
					 * parseFloat(vm.purchase.grossTotalAmount);
					 * //console.log("amount :"+vm.purchase.totalBillAmount); if
					 * (vm.purchase.discount && vm.purchase.discount > 0) {
					 * 
					 * if (vm.purchase.purchaseMeasureDiscountInAmount) {
					 * vm.purchase.totalBillAmount -= vm.purchase.discount; }
					 * else { vm.purchase.totalBillAmount -=
					 * (vm.purchase.totalBillAmount vm.purchase.discount / 100); } }
					 * vm.purchase.totalBillAmount = parseFloat(
					 * vm.purchase.totalBillAmount).toFixed(2); return
					 * vm.purchase.totalBillAmount; };
					 */

					vm.openImageModel = function(index) {
						if (index < 0 || index > (vm.purchase.imageURLList - 1)) {
							return;
						}

						vm.purchaseImageIndex = index;
						$("#purchase-image-show-model .modal").modal('show');
						vm.selectedPhotoURL = vm.purchase.imageURLList[index].imageURL;
					};

					vm.fetchImage = function(position) {
						vm.purchaseImageIndex += position;

						if (vm.purchaseImageIndex < 0) {
							vm.purchaseImageIndex = vm.purchase.imageURLList.length - 1;
						} else if (vm.purchaseImageIndex >= vm.purchase.imageURLList.length) {
							vm.purchaseImageIndex = 0;
						}
						vm.selectedPhotoURL = vm.purchase.imageURLList[vm.purchaseImageIndex].imageURL;
					};

					vm.deleteImage = function(index) {
						vm.purchase.imageURLList.splice(index, 1);
					};

					// Charge related method.
					vm.fetchChargeLedgerList = function(index) {
						var ledgerSearchText =vm.purchase.purchaseChargeList[index].ledger.ledgerName;
						if (ledgerSearchText.length > 2) {
							 $http.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : ledgerSearchText,
												companyId : vm.managePurchaseDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 30
											})
									.then(
											function(response) {
												vm.ledgerList = [];
												vm.ledgerList[index] = {};
												vm.ledgerList[index].ledgerData = response.data.data;
											 vm.ledgerList[index].ledgerData;
											}, errorCallback);
						}
					};

					vm.selectChargeLedger = function(index, ledgerObj) {
						if (!ledgerObj) {
							return;
						}
						
						if (vm.purchase.ledgerData && vm.purchase.purchaseChargeList[index] && vm.purchase.purchaseChargeList[index].purchaseChargeMappingId > 0) {
								vm.purchase.purchaseChargeList[index].purchaseChargeMappingId = 0;
									return;
						}

						vm.purchase.purchaseChargeList[index].ledger = {};
						vm.purchase.purchaseChargeList[index].ledger = ledgerObj;
						vm.purchase.purchaseChargeList[index].taxRate = ledgerObj.taxCode;

						if (vm.billInkDashboardService.noTaxCodeGSTType
								.indexOf(vm.purchase.ledgerData.gstType) > -1) {
							vm.purchase.purchaseChargeList[index].taxRate = 0;
						}
					};

					vm.getChargeAmount = function(index) {
						if (vm.purchase.purchaseChargeList[index]) {
							vm.purchase.purchaseChargeList[index].totalItemAmount = 0;
							vm.purchase.purchaseChargeList[index].quantity = 1;
							if (vm.purchase.purchaseChargeList[index].ledger
									&& vm.purchase.purchaseChargeList[index].ledger.ledgerId
									&& vm.purchase.purchaseChargeList[index].quantity
									&& vm.purchase.purchaseChargeList[index].purchaseCharge) {
								vm.purchase.purchaseChargeList[index].totalItemAmount = vm.purchase.purchaseChargeList[index].purchaseCharge
										* vm.purchase.purchaseChargeList[index].quantity;

								if (vm.purchase.purchaseChargeList[index].taxRate) {
									vm.purchase.purchaseChargeList[index].totalItemAmount += (vm.purchase.purchaseChargeList[index].totalItemAmount
											* vm.purchase.purchaseChargeList[index].taxRate / 100);
								}

								if (vm.purchase.purchaseChargeList[index].discount) {
									if (vm.purchase.purchaseChargeList[index].measureDiscountInAmount) {
										vm.purchase.purchaseChargeList[index].totalItemAmount -= vm.purchase.purchaseChargeList[index].discount;
									} else {
										vm.purchase.purchaseChargeList[index].totalItemAmount -= (vm.purchase.purchaseChargeList[index].totalItemAmount
												* vm.purchase.purchaseChargeList[index].discount / 100);
									}
								}
							}
							vm.purchase.purchaseChargeList[index].totalItemAmount = vm.purchase.purchaseChargeList[index].totalItemAmount
									.toFixed(2);
							vm.getChargeGrossTotalAmount();
							return vm.purchase.purchaseChargeList[index].totalItemAmount;
						}
					};

					vm.getChargeLevelAmount = function(index) {
						if (vm.purchase.purchaseChargeList[index]) {
							vm.purchase.purchaseChargeList[index].itemAmount = 0;
							vm.purchase.purchaseChargeList[index].quantity = 1;
							if (vm.purchase.purchaseChargeList[index].ledger
									&& vm.purchase.purchaseChargeList[index].ledger.ledgerId
									&& vm.purchase.purchaseChargeList[index].quantity
									&& vm.purchase.purchaseChargeList[index].purchaseCharge) {
								var itemAmount = vm.purchase.purchaseChargeList[index].purchaseCharge
										* vm.purchase.purchaseChargeList[index].quantity;

								if (vm.purchase.purchaseChargeList[index].discount) {

									if (vm.purchase.purchaseChargeList[index].measureDiscountInAmount) {
										itemAmount -= vm.purchase.purchaseChargeList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.purchase.purchaseChargeList[index].discount / 100);
									}
								}

								vm.purchase.purchaseChargeList[index].itemAmount = parseFloat(
										itemAmount).toFixed(2);
							}
							return vm.purchase.purchaseChargeList[index].itemAmount;
						}
					};

					vm.getChargeSGSTAmount = function(index) {
						if (vm.purchase.purchaseChargeList[index]) {
							vm.purchase.purchaseChargeList[index].sgst = 0;
							vm.purchase.purchaseChargeList[index].quantity = 1;
							if (vm.calculateSGST) {
								if (vm.purchase.purchaseChargeList[index].ledger
										&& vm.purchase.purchaseChargeList[index].ledger.ledgerId
										&& vm.purchase.purchaseChargeList[index].quantity
										&& vm.purchase.purchaseChargeList[index].purchaseCharge) {
									var itemAmount = vm.purchase.purchaseChargeList[index].purchaseCharge
											* vm.purchase.purchaseChargeList[index].quantity;

									if (vm.purchase.purchaseChargeList[index].discount) {

										if (vm.purchase.purchaseChargeList[index].measureDiscountInAmount) {
											itemAmount -= vm.purchase.purchaseChargeList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.purchase.purchaseChargeList[index].discount / 100);
										}
									}
									var taxAmount = 0;
									if (vm.purchase.purchaseChargeList[index].taxRate) {
										taxAmount = (itemAmount
												* vm.purchase.purchaseChargeList[index].taxRate / 100);
									}
									vm.purchase.purchaseChargeList[index].sgst = taxAmount / 2;
								}
							}
							vm.purchase.purchaseChargeList[index].sgst = parseFloat(
									vm.purchase.purchaseChargeList[index].sgst)
									.toFixed(2);
							return vm.purchase.purchaseChargeList[index].sgst;
						}
					};

					vm.getChargeCGSTAmount = function(index) {
						if (vm.purchase.purchaseChargeList[index]) {
							vm.purchase.purchaseChargeList[index].cgst = 0;
							vm.purchase.purchaseChargeList[index].quantity = 1;
							if (vm.calculateCGST) {
								if (vm.purchase.purchaseChargeList[index].ledger
										&& vm.purchase.purchaseChargeList[index].ledger.ledgerId
										&& vm.purchase.purchaseChargeList[index].quantity
										&& vm.purchase.purchaseChargeList[index].purchaseCharge) {
									var itemAmount = vm.purchase.purchaseChargeList[index].purchaseCharge
											* vm.purchase.purchaseChargeList[index].quantity;

									if (vm.purchase.purchaseChargeList[index].discount) {

										if (vm.purchase.purchaseChargeList[index].measureDiscountInAmount) {
											itemAmount -= vm.purchase.purchaseChargeList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.purchase.purchaseChargeList[index].discount / 100);
										}
									}
									var taxAmount = 0;
									if (vm.purchase.purchaseChargeList[index].taxRate) {
										taxAmount = (itemAmount
												* vm.purchase.purchaseChargeList[index].taxRate / 100);
									}

									vm.purchase.purchaseChargeList[index].cgst = taxAmount / 2;
								}
							}
							vm.purchase.purchaseChargeList[index].cgst = parseFloat(
									vm.purchase.purchaseChargeList[index].cgst)
									.toFixed(2);
							return vm.purchase.purchaseChargeList[index].cgst;
						}
					};

					vm.getChargeIGSTAmount = function(index) {
						if (vm.purchase.purchaseChargeList[index]) {
							vm.purchase.purchaseChargeList[index].igst = 0;
							vm.purchase.purchaseChargeList[index].quantity = 1;
							if (vm.calculateIGST) {
								if (vm.purchase.purchaseChargeList[index].ledger
										&& vm.purchase.purchaseChargeList[index].ledger.ledgerId
										&& vm.purchase.purchaseChargeList[index].quantity
										&& vm.purchase.purchaseChargeList[index].purchaseCharge) {
									var itemAmount = vm.purchase.purchaseChargeList[index].purchaseCharge
											* vm.purchase.purchaseChargeList[index].quantity;

									if (vm.purchase.purchaseChargeList[index].discount) {

										if (vm.purchase.purchaseChargeList[index].measureDiscountInAmount) {
											itemAmount -= vm.purchase.purchaseChargeList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.purchase.purchaseChargeList[index].discount / 100);
										}
									}

									var taxAmount = 0;
									if (vm.purchase.purchaseChargeList[index].taxRate) {
										taxAmount = (itemAmount
												* vm.purchase.purchaseChargeList[index].taxRate / 100);
									}

									vm.purchase.purchaseChargeList[index].igst = taxAmount;
								}
							}
							vm.purchase.purchaseChargeList[index].igst = parseFloat(
									vm.purchase.purchaseChargeList[index].igst)
									.toFixed(2);
							return vm.purchase.purchaseChargeList[index].igst;
						}
					};

					vm.getChargeGrossTotalAmount = function() {
						vm.purchase.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.purchase.purchaseItemModel.length; counter++) {
							if (vm.purchase.purchaseItemModel[counter].totalItemAmount
									&& vm.purchase.purchaseItemModel[counter].totalItemAmount > 0) {
								vm.purchase.grossTotalAmount += parseFloat(vm.purchase.purchaseItemModel[counter].totalItemAmount);
							}
						}

						if (vm.purchase.purchaseChargeList.length > 0) {
							for (var count = 0; count < vm.purchase.purchaseChargeList.length; count++) {
								if (vm.purchase.purchaseChargeList[count].totalItemAmount)
									vm.purchase.grossTotalAmount += parseFloat(vm.purchase.purchaseChargeList[count].totalItemAmount);
							}
						}

						vm.purchase.grossTotalAmount = parseFloat(
								vm.purchase.grossTotalAmount).toFixed(2);

						vm.purchase.totalBillAmount = parseFloat(vm.purchase.grossTotalAmount);
						if (vm.purchase.discount && vm.purchase.discount > 0) {

							if (vm.purchase.purchaseMeasureDiscountInAmount) {
								vm.purchase.totalBillAmount -= vm.purchase.discount;
							} else {
								vm.purchase.totalBillAmount -= (vm.purchase.totalBillAmount
										* vm.purchase.discount / 100);
							}

						}
						vm.purchase.totalBillAmount = parseFloat(
								vm.purchase.totalBillAmount).toFixed(2);
						return vm.purchase.grossTotalAmount;
					};

					vm.addPurchaseCharge = function() {
						vm.numberOfAddedPurchaseCharge += 1;
						vm.purchase.purchaseChargeList[vm.numberOfAddedPurchaseCharge - 1] = {}
						vm.numberOfPurchaseCharge = new Array(
								vm.numberOfAddedPurchaseCharge);
					};

					vm.deletePurchaseCharge = function(index) {
						if (vm.numberOfAddedPurchaseCharge === 1) {
							return;
						}
						vm.numberOfAddedPurchaseCharge -= 1;
						vm.purchase.purchaseChargeList.splice(index, 1);
						vm.numberOfPurchaseCharge = new Array(
								vm.numberOfAddedPurchaseCharge);
					};

					// for print of purchase basic

					vm.setCompanyDetail = function(response) {
						vm.companyDataForPurchase = angular
								.copy(response.data.data);
						vm.selectedcityForpurchase = vm.companyDataForPurchase.cityName;
						console.log("vm.selectedcityForpurchase :"
								+ vm.selectedcityForpurchase);
					};

					vm.batchAddItemModel = function(index) {
						console.log("call this index for urcjase controller :"
								+ index);
						console
								.log("Purchase Item Id :"
										+ vm.purchase.purchaseItemModel[index].item.itemId);
						vm.index = index;
						vm.purchase.itemId = vm.purchase.purchaseItemModel[index].item.itemId;
						$("#new-batch-purchase-dialog .modal").modal('show');
					}

					vm.saveBatchPurchase = function() {
						console.log("vm.purchase.mfgDate :"
								+ vm.purchase.mfgDate)
						if (!vm.purchase.batchNo || vm.purchase.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.purchase.mfgDate || vm.purchase.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.purchase.mfgDate);
							vm.purchase.mfgDate = moment(date).format(
									"YYYY-MM-DD");
							var month = date.getMonth()
									+ vm.purchase.shelfLifes;
							date.setMonth(month);
							vm.purchase.expDate = moment(date).format(
									"YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {

							var date = new Date(vm.purchase.mfgDate);
							vm.purchase.mfgDate = moment(date).format(
									"YYYY-MM-DD");
							date.setDate(date.getDate()
									+ vm.purchase.shelfLifes)
							vm.purchase.expDate = moment(date).format(
									"YYYY-MM-DD");
						}

						console.log(vm.purchase.expDate);
						vm.purchase.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.purchase.shelfLifes;

						vm.batchNo = vm.purchase.batchNo;
						vm.mfgDate = vm.purchase.mfgDate;
						vm.shelfLife = vm.purchase.shelfLife;
						vm.id = vm.ItemId;

						$http
								.post('../../itemData/saveBatch', {
									batchNo : vm.batchNo,
									mfgDate : vm.mfgDate,
									expDate : vm.purchase.expDate,
									shelfLife : vm.shelfLife,
									itemId : vm.id
								})
								.then(
										function(response) {

											vm.purchase.purchaseItemModel[vm.index].batchNoList
													.push(response.data.data);
											console.log("Saving vat response."
													+ response.data.data)
											vm.purchase.purchaseItemModel[vm.index].batchId = response.data.data.batchId;

											// vm.sell.sellItemModel[vm.index].batchId
											// = response.data.data.batchId;
											var dt = formatDateForPurchase(response.data.data.mfgDate);
											var pt = formatDateForPurchase(response.data.data.expDate);
											vm.purchase.purchaseItemModel[vm.index].item.mfgDate = dt;
											vm.purchase.purchaseItemModel[vm.index].item.expDate = pt;
											// $("#new-batch-sell-dialog
											// .modal").modal('hide');
											$(
													"#new-batch-purchase-dialog .modal")
													.modal('hide');
											toaster.showMessage('success', '',
													'data saved successfully.');
										}, errorCallback);
						vm.purchase.mfgDate = "";
						vm.purchase.batchNo = "";
						vm.purchase.shelfLife = "";
						// $("#new-batch-purchase-dialog .modal").modal('hide');
					};

					function successCallBatchback() {
						toaster.showMessage('success', '',
								'data saved successfully.');
						$("#new-batch-purchase-dialog .modal").modal('hide');

					}
					vm.closedBatch = function() {
						$("#new-batch-purchase-dialog .modal").modal('hide');
					}

					// vm.saveBatchForPurchase = function() {
					// console.log("vm.purchase.mfgDate :"
					// + vm.purchase.mfgDate)
					// if (!vm.purchase.batchNo || vm.purchase.batchNo === "") {
					// toaster.showMessage('error', '',
					// 'Kindly enter BatchNo.');
					// return false;
					// }
					// if (!vm.purchase.mfgDate || vm.purchase.mfgDate === "") {
					// toaster.showMessage('error', '',
					// 'Kindly enter Mfg Date.');
					// return false;
					// }
					// // if (!vm.purchase.expDate || vm.purchase.expDate ===
					// // "") {
					// // toaster.showMessage('error', '', 'Kindly enter Exp
					// // Date.');
					// // return false;
					// // }
					//
					// if (document.getElementById("dayMonthDD").value ==
					// "Month") {
					// var date = new Date(vm.purchase.mfgDate);
					// var month = date.getMonth()
					// + vm.purchase.shelfLifes;
					// date.setMonth(month);
					// vm.purchase.expDate = moment(date).format(
					// "YYYY-MM-DD");
					// // vm.itemBatchObj.expDates =
					// // moment(date).format("MM-DD-YYYY");
					// } else if (document.getElementById("dayMonthDD").value ==
					// "days") {
					// vm.purchase.expDate = moment(vm.purchase.mfgDate)
					// .format("YYYY-MM-DD");
					// }
					//
					// vm.purchase.mfgDate = moment(vm.purchase.mfgDate)
					// .format("YYYY-MM-DD");
					// // vm.purchase.expDate =
					// // moment(vm.purchase.expDate).format("YYYY-MM-DD");
					// // vm.billInkDashboardService.item.push(vm.purchase);
					// console.log("shelflife :" + vm.purchase.shelfLifes);
					// vm.purchase.shelfLife = document
					// .getElementById("dayMonthDD").value
					// + "@" + vm.purchase.shelfLifes;
					//
					// $http.post('../../itemData/savebatchData', vm.purchase)
					// .then(successCallBatchback, errorCallback);
					// $("#add-batch-purchase-dialog .modal").modal('hide');
					// };

					vm.printPurchaseDiv = function() {
						/*
						 * var popupWin = window.open('', '_blank',
						 * 'width=300,height=300'); popupWin.document.open();
						 * popupWin.document.write('<html><head><link
						 * rel="stylesheet" type="text/css" href="style.css" /></head><body
						 * onload="window.print()"> <div
						 * ng-controller="addPurchaseDataController as vm"><table
						 * border ="1px"><tr><td>Select Ledger: </td><td ng-bind = ""></td><td>Bill
						 * Number:</td><td ng-bind = "vm.purchase.billNumber"></td></tr></table></div>
						 * </body></html>'); popupWin.document.close();
						 */
						var printContent = document
								.getElementById('printPurchaseDiv');
						var WinPrint = window.open('', '',
								'width=900,height=650');
						WinPrint.document.write(printContent.innerHTML);
						WinPrint.document.close();
						WinPrint.focus();
						WinPrint.print();
						WinPrint.close();
					}

					vm.withOutFreeQtyPrintDiv = function() {

						// vm.sell.sellPrintDate =
						// moment(vm.sell.sellDateObj).format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second
									var printContent = document
											.getElementById('printWithOutFeeQtySellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									// WinPrint.document.left(100);
									// WinPrint.document.top(150);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);
						//		

					}

					vm.withOutBatchPrintDiv = function() {
						// vm.sell.sellPrintDate =
						// moment(vm.sell.sellDateObj).format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second

									var printContent = document
											.getElementById('printWithOutbatchNoSellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.allFieldsPrintDiv = function() {
						// vm.sell.sellPrintDate =
						// moment(vm.sell.sellDateObj).format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second

							var printContent = document
									.getElementById('printAllFieldSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);

					}
					vm.prepareHSNCodeWiseTaxList = function() {
						vm.hsnTaxCodeList = {};
						vm.totalIGST = 0;
						vm.totalSGST = 0;
						vm.totalCGST = 0;

						for ( var i in vm.purchase.purchaseItemModel) {
							var sellItemObj = vm.purchase.purchaseItemModel[i];
							if (sellItemObj.item) {

								sellItemObj.item.hsnCode = sellItemObj.item.hsnCode
										|| "";
								vm.hsnTaxCodeList[sellItemObj.item.hsnCode] = vm.hsnTaxCodeList[sellItemObj.item.hsnCode]
										|| {};
								if (vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate]) {
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].igst += parseFloat(sellItemObj.igst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].cgst += parseFloat(sellItemObj.cgst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].sgst += parseFloat(sellItemObj.sgst);
								} else {
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate] = {};
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].igst = parseFloat(sellItemObj.igst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].cgst = parseFloat(sellItemObj.cgst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].sgst = parseFloat(sellItemObj.sgst);
								}
								vm.totalIGST += parseFloat(sellItemObj.igst);
								vm.totalCGST += parseFloat(sellItemObj.cgst);
								vm.totalSGST += parseFloat(sellItemObj.sgst);
							}
						}

						// for(var i in vm.sell.freightList) {
						// var sellFreightObj = vm.sell.freightList[i];
						// if (sellFreightObj.ledger) {
						//				
						// sellFreightObj.ledger.sacCode =
						// sellFreightObj.ledger.sacCode || "";
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode] =
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode] ||
						// {};
						// if
						// (vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate])
						// {
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].igst
						// += parseFloat(sellFreightObj.igst);
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].cgst
						// += parseFloat(sellFreightObj.cgst);
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].sgst
						// += parseFloat(sellFreightObj.sgst);
						// } else {
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate]
						// = {};
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].igst
						// = parseFloat(sellFreightObj.igst);
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].cgst
						// = parseFloat(sellFreightObj.cgst);
						// vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].sgst
						// = parseFloat(sellFreightObj.sgst);
						// }
						// vm.totalIGST += parseFloat(sellFreightObj.igst);
						// vm.totalCGST += parseFloat(sellFreightObj.cgst);
						// vm.totalSGST += parseFloat(sellFreightObj.sgst);
						// }
						// }

						vm.hsnList = [];
						if (Object.keys(vm.hsnTaxCodeList).length > 0) {
							for ( var hsnCode in vm.hsnTaxCodeList) {
								for ( var taxRate in vm.hsnTaxCodeList[hsnCode]) {
									var hsnTaxCodeObj = {};
									hsnTaxCodeObj.hsnCode = hsnCode;
									hsnTaxCodeObj.taxRate = taxRate;
									hsnTaxCodeObj.igst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].igst)
											.toFixed(2);
									hsnTaxCodeObj.cgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].cgst)
											.toFixed(2);
									hsnTaxCodeObj.sgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].sgst)
											.toFixed(2);
									vm.hsnList.push(hsnTaxCodeObj);
								}
							}
						}
						vm.totalIGST = parseFloat(vm.totalIGST).toFixed(2);
						vm.totalCGST = parseFloat(vm.totalCGST).toFixed(2);
						vm.totalSGST = parseFloat(vm.totalSGST).toFixed(2);
						vm.totalItemLevelAmountWithoutTax = parseFloat(vm.purchase.grossTotalAmount)
								- parseFloat(vm.totalSGST)
								- parseFloat(vm.totalCGST)
								- parseFloat(vm.totalIGST);
						vm.totalItemLevelAmountWithoutTax = parseFloat(
								vm.totalItemLevelAmountWithoutTax).toFixed(2);
					};
				});