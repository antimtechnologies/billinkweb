angular.module('managePurchaseData', ['infinite-scroll', 'toaster'])
.service("managePurchaseDataService", function($http) {
	
	var vm = this;

	vm.purchaseList = [];
	vm.startOfPurchase = 0;
	vm.noOfRecordForPurchase = 12;
	vm.purchaseFilterData = {};
	vm.hasMorePurchaseData = true;

	vm.companyCountRecordList = [];
	vm.startOfCompanyCount = 0;
	vm.noOfRecordForCompanyCount = 12;
	vm.hasMoreCompanyCountData = true;

	vm.getPurchaseList = function(companyId) {
		$http.post('../../purchaseData/getPurchaseListData', {
			companyId: companyId, 
			start: vm.startOfPurchase, 
			noOfRecord: vm.noOfRecordForPurchase,
			filterData : vm.purchaseFilterData
		}).then(successCallback, errorCallback);	
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfPurchase === 0) {
				vm.purchaseList = response.data.data;
				if (vm.purchaseList.length < vm.noOfRecordForPurchase) {
					vm.hasMorePurchaseData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForPurchase) {				
				vm.hasMorePurchaseData = false;
			}

			vm.purchaseList = vm.purchaseList.concat(response.data.data);
		}
	}

	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);;
	}

	vm.getCompanyCountData = function() {
		$http.post('../../companydata/getCompanyRequestCountDetails', {start: vm.startOfCompanyCount, noOfRecord: vm.noOfRecordForCompanyCount, sortParam: 'purchasePendingCount' }).
		then(companyCountSuccessCallBack, errorCallback);
	};

	function companyCountSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompanyCount === 0) {
				vm.companyCountRecordList = response.data.data;
				if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
					vm.hasMoreCompanyCountData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCompanyCount) {				
				vm.hasMoreCompanyCountData = false;
			}

			vm.companyCountRecordList = vm.companyCountRecordList.concat(response.data.data);
		}
	}

})
.controller('managePurchaseDataController', function($http, $ocLazyLoad, toaster, managePurchaseDataService, billInkDashboardService) {
	var vm = this;
	vm.managePurchaseDataService = managePurchaseDataService;
	vm.billInkDashboardService = billInkDashboardService;

	if (vm.billInkDashboardService.userRole !== "Vendor") {
		vm.statusList = ['New', 'Modified', 'Deleted'];
	} else {		
		vm.statusList = ['New', 'Modified', 'Deleted', 'UnPublished'];
	}

	vm.managePurchaseDataService.purchaseFilterData = {};
	vm.managePurchaseDataService.purchaseFilterData.statusList = [];

	vm.openPurchaseList = function(companyObj) {

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.managePurchaseDataService.purchaseFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.managePurchaseDataService.purchaseFilterData.isVerified = 0;
		}
		vm.billInkDashboardService.currentCompanyObj = companyObj;
		vm.managePurchaseDataService.selectedCompanyId = companyObj.companyId;
		vm.managePurchaseDataService.startOfPurchase = 0;
		vm.billInkDashboardService.currentViewUrl = "../purchase/view/viewPurchaseList.html";
		vm.managePurchaseDataService.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
	};
	
	vm.backToCompanyWisePurchaseCount = function() {
			vm.billInkDashboardService.currentViewUrl = "../purchase/view/companyWisePurchaseCount.html";
			vm.managePurchaseDataService.getCompanyCountData();
	};

	vm.init = function() {
		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.billInkDashboardService.currentViewUrl = "../purchase/view/companyWisePurchaseCount.html";
			vm.managePurchaseDataService.getCompanyCountData();
		} else {			
			vm.openPurchaseList(vm.billInkDashboardService.selectedCompany);
		}
	};

	vm.loadMorePurchase = function() {
		if (vm.managePurchaseDataService.hasMorePurchaseData) {
			vm.managePurchaseDataService.startOfPurchase +=  vm.managePurchaseDataService.noOfRecordForPurchase;
			vm.managePurchaseDataService.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
		}
	};
	
	vm.loadMoreCompanyCountData = function() {
		if (vm.managePurchaseDataService.hasMoreCompanyCountData) {
			vm.managePurchaseDataService.startOfCompanyCount +=  vm.managePurchaseDataService.noOfRecordForPurchase;
			vm.managePurchaseDataService.getCompanyCountData();
		}
	};

	vm.showAddPurchaseDataForm = function(requestFor) {
		vm.managePurchaseDataService.requestType = "Add";
		vm.managePurchaseDataService.requestFor = requestFor;
		vm.managePurchaseDataService.startOfPurchase = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../purchase/js/addEditPurchaseDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../purchase/view/addEditPurchaseDetail.html";
		});
	};

	vm.showDeletePrompt = function(purchaseId, statusName) {
		vm.selectedDeletePurchaseId = purchaseId;
		vm.selectedDeletePurchaseStatus = statusName;
		$("#purchase-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteSavedPurchaseData = function() {
		$http.post('../../purchaseData/deleteSavedPurchaseData', {
			purchaseId: vm.selectedDeletePurchaseId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.managePurchaseDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	}

	vm.deletePublishPurchaseData = function() {
		$http.post('../../purchaseData/deletePublishedPurchaseData', {
			purchaseId: vm.selectedDeletePurchaseId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.managePurchaseDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	};

	vm.deletePurchaseData = function() {
		if (vm.selectedDeletePurchaseStatus === "UnPublished") {
			vm.deleteSavedPurchaseData();
		} else {
			vm.deletePublishPurchaseData();
		}
	};

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeletePurchaseId = 0;
		vm.selectedDeletePurchaseStatus = "";
		$("#purchase-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
	}

	vm.editPurchaseData = function(purchaseId, imageURL) {
		vm.managePurchaseDataService.requestType = "Edit";
		vm.managePurchaseDataService.requestFor = imageURL && imageURL !== "" ? "uploadImage" : "enterManually";
		vm.managePurchaseDataService.editPurchaseId = purchaseId;
		vm.managePurchaseDataService.startOfPurchase = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../purchase/js/addEditPurchaseDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../purchase/view/addEditPurchaseDetail.html";
		});
	};

	// filter related function start
	vm.launchFilter = function() {
		$("#purchase-filter-dialog .modal").modal('show');
	};

	vm.closeFilterModal = function() {
		$("#purchase-filter-dialog .modal").modal('hide');
	};

	vm.applyFilter = function() {
		vm.searchFilterData();
		$("#purchase-filter-dialog .modal").modal('hide');
	};

	vm.searchFilterData = function() {
		vm.managePurchaseDataService.startOfPurchase = 0;
		vm.managePurchaseDataService.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
	};

	vm.resetFilter = function() {
		vm.managePurchaseDataService.purchaseFilterData = {};
		vm.managePurchaseDataService.purchaseFilterData.statusList = [];

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.managePurchaseDataService.purchaseFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.managePurchaseDataService.purchaseFilterData.isVerified = 0;
		}

		vm.managePurchaseDataService.startOfPurchase = 0;
		vm.managePurchaseDataService.getPurchaseList(vm.managePurchaseDataService.selectedCompanyId);
		$("#purchase-filter-dialog .modal").modal('hide');
	};

	vm.toggleSelection = function toggleSelection(status) {
		var idx = vm.managePurchaseDataService.purchaseFilterData.statusList.indexOf(status);
	
		if (idx > -1) {
			vm.managePurchaseDataService.purchaseFilterData.statusList.splice(idx, 1);
		} else {
			vm.managePurchaseDataService.purchaseFilterData.statusList.push(status);
		}
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.managePurchaseDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.managePurchaseDataService.purchaseFilterData.ledgerId = 0;
			vm.searchLedgerList = [];
		}
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.managePurchaseDataService.purchaseFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
	};
	// filter related function end

	vm.$onDestroy = function() {
		vm.managePurchaseDataService.purchaseList = [];
		vm.managePurchaseDataService.startOfPurchase = 0;
		vm.managePurchaseDataService.hasMorePurchaseData = true;

		vm.managePurchaseDataService.companyCountRecordList = [];
		vm.managePurchaseDataService.startOfCompanyCount = 0;
		vm.managePurchaseDataService.hasMoreCompanyCountData = true;

		vm.managePurchaseDataService.selectedCompanyId = 0;
	}
});
