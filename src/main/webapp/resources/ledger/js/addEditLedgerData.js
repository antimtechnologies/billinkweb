var aaddLedgerDataApp = angular.module('addEditLedgerData', ['fileUploadDirective', 'toaster']);

aaddLedgerDataApp.service('manageLedgerDataService', function($http){
	var svc = this;
	//svc.itemList = [];
});


aaddLedgerDataApp.controller('addEditLedgerDataController', function($http, fileUploadService, toaster, manageLedgerDataService, billInkDashboardService) {
	var vm = this;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.taxCodeList = [];
	vm.manageLedgerDataService = manageLedgerDataService;
	vm.billInkDashboardService = billInkDashboardService;
	vm.billInkDashboardService.ledger = {};
	vm.billInkDashboardService.ledger.ledgerDocumentList = [];
	vm.billInkDashboardService.ledger.multipleGstList = [];
	vm.billInkDashboardService.ledger.stateId=0;
	

	vm.getCustomerTypeList = [];
	
	vm.getPriceList = [];
	
	vm.getLocationList = [];
	
	vm.init = function() {
		$http.post('../../stateData/getStateList', vm.billInkDashboardService.ledger).then(vm.setStateList, vm.errorFetchData);
		$http.post('../../taxcodedata/getTaxCodeValueList', {}).then(vm.setTaxCodeList, vm.errorFetchData);

		$http.post('../../customerTypeData/getCustomerTypeListView',{companyId:vm.manageLedgerDataService.selectedCompanyId,start:0,noOfRecord:1000}).then(
		successCustomerCallback, errorCallback);
		
		var parameter = {companyId : manageLedgerDataService.selectedCompanyId};

		vm.billInkDashboardService.ledger = {};
		vm.billInkDashboardService.ledger.ledgerDocumentList = [];
		vm.billInkDashboardService.ledger.multipleGstList = [];
		vm.billInkDashboardService.ledger.schemeOneId = false;
		  vm.billInkDashboardService.ledger.schemeTwoId = false;
		  
		if (vm.manageLedgerDataService.requestType === "Edit") {
			$http.post('../../ledgerData/getLedgerDetailsById', { companyId: vm.manageLedgerDataService.selectedCompanyId, ledgerId : vm.manageLedgerDataService.editLedgerId}).
			then(vm.setLedgerDetail, vm.errorFetchData);			
		} else {
			vm.billInkDashboardService.ledger.addedBy = {};
			vm.billInkDashboardService.ledger.addedBy.userId = vm.billInkDashboardService.userId;
			vm.billInkDashboardService.ledger.companyId = manageLedgerDataService.selectedCompanyId; 
		}
		
	};

	
	vm.getPriceMasterByCustomer = function(customerTypeId){
		$http.get('../../pricemaster/findbycustomertype?customerTypeId='+customerTypeId).then(
				successPriceCallback, errorCallback);
	}
	
	
	
	function successPriceCallback(response){
		vm.getResponce = response.data.result;
	//	console.log("getPriceList :::"+vm.getResponce[0].discount);
		if (vm.getResponce[0] != null) {
			vm.billInkDashboardService.ledger.priceId = vm.getResponce[0].discount;	
		}
		
	}
	
	function successCustomerCallback(response){
		vm.billInkDashboardService.getCustomerTypeList = response.data.result; 
	}
	vm.errorFetchData = function(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	};

	vm.setTaxCodeList = function(response) {
		vm.billInkDashboardService.taxCodeList = response.data.data;
	};

	vm.setStateList = function(response) {
		vm.billInkDashboardService.stateList = response.data.data;
	};

	vm.setLedgerDetail = function(response) {
		vm.billInkDashboardService.ledger = angular.copy(response.data.data);
		
		vm.billInkDashboardService.ledger.companyId = manageLedgerDataService.selectedCompanyId; 
		console.log(vm.billInkDashboardService.ledger.creditLimit);
		console.log(vm.billInkDashboardService.ledger.creditPeriod);
		console.log(vm.billInkDashboardService.ledger.PeriodName);
		vm.billInkDashboardService.ledger.modifiedBy = {};
		
		if (vm.billInkDashboardService.ledger.ledgerCode) {
			 
				vm.billInkDashboardService.ledger.ledgerCodePrefix=vm.billInkDashboardService.ledger.ledgerCode.substring(0,2)
				vm.billInkDashboardService.ledger.currentledgerCode=vm.billInkDashboardService.ledger.ledgerCode.substring(2);
		}
		if(vm.billInkDashboardService.ledger.priceId == 1){
			document.getElementById("priceYesId").checked=true;
		}else{
			document.getElementById("priceNoId").checked=true;
		}
		if(vm.billInkDashboardService.ledger.schemeOneId == 1){
			document.getElementById("schemeOneId").checked=true;
		}
		if(vm.billInkDashboardService.ledger.schemeTwoId == 1){
			document.getElementById("schemeTwoId").checked=true;
		}
		vm.checkedledgerType();
		document.getElementById("customerTypeId").selectedIndex=vm.billInkDashboardService.ledger.customerTypeId;
		console.log("stateID");
		console.log(vm.billInkDashboardService.ledger.state);
		document.getElementById("userState").selectedIndex=vm.billInkDashboardService.ledger.state.stateId;
		vm.billInkDashboardService.ledger.modifiedBy.userId = vm.billInkDashboardService.userId;
	};

	vm.errorFetchList = function() {	
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			toaster.showMessage('success', '', 'data saved successfully.');
			vm.closeLedgerDataSave();
		} else {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}

	vm.saveLedgerData = function() {
		if (!vm.billInkDashboardService.ledger.ledgerName || vm.billInkDashboardService.ledger.ledgerName === "") {
			toaster.showMessage('error', "", 'Kindly enter ledger name.');
			return false;
		}
		if(vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Creditors" || vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Debtors" ){
			
			
//			var re = new RegExp();
			
			if(vm.billInkDashboardService.ledger.creditLimit && vm.billInkDashboardService.ledger.creditLimit != "" && !/^\d{0,25}(\.\d{1,2})?$/.test(vm.billInkDashboardService.ledger.creditLimit))
			{
				toaster.showMessage('error', "", 'Please correct creditlimit(eg: *.**).');
				return false;
			}
		}
		if (vm.billInkDashboardService.ledger.ledgerCodePrefix) {
			vm.billInkDashboardService.ledger.ledgerCode = vm.billInkDashboardService.ledger.ledgerCodePrefix.concat(vm.billInkDashboardService.ledger.currentledgerCode);
		}
		
//		if (vm.billInkDashboardService.ledger.PeriodName) {
//			console.log("type :"+vm.billInkDashboardService.ledger.PeriodName);
//			console.log("inputed :"+vm.billInkDashboardService.ledger.creditPeriodNumber);
//			//vm.billInkDashboardService.ledger.creditPeriod =vm.billInkDashboardService.ledger.creditPeriod;
//		}
		$http.post('../../ledgerData/saveLedgerData', vm.billInkDashboardService.ledger).then(successCallback, errorCallback);
	};

	vm.updateLedgerData = function() {
		if (!vm.billInkDashboardService.ledger.ledgerName && vm.billInkDashboardService.ledger.ledgerName === "") {
			toaster.showMessage('error', "", 'Kindly enter ledger name.');
			return false;
		}
		if(vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Creditors" || vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Debtors" ){
			
			
			if(vm.billInkDashboardService.ledger.creditLimit && vm.billInkDashboardService.ledger.creditLimit != "" && !/^\d{0,25}(\.\d{1,2})?$/.test(vm.billInkDashboardService.ledger.creditLimit))
			{
				toaster.showMessage('error', "", 'Please correct creditlimit(eg: *.**).');
				return false;
			}
		}
		if (vm.billInkDashboardService.ledger.ledgerCodePrefix) {
			vm.billInkDashboardService.ledger.ledgerCode = vm.billInkDashboardService.ledger.ledgerCodePrefix.concat(vm.billInkDashboardService.ledger.currentledgerCode);
		}
		
		//alert(vm.billInkDashboardService.ledger.priceId);
		//return false;
		$http.post('../../ledgerData/updateLedgerData', vm.billInkDashboardService.ledger).then(successCallback, errorCallback);
	};

	vm.closeLedgerDataSave = function() {
		vm.manageLedgerDataService.editLedgerId = 0;
		vm.manageLedgerDataService.startOfLedger = 0;
		vm.manageLedgerDataService.requestType = "";
		vm.manageLedgerDataService.getCompanyLedgerList();
		vm.manageLedgerDataService.ledgerContainerURL = "../ledger/view/viewLedgerList.html";
	};

	vm.addDocument = function() {
		angular.element("input[type='file']").val(null);
		vm.ledgerDocumentObj = {};
		vm.ledgerDocumentFile = "";
		$("#add-document-dialog .modal").modal('show');
	};

	vm.closedDocumentModal = function() {
		$("#add-document-dialog .modal").modal('hide');
	};

	vm.saveDocument = function() {
		var file = vm.ledgerDocumentFile;
		if (vm.ledgerDocumentFile) {			
			fileUploadService.uploadFileToUrl(file, 'company').then(function(result){
				if (result.data.status === "success") {				
					vm.ledgerDocumentObj.documentUrl = result.data.imagePath;
					vm.billInkDashboardService.ledger.ledgerDocumentList.push(vm.ledgerDocumentObj);
				}
				vm.ledgerDocumentObj = {};
				vm.ledgerDocumentFile = undefined;
				$("#add-document-dialog .modal").modal('hide');
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading document');
			});
		}
	};

	vm.billInkDashboardService.ledgerdeleteDocument = function(index) {
		vm.ledger.ledgerDocumentList.splice(index, 1);
	};
	
	// addedd
	
	vm.addMultipleGst = function() {
		//angular.element("input[type='file']").val(null);
		vm.ledgerMultipleGstObj = {};
		//vm.ledgerDocumentFile = "";
		$("#add-MultipleGST-dialog .modal").modal('show');
	};
	
	vm.closedMultipleGst = function(){
		$("#add-MultipleGST-dialog .modal").modal('hide');
	}
	vm.saveMultipleGstData = function() {
		var flag=true;
		if(vm.ledgerMultipleGstObj.city.toLowerCase() ===vm.billInkDashboardService.ledger.cityName.toLowerCase()){
			toaster.showMessage('error', "", 'You can not add primary city name multiple time for GST.');
			return false;
		}
		angular.forEach(vm.billInkDashboardService.ledger.multipleGstList,function(ledgerMultipleGstObj){
			console.log(ledgerMultipleGstObj);
			console.log(vm.ledgerMultipleGstObj.city);
			if(vm.ledgerMultipleGstObj.city.toLowerCase() ===ledgerMultipleGstObj.city.toLowerCase()){
//				
				vm.purchase.gstNumber=ledgerMultipleGstObj.goDownGst;
				return;
			}
		})
		
		
		if(!flag){
			toaster.showMessage('error', "", 'You can not add same city multiple time for GST.');
			return false;
		}
		if (!vm.ledgerMultipleGstObj.multipleGst || vm.ledgerMultipleGstObj.multipleGst === "") {
			toaster.showMessage('error', "", 'Kindly enter GstNo.');
			return false;
		}
		console.log("ledgerMultipleGstObj :"+vm.ledgerMultipleGstObj);
		vm.billInkDashboardService.ledger.multipleGstList.push(vm.ledgerMultipleGstObj);
		$("#add-MultipleGST-dialog .modal").modal('hide');
	};
	
	vm.billInkDashboardService.fillPriceMaster = function(){
		
		vm.getPriceMasterByCustomer(vm.billInkDashboardService.ledger.customerTypeId);
		
		//console.log("call this master :"+vm.selectCustomerType);
	}
	
	vm.deleteDocument = function(index) {
		vm.billInkDashboardService.ledger.ledgerDocumentList.splice(index, 1);
	};

	vm.deleteMultipleGst = function(index) {
		vm.billInkDashboardService.ledger.multipleGstList.splice(index, 1);
	};
	
	vm.checkedledgerType = function(){
		
		$(".creditoption").hide();
		if(vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Creditors" || vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Debtors" ){
			$(".creditoption").show();
			if(vm.billInkDashboardService.ledger.ledgerGroup == "Sundry Creditors"){
				vm.billInkDashboardService.ledger.ledgerCodePrefix = "CR";	
			}else{
				vm.billInkDashboardService.ledger.ledgerCodePrefix = "DR";
			}
			
		}
	}
	
	vm.change = function(val){
		 var checkBox1 = document.getElementById("schemeOneId");
		 var checkBox2 = document.getElementById("schemeTwoId");
		//console.log("checked :"+val);
		
		if (checkBox1.checked == true){
		    vm.billInkDashboardService.ledger.schemeOneId = true;
		  } else if(checkBox2.checked == true){
			  vm.billInkDashboardService.ledger.schemeTwoId = true;
		 }
	}
	
	vm.setState=function(stateid)
	{
		vm.billInkDashboardService.ledger.state.stateId=stateid;
		vm.billInkDashboardService.ledger.state.stateName="";
		
		console.log(vm.billInkDashboardService.ledger);
		
	}
	
	vm.changeprice = function(event){
		
		 var checkyes = document.getElementById("priceYesId");
		 var checkno = document.getElementById("priceNoId");
		 
		 if ((event.target.id) == "priceYesId" && checkyes.checked == true){
			 vm.billInkDashboardService.ledger.priceId = 1;
		    document.getElementById("priceNoId").checked=false;
		  } else if((event.target.id) == "priceNoId" &&checkno.checked == true){
			  vm.billInkDashboardService.ledger.priceId = 0;
			  document.getElementById("priceYesId").checked=false;
		  }
	}
	
	
});

//app.directive('validNumber', function() {
//	
//	return {
//		restrict : 'E',
//	 	link: function(scope, element, attrs, ngModelCtrl) {
//         if(!ngModelCtrl) {
//           return; 
//         }
//         ngModelCtrl.$parsers.push(function(val) {
//             if (angular.isUndefined(val)) {
//                 var val = '';
//             }
//         var clean = val.replace(/[^-0-9\.]/g, '');
//         var negativeCheck = clean.split('-');
//   		 var decimalCheck = clean.split('.');
//             if(!angular.isUndefined(negativeCheck[1])) {
//                 negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
//                 clean =negativeCheck[0] + '-' + negativeCheck[1];
//                 if(negativeCheck[0].length > 0) {
//                 	clean =negativeCheck[0];
//                 }
//                 
//             }
//        if(!angular.isUndefined(decimalCheck[1])) {
//                 decimalCheck[1] = decimalCheck[1].slice(0,2);
//                 clean =decimalCheck[0] + '.' + decimalCheck[1];
//             }
//        if (val !== clean) {
//            ngModelCtrl.$setViewValue(clean);
//            ngModelCtrl.$render();
//          }
//          return clean;
//         });
//         element.bind('keypress', function(event) {
//             if(event.keyCode === 32) {
//               event.preventDefault();
//             }
//           });
//	 	 }
//    };
//  });