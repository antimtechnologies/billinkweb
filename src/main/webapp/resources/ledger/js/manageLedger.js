var manageLedgerDataApp = angular.module('manageLedger', ['infiniteScroll', 'infinite-scroll']);

manageLedgerDataApp.service('manageLedgerDataService', function($http){
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfLedger			= 0;
	svc.noOfRecordForLedger		= 12;
	svc.hasMoreLedgerData 		= true;
	svc.ledgerList				= [];

	svc.getCompanyLedgerList = function() {
		if (svc.selectedCompanyId && svc.selectedCompanyId > 0) {			
			$http.post('../../ledgerData/getLedgerListData', {
								companyId: svc.selectedCompanyId, 
								start: svc.startOfLedger, 
								noOfRecord: svc.noOfRecordForLedger,
								ledgerName: svc.searchLedgerNameText
							}).then(svc.successCallback, svc.errorCallback);
		} 
	};

	svc.successCallback = function(response) {
		if (response.data.status === "success") {
			if (svc.startOfLedger === 0) {
				svc.ledgerList = response.data.data;
				if (svc.ledgerList.length < svc.noOfRecordForLedger) {
					svc.hasMoreLedgerData = false;					
				}
				return;
			}
			if (response.data.data.length < svc.noOfRecordForLedger) {				
				svc.hasMoreLedgerData = false;					
			}

			svc.ledgerList = svc.ledgerList.concat(response.data.data);
		}
	};

	svc.errorCallback = function() {
		svc.ledgerList = [];
	};

});

manageLedgerDataApp.controller('manageLedgerController', function($http, $ocLazyLoad, manageLedgerDataService, billInkDashboardService) {
	var vm 							= this;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.manageLedgerDataService		= manageLedgerDataService;
	vm.manageLedgerDataService.ledgerContainerURL = "../ledger/view/viewLedgerList.html";
	vm.billInkDashboardService		= billInkDashboardService; 
	vm.init = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};

	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
		}
	};

	vm.loadMoreLedger = function() {
		if (vm.manageLedgerDataService.hasMoreLedgerData) {			
			vm.manageLedgerDataService.startOfLedger += vm.manageLedgerDataService.noOfRecordForLedger;
			vm.manageLedgerDataService.getCompanyLedgerList();
		}
	};

	vm.getCompayLedgerDetail = function(companyId) {
		vm.manageLedgerDataService.selectedCompanyId = companyId;
		vm.manageLedgerDataService.startOfLedger = 0;
		vm.manageLedgerDataService.hasMoreLedgerData = true;
		vm.manageLedgerDataService.getCompanyLedgerList();
	}
	
	vm.searchLedgerData = function() {
				vm.manageLedgerDataService.startOfLedger = 0;
				vm.manageLedgerDataService.hasMoreLedgerData = true;
				vm.manageLedgerDataService.getCompanyLedgerList();
			};

	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					vm.manageLedgerDataService.selectedCompanyId = vm.companyList[0].companyId;
					vm.manageLedgerDataService.startOfLedger = 0;
					vm.manageLedgerDataService.getCompanyLedgerList();
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
		}
	}

	function errorCallback() {
		
	}

	vm.addLedgerData = function() {
		vm.manageLedgerDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../ledger/js/addEditLedgerData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageLedgerDataService.ledgerContainerURL = "../ledger/view/addEditLedgerData.html";
		});
	};

	vm.showDeletePrompt = function(ledgerId) {
		vm.selectedDeleteLedgerId = ledgerId;
		$("#ledger-delete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteLedgerData = function() {
		$http.post('../../ledgerData/deleteLedgerData', {ledgerId: vm.selectedDeleteLedgerId, companyId : vm.manageLedgerDataService.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId }).
		then(deleteLedgerSuccessCallback, errorCallback);	
	};

	function deleteLedgerSuccessCallback() {
		vm.closeDeleteModal();
		vm.getCompayLedgerDetail(vm.manageLedgerDataService.selectedCompanyId);
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteItemId = 0;
		$("#ledger-delete-confirmation-dialog .modal").modal('hide');
	};

	vm.editLedgerData = function(ledgerId) {
		vm.manageLedgerDataService.requestType = "Edit";
		vm.manageLedgerDataService.editLedgerId = ledgerId;
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../ledger/js/addEditLedgerData.js"],
				{serie: true}
		)
		.then(function () {
			vm.manageLedgerDataService.ledgerContainerURL = "../ledger/view/addEditLedgerData.html";
		});
	};

});

