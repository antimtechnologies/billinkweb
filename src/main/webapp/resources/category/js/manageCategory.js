angular
		.module('manageCategory',
				[ 'infinite-scroll', 'ui.bootstrap', 'toaster' ])
		.service("manageCategoryService", function($http) {
			var vm = this;
			vm.categoryList = [];
			vm.getCategoryList = function() {
				$http.post('../../categorydata/getItemCategoryList', {
					companyId : vm.selectedCompanyId,
					categoryName : vm.searchCategoryNameText
				}).then(successCallback, errorCallback);
			};

			function successCallback(response) {
				vm.categoryList = angular.copy(response.data.data);
			}

			function errorCallback(response) {
				toaster.showMessage('error', '', response.data.errMsg);
			}

		})
		.controller(
				'manageCategoryController',
				function($http, $ocLazyLoad, toaster, manageCategoryService,
						billInkDashboardService) {
					var vm = this;
					vm.manageCategoryService = manageCategoryService;
					vm.manageCategoryService.currentViewUrl = "../category/view/viewCategoryList.html";
					vm.billInkDashboardService = billInkDashboardService;
					vm.startOfCompany = 0;
					const noOfRecordForCompany = 30;
					vm.category = {};

					vm.init = function() {
						vm.manageCategoryService.categoryList = [];
						vm.getCompanyList();
					};

					vm.getCompayCategoryDetail = function(companyId) {
						vm.manageCategoryService.selectedCompanyId = companyId
						vm.manageCategoryService.getCategoryList();
					}

					vm.getCompanyList = function() {
						$http.post('../../companydata/getCompanyNameIdList', {
							start : vm.startOfCompany,
							noOfRecord : noOfRecordForCompany,
							companyName : vm.searchCompanyNameText
						}).then(getCompanyDataSuccessCallBack, errorCallback);
					};

					vm.loadMoreCompanyList = function() {
						if (vm.totalCount > vm.startOfCompany) {
							vm.startOfCompany += noOfRecordForCompany;
							vm.getCompanyList();
						}
					};

					vm.searchCategoryData = function() {
						vm.manageCategoryService.getCategoryList();
					};

					vm.getCompanyListOnSearch = function() {
						vm.startOfCompany = 0;
						vm.getCompanyList();
					};

					function getCompanyDataSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompany === 0) {
								vm.companyList = response.data.data;
								vm.totalCount = response.data.totalCount;
								if (vm.companyList.length > 0) {
									vm.manageCategoryService.selectedCompanyId = vm.companyList[0].companyId;
									vm.manageCategoryService.getCategoryList();
								}
								return;
							}
							vm.companyList = vm.companyList
									.concat(response.data.data);
						}
					}

					vm.deleteCategory = function(categoryId) {
						vm.selectedDeleteCategoryId = categoryId;
						$("#category-delelete-confirmation-dialog .modal")
								.modal('show');
					};

					vm.deleteCategoryData = function() {
						$http
								.post(
										'../../categorydata/deleteItemCategoryData',
										{
											categoryId : vm.selectedDeleteCategoryId,
											loggedInUserId : vm.billInkDashboardService.userId
										}).then(deleteCategorySuccessCallback,
										errorCallback);
					}

					function deleteCategorySuccessCallback() {
						vm.closeDeleteModal();
						vm.manageCategoryService.getCategoryList();
					}

					vm.closeDeleteModal = function() {
						vm.selectedDeleteCategoryId = 0;
						$("#category-delelete-confirmation-dialog .modal")
								.modal('hide');
					};

					vm.editCategory = function(categoryId, categoryName) {
						vm.category.categoryName = categoryName;
						vm.category.categoryId = categoryId;
						vm.category.companyId = vm.manageCategoryService.selectedCompanyId;
						$("#category-edit-confirmation-dialog .modal").modal(
								'show');
					};

					vm.UpdateCategoryData = function() {
						if (!vm.category.categoryName
								|| vm.category.categoryName === "") {
							toaster.showMessage('error', '',
									response.data.errMsg);
							return false;
						}
						$http.post('../../categorydata/updateItemCategoryData',
								vm.category).then(editCategorySuccessCallback,
								errorCallback);
					}

					function editCategorySuccessCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage('success', '',
									'data saved successfully.');
							vm.closeEditModal();
							vm.manageCategoryService.getCategoryList();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					vm.closeEditModal = function() {
						vm.selectedDeleteCategoryId = 0;
						$("#category-edit-confirmation-dialog .modal").modal(
								'hide');
					};

					vm.addCategoryData = function() {
						vm.category = {};
						vm.category.companyId = vm.manageCategoryService.selectedCompanyId;
						$("#category-add-confirmation-dialog .modal").modal(
								'show');
					};

					vm.saveCategoryData = function() {
						if (!vm.category.categoryName
								|| vm.category.categoryName === "") {
							toaster.showMessage('error', '',
									'kindly enter category name.');
							return false;
						}

						$http.post('../../categorydata/saveItemCategoryData',
								vm.category).then(saveCategorySuccessCallback,
								errorCallback);
					}

					function saveCategorySuccessCallback(response) {
						if (response.data.status === "success") {
							toaster.showMessage('success', '',
									'data saved successfully.');
							vm.closeAddModal();
							vm.manageCategoryService.getCategoryList();
						} else {
							toaster.showMessage('error', '',
									response.data.errMsg);
						}
					}

					vm.closeAddModal = function() {
						vm.selectedDeleteCategoryId = 0;
						$("#category-add-confirmation-dialog .modal").modal(
								'hide');
					};

					function errorCallback(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					}
					vm.$onDestroy = function() {
						vm.manageCategoryService.totalCount = 0;
						vm.manageCategoryService.categoryList = [];
					};

				});
