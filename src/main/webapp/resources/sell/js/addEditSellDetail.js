angular
		.module(
				'addSellData',
				[ 'fileUploadDirective', 'toaster', 'ngMaterial', 'ngMessages' ])
		.controller(
				'addSellDataController',
				function($http, fileUploadService, toaster,
						manageSellDataService, billInkDashboardService) {
					var vm = this;
					vm.manageSellDataService = manageSellDataService;
					vm.profilePhotoFile = null;

					vm.numberOfAddedItem = 1;
					vm.numberOfItem = new Array(vm.numberOfAddedItem);

					vm.sell = {};
					vm.sell.imageURLList = [];
					vm.sell.sellItemModel = [];
					vm.sell.sellItemModel[vm.numberOfAddedItem - 1] = {};

					vm.numberOfAddedFreight = 1;
					vm.numberOfFreight = new Array(vm.numberOfAddedFreight);
					vm.sell.freightList = [];
					vm.sell.freightList[vm.numberOfAddedFreight - 1] = {};

					vm.billInkDashboardService = billInkDashboardService;
					vm.taxCodeList = [];
					vm.locationList = [];
					vm.calculateIGST = false;
					vm.calculateSGST = false;
					vm.calculateCGST = false;
					vm.additionaldiscount = 0;

					vm.init = function() {
						$http.post('../../taxcodedata/getTaxCodeValueList', {})
								.then(vm.setTaxCodeList, vm.errorFetchData);
						$http
								.post(
										'../../companydata/getCompanyDetails',
										{
											companyId : vm.manageSellDataService.selectedCompanyId
										}).then(vm.setCompanyDetail,
										vm.errorFetchData);

						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						if (vm.manageSellDataService.requestType === "Edit") {
							$http
									.post(
											'../../sellData/getSellDetailsById',
											{
												sellId : vm.manageSellDataService.editSellId,
												companyId : vm.manageSellDataService.selectedCompanyId
											}).then(vm.setSellDetail,
											vm.errorFetchData);
						} else {
							vm.sell.addedBy = {};
							vm.sell.addedBy.userId = vm.billInkDashboardService.userId;
							$http
									.post(
											'../../companydata/getBillPrefixAndLastNumber',
											{
												companyId : vm.manageSellDataService.selectedCompanyId
											})
									.then(
											function(response) {
												vm.sell.billNumberPrefix = response.data.data.billNumberPrefix
														|| "";
												vm.sell.currentBillNumber = response.data.data.lastBillNumber ? response.data.data.lastBillNumber + 1
														: 1;
											}, vm.errorFetchData);
						}

						$http
								.get(
										'../../locationData/findbycompanyId?companyId='
												+ vm.manageSellDataService.selectedCompanyId)
								.then(vm.getLocationList, vm.errorFetchData);

						console.log("Company Object :");
						console
								.log(vm.billInkDashboardService.currentCompanyObj);
					};

					vm.addLedgerData = function() {
						billInkDashboardService
								.loadMenuPage({

									iconImgURL : "../images/manage_ledger.png",
									menuName : "Manage Ledger",
									url : "../ledger/view/manageLedger.html",
									js : [
											'../customdirective/infine-scroll-directive.js',
											'../ledger/js/manageLedger.js' ]

								});

					};

					vm.setCompanyDetail = function(response) {
						vm.companyDataForSell = angular
								.copy(response.data.data);
						vm.selectedStateForSell = vm.companyDataForSell.state.stateName;
					};

					vm.getLocationList = function(response) {
						/*
						 * $http.get('../../locationData/findbycompanyId?companyId='+vm.sell.companyId).then(function(response){
						 * vm.locationList[index] = response.data.result; },
						 * vm.errorFetchData);
						 */
						vm.locationList = response.data.result;
						console.log(vm.locationList);

					};
					vm.errorFetchData = function(response) {
						toaster.showMessage('error', '', response.data.errMsg);
					};

					vm.setTaxCodeList = function(response) {
						vm.taxCodeList = response.data.data;
					};

					vm.addSellFreight = function() {
						vm.numberOfAddedFreight += 1;
						vm.sell.freightList[vm.numberOfAddedFreight - 1] = {}
						vm.numberOfFreight = new Array(vm.numberOfAddedFreight);
					};

					vm.deleteSellFreight = function(index) {
						if (vm.numberOfAddedFreight === 1) {
							return;
						}
						vm.numberOfAddedFreight -= 1;
						vm.sell.freightList.splice(index, 1);
						vm.numberOfFreight = new Array(vm.numberOfAddedFreight);
					};

					vm.AddItem = function() {
						vm.numberOfAddedItem += 1;
						vm.sell.sellItemModel[vm.numberOfAddedItem - 1] = {}
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.deleteSellItem = function(index) {
						if (vm.numberOfAddedItem === 1) {
							return;
						}
						vm.numberOfAddedItem -= 1;
						vm.sell.sellItemModel.splice(index, 1);
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.setSellDetail = function(response) {
						vm.sell = angular.copy(response.data.data);
						vm.searchLedgerNameText = vm.sell.ledgerData.ledgerName;
						vm.sell.cityName = vm.sell.ledgerData.cityName;
						vm.sell.gstNumber = vm.sell.ledgerData.gstNumber;
						vm.sell.schemeOneId = vm.sell.ledgerData.schemeOneId;
						vm.sell.schemeTwoId = vm.sell.ledgerData.schemeTwoId;
						vm.sell.priceId = vm.sell.ledgerData.priceId;
						vm.sell.modifiedBy = {};
						vm.sell.modifiedBy.userId = vm.billInkDashboardService.userId;
						vm.sell.cityName = vm.sell.ledgerData.cityName;
						vm.sell.gstNo = vm.sell.ledgerData.gstNumber;
						console.log("1111111111133333333333355555555555");
						console.log(vm.sell.dispatchDate);
						console.log(vm.sell.sellDate);
						vm.sell.sellDateObj = new Date(vm.sell.sellDate);
						vm.sell.dispatchDateObj = new Date(vm.sell.dispatchDate);

						setGSTCalculationFlags();
						if (vm.sell.sellItemModel
								&& vm.sell.sellItemModel.length > 0) {
							vm.numberOfAddedItem = vm.sell.sellItemModel.length;
							vm.numberOfItem = new Array(vm.numberOfAddedItem);
						}

						if (!vm.sell.sellItemModel
								|| vm.sell.sellItemModel.length === 0) {
							vm.sell.sellItemModel = [];
							vm.sell.sellItemModel[vm.numberOfAddedItem - 1] = {};
						} else {
							vm.additionaldiscount = vm.sell.sellItemModel[0].additionaldiscount;
						}

						console.log("TESTINGGGGGG" + vm.additionaldiscount);

						if (vm.sell.freightList
								&& vm.sell.freightList.length > 0) {
							vm.numberOfAddedFreight = vm.sell.freightList.length;
							vm.numberOfFreight = new Array(
									vm.numberOfAddedFreight);
						}

						if (!vm.sell.freightList
								|| vm.sell.freightList.length === 0) {
							vm.sell.freightList = [];
							vm.sell.freightList[vm.numberOfAddedFreight - 1] = {};
						}

						for (var j = 0; j < vm.numberOfItem.length; j++) {
							var myVar = setInterval(getBatchItemDetails(j,
									vm.sell.sellItemModel[j].item.itemId), 1000);
						}
						vm.ledgerId = vm.sell.ledgerData.ledgerId;

						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : vm.sell.ledgerData.ledgerId
								}).then(function(response) {
							console.log("suucee City");
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);

						for (var i = 0; i < vm.numberOfItem.length; i++) {

							console.log("freeItemQty :"
									+ vm.sell.sellItemModel[i].freeItemQty);
							console.log("FreeitemId :"
									+ vm.sell.sellItemModel[i].freeItemId)
							console.log("ItemId :"
									+ vm.sell.sellItemModel[i].item.itemId);
							vm.sell.sellItemModel[i].freeItemQty = vm.sell.sellItemModel[i].freeItemQty;
							// vm.sell.sellItemModel[i].locationId =
							// vm.sell.sellItemModel[i].locationId;
							console.log("FreeItemId [" + i + "]:"
									+ vm.sell.sellItemModel[i].freeItemId);
							var myVar = setInterval(successGetFreeItem(i,
									vm.sell.sellItemModel[i].freeItemId,
									vm.sell.sellItemModel[i].item.itemId), 1000);
							vm.changeAmount(i);

						}

					};

					function getBatchItemDetails(i, itemId) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : itemId
										})
								.then(
										function(batchres) {

											vm.sell.sellItemModel[i].batchNoList = batchres.data.data;
											// vm.sell.sellItemModel[i].item.batchno
											// = batchres.data.data.batchNo;

											$http
													.post(
															'../../itemData/getoneBatch',
															{
																batchId : vm.sell.sellItemModel[i].batchId
															})
													.then(
															function(res) {

																vm.sell.sellItemModel[i].item.batchno = res.data.data.batchNo;
																var dt = formatDateForSale(res.data.data.mfgDate);
																var pt = formatDateForSale(res.data.data.expDate);

																vm.sell.sellItemModel[i].mfgdate = dt;
																vm.sell.sellItemModel[i].expdate = pt;

																var dtp = formatDate(res.data.data.mfgDate);
																var ptp = formatDate(res.data.data.expDate);

																vm.sell.sellItemModel[i].mfgdateForPrint = dtp;
																vm.sell.sellItemModel[i].expdateForPrint = ptp;
																// vm.sell.sellItemModel[k].batchId
																// =
																// res.data.data.batchId;

															}, errorCallback);

										}, errorCallback);

					}
					// function successGetFreeItem(i,id,itemId) {
					// $http.post('../../schemeData/getFreeItemByItemId',
					// {itemId: itemId })
					// .then(function(response){
					// vm.totalQtyOfFreeItem =
					// response.data.data.totalQtyOfItem;
					// vm.sell.sellItemModel[i].FreeQtyCheck=response.data.data.totalQtyOfItem;
					// console.log("totalQtyOfFreeItem
					// :"+vm.totalQtyOfFreeItem);
					// console.log(vm.sell.sellItemModel[i].FreeQtyCheck)
					// }, errorCallback);
					//					
					// $http.post('../../schemeData/getFreeItemNameByItemId',
					// {itemId: id})
					// .then(function(res){
					// console.log("i["+i+"] :"+i);
					// console.log("res.data.data.itemName
					// :"+res.data.data.itemName);
					// vm.sell.sellItemModel[i].FreeitemName =
					// res.data.data.itemName;
					// }, errorCallback);
					//					
					// }
					// 29-05-2019
					function successGetFreeItem(i, id, itemId) {

						if (vm.sell.schemeTwoId) {
							$http
									.post(
											'../../schemeData/getFreeItemByItemId',
											{
												itemId : itemId
											})
									.then(
											function(response) {
												vm.totalQtyOfFreeItem = response.data.data.totalQtyOfItem;
												vm.sell.sellItemModel[i].FreeQtyCheck = response.data.data.totalQtyOfItem;
												console
														.log("totalQtyOfFreeItem :"
																+ vm.totalQtyOfFreeItem);
												console
														.log(vm.sell.sellItemModel[i].FreeQtyCheck)
												if (response.data.data.totalQtyOfFreeItem > 0) {
													vm.Freequantity = response.data.data.totalQtyOfFreeItem;
													vm.sell.sellItemModel[i].freeItemId = response.data.data.freeItemId;
													vm.totalFreeQty = response.data.data.totalQtyOfFreeItem;
													// vm.totalQtyOfFreeItem =
													// response.data.data.totalQtyOfItem;
													// vm.sell.sellItemModel[i].freeItemQty
													// =
													// response.data.data.totalQtyOfFreeItem;
													vm.sell.sellItemModel[i].Totalquantity = response.data.data.totalQtyOfFreeItem;

													$http
															.post(
																	'../../schemeData/getFreeItemNameByItemId',
																	{
																		itemId : response.data.data.freeItemId
																	})
															.then(
																	function(
																			res) {
																		vm.sell.sellItemModel[i].FreeitemName = res.data.data.itemName;
//																		vm.changeAmount(i);
																		vm.changeQuantity(i);
																	},
																	errorCallback);
												}
											}, errorCallback);

							// $http.post('../../schemeData/getFreeItemNameByItemId',
							// {itemId: id})
							// .then(function(res){
							// console.log("i["+i+"] :"+i);
							// console.log("res.data.data.itemName
							// :"+res.data.data.itemName);
							// vm.sell.sellItemModel[i].FreeitemName =
							// res.data.data.itemName;
							// }, errorCallback);
						}
						;

					}

					vm.errorFetchData = function() {
					};

					function formatDateChk(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						// return [ year, month, day ].join('-');
						return [ month, day, year ].join('/');
					}

					vm.closeAddEditSellScreen = function() {
						vm.manageSellDataService
								.getSellList(vm.manageSellDataService.selectedCompanyId);
						vm.billInkDashboardService.currentViewUrl = "../sell/view/viewSellList.html";
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeAddEditSellScreen();
						} else if (response.data.status === "error") {
							alert(response.data.errMsg);
						}
					}

					function errorCallback(response) {
						alert(response.data.errMsg);
					}

					vm.saveSellData = function() {
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						vm.sell.sellDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");
						vm.sell.dispatchDate = moment(vm.sell.dispatchDateObj)
								.format("YYYY-MM-DD");
						vm.sell.billNumber = vm.sell.currentBillNumber;
						if (vm.sell.billNumberPrefix) {
							vm.sell.billNumber = vm.sell.billNumberPrefix
									.concat(vm.sell.currentBillNumber);
						}
						console.log("dispatchDateObj :"
								+ vm.sell.dispatchDateObj);
						console.log("dispatchDate ::" + vm.sell.dispatchDate);
						console.log("batch Item Id For save :"
								+ vm.sell.sellItemModel);

						$http.post('../../sellData/saveSellData', vm.sell)
								.then(successCallback, errorCallback);
					};

					vm.publishSellData = function() {
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId
						vm.sell.sellDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");
						vm.sell.dispatchDate = moment(vm.sell.dispatchDateObj)
								.format("YYYY-MM-DD");
						vm.sell.billNumber = vm.sell.currentBillNumber;
						if (vm.sell.billNumberPrefix) {
							vm.sell.billNumber = vm.sell.billNumberPrefix
									.concat(vm.sell.currentBillNumber);
						}
						$http.post('../../sellData/publishSellData', vm.sell)
								.then(successCallback, errorCallback);
					};

					function getImageURLListObj(imageURLList) {
						var sellImageURLList = [];
						var sellImageURL = {};

						for (var count = 0; count < imageURLList.length; count++) {
							sellImageURL = {};
							sellImageURL.imageURL = imageURLList[count];
							sellImageURLList.push(sellImageURL);
						}
						return sellImageURLList;
					}

					function validateSellSaveUpdate() {
						if (!vm.manageSellDataService.selectedCompanyId
								|| vm.manageSellDataService.selectedCompanyId < 1) {
							toaster
									.showMessage('error', '',
											'Company data is missing to save sell details');
							return false;
						}

						// if (!vm.sell.ledgerData ||
						// vm.sell.ledgerData.ledgerId < 1) {
						// toaster.showMessage('error', '', 'Kindly select
						// ledger data to save sell details');
						// return false;
						// }

						return true;
					}

					vm.uploadFileAndSaveSellData = function(action) {
						if (!validateSellSaveUpdate()) {
							return;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'sell')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.sell.imageURLList = vm.sell.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.sell.imageURLList
															&& vm.sell.imageURLList.length > 0) {
														vm.sell.imageURL = vm.sell.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm.saveSellData();
													} else if (action === "Publish") {
														vm.publishSellData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.sell.imageURL = null;
							if (vm.sell.imageURLList
									&& vm.sell.imageURLList.length > 0) {
								vm.sell.imageURL = vm.sell.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.saveSellData();
							} else if (action === "Publish") {
								vm.publishSellData();
							}
						}
					};

					vm.updateSavedSellData = function() {
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						vm.sell.sellDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");
						vm.sell.sellDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");
						vm.sell.billNumber = vm.sell.currentBillNumber;
						if (vm.sell.billNumberPrefix) {
							vm.sell.billNumber = vm.sell.billNumberPrefix
									.concat(vm.sell.currentBillNumber);
						}

						for ( var i in vm.sell.sellItemModel) {
							vm.sell.sellItemModel[i].additionaldiscount = vm.additionaldiscount;
						}

						console.log("CityName Update :" + vm.sell.cityName);
						console.log("GstNumber Update :" + vm.sell.gstNumber);
						console.log("location Id :"
								+ vm.sell.sellItemModel[0].locationId);
						$http.post('../../sellData/updateSavedSellData',
								vm.sell).then(successCallback, errorCallback);
					};

					vm.updatePublishSellData = function() {
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						vm.sell.sellDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");
						vm.sell.billNumber = vm.sell.currentBillNumber;
						if (vm.sell.billNumberPrefix) {
							vm.sell.billNumber = vm.sell.billNumberPrefix
									.concat(vm.sell.currentBillNumber);
						}

						for ( var i in vm.sell.sellItemModel) {
							vm.sell.sellItemModel[i].additionaldiscount = vm.additionaldiscount;
						}

						$http.post('../../sellData/updatePublishedSellData',
								vm.sell).then(successCallback, errorCallback);
					};

					vm.uploadFileAndUpdateSellData = function(action) {
						if (!validateSellSaveUpdate()) {
							return;
						}

						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'sell')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.sell.imageURLList = vm.sell.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.sell.imageURLList
															&& vm.sell.imageURLList.length > 0) {
														vm.sell.imageURL = vm.sell.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.updateSavedSellData();
													} else if (action === "Publish") {
														vm
																.updatePublishSellData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.sell.imageURL = null;
							if (vm.sell.imageURLList
									&& vm.sell.imageURLList.length > 0) {
								vm.sell.imageURL = vm.sell.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.updateSavedSellData();
							} else if (action === "Publish") {
								vm.updatePublishSellData();
							}
						}
					};

					vm.markSellAsVerified = function() {
						vm.sell.verifiedBy = {};
						vm.sell.verifiedBy.userId = vm.billInkDashboardService.userId;
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						$http
								.post('../../sellData/markSellAsVerified',
										vm.sell).then(successCallback,
										errorCallback);
					};

					vm.publishSavedSell = function() {
						vm.sell.companyId = vm.manageSellDataService.selectedCompanyId;
						$http.post('../../sellData/publishSavedSell', vm.sell)
								.then(successCallback, errorCallback);
					};

					vm.searchLedger = function() {
						if (vm.sell.ledgerData
								&& vm.sell.ledgerData.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];

							return;
						}
						if (vm.searchLedgerNameText.length > 0) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchLedgerNameText,
												companyId : vm.manageSellDataService.selectedCompanyId
											}).then(
											getLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.searchLedgerList = [];
							vm.sell.ledgerData = {};
							vm.calculateIGST = false;
							vm.calculateSGST = false;
							vm.calculateCGST = false;
						}
					};

					function getLedgerDataSuccessCallBack(response) {
						vm.sell.ledgerData = {};
						vm.calculateIGST = false;
						vm.calculateSGST = false;
						vm.calculateCGST = false;
						vm.searchLedgerList = response.data.data;
					}

					vm.selectLedger = function(ledgerObj) {
						if (!ledgerObj || !ledgerObj.ledgerName) {
							return;
						}
						vm.searchLedgerNameText = ledgerObj.ledgerName;
						vm.sell.ledgerData = ledgerObj;
						vm.sell.gstNo = ledgerObj.gstNumber;
						vm.ledgerId = ledgerObj.ledgerId;
						vm.sell.cityName = ledgerObj.cityName;
						vm.sell.gstNumber = ledgerObj.gstNumber;
						vm.sell.schemeOneId = ledgerObj.schemeOneId;
						vm.sell.schemeTwoId = ledgerObj.schemeTwoId;
						vm.sell.priceId = ledgerObj.priceId;
						setGSTCalculationFlags();
						console.log("PriceDiscount" + vm.sell.priceId);
						console.log("SchemeOne" + vm.sell.schemeOneId);
						console.log("SchemeTwo" + vm.sell.schemeTwoId);

						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : ledgerObj.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);
					};

					vm.getMRPData = function(index) {
						//		
						var billdateobj = "";
						if (vm.sell.sellDateObj) {
							billdateobj = ((new Date(vm.sell.sellDateObj))
									.getTime() / 1000);
						} else {
							billdateobj = 000;
						}
						$http
								.post(
										"../../itemData/getMRPRate",
										{
											itemId : vm.sell.sellItemModel[index].item.itemId,
											billDate : billdateobj
										})
								.then(
										function(response) {
											vm.sell.sellItemModel[index].sellRate = response.data.data[0].itemMrp;
											// console.log();
										}, errorCallback);

					}

					vm.getLedgerCityList = function() {

						var flag = true;
						
						
						angular.forEach(vm.ledgerCityList,function(ledgerList) {

											if (flag == true && vm.sell.cityName.toLowerCase() === ledgerList.city.toLowerCase()) {

												console.log(ledgerList);
												console.log(vm.billInkDashboardService.currentCompanyObj.state.stateId);
												console.log(vm.sell.ledgerData.state.stateId);
												console.log("Before Set");
												vm.sell.ledgerData.state.stateId=ledgerList.stateid;
												console.log("After Set");
												console.log(vm.sell.ledgerData.state.stateId);
												vm.sell.gstNumber = ledgerList.multipleGst;
												flag = false;
												return;
											}
										});
						setGSTCalculationFlags();
					};

					vm.setStateList = function(response) {

						vm.billInkDashboardService.stateList = [];
						vm.billInkDashboardService.stateList.stateList = response.data.data;
						console
								.log(vm.billInkDashboardService.stateList.stateList);
						$("#add-buyer-section-model .modal").modal('show');
					};
					vm.errorFetchList = function(response) {

					};

					function setGSTCalculationFlags() {
						
						console.log(vm.billInkDashboardService.igstTaxCalGSTType.indexOf(vm.sell.ledgerData.gstType));
//						if (vm.billInkDashboardService.igstTaxCalGSTType
//								.indexOf(vm.sell.ledgerData.gstType) > -1) {
//							vm.calculateIGST = true;
//						} else {
							console.log("Vishal is working");
							if (vm.sell.ledgerData
									&& vm.billInkDashboardService.currentCompanyObj
									&& vm.sell.ledgerData.state
									&& vm.sell.ledgerData.state.stateId === vm.billInkDashboardService.currentCompanyObj.state.stateId) {
								vm.calculateSGST = true;
								vm.calculateCGST = true;
								vm.calculateIGST = false;
							} else {
								vm.calculateIGST = true;
								vm.calculateSGST=false;
								vm.calculateCGST=false;
							}
							console.log("GST FLAG: I,S,C");
							console.log(vm.calculateIGST);
							console.log(vm.calculateSGST);
							console.log(vm.calculateCGST);
//						}
					}

					vm.fetchItemList = function(index) {
						var itemSearchText = vm.sell.sellItemModel[index].item.itemName;
						// if (itemSearchText.length > 2) {
						return $http
								.post(
										'../../itemData/getItemBasicDetailList',
										{
											itemName : itemSearchText,
											companyId : vm.manageSellDataService.selectedCompanyId,
											start : 0,
											noOfRecord : 30
										})
								.then(
										function(response) {
											vm.itemList = [];
											vm.itemList[index] = {};
											vm.itemList[index].itemData = response.data.data;
											vm.sell.sellItemModel[index].quantity = 1;
											vm.getLocationList(index);
											vm.getBatchnolist(index);
											return vm.itemList[index].itemData;
										}, errorCallback);
						// }
					};

					vm.getDiscount = function(index) {

						console.log("get Discount");
						if (vm.sell.priceId === 1) {
							var rate = vm.sell.sellItemModel[index].sellRate;

							// if (itemSearchText.length > 2) {

							if (vm.sell.sellItemModel[index]) {
								$http
										.post('../../ledgerData/getDiscount', {
											ledgerId : vm.ledgerId
										})
										.then(
												function(response) {

													vm.sell.sellItemModel[index].discount = response.data.data;
												}, errorCallback);

							}
						}
					};

					vm.getBatchnolist = function(index) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : vm.sell.sellItemModel[index].item.itemId
										})
								.then(
										function(batchres) {
											vm.sell.sellItemModel[index].batchNoList = batchres.data.data;
											vm.sell.sellItemModel[index].item.batchno = batchres.data.data[0].batchNo;
											vm.sell.sellItemModel[index].batchId = batchres.data.data[0].batchId;

											var dt = formatDateForSale(batchres.data.data[0].mfgDate);
											var pt = formatDateForSale(batchres.data.data[0].expDate);

											vm.sell.sellItemModel[index].mfgdate = dt;
											vm.sell.sellItemModel[index].expdate = pt;

											var dtp = formatDate(batchres.data.data[0].mfgDate);
											var ptp = formatDate(batchres.data.data[0].expDate);

											vm.sell.sellItemModel[index].mfgdateForPrint = dtp;
											vm.sell.sellItemModel[index].expdateForPrint = ptp;

										}, errorCallback);

					}

					vm.batchAddItemModel = function(index) {
						console.log("call this index for urcjase controller :"
								+ index);
						console.log("Purchase Item Id :"
								+ vm.sell.sellItemModel[index].item.itemId);
						vm.sell.itemId = vm.sell.sellItemModel[index].item.itemId;
						vm.index = index;
						console.log("batchAddItemModel Index :" + vm.index);
						// $("#add-batch-sell-dialog .modal").modal('show');
						$("#new-batch-sell-dialog .modal").modal('show');

					}

					// 15-05-2019 Start
					vm.getOneBatch = function(index) {
						console.log("getOneFucntion call");
						console.log("Fucntion Index:" + index);
						console.log("BatchId :"
								+ vm.sell.sellItemModel[index].batchId);
						if (vm.sell.sellItemModel[index].batchId
								&& vm.sell.sellItemModel[index].batchId != undefined) {
							console.log("If batchID :"
									+ vm.sell.sellItemModel[index].batchId);
							$http
									.post(
											'../../itemData/getoneBatch',
											{
												batchId : vm.sell.sellItemModel[index].batchId
											})
									.then(
											function(res) {

												var dt = formatDateForSale(res.data.data.mfgDate);
												var pt = formatDateForSale(res.data.data.expDate);
												vm.sell.sellItemModel[index].item.batchno = res.data.data.batchNo;
												vm.sell.sellItemModel[index].mfgdate = dt;
												vm.sell.sellItemModel[index].expdate = pt;

												var dtp = formatDate(res.data.data.mfgDate);
												var ptp = formatDate(res.data.data.expDate);

												vm.sell.sellItemModel[index].mfgdateForPrint = dtp;
												vm.sell.sellItemModel[index].expdateForPrint = ptp;
												// vm.sell.sellItemModel[index].batchId
												// = res.data.data.batchId;

											}, errorCallback);

						}
					}

					// vm.saveBatchForSell = function() {
					//		
					// if (!vm.sell.batchNo || vm.sell.batchNo === "") {
					// toaster.showMessage('error', '',
					// 'Kindly enter BatchNo.');
					// return false;
					// }
					// if (!vm.sell.mfgDate || vm.sell.mfgDate === "") {
					// toaster.showMessage('error', '',
					// 'Kindly enter Mfg Date.');
					// return false;
					// }
					// // if (!vm.purchase.expDate || vm.purchase.expDate ===
					// // "") {
					// // toaster.showMessage('error', '', 'Kindly enter Exp
					// // Date.');
					// // return false;
					// // }
					//
					// if (document.getElementById("dayMonthDD").value ==
					// "Month") {
					// var date = new Date(vm.sell.mfgDate);
					// var month = date.getMonth()
					// + vm.sell.shelfLifes;
					// date.setMonth(month);
					// vm.sell.expDate = moment(date).format(
					// "YYYY-MM-DD");
					// // vm.itemBatchObj.expDates =
					// // moment(date).format("MM-DD-YYYY");
					// } else if (document.getElementById("dayMonthDD").value ==
					// "days") {
					// vm.sell.expDate = moment(vm.sell.mfgDate)
					// .format("YYYY-MM-DD");
					// }
					//
					// vm.sell.mfgDate = moment(vm.sell.mfgDate)
					// .format("YYYY-MM-DD");
					// // vm.purchase.expDate =
					// // moment(vm.purchase.expDate).format("YYYY-MM-DD");
					// // vm.billInkDashboardService.item.push(vm.purchase);
					// //console.log("shelflife :" + vm.purchase.shelfLifes);
					// vm.sell.shelfLife = document
					// .getElementById("dayMonthDD").value
					// + "@" + vm.sell.shelfLifes;
					//
					// $http.post('../../itemData/savebatchData', vm.sell)
					// .then(successCallBatchback, errorCallback);
					// $("#add-batch-sell-dialog .modal").modal('hide');
					// };

					// 15-05-2019 End
					vm.setmfgmft = function(index, batchid) {

						var dt = formatDateForPurchase(vm.sell.sellItemModel[index].mfgDate);
						var pt = formatDateForPurchase(expDate);

						var dtp = formatDate(mfgDate);
						var ptp = formatDate(expDate);
						vm.sell.sellItemModel[index].mfgdate = dt;
						vm.sell.sellItemModel[index].expdate = pt;
						//		
						vm.sell.sellItemModel[index].mfgdateForPrint = dtp;
						vm.sell.sellItemModel[index].expdateForPrint = ptp;

					}

					// item batch No Search Start
					vm.fetchBatchNoItemList = function(index) {
						var itemBatchNoSearchText = vm.sell.sellItemModel[index].item.batchno;
						console.log("ItemBatch No :" + itemBatchNoSearchText);
						// if (itemSearchText.length > 2) {
						$http
								.post(
										'../../schemeData/getBatchItemDetails',
										{
											batchNo : itemBatchNoSearchText,
											itemId : vm.sell.sellItemModel[index].item.itemId
										})
								.then(
										function(response) {
											vm.itemBatchNoList = [];
											vm.itemBatchNoList[index] = {};
											vm.itemBatchNoList[index].itemBatchData = response.data.data;
										}, errorCallback);
						// }
					};
					vm.selectBatchItem = function(index, itemBatchObj) {
						vm.sell.sellItemModel[index].item = {};
						vm.sell.sellItemModel[index].item.batchNo = itemBatchObj.batchNo;
						vm.sell.sellItemModel[index].item.mfgDate = itemBatchObj.mfgDate;
						vm.sell.sellItemModel[index].item.expDate = itemBatchObj.expDate;
					};

					// item batch No Search End

					vm.selectItem = function(index, itemObj) {
						if (!itemObj) {
							return;
						}

						if (vm.sell.sellItemModel[index]
								&& vm.sell.sellItemModel[index].sellItemMappingId > 0) {
							vm.sell.sellItemModel[index].sellItemMappingId = 0;
							return;
						}
						vm.sell.sellItemModel[index].item = {};
						vm.sell.sellItemModel[index].item.itemName = itemObj.itemName;
						vm.sell.sellItemModel[index].item.itemId = itemObj.itemId;
						vm.sell.sellItemModel[index].item.hsnCode = itemObj.hsnCode;
						vm.sell.sellItemModel[index].taxRate = itemObj.taxCode;
						// vm.sell.sellItemModel[index].sellRate =
						// itemObj.sellRate;
						vm.ItemId = vm.sell.sellItemModel[index].item.itemId;
						vm.sell.sellItemModel[index].taxRatefreeItem = itemObj.taxCode;

						vm.sell.BillDate = moment(vm.sell.sellDateObj).format(
								"YYYY-MM-DD");

						console.log("date sell :" + vm.sell.BillDate);
						vm.getMRPData(index);

						$http
								.post(
										'../../itemData/getItemMrpByItemId',
										{
											itemId : vm.sell.sellItemModel[index].item.itemId,
											wetDate : vm.sell.BillDate
										})
								.then(
										function(response) {
											if (response.data.data.length > 0) {
												vm.sell.sellItemModel[index].sellRate = response.data.data[0].itemMrp;
											}

										}, errorCallback);

						// if
						// (vm.billInkDashboardService.noTaxCodeGSTType.indexOf(vm.sell.ledgerData.gstType)
						// > -1) {
						// vm.sell.sellItemModel[index].taxRate = 0;
						// }

						if (vm.sell.ledgerData
								&& vm.billInkDashboardService.noTaxCodeGSTType
										.indexOf(vm.sell.ledgerData.gstType) > -1) {
							vm.sell.sellItemModel[index].taxRate = 0;

						}

						if (vm.sell.schemeTwoId) {
							$http
									.post(
											'../../schemeData/getFreeItemByItemId',
											{
												itemId : vm.sell.sellItemModel[index].item.itemId
											})
									.then(
											function(response) {
												console.log("index :" + index);
												// vm.sell.sellItemModel[index].Freequantity
												// =
												// response.data.data.totalQtyOfFreeItem;
												// if
												// (response.data.data.totalQtyOfFreeItem
												// > 0) {
												vm.Freequantity = response.data.data.totalQtyOfFreeItem;
												vm.sell.sellItemModel[index].freeItemId = response.data.data.freeItemId;
												vm.totalFreeQty = response.data.data.totalQtyOfFreeItem;
												vm.totalQtyOfFreeItem = response.data.data.totalQtyOfItem;

												vm.sell.sellItemModel[index].Totalquantity = response.data.data.totalQtyOfFreeItem;

												$http
														.post(
																'../../schemeData/getFreeItemNameByItemId',
																{
																	itemId : response.data.data.freeItemId
																})
														.then(
																function(res) {
																	vm.sell.sellItemModel[index].FreeitemName = res.data.data.itemName;
																	vm.changeAmount(index);
																	vm.changeQuantity(index);
																},
																errorCallback);
												// }
											}, errorCallback);
						}
						vm.getBatchnolist(index);
						vm.getDiscount(index);

					};

					vm.closedSellBatchModal = function() {
						$("#new-batch-sell-dialog .modal").modal('hide');
					}

					vm.saveBatchSell = function() {
						console.log("sell Function call :" + vm.sell.batchNo);
						if (!vm.sell.batchNo || vm.sell.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.sell.mfgDate || vm.sell.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.sell.mfgDate);
							vm.sell.mfgDate = moment(date).format("YYYY-MM-DD");
							var month = date.getMonth() + vm.sell.shelfLifes;
							date.setMonth(month);
							vm.sell.expDate = moment(date).format("YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							console.log("start day wise");
							var date = new Date(vm.sell.mfgDate);
							vm.sell.mfgDate = moment(date).format("YYYY-MM-DD");
							console.log(date);
							date.setDate(date.getDate() + vm.sell.shelfLifes);
							console.log("after add date" + date);
							vm.sell.expDate = moment(date).format("YYYY-MM-DD");
							console.log("final date date" + vm.sell.expDate);
						}

						// vm.purchase.expDate =
						// moment(vm.purchase.expDate).format("YYYY-MM-DD");
						// vm.billInkDashboardService.item.push(vm.purchase);
						// console.log("shelflife :" + vm.purchase.shelfLifes);
						vm.sell.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.sell.shelfLifes;
						console.log("itemID :" + vm.sell.itemId);
						vm.batchNo = vm.sell.batchNo;
						vm.mfgDate = vm.sell.mfgDate;
						vm.shelfLife = vm.sell.shelfLife;
						vm.id = vm.ItemId;
						// return false;
						$http
								.post('../../itemData/saveBatch', {
									batchNo : vm.batchNo,
									mfgDate : vm.mfgDate,
									expDate : vm.sell.expDate,
									shelfLife : vm.shelfLife,
									itemId : vm.id
								})
								.then(
										function(response) {
											toaster.showMessage('success', '',
													'data saved successfully.');
											vm.sell.sellItemModel[vm.index].batchNoList
													.push(response.data.data);
											vm.sell.sellItemModel[vm.index].batchId = response.data.data.batchId;
											var dt = formatDateForSale(response.data.data.mfgDate);
											var pt = formatDateForSale(response.data.data.expDate);
											vm.sell.sellItemModel[vm.index].mfgdate = dt;
											vm.sell.sellItemModel[vm.index].expdate = pt;
											$("#new-batch-sell-dialog .modal")
													.modal('hide');
										}, errorCallback);
						vm.sell.mfgDate = "";
						vm.sell.batchNo = "";
						vm.sell.shelfLife = "";
						// $("#new-batch-sell-dialog .modal").modal('hide');
					};

					vm.closedBatch = function() {
						$("#new-batch-sell-dialog .modal").modal('hide');
					}
					function formatDateForSale(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						console.log("date of d:" + d);
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					vm.changeQuantity = function(index) {
						console.log(vm.sell.sellItemModel[index].quantity);
						console.log("Free qty:"
								+ vm.sell.sellItemModel[index].quantity);
						console.log("Free:" + vm.totalQtyOfFreeItem)
						if (vm.sell.sellItemModel[index].quantity >= vm.totalQtyOfFreeItem) {
							// alert("vishal");
							console
									.log(((vm.Freequantity * vm.sell.sellItemModel[index].quantity) / vm.totalQtyOfFreeItem));
							finalfreeqty = (vm.Freequantity * Math
									.trunc(vm.sell.sellItemModel[index].quantity
											/ vm.totalQtyOfFreeItem));
							// alert(finalfreeqty);
							vm.sell.sellItemModel[index].freeItemQty = finalfreeqty;
						} else {
							vm.sell.sellItemModel[index].freeItemQty = 0;
						}
					}

					function getFreeItemNameSuccessCallBack(response) {
						console.log("Free ItemName :"
								+ response.data.data.itemName);

					}

					vm.changeAmount = function(index) {
						console.log("VISHAL is working.");
						console.log(index);
						if (index == -1) {
							vm.sell.totalBillAmount = 0;
							vm.sell.totalBillAmount = parseFloat(vm.sell.grossTotalAmount);
							if (vm.sell.discount && vm.sell.discount > 0) {

								if (vm.sell.purchaseMeasureDiscountInAmount) {
									vm.sell.totalBillAmount -= vm.sell.discount;
								} else {
									vm.sell.totalBillAmount -= (vm.sell.totalBillAmount
											* vm.sell.discount / 100);
									console.log("Total :"
											+ vm.sell.totalBillAmount);
								}

							}
							vm.sell.totalBillAmount = parseFloat(
									vm.sell.totalBillAmount).toFixed(2);
						} else {
							vm.sell.sellItemModel[index].totalItemAmount = 0;
							vm.sell.sellItemModel[index].itemAmount = 0;
							vm.sell.sellItemModel[index].sgst = 0;
							vm.sell.sellItemModel[index].cgst = 0;
							vm.sell.sellItemModel[index].igst = 0;
							vm.sell.grossTotalAmount = 0;
							vm.sell.totalBillAmount = 0;

							var ItemAmount = 0, ItemAmountWithTax = 0, ItemDiscount = 0, ItemAmountWithTaxDiscount = 0;
							vm.changeQuantity(index);

							if (vm.sell.sellItemModel[index].item
									&& vm.sell.sellItemModel[index].item.itemId
									&& vm.sell.sellItemModel[index].quantity
									&& vm.sell.sellItemModel[index].sellRate) {

								ItemAmount = vm.sell.sellItemModel[index].sellRate
										* vm.sell.sellItemModel[index].quantity;
								ItemAmountWithTax = ItemAmount
										+ (ItemAmount
												* vm.sell.sellItemModel[index].taxRate / 100);

								if (vm.sell.sellItemModel[index].discount) {

									if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
										ItemDiscount = vm.sell.sellItemModel[index].discount;
										ItemAmountWithTaxDiscount = vm.sell.sellItemModel[index].discount;
									} else {
										ItemDiscount = (ItemAmount
												* vm.sell.sellItemModel[index].discount / 100);
										ItemAmountWithTaxDiscount = (ItemAmountWithTax
												* vm.sell.sellItemModel[index].discount / 100);
									}
								}

								if (vm.sell.sellItemModel[index].additionaldiscount) {

									if (vm.sell.sellItemModel[index].addmeasureDiscountInAmount) {
										ItemAmount -= vm.sell.sellItemModel[index].additionaldiscount;
										ItemAmountWithTax -= vm.sell.sellItemModel[index].additionaldiscount;
									} else {
										ItemAmount -= (ItemAmount
												* vm.sell.sellItemModel[index].additionaldiscount / 100);
										ItemAmountWithTax -= (ItemAmountWithTax
												* vm.sell.sellItemModel[index].additionaldiscount / 100);
									}
								}

								ItemAmount -= ItemDiscount;
								ItemAmountWithTax -= ItemAmountWithTaxDiscount;

							}

							vm.sell.sellItemModel[index].itemAmount = ItemAmount
									.toFixed(2);
							vm.sell.sellItemModel[index].totalItemAmount = ItemAmountWithTax
									.toFixed(2);

							var taxAmount = (ItemAmount * vm.sell.sellItemModel[index].taxRate) / 100;
							if (vm.calculateIGST)
								vm.sell.sellItemModel[index].igst = taxAmount
										.toFixed(2);

							taxAmount = taxAmount / 2;
							if (vm.calculateSGST)
								vm.sell.sellItemModel[index].sgst = taxAmount
										.toFixed(2);

							if (vm.calculateCGST)
								vm.sell.sellItemModel[index].cgst = taxAmount
										.toFixed(2);

							for (var counter = 0; counter < vm.sell.sellItemModel.length; counter++) {
								if (vm.sell.sellItemModel[counter].totalItemAmount
										&& vm.sell.sellItemModel[counter].totalItemAmount > 0) {
									vm.sell.grossTotalAmount += parseFloat(vm.sell.sellItemModel[counter].totalItemAmount);
								}
							}

							console.log("call gross total Frieghtlist"
									+ vm.sell.freightList.length);
							if (vm.sell.freightList.length > 0) {
								for (var count = 0; count < vm.sell.freightList.length; count++) {
									if (vm.sell.freightList[count].totalItemAmount)
										vm.sell.grossTotalAmount += parseFloat(vm.sell.freightList[count].totalItemAmount);
								}
							}

							vm.sell.grossTotalAmount = parseFloat(
									vm.sell.grossTotalAmount).toFixed(2);

//							if (vm.additionaldiscount === 0
//									&& vm.sell.grossTotalAmount > 0) {
//								// console.log("Vishal121212 is checking");
//
//								vm.getAdditionDiscount(index);
//							}

							if (vm.additionaldiscount > 0) {
								console.log("additional Discount"
										+ vm.additionaldiscount);
								ItemAmount = vm.sell.sellItemModel[index].sellRate
										* vm.sell.sellItemModel[index].quantity;
								console.log("ITEM AMOUNT" + ItemAmount);

								if (vm.sell.sellItemModel[index].discount) {

									if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
										ItemDiscount = vm.sell.sellItemModel[index].discount;
										ItemAmountWithTaxDiscount = vm.sell.sellItemModel[index].discount;
									} else {
										ItemDiscount = (ItemAmount
												* vm.sell.sellItemModel[index].discount / 100);
										ItemAmountWithTaxDiscount = (ItemAmountWithTax
												* vm.sell.sellItemModel[index].discount / 100);
									}
								}
								ItemAmount -= (ItemDiscount + (ItemAmount
										* vm.additionaldiscount / 100));

								ItemAmountWithTax = ItemAmount
										+ (ItemAmount
												* vm.sell.sellItemModel[index].taxRate / 100);

								vm.sell.sellItemModel[index].itemAmount = ItemAmount
										.toFixed(2);

								console
										.log("AFTER additional Discount ITEM AMOUNT"
												+ ItemAmount);

								vm.sell.sellItemModel[index].totalItemAmount = ItemAmountWithTax
										.toFixed(2);
								var taxAmount = (ItemAmount * vm.sell.sellItemModel[index].taxRate) / 100;
								if (vm.calculateIGST)
									vm.sell.sellItemModel[index].igst = taxAmount
											.toFixed(2);

								taxAmount = taxAmount / 2;
								if (vm.calculateSGST)
									vm.sell.sellItemModel[index].sgst = taxAmount
											.toFixed(2);

								if (vm.calculateCGST)
									vm.sell.sellItemModel[index].cgst = taxAmount
											.toFixed(2);

								for (var counter = 0; counter < vm.sell.sellItemModel.length; counter++) {
									if (vm.sell.sellItemModel[counter].totalItemAmount
											&& vm.sell.sellItemModel[counter].totalItemAmount > 0) {
										vm.sell.grossTotalAmount += parseFloat(vm.sell.sellItemModel[counter].totalItemAmount);
									}
								}

								console.log("call gross total Frieghtlist"+ vm.sell.freightList.length);
								if (vm.sell.freightList.length > 0) {
									for (var count = 0; count < vm.sell.freightList.length; count++) {
										if (vm.sell.freightList[count].totalItemAmount)
											vm.sell.grossTotalAmount += parseFloat(vm.sell.freightList[count].totalItemAmount);
									}
								}

								vm.sell.grossTotalAmount = parseFloat(
										vm.sell.grossTotalAmount).toFixed(2);

							}

							vm.sell.totalBillAmount = parseFloat(vm.sell.grossTotalAmount);
							console
									.log("3344433344433"
											+ vm.additionaldiscount);

							if (vm.sell.discount && vm.sell.discount > 0) {

								if (vm.sell.purchaseMeasureDiscountInAmount) {
									vm.sell.totalBillAmount -= vm.sell.discount;
								} else {
									vm.sell.totalBillAmount -= (vm.sell.totalBillAmount
											* vm.sell.discount / 100);
								}

							}

							vm.sell.totalBillAmount = parseFloat(
									vm.sell.totalBillAmount).toFixed(2);

						}
					}

					/*
					 * vm.getAmount = function(index) {
					 * 
					 * //console.log("ttttttt"); // if(vm.additionaldiscount >
					 * 0){ //
					 * vm.sell.sellItemModel[index].additionaldiscount=vm.additionaldiscount; // }
					 * 
					 * //console.log("According Vishal");
					 * //console.log("According Vishal");
					 * vm.sell.sellItemModel[index].totalItemAmount = 0;
					 * if(vm.sell.sellItemModel[index].item &&
					 * vm.sell.sellItemModel[index].item.itemId &&
					 * vm.sell.sellItemModel[index].quantity &&
					 * vm.sell.sellItemModel[index].sellRate) {
					 * vm.sell.sellItemModel[index].totalItemAmount =
					 * vm.sell.sellItemModel[index].sellRate *
					 * vm.sell.sellItemModel[index].quantity;
					 * vm.sell.sellItemModel[index].totalItemAmount +=
					 * (vm.sell.sellItemModel[index].totalItemAmount *
					 * vm.sell.sellItemModel[index].taxRate / 100);
					 * 
					 * if (vm.sell.sellItemModel[index].discount) { if
					 * (vm.sell.sellItemModel[index].measureDiscountInAmount) {
					 * vm.sell.sellItemModel[index].totalItemAmount -=
					 * vm.sell.sellItemModel[index].discount; } else {
					 * vm.sell.sellItemModel[index].totalItemAmount -=
					 * (vm.sell.sellItemModel[index].totalItemAmount *
					 * vm.sell.sellItemModel[index].discount / 100); } }
					 * 
					 * if (vm.sell.sellItemModel[index].additionaldiscount) { if
					 * (vm.sell.sellItemModel[index].addmeasureDiscountInAmount ) {
					 * vm.sell.sellItemModel[index].totalItemAmount -=
					 * vm.sell.sellItemModel[index].additionaldiscount; } else {
					 * vm.sell.sellItemModel[index].totalItemAmount -=
					 * (vm.sell.sellItemModel[index].totalItemAmount *
					 * vm.sell.sellItemModel[index].addmeasureDiscountInAmount /
					 * 100); } } } vm.sell.sellItemModel[index].totalItemAmount =
					 * vm.sell.sellItemModel[index].totalItemAmount.toFixed(2);
					 * return vm.sell.sellItemModel[index].totalItemAmount; };
					 */

					/*
					 * vm.getItemLevelAmount = function(index) {
					 * vm.sell.sellItemModel[index].itemAmount = 0;
					 * if(vm.sell.sellItemModel[index].item &&
					 * vm.sell.sellItemModel[index].item.itemId &&
					 * vm.sell.sellItemModel[index].quantity &&
					 * vm.sell.sellItemModel[index].sellRate) { var itemAmount =
					 * vm.sell.sellItemModel[index].sellRate *
					 * vm.sell.sellItemModel[index].quantity;
					 * 
					 * var ItemDiscount = 0, ItemAmountWithTaxDiscount = 0;
					 * 
					 * if (vm.sell.sellItemModel[index].discount) {
					 * 
					 * if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
					 * ItemDiscount = vm.sell.sellItemModel[index].discount; }
					 * else { ItemDiscount = (itemAmount *
					 * vm.sell.sellItemModel[index].discount / 100); } } if
					 * (vm.additionaldiscount) { if
					 * (vm.sell.sellItemModel[index].addmeasureDiscountInAmount ) {
					 * itemAmount -= vm.additionaldiscount;
					 * //vm.sell.sellItemModel[index].additionaldiscount } else {
					 * itemAmount -= (itemAmount * vm.additionaldiscount / 100);
					 * //vm.sell.sellItemModel[index].additionaldiscount } }
					 * itemAmount -= ItemDiscount;
					 * vm.sell.sellItemModel[index].itemAmount =
					 * parseFloat(itemAmount).toFixed(2); } return
					 * vm.sell.sellItemModel[index].itemAmount; };
					 */

					/*
					 * vm.getSGSTAmount = function(index) {
					 * 
					 * vm.sell.sellItemModel[index].sgst = 0;
					 * 
					 * if(vm.calculateSGST) {
					 * if(vm.sell.sellItemModel[index].item &&
					 * vm.sell.sellItemModel[index].item.itemId &&
					 * vm.sell.sellItemModel[index].quantity &&
					 * vm.sell.sellItemModel[index].sellRate) { var itemAmount =
					 * vm.sell.sellItemModel[index].sellRate *
					 * vm.sell.sellItemModel[index].quantity;
					 * 
					 * if (vm.sell.sellItemModel[index].discount) {
					 * 
					 * if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
					 * itemAmount -= vm.sell.sellItemModel[index].discount; }
					 * else { itemAmount -= (itemAmount *
					 * vm.sell.sellItemModel[index].discount / 100); } }
					 * 
					 * var taxAmount = (itemAmount *
					 * vm.sell.sellItemModel[index].taxRate / 100);
					 * vm.sell.sellItemModel[index].sgst = taxAmount / 2; } }
					 * vm.sell.sellItemModel[index].sgst =
					 * parseFloat(vm.sell.sellItemModel[index].sgst).toFixed(2);
					 * return vm.sell.sellItemModel[index].sgst; };
					 */

					/*
					 * vm.getCGSTAmount = function(index) {
					 * 
					 * vm.sell.sellItemModel[index].cgst = 0;
					 * if(vm.calculateCGST) {
					 * if(vm.sell.sellItemModel[index].item &&
					 * vm.sell.sellItemModel[index].item.itemId &&
					 * vm.sell.sellItemModel[index].quantity &&
					 * vm.sell.sellItemModel[index].sellRate) { var itemAmount =
					 * vm.sell.sellItemModel[index].sellRate *
					 * vm.sell.sellItemModel[index].quantity;
					 * 
					 * if (vm.sell.sellItemModel[index].discount) {
					 * 
					 * if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
					 * itemAmount -= vm.sell.sellItemModel[index].discount; }
					 * else { itemAmount -= (itemAmount *
					 * vm.sell.sellItemModel[index].discount / 100); } } var
					 * taxAmount = (itemAmount *
					 * vm.sell.sellItemModel[index].taxRate / 100);
					 * vm.sell.sellItemModel[index].cgst = taxAmount / 2; } }
					 * vm.sell.sellItemModel[index].cgst =
					 * parseFloat(vm.sell.sellItemModel[index].cgst).toFixed(2);
					 * return vm.sell.sellItemModel[index].cgst; };
					 */

					/*
					 * vm.getIGSTAmount = function(index) {
					 * 
					 * vm.sell.sellItemModel[index].igst = 0;
					 * 
					 * if(vm.calculateIGST) {
					 * if(vm.sell.sellItemModel[index].item &&
					 * vm.sell.sellItemModel[index].item.itemId &&
					 * vm.sell.sellItemModel[index].quantity &&
					 * vm.sell.sellItemModel[index].sellRate) { var itemAmount =
					 * vm.sell.sellItemModel[index].sellRate *
					 * vm.sell.sellItemModel[index].quantity;
					 * 
					 * if (vm.sell.sellItemModel[index].discount) {
					 * 
					 * if (vm.sell.sellItemModel[index].measureDiscountInAmount) {
					 * itemAmount -= vm.sell.sellItemModel[index].discount; }
					 * else { itemAmount -= (itemAmount *
					 * vm.sell.sellItemModel[index].discount / 100); } } var
					 * taxAmount = (itemAmount *
					 * vm.sell.sellItemModel[index].taxRate / 100);
					 * vm.sell.sellItemModel[index].igst = taxAmount; } }
					 * vm.sell.sellItemModel[index].igst =
					 * parseFloat(vm.sell.sellItemModel[index].igst).toFixed(2);
					 * return vm.sell.sellItemModel[index].igst; };
					 */

					/*
					 * vm.getGrossTotalAmount = function() {
					 * vm.sell.grossTotalAmount = 0; for (var counter = 0;
					 * counter < vm.sell.sellItemModel.length; counter++) { if
					 * (vm.sell.sellItemModel[counter].totalItemAmount &&
					 * vm.sell.sellItemModel[counter].totalItemAmount > 0) {
					 * vm.sell.grossTotalAmount +=
					 * parseFloat(vm.sell.sellItemModel[counter].totalItemAmount); } }
					 * 
					 * if (vm.sell.freightList.length > 0) { for(var count = 0;
					 * count < vm.sell.freightList.length; count++) { if
					 * (vm.sell.freightList[count].totalItemAmount)
					 * vm.sell.grossTotalAmount +=
					 * parseFloat(vm.sell.freightList[count].totalItemAmount); } } //
					 * console.log("vm.additionaldiscount
					 * "+vm.additionaldiscount); //
					 * console.log("vm.sell.grossTotalAmount
					 * "+vm.sell.grossTotalAmount); if(vm.additionaldiscount ===
					 * 0 && vm.sell.grossTotalAmount > 0){
					 * //console.log(vm.additionaldiscount); //
					 * console.log("Vishal121212 is checking");
					 * //console.log(vm.additionaldiscount);
					 * vm.getAdditionDiscount(); } vm.sell.grossTotalAmount =
					 * parseFloat(vm.sell.grossTotalAmount).toFixed(2);
					 * 
					 * return vm.sell.grossTotalAmount; };
					 */

					/*
					 * vm.getTotalBillAmount = function() {
					 * vm.sell.totalBillAmount =
					 * parseFloat(vm.sell.grossTotalAmount);
					 * 
					 * if (vm.sell.discount && vm.sell.discount > 0) { if
					 * (vm.sell.sellMeasureDiscountInAmount) {
					 * vm.sell.totalBillAmount -= vm.sell.discount; } else {
					 * vm.sell.totalBillAmount -= (vm.sell.totalBillAmount *
					 * vm.sell.discount / 100); } }
					 * 
					 * vm.sell.totalBillAmount =
					 * parseFloat(vm.sell.totalBillAmount).toFixed(2);
					 * 
					 * if (parseFloat(vm.sell.totalBillAmount) > 0) { //
					 * vm.prepareHSNCodeWiseTaxList(); }
					 * 
					 * return vm.sell.totalBillAmount; };
					 */

					vm.openImageModel = function(index) {
						if (index < 0 || index > (vm.sell.imageURLList - 1)) {
							return;
						}

						vm.sellImageIndex = index;
						$("#sell-image-show-model .modal").modal('show');
						vm.selectedPhotoURL = vm.sell.imageURLList[index].imageURL;
					};

					vm.fetchImage = function(position) {
						vm.sellImageIndex += position;

						if (vm.sellImageIndex < 0) {
							vm.sellImageIndex = vm.sell.imageURLList.length - 1;
						} else if (vm.sellImageIndex >= vm.sell.imageURLList.length) {
							vm.sellImageIndex = 0;
						}
						vm.selectedPhotoURL = vm.sell.imageURLList[vm.sellImageIndex].imageURL;
					};

					vm.deleteImage = function(index) {
						vm.sell.imageURLList.splice(index, 1);
					};

					// Freight related method.
					vm.fetchfreightLedgerList = function(index) {
						var ledgerSearchText = vm.sell.freightList[index].ledger.ledgerName;
						if (ledgerSearchText.length > 2) {
							$http.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : ledgerSearchText,
												companyId : vm.manageSellDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 30
											})
									.then(
											function(response) {
												vm.ledgerList = [];
												vm.ledgerList[index] = {};
												vm.ledgerList[index].ledgerData = response.data.data;
												vm.ledgerList[index].ledgerData;
											}, errorCallback);
						}
					};

					vm.resetDiscount = function(index) {
						console.log("Vishal change");
						vm.additionaldiscount = 0;
						vm.getAdditionDiscount(index);
					}

					vm.getAdditionDiscount = function(index) {
						console.log("Checkingtheend" + vm.sell.schemeOneId);
						if (vm.sell.schemeOneId) {
							console.log("Additional Discount is called.");

							$http
									.post(
											'../../schemeData/getSchemeOneDataByMrp',
											{
												mrp : vm.sell.grossTotalAmount,
												companyId : vm.manageSellDataService.selectedCompanyId
											})
									.then(
											function(response) {

												console.log(response.data.data);
												// console.log(response.data.data[0].totalMrpOnDiscount);
												if (response.data.data[0]){
													vm.additionaldiscount = response.data.data[0].totalMrpOnDiscount;
													
												}
												
												vm.changeAmount(index);
											}, errorCallback);

							// vm.additionaldiscount=10;
						} else {
							vm.changeAmount(index);

						}
					};

					vm.selectFreightLedger = function(index, ledgerObj) {
						if (!ledgerObj) {
							return;
						}

						if (vm.sell.freightList[index]
								&& vm.sell.freightList[index].sellFreightMappingId > 0) {
							vm.sell.freightList[index].sellFreightMappingId = 0;
							return;
						}
						vm.sell.freightList[index].ledger = {};
						vm.sell.freightList[index].ledger = ledgerObj;
						vm.sell.freightList[index].freightCharge = ledgerObj.ledgerCharge;
						vm.sell.freightList[index].taxRate = ledgerObj.taxCode;

						if (vm.billInkDashboardService.noTaxCodeGSTType
								.indexOf(vm.sell.ledgerData.gstType) > -1) {
							vm.sell.freightList[index].taxRate = 0;
						}
					};

					vm.getFreightAmount = function(index) {
						vm.sell.freightList[index].totalItemAmount = 0;
						vm.sell.freightList[index].quantity = 1;
						if (vm.sell.freightList[index].ledger
								&& vm.sell.freightList[index].ledger.ledgerId
								&& vm.sell.freightList[index].quantity
								&& vm.sell.freightList[index].freightCharge) {
							vm.sell.freightList[index].totalItemAmount = vm.sell.freightList[index].freightCharge
									* vm.sell.freightList[index].quantity;

							if (vm.sell.freightList[index].taxRate) {
								vm.sell.freightList[index].totalItemAmount += (vm.sell.freightList[index].totalItemAmount
										* vm.sell.freightList[index].taxRate / 100);
							}

							if (vm.sell.freightList[index].discount) {
								if (vm.sell.freightList[index].measureDiscountInAmount) {
									vm.sell.freightList[index].totalItemAmount -= vm.sell.freightList[index].discount;
								} else {
									vm.sell.freightList[index].totalItemAmount -= (vm.sell.freightList[index].totalItemAmount
											* vm.sell.freightList[index].discount / 100);
								}
							}
						}
						vm.sell.freightList[index].totalItemAmount = vm.sell.freightList[index].totalItemAmount
								.toFixed(2);
						vm.getFreightGrossTotalAmount();
						return vm.sell.freightList[index].totalItemAmount;
					};

					vm.getFreightLevelAmount = function(index) {
						vm.sell.freightList[index].itemAmount = 0;
						vm.sell.freightList[index].quantity = 1;
						if (vm.sell.freightList[index].ledger
								&& vm.sell.freightList[index].ledger.ledgerId
								&& vm.sell.freightList[index].quantity
								&& vm.sell.freightList[index].freightCharge) {
							var itemAmount = vm.sell.freightList[index].freightCharge
									* vm.sell.freightList[index].quantity;

							if (vm.sell.freightList[index].discount) {

								if (vm.sell.freightList[index].measureDiscountInAmount) {
									itemAmount -= vm.sell.freightList[index].discount;
								} else {
									itemAmount -= (itemAmount
											* vm.sell.freightList[index].discount / 100);
								}
							}

							vm.sell.freightList[index].itemAmount = parseFloat(
									itemAmount).toFixed(2);
						}
						return vm.sell.freightList[index].itemAmount;
					};

					vm.getFreightSGSTAmount = function(index) {

						vm.sell.freightList[index].sgst = 0;
						vm.sell.freightList[index].quantity = 1;
						if (vm.calculateSGST) {
							if (vm.sell.freightList[index].ledger
									&& vm.sell.freightList[index].ledger.ledgerId
									&& vm.sell.freightList[index].quantity
									&& vm.sell.freightList[index].freightCharge) {
								var itemAmount = vm.sell.freightList[index].freightCharge
										* vm.sell.freightList[index].quantity;

								if (vm.sell.freightList[index].discount) {

									if (vm.sell.freightList[index].measureDiscountInAmount) {
										itemAmount -= vm.sell.freightList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.sell.freightList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.sell.freightList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.sell.freightList[index].taxRate / 100);
								}
								vm.sell.freightList[index].sgst = taxAmount / 2;
							}
						}
						vm.sell.freightList[index].sgst = parseFloat(
								vm.sell.freightList[index].sgst).toFixed(2);
						return vm.sell.freightList[index].sgst;
					};

					vm.getFreightCGSTAmount = function(index) {

						vm.sell.freightList[index].cgst = 0;
						vm.sell.freightList[index].quantity = 1;
						if (vm.calculateCGST) {
							if (vm.sell.freightList[index].ledger
									&& vm.sell.freightList[index].ledger.ledgerId
									&& vm.sell.freightList[index].quantity
									&& vm.sell.freightList[index].freightCharge) {
								var itemAmount = vm.sell.freightList[index].freightCharge
										* vm.sell.freightList[index].quantity;

								if (vm.sell.freightList[index].discount) {

									if (vm.sell.freightList[index].measureDiscountInAmount) {
										itemAmount -= vm.sell.freightList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.sell.freightList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.sell.freightList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.sell.freightList[index].taxRate / 100);
								}

								vm.sell.freightList[index].cgst = taxAmount / 2;
							}
						}
						vm.sell.freightList[index].cgst = parseFloat(
								vm.sell.freightList[index].cgst).toFixed(2);
						return vm.sell.freightList[index].cgst;
					};

					vm.getFreightIGSTAmount = function(index) {

						vm.sell.freightList[index].igst = 0;
						vm.sell.freightList[index].quantity = 1;
						if (vm.calculateIGST) {
							if (vm.sell.freightList[index].ledger
									&& vm.sell.freightList[index].ledger.ledgerId
									&& vm.sell.freightList[index].quantity
									&& vm.sell.freightList[index].freightCharge) {
								var itemAmount = vm.sell.freightList[index].freightCharge
										* vm.sell.freightList[index].quantity;

								if (vm.sell.freightList[index].discount) {

									if (vm.sell.freightList[index].measureDiscountInAmount) {
										itemAmount -= vm.sell.freightList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.sell.freightList[index].discount / 100);
									}
								}

								var taxAmount = 0;
								if (vm.sell.freightList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.sell.freightList[index].taxRate / 100);
								}

								vm.sell.freightList[index].igst = taxAmount;
							}
						}
						vm.sell.freightList[index].igst = parseFloat(
								vm.sell.freightList[index].igst).toFixed(2);
						return vm.sell.freightList[index].igst;
					};

					vm.getFreightGrossTotalAmount = function() {
						vm.sell.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.sell.sellItemModel.length; counter++) {
							if (vm.sell.sellItemModel[counter].totalItemAmount
									&& vm.sell.sellItemModel[counter].totalItemAmount > 0) {
								vm.sell.grossTotalAmount += parseFloat(vm.sell.sellItemModel[counter].totalItemAmount);
							}
						}
						console.log("11111111" + vm.sell.freightList.length);
						if (vm.sell.freightList.length > 0) {
							for (var count = 0; count < vm.sell.freightList.length; count++) {
								if (vm.sell.freightList[count].totalItemAmount)
									vm.sell.grossTotalAmount += parseFloat(vm.sell.freightList[count].totalItemAmount);
							}
						}

						// if(vm.additionaldiscount === 0 &&
						// vm.sell.grossTotalAmount > 0){
						// //console.log("Vishal121212 is checking");
						// vm.getAdditionDiscount();
						// }

						vm.sell.grossTotalAmount = parseFloat(
								vm.sell.grossTotalAmount).toFixed(2);

						vm.sell.totalBillAmount = parseFloat(vm.sell.grossTotalAmount);
						if (vm.sell.discount && vm.sell.discount > 0) {

							if (vm.sell.purchaseMeasureDiscountInAmount) {
								vm.sell.totalBillAmount -= vm.sell.discount;
							} else {
								vm.sell.totalBillAmount -= (vm.sell.totalBillAmount
										* vm.sell.discount / 100);
							}

						}

						vm.sell.totalBillAmount = parseFloat(
								vm.sell.totalBillAmount).toFixed(2);

						return vm.sell.grossTotalAmount;
					};

					vm.prepareHSNCodeWiseTaxList = function() {
						vm.hsnTaxCodeList = {};
						vm.totalIGST = 0;
						vm.totalSGST = 0;
						vm.totalCGST = 0;

						for ( var i in vm.sell.sellItemModel) {
							var sellItemObj = vm.sell.sellItemModel[i];
							if (sellItemObj.item) {

								sellItemObj.item.hsnCode = sellItemObj.item.hsnCode
										|| "";
								vm.hsnTaxCodeList[sellItemObj.item.hsnCode] = vm.hsnTaxCodeList[sellItemObj.item.hsnCode]
										|| {};
								if (vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate]) {
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].igst += parseFloat(sellItemObj.igst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].cgst += parseFloat(sellItemObj.cgst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].sgst += parseFloat(sellItemObj.sgst);
								} else {
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate] = {};
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].igst = parseFloat(sellItemObj.igst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].cgst = parseFloat(sellItemObj.cgst);
									vm.hsnTaxCodeList[sellItemObj.item.hsnCode][sellItemObj.taxRate].sgst = parseFloat(sellItemObj.sgst);
								}
								vm.totalIGST += parseFloat(sellItemObj.igst);
								vm.totalCGST += parseFloat(sellItemObj.cgst);
								vm.totalSGST += parseFloat(sellItemObj.sgst);
							}
						}

						for ( var i in vm.sell.freightList) {
							var sellFreightObj = vm.sell.freightList[i];
							if (sellFreightObj.ledger) {

								sellFreightObj.ledger.sacCode = sellFreightObj.ledger.sacCode
										|| "";
								vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode] = vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode]
										|| {};
								if (vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate]) {
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].igst += parseFloat(sellFreightObj.igst);
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].cgst += parseFloat(sellFreightObj.cgst);
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].sgst += parseFloat(sellFreightObj.sgst);
								} else {
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate] = {};
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].igst = parseFloat(sellFreightObj.igst);
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].cgst = parseFloat(sellFreightObj.cgst);
									vm.hsnTaxCodeList[sellFreightObj.ledger.sacCode][sellFreightObj.taxRate].sgst = parseFloat(sellFreightObj.sgst);
								}
								vm.totalIGST += parseFloat(sellFreightObj.igst);
								vm.totalCGST += parseFloat(sellFreightObj.cgst);
								vm.totalSGST += parseFloat(sellFreightObj.sgst);
							}
						}

						vm.hsnList = [];
						if (Object.keys(vm.hsnTaxCodeList).length > 0) {
							for ( var hsnCode in vm.hsnTaxCodeList) {
								for ( var taxRate in vm.hsnTaxCodeList[hsnCode]) {
									var hsnTaxCodeObj = {};
									hsnTaxCodeObj.hsnCode = hsnCode;
									hsnTaxCodeObj.taxRate = taxRate;
									hsnTaxCodeObj.igst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].igst)
											.toFixed(2);
									hsnTaxCodeObj.cgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].cgst)
											.toFixed(2);
									hsnTaxCodeObj.sgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].sgst)
											.toFixed(2);
									vm.hsnList.push(hsnTaxCodeObj);
								}
							}
						}
						vm.totalIGST = parseFloat(vm.totalIGST).toFixed(2);
						vm.totalCGST = parseFloat(vm.totalCGST).toFixed(2);
						vm.totalSGST = parseFloat(vm.totalSGST).toFixed(2);
						vm.totalItemLevelAmountWithoutTax = parseFloat(vm.sell.grossTotalAmount)
								- parseFloat(vm.totalSGST)
								- parseFloat(vm.totalCGST)
								- parseFloat(vm.totalIGST);
						vm.totalItemLevelAmountWithoutTax = parseFloat(
								vm.totalItemLevelAmountWithoutTax).toFixed(2);
					};

					vm.printDiv = function() {
						vm.sell.sellPrintDate = moment(vm.sell.sellDateObj)
								.format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second
							var printContent = document
									.getElementById('printSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);
					}

					// 22-04-2019 Invoice Print Function Start
					vm.withOutFreeQtyPrintDiv = function() {
						vm.sell.sellPrintDate = moment(vm.sell.sellDateObj)
								.format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second
									var printContent = document
											.getElementById('printWithOutFeeQtySellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									// WinPrint.document.left(100);
									// WinPrint.document.top(150);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.withOutBatchPrintDiv = function() {
						vm.sell.sellPrintDate = moment(vm.sell.sellDateObj)
								.format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second

									var printContent = document
											.getElementById('printWithOutbatchNoSellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.allFieldsPrintDiv = function() {
						vm.sell.sellPrintDate = moment(vm.sell.sellDateObj)
								.format("DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second

							var printContent = document
									.getElementById('printAllFieldSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							// WinPrint.document.close();
							// WinPrint.focus();
							// WinPrint.print();
							// WinPrint.close();
						}, 1000);

					}
					// 22-04-2019 Invoice Print Function End

					vm.launchPrintData = function() {
					}

					var one = [ "", "one ", "two ", "three ", "four ", "five ",
							"six ", "seven ", "eight ", "nine ", "ten ",
							"eleven ", "twelve ", "thirteen ", "fourteen ",
							"fifteen ", "sixteen ", "seventeen ", "eighteen ",
							"nineteen " ];
					var ten = [ "", "", "twenty ", "thirty ", "forty ",
							"fifty ", "sixty ", "seventy ", "eighty ",
							"ninety " ];
					function numToWords(n, s) {
						var str = "";
						// if n is more than 19, divide it
						if (n > 19) {
							str += ten[parseInt(n / 10)] + one[n % 10];
						} else {
							str += one[n];
						}

						// if n is non-zero
						if (n != 0) {
							str += s;
						}
						return str;
					}

					function convertToWords(n) {
						// stores word representation of given number n
						var out = "";

						// handles digits at ten millions and hundred
						// millions places (if any)
						out += numToWords(parseInt((n / 10000000)), "crore ");

						// handles digits at hundred thousands and one
						// millions places (if any)
						out += numToWords(parseInt(((n / 100000) % 100)),
								"lakh ");

						// handles digits at thousands and tens thousands
						// places (if any)
						out += numToWords(parseInt(((n / 1000) % 100)),
								"thousand ");

						// handles digit at hundreds places (if any)
						out += numToWords(parseInt(((n / 100) % 10)),
								"hundred ");

						if (n > 100 && n % 100 > 0) {
							out += "and ";
						}

						// handles digits at ones and tens places (if any)
						out += numToWords(parseInt((n % 100)), "");

						return out;
					}

					vm.convertToWordsString = function(number) {
						if (number) {
							var sArray = number.split(".");
							var paisePart = "";
							var integer = parseInt(sArray[0]);
							if (sArray.length == 2) {
								var s = parseInt(sArray[1]);
								paisePart = convertToWords(s);
							}
							var words = convertToWords(integer)
									+ (paisePart.length > 0 ? ("and "
											+ paisePart + "Paise") : "")
									+ " Only";
							return words;
						}

					}

				});