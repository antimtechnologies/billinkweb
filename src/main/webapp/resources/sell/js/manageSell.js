angular.module('manageSellData', ['infinite-scroll'])
.service("manageSellDataService", function($http) {
	
	var vm = this;

	vm.sellList = [];
	vm.startOfSell = 0;
	vm.noOfRecordForSell = 12;
	vm.sellFilterData = {};
	vm.hasMoreSellData = true;

	vm.companyCountRecordList = [];
	vm.startOfCompanyCount = 0;
	vm.noOfRecordForCompanyCount = 12;
	vm.hasMoreCompanyCountData = true;

	vm.getSellList = function(companyId) {
		$http.post('../../sellData/getSellListData', {
			companyId: companyId, 
			start: vm.startOfSell, 
			noOfRecord: vm.noOfRecordForSell,
			filterData : vm.sellFilterData
		}).then(successCallback, errorCallback);
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			if (vm.startOfSell === 0) {
				vm.sellList = response.data.data;
				if (vm.sellList.length < vm.noOfRecordForSell) {
					vm.hasMoreSellData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForSell) {				
				vm.hasMoreSellData = false;
			}

			vm.sellList = vm.sellList.concat(response.data.data);
		}
	}

	function errorCallback() {
			
	}

	vm.getCompanyCountData = function() {
		$http.post('../../companydata/getCompanyRequestCountDetails', {start: vm.startOfCompanyCount, noOfRecord: vm.noOfRecordForCompanyCount, sortParam: 'sellPendingCount' }).
		then(companyCountSuccessCallBack, errorCallback);
	};

	function companyCountSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompanyCount === 0) {
				vm.companyCountRecordList = response.data.data;
				if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
					vm.hasMoreCompanyCountData = false;
				}
				return;
			}

			if (response.data.data.length < vm.noOfRecordForCompanyCount) {				
				vm.hasMoreCompanyCountData = false;
			}

			vm.companyCountRecordList = vm.companyCountRecordList.concat(response.data.data);
		}
	}

})
.controller('manageSellDataController', function($http, $ocLazyLoad, manageSellDataService, billInkDashboardService) {
	var vm = this;
	vm.backToCompanyWiseSellData = function() {
			vm.billInkDashboardService.currentViewUrl = "../sell/view/companyWiseSellCount.html";
			vm.manageSellDataService.getCompanyCountData();
	};

	vm.manageSellDataService = manageSellDataService;
	vm.billInkDashboardService = billInkDashboardService;

	if (vm.billInkDashboardService.userRole !== "Vendor") {
		vm.statusList = ['New', 'Modified', 'Deleted'];
	} else {		
		vm.statusList = ['New', 'Modified', 'Deleted', 'UnPublished'];
	}

	vm.manageSellDataService.sellFilterData = {};
	vm.manageSellDataService.sellFilterData.statusList = [];

	vm.openSellList = function(companyObj) {

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageSellDataService.sellFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageSellDataService.sellFilterData.isVerified = 0;
		}

		vm.billInkDashboardService.currentCompanyObj = angular.copy(companyObj);
		vm.manageSellDataService.selectedCompanyId = companyObj.companyId;
		vm.manageSellDataService.startOfSell = 0;
		vm.billInkDashboardService.currentViewUrl = "../sell/view/viewSellList.html";
		vm.manageSellDataService.getSellList(vm.manageSellDataService.selectedCompanyId);
	};

	vm.init = function() {
		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.billInkDashboardService.currentViewUrl = "../sell/view/companyWiseSellCount.html";
			vm.manageSellDataService.getCompanyCountData();
		} else {			
			vm.openSellList(vm.billInkDashboardService.selectedCompany);
		}
	};

	vm.loadMoreSell = function() {
		if (vm.manageSellDataService.hasMoreSellData) {
			vm.manageSellDataService.startOfSell +=  vm.manageSellDataService.noOfRecordForSell;
			vm.manageSellDataService.getSellList(vm.manageSellDataService.selectedCompanyId);
		}
	};

	vm.loadMoreCompanyCountData = function() {
		if (vm.manageSellDataService.hasMoreCompanyCountData) {
			vm.manageSellDataService.startOfCompanyCount +=  vm.manageSellDataService.noOfRecordForSell;
			vm.manageSellDataService.getCompanyCountData();
		}
	};

	vm.showAddSellDataForm = function(requestFor) {
		vm.manageSellDataService.requestType = "Add";
		vm.manageSellDataService.requestFor = requestFor;
		vm.manageSellDataService.startOfSell = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../sell/js/addEditSellDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../sell/view/addEditSellDetail.html";
		});
	};

	vm.showAddRetailDataForm = function(requestFor) {
		vm.manageSellDataService.requestType = "Add";
		vm.manageSellDataService.requestFor = requestFor;
		vm.manageSellDataService.startOfSell = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../sell/js/addEditRetailDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../sell/view/addEditRetailDetail.html";
		});
	};
	
	
	vm.showDeletePrompt = function(sellId, statusName) {
		vm.selectedDeleteSellId = sellId;
		vm.selectedDeleteSellStatus = statusName;
		$("#sell-delelete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteSavedSellData = function() {
		$http.post('../../sellData/deleteSavedSellData', {
			sellId: vm.selectedDeleteSellId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageSellDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	}

	vm.deletePublishSellData = function() {
		$http.post('../../sellData/deletePublishedSellData', {
			sellId: vm.selectedDeleteSellId, 
			loggedInUserId: vm.billInkDashboardService.userId,
			companyId: vm.manageSellDataService.selectedCompanyId
		}).then(deleteUserSuccessCallback, errorCallback);
	};

	vm.deleteSellData = function() {
		if (vm.selectedDeleteSellStatus === "UnPublished") {
			vm.deleteSavedSellData();
		} else {
			vm.deletePublishSellData();
		}
	};

	function deleteUserSuccessCallback() {
		vm.closeDeleteModal();
		vm.init();
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteSellId = 0;
		vm.selectedDeleteSellStatus = "";
		$("#sell-delelete-confirmation-dialog .modal").modal('hide');
	};

	function errorCallback() {
	}

	vm.editSellData = function(sellId, imageURL) {
		vm.manageSellDataService.requestType = "Edit";
		vm.manageSellDataService.requestFor = imageURL && imageURL !== "" ? "uploadImage" : "enterManually";
		vm.manageSellDataService.editSellId = sellId;
		vm.manageSellDataService.startOfSell = 0;
		$ocLazyLoad
		.load(["../customdirective/fileUploadDirective.js", "../sell/js/addEditSellDetail.js"])
		.then(function () {
			vm.billInkDashboardService.currentViewUrl = "../sell/view/addEditSellDetail.html";
		});
	};


	// filter related function started.
	vm.launchFilter = function() {
		$("#sell-filter-dialog .modal").modal('show');
	};

	vm.closeFilterModal = function() {
		$("#sell-filter-dialog .modal").modal('hide');
	};

	vm.applyFilter = function() {
		vm.searchFilterData();
		$("#sell-filter-dialog .modal").modal('hide');
	};

	vm.searchFilterData = function() {
		vm.manageSellDataService.startOfSell = 0;
		vm.manageSellDataService.getSellList(vm.manageSellDataService.selectedCompanyId);
	};

	vm.resetFilter = function() {
		vm.manageSellDataService.sellFilterData = {};
		vm.manageSellDataService.sellFilterData.statusList = [];

		if (vm.billInkDashboardService.userRole !== "Vendor") {
			vm.manageSellDataService.sellFilterData.statusList = ['New', 'Modified', 'Deleted'];
			vm.manageSellDataService.sellFilterData.isVerified = 0;
		}

		vm.manageSellDataService.startOfSell = 0;
		vm.manageSellDataService.getSellList(vm.manageSellDataService.selectedCompanyId);
		$("#sell-filter-dialog .modal").modal('hide');
	};

	vm.toggleSelection = function toggleSelection(status) {
		var idx = vm.manageSellDataService.sellFilterData.statusList.indexOf(status);
	
		if (idx > -1) {
			vm.manageSellDataService.sellFilterData.statusList.splice(idx, 1);
		} else {
			vm.manageSellDataService.sellFilterData.statusList.push(status);
		}
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageSellDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.manageSellDataService.sellFilterData.ledgerId = 0;
			vm.searchLedgerList = [];
		}
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.manageSellDataService.sellFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		//$("#add-buyer-section-model .modal").modal('show');
	};
	vm.selectLedgerForRetail = function(ledgerObj) {
		vm.manageSellDataService.sellFilterData.ledgerId = ledgerObj.ledgerId; 
		vm.billInkDashboardService.buyer.ledgerId = ledgerObj.ledgerId; 
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		$http.post('../../stateData/getStateList', {}).then(vm.setStateList,
				vm.errorFetchList);
		$("#add-buyer-section-model .modal").modal('show');
	};
	
	vm.setStateList = function(response) {
		vm.stateList = response.data.data;
	};
	vm.errorFetchList = function(response) {

	};
	// filter related function end.

	vm.$onDestroy = function() {
		vm.manageSellDataService.sellList = [];
		vm.manageSellDataService.startOfSell = 0;
		vm.manageSellDataService.hasMoreSellData = true;

		vm.manageSellDataService.companyCountRecordList = [];
		vm.manageSellDataService.startOfCompanyCount = 0;
		vm.manageSellDataService.hasMoreCompanyCountData = true;

		vm.manageSellDataService.selectedCompanyId = 0;
	}
});
