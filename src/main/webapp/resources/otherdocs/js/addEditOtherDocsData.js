angular.module('addOtherDocsData', ['fileUploadDirective', 'toaster'])
.controller('addOtherDocsDataController', function($http, fileUploadService, toaster, manageOtherDocsDataService, billInkDashboardService) {
	var vm = this;
	vm.manageOtherDocsDataService = manageOtherDocsDataService;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.otherDocs = {};
	vm.otherDocs.otherDocImageURLList = [];
	vm.billInkDashboardService = billInkDashboardService;

	vm.init = function() {
		vm.otherDocs.companyId = vm.manageOtherDocsDataService.selectedCompanyId;
		if (vm.manageOtherDocsDataService.requestType === "Edit") {
			$http.post('../../otherDocsData/getOtherDocsDetailsById', { otherDocsId: vm.manageOtherDocsDataService.editOtherDocsId, companyId : vm.manageOtherDocsDataService.selectedCompanyId})
			.then(vm.setOtherDocsDetail, vm.errorFetchData);			
		} else {
			vm.otherDocs.addedBy = {};
			vm.otherDocs.addedBy.userId = vm.billInkDashboardService.userId;
		}
	};

	vm.setOtherDocsDetail = function(response) {
		vm.otherDocs = angular.copy(response.data.data);
	};

	vm.errorFetchData = function() {
	};

	vm.closeAddEditOtherDocsScreen = function() {
		vm.manageOtherDocsDataService.getCompanyOtherDocsList(vm.manageOtherDocsDataService.selectedCompanyId);
		vm.manageOtherDocsDataService.otherDocsContainerURL = "../otherdocs/view/viewOtherDocsList.html";		
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			vm.closeAddEditOtherDocsScreen();
		} else if (response.data.status === "error") {
			alert(response.data.errMsg);
		}
	}

	function errorCallback(response) {
		alert(response.data.errMsg);
	}

	vm.saveOtherDocsData = function() {
		vm.otherDocs.companyId = vm.manageOtherDocsDataService.selectedCompanyId;
		$http.post('../../otherDocsData/saveOtherDocsData', vm.otherDocs).then(successCallback, errorCallback);
	};

	vm.uploadFileAndSaveOtherDocsData = function (action) {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'otherDocs').then(function(result){
				if (result.data.status === "success") {				
					vm.otherDocs.otherDocImageURLList = vm.otherDocs.otherDocImageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.otherDocs.otherDocImageURLList && vm.otherDocs.otherDocImageURLList.length > 0) {						
						vm.otherDocs.imageURL = vm.otherDocs.otherDocImageURLList[0].imageURL;
					}
					vm.saveOtherDocsData();						
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.otherDocs.imageURL = null;
			if (vm.otherDocs.otherDocImageURLList && vm.otherDocs.otherDocImageURLList.length > 0) {						
				vm.otherDocs.imageURL = vm.otherDocs.otherDocImageURLList[0].imageURL;
			}
			vm.saveOtherDocsData();						
		}
	};

	vm.updateOtherDocsData = function() {
		vm.otherDocs.companyId = vm.manageOtherDocsDataService.selectedCompanyId;
		$http.post('../../otherDocsData/updateOtherDocsData', vm.otherDocs).then(successCallback, errorCallback);
	};

	vm.uploadFileAndUpdateOtherDocsData = function(action) {
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'otherDocs').then(function(result){
				if (result.data.status === "success") {
					vm.otherDocs.otherDocImageURLList = vm.otherDocs.otherDocImageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.otherDocs.otherDocImageURLList && vm.otherDocs.otherDocImageURLList.length > 0) {						
						vm.otherDocs.imageURL = vm.otherDocs.otherDocImageURLList[0].imageURL;
					}
					vm.updateOtherDocsData();						
				}
			}, function(error) {
				alert('error');
			});
		} else {
			vm.otherDocs.imageURL = null;
			if (vm.otherDocs.otherDocImageURLList && vm.otherDocs.otherDocImageURLList.length > 0) {						
				vm.otherDocs.imageURL = vm.otherDocs.otherDocImageURLList[0].imageURL;
			}
			vm.updateOtherDocsData();						
		}
	};

	function getImageURLListObj(otherDocImageURLList) {
		var otherDocsImageURLList = [];
		var otherDocsImageURLObj = {};

		for (var count = 0; count < otherDocImageURLList.length; count++) {
			otherDocsImageURLObj = {};
			otherDocsImageURLObj.imageURL = otherDocImageURLList[count];
			otherDocsImageURLList.push(otherDocsImageURLObj);
		}

		return otherDocsImageURLList;
	}

	vm.markOtherDocsAsVerified = function() {
		vm.otherDocs.verifiedBy = {};
		vm.otherDocs.verifiedBy.userId = vm.billInkDashboardService.userId;
		vm.otherDocs.companyId = vm.manageOtherDocsDataService.selectedCompanyId;
		$http.post('../../otherDocsData/markOtherDocsAsVerified', vm.otherDocs).then(successCallback, errorCallback);
	};

	vm.publishSavedOtherDocs = function() {
		vm.otherDocs.companyId = vm.manageOtherDocsDataService.selectedCompanyId;
		$http.post('../../otherDocsData/publishSavedOtherDocs', vm.otherDocs).then(successCallback, errorCallback);
	};

	vm.searchLedger = function() {
		if (vm.searchLedgerNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageOtherDocsDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.searchLedgerList = [];
			vm.otherDocs.ledgerData = {};
		}
	};
	
	vm.searchOtherDocs = function() {
		if (vm.searchOtherDocsNameText.length > 2) {			
			$http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchOtherDocsNameText, companyId: vm.manageOtherDocsDataService.selectedCompanyId })
			.then(getOtherDocsDataSuccessCallBack, errorCallback);
		} else {
			vm.searchOtherDocsList = [];
			vm.otherDocs.otherDocsData = {};
		}
	};

	function getOtherDocsDataSuccessCallBack(response) {
		vm.otherDocs.otherDocsData = {};
		vm.searchOtherDocsList = response.data.data;
	}

	vm.selectOtherDocs = function(ledgerObj) {
		vm.searchOtherDocsNameText = ledgerObj.ledgerName;
		vm.otherDocs.otherDocsData = {};
		vm.otherDocs.otherDocsData.ledgerId = ledgerObj.ledgerId;
	};

	function getLedgerDataSuccessCallBack(response) {
		vm.otherDocs.ledgerData = {};
		vm.searchLedgerList = response.data.data;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		vm.otherDocs.ledgerData = {};
		vm.otherDocs.ledgerData.ledgerId = ledgerObj.ledgerId;
	};

	vm.openImageModel = function(index) {
		if (index < 0 || index > (vm.otherDocs.otherDocImageURLList - 1)) {
			return;
		}

		vm.otherDocsImageIndex = index;
		$("#otherDocs-image-show-model .modal").modal('show');
		vm.selectedPhotoURL = vm.otherDocs.otherDocImageURLList[index].imageURL;
	};

	vm.fetchImage = function(position) {
		vm.otherDocsImageIndex += position;

		if (vm.otherDocsImageIndex < 0) {
			vm.otherDocsImageIndex = vm.otherDocs.otherDocImageURLList.length - 1;
		} else if (vm.otherDocsImageIndex >= vm.otherDocs.otherDocImageURLList.length) {
			vm.otherDocsImageIndex = 0;
		}
		vm.selectedPhotoURL = vm.otherDocs.otherDocImageURLList[vm.otherDocsImageIndex].imageURL;
	};

	vm.deleteImage = function(index) {
		vm.otherDocs.otherDocImageURLList.splice(index, 1);
	};

});