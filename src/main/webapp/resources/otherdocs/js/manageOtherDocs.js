var manageOtherDocsDataApp = angular.module('manageOtherDocs', ['infiniteScroll', 'infinite-scroll'])

manageOtherDocsDataApp.service('manageOtherDocsDataService', function($http){
	var svc 					= this;
	svc.selectedCompanyId 		= 0;
	svc.startOfOtherDocs			= 0;
	svc.noOfRecordForOtherDocs		= 12;
	svc.totalOtherDocsCount 		= 0;

	svc.getCompanyOtherDocsList = function() {
		$http.post('../../otherDocsData/getOtherDocsListData', {companyId: svc.selectedCompanyId, start: svc.startOfOtherDocs, noOfRecord: svc.noOfRecordForOtherDocs }).
		then(svc.successCallback, svc.errorCallback);
	};

	svc.successCallback = function(response) {
		if (response.data.status === "success") {
			if (svc.startOfOtherDocs === 0) {
				svc.totalOtherDocsCount = response.data.totalCount;
				svc.otherDocsList = response.data.data;
				return;
			}
			svc.otherDocsList = svc.otherDocsList.concat(response.data.data);
		}
	};

	svc.errorCallback = function() {
		svc.otherDocsList = [];
	};

});

manageOtherDocsDataApp.controller('manageOtherDocsController', function($http, $ocLazyLoad, manageOtherDocsDataService, billInkDashboardService) {
	var vm 							= this;
	vm.startOfCompany 				= 0;
	const noOfRecordForCompany 		= 30;
	vm.manageOtherDocsDataService		= manageOtherDocsDataService;
	vm.manageOtherDocsDataService.otherDocsContainerURL = "../otherdocs/view/viewOtherDocsList.html";
	vm.billInkDashboardService		= billInkDashboardService; 
	vm.init = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyListOnSearch = function() {
		vm.startOfCompany = 0;
		vm.getCompanyList();
	};

	vm.getCompanyList = function() {
		$http.post('../../companydata/getCompanyNameIdList', {start: vm.startOfCompany, noOfRecord: noOfRecordForCompany, companyName: vm.searchCompanyNameText })
		.then(getCompanyDataSuccessCallBack, errorCallback);
	};

	vm.loadMoreCompanyList = function() {
		if (vm.totalCount > vm.startOfCompany) {			
			vm.startOfCompany += noOfRecordForCompany;
			vm.getCompanyList();
		}
	};

	vm.loadMoreOtherDocs = function() {
		if (vm.manageOtherDocsDataService.totalOtherDocsCount > vm.manageOtherDocsDataService.startOfOtherDocs) {			
			vm.manageOtherDocsDataService.startOfOtherDocs += vm.manageOtherDocsDataService.noOfRecordForOtherDocs;
			vm.manageOtherDocsDataService.getCompanyOtherDocsList();
		}
	};

	vm.getCompayOtherDocsDetail = function(companyId) {
		vm.manageOtherDocsDataService.selectedCompanyId = companyId;
		vm.manageOtherDocsDataService.startOfOtherDocs = 0;
		vm.manageOtherDocsDataService.getCompanyOtherDocsList();
		vm.manageOtherDocsDataService.otherDocsContainerURL = "../otherdocs/view/viewOtherDocsList.html";
	}

	function getCompanyDataSuccessCallBack(response) {
		if (response.data.status === "success") {
			if (vm.startOfCompany === 0) {
				vm.companyList = response.data.data;
				vm.totalCount = response.data.totalCount;
				if (vm.companyList.length > 0) {
					vm.manageOtherDocsDataService.selectedCompanyId = vm.companyList[0].companyId;
					vm.manageOtherDocsDataService.startOfOtherDocs = 0;
					vm.manageOtherDocsDataService.getCompanyOtherDocsList();
				}
				return;
			}
			vm.companyList = vm.companyList.concat(response.data.data);
		}
	}

	function errorCallback() {
		
	}

	vm.addOtherDocsData = function() {
		vm.manageOtherDocsDataService.requestType = "Add";
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../otherdocs/js/addEditOtherDocsData.js"],
				{serie: true}		
		)
		.then(function () {
			vm.manageOtherDocsDataService.otherDocsContainerURL = "../otherdocs/view/addEditOtherDocsData.html";
		});
	};

	vm.showDeletePrompt = function(otherDocsId) {
		vm.selectedDeleteOtherDocsId = otherDocsId;
		$("#otherDocs-delete-confirmation-dialog .modal").modal('show');
	};

	vm.deleteOtherDocsData = function() {
		$http.post('../../otherDocsData/deleteOtherDocsData', {otherDocsId: vm.selectedDeleteOtherDocsId, companyId : vm.manageOtherDocsDataService.selectedCompanyId, loggedInUserId: vm.billInkDashboardService.userId }).
		then(deleteOtherDocsSuccessCallback, errorCallback);	
	};

	function deleteOtherDocsSuccessCallback() {
		vm.closeDeleteModal();
		vm.getCompayOtherDocsDetail(vm.manageOtherDocsDataService.selectedCompanyId);
	}

	vm.closeDeleteModal = function() {
		vm.selectedDeleteItemId = 0;
		$("#otherDocs-delete-confirmation-dialog .modal").modal('hide');
	};

	vm.editOtherDocsData = function(otherDocsId) {
		vm.manageOtherDocsDataService.requestType = "Edit";
		vm.manageOtherDocsDataService.editOtherDocsId = otherDocsId;
		$ocLazyLoad
		.load(
				["../customdirective/fileUploadDirective.js", "../otherdocs/js/addEditOtherDocsData.js"],
				{serie: true}
		)
		.then(function () {
			vm.manageOtherDocsDataService.otherDocsContainerURL = "../otherdocs/view/addEditOtherDocsData.html";
		});
	};

});

