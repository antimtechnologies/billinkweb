angular
		.module('manageSellPaymentData', [ 'infinite-scroll' ])
		.service(
				"manageSellPaymentDataService",
				function($http) {

					var vm = this;

					vm.sellPaymentList = [];
					vm.startOfSellPayment = 0;
					vm.noOfRecordForSellPayment = 12;
					vm.sellPaymentFilterData = {};
					vm.hasMoreSellPaymentData = true;

					vm.companyCountRecordList = [];
					vm.startOfCompanyCount = 0;
					vm.noOfRecordForCompanyCount = 12;
					vm.hasMoreCompanyCountData = true;

					vm.getSellPaymentList = function(companyId) {
						$http.post(
								'../../sellPaymentData/getSellPaymentListData',
								{
									companyId : companyId,
									start : vm.startOfSellPayment,
									noOfRecord : vm.noOfRecordForSellPayment,
									filterData : vm.sellPaymentFilterData
								}).then(successCallback, errorCallback);
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							if (vm.startOfSellPayment === 0) {
								vm.sellPaymentList = response.data.data;
								if (vm.sellPaymentList.length < vm.noOfRecordForSellPayment) {
									vm.hasMoreSellPaymentData = false;
								}
								return;
							}

							if (response.data.data.length < vm.noOfRecordForSellPayment) {
								vm.hasMoreSellPaymentData = false;
							}

							vm.sellPaymentList = vm.sellPaymentList
									.concat(response.data.data);
						}
					}

					function errorCallback() {

					}

					vm.getCompanyCountData = function() {
						$http
								.post(
										'../../companydata/getCompanyRequestCountDetails',
										{
											start : vm.startOfCompanyCount,
											noOfRecord : vm.noOfRecordForCompanyCount,
											sortParam : 'sellPaymentPendingCount'
										}).then(companyCountSuccessCallBack,
										errorCallback);
					};

					function companyCountSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompanyCount === 0) {
								vm.companyCountRecordList = response.data.data;
								if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
									vm.hasMoreCompanyCountData = false;
								}
								return;
							}

							if (response.data.data.length < vm.noOfRecordForCompanyCount) {
								vm.hasMoreCompanyCountData = false;
							}

							vm.companyCountRecordList = vm.companyCountRecordList
									.concat(response.data.data);
						}
					}

				})
		.controller(
				'manageSellPaymentDataController',
				function($http, $ocLazyLoad, manageSellPaymentDataService,
						billInkDashboardService) {
					var vm = this;
					vm.manageSellPaymentDataService = manageSellPaymentDataService;
					vm.billInkDashboardService = billInkDashboardService;
					if (vm.billInkDashboardService.userRole !== "Vendor") {
						vm.statusList = [ 'New', 'Modified', 'Deleted' ];
					} else {
						vm.statusList = [ 'New', 'Modified', 'Deleted',
								'UnPublished' ];
					}

					vm.manageSellPaymentDataService.sellPaymentFilterData = {};
					vm.manageSellPaymentDataService.sellPaymentFilterData.statusList = [];

					vm.openSellPaymentList = function(companyObj) {

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.manageSellPaymentDataService.sellPaymentFilterData.statusList = [
									'New', 'Modified', 'Deleted' ];
							vm.manageSellPaymentDataService.sellPaymentFilterData.isVerified = 0;
						}

						vm.billInkDashboardService.currentCompanyObj = companyObj;
						vm.manageSellPaymentDataService.selectedCompanyId = companyObj.companyId;
						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/viewSellPaymentList.html";
						vm.manageSellPaymentDataService
								.getSellPaymentList(vm.manageSellPaymentDataService.selectedCompanyId);
					};

					vm.backToCompanyWiseSellPaymentData = function() {
						vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/companyWiseSellPaymentCount.html";
						vm.manageSellPaymentDataService.getCompanyCountData();
					};

					vm.init = function() {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/companyWiseSellPaymentCount.html";
							vm.manageSellPaymentDataService
									.getCompanyCountData();
						} else {
							vm
									.openSellPaymentList(vm.billInkDashboardService.selectedCompany);
						}
					};

					vm.loadMoreSellPayment = function() {
						if (vm.manageSellPaymentDataService.hasMoreSellPaymentData) {
							vm.manageSellPaymentDataService.startOfSellPayment += vm.manageSellPaymentDataService.noOfRecordForSellPayment;
							vm.manageSellPaymentDataService
									.getSellPaymentList(vm.manageSellPaymentDataService.selectedCompanyId);
						}
					};

					vm.loadMoreCompanyCountData = function() {
						if (vm.manageSellPaymentDataService.hasMoreCompanyCountData) {
							vm.manageSellPaymentDataService.startOfCompanyCount += vm.manageSellPaymentDataService.noOfRecordForSellPayment;
							vm.manageSellPaymentDataService
									.getCompanyCountData();
						}
					};

					vm.showAddSellPaymentDataForm = function(requestFor) {
						vm.manageSellPaymentDataService.requestType = "Add";
						vm.manageSellPaymentDataService.requestFor = requestFor;
						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../sellpayment/js/addEditSellPaymentDetail.js" ])
								.then(
										function() {
											vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/addEditSellPaymentDetail.html";
										});
					};

					vm.showDeletePrompt = function(sellPaymentId, statusName) {
						vm.selectedDeleteSellPaymentId = sellPaymentId;
						vm.selectedDeleteSellPaymentStatus = statusName;
						$("#sellPayment-delelete-confirmation-dialog .modal")
								.modal('show');
					};

					vm.deleteSavedSellPaymentData = function() {
						$http
								.post(
										'../../sellPaymentData/deleteSavedSellPaymentData',
										{
											sellPaymentId : vm.selectedDeleteSellPaymentId,
											loggedInUserId : vm.billInkDashboardService.userId,
											companyId : vm.manageSellPaymentDataService.selectedCompanyId
										}).then(deleteUserSuccessCallback,
										errorCallback);
					}

					vm.deletePublishSellPaymentData = function() {
						$http
								.post(
										'../../sellPaymentData/deletePublishedSellPaymentData',
										{
											sellPaymentId : vm.selectedDeleteSellPaymentId,
											loggedInUserId : vm.billInkDashboardService.userId,
											companyId : vm.manageSellPaymentDataService.selectedCompanyId
										}).then(deleteUserSuccessCallback,
										errorCallback);
					};

					vm.deleteSellPaymentData = function() {
						if (vm.selectedDeleteSellPaymentStatus === "UnPublished") {
							vm.deleteSavedSellPaymentData();
						} else {
							vm.deletePublishSellPaymentData();
						}
					};

					function deleteUserSuccessCallback() {
						vm.closeDeleteModal();
						vm.init();
					}

					vm.closeDeleteModal = function() {
						vm.selectedDeleteSellPaymentId = 0;
						vm.selectedDeleteSellPaymentStatus = "";
						$("#sellPayment-delelete-confirmation-dialog .modal")
								.modal('hide');
					};

					function errorCallback() {
					}

					vm.editSellPaymentData = function(sellPaymentId, imageURL) {
						vm.manageSellPaymentDataService.requestType = "Edit";
						vm.manageSellPaymentDataService.requestFor = imageURL
								&& imageURL !== "" ? "uploadImage"
								: "enterManually";
						vm.manageSellPaymentDataService.editSellPaymentId = sellPaymentId;
						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../sellpayment/js/addEditSellPaymentDetail.js" ])
								.then(
										function() {
											vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/addEditSellPaymentDetail.html";
										});
					};

					// filter related function start
					vm.launchFilter = function() {
						$("#sellPayment-filter-dialog .modal").modal('show');
					};

					vm.closeFilterModal = function() {
						$("#sellPayment-filter-dialog .modal").modal('hide');
					};

					vm.applyFilter = function() {
						vm.searchFilterData();
						$("#sellPayment-filter-dialog .modal").modal('hide');
					};

					vm.searchFilterData = function() {
						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						vm.manageSellPaymentDataService
								.getSellPaymentList(vm.manageSellPaymentDataService.selectedCompanyId);
					};

					vm.resetFilter = function() {
						vm.manageSellPaymentDataService.sellPaymentFilterData = {};
						vm.manageSellPaymentDataService.sellPaymentFilterData.statusList = [];

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.manageSellPaymentDataService.sellPaymentFilterData.statusList = [
									'New', 'Modified', 'Deleted' ];
							vm.manageSellPaymentDataService.sellPaymentFilterData.isVerified = 0;
						}

						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						vm.manageSellPaymentDataService
								.getSellPaymentList(vm.manageSellPaymentDataService.selectedCompanyId);
						$("#sellPayment-filter-dialog .modal").modal('hide');
					};

					vm.toggleSelection = function toggleSelection(status) {
						var idx = vm.manageSellPaymentDataService.sellPaymentFilterData.statusList
								.indexOf(status);

						if (idx > -1) {
							vm.manageSellPaymentDataService.sellPaymentFilterData.statusList
									.splice(idx, 1);
						} else {
							vm.manageSellPaymentDataService.sellPaymentFilterData.statusList
									.push(status);
						}
					};

					vm.searchLedger = function() {
						if (vm.searchLedgerNameText.length > 2) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchLedgerNameText,
												companyId : vm.manageSellPaymentDataService.selectedCompanyId
											}).then(
											getLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.manageSellPaymentDataService.sellPaymentFilterData.ledgerId = 0;
							vm.searchLedgerList = [];
						}
					};

					function getLedgerDataSuccessCallBack(response) {
						vm.searchLedgerList = response.data.data;
					}

					vm.selectLedger = function(ledgerObj) {
						vm.manageSellPaymentDataService.sellPaymentFilterData.ledgerId = ledgerObj.ledgerId;
						vm.searchLedgerNameText = ledgerObj.ledgerName;
					};
					// filter related function end

					vm.$onDestroy = function() {
						vm.manageSellPaymentDataService.sellPaymentList = [];
						vm.manageSellPaymentDataService.startOfSellPayment = 0;
						vm.manageSellPaymentDataService.hasMoreSellPaymentData = true;

						vm.manageSellPaymentDataService.companyCountRecordList = [];
						vm.manageSellPaymentDataService.startOfCompanyCount = 0;
						vm.manageSellPaymentDataService.hasMoreCompanyCountData = true;

						vm.manageSellPaymentDataService.selectedCompanyId = 0;
					}
				});
