angular.module('addSellPaymentData', ['fileUploadDirective', 'toaster'])
.controller('addSellPaymentDataController', function($http, fileUploadService, toaster, manageSellPaymentDataService, billInkDashboardService) {
	var vm = this;
	vm.manageSellPaymentDataService = manageSellPaymentDataService;
	vm.profilePhotoFile = null;
	vm.stateList = [];
	vm.sellPayment = {};
	vm.sellPayment.sellList = [];
	vm.billInkDashboardService = billInkDashboardService;
	vm.taxCodeList = [0, 0.5, 3, 5, 12, 18, 28];
	vm.selectedBill = {};
	vm.sellPayment.imageURLList = [];
	vm.init = function() {
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		if (vm.manageSellPaymentDataService.requestType === "Edit") {
			$http.post('../../sellPaymentData/getSellPaymentDetailsById', { sellPaymentId: vm.manageSellPaymentDataService.editSellPaymentId, companyId : vm.manageSellPaymentDataService.selectedCompanyId})
			.then(vm.setSellPaymentDetail, vm.errorFetchData);			
		} else {
			vm.sellPayment.addedBy = {};
			vm.sellPayment.addedBy.userId = vm.billInkDashboardService.userId;
		}
	};

	vm.openImageModel = function(photoURL) {
		$("#sellPayment-image-show-model .modal").modal('show');
		vm.selectedSellPaymentPhotoURL = photoURL;
	};

	vm.setSellPaymentDetail = function(response) {
		
		if (response.data.status === "success") {
			vm.sellPayment = angular.copy(response.data.data);
			vm.searchLedgerNameText = vm.sellPayment.ledgerData.ledgerName;
			vm.searchReceiptInLedgerNameText = vm.sellPayment.receiptInLedger.ledgerName;
			if (!(vm.sellPayment.receiptInLedger && vm.sellPayment.receiptInLedger.ledgerId > 0)) {
				vm.sellPayment.receiptInLedger = null;
			}
			vm.sellPayment.sellPaymentDateObj = new Date(vm.sellPayment.sellPaymentDate);
			vm.sellPayment.modifiedBy = {};
			vm.sellPayment.modifiedBy.userId = vm.billInkDashboardService.userId;
			vm.sellPayment.paymentDateObj = new Date(vm.sellPayment.paymentDate);
		} else if (response.data.status === "error") {
			toaster.showMessage('error', '', response.data.errMsg);
		}

	};

	vm.errorFetchData = function() {
	};

	vm.closeAddEditSellPaymentScreen = function() {
		vm.manageSellPaymentDataService.getSellPaymentList(vm.manageSellPaymentDataService.selectedCompanyId);
		vm.billInkDashboardService.currentViewUrl = "../sellpayment/view/viewSellPaymentList.html";		
	};

	function successCallback(response) {
		if (response.data.status === "success") {
			vm.closeAddEditSellPaymentScreen();
		} else if (response.data.status === "error") {
			toaster.showMessage('error', '', response.data.errMsg);
		}
	}

	function errorCallback(response) {
		toaster.showMessage('error', '', response.data.errMsg);
	}

	function validatesellPaymentSaveData() {		

		if (!vm.sellPayment.ledgerData || !vm.sellPayment.ledgerData.ledgerId || vm.sellPayment.ledgerData.ledgerId < 1) {
			toaster.showMessage('error', '', 'kindly select ledger details.');
			return false; 
		}

		if (!vm.sellPayment.receiptInLedger || !vm.sellPayment.receiptInLedger.ledgerId || vm.sellPayment.receiptInLedger.ledgerId < 1) {
			toaster.showMessage('error', '', 'kindly select receipt in ledger.');
			return false;
		}
		
		return true;
	}

	vm.saveSellPaymentData = function() {
		vm.sellPayment.paymentDate = moment(vm.sellPayment.paymentDateObj).format("YYYY-MM-DD");
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		vm.sellPayment.sellPaymentDate = moment(vm.sellPayment.sellPaymentDateObj).format("YYYY-MM-DD");
		$http.post('../../sellPaymentData/saveSellPaymentData', vm.sellPayment).then(successCallback, errorCallback);
	};

	vm.publishSellPaymentData = function() {
		vm.sellPayment.paymentDate = moment(vm.sellPayment.paymentDateObj).format("YYYY-MM-DD");
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId
		vm.sellPayment.sellPaymentDate = moment(vm.sellPayment.sellPaymentDateObj).format("YYYY-MM-DD");
		$http.post('../../sellPaymentData/publishSellPaymentData', vm.sellPayment).then(successCallback, errorCallback);		
	};

	vm.uploadFileAndSaveSellPaymentData = function (action) {

		if (!validatesellPaymentSaveData()) {
			return false;
		}
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'sellPayment').then(function(result){
				if (result.data.status === "success") {				
					vm.sellPayment.imageURLList = vm.sellPayment.imageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.sellPayment.imageURLList && vm.sellPayment.imageURLList.length > 0) {						
						vm.sellPayment.imageURL = vm.sellPayment.imageURLList[0].imageURL;
					}
					if (action === "Save") {
						vm.saveSellPaymentData();						
					} else if (action === "Publish") {						
						vm.publishSellPaymentData();
					}
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading files');
			});
		} else {
			vm.sellPayment.imageURL = null;
			if (vm.sellPayment.imageURLList && vm.sellPayment.imageURLList.length > 0) {						
				vm.sellPayment.imageURL = vm.sellPayment.imageURLList[0].imageURL;
			}
			if (action === "Save") {
				vm.saveSellPaymentData();						
			} else if (action === "Publish") {						
				vm.publishSellPaymentData();
			}
		}
	};

	vm.updateSavedSellPaymentData = function() {
		vm.sellPayment.paymentDate = moment(vm.sellPayment.paymentDateObj).format("YYYY-MM-DD");
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		vm.sellPayment.sellPaymentDate = moment(vm.sellPayment.sellPaymentDateObj).format("YYYY-MM-DD");
		$http.post('../../sellPaymentData/updateSavedSellPaymentData', vm.sellPayment).then(successCallback, errorCallback);
	};

	vm.updatePublishSellPaymentData = function() {
		vm.sellPayment.paymentDate = moment(vm.sellPayment.paymentDateObj).format("YYYY-MM-DD");
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		vm.sellPayment.sellPaymentDate = moment(vm.sellPayment.sellPaymentDateObj).format("YYYY-MM-DD");
		$http.post('../../sellPaymentData/updatePublishedSellPaymentData', vm.sellPayment).then(successCallback, errorCallback);
	};

	vm.uploadFileAndUpdateSellPaymentData = function(action) {
		if (!validatesellPaymentSaveData()) {
			return false;
		}
		var file = vm.profilePhotoFile;
		if (vm.profilePhotoFile) {			
			fileUploadService.uploadFileToUrl(file, 'sellPayment').then(function(result){
				if (result.data.status === "success") {		
					vm.sellPayment.imageURLList = vm.sellPayment.imageURLList.concat(getImageURLListObj(result.data.imageURLList));
					if (vm.sellPayment.imageURLList && vm.sellPayment.imageURLList.length > 0) {						
						vm.sellPayment.imageURL = vm.sellPayment.imageURLList[0].imageURL;
					}
					if (action === "Save") {
						vm.updateSavedSellPaymentData();						
					} else if (action === "Publish") {						
						vm.updatePublishSellPaymentData();
					}
				}
			}, function(error) {
				toaster.showMessage('error', '', 'error while uploading files');
			});
		} else {
			vm.sellPayment.imageURL = null;
			if (vm.sellPayment.imageURLList && vm.sellPayment.imageURLList.length > 0) {						
				vm.sellPayment.imageURL = vm.sellPayment.imageURLList[0].imageURL;
			}
			if (action === "Save") {
				vm.updateSavedSellPaymentData();						
			} else if (action === "Publish") {						
				vm.updatePublishSellPaymentData();
			}
		}
	};

	vm.markSellPaymentAsVerified = function() {
		vm.sellPayment.verifiedBy = {};
		vm.sellPayment.verifiedBy.userId = vm.billInkDashboardService.userId;
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		$http.post('../../sellPaymentData/markSellPaymentAsVerified', vm.sellPayment).then(successCallback, errorCallback);
	};

	vm.publishSavedSellPayment = function() {
		if (!validatesellPaymentSaveData()) {
			return false;
		}
		vm.sellPayment.companyId = vm.manageSellPaymentDataService.selectedCompanyId;
		$http.post('../../sellPaymentData/publishSavedSellPayment', vm.sellPayment).then(successCallback, errorCallback);
	};

	vm.searchLedger = function() {
		if (vm.sellPayment.ledgerData && vm.sellPayment.ledgerData.ledgerName === vm.searchLedgerNameText) {
 			vm.searchLedgerList = [];
			return;
 		}
		if (vm.searchLedgerNameText.length > 2) {			
			return $http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchLedgerNameText, companyId: vm.manageSellPaymentDataService.selectedCompanyId })
			.then(getLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.searchLedgerList = [];
			vm.sellPayment.ledgerData = {};
		}
	};

	vm.searchReceiptInLedger = function() {
		if (vm.sellPayment.receiptInLedger && vm.sellPayment.receiptInLedger.ledgerName === vm.searchReceiptInLedgerNameText) {
 			vm.searchLedgerList = [];
 			return;
		}
		if (vm.searchReceiptInLedgerNameText.length > 2) {			
			return $http.post('../../ledgerData/getLedgerNameIdListData', {ledgerName: vm.searchReceiptInLedgerNameText, companyId: vm.manageSellPaymentDataService.selectedCompanyId })
			.then(getReceiptInLedgerDataSuccessCallBack, errorCallback);
		} else {
			vm.searchLedgerList = [];
			vm.sellPayment.receiptInLedger = {};
		}
	};

	function getReceiptInLedgerDataSuccessCallBack(response) {
		vm.sellPayment.receiptInLedger = {};
		vm.searchReceiptInLedgerList = response.data.data;
		return vm.searchReceiptInLedgerList ;
	}

	vm.selectReceiptInLedger = function(ledgerObj) {
		vm.searchReceiptInLedgerNameText = ledgerObj.ledgerName;
		vm.sellPayment.receiptInLedger = ledgerObj;
	};

	vm.selectSellBill = function(sellObj) {
		if (indexOfSellObj(sellObj, vm.sellPayment.sellList) === -1) {
			var sellPaymentSellMapping = {};
			sellPaymentSellMapping.sell = sellObj;
			vm.sellPayment.sellList.push(sellPaymentSellMapping);
			vm.searchSellBillList = [];
			vm.searchBillNumber = null;
		}
	};

	vm.deleteFromSeletedSell = function(sellObj) {
		var sellObjIndex = indexOfSellObj(sellObj, vm.sellPayment.sellList);
		vm.sellPayment.sellList.splice(sellObjIndex, 1);
	};

	vm.searchSellBill = function() {
		var sellFilterData = {};
		sellFilterData.sellId = vm.searchBillNumber;
		sellFilterData.ledgerId = vm.sellPayment.ledgerData.ledgerId;
		sellFilterData.paymentStatus = [0, 1];

		if (vm.searchBillNumber) {			
			$http.post('../../sellData/getSellListData', {
				companyId: vm.manageSellPaymentDataService.selectedCompanyId, 
				start: 0, 
				noOfRecord: 20,
				filterData : sellFilterData
			}).then(getSellBillDetail, errorCallback);
		} else {
			vm.searchSellBillList = [];
		}
	};

	function indexOfSellObj(sellObj, sellPaymentSellList) {
		for (var count = 0; count < sellPaymentSellList.length; count++) {
			if (sellObj.sellId === sellPaymentSellList[count].sellPayment.sellId) {
				return count;
			}
		}
		return -1;
	}

	function getSellBillDetail(response) {
		vm.searchSellBillList = response.data.data;		
	}

	function getLedgerDataSuccessCallBack(response) {
		vm.sellPayment.ledgerData = {};
		vm.searchLedgerList = response.data.data;
		return vm.searchLedgerList;
	}

	vm.selectLedger = function(ledgerObj) {
		vm.searchLedgerNameText = ledgerObj.ledgerName;
		vm.sellPayment.ledgerData = {};
		vm.sellPayment.ledgerData.ledgerId = ledgerObj.ledgerId;
	};

	vm.fetchItemList = function(index) {
		var itemSearchText = vm.sellPayment.sellPaymentItemMappingList[index].item.itemName;
		if (itemSearchText.length > 2) {			
			$http.post('../../itemData/getItemBasicDetailList', {itemName: itemSearchText, companyId: vm.manageSellPaymentDataService.selectedCompanyId, start: 0, noOfRecord: 30})
			.then(function(response) {
				vm.itemList = [];
				vm.itemList[index] = {};
				vm.itemList[index].itemData = response.data.data;
				return vm.searchLedgerList; 
			}, errorCallback);
		}
	};

	vm.selectLItem = function(index, itemObj) {
		vm.sellPayment.sellPaymentItemMappingList[index].item = {};
		vm.sellPayment.sellPaymentItemMappingList[index].item.itemName = itemObj.itemName;
		vm.sellPayment.sellPaymentItemMappingList[index].item.itemId = itemObj.itemId;
	};

	vm.openImageModel = function(index) {
		if (index < 0 || index > (vm.sellPayment.imageURLList - 1)) {
			return;
		}

		vm.sellImageIndex = index;
		$("#sellPayment-image-show-model .modal").modal('show');
		vm.selectedPhotoURL = vm.sellPayment.imageURLList[index].imageURL;
	};

	vm.fetchImage = function(position) {
		vm.sellImageIndex += position;

		if (vm.sellImageIndex < 0) {
			vm.sellImageIndex = vm.sellPayment.imageURLList.length - 1;
		} else if (vm.sellImageIndex >= vm.sellPayment.imageURLList.length) {
			vm.sellImageIndex = 0;
		}
		vm.selectedPhotoURL = vm.sellPayment.imageURLList[vm.sellImageIndex].imageURL;
	};

	vm.deleteImage = function(index) {
		vm.sellPayment.imageURLList.splice(index, 1);
	};

	function getImageURLListObj(imageURLList) {
		var sellPaymentImageURLList = [];
		var imageURLObj = {};

		for (var count = 0; count < imageURLList.length; count++) {
			imageURLObj = {};
			imageURLObj.imageURL = imageURLList[count];
			sellPaymentImageURLList.push(imageURLObj);
		}

		return sellPaymentImageURLList;
	}
});