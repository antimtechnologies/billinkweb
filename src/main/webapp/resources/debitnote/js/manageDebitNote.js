angular
		.module('manageDebitNoteData', [ 'infinite-scroll' ])
		.service(
				"manageDebitNoteDataService",
				function($http) {

					var vm = this;

					vm.debitNoteList = [];
					vm.startOfDebitNote = 0;
					vm.noOfRecordForDebitNote = 12;
					vm.debitNoteFilterData = {};
					vm.hasMoreDebitNoteData = true;

					vm.companyCountRecordList = [];
					vm.startOfCompanyCount = 0;
					vm.noOfRecordForCompanyCount = 12;
					vm.hasMoreCompanyCountData = true;

					vm.getDebitNoteList = function(companyId) {
						$http.post('../../debitNoteData/getDebitNoteListData',
								{
									companyId : companyId,
									start : vm.startOfDebitNote,
									noOfRecord : vm.noOfRecordForDebitNote,
									filterData : vm.debitNoteFilterData
								}).then(successCallback, errorCallback);
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							if (vm.startOfDebitNote === 0) {
								vm.debitNoteList = response.data.data;
								if (vm.debitNoteList.length < vm.noOfRecordForDebitNote) {
									vm.hasMoreDebitNoteData = false;
								}
								return;
							}

							if (response.data.data.length < vm.noOfRecordForDebitNote) {
								vm.hasMoreDebitNoteData = false;
							}

							vm.debitNoteList = vm.debitNoteList
									.concat(response.data.data);
						}
					}

					function errorCallback() {

					}

					vm.getCompanyCountData = function() {
						$http
								.post(
										'../../companydata/getCompanyRequestCountDetails',
										{
											start : vm.startOfCompanyCount,
											noOfRecord : vm.noOfRecordForCompanyCount,
											sortParam : 'debitNotePendingCount'
										}).then(companyCountSuccessCallBack,
										errorCallback);
					};

					function companyCountSuccessCallBack(response) {
						if (response.data.status === "success") {
							if (vm.startOfCompanyCount === 0) {
								vm.companyCountRecordList = response.data.data;
								if (vm.companyCountRecordList.length < vm.noOfRecordForCompanyCount) {
									vm.hasMoreCompanyCountData = false;
								}
								return;
							}

							if (response.data.data.length < vm.noOfRecordForCompanyCount) {
								vm.hasMoreCompanyCountData = false;
							}

							vm.companyCountRecordList = vm.companyCountRecordList
									.concat(response.data.data);
						}
					}

				})
		.controller(
				'manageDebitNoteDataController',
				function($http, $ocLazyLoad, manageDebitNoteDataService,
						billInkDashboardService) {
					var vm = this;
					vm.manageDebitNoteDataService = manageDebitNoteDataService;
					vm.billInkDashboardService = billInkDashboardService;

					if (vm.billInkDashboardService.userRole !== "Vendor") {
						vm.statusList = [ 'New', 'Modified', 'Deleted' ];
					} else {
						vm.statusList = [ 'New', 'Modified', 'Deleted',
								'UnPublished' ];
					}

					vm.manageDebitNoteDataService.debitNoteFilterData = {};
					vm.manageDebitNoteDataService.debitNoteFilterData.statusList = [];

					vm.openDebitNoteList = function(companyObj) {

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.manageDebitNoteDataService.debitNoteFilterData.statusList = [
									'New', 'Modified', 'Deleted' ];
							vm.manageDebitNoteDataService.debitNoteFilterData.isVerified = 0;
						}

						vm.billInkDashboardService.currentCompanyObj = companyObj;
						vm.manageDebitNoteDataService.selectedCompanyId = companyObj.companyId;
						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						vm.billInkDashboardService.currentViewUrl = "../debitnote/view/viewDebitNoteList.html";
						vm.manageDebitNoteDataService
								.getDebitNoteList(vm.manageDebitNoteDataService.selectedCompanyId);
					};

					vm.init = function() {
						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.billInkDashboardService.currentViewUrl = "../debitnote/view/companyWiseDebitNoteCount.html";
							vm.manageDebitNoteDataService.getCompanyCountData();
						} else {
							vm
									.openDebitNoteList(vm.billInkDashboardService.selectedCompany);
						}
					};

					vm.backToCompanyWiseDebitNoteData = function() {
						vm.billInkDashboardService.currentViewUrl = "../debitnote/view/companyWiseDebitNoteCount.html";
						vm.manageDebitNoteDataService.getCompanyCountData();
					};

					vm.loadMoreDebitNote = function() {
						if (vm.manageDebitNoteDataService.hasMoreDebitNoteData) {
							vm.manageDebitNoteDataService.startOfDebitNote += vm.manageDebitNoteDataService.noOfRecordForDebitNote;
							vm.manageDebitNoteDataService
									.getDebitNoteList(vm.manageDebitNoteDataService.selectedCompanyId);
						}
					};

					vm.loadMoreCompanyCountData = function() {
						if (vm.manageDebitNoteDataService.hasMoreCompanyCountData) {
							vm.manageDebitNoteDataService.startOfCompanyCount += vm.manageDebitNoteDataService.noOfRecordForDebitNote;
							vm.manageDebitNoteDataService.getCompanyCountData();
						}
					};

					vm.showAddDebitNoteDataForm = function(requestFor) {
						vm.manageDebitNoteDataService.requestType = "Add";
						vm.manageDebitNoteDataService.requestFor = requestFor;
						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../debitnote/js/addEditDebitNoteDetail.js" ])
								.then(
										function() {
											vm.billInkDashboardService.currentViewUrl = "../debitnote/view/addEditDebitNoteDetail.html";
										});
					};

					vm.showDeletePrompt = function(debitNoteId, statusName) {
						vm.selectedDeleteDebitNoteId = debitNoteId;
						vm.selectedDeleteDebitNoteStatus = statusName;
						$("#debitNote-delelete-confirmation-dialog .modal")
								.modal('show');
					};

					vm.deleteSavedDebitNoteData = function() {
						$http
								.post(
										'../../debitNoteData/deleteSavedDebitNoteData',
										{
											debitNoteId : vm.selectedDeleteDebitNoteId,
											loggedInUserId : vm.billInkDashboardService.userId,
											companyId : vm.manageDebitNoteDataService.selectedCompanyId
										}).then(deleteUserSuccessCallback,
										errorCallback);
					}

					vm.deletePublishDebitNoteData = function() {
						$http
								.post(
										'../../debitNoteData/deletePublishedDebitNoteData',
										{
											debitNoteId : vm.selectedDeleteDebitNoteId,
											loggedInUserId : vm.billInkDashboardService.userId,
											companyId : vm.manageDebitNoteDataService.selectedCompanyId
										}).then(deleteUserSuccessCallback,
										errorCallback);
					};

					vm.deleteDebitNoteData = function() {
						if (vm.selectedDeleteDebitNoteStatus === "UnPublished") {
							vm.deleteSavedDebitNoteData();
						} else {
							vm.deletePublishDebitNoteData();
						}
					};

					function deleteUserSuccessCallback() {
						vm.closeDeleteModal();
						vm.init();
					}

					vm.closeDeleteModal = function() {
						vm.selectedDeleteDebitNoteId = 0;
						vm.selectedDeleteDebitNoteStatus = "";
						$("#debitNote-delelete-confirmation-dialog .modal")
								.modal('hide');
					};

					function errorCallback() {
					}

					vm.editDebitNoteData = function(debitNoteId, imageURL) {
						vm.manageDebitNoteDataService.requestType = "Edit";
						vm.manageDebitNoteDataService.requestFor = imageURL
								&& imageURL !== "" ? "uploadImage"
								: "enterManually";
						vm.manageDebitNoteDataService.editDebitNoteId = debitNoteId;
						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						$ocLazyLoad
								.load(
										[
												"../customdirective/fileUploadDirective.js",
												"../debitnote/js/addEditDebitNoteDetail.js" ])
								.then(
										function() {
											vm.billInkDashboardService.currentViewUrl = "../debitnote/view/addEditDebitNoteDetail.html";
										});
					};

					// filter related function start
					vm.launchFilter = function() {
						$("#debitNote-filter-dialog .modal").modal('show');
					};

					vm.closeFilterModal = function() {
						$("#debitNote-filter-dialog .modal").modal('hide');
					};

					vm.applyFilter = function() {
						vm.searchFilterData();
						$("#debitNote-filter-dialog .modal").modal('hide');
					};

					vm.searchFilterData = function() {
						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						vm.manageDebitNoteDataService
								.getDebitNoteList(vm.manageDebitNoteDataService.selectedCompanyId);
					};

					vm.resetFilter = function() {
						vm.manageDebitNoteDataService.debitNoteFilterData = {};
						vm.manageDebitNoteDataService.debitNoteFilterData.statusList = [];

						if (vm.billInkDashboardService.userRole !== "Vendor") {
							vm.manageDebitNoteDataService.debitNoteFilterData.statusList = [
									'New', 'Modified', 'Deleted' ];
							vm.manageDebitNoteDataService.debitNoteFilterData.isVerified = 0;
						}

						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						vm.manageDebitNoteDataService
								.getDebitNoteList(vm.manageDebitNoteDataService.selectedCompanyId);
						$("#debitNote-filter-dialog .modal").modal('hide');
					};

					vm.toggleSelection = function toggleSelection(status) {
						var idx = vm.manageDebitNoteDataService.debitNoteFilterData.statusList
								.indexOf(status);

						if (idx > -1) {
							vm.manageDebitNoteDataService.debitNoteFilterData.statusList
									.splice(idx, 1);
						} else {
							vm.manageDebitNoteDataService.debitNoteFilterData.statusList
									.push(status);
						}
					};

					vm.searchLedger = function() {
						if (vm.searchLedgerNameText.length > 2) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : vm.searchLedgerNameText,
												companyId : vm.manageDebitNoteDataService.selectedCompanyId
											}).then(
											getLedgerDataSuccessCallBack,
											errorCallback);
						} else {
							vm.manageDebitNoteDataService.debitNoteFilterData.ledgerId = 0;
							vm.searchLedgerList = [];
						}
					};

					function getLedgerDataSuccessCallBack(response) {
						vm.searchLedgerList = response.data.data;
					}

					vm.selectLedger = function(ledgerObj) {
						vm.manageDebitNoteDataService.debitNoteFilterData.ledgerId = ledgerObj.ledgerId;
						vm.searchLedgerNameText = ledgerObj.ledgerName;
					};
					// filter related function end

					vm.$onDestroy = function() {
						vm.manageDebitNoteDataService.debitNoteList = [];
						vm.manageDebitNoteDataService.startOfDebitNote = 0;
						vm.manageDebitNoteDataService.hasMoreDebitNoteData = true;

						vm.manageDebitNoteDataService.companyCountRecordList = [];
						vm.manageDebitNoteDataService.startOfCompanyCount = 0;
						vm.manageDebitNoteDataService.hasMoreCompanyCountData = true;

						vm.manageDebitNoteDataService.selectedCompanyId = 0;
					}
				});
