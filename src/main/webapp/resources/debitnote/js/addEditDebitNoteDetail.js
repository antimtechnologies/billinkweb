angular
		.module('addDebitNoteData', [ 'fileUploadDirective', 'toaster' ])
		.controller(
				'addDebitNoteDataController',
				function($http, $filter, fileUploadService, toaster,
						manageDebitNoteDataService, billInkDashboardService) {
					var vm = this;
					vm.manageDebitNoteDataService = manageDebitNoteDataService;
					vm.profilePhotoFile = null;
					vm.stateList = [];
					vm.debitNote = {};
					vm.person = [];
					vm.debitNote.imageURLList = [];
					vm.numberOfAddedItem = 1;
					vm.debitNote.debitNoteItemMappingList = [];
					vm.debitNote.debitNoteItemMappingList[vm.numberOfAddedItem - 1] = {};
					vm.numberOfItem = new Array(vm.numberOfAddedItem);
					vm.debitNote.debitNotePurchaseMappingList = [];
					vm.billInkDashboardService = billInkDashboardService;
					vm.taxCodeList = [];

	vm.locationList = [];
	
	vm.calculateIGST = false;
	vm.calculateSGST = false;
	vm.calculateCGST = false;

	vm.selectedBill = {};

	vm.numberOfAddedDebitNoteCharge = 1;
	vm.numberOfDebitNoteCharge = new Array(vm.numberOfAddedDebitNoteCharge);
	vm.debitNote.debitNoteChargeModelList = [];
	vm.debitNote.debitNoteChargeModelList[vm.numberOfAddedDebitNoteCharge - 1] = {};

	vm.init = function() {
						vm.debitNote.debitNoteItemMappingList = [];
						vm.person = [];
						vm.getLocationList();
						$http.post('../../taxcodedata/getTaxCodeValueList', {})
								.then(vm.setTaxCodeList, vm.errorFetchData);
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						if (vm.manageDebitNoteDataService.requestType === "Edit") {
							$http
									.post(
											'../../debitNoteData/getDebitNoteDetailsById',
											{
												debitNoteId : vm.manageDebitNoteDataService.editDebitNoteId,
												companyId : vm.manageDebitNoteDataService.selectedCompanyId
											}).then(vm.setDebitNoteDetail,
											vm.errorFetchData);
						} else {
							vm.debitNote.addedBy = {};
							vm.debitNote.addedBy.userId = vm.billInkDashboardService.userId;
							$http
									.post(
											'../../companydata/getBillPrefixAndLastNumber',
											{
												companyId : vm.manageDebitNoteDataService.selectedCompanyId
											})
									.then(
											function(response) {
												vm.debitNote.billNumberPrefix = response.data.data.billNumberPrefix;
												vm.debitNote.currentBillNumber = response.data.data.lastDebitNoteNumber + 1;
											}, vm.errorFetchData);
						}

						$http
								.post(
										'../../companydata/getCompanyDetails',
										{
											companyId : vm.manageDebitNoteDataService.selectedCompanyId
										}).then(vm.setCompanyDetail,
										vm.errorFetchData);

						$http.get(
								'../../locationData/findbycompanyId?companyId='
										+ vm.debitNote.companyId).then(
								function(response) {
									vm.locationList = response.data.result;
								}, vm.errorFetchData);
					};

					vm.setTaxCodeList = function(response) {
						vm.taxCodeList = response.data.data;
					};

					vm.addLedgerData = function() {
						billInkDashboardService
								.loadMenuPage({

									iconImgURL : "../images/manage_ledger.png",
									menuName : "Manage Ledger",
									url : "../ledger/view/manageLedger.html",
									js : [
											'../customdirective/infine-scroll-directive.js',
											'../ledger/js/manageLedger.js' ]

								});

					};

					vm.addDebitNoteCharge = function() {
						vm.numberOfAddedDebitNoteCharge += 1;
						vm.debitNote.debitNoteChargeModelList[vm.numberOfAddedDebitNoteCharge - 1] = {}
						vm.numberOfDebitNoteCharge = new Array(
								vm.numberOfAddedDebitNoteCharge);
					};

					vm.deleteDebitNoteCharge = function(index) {
						if (vm.numberOfAddedDebitNoteCharge === 1) {
							return;
						}
						vm.numberOfAddedDebitNoteCharge -= 1;
						vm.debitNote.debitNoteChargeModelList.splice(index, 1);
						vm.numberOfDebitNoteCharge = new Array(
								vm.numberOfAddedDebitNoteCharge);
					};

					vm.openImageModel = function(photoURL) {
						$("#debitNote-image-show-model .modal").modal('show');
						vm.selectedDebitNotePhotoURL = photoURL;
					};

					vm.AddItem = function() {
						vm.numberOfAddedItem += 1;
						vm.debitNote.debitNoteItemMappingList[vm.numberOfAddedItem - 1] = {}
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.deleteDebitNoteItem = function(index) {
						vm.numberOfAddedItem -= 1;
						vm.debitNote.debitNoteItemMappingList.splice(index, 1);
						vm.numberOfItem = new Array(vm.numberOfAddedItem);
					};

					vm.setCompanyDetail = function(response) {
						vm.companyDataForDebitNote = angular
								.copy(response.data.data);

					};
					vm.setDebitNoteDetail = function(response) {
						vm.debitNote = angular.copy(response.data.data);
						vm.searchLedgerNameText = vm.debitNote.ledgerData.ledgerName;
						// vm.searchLedgerNameText =
						// vm.debitNote.ledgerData.ledgerName;
						vm.debitNote.debitNoteDateObj = new Date(
								vm.debitNote.debitNoteDate);
						vm.debitNote.modifiedBy = {};
						vm.debitNote.modifiedBy.userId = vm.billInkDashboardService.userId;
						setGSTCalculationFlags();

						var dt = formatDateForSale(vm.debitNote.originalInvoiceDate);
						vm.debitNote.originalInvoiceDate = dt;
						if (vm.debitNote.debitNoteItemMappingList
								&& vm.debitNote.debitNoteItemMappingList.length > 0) {
							vm.numberOfAddedItem = vm.debitNote.debitNoteItemMappingList.length;
							vm.numberOfItem = new Array(vm.numberOfAddedItem);
						}

						if (!vm.debitNote.debitNoteItemMappingList
								|| vm.debitNote.debitNoteItemMappingList.length === 0) {
							vm.debitNote.debitNoteItemMappingList = [];
							vm.debitNote.debitNoteItemMappingList[vm.numberOfAddedItem - 1] = {};
						}

						if (vm.debitNote.debitNoteChargeModelList
								&& vm.debitNote.debitNoteChargeModelList.length > 0) {
							vm.numberOfAddedDebitNoteCharge = vm.debitNote.debitNoteChargeModelList.length;
							vm.numberOfDebitNoteCharge = new Array(
									vm.numberOfAddedDebitNoteCharge);
						}

						if (!vm.debitNote.debitNoteChargeModelList
								|| vm.debitNote.debitNoteChargeModelList.length === 0) {
							vm.debitNote.debitNoteChargeModelList = [];
							vm.debitNote.debitNoteChargeModelList[vm.numberOfAddedDebitNoteCharge - 1] = {};
						}
						vm.ledgerId = vm.debitNote.ledgerData.ledgerId;
						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : vm.debitNote.ledgerData.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);

						$http
								.post(
										'../../ledgerData/getMultipleGstDataByCityName',
										{
											ledgerId : vm.debitNote.ledgerData.ledgerId,
											city : vm.debitNote.cityName
										}).then(function(response) {
									vm.ledgerGstList = response.data.data;
								}, errorCallback);

						$http.post(
								'../../purchaseData/getLedgerPurchaseDetails',
								{
									ledgerId : vm.debitNote.ledgerData.ledgerId
								}).then(function(response) {
							vm.ledgerPurchaseList = response.data.data;
						}, errorCallback);

						for (var i = 0; i < vm.numberOfItem.length; i++) {
							vm.debitNote.debitNoteItemMappingList[i].itemName = vm.debitNote.debitNoteItemMappingList[i].item.itemName;
						}

						for (var j = 0; j < vm.numberOfItem.length; j++) {
							var myVar = setInterval(
									getBatchItemDetails(
											j,
											vm.debitNote.debitNoteItemMappingList[j].item.itemId),
									1000);
						}
					};

					function getBatchItemDetails(i, itemId) {

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : itemId
										})
								.then(
										function(batchres) {

											vm.debitNote.debitNoteItemMappingList[i].batchNoList = batchres.data.data;
											vm.debitNote.debitNoteItemMappingList[i].batchno = batchres.data.data.batchNo;
											console
													.log("batch Id :"
															+ vm.debitNote.debitNoteItemMappingList[i].batchId);
											$http
													.post(
															'../../itemData/getoneBatch',
															{
																batchId : vm.debitNote.debitNoteItemMappingList[i].batchId
															})
													.then(
															function(res) {
																var dt = formatDateForSale(res.data.data.mfgDate);
																var pt = formatDateForSale(res.data.data.expDate);

																vm.debitNote.debitNoteItemMappingList[i].mfgdate = dt;
																vm.debitNote.debitNoteItemMappingList[i].expdate = pt;
																// vm.sell.sellItemModel[k].batchId
																// =
																// res.data.data.batchId;
																vm.debitNote.debitNoteItemMappingList[i].batchno = res.data.data.batchNo;
																var dtp = formatDate(res.data.data.mfgDate);
																var ptp = formatDate(res.data.data.expDate);
																vm.debitNote.debitNoteItemMappingList[i].mfgdateForPrint = dtp;
																vm.debitNote.debitNoteItemMappingList[i].expdateForPrint = ptp;

															}, errorCallback);

										}, errorCallback);

					}
					
					vm.errorFetchData = function() {
					};

					vm.closeAddEditDebitNoteScreen = function() {
						vm.manageDebitNoteDataService
								.getDebitNoteList(vm.manageDebitNoteDataService.selectedCompanyId);
						vm.billInkDashboardService.currentViewUrl = "../debitnote/view/viewDebitNoteList.html";
					};

					function successCallback(response) {
						if (response.data.status === "success") {
							vm.closeAddEditDebitNoteScreen();
						} else if (response.data.status === "error") {
							alert(response.data.errMsg);
						}
					}

					function errorCallback(response) {
						alert(response.data.errMsg);
					}

					vm.saveDebitNoteData = function() {

						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						vm.debitNote.debitNoteDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"YYYY-MM-DD");
						vm.debitNote.billNumberPrefix = vm.debitNote.billNumberPrefix
								|| "";
						vm.debitNote.billNumber = vm.debitNote.billNumberPrefix
								.concat(vm.debitNote.currentBillNumber);
						$http.post('../../debitNoteData/saveDebitNoteData',
								vm.debitNote).then(successCallback,
								errorCallback);
					};

					vm.publishDebitNoteData = function() {
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId
						vm.debitNote.debitNoteDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"YYYY-MM-DD");
						vm.debitNote.billNumberPrefix = vm.debitNote.billNumberPrefix
								|| "";
						vm.debitNote.billNumber = vm.debitNote.billNumberPrefix
								.concat(vm.debitNote.currentBillNumber);
						$http.post('../../debitNoteData/publishDebitNoteData',
								vm.debitNote).then(successCallback,
								errorCallback);
					};

					function validateDebitNoteSaveUpdate() {
						if (!vm.manageDebitNoteDataService.selectedCompanyId
								|| vm.manageDebitNoteDataService.selectedCompanyId < 1) {
							toaster
									.showMessage('error', '',
											'Company data is missing to save expense details');
							return false;
						}

						// if (!vm.debitNote.ledgerData ||
						// vm.debitNote.ledgerData.ledgerId < 1) {
						// toaster.showMessage('error', '', 'Kindly select
						// ledger data to save expense
						// details');
						// return false;
						// }

						return true;
					}

					vm.uploadFileAndSaveDebitNoteData = function(action) {
						if (!validateDebitNoteSaveUpdate()) {
							return;
						}
						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'debitNote')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.debitNote.imageURLList = vm.debitNote.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.debitNote.imageURLList
															&& vm.debitNote.imageURLList.length > 0) {
														vm.debitNote.imageURL = vm.debitNote.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm.saveDebitNoteData();
													} else if (action === "Publish") {
														vm
																.publishDebitNoteData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.debitNote.imageURL = null;
							if (vm.debitNote.imageURLList
									&& vm.debitNote.imageURLList.length > 0) {
								vm.debitNote.imageURL = vm.debitNote.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.saveDebitNoteData();
							} else if (action === "Publish") {
								vm.publishDebitNoteData();
							}
						}
					};

					vm.updateSavedDebitNoteData = function() {
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						vm.debitNote.debitNoteDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"YYYY-MM-DD");
						vm.debitNote.billNumberPrefix = vm.debitNote.billNumberPrefix
								|| "";
						vm.debitNote.billNumber = vm.debitNote.billNumberPrefix
								.concat(vm.debitNote.currentBillNumber);
						$http.post(
								'../../debitNoteData/updateSavedDebitNoteData',
								vm.debitNote).then(successCallback,
								errorCallback);
					};

					vm.updatePublishDebitNoteData = function() {
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						vm.debitNote.debitNoteDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"YYYY-MM-DD");
						vm.debitNote.billNumberPrefix = vm.debitNote.billNumberPrefix
								|| "";
						vm.debitNote.billNumber = vm.debitNote.billNumberPrefix
								.concat(vm.debitNote.currentBillNumber);
						$http
								.post(
										'../../debitNoteData/updatePublishedDebitNoteData',
										vm.debitNote).then(successCallback,
										errorCallback);
					};

					vm.uploadFileAndUpdateDebitNoteData = function(action) {
						if (!validateDebitNoteSaveUpdate()) {
							return;
						}
						var file = vm.profilePhotoFile;
						if (vm.profilePhotoFile) {
							fileUploadService
									.uploadFileToUrl(file, 'debitNote')
									.then(
											function(result) {
												if (result.data.status === "success") {
													vm.debitNote.imageURLList = vm.debitNote.imageURLList
															.concat(getImageURLListObj(result.data.imageURLList));
													if (vm.debitNote.imageURLList
															&& vm.debitNote.imageURLList.length > 0) {
														vm.debitNote.imageURL = vm.debitNote.imageURLList[0].imageURL;
													}
													if (action === "Save") {
														vm
																.updateSavedDebitNoteData();
													} else if (action === "Publish") {
														vm
																.updatePublishDebitNoteData();
													}
												}
											}, function(error) {
												alert('error');
											});
						} else {
							vm.debitNote.imageURL = null;
							if (vm.debitNote.imageURLList
									&& vm.debitNote.imageURLList.length > 0) {
								vm.debitNote.imageURL = vm.debitNote.imageURLList[0].imageURL;
							}
							if (action === "Save") {
								vm.updateSavedDebitNoteData();
							} else if (action === "Publish") {
								vm.updatePublishDebitNoteData();
							}
						}
					};

					vm.markDebitNoteAsVerified = function() {
						vm.debitNote.verifiedBy = {};
						vm.debitNote.verifiedBy.userId = vm.billInkDashboardService.userId;
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						$http.post(
								'../../debitNoteData/markDebitNoteAsVerified',
								vm.debitNote).then(successCallback,
								errorCallback);
					};

					vm.publishSavedDebitNote = function() {
						vm.debitNote.companyId = vm.manageDebitNoteDataService.selectedCompanyId;
						$http.post('../../debitNoteData/publishSavedDebitNote',
								vm.debitNote).then(successCallback,
								errorCallback);
					};
					var purchaseId = "";
					vm.selectPurchaseBill = function(purchaseObj) {
						console.log(purchaseObj);
						console.log(vm.debitNote.debitNotePurchaseMappingList)
						if (indexOfPurchaseObj(purchaseObj,
								vm.debitNote.debitNotePurchaseMappingList) === -1) {
							var debitNotePurchaseMapping = {};
							debitNotePurchaseMapping.purchase = purchaseObj;
							vm.debitNote.debitNotePurchaseMappingList
									.push(debitNotePurchaseMapping);
							// vm.person.push($filter('orderBy')(vm.debitNote.debitNotePurchaseMappingList,
							// 'purchaseDate'));
							vm.searchPurchaseBillList = [];
							vm.searchBillNumber = null;
						}
						getPurchseIdForSelectedBill();

					};

					function getPurchseIdForSelectedBill() {
						if (vm.debitNote.debitNotePurchaseMappingList.length != 0) {
							console.log("not null ")
							for (var i = 0; i < vm.debitNote.debitNotePurchaseMappingList.length; i++) {
								console.log("for loop call");
								purchaseId = purchaseId
										.concat(+vm.debitNote.debitNotePurchaseMappingList[i].purchase.purchaseId
												+ ",");
							}
						}
					}

					vm.deleteFromSeletedPurchase = function(purchaseObj) {
						var purchaseObjIndex = indexOfPurchaseObj(purchaseObj,
								vm.debitNote.debitNotePurchaseMappingList);
						vm.debitNote.debitNotePurchaseMappingList.splice(
								purchaseObjIndex, 1);
					};

					vm.searchPurchaseBill = function() {
						var purchaseFilterData = {};
						purchaseFilterData.purchaseId = vm.searchBillNumber;
						purchaseFilterData.ledgerId = vm.debitNote.ledgerData.ledgerId;

						if (vm.searchBillNumber) {
							$http
									.post(
											'../../purchaseData/getPurchaseListData',
											{
												companyId : vm.manageDebitNoteDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 20,
												filterData : purchaseFilterData
											}).then(getPurchaseBillDetail,
											errorCallback);
						} else {
							vm.searchPurchaseBillList = [];
						}
					};

					vm.batchAddItemModel = function(index) {
						// console.log("call this index for urcjase controller
						// :"
						// + index);
						// console
						// .log("Purchase Item Id :"
						// + vm.purchase.purchaseItemModel[index].item.itemId);
						vm.index = index;
						vm.debitNote.itemId = vm.debitNote.debitNoteItemMappingList[index].item.itemId;
						vm.debitNote.debitNoteItemMappingList[vm.index].batchNoList = [];
						$("#new-batch-debit-dialog .modal").modal('show');
					}
					vm.saveBatchDebitNote = function() {
						console.log("vm.purchase.mfgDate :"
								+ vm.debitNote.mfgDate)
						if (!vm.debitNote.batchNo
								|| vm.debitNote.batchNo === "") {
							toaster.showMessage('error', '',
									'Kindly enter BatchNo.');
							return false;
						}
						if (!vm.debitNote.mfgDate
								|| vm.debitNote.mfgDate === "") {
							toaster.showMessage('error', '',
									'Kindly enter Mfg Date.');
							return false;
						}
						// if (!vm.purchase.expDate || vm.purchase.expDate ===
						// "") {
						// toaster.showMessage('error', '', 'Kindly enter Exp
						// Date.');
						// return false;
						// }

						if (document.getElementById("dayMonthDD").value == "Month") {
							var date = new Date(vm.debitNote.mfgDate);
							var month = date.getMonth()
									+ vm.debitNote.shelfLifes;
							date.setMonth(month);
							vm.debitNote.expDate = moment(date).format(
									"YYYY-MM-DD");
							// vm.itemBatchObj.expDates =
							// moment(date).format("MM-DD-YYYY");
						} else if (document.getElementById("dayMonthDD").value == "days") {
							vm.debitNote.expDate = moment(vm.debitNote.mfgDate)
									.format("YYYY-MM-DD");
						}

						vm.debitNote.mfgDate = moment(vm.debitNote.mfgDate)
								.format("YYYY-MM-DD");
						// vm.purchase.expDate =
						// moment(vm.purchase.expDate).format("YYYY-MM-DD");
						// vm.billInkDashboardService.item.push(vm.purchase);
						console.log("shelflife :" + vm.debitNote.shelfLifes);
						vm.debitNote.shelfLife = document
								.getElementById("dayMonthDD").value
								+ "@" + vm.debitNote.shelfLifes;

						vm.batchNo = vm.debitNote.batchNo;
						vm.mfgDate = vm.debitNote.mfgDate;
						vm.shelfLife = vm.debitNote.shelfLife;
						vm.id = vm.ItemId;

						$http
								.post('../../itemData/saveBatch', {
									batchNo : vm.batchNo,
									mfgDate : vm.mfgDate,
									expDate : vm.debitNote.expDate,
									shelfLife : vm.shelfLife,
									itemId : vm.id
								})
								.then(
										function(response) {

											vm.debitNote.debitNoteItemMappingList[vm.index].batchNoList
													.push(response.data.data);
											vm.debitNote.debitNoteItemMappingList[vm.index].batchId = response.data.data.batchId;
											// vm.debitNote.debitNoteItemMappingList[$index].batchNoList
											// vm.sell.sellItemModel[vm.index].batchId
											// =
											// response.data.data.batchId;
											var dt = formatDateForPurchase(response.data.data.mfgDate);
											var pt = formatDateForPurchase(response.data.data.expDate);
											vm.debitNote.debitNoteItemMappingList[vm.index].item.mfgDate = dt;
											vm.debitNote.debitNoteItemMappingList[vm.index].item.expDate = pt;
											$("#new-batch-debit-dialog .modal")
													.modal('hide');
											toaster.showMessage('success', '',
													'data saved successfully.');
										}, errorCallback);
						vm.debitNote.mfgDate = "";
						vm.debitNote.batchNo = "";
						vm.debitNote.shelfLife = "";
						// $("#new-batch-purchase-dialog .modal").modal('hide');
					};

					vm.closedBatch = function() {
						$("#new-batch-debit-dialog .modal").modal('hide');
					}

					function formatDateForPurchase(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						console.log("date of d:" + d);
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}

					function indexOfPurchaseObj(purchaseObj,
							debitNotePurchaseList) {
						for (var count = 0; count < debitNotePurchaseList.length; count++) {
							if (purchaseObj.purchaseId === debitNotePurchaseList[count].purchase.purchaseId) {
								return count;
							}
						}
						return -1;
					}

					function getPurchaseBillDetail(response) {
						vm.searchPurchaseBillList = response.data.data;
						console.log(vm.searchPurchaseBillList);
					}

					vm.getLocationList = function() {
						vm.locationList=[];
						$http.get(
										'../../locationData/findbycompanyId?companyId='
												+ vm.debitNote.companyId)
								.then(
										function(response) {
											
											vm.locationList = response.data.result;
										}, vm.errorFetchData);

					};
					
					vm.searchLedger = function() {
						if (vm.debitNote.ledgerData
								&& vm.debitNote.ledgerData.ledgerName === vm.searchLedgerNameText) {
							vm.searchLedgerList = [];
							return;

p						}
						return $http
								.post(
										'../../ledgerData/getLedgerNameIdListData',
										{
											ledgerName : vm.searchLedgerNameText,
											companyId : vm.manageDebitNoteDataService.selectedCompanyId
										}).then(function(response) {
									vm.searchLedgerList = response.data.data;
									vm.calculateIGST = false;
									vm.calculateSGST = false;
									vm.calculateCGST = false;
									return vm.searchLedgerList;
								}, errorCallback);
					};
					// if (vm.searchLedgerNameText.length > 0) {
					// $http.post('../../ledgerData/getLedgerNameIdListData',
					// {ledgerName: vm.searchLedgerNameText, companyId:
					// vm.manageDebitNoteDataService.selectedCompanyId })
					// .then(getLedgerDataSuccessCallBack, errorCallback);
					// } else {
					// vm.searchLedgerList = [];
					// vm.debitNote.ledgerData = {};
					// }
					// };

					function getLedgerDataSuccessCallBack(response) {
						vm.debitNote.ledgerData = {};
						vm.searchLedgerList = response.data.data;

						vm.calculateIGST = false;
						vm.calculateSGST = false;
						vm.calculateCGST = false;

					}

					vm.selectLedger = function(ledgerObj) {
						if (!ledgerObj || !ledgerObj.ledgerName) {
							return;
						}
						vm.searchLedgerNameText = ledgerObj.ledgerName;
						vm.debitNote.ledgerData = ledgerObj;
						console.log("Selected ledgerId :" + ledgerObj.ledgerId);
						vm.ledgerId = ledgerObj.ledgerId;

						$http.post(
								'../../purchaseData/getLedgerPurchaseDetails',
								{
									ledgerId : ledgerObj.ledgerId
								}).then(function(response) {
							// vm.ledgerCityList = response.data.data;
							vm.ledgerPurchaseList = response.data.data;
							// console.log("size:"+vm.ledgerPurchaseList.length);
						}, errorCallback);

						$http.post('../../ledgerData/getMultipleGstDataspcndn',
								{
									ledgerId : ledgerObj.ledgerId
								}).then(function(response) {
							vm.ledgerCityList = response.data.data;
							vm.getLedgerCityList();
						}, errorCallback);

						setGSTCalculationFlags();
					};

					vm.getLedgerCityList = function() {

						if (vm.ledgerCityList && vm.debitNote.cityName) {
							var flag = true;
							angular
									.forEach(
											vm.ledgerCityList,
											function(ledgerList) {

												if (flag == true
														&& vm.debitNote.cityName
																.toLowerCase() === ledgerList.city
																.toLowerCase()) {

													console.log(ledgerList);
													vm.debitNote.gstNumber = ledgerList.multipleGst;
													flag = false;
													return;
												}
											});
						}

					};

					vm.getLedgerpurchaseDetails = function() {
						if (vm.debitNote.InvoiceNumber != undefined
								&& vm.debitNote.InvoiceNumber != "") {
							var debitNoteDate = vm.debitNote.InvoiceNumber
									.split("@");
							vm.debitNote.originalInvoiceNumber = debitNoteDate[0];
							var dt = formatDateForSale(debitNoteDate[1]);

							vm.debitNote.originalInvoiceDate = dt;
							vm.BillDate = moment(debitNoteDate[1]).format(
									"YYYY-MM-DD");
							$http
									.post(
											'../../purchaseData/getledgerItemPurchaseList',
											{
												purchaseId : debitNoteDate[8]
											})
									.then(
											function(response) {

												vm.ledgerpurchase = angular
														.copy(response.data.data);

												if (vm.ledgerpurchase
														&& vm.ledgerpurchase.length > 0) {
													vm.numberOfAddedItem = vm.ledgerpurchase.length;
													vm.numberOfItem = new Array(
															vm.numberOfAddedItem);
												}

												for (var i = 0; i < vm.numberOfItem.length; i++) {
													vm.debitNote.debitNoteItemMappingList[i].item = {};
													vm.debitNote.debitNoteItemMappingList[i].itemName = vm.ledgerpurchase[i].item.itemName;
													vm.debitNote.debitNoteItemMappingList[i].quantity = vm.ledgerpurchase[i].quantity;
													vm.debitNote.debitNoteItemMappingList[i].item.itemId = vm.ledgerpurchase[i].item.itemId;
													vm.debitNote.debitNoteItemMappingList[i].taxRate = 0;
													vm.debitNote.debitNoteItemMappingList[i].item.hsnCode = 0;
													vm.debitNote.debitNoteItemMappingList[i].taxRatefreeItem = 0;
													vm.getAmount(i);
													$http
															.post(
																	'../../schemeData/getBatchItemDetailsByItemId',
																	{
																		itemId : vm.ledgerpurchase[i].item.itemId
																	})
															.then(
																	function(
																			batchres) {

																		var j = i - 1;

																		var dt = formatDateForSale(batchres.data.data[j].mfgDate);
																		var pt = formatDateForSale(batchres.data.data[j].expDate);

																		vm.debitNote.debitNoteItemMappingList[j].mfgdate = dt;
																		vm.debitNote.debitNoteItemMappingList[j].expdate = pt;

																		vm.debitNote.debitNoteItemMappingList[j].batchno = batchres.data.data[j].batchNo;
																		vm.debitNote.debitNoteItemMappingList[j].batchId = batchres.data.data[j].batchId;

																	},
																	errorCallback);
												}

											}, errorCallback);

							$http
									.post(
											'../../purchaseData/getItemPurchaserateByItemId',
											{
												itemId : debitNoteDate[7],
												wetPurachaseRateDate : vm.BillDate
											})
									.then(
											function(response) {
												for (var i = 0; i < vm.numberOfItem.length; i++) {
													vm.debitNote.debitNoteItemMappingList[i].purchaseRate = response.data.data[0].itemPurchaseRate;
												}

											}, errorCallback);

						}
					};

					function setGSTCalculationFlags() {
						if (vm.billInkDashboardService.igstTaxCalGSTType
								.indexOf(vm.debitNote.ledgerData.gstType) > -1) {
							vm.calculateIGST = true;
						} else {
							if (vm.debitNote.ledgerData
									&& vm.billInkDashboardService.currentCompanyObj
									&& vm.debitNote.ledgerData.state
									&& vm.debitNote.ledgerData.state.stateId === vm.billInkDashboardService.currentCompanyObj.state.stateId) {
								vm.calculateSGST = true;
								vm.calculateCGST = true;
							} else {
								vm.calculateIGST = true;
							}
						}
					}
					vm.fetchItemList = function(index) {

						var itemSearchText = vm.debitNote.debitNoteItemMappingList[index].itemName;
						if (itemSearchText.length > 0) {
							// $http.post('../../itemData/getItemBasicDetailList',
							// {itemName:
							// itemSearchText, companyId:
							// vm.manageDebitNoteDataService.selectedCompanyId,
							// start: 0, noOfRecord: 30})
							// .then(function(response) {
							// vm.itemList = [];
							// vm.itemList[index] = {};
							// vm.itemList[index].itemData = response.data.data;
//							vm.getLocationList(index);
							// }, errorCallback);

							
							console.log("getItemList :" + purchaseId);
							$http.post(
									'../../purchaseData/getItemNameForDebit', {
										itemName : itemSearchText,
										purchaseId : purchaseId
									}).then(function(res) {

								vm.debitData = angular.copy(res.data.data);
								vm.itemList = [];
								vm.itemList[index] = {};
								vm.itemList[index].itemData = res.data.data;
//								vm.getLocationList(index);
								// console.log("length res
								// :"+vm.debitData.length);
								// vm.itemList = {};
								// vm.itemListDebit = [];

							}, errorCallback);

						}

						// return
						// $http.post('../../itemData/getItemBasicDetailList',
						// {itemName: vm.searchItemNameText[index], companyId:
						// vm.manageDebitNoteDataService.selectedCompanyId,
						// start: 0, noOfRecord: 30})
						// .then(function(response) {
						// vm.itemList = [];
						// vm.itemList[index] = {};
						// vm.itemList[index].itemData = response.data.data;
						// return vm.itemList[index].itemData;
						// }, errorCallback);
					}

					vm.selectItem = function(index, itemObj) {
						if (!itemObj) {
							return;
						}

						if (vm.debitNote.debitNoteItemMappingList[index]
								&& vm.debitNote.debitNoteItemMappingList[index].debitNoteItemMappingId > 0) {
							vm.debitNote.debitNoteItemMappingList[index].debitNoteItemMappingId = 0;
							return;
						}
						vm.debitNote.debitNoteItemMappingList[index].item = {};
						vm.debitNote.debitNoteItemMappingList[index].itemName = itemObj.itemName;
						vm.debitNote.debitNoteItemMappingList[index].item.itemId = itemObj.itemId;
						vm.debitNote.debitNoteItemMappingList[index].taxRate = itemObj.taxCode;
						vm.debitNote.debitNoteItemMappingList[index].item.hsnCode = itemObj.hsnCode;
						vm.debitNote.debitNoteItemMappingList[index].purchaseRate = itemObj.purchaseRate
						vm.debitNote.debitNoteItemMappingList[index].taxRatefreeItem = itemObj.taxCode;
						vm.debitNote.debitNoteItemMappingList[index].quantity = itemObj.quantity;
						vm.debitNote.debitNoteItemMappingList[index].discount = itemObj.discount;
						vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount = itemObj.measureDiscountInAmount;
						vm.debitNote.debitNoteItemMappingList[index].additionaldiscount = itemObj.additionaldiscount;
						vm.debitNote.debitNoteItemMappingList[index].addmeasureDiscountInAmount = itemObj.addmeasureDiscountInAmount;
//						vm.getLocationList(index);
						vm.debitNote.debitNoteItemMappingList[index].locationId = itemObj.locationId;

						var dt = formatDateForSale(itemObj.mfgDate);
						var pt = formatDateForSale(itemObj.expDate);
						vm.debitNote.debitNoteItemMappingList[index].mfgdate = dt;
						vm.debitNote.debitNoteItemMappingList[index].expdate = pt;

						vm.ItemId = vm.debitNote.debitNoteItemMappingList[index].item.itemId;
						
						// if
						// (vm.billInkDashboardService.noTaxCodeGSTType.indexOf(vm.debitNote.ledgerData.gstType)
						// > -1) {
						// vm.debitNote.debitNoteItemMappingList[index].taxRate
						// = 0;
						// }

						$http
								.post(
										'../../schemeData/getFreeItemByItemId',
										{
											itemId : vm.debitNote.debitNoteItemMappingList[index].item.itemId
										})
								.then(
										function(response) {
											vm.debitNote.debitNoteItemMappingList[index].freequantity = response.data.data.totalQtyOfFreeItem;

											console
													.log("TotalQtyOfItem =====::"
															+ response.data.data.totalQtyOfItem);

											console
													.log("freeItemId=== :"
															+ response.data.data.freeItemId);
											console
													.log("Total Free Item == :"
															+ response.data.data.totalQtyOfFreeItem);

											vm.totalQtyOfFreeItems = response.data.data.totalQtyOfItem;

											vm.debitNote.debitNoteItemMappingList[index].freeItemId = response.data.data.freeItemId;
											vm.debitNote.debitNoteItemMappingList[index].freequantity = response.data.data.totalQtyOfFreeItem;
											$http
													.post(
															'../../schemeData/getFreeItemNameByItemId',
															{
																itemId : response.data.data.freeItemId
															})
													.then(
															function(res) {
																vm.debitNote.debitNoteItemMappingList[index].FreeitemName = res.data.data.itemName;
															}, errorCallback);
										}, errorCallback);

						// for batch display

						$http
								.post(
										'../../schemeData/getBatchItemDetailsByItemId',
										{
											itemId : vm.debitNote.debitNoteItemMappingList[index].item.itemId
										})
								.then(
										function(batchres) {
											vm.debitNote.debitNoteItemMappingList[index].batchNoList = batchres.data.data;
											var dt = formatDateForSale(batchres.data.data[0].mfgDate);
											var pt = formatDateForSale(batchres.data.data[0].expDate);
											vm.debitNote.debitNoteItemMappingList[index].mfgdate = dt;
											vm.debitNote.debitNoteItemMappingList[index].expdate = pt;

											vm.debitNote.debitNoteItemMappingList[index].batchno = batchres.data.data[0].batchNo;
											vm.debitNote.debitNoteItemMappingList[index].batchId = batchres.data.data[0].batchId;

											var dtp = formatDate(batchres.data.data[0].mfgDate);
											var ptp = formatDate(batchres.data.data[0].expDate);

											vm.debitNote.debitNoteItemMappingList[index].mfgdateForPrint = dtp;
											vm.debitNote.debitNoteItemMappingList[index].expdateForPrint = ptp;

										}, errorCallback);

						vm.debitNote.debitNoteItemMappingList[index].batchId = itemObj.batchId;
						

					};

					vm.getOneBatch = function(index) {
						console.log("getOneFucntion call");
						console.log("Fucntion Index:" + index);
						console.log("BatchId :"
								+ vm.debitNote.debitNoteItemMappingList[index].batchId);
						if (vm.debitNote.debitNoteItemMappingList[index].batchId
								&& vm.debitNote.debitNoteItemMappingList[index].batchId != undefined) {
							console
									.log("If batchID :"
											+ vm.debitNote.debitNoteItemMappingList[index].batchId);
							$http
									.post(
											'../../itemData/getoneBatch',
											{
												batchId : vm.debitNote.debitNoteItemMappingList[index].batchId
											})
									.then(
											function(res) {
												var dt = formatDateForSale(res.data.data.mfgDate);
												var pt = formatDateForSale(res.data.data.expDate);

												vm.debitNote.debitNoteItemMappingList[index].mfgdate = dt;
												vm.debitNote.debitNoteItemMappingList[index].expdate = pt;

												// vm.purchase.purchaseItemModel[index].batchNo
												// = res.data.data.batchNo;
												// vm.purchase.purchaseItemModel[index].batchId
												// = res.data.data.batchId;

											}, errorCallback);

						}

					}

					function formatDateForSale(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;
						return d;
						// return [ month, day, year ].join('-');
						// return [ year, month, day ].join('-');
					}

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}
					vm.fetchBatchNoItemList = function(index) {
						var itemBatchNoSearchText = vm.debitNote.debitNoteItemMappingList[index].item.batchNo;
						console.log("ItemBatch No :" + itemBatchNoSearchText);
						// if (itemSearchText.length > 2) {
						$http
								.post(
										'../../schemeData/getBatchItemDetails',
										{
											batchNo : itemBatchNoSearchText,
											itemId : vm.debitNote.debitNoteItemMappingList[index].item.itemId
										})
								.then(
										function(response) {
											vm.itemBatchNoList = [];
											vm.itemBatchNoList[index] = {};
											vm.itemBatchNoList[index].itemBatchData = response.data.data;
										}, errorCallback);
						// }
					};

					vm.getAmount = function(index) {
						if (vm.debitNote.debitNoteItemMappingList[index]) {
							vm.debitNote.debitNoteItemMappingList[index].totalItemAmount = 0;
							if (vm.debitNote.debitNoteItemMappingList[index].item
									&& vm.debitNote.debitNoteItemMappingList[index].item.itemId
									&& vm.debitNote.debitNoteItemMappingList[index].quantity
									&& vm.debitNote.debitNoteItemMappingList[index].purchaseRate) {
								vm.debitNote.debitNoteItemMappingList[index].totalItemAmount = vm.debitNote.debitNoteItemMappingList[index].purchaseRate
										* vm.debitNote.debitNoteItemMappingList[index].quantity;
								vm.debitNote.debitNoteItemMappingList[index].totalItemAmount += (vm.debitNote.debitNoteItemMappingList[index].totalItemAmount
										* vm.debitNote.debitNoteItemMappingList[index].taxRate / 100);

								if (vm.debitNote.debitNoteItemMappingList[index].discount) {

									if (vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount) {
										vm.debitNote.debitNoteItemMappingList[index].totalItemAmount -= vm.debitNote.debitNoteItemMappingList[index].discount;
									} else {
										vm.debitNote.debitNoteItemMappingList[index].totalItemAmount -= (vm.debitNote.debitNoteItemMappingList[index].totalItemAmount
												* vm.debitNote.debitNoteItemMappingList[index].discount / 100);
									}
								}
							}
							vm.debitNote.debitNoteItemMappingList[index].totalItemAmount = vm.debitNote.debitNoteItemMappingList[index].totalItemAmount
									.toFixed(2);
							return vm.debitNote.debitNoteItemMappingList[index].totalItemAmount;
						}
					};

					vm.getItemLevelAmount = function(index) {

						if (vm.debitNote.debitNoteItemMappingList[index]) {
							vm.debitNote.debitNoteItemMappingList[index].itemAmount = 0;
							if (vm.debitNote.debitNoteItemMappingList[index].item
									&& vm.debitNote.debitNoteItemMappingList[index].item.itemId
									&& vm.debitNote.debitNoteItemMappingList[index].quantity
									&& vm.debitNote.debitNoteItemMappingList[index].purchaseRate) {
								var itemAmount = vm.debitNote.debitNoteItemMappingList[index].purchaseRate
										* vm.debitNote.debitNoteItemMappingList[index].quantity;

								if (vm.debitNote.debitNoteItemMappingList[index].discount) {

									if (vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount) {
										itemAmount -= vm.debitNote.debitNoteItemMappingList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.debitNote.debitNoteItemMappingList[index].discount / 100);
									}
								}

								vm.debitNote.debitNoteItemMappingList[index].itemAmount = parseFloat(
										itemAmount).toFixed(2);
							}
							return vm.debitNote.debitNoteItemMappingList[index].itemAmount;
						}
					};

					vm.getSGSTAmount = function(index) {

						if (vm.debitNote.debitNoteItemMappingList[index]) {
							vm.debitNote.debitNoteItemMappingList[index].sgst = 0;

							if (vm.calculateSGST) {
								if (vm.debitNote.debitNoteItemMappingList[index].item
										&& vm.debitNote.debitNoteItemMappingList[index].item.itemId
										&& vm.debitNote.debitNoteItemMappingList[index].quantity
										&& vm.debitNote.debitNoteItemMappingList[index].purchaseRate) {
									var itemAmount = vm.debitNote.debitNoteItemMappingList[index].purchaseRate
											* vm.debitNote.debitNoteItemMappingList[index].quantity;

									if (vm.debitNote.debitNoteItemMappingList[index].discount) {

										if (vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount) {
											itemAmount -= vm.debitNote.debitNoteItemMappingList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.debitNote.debitNoteItemMappingList[index].discount / 100);
										}
									}

									var taxAmount = (itemAmount
											* vm.debitNote.debitNoteItemMappingList[index].taxRate / 100);
									vm.debitNote.debitNoteItemMappingList[index].sgst = taxAmount / 2;
								}
							}
							vm.debitNote.debitNoteItemMappingList[index].sgst = parseFloat(
									vm.debitNote.debitNoteItemMappingList[index].sgst)
									.toFixed(2);
							return vm.debitNote.debitNoteItemMappingList[index].sgst;
						}
					};

					vm.getCGSTAmount = function(index) {

						if (vm.debitNote.debitNoteItemMappingList[index]) {
							vm.debitNote.debitNoteItemMappingList[index].cgst = 0;
							if (vm.calculateCGST) {
								if (vm.debitNote.debitNoteItemMappingList[index].item
										&& vm.debitNote.debitNoteItemMappingList[index].item.itemId
										&& vm.debitNote.debitNoteItemMappingList[index].quantity
										&& vm.debitNote.debitNoteItemMappingList[index].purchaseRate) {
									var itemAmount = vm.debitNote.debitNoteItemMappingList[index].purchaseRate
											* vm.debitNote.debitNoteItemMappingList[index].quantity;

									if (vm.debitNote.debitNoteItemMappingList[index].discount) {

										if (vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount) {
											itemAmount -= vm.debitNote.debitNoteItemMappingList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.debitNote.debitNoteItemMappingList[index].discount / 100);
										}
									}
									var taxAmount = (itemAmount
											* vm.debitNote.debitNoteItemMappingList[index].taxRate / 100);
									vm.debitNote.debitNoteItemMappingList[index].cgst = taxAmount / 2;
								}
							}
							vm.debitNote.debitNoteItemMappingList[index].cgst = parseFloat(
									vm.debitNote.debitNoteItemMappingList[index].cgst)
									.toFixed(2);
							return vm.debitNote.debitNoteItemMappingList[index].cgst;
						}
					};

					vm.getIGSTAmount = function(index) {

						if (vm.debitNote.debitNoteItemMappingList[index]) {
							vm.debitNote.debitNoteItemMappingList[index].igst = 0;

							if (vm.calculateIGST) {
								if (vm.debitNote.debitNoteItemMappingList[index].item
										&& vm.debitNote.debitNoteItemMappingList[index].item.itemId
										&& vm.debitNote.debitNoteItemMappingList[index].quantity
										&& vm.debitNote.debitNoteItemMappingList[index].purchaseRate) {
									var itemAmount = vm.debitNote.debitNoteItemMappingList[index].purchaseRate
											* vm.debitNote.debitNoteItemMappingList[index].quantity;

									if (vm.debitNote.debitNoteItemMappingList[index].discount) {

										if (vm.debitNote.debitNoteItemMappingList[index].measureDiscountInAmount) {
											itemAmount -= vm.debitNote.debitNoteItemMappingList[index].discount;
										} else {
											itemAmount -= (itemAmount
													* vm.debitNote.debitNoteItemMappingList[index].discount / 100);
										}
									}
									var taxAmount = (itemAmount
											* vm.debitNote.debitNoteItemMappingList[index].taxRate / 100);
									vm.debitNote.debitNoteItemMappingList[index].igst = taxAmount;
								}
							}
							vm.debitNote.debitNoteItemMappingList[index].igst = parseFloat(
									vm.debitNote.debitNoteItemMappingList[index].igst)
									.toFixed(2);
							return vm.debitNote.debitNoteItemMappingList[index].igst;
						}
					};

					vm.getGrossTotalAmount = function() {
						vm.debitNote.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.debitNote.debitNoteItemMappingList.length; counter++) {
							if (vm.debitNote.debitNoteItemMappingList[counter].totalItemAmount
									&& vm.debitNote.debitNoteItemMappingList[counter].totalItemAmount > 0) {
								vm.debitNote.grossTotalAmount += parseFloat(vm.debitNote.debitNoteItemMappingList[counter].totalItemAmount);
							}
						}

						if (vm.debitNote.debitNoteChargeModelList.length > 0) {
							for (var count = 0; count < vm.debitNote.debitNoteChargeModelList.length; count++) {
								if (vm.debitNote.debitNoteChargeModelList[count].totalItemAmount)
									vm.debitNote.grossTotalAmount += parseFloat(vm.debitNote.debitNoteChargeModelList[count].totalItemAmount);
							}
						}

						vm.debitNote.grossTotalAmount = parseFloat(
								vm.debitNote.grossTotalAmount).toFixed(2);
						return vm.debitNote.grossTotalAmount;
					};

					vm.getTotalBillAmount = function() {
						vm.debitNote.amount = parseFloat(vm.debitNote.grossTotalAmount);

						if (vm.debitNote.discount && vm.debitNote.discount > 0) {

							if (vm.debitNote.purchaseMeasureDiscountInAmount) {
								vm.debitNote.amount -= vm.debitNote.discount;
							} else {
								vm.debitNote.amount -= (vm.debitNote.amount
										* vm.debitNote.discount / 100);
							}

						}
						vm.debitNote.amount = parseFloat(vm.debitNote.amount)
								.toFixed(2);
						return vm.debitNote.amount;
					};

					function getImageURLListObj(imageURLList) {
						var debitNoteImageURLList = [];
						var imageURLObj = {};

						for (var count = 0; count < imageURLList.length; count++) {
							imageURLObj = {};
							imageURLObj.imageURL = imageURLList[count];
							debitNoteImageURLList.push(imageURLObj);
						}

						return debitNoteImageURLList;
					}

					vm.openImageModel = function(index) {
						if (index < 0
								|| index > (vm.debitNote.imageURLList - 1)) {
							return;
						}

						vm.debitNoteImageIndex = index;
						$("#debitNote-image-show-model .modal").modal('show');
						vm.selectedPhotoURL = vm.debitNote.imageURLList[index].imageURL;
					};

					vm.fetchImage = function(position) {
						vm.debitNoteImageIndex += position;

						if (vm.debitNoteImageIndex < 0) {
							vm.debitNoteImageIndex = vm.debitNote.imageURLList.length - 1;
						} else if (vm.debitNoteImageIndex >= vm.debitNote.imageURLList.length) {
							vm.debitNoteImageIndex = 0;
						}
						vm.selectedPhotoURL = vm.debitNote.imageURLList[vm.debitNoteImageIndex].imageURL;
					};

					vm.deleteImage = function(index) {
						vm.debitNote.imageURLList.splice(index, 1);
					};

					// DebitNoteCharge related method.
					vm.fetchDebitNoteChargeLedgerList = function(index) {
						var ledgerSearchText = vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerName;
						if (ledgerSearchText.length > 2) {
							$http
									.post(
											'../../ledgerData/getLedgerNameIdListData',
											{
												ledgerName : ledgerSearchText,
												companyId : vm.manageDebitNoteDataService.selectedCompanyId,
												start : 0,
												noOfRecord : 30
											})
									.then(
											function(response) {
												vm.ledgerList = [];
												vm.ledgerList[index] = {};
												vm.ledgerList[index].ledgerData = response.data.data;
											}, errorCallback);
						}

						return $http
								.post(
										'../../ledgerData/getLedgerNameIdListData',
										{
											ledgerName : vm.searchChargeNameText[index],
											companyId : vm.manageDebitNoteDataService.selectedCompanyId,
											start : 0,
											noOfRecord : 30
										})
								.then(
										function(response) {
											vm.ledgerList = [];
											vm.ledgerList[index] = {};
											vm.ledgerList[index].ledgerData = response.data.data;
											return vm.ledgerList[index].ledgerData
										}, errorCallback);
					};

					vm.selectDebitNoteChargeLedger = function(index, ledgerObj) {
						if (!ledgerObj) {
							return;
						}

						if (vm.debitNote.debitNoteChargeModelList[index]
								&& vm.debitNote.debitNoteChargeModelList[index].debitNoteChargeMappingId > 0) {
							vm.debitNote.debitNoteChargeModelList[index].debitNoteChargeMappingId = 0;
							return;
						}
						vm.debitNote.debitNoteChargeModelList[index].ledger = {};
						vm.debitNote.debitNoteChargeModelList[index].ledger = ledgerObj;
						vm.debitNote.debitNoteChargeModelList[index].taxRate = ledgerObj.taxCode;
						if (vm.billInkDashboardService.noTaxCodeGSTType
								.indexOf(vm.debitNote.ledgerData.gstType) > -1) {
							vm.debitNote.debitNoteChargeModelList[index].taxRate = 0;
						}
					};

					vm.getDebitNoteChargeAmount = function(index) {
						vm.debitNote.debitNoteChargeModelList[index].totalItemAmount = 0;
						vm.debitNote.debitNoteChargeModelList[index].quantity = 1;
						if (vm.debitNote.debitNoteChargeModelList[index].ledger
								&& vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerId
								&& vm.debitNote.debitNoteChargeModelList[index].quantity
								&& vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge) {
							vm.debitNote.debitNoteChargeModelList[index].totalItemAmount = vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge
									* vm.debitNote.debitNoteChargeModelList[index].quantity;

							if (vm.debitNote.debitNoteChargeModelList[index].taxRate) {
								vm.debitNote.debitNoteChargeModelList[index].totalItemAmount += (vm.debitNote.debitNoteChargeModelList[index].totalItemAmount
										* vm.debitNote.debitNoteChargeModelList[index].taxRate / 100);
							}

							if (vm.debitNote.debitNoteChargeModelList[index].discount) {
								if (vm.debitNote.debitNoteChargeModelList[index].measureDiscountInAmount) {
									vm.debitNote.debitNoteChargeModelList[index].totalItemAmount -= vm.debitNote.debitNoteChargeModelList[index].discount;
								} else {
									vm.debitNote.debitNoteChargeModelList[index].totalItemAmount -= (vm.debitNote.debitNoteChargeModelList[index].totalItemAmount
											* vm.debitNote.debitNoteChargeModelList[index].discount / 100);
								}
							}
						}
						vm.debitNote.debitNoteChargeModelList[index].totalItemAmount = vm.debitNote.debitNoteChargeModelList[index].totalItemAmount
								.toFixed(2);
						return vm.debitNote.debitNoteChargeModelList[index].totalItemAmount;
					};

					vm.getDebitNoteChargeLevelAmount = function(index) {
						vm.debitNote.debitNoteChargeModelList[index].itemAmount = 0;
						vm.debitNote.debitNoteChargeModelList[index].quantity = 1;
						if (vm.debitNote.debitNoteChargeModelList[index].ledger
								&& vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerId
								&& vm.debitNote.debitNoteChargeModelList[index].quantity
								&& vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge) {
							var itemAmount = vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge
									* vm.debitNote.debitNoteChargeModelList[index].quantity;

							if (vm.debitNote.debitNoteChargeModelList[index].discount) {

								if (vm.debitNote.debitNoteChargeModelList[index].measureDiscountInAmount) {
									itemAmount -= vm.debitNote.debitNoteChargeModelList[index].discount;
								} else {
									itemAmount -= (itemAmount
											* vm.debitNote.debitNoteChargeModelList[index].discount / 100);
								}
							}

							vm.debitNote.debitNoteChargeModelList[index].itemAmount = parseFloat(
									itemAmount).toFixed(2);
						}
						return vm.debitNote.debitNoteChargeModelList[index].itemAmount;
					};

					vm.getDebitNoteChargeSGSTAmount = function(index) {

						vm.debitNote.debitNoteChargeModelList[index].sgst = 0;
						vm.debitNote.debitNoteChargeModelList[index].quantity = 1;
						if (vm.calculateSGST) {
							if (vm.debitNote.debitNoteChargeModelList[index].ledger
									&& vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerId
									&& vm.debitNote.debitNoteChargeModelList[index].quantity
									&& vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge) {
								var itemAmount = vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge
										* vm.debitNote.debitNoteChargeModelList[index].quantity;

								if (vm.debitNote.debitNoteChargeModelList[index].discount) {

									if (vm.debitNote.debitNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.debitNote.debitNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.debitNote.debitNoteChargeModelList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.debitNote.debitNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.debitNote.debitNoteChargeModelList[index].taxRate / 100);
								}
								vm.debitNote.debitNoteChargeModelList[index].sgst = taxAmount / 2;
							}
						}
						vm.debitNote.debitNoteChargeModelList[index].sgst = parseFloat(
								vm.debitNote.debitNoteChargeModelList[index].sgst)
								.toFixed(2);
						return vm.debitNote.debitNoteChargeModelList[index].sgst;
					};

					vm.getDebitNoteChargeCGSTAmount = function(index) {

						vm.debitNote.debitNoteChargeModelList[index].cgst = 0;
						vm.debitNote.debitNoteChargeModelList[index].quantity = 1;
						if (vm.calculateCGST) {
							if (vm.debitNote.debitNoteChargeModelList[index].ledger
									&& vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerId
									&& vm.debitNote.debitNoteChargeModelList[index].quantity
									&& vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge) {
								var itemAmount = vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge
										* vm.debitNote.debitNoteChargeModelList[index].quantity;

								if (vm.debitNote.debitNoteChargeModelList[index].discount) {

									if (vm.debitNote.debitNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.debitNote.debitNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.debitNote.debitNoteChargeModelList[index].discount / 100);
									}
								}
								var taxAmount = 0;
								if (vm.debitNote.debitNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.debitNote.debitNoteChargeModelList[index].taxRate / 100);
								}

								vm.debitNote.debitNoteChargeModelList[index].cgst = taxAmount / 2;
							}
						}
						vm.debitNote.debitNoteChargeModelList[index].cgst = parseFloat(
								vm.debitNote.debitNoteChargeModelList[index].cgst)
								.toFixed(2);
						return vm.debitNote.debitNoteChargeModelList[index].cgst;
					};

					vm.getDebitNoteChargeIGSTAmount = function(index) {

						vm.debitNote.debitNoteChargeModelList[index].igst = 0;
						vm.debitNote.debitNoteChargeModelList[index].quantity = 1;
						if (vm.calculateIGST) {
							if (vm.debitNote.debitNoteChargeModelList[index].ledger
									&& vm.debitNote.debitNoteChargeModelList[index].ledger.ledgerId
									&& vm.debitNote.debitNoteChargeModelList[index].quantity
									&& vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge) {
								var itemAmount = vm.debitNote.debitNoteChargeModelList[index].debitNoteCharge
										* vm.debitNote.debitNoteChargeModelList[index].quantity;

								if (vm.debitNote.debitNoteChargeModelList[index].discount) {

									if (vm.debitNote.debitNoteChargeModelList[index].measureDiscountInAmount) {
										itemAmount -= vm.debitNote.debitNoteChargeModelList[index].discount;
									} else {
										itemAmount -= (itemAmount
												* vm.debitNote.debitNoteChargeModelList[index].discount / 100);
									}
								}

								var taxAmount = 0;
								if (vm.debitNote.debitNoteChargeModelList[index].taxRate) {
									taxAmount = (itemAmount
											* vm.debitNote.debitNoteChargeModelList[index].taxRate / 100);
								}

								vm.debitNote.debitNoteChargeModelList[index].igst = taxAmount;
							}
						}
						vm.debitNote.debitNoteChargeModelList[index].igst = parseFloat(
								vm.debitNote.debitNoteChargeModelList[index].igst)
								.toFixed(2);
						return vm.debitNote.debitNoteChargeModelList[index].igst;
					};

					vm.getDebitNoteChargeGrossTotalAmount = function() {
						vm.debitNote.grossTotalAmount = 0;
						for (var counter = 0; counter < vm.debitNote.debitNoteChargeModelList.length; counter++) {
							if (vm.debitNote.debitNoteChargeModelList[counter].totalItemAmount
									&& vm.debitNote.debitNoteChargeModelList[counter].totalItemAmount > 0) {
								vm.debitNote.grossTotalAmount += parseFloat(vm.debitNote.debitNoteChargeModelList[counter].totalItemAmount);
							}
						}
						vm.debitNote.grossTotalAmount = parseFloat(
								vm.debitNote.grossTotalAmount).toFixed(2);
						return vm.debitNote.grossTotalAmount;
					};

					vm.prepareHSNCodeWiseTaxList = function() {
						vm.hsnTaxCodeList = {};
						vm.totalIGST = 0;
						vm.totalSGST = 0;
						vm.totalCGST = 0;

						for ( var i in vm.debitNote.debitNoteItemMappingList) {
							var debitNoteItemObj = vm.debitNote.debitNoteItemMappingList[i];
							if (debitNoteItemObj.item) {

								debitNoteItemObj.item.hsnCode = debitNoteItemObj.item.hsnCode
										|| "";
								vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode] = vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode]
										|| {};
								if (vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate]) {
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].igst += parseFloat(debitNoteItemObj.igst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].cgst += parseFloat(debitNoteItemObj.cgst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].sgst += parseFloat(debitNoteItemObj.sgst);
								} else {
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate] = {};
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].igst = parseFloat(debitNoteItemObj.igst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].cgst = parseFloat(debitNoteItemObj.cgst);
									vm.hsnTaxCodeList[debitNoteItemObj.item.hsnCode][debitNoteItemObj.taxRate].sgst = parseFloat(debitNoteItemObj.sgst);
								}
								vm.totalIGST += parseFloat(debitNoteItemObj.igst);
								vm.totalCGST += parseFloat(debitNoteItemObj.cgst);
								vm.totalSGST += parseFloat(debitNoteItemObj.sgst);
							}
						}

						for ( var i in vm.debitNote.debitNoteItemMappingList) {
							var debitNoteFreightObj = vm.debitNote.debitNoteItemMappingList[i];
							if (debitNoteFreightObj.ledger) {

								debitNoteFreightObj.ledger.sacCode = debitNoteFreightObj.ledger.sacCode
										|| "";
								vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode] = vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode]
										|| {};
								if (vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate]) {
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].igst += parseFloat(debitNoteFreightObj.igst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].cgst += parseFloat(debitNoteFreightObj.cgst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].sgst += parseFloat(debitNoteFreightObj.sgst);
								} else {
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate] = {};
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].igst = parseFloat(debitNoteFreightObj.igst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].cgst = parseFloat(debitNoteFreightObj.cgst);
									vm.hsnTaxCodeList[debitNoteFreightObj.ledger.sacCode][debitNoteFreightObj.taxRate].sgst = parseFloat(debitNoteFreightObj.sgst);
								}
								vm.totalIGST += parseFloat(debitNoteFreightObj.igst);
								vm.totalCGST += parseFloat(debitNoteFreightObj.cgst);
								vm.totalSGST += parseFloat(debitNoteFreightObj.sgst);
							}
						}

						vm.hsnList = [];
						if (Object.keys(vm.hsnTaxCodeList).length > 0) {
							for ( var hsnCode in vm.hsnTaxCodeList) {
								for ( var taxRate in vm.hsnTaxCodeList[hsnCode]) {
									var hsnTaxCodeObj = {};
									hsnTaxCodeObj.hsnCode = hsnCode;
									hsnTaxCodeObj.taxRate = taxRate;
									hsnTaxCodeObj.igst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].igst)
											.toFixed(2);
									hsnTaxCodeObj.cgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].cgst)
											.toFixed(2);
									hsnTaxCodeObj.sgst = parseFloat(
											vm.hsnTaxCodeList[hsnCode][taxRate].sgst)
											.toFixed(2);
									vm.hsnList.push(hsnTaxCodeObj);
								}
							}
						}
						vm.totalIGST = parseFloat(vm.totalIGST).toFixed(2);
						vm.totalCGST = parseFloat(vm.totalCGST).toFixed(2);
						vm.totalSGST = parseFloat(vm.totalSGST).toFixed(2);
						vm.totalItemLevelAmountWithoutTax = parseFloat(vm.debitNote.grossTotalAmount)
								- parseFloat(vm.totalSGST)
								- parseFloat(vm.totalCGST)
								- parseFloat(vm.totalIGST);
						vm.totalItemLevelAmountWithoutTax = parseFloat(
								vm.totalItemLevelAmountWithoutTax).toFixed(2);
					};

					vm.printDiv = function() {
						vm.debitNote.sellPrintDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second
							var printContent = document
									.getElementById('printSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);
					}

					vm.withOutBatchPrintDiv = function() {
						vm.debitNote.sellPrintDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(
								function() {
									// your code to be executed after 1 second

									var printContent = document
											.getElementById('printWithOutbatchNoSellDiv');
									var WinPrint = window.open('', '',
											'width=900,height=650');
									WinPrint.document
											.write(printContent.innerHTML);
									WinPrint.document.close();
									WinPrint.focus();
									WinPrint.print();
									WinPrint.close();
								}, 1000);

					}

					vm.allFieldsPrintDiv = function() {
						vm.debitNote.sellPrintDate = moment(
								vm.debitNote.debitNoteDateObj).format(
								"DD MMM, YYYY");
						vm.prepareHSNCodeWiseTaxList();
						setTimeout(function() {
							// your code to be executed after 1 second
							var printContent = document
									.getElementById('printAllFieldSellDiv');
							var WinPrint = window.open('', '',
									'width=900,height=650');
							WinPrint.document.write(printContent.innerHTML);
							WinPrint.document.close();
							WinPrint.focus();
							WinPrint.print();
							WinPrint.close();
						}, 1000);

					}

					var one = [ "", "one ", "two ", "three ", "four ", "five ",
							"six ", "seven ", "eight ", "nine ", "ten ",
							"eleven ", "twelve ", "thirteen ", "fourteen ",
							"fifteen ", "sixteen ", "seventeen ", "eighteen ",
							"nineteen " ];
					var ten = [ "", "", "twenty ", "thirty ", "forty ",
							"fifty ", "sixty ", "seventy ", "eighty ",
							"ninety " ];
					function numToWords(n, s) {
						var str = "";
						// if n is more than 19, divide it
						if (n > 19) {
							str += ten[parseInt(n / 10)] + one[n % 10];
						} else {
							str += one[n];
						}

						// if n is non-zero
						if (n != 0) {
							str += s;
						}
						return str;
					}
					function convertToWords(n) {
						// stores word representation of given number n
						var out = "";

						// handles digits at ten millions and hundred
						// millions places (if any)
						out += numToWords(parseInt((n / 10000000)), "crore ");

						// handles digits at hundred thousands and one
						// millions places (if any)
						out += numToWords(parseInt(((n / 100000) % 100)),
								"lakh ");

						// handles digits at thousands and tens thousands
						// places (if any)
						out += numToWords(parseInt(((n / 1000) % 100)),
								"thousand ");

						// handles digit at hundreds places (if any)
						out += numToWords(parseInt(((n / 100) % 10)),
								"hundred ");

						if (n > 100 && n % 100 > 0) {
							out += "and ";
						}

						// handles digits at ones and tens places (if any)
						out += numToWords(parseInt((n % 100)), "");

						return out;
					}
					vm.convertToWordsString = function(number) {
						var sArray = number.split(".");
						var paisePart = "";
						var integer = parseInt(sArray[0]);
						if (sArray.length == 2) {
							var s = parseInt(sArray[1]);
							paisePart = convertToWords(s);
						}
						var words = convertToWords(integer)
								+ (paisePart.length > 0 ? ("and " + paisePart + "Paise")
										: "") + " Only";
						return words;

					}

				});