<html>
<head>
<title>BILLINK</title>
<link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>
.login-block {
	background: linear-gradient(to bottom, #16a7df, #007bff);
	/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	float: left;
	width: 100%;
	padding: 50px 0;
}

.container {
	background: #fff;
	border-radius: 10px;
	box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1);
}

.carousel-inner {
	border-radius: 0 10px 10px 0;
}

.carousel-caption {
	text-align: left;
	left: 5%;
}

.login-sec {
	padding: 50px 30px;
	position: relative;
}

.login-sec .copy-text {
	position: absolute;
	width: 80%;
	bottom: 20px;
	font-size: 13px;
	text-align: center;
}

.login-sec .copy-text i {
	color: #FEB58A;
}

.login-sec .copy-text a {
	color: #E36262;
}

.login-sec h2 {
	margin-bottom: 30px;
	font-weight: 800;
	font-size: 30px;
	color: #16a7df;
}

.login-sec h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #007bff;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
	margin-left: auto;
	margin-right: auto
}

.btn-login {
	background: #007bff;
	color: #fff;
	font-weight: 600;
}

button#login-submit:focus {
    background: #007bff;
    color: #fff;
    font-weight: 600;
}

.banner-text {
	width: 70%;
	position: absolute;
	bottom: 40px;
	padding-left: 20px;
}

.banner-text h2 {
	color: #fff;
	font-weight: 600;
}

.banner-text h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #FFF;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
}

.banner-text p {
	color: #fff;
}
</style>
<script src="resources/js/jquery-3.3.1.min.js"></script>
<script src="resources/js/angular.min.js"></script>
<script src="resources/js/index.js"></script>
<script src="resources/js/loaddingSpinner.js"></script>
<script src="resources/js/toaster.js"></script>
<link href="resources/bootstrap/css/toaster.css" rel="stylesheet">
<title></title>
</head>
<body ng-app="login" ng-controller="loginController as vm">
	<section class="login-block" style="height: 100%;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 login-sec">
					<div class="col-sm-12" style="padding-bottom: 60px;" align="center">
						<img class="d-block img-fluid"
									src="resources/images/bill_ink_logo.png" alt="First slide">
					</div>
					<h2 class="text-center">Login Now</h2>
					<form ng-submit="vm.login()">
						<div class="form-group">
							<label for="exampleInputEmail1" class="text-uppercase">User Name</label>
							<input type="text" class="form-control" ng-model="vm.billinkusername"
								placeholder="user name" autofocus="autofocus" >
	
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1" class="text-uppercase">Password</label>
							<input type="password" class="form-control" ng-model="vm.billinkpassword"
								placeholder="password">
						</div>
						
						<div style="color: red;" ng-bind="vm.errorMessage"></div>
						<div class="">
							<button type="submit" id="login-submit"
								class="btn btn-login form-control">Login</button>
						</div>
					</form>
				</div>
				<div class="col-md-8 banner-sec">
					<div id="carouselExampleIndicators" class="carousel slide">
						<div class="carousel-inner">
							<div class="carousel-item active" align="center">
								<img class="d-block img-fluid"
									src="resources/images/bill_ink_logo.jpeg" alt="First slide">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
<!------ Include the above in your HEAD tag ---------->
