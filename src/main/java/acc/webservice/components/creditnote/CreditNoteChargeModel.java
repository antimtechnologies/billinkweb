package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_CHARGE_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;

public class CreditNoteChargeModel {

	private int creditNoteChargeMappingId;
	private double creditNoteCharge;
	private boolean deleted;
	private LedgerModel ledger;
	private CreditNoteModel creditNote;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private boolean measureDiscountInAmount;

	public CreditNoteChargeModel() {
	}

	public CreditNoteChargeModel(ResultSet rs) throws SQLException {
		setCreditNoteChargeMappingId(rs.getInt(CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE_MAPPING_ID.toString()));
		setCreditNoteCharge(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE.toString()));
		setDiscount(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setTaxRate(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setItemAmount(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString()));
		setCgst(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.C_GST.toString()));
		setIgst(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(CREDIT_NOTE_CHARGE_DATA_COLUMN.S_GST.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(CREDIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		LedgerModel fetchedLedger = new LedgerModel();
		fetchedLedger.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedger.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedger.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));

		setLedger(fetchedLedger);
	}

	public int getCreditNoteChargeMappingId() {
		return creditNoteChargeMappingId;
	}

	public void setCreditNoteChargeMappingId(int creditNoteChargeMappingId) {
		this.creditNoteChargeMappingId = creditNoteChargeMappingId;
	}

	public CreditNoteModel getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(CreditNoteModel creditNote) {
		this.creditNote = creditNote;
	}

	public double getCreditNoteCharge() {
		return creditNoteCharge;
	}

	public void setCreditNoteCharge(double creditNoteCharge) {
		this.creditNoteCharge = creditNoteCharge;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LedgerModel getLedger() {
		return ledger;
	}

	public void setLedger(LedgerModel ledger) {
		this.ledger = ledger;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	@Override
	public String toString() {
		return "SellFreightChargeModel [sellFreightMappingId=" + creditNoteChargeMappingId + ", freightCharge="
				+ creditNoteCharge + "]";
	}

}
