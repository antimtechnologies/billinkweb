package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class CreditNoteImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<CreditNoteImageModel> getCreditNoteImageList(int companyId, int creditNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCreditNoteImageResultExctractor());
	}

	private ResultSetExtractor<List<CreditNoteImageModel>> getCreditNoteImageResultExctractor() {
		return new ResultSetExtractor<List<CreditNoteImageModel>>() {
			@Override
			public List<CreditNoteImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CreditNoteImageModel> creditNoteImageModelList = new ArrayList<>();
				while (rs.next()) {
					creditNoteImageModelList.add(new CreditNoteImageModel(rs));
				}
				return creditNoteImageModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteImageURLData(List<CreditNoteImageModel> creditNoteImageModelList, int companyId, int creditNoteId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s) ", CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> creditNoteImageURLMapList = new ArrayList<>();
		Map<String, Object> creditNoteImageURLMap;
		for (CreditNoteImageModel creditNoteImageModel : creditNoteImageModelList) {
			creditNoteImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(creditNoteImageModel.getImageURL())) {			
				creditNoteImageModel.setImageURL(creditNoteImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			creditNoteImageURLMap.put(CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL.toString(), creditNoteImageModel.getImageURL());
			creditNoteImageURLMap.put(CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
			creditNoteImageURLMap.put(CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			creditNoteImageURLMapList.add(creditNoteImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), creditNoteImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteImageURLData(int companyId, int creditNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_IMAGE_URL, CREDIT_NOTE_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.CREDIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, CREDIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, creditNoteId, companyId });
	}

}
