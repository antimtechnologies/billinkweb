package acc.webservice.components.creditnote;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.global.utils.DateUtility;
import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.creditnote.utility.CreditNoteToDebitNoteUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.components.users.UserService;

@RestController
@RequestMapping("/creditNoteData/")
public class CreditNoteController {

	@Autowired
	private CreditNoteService creditNoteService;

	@Autowired
	private CreditNoteToDebitNoteUtility creditNoteToDebitNoteUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getCreditNoteListData", method = RequestMethod.POST)
	public Map<String, Object> getCreditNoteListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.getCreditNoteListData(companyId, start, numberOfRecord, filterParamter));
		return response;
	}

	@RequestMapping(value = "getCreditNoteDetailsById", method = RequestMethod.POST)
	Map<String, Object> getCreditNoteDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int creditNoteId = ApplicationUtility.getIntValue(parameters,
				CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.getCreditNoteDetailsById(companyId, creditNoteId));
		return response;
	}

	@RequestMapping(value = "publishCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> publishCreditNoteData(@RequestBody CreditNoteModel creditNoteModel)
			throws AccountingSofwareException {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.publishCreditNoteData(creditNoteModel));
		try {
			creditNoteToDebitNoteUtility.saveDebitNoteForSelectedLedger(creditNoteModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return response;
	}

	@RequestMapping(value = "saveCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> saveCreditNoteData(@RequestBody CreditNoteModel creditNoteModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.saveCreditNoteData(creditNoteModel));
		return response;
	}

	@RequestMapping(value = "updatePublishedCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedCreditNoteData(@RequestBody CreditNoteModel creditNoteModel) {
		Map<String, Object> response = new HashMap<>();
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				creditNoteModel.getCreditNoteDate())) {
			creditNoteModel.setCreditNoteDate(DateUtility.convertDateFormat(creditNoteModel.getCreditNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		response.put("status", "success");
		response.put("data", creditNoteService.updatePublishedCreditNoteData(creditNoteModel));
		return response;
	}

	@RequestMapping(value = "updateSavedCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedCreditNoteData(@RequestBody CreditNoteModel creditNoteModel) {
		Map<String, Object> response = new HashMap<>();
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				creditNoteModel.getCreditNoteDate())) {
			creditNoteModel.setCreditNoteDate(DateUtility.convertDateFormat(creditNoteModel.getCreditNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		response.put("status", "success");
		response.put("data", creditNoteService.updateSavedCreditNoteData(creditNoteModel));
		return response;
	}

	@RequestMapping(value = "deletePublishedCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishedCreditNoteData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int creditNoteId = ApplicationUtility.getIntValue(parameters,
				CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.deletePublishedCreditNoteData(companyId, creditNoteId, deletedBy));
		return response;
	}

	@RequestMapping(value = "deleteSavedCreditNoteData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedCreditNoteData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int creditNoteId = ApplicationUtility.getIntValue(parameters,
				CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.deleteSavedCreditNoteData(companyId, creditNoteId, deletedBy));
		return response;
	}

	@RequestMapping(value = "markCreditNoteAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markCreditNoteAsVerified(@RequestBody CreditNoteModel creditNoteModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", creditNoteService.markCreditNoteAsVerified(creditNoteModel));
		return response;
	}

	@RequestMapping(value = "publishSavedCreditNote", method = RequestMethod.POST)
	public Map<String, Object> publishSavedCreditNote(@RequestBody CreditNoteModel creditNoteModel)
			throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				creditNoteModel.getCreditNoteDate())) {
			creditNoteModel.setCreditNoteDate(DateUtility.convertDateFormat(creditNoteModel.getCreditNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}

		responseData.put("status", "success");
		responseData.put("data", creditNoteService.publishSavedCreditNote(creditNoteModel));

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteDate()) > 0) {
			creditNoteModel.setCreditNoteDate(DateUtility.convertDateFormat(creditNoteModel.getCreditNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}

		try {
			if (creditNoteModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
				creditNoteToDebitNoteUtility.saveDebitNoteForSelectedLedger(creditNoteModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "getcreditDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getCreditDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;

		if (request.getParameter("companyId") != null && request.getParameter("ledgerId") != null
				&& request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = "http://localhost:8090/user/creditnotecharge/findcreditnotedetails?companyId=" + companyId;
		} else if (request.getParameter("companyId") != null && request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			String billNumber = request.getParameter("billNumber");
			fooResourceUrl = "http://localhost:8090/user/creditnotecharge/findcreditnotedetails?companyId=" + companyId
					+ "&billNumber" + billNumber;
		} else if (request.getParameter("companyId") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = "http://localhost:8090/user/creditnotecharge/findcreditnotedetails?companyId=" + companyId;
		} else if (request.getParameter("billNumber") != null) {
			String billNumber = request.getParameter("companyId");
			fooResourceUrl = "http://localhost:8090/user/creditnotecharge/findcreditnotedetails?billNumber="
					+ billNumber;
		} else {
			fooResourceUrl = "http://localhost:8090/user/creditnotecharge/findcreditnotedetails";
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;

	}

}
