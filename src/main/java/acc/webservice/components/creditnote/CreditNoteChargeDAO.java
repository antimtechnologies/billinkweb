package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_CHARGE_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;

@Lazy
@Repository
public class CreditNoteChargeDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteChargeMappingData(List<CreditNoteChargeModel> creditNoteChargeModel, int companyId, int creditNoteId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_CHARGE_DATA))
				.append(String.format(" ( %s, %s, %s, %s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE, CREDIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, CREDIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" %s, %s, %s, %s ) ", CREDIT_NOTE_CHARGE_DATA_COLUMN.I_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.S_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.C_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE, CREDIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, CREDIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s ) ", CREDIT_NOTE_CHARGE_DATA_COLUMN.I_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.S_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.C_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> creditNoteFrightMappingList = new ArrayList<>();
		Map<String, Object> creditNoteFrightMap;
		for (CreditNoteChargeModel creditNoteCharge : creditNoteChargeModel) {

			if (creditNoteCharge.getLedger() == null || creditNoteCharge.getLedger().getLedgerId() <= 0) {
				continue;
			}

			creditNoteFrightMap = new HashMap<>();
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE.toString(), creditNoteCharge.getCreditNoteCharge());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID.toString(), creditNoteCharge.getLedger().getLedgerId());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString(), creditNoteCharge.getDiscount());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE.toString(), creditNoteCharge.getTaxRate());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), creditNoteCharge.getTotalItemAmount());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString(), creditNoteCharge.getItemAmount());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.I_GST.toString(), creditNoteCharge.getIgst());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.S_GST.toString(), creditNoteCharge.getSgst());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.C_GST.toString(), creditNoteCharge.getCgst());
			creditNoteFrightMap.put(CREDIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), creditNoteCharge.isMeasureDiscountInAmount());

			creditNoteFrightMappingList.add(creditNoteFrightMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), creditNoteFrightMappingList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteChargeMapping(int companyId, int creditNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_CHARGE_DATA, CREDIT_NOTE_CHARGE_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, creditNoteId, companyId });
	}

	List<CreditNoteChargeModel> getCreditNoteChargeList(int companyId, int creditNoteId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		
		sqlQueryToFetchData.append(String.format("SELECT SFM.%s, SFM.%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE, CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_CHARGE_MAPPING_ID))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, CREDIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT, CREDIT_NOTE_CHARGE_DATA_COLUMN.I_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.S_GST))
			.append(String.format(" SFM.%s, SFM.%s, ", CREDIT_NOTE_CHARGE_DATA_COLUMN.C_GST, CREDIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
			.append(String.format(" LD.%s, LD.%s, LD.%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.SAC_CODE))
			.append(String.format(" FROM %s SFM ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_CHARGE_DATA))
			.append(String.format(" INNER JOIN %s LD ON SFM.%s = LD.%s AND LD.%s = 0", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, CREDIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.IS_DELETED))
			.append(String.format(" AND LD.%s = ? ", LEDGER_DETAILS_COLUMN.COMPANY_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
			.append(String.format(" WHERE SFM.%s = ? AND SFM.%s = 0 ", CREDIT_NOTE_CHARGE_DATA_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.IS_DELETED))
			.append(String.format(" AND SFM.%s = ? ", CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID, CREDIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().query(sqlQueryToFetchData.toString(), new Object[] { companyId, creditNoteId, companyId }, getCreditNoteChargeDetailsExtractor());

	}

	private ResultSetExtractor<List<CreditNoteChargeModel>> getCreditNoteChargeDetailsExtractor() {
		return new ResultSetExtractor<List<CreditNoteChargeModel>>() {
			@Override
			public List<CreditNoteChargeModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CreditNoteChargeModel> creditNoteChargeChargeList = new ArrayList<>();
				while (rs.next()) {
					creditNoteChargeChargeList.add(new CreditNoteChargeModel(rs));
				}
				return creditNoteChargeChargeList;
			}
		};
	}

}
