package acc.webservice.components.creditnote.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.creditnote.CreditNoteChargeModel;
import acc.webservice.components.creditnote.CreditNoteImageModel;
import acc.webservice.components.creditnote.CreditNoteItemMappingModel;
import acc.webservice.components.creditnote.CreditNoteSellMappingModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class CreditNoteModel {

	private int creditNoteId;
	private String imageURL;
	private double amount;
	private double grosssTotalAmount;
	private String description;
	private String creditNoteDate;
	private LedgerModel ledgerData;
	private boolean deleted;
	private boolean verified;
	private StatusModel status;
	private String addedDateTime;
	private String modifiedDateTime;
	private String verifiedDateTime;
	private String deletedDateTime;
	private UserModel addedBy;
	private UserModel modifiedBy;
	private String billNumber;
	private UserModel verifiedBy;
	private UserModel deletedBy;
	private double discount;
	private String billDate;
	private String poNo;
	private String lrNo;
	private String eWayBillNo;
	private boolean sellMeasureDiscountInAmount;
	private List<CreditNoteItemMappingModel> creditNoteItemMappingList;
	private List<CreditNoteSellMappingModel> creditNoteSellMappingList;
	private List<CreditNoteImageModel> imageURLList;
	private List<CreditNoteChargeModel> creditNoteChargeModelList;
	private int companyId;
	private String referenceNumber1;
	private String referenceNumber2;
	private String billNumberPrefix;
	private int currentBillNumber;
	private String cityName;
	private String gstNumber;
	private String originalInvoiceNumber;
	private String originalInvoiceDate;

	public CreditNoteModel() {
	}

	public CreditNoteModel(ResultSet rs) throws SQLException {
		setBillNumber(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString()));
		setCreditNoteId(rs.getInt(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString()));
		setDiscount(rs.getDouble(CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString()));
		setSellMeasureDiscountInAmount(
				rs.getBoolean(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		String imageURL = rs.getString(CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}

		setAmount(rs.getDouble(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString()));
		setGrosssTotalAmount(rs.getDouble(CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString()));
		setSellMeasureDiscountInAmount(
				rs.getBoolean(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setReferenceNumber1(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));
		setDescription(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString()));
		setAmount(rs.getDouble(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString()));
		setVerified(rs.getBoolean(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString()));
		setBillNumberPrefix(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString()));
		setCurrentBillNumber(rs.getInt(CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString()));
	
		setCityName(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString()));
		setGstNumber(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString()));
		setOriginalInvoiceNumber(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString()));
		setPoNo(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString()));
		setLrNo(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString()));
		seteWayBillNo(rs.getString(CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString()));
		
		Timestamp fetchedBillDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE.toString());
		if (fetchedBillDate != null) {
			setBillDate(DateUtility.converDateToUserString(fetchedBillDate));
		}
		
		Timestamp fetchedInvoiceDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString());
		if (fetchedInvoiceDate != null) {
			setOriginalInvoiceDate(DateUtility.converDateToUserString(fetchedInvoiceDate));
		}
		

		Timestamp fetchedCreditNoteDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE.toString());
		if (fetchedCreditNoteDate != null) {
			setCreditNoteDate(DateUtility.converDateToUserString(fetchedCreditNoteDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerfiedDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerfiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerfiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		LedgerModel fetchedLedgerData = new LedgerModel();
		fetchedLedgerData.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedgerData.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedgerData.setState(new StateModel(rs));
		setLedgerData(fetchedLedgerData);

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		setStatus(new StatusModel(rs));
	}

	public int getCreditNoteId() {
		return creditNoteId;
	}

	public void setCreditNoteId(int creditNoteId) {
		this.creditNoteId = creditNoteId;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCreditNoteDate() {
		return creditNoteDate;
	}

	public void setCreditNoteDate(String creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getOriginalInvoiceNumber() {
		return originalInvoiceNumber;
	}

	public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
		this.originalInvoiceNumber = originalInvoiceNumber;
	}

	public String getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(String originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<CreditNoteItemMappingModel> getCreditNoteItemMappingList() {
		return creditNoteItemMappingList;
	}

	public void setCreditNoteItemMappingList(List<CreditNoteItemMappingModel> creditNoteItemMappingList) {
		this.creditNoteItemMappingList = creditNoteItemMappingList;
	}

	public List<CreditNoteSellMappingModel> getCreditNoteSellMappingList() {
		return creditNoteSellMappingList;
	}

	public void setCreditNoteSellMappingList(List<CreditNoteSellMappingModel> creditNoteSellMappingList) {
		this.creditNoteSellMappingList = creditNoteSellMappingList;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public boolean isSellMeasureDiscountInAmount() {
		return sellMeasureDiscountInAmount;
	}

	public void setSellMeasureDiscountInAmount(boolean sellMeasureDiscountInAmount) {
		this.sellMeasureDiscountInAmount = sellMeasureDiscountInAmount;
	}

	public List<CreditNoteImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<CreditNoteImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public List<CreditNoteChargeModel> getCreditNoteChargeModelList() {
		return creditNoteChargeModelList;
	}

	public void setCreditNoteChargeModelList(List<CreditNoteChargeModel> creditNoteChargeModelList) {
		this.creditNoteChargeModelList = creditNoteChargeModelList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public double getGrosssTotalAmount() {
		return grosssTotalAmount;
	}

	public void setGrosssTotalAmount(double grosssTotalAmount) {
		this.grosssTotalAmount = grosssTotalAmount;
	}

	public String getBillNumberPrefix() {
		return billNumberPrefix;
	}

	public void setBillNumberPrefix(String billNumberPrefix) {
		this.billNumberPrefix = billNumberPrefix;
	}

	public int getCurrentBillNumber() {
		return currentBillNumber;
	}

	public void setCurrentBillNumber(int currentBillNumber) {
		this.currentBillNumber = currentBillNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public String geteWayBillNo() {
		return eWayBillNo;
	}

	public void seteWayBillNo(String eWayBillNo) {
		this.eWayBillNo = eWayBillNo;
	}

	@Override
	public String toString() {
		return "CreditNoteModel [creditNoteId=" + creditNoteId + ", ledgerData=" + ledgerData + ", imageURL=" + imageURL
				+ ", amount=" + amount + ", creditNoteDate=" + creditNoteDate + ", description=" + description
				+ ", statusModel=" + status + ", addedDateTime=" + addedDateTime + ", modifiedDateTime="
				+ modifiedDateTime + ", verifiedDateTime=" + verifiedDateTime + ", deletedBy=" + deletedBy
				+ ", deletedDateTime=" + deletedDateTime + ", isDeleted=" + deleted + ", isVerified=" + verified
				+ ", companyId=" + companyId + "]";
	}

}
