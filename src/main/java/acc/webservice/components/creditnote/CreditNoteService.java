package acc.webservice.components.creditnote;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class CreditNoteService {

	@Autowired
	private CreditNoteDAO creditNoteDAO;

	@Autowired
	private CreditNoteItemMappingDAO creditNoteItemMappingDAO;

	@Autowired
	private CreditNoteSellMappingDAO creditNoteSellMappingDAO;

	@Autowired
	private CreditNoteImageDAO creditNoteImageDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private CreditNoteChargeDAO creditNoteChargeDAO;

	public List<CreditNoteModel> getCreditNoteListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		return creditNoteDAO.getCreditNoteListData(companyId, start, numberOfRecord, filterParamter);
	}

	public CreditNoteModel getCreditNoteDetailsById(int companyId, int creditNoteId) {
		CreditNoteModel creditNoteModel = creditNoteDAO.getCreditNoteDetailsById(companyId, creditNoteId);
		creditNoteModel.setCreditNoteChargeModelList(creditNoteChargeDAO.getCreditNoteChargeList(companyId, creditNoteId));
		creditNoteModel.setImageURLList(creditNoteImageDAO.getCreditNoteImageList(companyId, creditNoteId));
		return creditNoteModel;
	}

	public CreditNoteModel getCreditNoteDetailsByBillNumber(int companyId, String billNumber) {
		CreditNoteModel creditNoteModel = creditNoteDAO.getCreditNoteDetailsByBillNumber(companyId, billNumber);
		creditNoteModel.setCreditNoteChargeModelList(creditNoteChargeDAO.getCreditNoteChargeList(companyId, creditNoteModel.getCreditNoteId()));
		creditNoteModel.setImageURLList(creditNoteImageDAO.getCreditNoteImageList(companyId, creditNoteModel.getCreditNoteId()));
		return creditNoteModel;
	}

	public CreditNoteModel publishCreditNoteData(CreditNoteModel creditNoteModel) {
		validateCreditNoteManupulateData(creditNoteModel);
		creditNoteDAO.publishCreditNoteData(creditNoteModel);
		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteItemMappingList()) > 0) {
			creditNoteItemMappingDAO.saveCreditNoteItemMappingData(creditNoteModel.getCreditNoteItemMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteSellMappingList()) > 0) {
			creditNoteSellMappingDAO.saveCreditNoteSellMappingData(creditNoteModel.getCreditNoteSellMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteChargeModelList()) > 0) {
			creditNoteChargeDAO.saveCreditNoteChargeMappingData(creditNoteModel.getCreditNoteChargeModelList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getImageURLList()) > 0) {
			creditNoteImageDAO.saveCreditNoteImageURLData(creditNoteModel.getImageURLList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}
		companyService.updateCompanyCredintNoteCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 2), creditNoteModel.getCompanyId());
		companyService.updateCompanyCredintNotePendingCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 0), creditNoteModel.getCompanyId());
		companyService.updateCompanyLastCreditNoteNumber(creditNoteModel.getCompanyId(), creditNoteDAO.getMaxBillNumber(creditNoteModel.getCompanyId()));
		
		return creditNoteModel;
	}

	public CreditNoteModel saveCreditNoteData(CreditNoteModel creditNoteModel) {
		validateCreditNoteManupulateData(creditNoteModel);
		creditNoteDAO.saveCreditNoteData(creditNoteModel);
		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteItemMappingList()) > 0) {
			creditNoteItemMappingDAO.saveCreditNoteItemMappingData(creditNoteModel.getCreditNoteItemMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteSellMappingList()) > 0) {
			creditNoteSellMappingDAO.saveCreditNoteSellMappingData(creditNoteModel.getCreditNoteSellMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteChargeModelList()) > 0) {
			creditNoteChargeDAO.saveCreditNoteChargeMappingData(creditNoteModel.getCreditNoteChargeModelList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getImageURLList()) > 0) {
			creditNoteImageDAO.saveCreditNoteImageURLData(creditNoteModel.getImageURLList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}
		
		companyService.updateCompanyLastCreditNoteNumber(creditNoteModel.getCompanyId(), creditNoteDAO.getMaxBillNumber(creditNoteModel.getCompanyId()));
 		

		return creditNoteModel;
	}

	public CreditNoteModel updatePublishedCreditNoteData(CreditNoteModel creditNoteModel) {
		validateCreditNoteManupulateData(creditNoteModel);
		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteItemMappingList()) > 0) {
			creditNoteItemMappingDAO.deleteCreditNoteItemMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteItemMappingDAO.saveCreditNoteItemMappingData(creditNoteModel.getCreditNoteItemMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteSellMappingList()) > 0) {
			creditNoteSellMappingDAO.deleteCreditNoteSellMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteSellMappingDAO.saveCreditNoteSellMappingData(creditNoteModel.getCreditNoteSellMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteChargeModelList()) > 0) {
			creditNoteChargeDAO.deleteCreditNoteChargeMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteChargeDAO.saveCreditNoteChargeMappingData(creditNoteModel.getCreditNoteChargeModelList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getImageURLList()) > 0) {
			creditNoteImageDAO.deleteCreditNoteImageURLData(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteImageDAO.saveCreditNoteImageURLData(creditNoteModel.getImageURLList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}
		creditNoteDAO.updatePublishedCreditNoteData(creditNoteModel);
		companyService.updateCompanyCredintNotePendingCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 0), creditNoteModel.getCompanyId());
		companyService.updateCompanyLastCreditNoteNumber(creditNoteModel.getCompanyId(), creditNoteDAO.getMaxBillNumber(creditNoteModel.getCompanyId()));
		return creditNoteModel;
	}

	public CreditNoteModel updateSavedCreditNoteData(CreditNoteModel creditNoteModel) {
		
		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteItemMappingList()) > 0) {
			creditNoteItemMappingDAO.deleteCreditNoteItemMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteItemMappingDAO.saveCreditNoteItemMappingData(creditNoteModel.getCreditNoteItemMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteSellMappingList()) > 0) {
			creditNoteSellMappingDAO.deleteCreditNoteSellMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteSellMappingDAO.saveCreditNoteSellMappingData(creditNoteModel.getCreditNoteSellMappingList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteChargeModelList()) > 0) {
			creditNoteChargeDAO.deleteCreditNoteChargeMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteChargeDAO.saveCreditNoteChargeMappingData(creditNoteModel.getCreditNoteChargeModelList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		if (ApplicationUtility.getSize(creditNoteModel.getImageURLList()) > 0) {
			creditNoteImageDAO.deleteCreditNoteImageURLData(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteImageDAO.saveCreditNoteImageURLData(creditNoteModel.getImageURLList(), creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
		}

		creditNoteModel = creditNoteDAO.updateSavedCreditNoteData(creditNoteModel);
		companyService.updateCompanyLastCreditNoteNumber(creditNoteModel.getCompanyId(), creditNoteDAO.getMaxBillNumber(creditNoteModel.getCompanyId()));
		return creditNoteModel;
	}

	public CreditNoteModel markCreditNoteAsVerified(CreditNoteModel creditNoteModel) {
		if (creditNoteModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			creditNoteItemMappingDAO.deleteCreditNoteItemMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteSellMappingDAO.deleteCreditNoteSellMapping(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteImageDAO.deleteCreditNoteImageURLData(creditNoteModel.getCompanyId(), creditNoteModel.getCreditNoteId());
			creditNoteDAO.markCreditNoteAsDeleteAndVerified(creditNoteModel);
		} else {			
			creditNoteDAO.markCreditNoteAsVerified(creditNoteModel);
		}
		companyService.updateCompanyCredintNotePendingCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 0), creditNoteModel.getCompanyId());
		return creditNoteModel;
	}

	public int deletePublishedCreditNoteData(int companyId, int creditNoteId, int deletedBy) {
		creditNoteDAO.deletePublishedCreditNoteData(companyId, creditNoteId, deletedBy);
		companyService.updateCompanyLastCreditNoteNumber(companyId, creditNoteDAO.getMaxBillNumber(companyId));
		return companyService.updateCompanyDebitNotePendingCount(creditNoteDAO.getPendingCreditNoteCountData(companyId, 0), companyId);
	}

	public int deleteSavedCreditNoteData(int companyId, int creditNoteId, int deletedBy) {
		creditNoteItemMappingDAO.deleteCreditNoteItemMapping(companyId, creditNoteId);
		creditNoteSellMappingDAO.deleteCreditNoteSellMapping(companyId, creditNoteId);
		creditNoteImageDAO.deleteCreditNoteImageURLData(companyId, creditNoteId);
		creditNoteDAO.deleteSavedCreditNoteData(companyId, creditNoteId, deletedBy);
		companyService.updateCompanyLastCreditNoteNumber(companyId, creditNoteDAO.getMaxBillNumber(companyId));
 		return companyService.updateCompanyCredintNotePendingCount(companyId, companyId);
	}

	public CreditNoteModel publishSavedCreditNote(CreditNoteModel creditNoteModel) {
		updateSavedCreditNoteData(creditNoteModel);
		creditNoteDAO.publishSavedCreditNote(creditNoteModel);
		companyService.updateCompanyCredintNoteCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 2), creditNoteModel.getCompanyId());
		companyService.updateCompanyCredintNotePendingCount(creditNoteDAO.getPendingCreditNoteCountData(creditNoteModel.getCompanyId(), 0), creditNoteModel.getCompanyId());
		return creditNoteModel;
	}

	private void validateCreditNoteManupulateData(CreditNoteModel creditNoteModel) {

		if (creditNoteModel.getLedgerData() == null || creditNoteModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save credit note data.");
		}

	}
}
