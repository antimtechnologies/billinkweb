package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class CreditNoteDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CreditNoteSellMappingDAO creditNoteSellMappingDAO;

	@SuppressWarnings("unchecked")
	List<CreditNoteModel> getCreditNoteListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {

		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData
				.append(String.format(" SELECT CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL, CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE,
						CREDIT_NOTE_DETAILS_COLUMN.AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" CND.%s, CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME, CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ",
						CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE))
				.append(String.format(" CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" FROM %s CND ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" INNER JOIN %s SM ON CND.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON CND.%s = LM.%s AND LM.%s = :%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON CND.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s MUM ON CND.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s DUM ON CND.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON CND.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" WHERE CND.%s = 0 ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND CND.%s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {
					sqlQueryToFetchData.append(
							String.format(" AND CND.%s IN (:statusList) ", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter,
						CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString());

				if (isVerified == 0 || isVerified == 1) {
					sqlQueryToFetchData.append(String.format(" AND CND.%s = :%s ",
							CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED, CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter,
						CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString());

				if (ledgerId > 0) {
					sqlQueryToFetchData.append(String.format(" AND CND.%s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
							CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString())) {
				String sellId = ApplicationUtility.getStrValue(filterParamter,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND (CND.%s LIKE :%s OR CND.%s LIKE :%s) ",
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID));
				parameters.put(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), "%" + sellId + "%");
			}

		}

		sqlQueryToFetchData.append(
				String.format(" ORDER BY CND.%s DESC LIMIT :%s, :%s ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						APPLICATION_GENERIC_ENUM.START, APPLICATION_GENERIC_ENUM.NO_OF_RECORD));
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getCreditNoteDetailsListExtractor());
	}

	private ResultSetExtractor<List<CreditNoteModel>> getCreditNoteDetailsListExtractor() {

		return new ResultSetExtractor<List<CreditNoteModel>>() {
			@Override
			public List<CreditNoteModel> extractData(ResultSet rs) throws SQLException {
				List<CreditNoteModel> creditList = new ArrayList<>();
				while (rs.next()) {
					creditList.add(new CreditNoteModel(rs));
				}
				return creditList;
			}
		};
	}

	CreditNoteModel getCreditNoteDetailsByBillNumber(int companyId, String billNumber) {
		StringBuilder sqlQueryToFetchData = getCreditNoteDetailQuery();
		sqlQueryToFetchData.append(String.format(" AND CND.%s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
				CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), billNumber);
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getCreditNoteDetailsExtractor(companyId));
	}

	CreditNoteModel getCreditNoteDetailsById(int companyId, int creditNoteId) {
		StringBuilder sqlQueryToFetchData = getCreditNoteDetailQuery();
		sqlQueryToFetchData.append(String.format(" AND CND.%s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
				CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getCreditNoteDetailsExtractor(companyId));
	}

	private StringBuilder getCreditNoteDetailQuery() {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData
				.append(String.format(" SELECT CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL, CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE,
						CREDIT_NOTE_DETAILS_COLUMN.AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ",
						CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" CND.%s, CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME, CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" CND.%s, CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE))
				.append(String.format(" CND.%s, CND.%s, ", CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" IM.%s, IM.%s, IM.%s, ", ITEM_DETAILS_COLUMN.HSN_CODE,
						ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME))
				.append(String.format(" CIM.%s, CIM.%s,CIM.%s,CIM.%s, ",
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ITEM_MAPPING_ID,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.SELL_RATE, CREDIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID))
				.append(String.format(" CIM.%s, CIM.%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" CIM.%s, CIM.%s, CIM.%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.C_GST,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE))
				.append(String.format(" CIM.%s, CIM.%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT))
				.append(String.format(" CIM.%s, CIM.%s, CIM.%s, CIM.%s ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.I_GST,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" FROM %s CND ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" INNER JOIN %s SM ON CND.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON CND.%s = LM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON CND.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s MUM ON CND.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s DUM ON CND.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON CND.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CIM ON CIM.%s = CND.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_ITEM_MAPPING,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND CIM.%s = 0 AND CIM.%s = :%s ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.IS_DELETED,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID, CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" LEFT JOIN %s IM ON IM.%s = CIM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS, ITEM_DETAILS_COLUMN.ITEM_ID,
						CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID))
				.append(String.format(" AND IM.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" WHERE CND.%s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND CND.%s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED));
		return sqlQueryToFetchData;
	}

	private ResultSetExtractor<CreditNoteModel> getCreditNoteDetailsExtractor(int companyId) {

		return new ResultSetExtractor<CreditNoteModel>() {
			@Override
			public CreditNoteModel extractData(ResultSet rs) throws SQLException {
				CreditNoteModel creditDetails = new CreditNoteModel();
				List<CreditNoteItemMappingModel> creditItemMappingList = new ArrayList<>();
				while (rs.next()) {
					if (creditDetails.getCreditNoteId() < 1) {
						creditDetails = new CreditNoteModel(rs);
						creditDetails.setCreditNoteSellMappingList(creditNoteSellMappingDAO
								.getCreditNoteSellMappingListForCreditNote(creditDetails.getCreditNoteId(), companyId));
					}

					if (rs.getInt(CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ITEM_MAPPING_ID.toString()) > 0) {
						creditItemMappingList.add(new CreditNoteItemMappingModel(rs));
					}
				}
				creditDetails.setCreditNoteItemMappingList(creditItemMappingList);
				return creditDetails;
			}
		};
	}

	CreditNoteModel publishCreditNoteData(CreditNoteModel creditNoteModel) {
		StringBuilder sqlQuery = getCreditNoteInsertQuery(creditNoteModel);

		if (!ApplicationUtility.isNullEmpty(creditNoteModel.getImageURL())) {
			creditNoteModel.setImageURL(creditNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), creditNoteModel.getImageURL())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), creditNoteModel.getDescription())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE.toString(), creditNoteModel.getCreditNoteDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), creditNoteModel.getBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(),
						creditNoteModel.getReferenceNumber1())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(),
						creditNoteModel.getReferenceNumber2())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(),
						creditNoteModel.getBillNumberPrefix())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						creditNoteModel.getCurrentBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(),
						creditNoteModel.getLedgerData().getLedgerId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), creditNoteModel.getAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						creditNoteModel.getGrosssTotalAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), creditNoteModel.getDiscount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						creditNoteModel.isSellMeasureDiscountInAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE.toString(), creditNoteModel.getBillDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString(), creditNoteModel.getPoNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString(), creditNoteModel.getLrNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), creditNoteModel.getCityName())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), creditNoteModel.getGstNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString(),
						creditNoteModel.getOriginalInvoiceDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString(),
						creditNoteModel.getOriginalInvoiceNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), creditNoteModel.geteWayBillNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY.toString(), creditNoteModel.getAddedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		creditNoteModel.setCreditNoteId(holder.getKey().intValue());
		return creditNoteModel;
	}

	CreditNoteModel saveCreditNoteData(CreditNoteModel creditNoteModel) {
		StringBuilder sqlQuery = getCreditNoteInsertQuery(creditNoteModel);

		if (!ApplicationUtility.isNullEmpty(creditNoteModel.getImageURL())) {
			creditNoteModel.setImageURL(creditNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), creditNoteModel.getImageURL())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), creditNoteModel.getDescription())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE.toString(), creditNoteModel.getCreditNoteDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(),
						creditNoteModel.getLedgerData().getLedgerId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), creditNoteModel.getBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(),
						creditNoteModel.getReferenceNumber1())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(),
						creditNoteModel.getReferenceNumber2())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(),
						creditNoteModel.getBillNumberPrefix())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						creditNoteModel.getCurrentBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), creditNoteModel.getAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						creditNoteModel.getGrosssTotalAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), creditNoteModel.getDiscount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						creditNoteModel.isSellMeasureDiscountInAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE.toString(), creditNoteModel.getBillDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString(), creditNoteModel.getPoNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString(), creditNoteModel.getLrNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), creditNoteModel.getCityName())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), creditNoteModel.getGstNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString(),
						creditNoteModel.getOriginalInvoiceDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString(),
						creditNoteModel.getOriginalInvoiceNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), creditNoteModel.geteWayBillNo())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY.toString(), creditNoteModel.getAddedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		creditNoteModel.setCreditNoteId(holder.getKey().intValue());
		return creditNoteModel;
	}

	private StringBuilder getCreditNoteInsertQuery(CreditNoteModel creditNoteModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format("( %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE))
				.append(String.format(" %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
				.append(String.format(" %s, %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s, %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" %s, %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE,
						CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" %s, %s, %s, %s,", CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" %s, %s, %s )", CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME, CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(" VALUES ")
				.append(String.format("( :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE))
				.append(String.format(" :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
				.append(String.format(" :%s, :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.AMOUNT, CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" :%s, :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" :%s, :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_DATE,
						CREDIT_NOTE_DETAILS_COLUMN.P_O_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.L_R_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" :%s, :%s,:%s, :%s, ", CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER, CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" :%s, :%s, :%s )", CREDIT_NOTE_DETAILS_COLUMN.ADDED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME, CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED));

		return sqlQuery;
	}

	CreditNoteModel updatePublishedCreditNoteData(CreditNoteModel creditNoteModel) {

		if (!ApplicationUtility.isNullEmpty(creditNoteModel.getImageURL())) {
			creditNoteModel.setImageURL(creditNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		StringBuilder sqlQuery = getUpdateCreditNoteQuery(creditNoteModel);

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), creditNoteModel.getImageURL())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), creditNoteModel.getDescription())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE.toString(), creditNoteModel.getCreditNoteDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(),
						creditNoteModel.getLedgerData().getLedgerId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), creditNoteModel.getBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(),
						creditNoteModel.getReferenceNumber1())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(),
						creditNoteModel.getReferenceNumber2())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(),
						creditNoteModel.getBillNumberPrefix())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						creditNoteModel.getCurrentBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), creditNoteModel.getAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						creditNoteModel.getGrosssTotalAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), creditNoteModel.getDiscount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						creditNoteModel.isSellMeasureDiscountInAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY.toString(),
						creditNoteModel.getModifiedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteModel.getCreditNoteId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		creditNoteModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return creditNoteModel;
	}

	private StringBuilder getUpdateCreditNoteQuery(CreditNoteModel creditNoteModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2,
						CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.AMOUNT))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED));
		return sqlQuery;
	}

	CreditNoteModel updateSavedCreditNoteData(CreditNoteModel creditNoteModel) {

		if (!ApplicationUtility.isNullEmpty(creditNoteModel.getImageURL())) {
			creditNoteModel.setImageURL(creditNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		StringBuilder sqlQuery = getUpdateCreditNoteQuery(creditNoteModel);

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), creditNoteModel.getImageURL())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), creditNoteModel.getDescription())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_DATE.toString(), creditNoteModel.getCreditNoteDate())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(),
						creditNoteModel.getLedgerData().getLedgerId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), creditNoteModel.getBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(),
						creditNoteModel.getReferenceNumber1())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(),
						creditNoteModel.getReferenceNumber2())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(),
						creditNoteModel.getBillNumberPrefix())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						creditNoteModel.getCurrentBillNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), creditNoteModel.getCityName())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), creditNoteModel.getGstNumber())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), creditNoteModel.getAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						creditNoteModel.getGrosssTotalAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), creditNoteModel.getDiscount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						creditNoteModel.isSellMeasureDiscountInAmount())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_BY.toString(),
						creditNoteModel.getModifiedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteModel.getCreditNoteId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		creditNoteModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return creditNoteModel;
	}

	int markCreditNoteAsVerified(CreditNoteModel creditNoteModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY.toString(),
						creditNoteModel.getVerifiedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteModel.getCreditNoteId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	int deletePublishedCreditNoteData(int companyId, int creditNoteId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deleteSavedCreditNoteData(int companyId, int creditNoteId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int markCreditNoteAsDeleteAndVerified(CreditNoteModel creditNoteModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s, ", CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_BY.toString(),
						creditNoteModel.getVerifiedBy().getUserId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteModel.getCreditNoteId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	CreditNoteModel publishSavedCreditNote(CreditNoteModel creditNoteModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format(" %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" WHERE %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID,
						CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteModel.getCreditNoteId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), creditNoteModel.getCompanyId())
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		creditNoteModel.setStatus(status);
		return creditNoteModel;
	}

	int getPendingCreditNoteCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ",
				CREDIT_NOTE_DETAILS_COLUMN.CREDIT_NOTE_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))
				.append(String.format("WHERE %s != :%s AND %s = 0", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID, CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", CREDIT_NOTE_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

	int getMaxBillNumber(int companyId) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("SELECT MAX(%s) AS maxBillNumber FROM %s ",
				CREDIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER, COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE))

				.append(String.format("WHERE %s = 0 ", CREDIT_NOTE_DETAILS_COLUMN.IS_DELETED))

				.append(String.format(" AND %s = :%s  ", CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID))

				.append(String.format(" AND %s != :%s  ", CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()

				.addValue(CREDIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)

				.addValue(CREDIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()));

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getMaxBillNumberExctractor());

	}

	private ResultSetExtractor<Integer> getMaxBillNumberExctractor() {

		return new ResultSetExtractor<Integer>() {

			@Override

			public Integer extractData(ResultSet rs) throws SQLException {

				int maxBillNumber = 0;

				while (rs.next()) {

					maxBillNumber = rs.getInt("maxBillNumber");

				}

				return maxBillNumber;

			}

		};

	}

}
