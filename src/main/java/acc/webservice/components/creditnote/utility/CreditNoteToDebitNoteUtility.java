package acc.webservice.components.creditnote.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.creditnote.CreditNoteChargeModel;
import acc.webservice.components.creditnote.CreditNoteImageModel;
import acc.webservice.components.creditnote.CreditNoteItemMappingModel;
import acc.webservice.components.creditnote.CreditNoteSellMappingModel;
import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.debitnote.DebitNoteChargeModel;
import acc.webservice.components.debitnote.DebitNoteImageModel;
import acc.webservice.components.debitnote.DebitNoteItemMappingModel;
import acc.webservice.components.debitnote.DebitNotePurchaseMappingModel;
import acc.webservice.components.debitnote.DebitNoteService;
import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.location.LocationModel;
import acc.webservice.components.location.LocationModelService;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.sell.SellService;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.sell.utility.SellToPurchaseUtility;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class CreditNoteToDebitNoteUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private DebitNoteService debitNoteService;

	@Autowired
	private SellService sellService;

	@Autowired
	private SellToPurchaseUtility sellToPurchaseUtility;

	@Autowired
	private LocationModelService locationService;

	public DebitNoteModel saveDebitNoteForSelectedLedger(CreditNoteModel creditNoteModel)
			throws AccountingSofwareException {
		DebitNoteModel debitNoteModel = new DebitNoteModel();

		if (ApplicationUtility.getSize(creditNoteModel.getBillNumber()) < 1) {
			return debitNoteModel;
		}

		LedgerModel ledgerBasicDetails = ledgerService.getBasicLedgerDetailByLedgerId(
				creditNoteModel.getLedgerData().getLedgerId(), creditNoteModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());
			CompanyModel debitNoteCompanyModel = companyService.getCompanyDetails(selectedLedgerCompanyId);

			if (debitNoteCompanyModel.isAllowB2BImport()) {
				debitNoteModel = createCreditNoteModelFromCreditNoteModel(creditNoteModel, debitNoteCompanyModel);
				debitNoteService.saveDebitNoteData(debitNoteModel);
			}
		}
	return debitNoteModel;

	}

	private DebitNoteModel createCreditNoteModelFromCreditNoteModel(CreditNoteModel creditNoteModel,
			CompanyModel selectedLedgerCompany) throws AccountingSofwareException {

		DebitNoteModel debitNoteModel = new DebitNoteModel();
		debitNoteModel.setCompanyId(selectedLedgerCompany.getCompanyId());
		debitNoteModel.setCurrentBillNumber(selectedLedgerCompany.getLastDebitNoteNumber() + 1);
		debitNoteModel.setBillNumberPrefix(selectedLedgerCompany.getBillNumberPrefix());
		debitNoteModel.setBillNumber("" + selectedLedgerCompany.getBillNumberPrefix() + debitNoteModel.getCurrentBillNumber());
		debitNoteModel.setReferenceNumber1(creditNoteModel.getBillNumber());
		debitNoteModel.setDiscount(creditNoteModel.getDiscount());
		debitNoteModel.setGrosssTotalAmount(creditNoteModel.getGrosssTotalAmount());
		debitNoteModel.setDebitNoteDate(creditNoteModel.getCreditNoteDate());
		debitNoteModel.setPurchaseMeasureDiscountInAmount(creditNoteModel.isSellMeasureDiscountInAmount());
		debitNoteModel.setAmount(creditNoteModel.getAmount());
		debitNoteModel.setImageURL(creditNoteModel.getImageURL());
		debitNoteModel.setDescription(creditNoteModel.getDescription());
		debitNoteModel.setPoNumber(creditNoteModel.getPoNo());
		debitNoteModel.setLrNumber(creditNoteModel.getLrNo());
		debitNoteModel.setEwayBillNumber(creditNoteModel.geteWayBillNo());
		debitNoteModel.setCityName(creditNoteModel.getCityName());
		debitNoteModel.setGstNumber(creditNoteModel.getGstNumber());

		CompanyModel sellCompanyDetail = companyService.getCompanyDetails(creditNoteModel.getCompanyId());
		LedgerModel sellLedgerData = new LedgerModel();
		int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompany.getCompanyId(), sellCompanyDetail.getGstNumber());
 		
		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompany.getCompanyId(), sellCompanyDetail);
	 		
			
		}
		sellLedgerData.setLedgerId(purchaseLedgerId);
		debitNoteModel.setLedgerData(sellLedgerData);

		List<DebitNoteImageModel> debitNoteImageURLList = new ArrayList<>();
		DebitNoteImageModel debitNoteImageModel;

		if (ApplicationUtility.getSize(creditNoteModel.getImageURLList()) > 0) {
			for (CreditNoteImageModel creditImageModel : creditNoteModel.getImageURLList()) {
				debitNoteImageModel = new DebitNoteImageModel();
				debitNoteImageModel.setImageURL(creditImageModel.getImageURL());
				debitNoteImageURLList.add(debitNoteImageModel);
			}
		}

		debitNoteModel.setImageURLList(debitNoteImageURLList);
		List<DebitNoteItemMappingModel> debitNoteItemList = new ArrayList<>();
		DebitNoteItemMappingModel debitNoteItemModel;

		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteItemMappingList()) > 0) {
			for (CreditNoteItemMappingModel creditNoteItemModel : creditNoteModel.getCreditNoteItemMappingList()) {
				if (creditNoteItemModel.getItem() == null || creditNoteItemModel.getItem().getItemId() <= 0) {
					continue;
				}
				debitNoteItemModel = getDebitNoteItemModelFromCreditNoteItemModel(creditNoteItemModel, creditNoteModel.getCompanyId(), selectedLedgerCompany.getCompanyId());
				debitNoteItemList.add(debitNoteItemModel);
			}
		}
		debitNoteModel.setDebitNoteItemMappingList(debitNoteItemList);

		List<DebitNoteChargeModel> debitNoteChargeList = new ArrayList<>();
		DebitNoteChargeModel debitNoteChargeModel;
		if (ApplicationUtility.getSize(creditNoteModel.getCreditNoteChargeModelList()) > 0) {
			for (CreditNoteChargeModel creditNoteChargeModel : creditNoteModel.getCreditNoteChargeModelList()) {
				if (creditNoteChargeModel.getLedger() == null || creditNoteChargeModel.getLedger().getLedgerId() <= 0) {
					continue;
				}
				debitNoteChargeModel = getDebitNoteChargeModelFromCreditNoteChargeModel(creditNoteChargeModel, creditNoteModel.getCompanyId(), selectedLedgerCompany.getCompanyId());
				debitNoteChargeList.add(debitNoteChargeModel);
			}
		}
		debitNoteModel.setDebitNoteChargeModelList(debitNoteChargeList);

		List<CreditNoteSellMappingModel> creditNoteSellMappingList = creditNoteModel.getCreditNoteSellMappingList();
		List<DebitNotePurchaseMappingModel> debitNotePurchaseMappingList = new ArrayList<>();
		SellModel sellModel = new SellModel();
		DebitNotePurchaseMappingModel debitNotePurchaseMappingModel;
		PurchaseModel purchaseModel;

		if (ApplicationUtility.getSize(creditNoteSellMappingList) > 0) {
			for (CreditNoteSellMappingModel creditNoteSellMappingModel : creditNoteSellMappingList) {
				debitNotePurchaseMappingModel = new DebitNotePurchaseMappingModel();
				sellModel = sellService.getSellDetailsById(creditNoteModel.getCompanyId(),
						creditNoteSellMappingModel.getSell().getSellId());

				if (sellModel.getSellDate() != null) {
					sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(),
							DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATE_FORMAT));
					sellModel.setDispatchDate(DateUtility.convertDateFormat(sellModel.getDispatchDate(),
							DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATE_FORMAT));
				}
				sellModel.setCompanyId(creditNoteModel.getCompanyId());
				sellToPurchaseUtility.savePurchaseForSelectedLedger(sellModel);
				purchaseModel = sellToPurchaseUtility.savePurchaseForSelectedLedger(sellModel);

				if (purchaseModel.getPurchaseId() > 0) {
					debitNotePurchaseMappingModel.setPurchase(purchaseModel);
					debitNotePurchaseMappingList.add(debitNotePurchaseMappingModel);
				}
			}
		}

		debitNoteModel.setDebitNotePurchaseMappingList(debitNotePurchaseMappingList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		debitNoteModel.setAddedBy(userData);

		return debitNoteModel;
	}

	private DebitNoteChargeModel getDebitNoteChargeModelFromCreditNoteChargeModel(
			CreditNoteChargeModel creditNoteChargeModel, int purchaseCompanyId, int sellCompanyId)
			throws AccountingSofwareException {
		DebitNoteChargeModel debitNoteChargeModel = new DebitNoteChargeModel();
		int ledgerId = ledgerService.getLedgerIdByName(sellCompanyId,
				creditNoteChargeModel.getLedger().getLedgerName());

		if (ledgerId < 1) {
			LedgerModel ledgerModel = ledgerService.getLedgerDetailsById(purchaseCompanyId,
					creditNoteChargeModel.getLedger().getLedgerId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			ledgerModel.setAddedBy(userData);
			ledgerModel.setLedgerDocumentList(null);
			ledgerModel.setOpeningBalance(0);
			ledgerModel.setCompanyId(sellCompanyId);
			ledgerId = ledgerService.saveLedgerData(ledgerModel).getLedgerId();
		}

		LedgerModel ledgerData = new LedgerModel();
		ledgerData.setLedgerId(ledgerId);

		debitNoteChargeModel.setLedger(ledgerData);
		debitNoteChargeModel.setCgst(creditNoteChargeModel.getCgst());
		debitNoteChargeModel.setDiscount(creditNoteChargeModel.getDiscount());
		debitNoteChargeModel.setIgst(creditNoteChargeModel.getIgst());
		debitNoteChargeModel.setItemAmount(creditNoteChargeModel.getItemAmount());
		debitNoteChargeModel.setMeasureDiscountInAmount(creditNoteChargeModel.isMeasureDiscountInAmount());
		debitNoteChargeModel.setDebitNoteCharge(creditNoteChargeModel.getCreditNoteCharge());
		debitNoteChargeModel.setSgst(creditNoteChargeModel.getSgst());
		debitNoteChargeModel.setTaxRate(creditNoteChargeModel.getTaxRate());
		debitNoteChargeModel.setTotalItemAmount(creditNoteChargeModel.getTotalItemAmount());
		return debitNoteChargeModel;
	}

	private DebitNoteItemMappingModel getDebitNoteItemModelFromCreditNoteItemModel(
			CreditNoteItemMappingModel creditNoteItemModel, int purchaseCompanyId, int sellCompanyId) {
		DebitNoteItemMappingModel debitNoteItemModel = new DebitNoteItemMappingModel();
		int itemId = itemService.getItemIdByName(sellCompanyId, creditNoteItemModel.getItem().getItemName());

		if (itemId < 1) {
			ItemModel itemModel = itemService.getItemDetailsById(purchaseCompanyId,
					creditNoteItemModel.getItem().getItemId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			itemModel.setAddedBy(userData);
			itemModel.setCompanyId(sellCompanyId);
			itemModel.setItemDocumentList(null);
			itemModel.setQuantity(0);
			itemModel.setBrand(null);
			itemModel.setCategory(null);

			itemId = itemService.saveItemData(itemModel).getItemId();
		}
		LocationModel locationModel = locationService.getLocationByCompanyLocationId(sellCompanyId,
				creditNoteItemModel.getLocationId());
		if (locationModel != null) {
			LocationModel locationSellData = locationService.chekcklocationUniqueInsert(purchaseCompanyId,
					locationModel);
			debitNoteItemModel.setLocationId(locationSellData.getLocationId());
		}
		ItemBatchModel batchModel = itemService.getOneBatch(creditNoteItemModel.getBatchId());
		if (batchModel != null) {
			ItemBatchModel batchSellData = itemService.checkuniqueandInsertBatch(batchModel, itemId);
			debitNoteItemModel.setBatchId(batchSellData.getBatchId());
		}
		ItemModel itemData = new ItemModel();
		itemData.setItemId(itemId);

		debitNoteItemModel.setItem(itemData);
		debitNoteItemModel.setCgst(creditNoteItemModel.getCgst());
		debitNoteItemModel.setDiscount(creditNoteItemModel.getDiscount());
		debitNoteItemModel.setIgst(creditNoteItemModel.getIgst());
		debitNoteItemModel.setItemAmount(creditNoteItemModel.getItemAmount());
		debitNoteItemModel.setMeasureDiscountInAmount(creditNoteItemModel.isMeasureDiscountInAmount());
		debitNoteItemModel.setPurchaseRate(creditNoteItemModel.getSellRate());
		debitNoteItemModel.setQuantity(creditNoteItemModel.getQuantity());
		debitNoteItemModel.setSgst(creditNoteItemModel.getSgst());
		debitNoteItemModel.setTaxRate(creditNoteItemModel.getTaxRate());
		debitNoteItemModel.setTotalItemAmount(creditNoteItemModel.getTotalItemAmount());
		debitNoteItemModel.setBatchId(creditNoteItemModel.getBatchId());
		return debitNoteItemModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail)
			throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
