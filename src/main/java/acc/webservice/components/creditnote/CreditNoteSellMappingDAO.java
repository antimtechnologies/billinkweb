package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_SELL_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;

@Repository
@Lazy
public class CreditNoteSellMappingDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteSellMappingData(List<CreditNoteSellMappingModel> creditNoteSellMappingList, int companyId, int creditNoteId) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_SELL_MAPPING))
				.append(String.format(" ( %s, %s, %s ) ", CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.SELL_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s ) ", CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.SELL_ID));

		List<Map<String, Object>> creditNoteSellList = new ArrayList<>();
		Map<String, Object> creditNoteSellMap;
		for (CreditNoteSellMappingModel creditNoteSellMapping : creditNoteSellMappingList) {
			creditNoteSellMap = new HashMap<>();
			creditNoteSellMap.put(CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
			creditNoteSellMap.put(CREDIT_NOTE_SELL_MAPPING_COLUMN.SELL_ID.toString(), creditNoteSellMapping.getSell().getSellId());
			creditNoteSellMap.put(CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
			creditNoteSellList.add(creditNoteSellMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), creditNoteSellList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteSellMapping(int companyId, int creditNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_SELL_MAPPING, CREDIT_NOTE_SELL_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ",CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, creditNoteId, companyId });
	}

	List<CreditNoteSellMappingModel> getCreditNoteSellMappingListForCreditNote(int creditNoteId, int companyId) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT CNSD.%s, SD.%s, SD.%s, ", CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_SELL_MAPPING_ID, SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" SD.%s, SD.%s, SD.%s, SD.%s ", SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_DETAILS_COLUMN.SELL_BILL_DATE, SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT))
				.append(String.format(" FROM %s CNSD ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_SELL_MAPPING))
				.append(String.format(" INNER JOIN %s SD ON SD.%s = CNSD.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS, SELL_DETAILS_COLUMN.SELL_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.SELL_ID))
				.append(String.format(" AND SD.%s = :%s AND SD.%s = 0 ", SELL_DETAILS_COLUMN.COMPANY_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" WHERE CNSD.%s = 0 AND CNSD.%s = :%s", CREDIT_NOTE_SELL_MAPPING_COLUMN.IS_DELETED, CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID))
				.append(String.format(" AND CNSD.%s = :%s", CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID, CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
		parameters.put(CREDIT_NOTE_SELL_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
 		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getCreditNoteSellDetailExctractor());
	}

	private ResultSetExtractor<List<CreditNoteSellMappingModel>> getCreditNoteSellDetailExctractor() {
		return new ResultSetExtractor<List<CreditNoteSellMappingModel>>() {
			@Override
			public List<CreditNoteSellMappingModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CreditNoteSellMappingModel> creditNoteSellList = new ArrayList<>();
				while (rs.next()) {
					creditNoteSellList.add(new CreditNoteSellMappingModel(rs));
				}

				return creditNoteSellList;
			}
		};
	}

	
}
