package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_SELL_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class CreditNoteSellMappingModel {

	private int creditNoteSellMappingId;
	private CreditNoteModel creditNote;
	private SellModel sell;
	private boolean isDeleted;

	public CreditNoteSellMappingModel() {
	}

	public CreditNoteSellMappingModel(ResultSet rs) throws SQLException {
		setCreditNoteSellMappingId(rs.getInt(CREDIT_NOTE_SELL_MAPPING_COLUMN.CREDIT_NOTE_SELL_MAPPING_ID.toString()));

		SellModel fetchedSell = new SellModel();
		fetchedSell.setSellId(rs.getInt(SELL_DETAILS_COLUMN.SELL_ID.toString()));
		fetchedSell.setBillNumber(rs.getString(SELL_DETAILS_COLUMN.BILL_NUMBER.toString()));
		fetchedSell.setTotalBillAmount(rs.getDouble(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));
		if (!ApplicationUtility.isNullEmpty(rs.getString(SELL_DETAILS_COLUMN.IMAGE_URL.toString()))) {			
			fetchedSell.setImageURL(ApplicationUtility.getServerURL() + rs.getString(SELL_DETAILS_COLUMN.IMAGE_URL.toString()));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(SELL_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		fetchedSell.setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedSellDate = rs.getTimestamp(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString());
		if (fetchedSellDate != null) {
			fetchedSell.setSellDate(DateUtility.converDateToUserString(fetchedSellDate));
		}

		setSell(fetchedSell);

	}
	public int getCreditNoteSellMappingId() {
		return creditNoteSellMappingId;
	}

	public void setCreditNoteSellMappingId(int creditNoteSellMappingId) {
		this.creditNoteSellMappingId = creditNoteSellMappingId;
	}

	public CreditNoteModel getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(CreditNoteModel creditNote) {
		this.creditNote = creditNote;
	}

	public SellModel getSell() {
		return sell;
	}

	public void setSell(SellModel sell) {
		this.sell = sell;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
