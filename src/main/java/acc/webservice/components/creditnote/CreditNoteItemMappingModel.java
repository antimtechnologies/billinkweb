package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;

public class CreditNoteItemMappingModel {

	private int creditNoteItemMappingId;
	private CreditNoteModel creditNote;
	private ItemModel item;
	private double quantity;
	private double sellRate;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private boolean measureDiscountInAmount;
	private int freequantity;
	private int freeItemId;
	private int locationId;
	private int batchId;
	private double additionaldiscount;
	private boolean addmeasureDiscountInAmount;

	private boolean isDeleted;

	public CreditNoteItemMappingModel() {
	}

	public CreditNoteItemMappingModel(ResultSet rs) throws SQLException {
		setCreditNoteItemMappingId(rs.getInt(CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ITEM_MAPPING_ID.toString()));
		setQuantity(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY.toString()));
		setSellRate(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.SELL_RATE.toString()));
		setTaxRate(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setDiscount(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(CREDIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		setAdditionaldiscount(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
		setAddmeasureDiscountInAmount(rs.getBoolean(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setItemAmount(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT.toString()));
		setIgst(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.S_GST.toString()));
		setCgst(rs.getDouble(CREDIT_NOTE_ITEM_MAPPING_COLUMN.C_GST.toString()));
		setLocationId(rs.getInt(CREDIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID.toString()));
		setBatchId(rs.getInt(CREDIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID.toString()));
		ItemModel fetchedItem = new ItemModel();
		fetchedItem.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
		fetchedItem.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
		fetchedItem.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
		setItem(fetchedItem);
	}

	public int getCreditNoteItemMappingId() {
		return creditNoteItemMappingId;
	}

	public void setCreditNoteItemMappingId(int creditNoteItemMappingId) {
		this.creditNoteItemMappingId = creditNoteItemMappingId;
	}

	public CreditNoteModel getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(CreditNoteModel creditNote) {
		this.creditNote = creditNote;
	}

	public ItemModel getItem() {
		return item;
	}

	public void setItem(ItemModel item) {
		this.item = item;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	

	public double getAdditionaldiscount() {
		return additionaldiscount;
	}

	public void setAdditionaldiscount(double additionaldiscount) {
		this.additionaldiscount = additionaldiscount;
	}

	public boolean isAddmeasureDiscountInAmount() {
		return addmeasureDiscountInAmount;
	}

	public void setAddmeasureDiscountInAmount(boolean addmeasureDiscountInAmount) {
		this.addmeasureDiscountInAmount = addmeasureDiscountInAmount;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public double getSellRate() {
		return sellRate;
	}

	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	
	public int getFreequantity() {
		return freequantity;
	}

	public void setFreequantity(int freequantity) {
		this.freequantity = freequantity;
	}

	public int getFreeItemId() {
		return freeItemId;
	}

	public void setFreeItemId(int freeItemId) {
		this.freeItemId = freeItemId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	@Override
	public String toString() {
		return "CreditNoteItemMappingModel [creditNoteItemMappingId=" + creditNoteItemMappingId + ", quantity="
				+ quantity + ", sellRate=" + sellRate + ", discount=" + discount + ", taxRate=" + taxRate
				+ ", totalItemAmount=" + totalItemAmount + ", itemAmount=" + itemAmount + ", cgst=" + cgst + ", sgst="
				+ sgst + ", igst=" + igst + ", measureDiscountInAmount=" + measureDiscountInAmount + ", isDeleted="
				+ isDeleted + "]";
	}

}
