package acc.webservice.components.creditnote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_ITEM_MAPPING_COLUMN;

@Repository
@Lazy
public class CreditNoteItemMappingDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteItemMappingData(List<CreditNoteItemMappingModel> creditNoteItemMappingList, int companyId, int creditNoteId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_ITEM_MAPPING))
				.append(String.format(" ( %s, %s, %s, %s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY, CREDIT_NOTE_ITEM_MAPPING_COLUMN.SELL_RATE, CREDIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" %s, %s, %s, %s, %s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT, CREDIT_NOTE_ITEM_MAPPING_COLUMN.I_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.C_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY, CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID))
				.append(String.format(" %s, %s, %s, %s, %s, %s )", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT, CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE, CREDIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT,CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,CREDIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY, CREDIT_NOTE_ITEM_MAPPING_COLUMN.SELL_RATE, CREDIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT, CREDIT_NOTE_ITEM_MAPPING_COLUMN.I_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.C_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, CREDIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY, CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s, :%s ) ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT, CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ID, CREDIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE, CREDIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT,CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,CREDIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> sellItemMappingList = new ArrayList<>();
		Map<String, Object> sellItemMap;
		for (CreditNoteItemMappingModel creditNoteItemMappingModel : creditNoteItemMappingList) {

			if (creditNoteItemMappingModel.getItem() == null || creditNoteItemMappingModel.getItem().getItemId() <= 0) {				
				continue;
			}

			sellItemMap = new HashMap<>();
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID.toString(), creditNoteItemMappingModel.getItem().getItemId());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY.toString(), creditNoteItemMappingModel.getQuantity());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.SELL_RATE.toString(), creditNoteItemMappingModel.getSellRate());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT.toString(), creditNoteItemMappingModel.getDiscount());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT.toString(), creditNoteItemMappingModel.getTotalItemAmount());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), creditNoteItemMappingModel.isMeasureDiscountInAmount());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE.toString(), creditNoteItemMappingModel.getTaxRate());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT.toString(), creditNoteItemMappingModel.getItemAmount());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.I_GST.toString(), creditNoteItemMappingModel.getIgst());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.S_GST.toString(), creditNoteItemMappingModel.getSgst());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.C_GST.toString(), creditNoteItemMappingModel.getCgst());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID.toString(), creditNoteItemMappingModel.getLocationId());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID.toString(), creditNoteItemMappingModel.getBatchId());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY.toString(), creditNoteItemMappingModel.getFreequantity());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID.toString(), creditNoteItemMappingModel.getFreeItemId());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ID.toString(), creditNoteId);
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString(), creditNoteItemMappingModel.getAdditionaldiscount());
			sellItemMap.put(CREDIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString(), creditNoteItemMappingModel.isAddmeasureDiscountInAmount());
			sellItemMappingList.add(sellItemMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellItemMappingList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteItemMapping(int companyId, int creditNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_CREDIT_NOTE_ITEM_MAPPING, CREDIT_NOTE_ITEM_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.CREDIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", CREDIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, creditNoteId, companyId });
	}

}
