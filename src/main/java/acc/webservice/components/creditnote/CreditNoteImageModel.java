package acc.webservice.components.creditnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class CreditNoteImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private CreditNoteModel creditNoteModel;

	public CreditNoteImageModel() {
	}

	public CreditNoteImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(CREDIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {			
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public CreditNoteModel getCreditNoteModel() {
		return creditNoteModel;
	}

	public void setCreditNoteModel(CreditNoteModel creditNoteModel) {
		this.creditNoteModel = creditNoteModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
