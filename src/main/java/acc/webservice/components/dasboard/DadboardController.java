package acc.webservice.components.dasboard;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import acc.webservice.components.ResponseTemplet.SetResponseTemplet;
import acc.webservice.components.ResponseTemplet.SetResponseTempletStatusCode;
import acc.webservice.components.utills.ResponseListTemplate;

@RestController
@RequestMapping("/dasboard/")
public class DadboardController {

	@Autowired
	ResponseListTemplate template;
	
	
	@Autowired
	SetResponseTemplet setStatus;
	
	@Autowired
	SetResponseTempletStatusCode setStatusCode;
	
	
	@PersistenceContext
	private EntityManager manager;
	
	
	//private String ip = "http://localhost:80/";
	
	//private String ip = "http://103.235.105.107:80/";
	
	private String ip = "http://103.233.25.43:80/reports/";
	
	@RequestMapping(value="getalltotalamount",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getDebitDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("type") != null && request.getParameter("time_period") != null && request.getParameter("error_code") != null) {
		   String type = request.getParameter("type");
			int day = Integer.parseInt(request.getParameter("time_period"));
			int errorcode = Integer.parseInt(request.getParameter("error_code"));
			fooResourceUrl  = ip+"/user/dashboard/findtotalamount?type="+type+"&time_period="+day+"&error_code="+errorcode;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	
//	@RequestMapping(value="/getalltotalamount",method = RequestMethod.GET)
//	public ResponseListTemplate getAllFeeBygrNo(HttpServletRequest request) {
//		if (request.getParameter("type") != null) {
//			if (request.getParameter("time_period") != null) {
//				int error_code = Integer.parseInt(request.getParameter("error_code"));
//				//int error_code = 7;
//				StoredProcedureQuery query = manager.createStoredProcedureQuery("gettotalamountofsale", GetTotalSale.class);
//				query.registerStoredProcedureParameter("_type", String.class, ParameterMode.IN);
//				query.registerStoredProcedureParameter("_time_period", Integer.class, ParameterMode.IN);
//				query.setParameter("_type", request.getParameter("type"));
//				query.setParameter("_time_period", Integer.parseInt(request.getParameter("time_period")));
//				List<?> list = query.getResultList();
//				if (list.size() != 0) {
//					template.setError(setStatusCode.SucessResponse("SuccessFully Access Record..!!",error_code));
//					template.setResult(list);
//					return template;
//				} else {
//					 template.setError(setStatus.FailsResponse("No Recored Found..!!"));
//			         template.setResult(null);
//			         return template;
//				}
//			} else {
//				 template.setError(setStatus.FailsResponse("Please Enter time period..!!"));
//		         template.setResult(null);
//		         return template;
//			}
//		} else {
//			template.setError(setStatus.FailsResponse("Please Enter type..!!"));
//	         template.setResult(null);
//	         return template;
//		}
//
//	}
}
