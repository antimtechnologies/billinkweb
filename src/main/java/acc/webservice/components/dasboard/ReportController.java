package acc.webservice.components.dasboard;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/report/")
public class ReportController {

	
	
//	private String ip = "http://103.233.24.74:80/";
	
//	private String ip = "http://103.235.105.107:80/";
	
//	private String ip = "http://103.233.24.74:80/";
	
   private String ip = "http://103.233.25.43:80/reports/";
	
	@RequestMapping(value="purchase",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchaseDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
			fooResourceUrl  = ip+"/user/purchase/?startDate="+startDate+"&endDate="+endDate;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="sale",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSaleDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
			fooResourceUrl  = ip+"/user/sales/?startDate="+startDate+"&endDate="+endDate;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="credit",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getCreditDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
			fooResourceUrl  = ip+"/user/creditnote/?startDate="+startDate+"&endDate="+endDate;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="debit",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getDebitDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
			fooResourceUrl  = ip+"/user/debitnote/?startDate="+startDate+"&endDate="+endDate;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="getledgercreditdebitdetail",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getAllCrditDebitLedgerDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null && request.getParameter("ledgerId") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
		  int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
			fooResourceUrl  = ip+"/user/ledger/getledgercreditdebitdetail?startDate="+startDate+"&endDate="+endDate+"&ledgerId="+ledgerId;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="gettodaycreditdebitdetail",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getTodayCrditDebitLedgerDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("ledgerId") != null) {
	     int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
	     System.err.println("ledger Id :"+request.getParameter("ledgerId"));
			fooResourceUrl  = ip+"/user/ledger/gettodayledgercreditdebitdetail?&ledgerId="+ledgerId;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	@RequestMapping(value="getAllLedgerName",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getLedgerNameDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
	//	if(request.getParameter("ledgerId") != null) {
		//  int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
			fooResourceUrl  = ip+"/user/ledger/getall";
		  //+ledgerId;
		//}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
//	@RequestMapping(value="allcreditledger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getAllCreditLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//		
//		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
//		  String startDate = request.getParameter("startDate");
//		  String endDate = request.getParameter("endDate");
//			fooResourceUrl  = ip+"/user/ledger/getallcredit?startDate="+startDate+"&endDate="+endDate;
//		}
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
//	
//	@RequestMapping(value="allcreditledger/today",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getCurrentCreditLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//			fooResourceUrl  = ip+"/user/ledger/getallcredit/today";
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
//	@RequestMapping(value="alldebitledger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getAllDebitLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//		
//		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
//		  String startDate = request.getParameter("startDate");
//		  String endDate = request.getParameter("endDate");
//			fooResourceUrl  = ip+"/user/ledger/getalldebit?startDate="+startDate+"&endDate="+endDate;
//		}
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
//	
//	@RequestMapping(value="alldebitledger/today",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getCurrentDayDebitLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//		
//		 	fooResourceUrl  = ip+"/user/ledger/getalldebit/today";
//		
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
//	
//	@RequestMapping(value="alltaxesdebitledger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getTaxesDebitLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//		
//		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
//		  String startDate = request.getParameter("startDate");
//		  String endDate = request.getParameter("endDate");
//			fooResourceUrl  = ip+"/user/taxesledger/getalldebitcharge?startDate="+startDate+"&endDate="+endDate;
//		}
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
//	
//	@RequestMapping(value="alltaxescreditledger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getTaxesCreditLedgerDetails(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl = null;
//		
//		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null) {
//		  String startDate = request.getParameter("startDate");
//		  String endDate = request.getParameter("endDate");
//			fooResourceUrl  = ip+"/user/taxesledger/getallcreditcharge?startDate="+startDate+"&endDate="+endDate;
//		}
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	
//	}
	@RequestMapping(value="getstockdetails",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getStockDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null && request.getParameter("companyId") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
		  int companyID = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl  = ip+"/user/dashboard/getstockdetails?startDate="+startDate+"&endDate="+endDate+"&companyId="+companyID;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	// created byBhadresh 6-8-19
	
	@RequestMapping(value="gettaxablereport",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> gettaxAbleReport(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("startDate") != null && request.getParameter("endDate") != null && request.getParameter("ledgerId") != null) {
		  String startDate = request.getParameter("startDate");
		  String endDate = request.getParameter("endDate");
		  int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
			fooResourceUrl  = ip+"/user/ledger/gettexable?startDate="+startDate+"&endDate="+endDate+"&ledgerId="+ledgerId;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}
	
	
}
