package acc.webservice.components.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_DOCUMENT_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class CompanyDocumentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<CompanyDocumentModel> getCompanyDocumentList(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s, %s ", COMPANY_DOCUMENT_COLUMN.DOCUMENT_URL_ID, COMPANY_DOCUMENT_COLUMN.DOCUMENT_URL, COMPANY_DOCUMENT_COLUMN.DOCUMENT_TYPE, COMPANY_DOCUMENT_COLUMN.DESCRIPTION))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_COMPANY_DOCUMENT_DETAILS))
			.append(String.format(" WHERE %s = :%s AND %s = 0  ", COMPANY_DOCUMENT_COLUMN.COMPANY_ID, COMPANY_DOCUMENT_COLUMN.COMPANY_ID, COMPANY_DOCUMENT_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(COMPANY_DOCUMENT_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCompanyDocumentResultExctractor());
	}

	private ResultSetExtractor<List<CompanyDocumentModel>> getCompanyDocumentResultExctractor() {
		return new ResultSetExtractor<List<CompanyDocumentModel>>() {
			@Override
			public List<CompanyDocumentModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CompanyDocumentModel> companyDocumentModelList = new ArrayList<>();
				while (rs.next()) {
					companyDocumentModelList.add(new CompanyDocumentModel(rs));
				}
				return companyDocumentModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveCompanyDocumentData(List<CompanyDocumentModel> companyDocumentModelList, int companyId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_COMPANY_DOCUMENT_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s ) ", COMPANY_DOCUMENT_COLUMN.DOCUMENT_TYPE, COMPANY_DOCUMENT_COLUMN.DESCRIPTION, COMPANY_DOCUMENT_COLUMN.DOCUMENT_URL, COMPANY_DOCUMENT_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", COMPANY_DOCUMENT_COLUMN.DOCUMENT_TYPE, COMPANY_DOCUMENT_COLUMN.DESCRIPTION, COMPANY_DOCUMENT_COLUMN.DOCUMENT_URL, COMPANY_DOCUMENT_COLUMN.COMPANY_ID));

		List<Map<String, Object>> companyDocumentURLMapList = new ArrayList<>();
		Map<String, Object> companyDocumentURLMap;
		for (CompanyDocumentModel companyDocumentModel : companyDocumentModelList) {
			companyDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(companyDocumentModel.getDocumentUrl())) {			
				companyDocumentModel.setDocumentUrl(companyDocumentModel.getDocumentUrl().replace(ApplicationUtility.getServerURL(), ""));
			}
			companyDocumentURLMap.put(COMPANY_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString(), companyDocumentModel.getDocumentType());
			companyDocumentURLMap.put(COMPANY_DOCUMENT_COLUMN.DESCRIPTION.toString(), companyDocumentModel.getDescription());
			companyDocumentURLMap.put(COMPANY_DOCUMENT_COLUMN.DOCUMENT_URL.toString(), companyDocumentModel.getDocumentUrl());
			companyDocumentURLMap.put(COMPANY_DOCUMENT_COLUMN.COMPANY_ID.toString(), companyId);
			companyDocumentURLMapList.add(companyDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), companyDocumentURLMapList.toArray(new HashMap[0]));
	}

	int deleteCompanyDocumentData(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", DATABASE_TABLE.ACC_COMPANY_DOCUMENT_DETAILS, COMPANY_DOCUMENT_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", COMPANY_DOCUMENT_COLUMN.COMPANY_ID, COMPANY_DOCUMENT_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, companyId });
	}

}
