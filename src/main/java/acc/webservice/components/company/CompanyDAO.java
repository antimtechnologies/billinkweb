package acc.webservice.components.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.state.StateModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_COMPANY_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.ROLES_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class CompanyDAO {

	private static final Logger logger = Logger.getLogger(CompanyDAO.class);

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	Integer getCompanyCount(String companyName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT COUNT(CD.%s) AS %s ", COMPANY_TABLE_COLUMN.COMPANY_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
						.append(String.format(" WHERE CD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), "%" + companyName + "%");

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getCompanyCount());
	}

	private ResultSetExtractor<Integer> getCompanyCount() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				int totalCount = 0;
				while(rs.next()) {
					totalCount = rs.getInt(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString());
				}
				return totalCount;
			}
		};
	}



	List<CompanyModel> getCompanyNameIdList(int start, int numberOfRecord, String companyName, UserModel userModel) {
		logger.debug("CompanyDAO.getCompanyNameIdList() -- method start");
		logger.debug("Parameters are { start : " + start + ", noOfRecord : " + numberOfRecord);
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT CD.%s, CD.%s ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" INNER JOIN %s UCM ON CD.%s = UCM.%s ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING, COMPANY_TABLE_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID));
		}

		sqlQueryToFetchData.append(String.format(" WHERE CD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" AND UCM.%s = 0 AND UCM.%s = :%s ", USER_COMPANY_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.USER_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID));
			parameters.put(USER_COMPANY_MAPPING_COLUMN.USER_ID.toString(), userModel.getUserId());
		}

		sqlQueryToFetchData.append(String.format(" ORDER BY CD.%s ASC ", COMPANY_TABLE_COLUMN.COMPANY_NAME));

		if (numberOfRecord > 0) {
			sqlQueryToFetchData.append(" LIMIT :start, :noOfRecord ");
		}

		logger.debug("sql query : " + sqlQueryToFetchData);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), "%" + companyName + "%");

		logger.debug("CompanyDAO.getCompanyNameIdList() -- method end");
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getCompanyNameIdData());
	}

	private ResultSetExtractor<List<CompanyModel>> getCompanyNameIdData() {

		return new ResultSetExtractor<List<CompanyModel>>() {
			@Override
			public List<CompanyModel> extractData(ResultSet rs) throws SQLException {
				List<CompanyModel> companyList = new ArrayList<>();
				CompanyModel companyData;
				while (rs.next()) {
					companyData = new CompanyModel();
					companyData.setCompanyId(rs.getInt(COMPANY_TABLE_COLUMN.COMPANY_ID.toString()));
					companyData.setCompanyName(rs.getString(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString()));
					companyList.add(companyData);
				}
				return companyList;
			}
		};
	}

	List<CompanyModel> getCompanyList(int start, int numberOfRecord, String companyName, UserModel userModel) {

		logger.debug("CompanyDAO.getCompanyList() -- method start");
		logger.debug("Parameters are { start : " + start + ", noOfRecord : " + numberOfRecord);
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.GST_NUMBER))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_TYPE, COMPANY_TABLE_COLUMN.MOBILE_NUMBER, COMPANY_TABLE_COLUMN.EMAIL_ID, COMPANY_TABLE_COLUMN.LANDLINE_NUMBER))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.PAN_NUMBER, COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.PIN_CODE, COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.ADDED_BY, COMPANY_TABLE_COLUMN.ADDED_DATE_TIME, COMPANY_TABLE_COLUMN.AADHAR_NUMBER, COMPANY_TABLE_COLUMN.CITY_NAME))
						.append(String.format(" CD.%s, CD.%s, CD.%s,CD.%s,CD.%s,CD.%s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT,COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER,COMPANY_TABLE_COLUMN.CITY_NAME,COMPANY_TABLE_COLUMN.EMAIL_ID))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER))
						.append(String.format(" SM.%s, SM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" UM.%s, UM.%s ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_NAME))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
						.append(String.format(" LEFT JOIN %s SM ON CD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, COMPANY_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" INNER JOIN %s UCM ON CD.%s = UCM.%s ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING, COMPANY_TABLE_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID));
		}

		sqlQueryToFetchData.append(String.format(" INNER JOIN %s UM ON CD.%s = UM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, COMPANY_TABLE_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE CD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s ) ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" AND UCM.%s = 0 AND UCM.%s = :%s ", USER_COMPANY_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.USER_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID));
			parameters.put(USER_COMPANY_MAPPING_COLUMN.USER_ID.toString(), userModel.getUserId());
		}

		sqlQueryToFetchData.append(String.format(" ORDER BY CD.%s ASC LIMIT :start, :noOfRecord ", COMPANY_TABLE_COLUMN.COMPANY_NAME));

		logger.debug("sql query : " + sqlQueryToFetchData);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), "%" + companyName + "%");

		logger.debug("CompanyDAO.getCompanyList() -- method end");
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getCompanyData());
	}

	private ResultSetExtractor<List<CompanyModel>> getCompanyData() {

		return new ResultSetExtractor<List<CompanyModel>>() {
			@Override
			public List<CompanyModel> extractData(ResultSet rs) throws SQLException {
				List<CompanyModel> companyList = new ArrayList<>();
				CompanyModel companyData;
				while (rs.next()) {
					companyData = new CompanyModel(rs);
					UserModel addedBy = new UserModel();
					addedBy.setUserId(rs.getInt(USER_TABLE_COLOUMN.USER_ID.toString()));
					addedBy.setUserName(rs.getString(USER_TABLE_COLOUMN.USER_NAME.toString()));
					companyData.setAddedBy(addedBy);
					companyList.add(companyData);
				}
				return companyList;
			}
		};
	}

	CompanyModel saveCompanyData(CompanyModel company) {
		StringBuilder sqlQueryCompanySave = new StringBuilder();
		sqlQueryCompanySave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format("( %s, %s, %s, ", COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.GST_NUMBER, COMPANY_TABLE_COLUMN.COMPANY_TYPE))
							.append(String.format(" %s, %s, %s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT))
							.append(String.format(" %s, %s, %s, %s, ", COMPANY_TABLE_COLUMN.MOBILE_NUMBER, COMPANY_TABLE_COLUMN.EMAIL_ID, COMPANY_TABLE_COLUMN.LANDLINE_NUMBER, COMPANY_TABLE_COLUMN.PAN_NUMBER))
							.append(String.format(" %s, %s, %s, %s, ", COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.PIN_CODE, COMPANY_TABLE_COLUMN.STATE_ID, COMPANY_TABLE_COLUMN.AADHAR_NUMBER))
							.append(String.format(" %s, %s, %s, %s, ", COMPANY_TABLE_COLUMN.ADDED_BY, COMPANY_TABLE_COLUMN.ADDED_DATE_TIME,COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL, COMPANY_TABLE_COLUMN.CITY_NAME))
							.append(String.format(" %s, %s,%s ) ", COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER,COMPANY_TABLE_COLUMN.SKU_PREFIX))
 							
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, ", COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.GST_NUMBER, COMPANY_TABLE_COLUMN.COMPANY_TYPE))
							.append(String.format(" :%s, :%s, :%s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT))
							.append(String.format(" :%s, :%s, :%s, :%s, ", COMPANY_TABLE_COLUMN.MOBILE_NUMBER, COMPANY_TABLE_COLUMN.EMAIL_ID, COMPANY_TABLE_COLUMN.LANDLINE_NUMBER, COMPANY_TABLE_COLUMN.PAN_NUMBER))
							.append(String.format(" :%s, :%s, :%s, :%s, ", COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.PIN_CODE, COMPANY_TABLE_COLUMN.STATE_ID, COMPANY_TABLE_COLUMN.AADHAR_NUMBER))
							.append(String.format(" :%s, :%s, :%s, :%s, ", COMPANY_TABLE_COLUMN.ADDED_BY, COMPANY_TABLE_COLUMN.ADDED_DATE_TIME,COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL, COMPANY_TABLE_COLUMN.CITY_NAME))
							.append(String.format(" :%s, :%s,:%s )", COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER,COMPANY_TABLE_COLUMN.SKU_PREFIX));
							 

		KeyHolder holder = new GeneratedKeyHolder();
		if (!ApplicationUtility.isNullEmpty(company.getProfilePhotoURL())) {			
			company.setProfilePhotoURL(company.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			company.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.COMPANY_PROFILE_PHOTO_DEF_URL.toString());
		}
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), company.getCompanyName())
				.addValue(COMPANY_TABLE_COLUMN.STATE_ID.toString(), company.getState().getStateId())
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_TYPE.toString(), company.getCompanyType())
				.addValue(COMPANY_TABLE_COLUMN.GST_NUMBER.toString(), company.getGstNumber())
				.addValue(COMPANY_TABLE_COLUMN.MOBILE_NUMBER.toString(), company.getMobileNumber())
				.addValue(COMPANY_TABLE_COLUMN.CITY_NAME.toString(), company.getCityName())
				.addValue(COMPANY_TABLE_COLUMN.LANDLINE_NUMBER.toString(), company.getLandlineNumber())
				.addValue(COMPANY_TABLE_COLUMN.EMAIL_ID.toString(), company.getEmailId())
				.addValue(COMPANY_TABLE_COLUMN.PAN_NUMBER.toString(), company.getPanNumber())
				.addValue(COMPANY_TABLE_COLUMN.OFFICE_ADDRESS.toString(), company.getOfficeAddress())
				.addValue(COMPANY_TABLE_COLUMN.AADHAR_NUMBER.toString(), company.getAadharNumber())
				.addValue(COMPANY_TABLE_COLUMN.PIN_CODE.toString(), company.getPinCode())
				.addValue(COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT.toString(), company.isAllowB2BImport())
				.addValue(COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString(), company.getLastBillNumber())
				.addValue(COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX.toString(), company.getBillNumberPrefix())
				.addValue(COMPANY_TABLE_COLUMN.ADDED_BY.toString(), company.getAddedBy().getUserId())
				.addValue(COMPANY_TABLE_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL.toString(), company.getProfilePhotoURL())
				.addValue(COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER.toString(), company.getLastCreditNoteNumber())
				.addValue(COMPANY_TABLE_COLUMN.SKU_PREFIX.toString(), company.getSkuprefix())
				.addValue(COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER.toString(), company.getLastDebitNoteNumber());
		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		company.setCompanyId(holder.getKey().intValue());
		return company;
	}

	CompanyModel updateCompanyData(CompanyModel company) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.GST_NUMBER, COMPANY_TABLE_COLUMN.GST_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.COMPANY_TYPE, COMPANY_TABLE_COLUMN.COMPANY_TYPE))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.MOBILE_NUMBER, COMPANY_TABLE_COLUMN.MOBILE_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.EMAIL_ID, COMPANY_TABLE_COLUMN.EMAIL_ID))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.LANDLINE_NUMBER, COMPANY_TABLE_COLUMN.LANDLINE_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.PAN_NUMBER, COMPANY_TABLE_COLUMN.PAN_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.OFFICE_ADDRESS))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.CITY_NAME, COMPANY_TABLE_COLUMN.CITY_NAME))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.PIN_CODE, COMPANY_TABLE_COLUMN.PIN_CODE))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.STATE_ID, COMPANY_TABLE_COLUMN.STATE_ID))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.AADHAR_NUMBER, COMPANY_TABLE_COLUMN.AADHAR_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL, COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER)) 	
							.append(String.format(" %s = :%s  ", COMPANY_TABLE_COLUMN.SKU_PREFIX,COMPANY_TABLE_COLUMN.SKU_PREFIX))
							.append(String.format(" WHERE %s = :%s  ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));

		if (!ApplicationUtility.isNullEmpty(company.getProfilePhotoURL())) {			
			company.setProfilePhotoURL(company.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			company.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.COMPANY_PROFILE_PHOTO_DEF_URL.toString());
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), company.getCompanyName())
				.addValue(COMPANY_TABLE_COLUMN.STATE_ID.toString(), company.getState().getStateId())
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_TYPE.toString(), company.getCompanyType())
				.addValue(COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT.toString(), company.isAllowB2BImport())
				.addValue(COMPANY_TABLE_COLUMN.GST_NUMBER.toString(), company.getGstNumber())
				.addValue(COMPANY_TABLE_COLUMN.CITY_NAME.toString(), company.getCityName())
				.addValue(COMPANY_TABLE_COLUMN.MOBILE_NUMBER.toString(), company.getMobileNumber())
				.addValue(COMPANY_TABLE_COLUMN.LANDLINE_NUMBER.toString(), company.getLandlineNumber())
				.addValue(COMPANY_TABLE_COLUMN.EMAIL_ID.toString(), company.getEmailId())
				.addValue(COMPANY_TABLE_COLUMN.AADHAR_NUMBER.toString(), company.getAadharNumber())
				.addValue(COMPANY_TABLE_COLUMN.PAN_NUMBER.toString(), company.getPanNumber())
				.addValue(COMPANY_TABLE_COLUMN.OFFICE_ADDRESS.toString(), company.getOfficeAddress())
				.addValue(COMPANY_TABLE_COLUMN.PIN_CODE.toString(), company.getPinCode())
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), company.getCompanyId())
				.addValue(COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString(), company.getLastBillNumber())
				.addValue(COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER.toString(), company.getLastDebitNoteNumber())
				.addValue(COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER.toString(), company.getLastCreditNoteNumber())				 
				.addValue(COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX.toString(), company.getBillNumberPrefix())
				.addValue(COMPANY_TABLE_COLUMN.SKU_PREFIX.toString(), company.getSkuprefix())
				.addValue(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0)
				.addValue(COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL.toString(), company.getProfilePhotoURL());

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return company;
	}

	int deleteCompanyData(int companyId, int deletedBy) {
		
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", COMPANY_TABLE_COLUMN.DELETED_BY, COMPANY_TABLE_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", COMPANY_TABLE_COLUMN.DELETED_DATE_TIME, COMPANY_TABLE_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID));
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 1)
				.addValue(COMPANY_TABLE_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(COMPANY_TABLE_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	CompanyModel getCompanyDetails(int companyId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.GST_NUMBER))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_TYPE, COMPANY_TABLE_COLUMN.MOBILE_NUMBER, COMPANY_TABLE_COLUMN.EMAIL_ID, COMPANY_TABLE_COLUMN.LANDLINE_NUMBER))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.PAN_NUMBER, COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.PIN_CODE, COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL, COMPANY_TABLE_COLUMN.CITY_NAME))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.ADDED_BY, COMPANY_TABLE_COLUMN.ADDED_DATE_TIME, COMPANY_TABLE_COLUMN.DELETED_BY, COMPANY_TABLE_COLUMN.DELETED_DATE_TIME, COMPANY_TABLE_COLUMN.AADHAR_NUMBER))
						.append(String.format(" CD.%s, CD.%s, CD.%s, CD.%s,", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT))
						.append(String.format(" CD.%s, CD.%s,CD.%s, ", COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER,COMPANY_TABLE_COLUMN.SKU_PREFIX))
 						.append(String.format(" SM.%s, SM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" UM.%s, UM.%s ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_NAME))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
						.append(String.format(" LEFT JOIN %s SM ON CD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, COMPANY_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" INNER JOIN %s UM ON CD.%s = UM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, COMPANY_TABLE_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE CD.%s = :%s", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getCompanyDetailsExctractor());
	}

	private ResultSetExtractor<CompanyModel> getCompanyDetailsExctractor() {

		return new ResultSetExtractor<CompanyModel>() {
			@Override
			public CompanyModel extractData(ResultSet rs) throws SQLException {
				CompanyModel companyDetails = new CompanyModel();
				while (rs.next()) {
					companyDetails = new CompanyModel(rs);
					UserModel addedBy = new UserModel();
					addedBy.setUserId(rs.getInt(USER_TABLE_COLOUMN.USER_ID.toString()));
					addedBy.setUserName(rs.getString(USER_TABLE_COLOUMN.USER_NAME.toString()));
					companyDetails.setAddedBy(addedBy);
				}
				return companyDetails;
			}
		};
	}

	public int updateCompanyCountData(int count, int companyId, COMPANY_TABLE_COLUMN columnName) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format(" %s = :%s ", columnName, columnName))
							.append(String.format(" WHERE %s = :%s", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0)
				.addValue(columnName.toString(), count);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	public int updateCompanyPurchasePendingCount(int purchasePendingCount, int companyId) {
		return updateCompanyCountData(purchasePendingCount, companyId, COMPANY_TABLE_COLUMN.PURCHASE_PENDING_COUNT);
	}

	public int updateCompanyPurchaseCount(int purchaseCount, int companyId) {
		return updateCompanyCountData(purchaseCount, companyId, COMPANY_TABLE_COLUMN.PURCHASE_PROCESS_COUNT);
	}

	public int updateCompanySellPendingCount(int sellPendingCount, int companyId) {
		return updateCompanyCountData(sellPendingCount, companyId, COMPANY_TABLE_COLUMN.SELL_PENDING_COUNT);
	}

	public int updateCompanySellCount(int sellCount, int companyId) {
		return updateCompanyCountData(sellCount, companyId, COMPANY_TABLE_COLUMN.SELL_PROCESS_COUNT);
	}

	public int updateCompanyExpensePendingCount(int expensePendingCount, int companyId) {
		return updateCompanyCountData(expensePendingCount, companyId, COMPANY_TABLE_COLUMN.EXPENSE_PENDING_COUNT);
	}

	public int updateCompanyExpenseCount(int expenseCount, int companyId) {
		return updateCompanyCountData(expenseCount, companyId, COMPANY_TABLE_COLUMN.EXPENSE_PROCESS_COUNT);
	}

	public int updateCompanyPurchasePaymentPendingCount(int paymentPendingCount, int companyId) {
		return updateCompanyCountData(paymentPendingCount, companyId, COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PENDING_COUNT);
	}

	public int updateCompanyPurchasePaymentCount(int paymentCount, int companyId) {
		return updateCompanyCountData(paymentCount, companyId, COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PROCESS_COUNT);
	}

	public int updateCompanySellPaymentPendingCount(int paymentPendingCount, int companyId) {
		return updateCompanyCountData(paymentPendingCount, companyId, COMPANY_TABLE_COLUMN.SELL_PAYMENT_PENDING_COUNT);
	}

	public int updateCompanySellPaymentCount(int count, int companyId) {
		return updateCompanyCountData(count, companyId, COMPANY_TABLE_COLUMN.SELL_PAYMENT_PROCESS_COUNT);
	}

	public int updateCompanyCredintNotePendingCount(int creditNotePendingCount, int companyId) {
		return updateCompanyCountData(creditNotePendingCount, companyId, COMPANY_TABLE_COLUMN.CREDIT_NOTE_PENDING_COUNT);
	}

	public int updateCompanyCredintNoteCount(int creditNoteCount, int companyId) {
		return updateCompanyCountData(creditNoteCount, companyId, COMPANY_TABLE_COLUMN.CREDIT_NOTE_PROCESS_COUNT);
	}

	public int updateCompanyDebitNotePendingCount(int debitNotePendingCount, int companyId) {
		return updateCompanyCountData(debitNotePendingCount, companyId, COMPANY_TABLE_COLUMN.DEBIT_NOTE_PENDING_COUNT);
	}

	public int updateCompanyDebitNoteCount(int debitNoteCount, int companyId) {
		return updateCompanyCountData(debitNoteCount, companyId, COMPANY_TABLE_COLUMN.DEBIT_NOTE_PROCESS_COUNT);
	}

	public List<CompanyModel> getCompanyRequestCountDetails(int start, int noOfRecord, String companyName, String sortParam, UserModel userModel) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.GST_NUMBER))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.PURCHASE_PENDING_COUNT, COMPANY_TABLE_COLUMN.PURCHASE_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.SELL_PENDING_COUNT, COMPANY_TABLE_COLUMN.SELL_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.EXPENSE_PENDING_COUNT, COMPANY_TABLE_COLUMN.EXPENSE_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.CREDIT_NOTE_PENDING_COUNT, COMPANY_TABLE_COLUMN.CREDIT_NOTE_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.DEBIT_NOTE_PENDING_COUNT, COMPANY_TABLE_COLUMN.DEBIT_NOTE_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PENDING_COUNT, COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.SELL_PAYMENT_PENDING_COUNT, COMPANY_TABLE_COLUMN.SELL_PAYMENT_PROCESS_COUNT))
						.append(String.format(" CD.%s, CD.%s, CD.%s,CD.%s,CD.%s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT,COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER,COMPANY_TABLE_COLUMN.SKU_PREFIX))
					    .append(String.format(" SM.%s, SM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" %s + %s + %s + %s + %s + %s + %s AS %s ", COMPANY_TABLE_COLUMN.PURCHASE_PENDING_COUNT, COMPANY_TABLE_COLUMN.SELL_PENDING_COUNT, 
								COMPANY_TABLE_COLUMN.CREDIT_NOTE_PENDING_COUNT, COMPANY_TABLE_COLUMN.DEBIT_NOTE_PENDING_COUNT, 
								COMPANY_TABLE_COLUMN.SELL_PAYMENT_PENDING_COUNT, COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PENDING_COUNT, 
								COMPANY_TABLE_COLUMN.EXPENSE_PENDING_COUNT, APPLICATION_GENERIC_ENUM.TOTAL_PENDING_COUNT))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON SM.%s = CD.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, COMPANY_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" INNER JOIN %s UCM ON CD.%s = UCM.%s ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING, COMPANY_TABLE_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID));
		}

		sqlQueryToFetchData.append(String.format(" WHERE CD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.COMPANY_NAME));

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userModel.getRole().getRoleName())) {
			sqlQueryToFetchData.append(String.format(" AND UCM.%s = 0 AND UCM.%s = :%s ", USER_COMPANY_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.USER_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID));
			parameters.put(USER_COMPANY_MAPPING_COLUMN.USER_ID.toString(), userModel.getUserId());
		}

		sqlQueryToFetchData.append(String.format(" ORDER BY %s DESC, %s ASC LIMIT :start, :noOfRecord ", "".equals(sortParam) ? APPLICATION_GENERIC_ENUM.TOTAL_PENDING_COUNT.toString() : sortParam, COMPANY_TABLE_COLUMN.COMPANY_NAME));

		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString(), "%" + companyName + "%");
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getCompanyRequestCountExctractor());
	}

	private ResultSetExtractor<List<CompanyModel>> getCompanyRequestCountExctractor() {
		return new ResultSetExtractor<List<CompanyModel>>() {
			@Override
			public List<CompanyModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CompanyModel> companyList = new ArrayList<>();
				CompanyModel companyModel;
				while(rs.next()) {

					companyModel = new CompanyModel();

					companyModel.setState(new StateModel(rs));
					companyModel.setCompanyId(rs.getInt(COMPANY_TABLE_COLUMN.COMPANY_ID.toString()));
					companyModel.setCompanyName(rs.getString(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString()));
					companyModel.setGstNumber(rs.getString(COMPANY_TABLE_COLUMN.GST_NUMBER.toString()));
					companyModel.setPurchasePendingCount(rs.getInt(COMPANY_TABLE_COLUMN.PURCHASE_PENDING_COUNT.toString()));
					companyModel.setPurchaseProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.PURCHASE_PROCESS_COUNT.toString()));
					companyModel.setPurchasePaymentPendingCount(rs.getInt(COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PENDING_COUNT.toString()));
					companyModel.setPurchasePaymentProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.PURCHASE_PAYMENT_PROCESS_COUNT.toString()));
					companyModel.setSellPendingCount(rs.getInt(COMPANY_TABLE_COLUMN.SELL_PENDING_COUNT.toString()));
					companyModel.setSellProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.SELL_PROCESS_COUNT.toString()));
					companyModel.setSellPaymentPendingCount(rs.getInt(COMPANY_TABLE_COLUMN.SELL_PAYMENT_PENDING_COUNT.toString()));
					companyModel.setSellPaymentProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.SELL_PAYMENT_PROCESS_COUNT.toString()));
					companyModel.setExpensePendingCount(rs.getInt(COMPANY_TABLE_COLUMN.EXPENSE_PENDING_COUNT.toString()));
					companyModel.setExpenseProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.EXPENSE_PROCESS_COUNT.toString()));
					companyModel.setDebitNotePendingCount(rs.getInt(COMPANY_TABLE_COLUMN.DEBIT_NOTE_PENDING_COUNT.toString()));
					companyModel.setDebitNoteProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.DEBIT_NOTE_PROCESS_COUNT.toString()));
					companyModel.setCreditNotePendingCount(rs.getInt(COMPANY_TABLE_COLUMN.CREDIT_NOTE_PENDING_COUNT.toString()));
					companyModel.setCreditNoteProcessCount(rs.getInt(COMPANY_TABLE_COLUMN.CREDIT_NOTE_PROCESS_COUNT.toString()));
					companyModel.setTotalPendingCount(rs.getInt(APPLICATION_GENERIC_ENUM.TOTAL_PENDING_COUNT.toString()));
					companyModel.setSkuprefix(rs.getString(COMPANY_TABLE_COLUMN.SKU_PREFIX.toString()));

					companyList.add(companyModel);
				}
				return companyList;
			}
		};
	}

	int updateCompanyBillNumber(int companyId, int lastBillNumber, String columnName) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format(" %s = :%s ", columnName, columnName))
							.append(String.format(" WHERE %s = :%s", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0)
				.addValue(columnName, lastBillNumber);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int updateCompanyLastItemNumber(int companyId, int lastItemNumber) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
							.append(String.format(" %s = :%s ", COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER, COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER))
							.append(String.format(" WHERE %s = :%s", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0)
				.addValue(COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER.toString(), lastItemNumber);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
	
	
	public CompanyModel getBillPrefixAndLastNumber(int companyId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT CD.%s, CD.%s, CD.%s, CD.%s,CD.%s,CD.%s ", COMPANY_TABLE_COLUMN.SKU_PREFIX,COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER, COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER,COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER))
 						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
						.append(String.format(" WHERE %s = :%s ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID));

		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getBillPrefixAndLastNumberExctractor(companyId));
	}

	private ResultSetExtractor<CompanyModel> getBillPrefixAndLastNumberExctractor(int companyId) {
		return new ResultSetExtractor<CompanyModel>() {
			@Override
			public CompanyModel extractData(ResultSet rs) throws SQLException {
				CompanyModel companyData = new CompanyModel();
				while (rs.next()) {
					companyData.setCompanyId(companyId);
					companyData.setBillNumberPrefix(rs.getString(COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX.toString()));
					companyData.setLastBillNumber(rs.getInt(COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString()));
					companyData.setLastItemNumber(rs.getInt(COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER.toString()));
					companyData.setLastCreditNoteNumber(rs.getInt(COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER.toString()));
					companyData.setLastDebitNoteNumber(rs.getInt(COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER.toString()));
					companyData.setSkuprefix(rs.getString(COMPANY_TABLE_COLUMN.SKU_PREFIX.toString()));
				}
				return companyData;
			}
		};
	}

	int getCompanyIdFromGSTNumber(String gstNumber) {
		int companyId = 0;
		try {
			StringBuilder query = new StringBuilder("SELECT ").append(COMPANY_TABLE_COLUMN.COMPANY_ID).append( " from ")
					.append(DATABASE_TABLE.ACC_COMPANY_DETAILS)
					.append(String.format(" WHERE %s = :%s AND %s = :%s ", COMPANY_TABLE_COLUMN.GST_NUMBER, COMPANY_TABLE_COLUMN.GST_NUMBER,
								COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));
			
			Map<String, Object> parameter = new HashMap<>();
			parameter.put(COMPANY_TABLE_COLUMN.GST_NUMBER.toString(), gstNumber);
			parameter.put(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0);
			companyId =  namedParameterJdbcTemplate.queryForObject(query.toString(), parameter, Integer.class);
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return companyId;
	}

	int isCompanyExistWithGSTNumber(String gstNumber, int companyId) {
		StringBuilder query = new StringBuilder(String.format("SELECT COUNT(%s) AS %s FROM ", COMPANY_TABLE_COLUMN.COMPANY_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
				.append(DATABASE_TABLE.ACC_COMPANY_DETAILS)
				.append(String.format(" WHERE %s = :%s", COMPANY_TABLE_COLUMN.GST_NUMBER, COMPANY_TABLE_COLUMN.GST_NUMBER))
				.append(String.format(" AND %s = :%s ", COMPANY_TABLE_COLUMN.IS_DELETED, COMPANY_TABLE_COLUMN.IS_DELETED));
		
		Map<String, Object> parameters = new HashMap<>(); 
		if (companyId > 0) {
			query.append(String.format(" AND %s != :%s ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID));
			parameters.put(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId);
		}

		parameters.put(COMPANY_TABLE_COLUMN.GST_NUMBER.toString(), gstNumber);
		parameters.put(COMPANY_TABLE_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(COMPANY_TABLE_COLUMN.IS_DELETED.toString(), 0);
		return namedParameterJdbcTemplate.query(query.toString(), parameters, getCompanyCount());
	}

}
