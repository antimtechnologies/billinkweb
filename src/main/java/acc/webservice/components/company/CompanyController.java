package acc.webservice.components.company;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.components.users.UserService;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.exception.model.AuthenticationException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/companydata/")
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "getCompanyNameIdList", method = RequestMethod.POST)
	public Map<String, Object> getCompanyNameIdList(@RequestBody Map<String, Object> requestData, HttpServletRequest request) throws AuthenticationException {
		int start = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String companyName = ApplicationUtility.getStrValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_NAME.toString());
		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());
		HttpSession session = request.getSession();

		if ("".equals(userName)) {
			userName = (String) session.getAttribute(USER_TABLE_COLOUMN.USER_NAME.toString());
		}

		if (ApplicationUtility.getSize(userName) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.getCompanyNameIdList(start, noOfRecord, companyName, userService.getBasicUserDetailForUserName(userName)));

		if (start == 0) {
			responseData.put(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString(), companyService.getCompanyCount(companyName));			
		}
		return responseData;
	}

	@RequestMapping(value = "getCompanyList", method = RequestMethod.POST)
	public Map<String, Object> getCompanyList(@RequestBody Map<String, Object> requestData, HttpServletRequest request) throws AuthenticationException {
		int start = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String companyName = ApplicationUtility.getStrValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_NAME.toString());
		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());
		HttpSession session = request.getSession();

		if ("".equals(userName)) {
			userName = (String) session.getAttribute(USER_TABLE_COLOUMN.USER_NAME.toString());
		}

		if (ApplicationUtility.getSize(userName) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.getCompanyList(start, noOfRecord, companyName, userService.getBasicUserDetailForUserName(userName)));
		if (start == 0) {
			responseData.put(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString(), companyService.getCompanyCount(companyName));			
		}
		return responseData;
	}

	@RequestMapping(value = "getCompanyDetails", method = RequestMethod.POST)
	public Map<String, Object> getCompanyDetails(@RequestBody Map<String, Object> requestData) {
		Map<String, Object> responseData = new HashMap<>();
		int companyId = ApplicationUtility.getIntValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		responseData.put("status", "success");
		responseData.put("data", companyService.getCompanyDetails(companyId));
		return responseData;
	}

	@RequestMapping(value = "saveCompanyData", method = RequestMethod.POST)
	public Map<String, Object> saveCompanyData(@RequestBody CompanyModel company) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.saveCompanyData(company));
		return responseData;
	}

	@RequestMapping(value = "updateCompanyData", method = RequestMethod.POST)
	public Map<String, Object> updateCompanyData(@RequestBody CompanyModel company) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.updateCompanyData(company));
		return responseData;
	}

	@RequestMapping(value = "deleteCompanyData", method = RequestMethod.POST)
	public Map<String, Object> deleteCompanyData(@RequestBody Map<String, Object> requestData) {
		int companyId = ApplicationUtility.getIntValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.deleteCompanyData(companyId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "getCompanyRequestCountDetails", method = RequestMethod.POST)
	public Map<String, Object> getCompanyRequestCountDetails(@RequestBody Map<String, Object> requestData, HttpServletRequest request) throws AuthenticationException {
		int start = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String companyName = ApplicationUtility.getStrValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_NAME.toString());
		String sortParam = ApplicationUtility.getStrValue(requestData, APPLICATION_GENERIC_ENUM.SORT_PARAM.toString());

		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());
		HttpSession session = request.getSession();

		if ("".equals(userName)) {
			userName = (String) session.getAttribute(USER_TABLE_COLOUMN.USER_NAME.toString());
		}

		if (ApplicationUtility.getSize(userName) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.getCompanyRequestCountDetails(start, noOfRecord, companyName, sortParam, userService.getBasicUserDetailForUserName(userName)));

		return responseData;
	}

	@RequestMapping(value = "getBillPrefixAndLastNumber", method = RequestMethod.POST)
	public Map<String, Object> getBillPrefixAndLastNumber(@RequestBody Map<String, Object> requestData, HttpServletRequest request) {
		int companyId = ApplicationUtility.getIntValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", companyService.getBillPrefixAndLastNumber(companyId));
		return responseData;
	}

}
