package acc.webservice.components.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.menu.MenuModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class CompanyModel {

	private int companyId;
	private String companyName;
	private String gstNumber;
	private List<String> businessIndustry;
	private String companyType;
	private String panNumber;
	private long mobileNumber;
	private String emailId;
	private String landlineNumber;
	private String officeAddress;
	private int pinCode;
	private StateModel state;
	private List<UserModel> userList;
	private String profilePhotoURL;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel deletedBy;
	private String deleteDateTime;
	private List<MenuModel> userCompanyMenuList;
	private int purchasePendingCount;
	private int purchaseProcessCount;
	private int sellPendingCount;
	private int sellProcessCount;
	private int creditNotePendingCount;
	private int creditNoteProcessCount;
	private int debitNotePendingCount;
	private int debitNoteProcessCount;
	private int expensePendingCount;
	private int expenseProcessCount;
	private int purchasePaymentPendingCount;
	private int purchasePaymentProcessCount;
	private int sellPaymentPendingCount;
	private int sellPaymentProcessCount;
	private int totalPendingCount;
	private long aadharNumber;
	private boolean allowB2BImport;
	private String cityName;
	private List<CompanyDocumentModel> companyDocumentList;
	private String billNumberPrefix;
	private int lastBillNumber, lastItemNumber;
	private int lastDebitNoteNumber;
	private int lastCreditNoteNumber;
	private String skuprefix;
	
	public CompanyModel() {

	}

	public CompanyModel(ResultSet resultSet) throws SQLException {
		setCompanyId(resultSet.getInt(COMPANY_TABLE_COLUMN.COMPANY_ID.toString()));
		setCityName(resultSet.getString(COMPANY_TABLE_COLUMN.CITY_NAME.toString()));
		setCompanyName(resultSet.getString(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString()));
		setGstNumber(resultSet.getString(COMPANY_TABLE_COLUMN.GST_NUMBER.toString()));
		setCompanyType(resultSet.getString(COMPANY_TABLE_COLUMN.COMPANY_TYPE.toString()));
		setMobileNumber(resultSet.getLong(COMPANY_TABLE_COLUMN.MOBILE_NUMBER.toString()));
		setEmailId(resultSet.getString(COMPANY_TABLE_COLUMN.EMAIL_ID.toString()));
		setLandlineNumber(resultSet.getString(COMPANY_TABLE_COLUMN.LANDLINE_NUMBER.toString()));
		setPanNumber(resultSet.getString(COMPANY_TABLE_COLUMN.PAN_NUMBER.toString()));
		setOfficeAddress(resultSet.getString(COMPANY_TABLE_COLUMN.OFFICE_ADDRESS.toString()));
		setPinCode(resultSet.getInt(COMPANY_TABLE_COLUMN.PIN_CODE.toString()));
		setProfilePhotoURL(ApplicationUtility.getServerURL()+resultSet.getString(COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL.toString()));
		setAadharNumber(resultSet.getLong(COMPANY_TABLE_COLUMN.AADHAR_NUMBER.toString()));
		Timestamp fetchedAddedDateTime = resultSet.getTimestamp(COMPANY_TABLE_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDateTime));

		setBillNumberPrefix(resultSet.getString(COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX.toString()));
		setLastBillNumber(resultSet.getInt(COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString()));
		setLastItemNumber(resultSet.getInt(COMPANY_TABLE_COLUMN.LAST_ITEM_NUMBER.toString()));
		setLastCreditNoteNumber(resultSet.getInt(COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER.toString()));
		setLastDebitNoteNumber(resultSet.getInt(COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER.toString()));
		 		
		setState(new StateModel(resultSet));
		setAllowB2BImport(resultSet.getBoolean(COMPANY_TABLE_COLUMN.ALLOW_B2B_IMPORT.toString()));
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public List<String> getBusinessIndustry() {
		return businessIndustry;
	}

	public void setBusinessIndustry(List<String> businessIndustry) {
		this.businessIndustry = businessIndustry;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public StateModel getState() {
		return state;
	}

	public void setState(StateModel state) {
		this.state = state;
	}

	public List<UserModel> getUserList() {
		return userList;
	}

	public void setUserList(List<UserModel> userList) {
		this.userList = userList;
	}

	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}

	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeleteDateTime() {
		return deleteDateTime;
	}

	public void setDeleteDateTime(String deleteDateTime) {
		this.deleteDateTime = deleteDateTime;
	}

	public List<MenuModel> getUserCompanyMenuList() {
		return userCompanyMenuList;
	}

	public void setUserCompanyMenuList(List<MenuModel> userCompanyMenuList) {
		this.userCompanyMenuList = userCompanyMenuList;
	}

	public int getPurchasePendingCount() {
		return purchasePendingCount;
	}

	public void setPurchasePendingCount(int purchasePendingCount) {
		this.purchasePendingCount = purchasePendingCount;
	}

	public int getSellPendingCount() {
		return sellPendingCount;
	}

	public void setSellPendingCount(int sellPendingCount) {
		this.sellPendingCount = sellPendingCount;
	}

	public int getCreditNotePendingCount() {
		return creditNotePendingCount;
	}

	public void setCreditNotePendingCount(int creditNotePendingCount) {
		this.creditNotePendingCount = creditNotePendingCount;
	}

	public int getDebitNotePendingCount() {
		return debitNotePendingCount;
	}

	public void setDebitNotePendingCount(int debitNotePendingCount) {
		this.debitNotePendingCount = debitNotePendingCount;
	}

	public int getExpensePendingCount() {
		return expensePendingCount;
	}

	public void setExpensePendingCount(int expensePendingCount) {
		this.expensePendingCount = expensePendingCount;
	}

	public int getPurchasePaymentPendingCount() {
		return purchasePaymentPendingCount;
	}

	public void setPurchasePaymentPendingCount(int purchasePaymentPendingCount) {
		this.purchasePaymentPendingCount = purchasePaymentPendingCount;
	}

	public int getSellPaymentPendingCount() {
		return sellPaymentPendingCount;
	}

	public void setSellPaymentPendingCount(int sellPaymentPendingCount) {
		this.sellPaymentPendingCount = sellPaymentPendingCount;
	}

	public int getPurchaseProcessCount() {
		return purchaseProcessCount;
	}

	public void setPurchaseProcessCount(int purchaseProcessCount) {
		this.purchaseProcessCount = purchaseProcessCount;
	}

	public int getSellProcessCount() {
		return sellProcessCount;
	}

	public void setSellProcessCount(int sellProcessCount) {
		this.sellProcessCount = sellProcessCount;
	}

	public int getCreditNoteProcessCount() {
		return creditNoteProcessCount;
	}

	public void setCreditNoteProcessCount(int creditNoteProcessCount) {
		this.creditNoteProcessCount = creditNoteProcessCount;
	}

	public int getDebitNoteProcessCount() {
		return debitNoteProcessCount;
	}

	public void setDebitNoteProcessCount(int debitNoteProcessCount) {
		this.debitNoteProcessCount = debitNoteProcessCount;
	}

	public int getExpenseProcessCount() {
		return expenseProcessCount;
	}

	public void setExpenseProcessCount(int expenseProcessCount) {
		this.expenseProcessCount = expenseProcessCount;
	}

	public int getPurchasePaymentProcessCount() {
		return purchasePaymentProcessCount;
	}

	public void setPurchasePaymentProcessCount(int purchasePaymentProcessCount) {
		this.purchasePaymentProcessCount = purchasePaymentProcessCount;
	}

	public int getSellPaymentProcessCount() {
		return sellPaymentProcessCount;
	}

	public void setSellPaymentProcessCount(int sellPaymentProcessCount) {
		this.sellPaymentProcessCount = sellPaymentProcessCount;
	}

	public int getTotalPendingCount() {
		return totalPendingCount;
	}

	public void setTotalPendingCount(int totalPendingCount) {
		this.totalPendingCount = totalPendingCount;
	}

	public long getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(long aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public List<CompanyDocumentModel> getCompanyDocumentList() {
		return companyDocumentList;
	}

	public void setCompanyDocumentList(List<CompanyDocumentModel> companyDocumentList) {
		this.companyDocumentList = companyDocumentList;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getBillNumberPrefix() {
		return billNumberPrefix;
	}

	public void setBillNumberPrefix(String billNumberPrefix) {
		this.billNumberPrefix = billNumberPrefix;
	}

	public int getLastBillNumber() {
		return lastBillNumber;
	}

	public void setLastBillNumber(int lastBillNumber) {
		this.lastBillNumber = lastBillNumber;
	}

	public boolean isAllowB2BImport() {
		return allowB2BImport;
	}

	public void setAllowB2BImport(boolean allowB2BImport) {
		this.allowB2BImport = allowB2BImport;
	}

	public int getLastItemNumber() {
		return lastItemNumber;
	}

	public void setLastItemNumber(int lastItemNumber) {
		this.lastItemNumber = lastItemNumber;
	}

	public int getLastDebitNoteNumber() {
		return lastDebitNoteNumber;
	}

	public void setLastDebitNoteNumber(int lastDebitNoteNumber) {
		this.lastDebitNoteNumber = lastDebitNoteNumber;
	}

	public int getLastCreditNoteNumber() {
		return lastCreditNoteNumber;
	}

	public void setLastCreditNoteNumber(int lastCreditNoteNumber) {
		this.lastCreditNoteNumber = lastCreditNoteNumber;
	}
	
	

	public String getSkuprefix() {
		return skuprefix;
	}

	public void setSkuprefix(String skuprefix) {
		this.skuprefix = skuprefix;
	}



	@Override
	public String toString() {
		return "CompanyModel [companyId=" + companyId + ", companyName=" + companyName + ", gstNumber=" + gstNumber
				+ ", businessIndustry=" + businessIndustry + ", companyType=" + companyType + ", panNumber=" + panNumber
				+ ", mobileNumber=" + mobileNumber + ", emailId=" + emailId + ", landlineNumber=" + landlineNumber
				+ ", officeAddress=" + officeAddress + ", pinCode=" + pinCode + ", state=" + state + ", userList="
				+ userList + ", profilePhotoURL=" + profilePhotoURL + ", addedBy=" + addedBy + ", addedDateTime="
				+ addedDateTime + ", deletedBy=" + deletedBy + ", deleteDateTime=" + deleteDateTime
				+ ", userCompanyMenuList=" + userCompanyMenuList + ", purchasePendingCount=" + purchasePendingCount
				+ ", purchaseProcessCount=" + purchaseProcessCount + ", sellPendingCount=" + sellPendingCount
				+ ", sellProcessCount=" + sellProcessCount + ", creditNotePendingCount=" + creditNotePendingCount
				+ ", creditNoteProcessCount=" + creditNoteProcessCount + ", debitNotePendingCount="
				+ debitNotePendingCount + ", debitNoteProcessCount=" + debitNoteProcessCount + ", expensePendingCount="
				+ expensePendingCount + ", expenseProcessCount=" + expenseProcessCount
				+ ", purchasePaymentPendingCount=" + purchasePaymentPendingCount + ", purchasePaymentProcessCount="
				+ purchasePaymentProcessCount + ", sellPaymentPendingCount=" + sellPaymentPendingCount
				+ ", sellPaymentProcessCount=" + sellPaymentProcessCount + ", totalPendingCount=" + totalPendingCount
				+ ", aadharNumber=" + aadharNumber + ", allowB2BImport=" + allowB2BImport + ", cityName=" + cityName
				+ ", companyDocumentList=" + companyDocumentList + ", billNumberPrefix=" + billNumberPrefix
				+ ", lastBillNumber=" + lastBillNumber + ", lastItemNumber=" + lastItemNumber + ", lastDebitNoteNumber="
				+ lastDebitNoteNumber + ", lastCreditNoteNumber=" + lastCreditNoteNumber + ", skuprefix=" + skuprefix
				+ "]";
	}

}
