package acc.webservice.components.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.users.UserModel;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.exception.model.AuthenticationException;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class CompanyService {

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private CompanyDocumentDAO companyDocumentDAO;

	@Autowired
	private CompanyBusinessIndustryDAO companyBusinessIndustryDAO;

	public Integer getCompanyCount(String companyName) {
		return companyDAO.getCompanyCount(companyName);
	}

	public List<CompanyModel> getCompanyList(int start, int numberOfRecord, String companyName, UserModel userModel) throws AuthenticationException {

		if (userModel == null || userModel.getRole() == null || ApplicationUtility.getSize(userModel.getRole().getRoleName()) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}
		return companyDAO.getCompanyList(start, numberOfRecord, companyName, userModel);
	}

	public CompanyModel getCompanyDetails(int companyId) {
		CompanyModel companyModel = companyDAO.getCompanyDetails(companyId);
		companyModel.setCompanyDocumentList(companyDocumentDAO.getCompanyDocumentList(companyId));
		companyModel.setBusinessIndustry(companyBusinessIndustryDAO.getBusinessIndustryValueList(companyId));
		return companyModel;
	}

	public CompanyModel saveCompanyData(CompanyModel company) throws AccountingSofwareException {
		
		if (isCompanyExistWithGSTNumber(company.getGstNumber(), company.getCompanyId())) {
			throw new AccountingSofwareException("Company with same GST Number is already exist.");
		}

		companyDAO.saveCompanyData(company);
		if (ApplicationUtility.getSize(company.getCompanyDocumentList()) > 0) {
			companyDocumentDAO.saveCompanyDocumentData(company.getCompanyDocumentList(), company.getCompanyId());
		}
		
		if (ApplicationUtility.getSize(company.getBusinessIndustry()) > 0) {
			companyBusinessIndustryDAO.saveBusinessIndustryData(company.getBusinessIndustry(), company.getCompanyId());
		}
		return company;
	}

	private boolean isCompanyExistWithGSTNumber(String gstNumber, int companyId) {
		
		if (ApplicationUtility.getSize(gstNumber) < 1) {
			return false;
		}

		return companyDAO.isCompanyExistWithGSTNumber(gstNumber, companyId) > 0;
	}

	public CompanyModel updateCompanyData(CompanyModel company) throws AccountingSofwareException {
		if (isCompanyExistWithGSTNumber(company.getGstNumber(), company.getCompanyId())) {
			throw new AccountingSofwareException("Company with same GST Number is already exist.");
		}

		companyDocumentDAO.deleteCompanyDocumentData(company.getCompanyId());
		if (ApplicationUtility.getSize(company.getCompanyDocumentList()) > 0) {
			companyDocumentDAO.saveCompanyDocumentData(company.getCompanyDocumentList(), company.getCompanyId());
		}
		
		if (ApplicationUtility.getSize(company.getBusinessIndustry()) > 0) {
			companyBusinessIndustryDAO.deleteBusinessIndustryData(company.getCompanyId());
			companyBusinessIndustryDAO.saveBusinessIndustryData(company.getBusinessIndustry(), company.getCompanyId());
		}
		return companyDAO.updateCompanyData(company);
	}

	public int deleteCompanyData(int companyId, int deletedBy) {
		companyDocumentDAO.deleteCompanyDocumentData(companyId);
		companyBusinessIndustryDAO.deleteBusinessIndustryData(companyId);
		return companyDAO.deleteCompanyData(companyId, deletedBy);
	}

	public List<CompanyModel> getCompanyNameIdList(int start, int numberOfRecord, String companyName, UserModel userModel) throws AuthenticationException {

		if (userModel == null || userModel.getRole() == null || ApplicationUtility.getSize(userModel.getRole().getRoleName()) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}
		return companyDAO.getCompanyNameIdList(start, numberOfRecord, companyName, userModel);
	}

	public int updateCompanyPurchasePendingCount(int purchasePendingCount, int companyId) {
		return companyDAO.updateCompanyPurchasePendingCount(purchasePendingCount, companyId);
	}

	public int updateCompanyPurchaseCount(int purchaseCount, int companyId) {
		return companyDAO.updateCompanyPurchaseCount(purchaseCount, companyId);
	}

	public int updateCompanySellPendingCount(int sellPendingCount, int companyId) {
		return companyDAO.updateCompanySellPendingCount(sellPendingCount, companyId);
	}

	public int updateCompanySellCount(int sellCount, int companyId) {
		return companyDAO.updateCompanySellCount(sellCount, companyId);
	}

	public int updateCompanyExpensePendingCount(int expensePendingCount, int companyId) {
		return companyDAO.updateCompanyExpensePendingCount(expensePendingCount, companyId);
	}

	public int updateCompanyExpenseCount(int expenseCount, int companyId) {
		return companyDAO.updateCompanyExpenseCount(expenseCount, companyId);
	}

	public int updateCompanyPurchasePaymentPendingCount(int paymentPendingCount, int companyId) {
		return companyDAO.updateCompanyPurchasePaymentPendingCount(paymentPendingCount, companyId);
	}

	public int updateCompanyPurchasePaymentCount(int paymentCount, int companyId) {
		return companyDAO.updateCompanyPurchasePaymentCount(paymentCount, companyId);
	}
	
	public int updateCompanySellPaymentPendingCount(int paymentPendingCount, int companyId) {
		return companyDAO.updateCompanySellPaymentPendingCount(paymentPendingCount, companyId);
	}
	
	public int updateCompanySellPaymentCount(int sellPaymentCount, int companyId) {
		return companyDAO.updateCompanySellPaymentCount(sellPaymentCount, companyId);
	}

	public int updateCompanyCredintNotePendingCount(int creditNotePendingCount, int companyId) {
		return companyDAO.updateCompanyCredintNotePendingCount(creditNotePendingCount, companyId);
	}

	public int updateCompanyCredintNoteCount(int creditNoteCount, int companyId) {
		return companyDAO.updateCompanyCredintNoteCount(creditNoteCount, companyId);
	}

	public int updateCompanyDebitNotePendingCount(int debitNotePendingCount, int companyId) {
		return companyDAO.updateCompanyDebitNotePendingCount(debitNotePendingCount, companyId);
	}

	public int updateCompanyDebitNoteCount(int debitNoteCount, int companyId) {
		return companyDAO.updateCompanyDebitNoteCount(debitNoteCount, companyId);
	}

	public List<CompanyModel> getCompanyRequestCountDetails(int start, int noOfRecord, String companyName, String sortParam, UserModel userModel) throws AuthenticationException {

		if (userModel == null || userModel.getRole() == null || ApplicationUtility.getSize(userModel.getRole().getRoleName()) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}

		return companyDAO.getCompanyRequestCountDetails(start, noOfRecord, companyName, sortParam, userModel);
	}

	public int updateCompanyLastBillNumber(int companyId, int lastBillNumber) {
		return companyDAO.updateCompanyBillNumber(companyId, lastBillNumber, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString());
	}

	public int updateCompanyLastDebitNoteNumber(int companyId, int lastBillNumber) {
			return companyDAO.updateCompanyBillNumber(companyId, lastBillNumber, COMPANY_TABLE_COLUMN.LAST_DEBIT_NOTE_NUMBER.toString());
	}
		
	public int updateCompanyLastCreditNoteNumber(int companyId, int lastBillNumber) {
		return companyDAO.updateCompanyBillNumber(companyId, lastBillNumber, COMPANY_TABLE_COLUMN.LAST_CREDIT_NOTE_NUMBER.toString());
	}
	
	public int updateCompanyLastItemNumber(int companyId, int lastItemNumber) {
		return companyDAO.updateCompanyLastItemNumber(companyId, lastItemNumber);
	}

	public CompanyModel getBillPrefixAndLastNumber(int companyId) {
		return companyDAO.getBillPrefixAndLastNumber(companyId);
	}

	public int getCompanyIdByGSTNumber(String gstNumber) {
		return companyDAO.getCompanyIdFromGSTNumber(gstNumber);
	}

}
