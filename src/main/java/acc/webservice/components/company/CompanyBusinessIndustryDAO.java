package acc.webservice.components.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import acc.webservice.enums.DatabaseEnum.BUSINESS_INDUSTRY_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;

@Component
@Lazy
public class CompanyBusinessIndustryDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<String> getBusinessIndustryValueList(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT %s, %s ", BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY, BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY_ID))
				.append(String.format(" FROM %s WHERE %s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_BUSINESS_INDUSTRY, BUSINESS_INDUSTRY_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", BUSINESS_INDUSTRY_COLUMN.COMPANY_ID, BUSINESS_INDUSTRY_COLUMN.COMPANY_ID))
				.append(String.format(" ORDER BY %s ASC ", BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY));

		Map<String, Object> param = new HashMap<>();
		param.put(BUSINESS_INDUSTRY_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), param, getBusinessIndustryValueResultSetExctractor());
	}

	private ResultSetExtractor<List<String>> getBusinessIndustryValueResultSetExctractor() {
		return new ResultSetExtractor<List<String>>() {
			@Override
			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> businessIndustryList = new ArrayList<>();
				while (rs.next()) {
					businessIndustryList.add(rs.getString(BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY.toString()));
				}
				return businessIndustryList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveBusinessIndustryData(List<String> businessIndustryList, int companyId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_BUSINESS_INDUSTRY))
				.append(String.format(" ( %s, %s ) ", BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY, BUSINESS_INDUSTRY_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s ) ", BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY, BUSINESS_INDUSTRY_COLUMN.COMPANY_ID));

		List<Map<String, Object>> businessIndustryMapList = new ArrayList<>();
		Map<String, Object> businessIndustryURLMap;
		for (String businessIndustry : businessIndustryList) {
			businessIndustryURLMap = new HashMap<>();
			businessIndustryURLMap.put(BUSINESS_INDUSTRY_COLUMN.BUSINESS_INDUSTRY.toString(), businessIndustry);
			businessIndustryURLMap.put(BUSINESS_INDUSTRY_COLUMN.COMPANY_ID.toString(), companyId);
			businessIndustryMapList.add(businessIndustryURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), businessIndustryMapList.toArray(new HashMap[0]));
	}

	int deleteBusinessIndustryData(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_BUSINESS_INDUSTRY, BUSINESS_INDUSTRY_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", BUSINESS_INDUSTRY_COLUMN.COMPANY_ID, BUSINESS_INDUSTRY_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, companyId });
	}

}
