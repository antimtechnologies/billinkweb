package acc.webservice.components.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acc.webservice.components.exceldocument.ExcelDocumentModel;
import acc.webservice.components.exceldocument.ExcelDocumentService;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.sell.SellFreightChargeModel;
import acc.webservice.components.sell.SellItemModel;
import acc.webservice.components.sell.SellService;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.state.StateService;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Component
public class ExcelReader {

	@Autowired
	private ItemService itemService;
	@Autowired
	private LedgerService ledgerService;
	@Autowired
	private SellService sellService;
	@Autowired
	private StateService stateService;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private ExcellWriter excellWriter;
	@Autowired
	private ExcelDocumentService excelDocumentService;

	private Map<String, Integer> stateMap;

	private boolean checkIfRowIsEmpty(Row row) {
		if (row == null) {
			return true;
		}
		if (row.getLastCellNum() <= 0) {
			return true;
		}
		for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
			Cell cell = row.getCell(cellNum);
			if (cell != null && cell.getCellTypeEnum() != CellType.BLANK && ApplicationUtility.getSize(cell.toString()) > 0) {
				return false;
			}
		}
		return true;
	}

	public void processExcell(ExcelDocumentModel excellDocumentModel) throws IOException, InvalidFormatException {

		try {
			List<String> columnNameList = new ArrayList<>();
			// Creating a Workbook from an Excel file (.xls or .xlsx)
			stateMap = new HashMap<>();
			getStateList();
			Workbook workbook;// = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH1));
			File file = new File(excellDocumentModel.getDocumentURL());

			InputStream inp = new FileInputStream(excellDocumentModel.getDocumentURL());
			Sheet sheet = null; // sheet can be used as common for XSSF and HSSF
			if (excellDocumentModel.getDocumentURL().indexOf(".xlsx") > 1) {
				workbook = new XSSFWorkbook(inp);
			} else {
				POIFSFileSystem fs = new POIFSFileSystem(inp);
				workbook = new HSSFWorkbook(fs);
			}
			sheet = workbook.getSheetAt(0);

			// Create a DataFormatter to format and get each cell's value as String
			DataFormatter dataFormatter = new DataFormatter();
			SellModel sellModel;
			LedgerModel consigneeLedgerModel;
			SellItemModel sellItemModel;
			ItemModel itemModel;
			StateModel stateModel;
			StateModel buyerStateModel;
			LedgerModel buyerLedgerModel;
			SellFreightChargeModel sellFreightChargeModel;
			List<SellFreightChargeModel> sellFreightChargeModelList;

			LedgerModel sellFreightChargeModelLedger;
			List<Map<String, Object>> errorList = new ArrayList<>();
			Map<String, SellModel> billNumberSellModelMap = new HashMap<>();
			int cellCounter = 0;
			int rawCounter = 0;
			String errorExcellName = "";
			try {
				String path = this.getClass().getClassLoader().getResource("").getPath();
				String fullPath = URLDecoder.decode(path, "UTF-8");
				String pathArr[] = fullPath.split(servletContext.getContextPath() + "/WEB-INF/classes/");
				errorExcellName = pathArr[0] + "/resources/images/excelUpload/error_" + file.getName();
				if (excellDocumentModel.getDocumentURL() != null) {
					String errorFilePath = excellDocumentModel.getDocumentURL().substring(
							excellDocumentModel.getDocumentURL().indexOf("/resources"),
							excellDocumentModel.getDocumentURL().lastIndexOf("/"));
					excellDocumentModel.setErrorDocumentURL(errorFilePath + "/error_" + file.getName());
				}

			} catch (UnsupportedEncodingException e) {

			}

			for (Row row : sheet) {

				if (checkIfRowIsEmpty(row)) {
					break;
				}
				sellModel = new SellModel();
				consigneeLedgerModel = new LedgerModel();
				sellItemModel = new SellItemModel();
				stateModel = new StateModel();
				buyerLedgerModel = new LedgerModel();
				itemModel = new ItemModel();
				sellFreightChargeModel = new SellFreightChargeModel();
				sellFreightChargeModelLedger = new LedgerModel();
				buyerStateModel = new StateModel();

				sellModel.setSellItemModel(new ArrayList<>());
				sellModel.setCompanyId(excellDocumentModel.getCompanyId());

				sellModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				consigneeLedgerModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				buyerLedgerModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				sellItemModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				itemModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				sellFreightChargeModel.setExcellDocumentId(excellDocumentModel.getDocumentId());
				sellFreightChargeModelLedger.setExcellDocumentId(excellDocumentModel.getDocumentId());

				sellModel.setAddedBy(excellDocumentModel.getAddedBy());
				consigneeLedgerModel.setAddedBy(excellDocumentModel.getAddedBy());
				buyerLedgerModel.setAddedBy(excellDocumentModel.getAddedBy());
				itemModel.setAddedBy(excellDocumentModel.getAddedBy());
				sellFreightChargeModelLedger.setAddedBy(excellDocumentModel.getAddedBy());
				sellFreightChargeModelList = new ArrayList<>();

				cellCounter = 0;
				rawCounter++;

				// Read columns start
				if (rawCounter == 1) {
					for (Cell cell : row) {
						String cellValue = dataFormatter.formatCellValue(cell);
						columnNameList.add(cellValue);
						System.out.print(cellValue);
					}
					continue;
				}
				// Read columns end

				for (cellCounter = 0; cellCounter < columnNameList.size(); cellCounter++) {
					Cell cell = row.getCell(cellCounter);
					String cellValue = dataFormatter.formatCellValue(cell);
					if (cell == null || cellValue == null || cellValue.length() == 0) {
						continue;
					}

					switch (columnNameList.get(cellCounter).trim()) {
					case "Voucher Type":
						sellModel.setVoucherType(cellValue);
						break;
					case "VoucherNumber. Bill number":
						sellModel.setBillNumber(cellValue);
						break;
					case "REFERENCE_NUMBER":
						sellModel.setReferenceNumber1(cellValue);
						break;
					case "Date":
						sellModel.setSellDate(cellValue);
						break;

					case "Consignee Name":// Consignee Name or ledger name
						consigneeLedgerModel.setLedgerName(cellValue);
						break;
					case "Consignee Address":// Consignee Address or ledger address
						consigneeLedgerModel.setAddress(cellValue);
						break;
					case "Consignee GSTIN":// Consignee GSTIN or or ledger gst
						consigneeLedgerModel.setGstNumber(cellValue);
						break;
					case "Consignee State":// Consignee State
						if (stateMap.get(cellValue) != null) {
							stateModel.setStateName(cellValue);
							stateModel.setStateId(stateMap.get(cellValue));
						} else {
							stateModel.setStateName(cellValue);
							stateModel = stateService.insertStateData(stateModel);
						}
						consigneeLedgerModel.setState(stateModel);
						break;
					case "Consignee Pincode":
						consigneeLedgerModel.setPinCode(Integer.parseInt(cellValue));
						break;

					case "Buyers Name":// Buyers Name or if blank ledger name will be buyer name
						buyerLedgerModel.setLedgerName(cellValue);
						break;
					case "Buyers Address":// Buyers Address
						buyerLedgerModel.setAddedDateTime(cellValue);
						break;
					case "Buyers Country":// Buyers Country
						// TODO
						break;
					case "Buyers State":// Buyers State
						if (stateMap.get(cellValue) != null) {
							buyerStateModel.setStateName(cellValue);
							buyerStateModel.setStateId(stateMap.get(cellValue));
						} else {
							buyerStateModel.setStateName(cellValue);
							buyerStateModel = stateService.insertStateData(buyerStateModel);
						}
						buyerLedgerModel.setState(buyerStateModel);
						break;
					case "Buyers Pincode":
						buyerLedgerModel.setPinCode(Integer.parseInt(cellValue));
						break;
					case "Buyers Place Of Supply":// Buyers Place Of Supply//todo
						break;
					case "Buyers Reg Type":// Buyers Reg Type
						buyerLedgerModel.setGstType(cellValue);
						break;
					case "Buyers GSTIN":// Buyers GSTIN
						buyerLedgerModel.setGstNumber(cellValue);
						break;

					case "Order ID":// Order ID
						sellModel.setOrderId(Long.parseLong(cellValue));
						break;
					case "Suborder ID":// Suborder ID
						sellModel.setSubOrderId(Long.parseLong(cellValue));
						break;

					case "ItemName":// ItemName
						itemModel.setItemName(cellValue);
						break;
					case "Alias":// Alias
						itemModel.setItemAlias(cellValue);
						break;
					case "BOM Name":// BOM Name
						itemModel.setBomName(cellValue);
						break;
					case "BOM Alias":// BOM Alias
						itemModel.setBomAlias(cellValue);
						break;
					case "Qty":// Qty
						sellItemModel.setQuantity(Double.parseDouble(cellValue));
					case "Rate":// Rate
						sellItemModel.setSellRate(Double.parseDouble(cellValue));
						break;
					case "Discount":// Discoun
						sellItemModel.setDiscount(Double.parseDouble(cellValue));
						break;
					case "Amount":// Amount
						sellItemModel.setItemAmount(Double.parseDouble(cellValue));
						break;
					case "CGST":// CGST
						sellItemModel.setCgstPer(Double.parseDouble(cellValue));
						break;
					case "CGST Amount":// CGST Amount
						sellItemModel.setCgst(Double.parseDouble(cellValue));
						break;
					case "SGST":// SGST
						sellItemModel.setSgstPer(Double.parseDouble(cellValue));
						break;
					case "SGST Amount":// SGST Amount
						sellItemModel.setSgst(Double.parseDouble(cellValue));
						break;
					case "IGST":// IGST
						sellItemModel.setIgstPer(Double.parseDouble(cellValue));
						break;
					case "IGST Amount":// IGST Amount
						sellItemModel.setIgst(Double.parseDouble(cellValue));
						break;

					case "LedgerName1":
					case "LedgerName2":
					case "LedgerName3":
					case "LedgerName4":
					case "LedgerName5":
						if (sellFreightChargeModel.getLedger() != null
								&& ApplicationUtility.getSize(sellFreightChargeModel.getLedger().getLedgerName()) > 0) {
							sellFreightChargeModelList.add(sellFreightChargeModel);
						}
						sellFreightChargeModel = new SellFreightChargeModel();
						sellFreightChargeModelLedger.setLedgerName(cellValue);
						sellFreightChargeModel.setLedger(sellFreightChargeModelLedger);
						break;
					case "Amount1":
					case "Amount2":
					case "Amount3":
					case "Amount4":
					case "Amount5":
						sellFreightChargeModel.setFreightCharge(Double.parseDouble(cellValue));
						break;
					case "CGST1":
					case "CGST2":
					case "CGST3":
					case "CGST4":
					case "CGST5":
						sellFreightChargeModel.setCgstPer(Double.parseDouble(cellValue));
						break;
					case "CGST Amount1":
					case "CGST Amount2":
					case "CGST Amount3":
					case "CGST Amount4":
					case "CGST Amount5":
						sellFreightChargeModel.setCgst(Double.parseDouble(cellValue));
						break;
					case "SGST1":
					case "SGST2":
					case "SGST3":
					case "SGST4":
					case "SGST5":
						sellFreightChargeModel.setSgstPer(Double.parseDouble(cellValue));
						break;
					case "SGST Amount1":
					case "SGST Amount2":
					case "SGST Amount3":
					case "SGST Amount4":
					case "SGST Amount5":
						sellFreightChargeModel.setSgst(Double.parseDouble(cellValue));
						break;
					case "IGST1":
					case "IGST2":
					case "IGST3":
					case "IGST4":
					case "IGST5":
						sellFreightChargeModel.setIgstPer(Double.parseDouble(cellValue));
						break;
					case "IGST Amount1":
					case "IGST Amount2":
					case "IGST Amount3":
					case "IGST Amount4":
					case "IGST Amount5":
						sellFreightChargeModel.setIgst(Double.parseDouble(cellValue));
						break;
					default:
						break;
					}
					System.out.print(cellValue + " : " + "\t");
				}
				if (sellFreightChargeModel.getLedger() != null
						&& ApplicationUtility.getSize(sellFreightChargeModel.getLedger().getLedgerName()) > 0) {
					sellFreightChargeModelList.add(sellFreightChargeModel);
				}
				sellItemModel.setItem(itemModel);
				processData(sellItemModel, consigneeLedgerModel, sellModel, excellDocumentModel, errorList,
						billNumberSellModelMap, sellFreightChargeModelList, rawCounter, buyerLedgerModel, row);
			}
			saveSellData(billNumberSellModelMap);
			if (errorList != null && errorList.size() > 0) {
				excellWriter.createExcellFile(errorList, columnNameList, errorExcellName, excellDocumentModel);
			} else {
				excelDocumentService.updateExcelDocumentWhileSuccess(excellDocumentModel);
			}
			workbook.close();
		} catch (Exception e) {
			excelDocumentService.updateExcelDocumentWhileError(excellDocumentModel);
			e.printStackTrace();
		}
	}

	private void saveSellData(Map<String, SellModel> billNumberSellModelMap) {
		SellModel sellData = new SellModel();
		double totalValue = 0;
		double itemAmount = 0;
		for (Entry<String, SellModel> entry : billNumberSellModelMap.entrySet()) {
			try {
				sellData = entry.getValue();
				totalValue = 0;
				if (ApplicationUtility.getSize(sellData.getSellItemModel()) > 0) {
					for (SellItemModel sellItemModel : sellData.getSellItemModel()) {
						itemAmount = sellItemModel.getSellRate() * sellItemModel.getQuantity();
						sellItemModel.setItemAmount(itemAmount);
						totalValue += itemAmount - sellItemModel.getDiscount() + sellItemModel.getCgst()
								+ sellItemModel.getSgst() + sellItemModel.getIgst();
					}
				}

				if (ApplicationUtility.getSize(sellData.getFreightList()) > 0) {
					for (SellFreightChargeModel sellFreightChargeModel : sellData.getFreightList()) {
						totalValue += sellFreightChargeModel.getFreightCharge() + sellFreightChargeModel.getCgst()
								+ sellFreightChargeModel.getSgst() + sellFreightChargeModel.getIgst();
					}
				}

				totalValue = Math.round(totalValue * 100D) / 100D;
				sellData.setTotalBillAmount(totalValue);
				sellService.publishSellData(entry.getValue());
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}
	}

	private void processData(SellItemModel sellItemModel, LedgerModel consigneeLedgerModel, SellModel sellModel,
			ExcelDocumentModel excellDocumentModel, List<Map<String, Object>> errorList,
			Map<String, SellModel> billNumberSellModelMap, List<SellFreightChargeModel> sellFreightChargeModelList,
			int rawCounter, LedgerModel buyerLedgerModel, Row row) {
		int ledgerId = 0;
		int buyerLedgerId = 0;
		int itemId = 0;
		Map<String, Object> errorMap = new HashMap<>();
		try {
			calculateGstAmount(sellItemModel);
			if (sellModel.getBillNumber() != null && sellModel.getBillNumber().length() > 0) {// If bill number null
																								// then return
				if (billNumberSellModelMap.containsKey(sellModel.getBillNumber())) { // Bill number already exist
					sellModel = billNumberSellModelMap.get(sellModel.getBillNumber());
					if (sellItemModel.getItem() != null && sellItemModel.getItem().getItemName() != null
							&& sellItemModel.getItem().getItemName().length() > 0) {
						itemId = itemService.getItemIdByName(excellDocumentModel.getCompanyId(),
								sellItemModel.getItem().getItemName());
						if (itemId > 0) {
							sellItemModel.getItem().setItemId(itemId);
						} else {
							sellItemModel.getItem().setCompanyId(excellDocumentModel.getCompanyId());
							sellItemModel.setItem(itemService.saveItemData(sellItemModel.getItem()));
						}
					} else {
						errorMap.put("errorMessage", "Item Name Empty");
						errorMap.put("errorModel", sellItemModel);
						errorMap.put("rowNumber", rawCounter);
						errorMap.put("rowData", row);
						errorList.add(errorMap);
						return;
					}

				} else {

					if (consigneeLedgerModel.getLedgerName() != null
							&& consigneeLedgerModel.getLedgerName().length() > 0) {
						ledgerId = ledgerService.getLedgerIdByName(excellDocumentModel.getCompanyId(),
								consigneeLedgerModel.getLedgerName());
						if (ledgerId > 0) {
							consigneeLedgerModel.setLedgerId(ledgerId);
						} else {
							consigneeLedgerModel.setCompanyId(excellDocumentModel.getCompanyId());
							consigneeLedgerModel = ledgerService.saveLedgerData(consigneeLedgerModel);
						}
					} else {
						errorMap.put("errorMessage", "Cosignee Name Empty");
						errorMap.put("errorModel", sellItemModel);
						errorMap.put("rowNumber", rawCounter);
						errorMap.put("rowData", row);
						errorList.add(errorMap);
						return;
					}

					if (buyerLedgerModel.getLedgerName() != null && buyerLedgerModel.getLedgerName().length() > 0) {
						buyerLedgerId = ledgerService.getLedgerIdByName(excellDocumentModel.getCompanyId(),
								buyerLedgerModel.getLedgerName());
						if (buyerLedgerId > 0) {
							buyerLedgerModel.setLedgerId(buyerLedgerId);
						} else {
							buyerLedgerModel.setCompanyId(excellDocumentModel.getCompanyId());
							buyerLedgerModel = ledgerService.saveLedgerData(buyerLedgerModel);
						}
					} else {
						buyerLedgerModel = consigneeLedgerModel;
					}

					sellModel.setBuyerLedgerData(buyerLedgerModel);

					if (sellItemModel.getItem() != null && sellItemModel.getItem().getItemName() != null
							&& sellItemModel.getItem().getItemName().length() > 0) {
						itemId = itemService.getItemIdByName(excellDocumentModel.getCompanyId(),
								sellItemModel.getItem().getItemName());
						if (itemId > 0) {
							sellItemModel.getItem().setItemId(itemId);
						} else {
							sellItemModel.getItem().setCompanyId(excellDocumentModel.getCompanyId());
							sellItemModel.setItem(itemService.saveItemData(sellItemModel.getItem()));
						}
					} else {
						errorMap.put("errorMessage", "Item Name Empty");
						errorMap.put("errorModel", sellItemModel);
						errorMap.put("rowNumber", rawCounter);
						errorMap.put("rowData", row);
						errorList.add(errorMap);
						return;
					}

					try {
						if (ApplicationUtility.getSize(sellModel.getSellDate()) > 0) {
							sellModel.setSellDate(DateUtility.convertDateIntoFormat(sellModel.getSellDate(),
									DateUtility.DEFAULT_USER_DATE_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
						} else {
							errorMap.put("errorMessage", "Sell Date is empty.");
							errorMap.put("errorModel", sellItemModel);
							errorMap.put("rowNumber", rawCounter);
							errorMap.put("rowData", row);
							errorList.add(errorMap);
							return;
						}
					} catch (ParseException e) {
						errorMap.put("errorMessage", "Date format should be dd-MM-yyyy.");
						errorMap.put("errorModel", sellItemModel);
						errorMap.put("rowNumber", rawCounter);
						errorMap.put("rowData", row);
						errorList.add(errorMap);
						return;
					}

					sellModel.setLedgerData(consigneeLedgerModel);
				}
				if (sellFreightChargeModelList != null && sellFreightChargeModelList.size() > 0) {
					for (SellFreightChargeModel sellFreightChargeModel : sellFreightChargeModelList) {
						if (sellFreightChargeModel.getLedger() != null
								&& sellFreightChargeModel.getLedger().getLedgerName().length() > 0) {
							procesSellFreightChargeModel(sellFreightChargeModel, sellModel, excellDocumentModel);
						}
					}
				}

				sellModel.getSellItemModel().add(sellItemModel);
				billNumberSellModelMap.put(sellModel.getBillNumber(), sellModel);
			} else {
				errorMap.put("errorMessage", "Bill Nuber is epmty.");
				errorMap.put("errorModel", sellItemModel);
				errorMap.put("rowNumber", rawCounter);
				errorMap.put("rowData", row);
				errorList.add(errorMap);
				return;
			}
		} catch (Exception e) {
			errorMap.put("errorMessage", "Unexpected Error while processing");
			errorMap.put("errorModel", sellItemModel);
			errorMap.put("rowNumber", rawCounter);
			errorMap.put("rowData", row);
			errorList.add(errorMap);
		}
	}

	private void calculateGstAmount(SellItemModel sellItemModel) {
		if (sellItemModel.getIgstPer() != 0) {
			sellItemModel.setTaxRate(sellItemModel.getIgstPer());
		} else {
			sellItemModel.setTaxRate(sellItemModel.getCgstPer() + sellItemModel.getSgstPer());
			sellItemModel.getItem().setTaxCode(sellItemModel.getCgstPer() + sellItemModel.getSgstPer());
		}
	}

	private void procesSellFreightChargeModel(SellFreightChargeModel sellFreightChargeModel, SellModel sellModel,
			ExcelDocumentModel excellDocumentModel) throws AccountingSofwareException {
		int ledgerId;
		if (sellFreightChargeModel.getIgstPer() != 0) {
			sellFreightChargeModel.setTaxRate(sellFreightChargeModel.getIgstPer());
		} else {
			sellFreightChargeModel
					.setTaxRate(sellFreightChargeModel.getCgstPer() + sellFreightChargeModel.getSgstPer());
			sellFreightChargeModel.getLedger()
					.setTaxCode(sellFreightChargeModel.getCgstPer() + sellFreightChargeModel.getSgstPer());
		}
		if (sellFreightChargeModel.getLedger() != null && sellFreightChargeModel.getLedger().getLedgerName() != null
				&& sellFreightChargeModel.getLedger().getLedgerName().length() > 0) {
			ledgerId = ledgerService.getLedgerIdByName(excellDocumentModel.getCompanyId(),
					sellFreightChargeModel.getLedger().getLedgerName());
			if (ledgerId > 0) {
				sellFreightChargeModel.getLedger().setLedgerId(ledgerId);
			} else {
				sellFreightChargeModel.getLedger().setCompanyId(excellDocumentModel.getCompanyId());
				sellFreightChargeModel.setLedger(ledgerService.saveLedgerData(sellFreightChargeModel.getLedger()));
			}
			if (sellModel.getFreightList() == null) {
				sellModel.setFreightList(new ArrayList<>());
			}
			sellModel.getFreightList().add(sellFreightChargeModel);
		}
	}

	private void getStateList() {
		List<StateModel> stateDataList = stateService.getStateDataList();
		for (StateModel stateModel : stateDataList) {
			stateMap.put(stateModel.getStateName(), stateModel.getStateId());
		}
	}
}