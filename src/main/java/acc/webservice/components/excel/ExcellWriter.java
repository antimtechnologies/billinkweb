package acc.webservice.components.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acc.webservice.components.exceldocument.ExcelDocumentModel;
import acc.webservice.components.exceldocument.ExcelDocumentService;

@Component
public class ExcellWriter {

	@Autowired
	ExcelDocumentService excelDocumentService;
	
	public void createExcellFile(List<Map<String, Object>> errorList, List<String> columnNameList, String fileName, ExcelDocumentModel excellDocumentModel)
			throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Error Sheet");
		DataFormatter dataFormatter = new DataFormatter();

		int rowCount = 0;
		Row row;
		int cellCounter=0;
		Cell cell;
		Row rowData;
		row = sheet.createRow(rowCount);
		for (String columnName : columnNameList) {
				cell = row.createCell(cellCounter);
				cell.setCellValue(columnName);
				cellCounter++;
		}
		cell = row.createCell(cellCounter);
		cell.setCellValue("Error Message");

		for (Map<String, Object> map : errorList) {
			rowData = (Row) map.get("rowData");
			row = sheet.createRow(++rowCount);
			for (cellCounter = 0; cellCounter < columnNameList.size(); cellCounter++) {
				cell = row.createCell(cellCounter);
				Cell cell1 = rowData.getCell(cellCounter);
				cell.setCellValue(dataFormatter.formatCellValue(cell1));
			}
			cell = row.createCell(cellCounter);
			cell.setCellValue((String)map.get("errorMessage"));
		}

		try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
			workbook.write(outputStream);
			excelDocumentService.updateExcelDocumentWhileError(excellDocumentModel);
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
