package acc.webservice.components.excel;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.stereotype.Component;

import acc.webservice.components.exceldocument.ExcelDocumentModel;

@Component
public class CronJobScheduler {

	public static void scheduleExcellSheet(ExcelDocumentModel saveExcelDocument) {
		Scheduler scheduler = null;
		String triggerName 	= "readingExcellTrigger" + System.currentTimeMillis() + "_" + Math.random();
		String triggerGroup = "readingExcellTriggerGroup" + System.currentTimeMillis() + "_" + Math.random();
		String jobName 		= "readingExcellSheet";
		String jobGroupName = "readExcellGroup";
		try {
			
			scheduler = Schedular.getInstance();
			JobDetail jobDetail = JobBuilder.newJob(JobPerformer.class).withIdentity(jobName, jobGroupName).build();
//			String fileName = "C:\\\\\\\\Users\\\\\\\\Anant\\\\\\\\Desktop\\\\\\\\Excell\\\\\\\\MyTest\\\\\\\\\\\\\\\\Excell_For_test.xlsx";
			jobDetail.getJobDataMap().put("saveExcelDocument", saveExcelDocument);
			
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName, triggerGroup).startNow().build();
			
			scheduler.scheduleJob(jobDetail, trigger);
			//scheduler.unscheduleJob(getTriggerKey(triggerName,triggerGroup));
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//scheduleExcellSheet();
	}
}
