package acc.webservice.components.excel;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class Schedular 
{ 
    private static Scheduler scheduler = null; 
    
    private Schedular(){ }
    
    public static Scheduler getInstance() 
    { 
        if (scheduler == null) {
        	try {
				scheduler = new StdSchedulerFactory().getScheduler();
				scheduler.start(); 
			} catch (SchedulerException e) {
				e.printStackTrace();
				try {
					scheduler.shutdown();
				} catch (SchedulerException e1) {
					e.printStackTrace();
				}
			}finally {
			
			}
        } 
        return scheduler; 
    } 
} 