package acc.webservice.components.excel;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Controller;

import acc.webservice.components.exceldocument.ExcelDocumentModel;
import acc.webservice.config.BillinkApplicationContext;

@Controller
public class JobPerformer implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			ExcelReader excelReader = (ExcelReader) BillinkApplicationContext.getObject(ExcelReader.class);
			ExcelDocumentModel excellDocumentModel= (ExcelDocumentModel) context.getJobDetail().getJobDataMap().get("saveExcelDocument");
			excelReader.processExcell(excellDocumentModel);
		} catch (InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
		
	}

}
