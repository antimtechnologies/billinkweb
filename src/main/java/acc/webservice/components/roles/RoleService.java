package acc.webservice.components.roles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class RoleService {

	@Autowired
	private RoleDAO roleDAO;

	public List<RoleModel> getRoleDetails() {
		return roleDAO.getRoleDetails();
	}

	
}
