package acc.webservice.components.roles;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.ROLE_TABLE_COLUMN;

public class RoleModel {

	private int roleId;
	private String roleName;

	public RoleModel() { }
	public RoleModel(ResultSet rs) throws SQLException {
		setRoleId(rs.getInt(ROLE_TABLE_COLUMN.ROLE_ID.toString()));
		setRoleName(rs.getString(ROLE_TABLE_COLUMN.ROLE_NAME.toString()));
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "RoleModel [roleId=" + roleId + ", roleName=" + roleName + "]";
	}

}
