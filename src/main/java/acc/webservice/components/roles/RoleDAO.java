package acc.webservice.components.roles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ROLE_TABLE_COLUMN;

@Repository
@Lazy
public class RoleDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<RoleModel> getRoleDetails() {
		String sqlQuery = String.format(" SELECT %s, %s FROM %s WHERE %s = 0 ", ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_NAME, DATABASE_TABLE.ACC_ROLE_MASTER, ROLE_TABLE_COLUMN.IS_DELETED);
		return namedParameterJdbcTemplate.query(sqlQuery, getRoleDetailExctractor());
	}

	private ResultSetExtractor<List<RoleModel>> getRoleDetailExctractor() {
		return new ResultSetExtractor<List<RoleModel>>() {
			@Override
			public List<RoleModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				RoleModel roleData = null;
				List<RoleModel> roleDetailList = new ArrayList<>();
				while(rs.next()) {
					roleData = new RoleModel(rs);
					roleDetailList.add(roleData);
				}
				return roleDetailList;
			}
		};
	}
}
