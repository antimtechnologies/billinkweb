package acc.webservice.components.roles;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roleData/")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@RequestMapping(value="getRoleDetails", method=RequestMethod.POST)
	public Map<String,List<RoleModel>> getRoleDetails() {
		return Collections.singletonMap("data", roleService.getRoleDetails());
	}
}
