package acc.webservice.components.menu;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menuData/")
public class MenuController {

	@Autowired
	private MenuService menuService;

	@RequestMapping(value="getMenuDetails", method=RequestMethod.POST)
	public Map<String, Object> getMenuDetails() {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", menuService.getMenuDetails());
		return response;
	}

}
