package acc.webservice.components.menu;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.MENU_DETAILS_COLUMN;

@Repository
@Lazy
public class MenuDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public List<MenuModel> getMenuDetails() {
		String sqlQuery = String.format(" SELECT %s, %s FROM %s WHERE %s = 0 ", MENU_DETAILS_COLUMN.MENU_ID, MENU_DETAILS_COLUMN.MENU_NAME, DATABASE_TABLE.ACC_MENU_DETAILS, MENU_DETAILS_COLUMN.IS_DELETED);
		return namedParameterJdbcTemplate.query(sqlQuery, getMenuDetailExctractor());
	}

	private ResultSetExtractor<List<MenuModel>> getMenuDetailExctractor() {
		return new ResultSetExtractor<List<MenuModel>>() {

			@Override
			public List<MenuModel> extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<MenuModel> menuList = new ArrayList<>();
				MenuModel menuData = null;

				while(rs.next()) {
					menuData = new MenuModel(rs);
					menuList.add(menuData);
				}
				return menuList;
			}
		};
	}

}
