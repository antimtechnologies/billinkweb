package acc.webservice.components.menu;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.MENU_DETAILS_COLUMN;

public class MenuModel {

	private short menuId;
	private String menuName;
	private boolean selected;
	private short displayIndex;

	public MenuModel() { }

	public MenuModel(ResultSet rs) throws SQLException {
		setMenuId(rs.getShort(MENU_DETAILS_COLUMN.MENU_ID.toString()));
		setMenuName(rs.getString(MENU_DETAILS_COLUMN.MENU_NAME.toString()));
	}

	public short getMenuId() {
		return menuId;
	}

	public void setMenuId(short menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public short getDisplayIndex() {
		return displayIndex;
	}

	public void setDisplayIndex(short displayIndex) {
		this.displayIndex = displayIndex;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "MenuModel [menuId=" + menuId + ", MenuName=" + menuName + ", displayIndex=" + displayIndex + "]";
	}

}
