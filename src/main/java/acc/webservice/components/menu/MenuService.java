package acc.webservice.components.menu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class MenuService {

	@Autowired
	private MenuDAO menuDAO;

	public List<MenuModel> getMenuDetails() {
		return menuDAO.getMenuDetails();
	}

}
