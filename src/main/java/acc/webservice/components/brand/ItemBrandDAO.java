package acc.webservice.components.brand;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;

@Repository
@Lazy
public class ItemBrandDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<ItemBrandModel> getBrandList(int companyId, int start, int noOfRecord, String brandName) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT %s, %s, %s ", ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_BRAND_MASTER_COLUMN.COMPANY_ID))
				.append(String.format(" FROM %s WHERE %s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER, ACC_BRAND_MASTER_COLUMN.IS_DELETED))
				.append(String.format(" AND ( %s LIKE :%s OR '%%' = :%s ) ", ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_BRAND_MASTER_COLUMN.BRAND_NAME))
				.append(String.format(" AND %s = :%s ", ACC_BRAND_MASTER_COLUMN.COMPANY_ID, ACC_BRAND_MASTER_COLUMN.COMPANY_ID))
				.append(String.format(" ORDER BY %s ASC ", ACC_BRAND_MASTER_COLUMN.BRAND_NAME));

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(), APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		parameters.put(ACC_BRAND_MASTER_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(ACC_BRAND_MASTER_COLUMN.BRAND_NAME.toString(), "%" + brandName + "%");

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getItemBrandModelResultSetExctractor());
	}

	private ResultSetExtractor<List<ItemBrandModel>> getItemBrandModelResultSetExctractor() {
		return new ResultSetExtractor<List<ItemBrandModel>>() {

			@Override
			public List<ItemBrandModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemBrandModel> itemBrandList = new ArrayList<>();
				while (rs.next()) {
					itemBrandList.add(new ItemBrandModel(rs));
				}
				return itemBrandList;
			}
		};
	}

	public ItemBrandModel saveItemBrandList(ItemBrandModel itemBrandModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER))
				.append(String.format(" ( %s, %s ) VALUES ( :%s, :%s ) ", ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_BRAND_MASTER_COLUMN.COMPANY_ID, ACC_BRAND_MASTER_COLUMN.BRAND_NAME,ACC_BRAND_MASTER_COLUMN.COMPANY_ID));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(ACC_BRAND_MASTER_COLUMN.BRAND_NAME.toString(), itemBrandModel.getBrandName())
						.addValue(ACC_BRAND_MASTER_COLUMN.COMPANY_ID.toString(), itemBrandModel.getCompanyId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		itemBrandModel.setBrandId(holder.getKey().intValue());
		return itemBrandModel;
	}

	public ItemBrandModel updateItemBrandList(ItemBrandModel itemBrandModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER))
				.append(String.format(" %s = :%s ", ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_BRAND_MASTER_COLUMN.BRAND_NAME))
				.append(String.format(" WHERE %s = :%s ", ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_ID))
				.append(String.format(" AND %s = :%s ", ACC_BRAND_MASTER_COLUMN.COMPANY_ID, ACC_BRAND_MASTER_COLUMN.COMPANY_ID));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_BRAND_MASTER_COLUMN.BRAND_NAME.toString(), itemBrandModel.getBrandName());
		parameters.put(ACC_BRAND_MASTER_COLUMN.BRAND_ID.toString(), itemBrandModel.getBrandId());
		parameters.put(ACC_BRAND_MASTER_COLUMN.COMPANY_ID.toString(), itemBrandModel.getCompanyId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return itemBrandModel;
	}

	public int deleteItemBrandList(int brandId, int companyId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" DELETE FROM %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER))
				.append(String.format(" WHERE %s = :%s ", ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_ID))
				.append(String.format(" AND %s = :%s ", ACC_BRAND_MASTER_COLUMN.COMPANY_ID, ACC_BRAND_MASTER_COLUMN.COMPANY_ID));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_BRAND_MASTER_COLUMN.BRAND_ID.toString(), brandId);
		parameters.put(ACC_BRAND_MASTER_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

}
