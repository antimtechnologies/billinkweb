package acc.webservice.components.brand;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class ItemBrandService {

	@Autowired
	private ItemBrandDAO itemBrandDAO;

	public List<ItemBrandModel> getBrandList(int companyId, int start, int noOfRecord, String brandName) {
		return itemBrandDAO.getBrandList(companyId, start, noOfRecord, brandName);
	}

	public ItemBrandModel saveItemBrandList(ItemBrandModel itemBrandModel) {
		return itemBrandDAO.saveItemBrandList(itemBrandModel);
	}

	public ItemBrandModel updateItemBrandList(ItemBrandModel itemBrandModel) {
		return itemBrandDAO.updateItemBrandList(itemBrandModel);
	}

	public int deleteItemBrandList(int brandId, int companyId) {
		return itemBrandDAO.deleteItemBrandList(brandId, companyId);
	}

}
