package acc.webservice.components.brand;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;

public class ItemBrandModel {

	private int brandId;
	private String brandName;
	private boolean isDeleted;
	private int companyId;

	public ItemBrandModel() {
	}

	public ItemBrandModel(ResultSet rs) throws SQLException {
		setBrandId(rs.getInt(ACC_BRAND_MASTER_COLUMN.BRAND_ID.toString()));
		setBrandName(rs.getString(ACC_BRAND_MASTER_COLUMN.BRAND_NAME.toString()));
	}

	public int getBrandId() {
		return brandId;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "ItemBrandModel [brandId=" + brandId + ", brandName=" + brandName + ", isDeleted=" + isDeleted + "]";
	}

}
