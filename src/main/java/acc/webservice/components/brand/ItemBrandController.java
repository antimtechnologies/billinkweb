package acc.webservice.components.brand;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/branddata/")
public class ItemBrandController {

	@Autowired
	private ItemBrandService itemBrandService;

	@RequestMapping(value="getItemBrandList", method=RequestMethod.POST)
	public Map<String,Object> getItemBrandList(@RequestBody Map<String, Object> parameter) {
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String brandName = ApplicationUtility.getStrValue(parameter, ACC_BRAND_MASTER_COLUMN.BRAND_NAME.toString());
		return Collections.singletonMap("data", itemBrandService.getBrandList(companyId, start, noOfRecord, brandName));
	}

	@RequestMapping(value="saveItemBrandData", method=RequestMethod.POST)
	public Map<String,Object> saveItemBrandList(@RequestBody ItemBrandModel itemBrandModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemBrandService.saveItemBrandList(itemBrandModel));
		return response;
	}

	@RequestMapping(value="updateItemBrandData", method=RequestMethod.POST)
	public Map<String,Object> updateItemBrandList(@RequestBody ItemBrandModel itemBrandModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemBrandService.updateItemBrandList(itemBrandModel));
		return response;
	}
	
	@RequestMapping(value="deleteItemBrandData", method=RequestMethod.POST)
	public Map<String,Object> deleteItemBrandList(@RequestBody ItemBrandModel itemBrandModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemBrandService.deleteItemBrandList(itemBrandModel.getBrandId(), itemBrandModel.getCompanyId()));
		return response;
	}

	
	
	
}
