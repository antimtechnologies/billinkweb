package acc.webservice.components.customertype;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class CustomerTypeModelDao {

	
	final String SELECT_ALL_QUERY = "SELECT customerTypeId,customerType,isDeleted,companyId FROM acc_customer_type_details order by customerTypeId desc ;";
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	CustomerTypeModel saveCustomerType(CustomerTypeModel customerTypeModel) {
		StringBuilder sqlQueryCustomerTypeSave = new StringBuilder();
		sqlQueryCustomerTypeSave.append(String.format(" Insert INTO %s ", DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS))
							 .append(String.format("( %s,", CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE))
							 .append(String.format(" %s,", CUSTOMER_TYPE_COLUMN.IS_DELETED))
							 .append(String.format(" %s )", CUSTOMER_TYPE_COLUMN.COMPANY_ID))
							 .append(" VALUES ")
							 .append(String.format("( :%s, ", CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE))
							 .append(" 0, ")
							 .append(String.format(" :%s ) ", CUSTOMER_TYPE_COLUMN.COMPANY_ID))
							 .append(String.format("ON DUPLICATE KEY UPDATE isDeleted=0"));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString(),customerTypeModel.getCustomerType())
					.addValue(CUSTOMER_TYPE_COLUMN.COMPANY_ID.toString(),customerTypeModel.getCompanyId());
					
		
		namedParameterJdbcTemplate.update(sqlQueryCustomerTypeSave.toString(), parameters, holder);
//		customerTypeModel.setCustomerTypeId(holder.getKey().intValue());
		return customerTypeModel;
	}
	
    public List<CustomerTypeModel> getAllCustomerTypeList() {
        return this.namedParameterJdbcTemplate.query(SELECT_ALL_QUERY, new CustomerTypeMapper());
    }
    
    public List<CustomerTypeModel> getAllCustomerTypeList(int start, int noOfRecord,int companyId) {
    	StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT customerTypeId,customerType,isDeleted,companyId FROM acc_customer_type_details")
		.append(String.format(" where isDeleted=0 and %s=:%s ",PRICE_MASTER_COLUMN.COMPANY_ID,PRICE_MASTER_COLUMN.COMPANY_ID))
				.append(" order by customerTypeId desc"); 
    	if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(),
					APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}
    	Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(PRICE_MASTER_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		
		
        return this.namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, new CustomerTypeMapper());
    }
	
	
	
	private static final class CustomerTypeMapper implements RowMapper<CustomerTypeModel> {
        public CustomerTypeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CustomerTypeModel model = new CustomerTypeModel();
        	model.setCustomerTypeId(rs.getInt("customerTypeId"));
        	model.setCustomerType(rs.getString("customerType"));
        	model.setIsDeleted(rs.getInt("isDeleted"));
        	model.setCompanyId(rs.getInt("companyId"));
            return model;
        }
    }
	
	
public int deleteCustomerType(int customerId,int companyId,int deletedBy) {
		
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS))
							.append(String.format(" %s = :%s, ", CUSTOMER_TYPE_COLUMN.IS_DELETED,CUSTOMER_TYPE_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", CUSTOMER_TYPE_COLUMN.DELETED_BY, CUSTOMER_TYPE_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", CUSTOMER_TYPE_COLUMN.DELETED_DATE_TIME, CUSTOMER_TYPE_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID, CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID))
							.append(String.format(" AND %s = :%s  ", CUSTOMER_TYPE_COLUMN.COMPANY_ID, CUSTOMER_TYPE_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID.toString(), customerId)
				.addValue(CUSTOMER_TYPE_COLUMN.IS_DELETED.toString(), 1)
				.addValue(CUSTOMER_TYPE_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(CUSTOMER_TYPE_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(CUSTOMER_TYPE_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());
		System.out.println(sqlQueryCompanyUpdate.toString());
		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
}
