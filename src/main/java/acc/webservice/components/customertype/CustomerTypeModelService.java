package acc.webservice.components.customertype;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class CustomerTypeModelService {

	@Autowired
	CustomerTypeModelDao customerTypeModelDao;
	
	public CustomerTypeModel saveCutomerType(CustomerTypeModel customerTypeModel) {
		return customerTypeModelDao.saveCustomerType(customerTypeModel);
	}
	
	public List<CustomerTypeModel> getAllCustomerType() {
		return customerTypeModelDao.getAllCustomerTypeList();	
	}
	
	public List<CustomerTypeModel> getAllCustomerType(int start, int noOfRecord,int companyId) {
		return customerTypeModelDao.getAllCustomerTypeList(start, noOfRecord, companyId);	
	}
	
	
	public int deleteCustomerType(int customerId,int companyId,int deletedBy) {
		return customerTypeModelDao.deleteCustomerType(customerId, companyId, deletedBy);
	}
}
