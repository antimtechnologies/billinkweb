package acc.webservice.components.customertype;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;

public class CustomerTypeModel {

	
	private int customerTypeId;
	private String customerType;
	private int isDeleted;
	private int companyId;
	
	public CustomerTypeModel() {}
	public CustomerTypeModel(ResultSet rs) throws SQLException {
		setCustomerTypeId(rs.getInt(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID.toString()));
		setCustomerType(rs.getString(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString()));
	}
	
	
	
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getCustomerTypeId() {
		return customerTypeId;
	}
	public void setCustomerTypeId(int customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
