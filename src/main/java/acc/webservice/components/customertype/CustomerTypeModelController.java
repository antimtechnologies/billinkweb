package acc.webservice.components.customertype;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.components.ResponseTemplet.SetResponseTemplet;
import acc.webservice.components.utills.ResponseListTemplate;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/customerTypeData/")
public class CustomerTypeModelController {

	@Autowired
	CustomerTypeModelService customerTypeModelService;
	
	@Autowired
	ResponseListTemplate template;
	
	@Autowired
	SetResponseTemplet setStatus; 
	
	@RequestMapping(value="saveCutomerData", method=RequestMethod.POST)
	public Map<String, Object> saveCustomerTypeData(@RequestBody Map<String, Object> parameter){
		CustomerTypeModel custobj=new CustomerTypeModel();
		custobj.setCompanyId( ApplicationUtility.getIntValue(parameter, CUSTOMER_TYPE_COLUMN.COMPANY_ID.toString()));
		custobj.setCustomerType(ApplicationUtility.getStrValue(parameter, CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString()));
		
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", customerTypeModelService.saveCutomerType(custobj));
		return response;
	}
	
	@RequestMapping(value="getCustomerTypeList", method=RequestMethod.GET)
	public ResponseListTemplate getAllCustomerTypeData(){
		List<CustomerTypeModel> list = customerTypeModelService.getAllCustomerType();
		if (list.size() != 0) {
			template.setError(setStatus.SucessResponse("SuccessFully Get Recored"));
			template.setResult(list);
			return template;
		} else {
			template.setError(setStatus.FailsResponse("No  Recored Found !!"));
			template.setResult(null);
			return template;
		}
	}
	
	@RequestMapping(value="getCustomerTypeListView", method=RequestMethod.POST)
	public ResponseListTemplate getAllCustomerTypeDataView(@RequestBody Map<String, Object> parameter){
		
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		
		List<CustomerTypeModel> list = customerTypeModelService.getAllCustomerType(start, noOfRecord, companyId);
		if (list.size() != 0) {
			template.setError(setStatus.SucessResponse("SuccessFully Get Recored"));
			template.setResult(list);
			return template;
		} else {
			template.setError(setStatus.FailsResponse("No  Recored Found !!"));
			template.setResult(null);
			return template;
		}
	}
	
	
	@RequestMapping(value="deleteCustomerType", method=RequestMethod.POST)
	public Map<String, Object> deleteCustomerType(@RequestBody Map<String, Object> parameter){
		int customerId = ApplicationUtility.getIntValue(parameter, CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", customerTypeModelService.deleteCustomerType(customerId, companyId, deletedBy));
		return responseData;
	}
	
}
