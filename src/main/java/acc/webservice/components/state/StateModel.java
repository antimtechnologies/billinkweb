package acc.webservice.components.state;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;

public class StateModel {

	private int stateId;
	private String stateName;
	private int isDeleted;

	public StateModel() { }
	public StateModel(ResultSet resultSet) throws SQLException {
		setStateId(resultSet.getInt(STATE_TABLE_COLUMN.STATE_ID.toString()));
		setStateName(resultSet.getString(STATE_TABLE_COLUMN.STATE_NAME.toString()));
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	

}

