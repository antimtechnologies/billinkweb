package acc.webservice.components.state;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;

@Repository
@Lazy
public class StateDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<StateModel> getStateDataList() {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT SM.%s, SM.%s ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" FROM %s SM ", DATABASE_TABLE.ACC_STATE_MASTER))
				.append(String.format(" ORDER BY SM.%s", STATE_TABLE_COLUMN.STATE_NAME));

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), getStateDataResultSetExctractor());
	}

	private ResultSetExtractor<List<StateModel>> getStateDataResultSetExctractor() {

		return new ResultSetExtractor<List<StateModel>>() {

			@Override
			public List<StateModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<StateModel> stateDataList = new ArrayList<>();
				StateModel stateObj = new StateModel();
				while (rs.next()) {
					stateObj = new StateModel(rs);
					stateDataList.add(stateObj);
				}
				return stateDataList;
			}
		};
	}

	public StateModel insertStateData(StateModel stateModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_STATE_MASTER))
				.append(String.format(" (%s) VALUES ( :%s) ", STATE_TABLE_COLUMN.STATE_NAME, STATE_TABLE_COLUMN.STATE_NAME));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(STATE_TABLE_COLUMN.STATE_NAME.toString(), stateModel.getStateName());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		stateModel.setStateId(holder.getKey().intValue());
		return stateModel;
	}
}
