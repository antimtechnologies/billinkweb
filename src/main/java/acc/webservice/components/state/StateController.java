package acc.webservice.components.state;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Lazy
@RequestMapping("/stateData/")
public class StateController {

	@Autowired
	private StateService stateService;
	
	@RequestMapping(value="getStateList", method=RequestMethod.POST, produces = "application/json") 
	public Map<String,Object> getStateDataList() {
		Map<String,Object> stateList = new HashMap<String, Object>();
		stateList.put("data", stateService.getStateDataList());
		return stateList;
	}
	
//	@RequestMapping(value="getStateFromBoot", method=RequestMethod.POST, produces = "application/json")
	
	@RequestMapping(value="getStateFromBoot",method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> getDateFromSpringBoot() {
//		Map<String,Object> stateList = new HashMap<String, Object>();
//		stateList.put("data", stateService.getStateDataList());
//		return stateList;
		
		
//		System.err.println("call from mvc"); 
//		RestTemplate restTemplate = new RestTemplate();
//	    restTemplate.setMessageConverters(Arrays.asList(new MappingJackson2HttpMessageConverter()));
//	    ResponseEntity<?> response = restTemplate.getForEntity("http://localhost:8090/user/state/getall", StateModel.class);
//		return response;
		return null;
	}
	


//	@RequestMapping(value="getobject",method = RequestMethod.GET)
//	public ResponseEntity<String> getObject() {
//		RestTemplate restTemplate = new RestTemplate();
//		String fooResourceUrl= "http://localhost:8090/user/state/getall";
//		ResponseEntity<String> response
//		  = restTemplate.getForEntity(fooResourceUrl , String.class);
//	System.err.println("response :"+response);
//		return response;
//   }
	
//	@RequestMapping(value="getpurchase",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> getPurchase(HttpServletRequest request) {
//		RestTemplate restTemplate = new RestTemplate();
//		int companyId = Integer.parseInt(request.getParameter("companyId"));
//		String fooResourceUrl  = "http://localhost:8090/user/purchase/findbycompanyid?companyId="+companyId;
//		System.err.println("url :"+fooResourceUrl);
//		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
//		return response;
//	}
	
}
