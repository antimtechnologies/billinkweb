package acc.webservice.components.state;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class StateService {

	@Autowired
	private StateDAO stateDAO;

	public List<StateModel> getStateDataList() {
		return stateDAO.getStateDataList();
	}
	
	public StateModel insertStateData(StateModel stateModel) {
		return stateDAO.insertStateData(stateModel);
	}

}
