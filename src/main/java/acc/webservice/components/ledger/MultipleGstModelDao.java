package acc.webservice.components.ledger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DOCUMENT_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_MULTIPLE_GST_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Repository
@Lazy
public class MultipleGstModelDao {

	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@SuppressWarnings("unchecked")
	int[] saveMultipleGstData(List<MultipleGstModel> multipleGstModel,int ledgerId) {
		
		StringBuilder sqlQueryMultipleGstSave = new StringBuilder();
		sqlQueryMultipleGstSave.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS))
							.append(String.format("( %s, %s, %s, %s, %s )", LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST, LEDGER_MULTIPLE_GST_COLUMN.CITY, LEDGER_MULTIPLE_GST_COLUMN.STATE, LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, :%s, :%s )", LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST, LEDGER_MULTIPLE_GST_COLUMN.CITY, LEDGER_MULTIPLE_GST_COLUMN.STATE, LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID));

		List<Map<String, Object>> multipleGstMapList = new ArrayList<>();
		Map<String, Object> multipleGstLMap;
		for (MultipleGstModel multipleGstModels : multipleGstModel) {
			multipleGstLMap = new HashMap<>();

			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST.toString(), multipleGstModels.getMultipleGst());
			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.CITY.toString(), multipleGstModels.getCity());
			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.STATE.toString(), multipleGstModels.getState());
			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.ADDRESS.toString(), multipleGstModels.getAddress());
			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
			multipleGstMapList.add(multipleGstLMap);
		}
		return namedParameterJdbcTemplate.batchUpdate(sqlQueryMultipleGstSave.toString(), multipleGstMapList.toArray(new HashMap[0]));
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	int DeleteFromGstData(List<MultipleGstModel> multipleGstModel,int ledgerId) {
		
		StringBuilder sqlQueryMultipleGstSave = new StringBuilder();
		sqlQueryMultipleGstSave.append(String.format("Delete from %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS))
							.append(String.format(" where %s= :%s ", LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID));
						

		
		Map<String, Object> multipleGstLMap= new HashMap<>();

			
			multipleGstLMap.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
			
		return namedParameterJdbcTemplate.update(sqlQueryMultipleGstSave.toString(), multipleGstLMap);
		
	}

}
