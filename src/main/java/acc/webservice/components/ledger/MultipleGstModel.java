package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_MULTIPLE_GST_COLUMN;

public class MultipleGstModel {

	
	private int multipleGstId;
	private String multipleGst,city,state,address;
	private LedgerModel ledgerModel;
	private int Stateid;
	
	public MultipleGstModel() {
		
	}
	
	public MultipleGstModel(ResultSet rs) throws SQLException {
		setMultipleGstId(rs.getInt(LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST_ID.toString()));
		setMultipleGst(rs.getString(LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST.toString()));
		setCity(rs.getString(LEDGER_MULTIPLE_GST_COLUMN.CITY.toString()));
		setState(rs.getString(LEDGER_MULTIPLE_GST_COLUMN.STATE.toString()));
		setAddress(rs.getString(LEDGER_MULTIPLE_GST_COLUMN.ADDRESS.toString()));
		setStateid(rs.getInt(LEDGER_DETAILS_COLUMN.STATE_ID.toString()));
	}
	
	

	public int getStateid() {
		return Stateid;
	}

	public void setStateid(int stateid) {
		Stateid = stateid;
	}

	public int getMultipleGstId() {
		return multipleGstId;
	}

	public void setMultipleGstId(int multipleGstId) {
		this.multipleGstId = multipleGstId;
	}

	public String getMultipleGst() {
		return multipleGst;
	}

	public void setMultipleGst(String multipleGst) {
		this.multipleGst = multipleGst;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LedgerModel getLedgerModel() {
		return ledgerModel;
	}

	public void setLedgerModel(LedgerModel ledgerModel) {
		this.ledgerModel = ledgerModel;
	}
	
	
	
}
