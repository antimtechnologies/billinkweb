package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.enums.DatabaseEnum.LEDGER_PURCHASE_DETAILS;
import acc.webservice.global.utils.DateUtility;

public class LedgerPurchaseDetailsModel {

	private int purchaseId;
	private String purchaseDate;
	private String billNumber;
	private int itemId;
	private String itemName;
	private double quantity;
	private String batchNo;
	private String mfgDate,expDate;
	
	public LedgerPurchaseDetailsModel() {
		
	}
	public LedgerPurchaseDetailsModel(ResultSet rs) throws SQLException{
		Timestamp fetchedPurchaseDate = rs.getTimestamp(LEDGER_PURCHASE_DETAILS.PURCHASE_DATE.toString());
		if (fetchedPurchaseDate != null) {
			setPurchaseDate(DateUtility.converDateToUserString(fetchedPurchaseDate));
		}
		setPurchaseId(rs.getInt(LEDGER_PURCHASE_DETAILS.PURCHASE_ID.toString()));
		setBillNumber(rs.getString(LEDGER_PURCHASE_DETAILS.BILL_NUMBER.toString()));
		setItemId(rs.getInt(LEDGER_PURCHASE_DETAILS.ITEM_ID.toString()));
		setItemName(rs.getString(LEDGER_PURCHASE_DETAILS.ITEM_NAME.toString()));
		setQuantity(rs.getDouble(LEDGER_PURCHASE_DETAILS.QUANTITY.toString()));
		setBatchNo(rs.getString(LEDGER_PURCHASE_DETAILS.BATCH_NO.toString()));
		Timestamp fetchedMfgDateTime = rs.getTimestamp(LEDGER_PURCHASE_DETAILS.MFG_DATE.toString());
		setMfgDate(DateUtility.converDateToUserString(fetchedMfgDateTime));
		Timestamp fetchedExpDateTime = rs.getTimestamp(LEDGER_PURCHASE_DETAILS.EXP_DATE.toString());
		setExpDate(DateUtility.converDateToUserString(fetchedExpDateTime));
	} 
	
	public int getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
	
	
	
}
