package acc.webservice.components.ledger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class LedgerService {

	@Autowired
	private LedgerDAO ledgerDAO;

	@Autowired
	private LedgerDocumentDAO ledgerDocumentDAO;
	
	@Autowired
	private MultipleGstModelDao multipleGstModelDao;

	public List<LedgerModel> getBasicLedgerDetailsList(int companyId, int start, int numberOfRecord, String ledgerName) {
		return ledgerDAO.getBasicLedgerDetailsList(companyId, start, numberOfRecord, ledgerName);
	}

	public List<LedgerModel> getLedgerListData(int companyId, int start, int numberOfRecord, String ledgerName) {
		return ledgerDAO.getLedgerListData(companyId, start, numberOfRecord, ledgerName);
	}

	public LedgerModel getLedgerDetailsById(int companyId, int ledgerId) {
		LedgerModel ledgerModel = ledgerDAO.getLedgerDetailsById(companyId, ledgerId);
		ledgerModel.setLedgerDocumentList(ledgerDocumentDAO.getLedgerDocumentList(ledgerId));
		ledgerModel.setMultipleGstList(ledgerDAO.getMultipleGstData(ledgerId));
		return ledgerModel;
	}

	public LedgerModel saveLedgerData(LedgerModel ledgerModel) throws AccountingSofwareException {

		if (isLedgerWithSameGSTExist(ledgerModel.getCompanyId(), ledgerModel.getGstNumber(), ledgerModel.getLedgerId())) {
			throw new AccountingSofwareException("Ledger with same GST Number is already exists.");
		}
		LedgerModel model = ledgerDAO.saveLedgerData(ledgerModel);
		
		if (ApplicationUtility.getSize(ledgerModel.getLedgerDocumentList()) > 0) {
			ledgerDocumentDAO.saveLedgerDocumentData(ledgerModel.getLedgerDocumentList(), ledgerModel.getLedgerId());
		}
		
		if (ApplicationUtility.getSize(ledgerModel.getMultipleGstList()) > 0) {
			multipleGstModelDao.saveMultipleGstData(ledgerModel.getMultipleGstList(), ledgerModel.getLedgerId());
		}
		
		return model;
	}
	
	public List<MultipleGstModel> getMultipleGstData(int ledgerId) throws AccountingSofwareException {

		return ledgerDAO.getMultipleGstData(ledgerId);
	}
	public List<MultipleGstModel> getMultipleGstDataspcndn(int ledgerId) throws AccountingSofwareException {

		return ledgerDAO.getMultipleGstDataspcndn(ledgerId);
	}

	public List<MultipleGstModel> getMultipleGstDataByCityNameAndId(int ledgerId,String cityName) throws AccountingSofwareException {

		return ledgerDAO.getMultipleGstByCityNameAndLedgerID(ledgerId, cityName);
	}
	
	
	private boolean isLedgerWithSameGSTExist(int companyId, String gstNumber, int ledgerId) {

		if (ApplicationUtility.getSize(gstNumber) < 1) {
			return false;
		}

		return ledgerDAO.isLedgerWithSameGSTExist(companyId, gstNumber, ledgerId) > 0;
	}

	public LedgerModel updateLedgerData(LedgerModel ledgerModel) throws AccountingSofwareException {

		if (isLedgerWithSameGSTExist(ledgerModel.getCompanyId(), ledgerModel.getGstNumber(), ledgerModel.getLedgerId())) {
			throw new AccountingSofwareException("Ledger with same GST Number is already exists.");
		}

		ledgerDocumentDAO.deleteLedgerDocumentData(ledgerModel.getLedgerId());
		if (ApplicationUtility.getSize(ledgerModel.getLedgerDocumentList()) > 0) {
			ledgerDocumentDAO.saveLedgerDocumentData(ledgerModel.getLedgerDocumentList(), ledgerModel.getLedgerId());
		}
		if (ApplicationUtility.getSize(ledgerModel.getMultipleGstList()) > 0) {
			multipleGstModelDao.DeleteFromGstData(ledgerModel.getMultipleGstList(), ledgerModel.getLedgerId());
			multipleGstModelDao.saveMultipleGstData(ledgerModel.getMultipleGstList(), ledgerModel.getLedgerId());
		}
		return ledgerDAO.updateLedgerData(ledgerModel);
	}

	public int deleteLedgerData(int companyId, int ledgerId, int deletedBy) {
		ledgerDocumentDAO.deleteLedgerDocumentData(ledgerId);
		return ledgerDAO.deleteLedgerData(companyId, ledgerId, deletedBy);
	}

	public int getLedgerIdByName(int companyId, String ledgerName ) {
		return ledgerDAO.getLedgerIdByName(companyId, ledgerName);
	}

	public int getLedgerIdByGSTNumber(int companyId, String gstNumber ) {
		return ledgerDAO.getLedgerIdByGSTNumber(companyId, gstNumber);
	}

	public LedgerModel getBasicLedgerDetailByLedgerId(int ledgerId, int companyId) {
		return ledgerDAO.getBasicLedgerDetailByLedgerId(ledgerId, companyId);
	}
	
	public double getDiscount(int ledgerId) {
		return ledgerDAO.getDiscount(ledgerId);
	}
	
	public List<LedgerModel> getLedgerDetailsList(int companyId) {
		return ledgerDAO.getLedgerDetailsList(companyId);
	}
}
