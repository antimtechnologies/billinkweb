package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class LedgerDocumentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<LedgerDocumentModel> getLedgerDocumentList(int ledgerId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s, %s ", LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL_ID, LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL, LEDGER_DOCUMENT_COLUMN.DOCUMENT_TYPE, LEDGER_DOCUMENT_COLUMN.DESCRIPTION))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_LEDGER_DOCUMENT_DETAILS))
			.append(String.format(" WHERE %s = :%s AND %s = 0  ", LEDGER_DOCUMENT_COLUMN.LEDGER_ID, LEDGER_DOCUMENT_COLUMN.LEDGER_ID, LEDGER_DOCUMENT_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(LEDGER_DOCUMENT_COLUMN.LEDGER_ID.toString(), ledgerId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getLedgerDocumentResultExctractor());
	}

	private ResultSetExtractor<List<LedgerDocumentModel>> getLedgerDocumentResultExctractor() {
		return new ResultSetExtractor<List<LedgerDocumentModel>>() {
			@Override
			public List<LedgerDocumentModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<LedgerDocumentModel> ledgerDocumentModelList = new ArrayList<>();
				while (rs.next()) {
					ledgerDocumentModelList.add(new LedgerDocumentModel(rs));
				}
				return ledgerDocumentModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveLedgerDocumentData(List<LedgerDocumentModel> ledgerDocumentModelList, int ledgerId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_LEDGER_DOCUMENT_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s ) ", LEDGER_DOCUMENT_COLUMN.DOCUMENT_TYPE, LEDGER_DOCUMENT_COLUMN.DESCRIPTION, LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL, LEDGER_DOCUMENT_COLUMN.LEDGER_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", LEDGER_DOCUMENT_COLUMN.DOCUMENT_TYPE, LEDGER_DOCUMENT_COLUMN.DESCRIPTION, LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL, LEDGER_DOCUMENT_COLUMN.LEDGER_ID));

		List<Map<String, Object>> ledgerDocumentURLMapList = new ArrayList<>();
		Map<String, Object> ledgerDocumentURLMap;
		for (LedgerDocumentModel ledgerDocumentModel : ledgerDocumentModelList) {
			ledgerDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(ledgerDocumentModel.getDocumentUrl())) {			
				ledgerDocumentModel.setDocumentUrl(ledgerDocumentModel.getDocumentUrl().replace(ApplicationUtility.getServerURL(), ""));
			}

			ledgerDocumentURLMap.put(LEDGER_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString(), ledgerDocumentModel.getDocumentType());
			ledgerDocumentURLMap.put(LEDGER_DOCUMENT_COLUMN.DESCRIPTION.toString(), ledgerDocumentModel.getDescription());
			ledgerDocumentURLMap.put(LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL.toString(), ledgerDocumentModel.getDocumentUrl());
			ledgerDocumentURLMap.put(LEDGER_DOCUMENT_COLUMN.LEDGER_ID.toString(), ledgerId);
			ledgerDocumentURLMapList.add(ledgerDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), ledgerDocumentURLMapList.toArray(new HashMap[0]));
	}

	int deleteLedgerDocumentData(int ledgerId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", DATABASE_TABLE.ACC_LEDGER_DOCUMENT_DETAILS, LEDGER_DOCUMENT_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", LEDGER_DOCUMENT_COLUMN.LEDGER_ID, LEDGER_DOCUMENT_COLUMN.LEDGER_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, ledgerId });
	}

}
