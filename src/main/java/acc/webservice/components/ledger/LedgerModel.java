package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.state.StateModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.global.utils.DateUtility;

public class LedgerModel {

	private int ledgerId;
	private String ledgerName;
	private long mobileNumber;
	private String address;
	private int pinCode;
	private StateModel state;
	private String contactPersonName;
	private String ledgerGroup;
	private String gstType;
	private String gstNumber;
	private double openingBalance;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel deletedBy;
	private String sacCode;
	private String deletedDateTime;
	private int companyId;
	private double taxCode;
	private double ledgerCharge;
	private String cityName;
	private String panNumber;
	private List<LedgerDocumentModel> ledgerDocumentList;
	private int creditPeriod;
	private int creditLimit;
	private String LedgerCode;
	private int customerTypeId;
	private int priceId;
	private Boolean schemeOneId;
	private Boolean schemeTwoId;
	private String periodName;
	private List<MultipleGstModel> multipleGstList;
	
	private int excellDocumentId;
	
	public LedgerModel(ResultSet rs) throws SQLException {
		setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));
		setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		setCityName(rs.getString(LEDGER_DETAILS_COLUMN.CITY_NAME.toString()));
		setMobileNumber(rs.getLong(LEDGER_DETAILS_COLUMN.MOBILE_NUMBER.toString()));
		setAddress(rs.getString(LEDGER_DETAILS_COLUMN.ADDRESS.toString()));
		setPinCode(rs.getInt(LEDGER_DETAILS_COLUMN.PINCODE.toString()));
		setPanNumber(rs.getString(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString()));
		setContactPersonName(rs.getString(LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME.toString()));
		setLedgerGroup(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_GROUP.toString()));
		setGstType(rs.getString(LEDGER_DETAILS_COLUMN.GST_TYPE.toString()));
		setGstNumber(rs.getString(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString()));
		setOpeningBalance(rs.getDouble(LEDGER_DETAILS_COLUMN.OPENING_BALANCE.toString()));
		setTaxCode(rs.getDouble(LEDGER_DETAILS_COLUMN.TAX_CODE.toString()));
		setLedgerCharge(rs.getDouble(LEDGER_DETAILS_COLUMN.LEDGER_CHARGE.toString()));
		setCreditPeriod(rs.getInt(LEDGER_DETAILS_COLUMN.CREDIT_PERIOD.toString()));
		setCreditLimit(rs.getInt(LEDGER_DETAILS_COLUMN.CREDIT_LIMIT.toString()));
		setLedgerCode(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_CODE.toString()));
		setCustomerTypeId(rs.getInt(LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID.toString()));
		setPriceId(rs.getInt(LEDGER_DETAILS_COLUMN.PRICE_ID.toString()));
		setSchemeOneId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString()));
		setSchemeTwoId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString()));
		setPeriodName(rs.getString(LEDGER_DETAILS_COLUMN.PeriodName.toString()));
		setState(new StateModel(rs));
		UserModel objAddedBy = new UserModel();
		objAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		objAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(objAddedBy);
		Timestamp fetchedAddedDateTime = rs.getTimestamp(LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDateTime));

		UserModel objModifiedBy = new UserModel();
		objModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		objModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(objModifiedBy);
		Timestamp fetchedModifiedDateTime = rs.getTimestamp(LEDGER_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDateTime != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDateTime));
		}
	}

	public LedgerModel() {
	}
	
	

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public int getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public StateModel getState() {
		return state;
	}

	public void setState(StateModel state) {
		this.state = state;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getLedgerGroup() {
		return ledgerGroup;
	}

	public void setLedgerGroup(String ledgerGroup) {
		this.ledgerGroup = ledgerGroup;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public double getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public double getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(double taxCode) {
		this.taxCode = taxCode;
	}

	public double getLedgerCharge() {
		return ledgerCharge;
	}

	public void setLedgerCharge(double ledgerCharge) {
		this.ledgerCharge = ledgerCharge;
	}

	public List<LedgerDocumentModel> getLedgerDocumentList() {
		return ledgerDocumentList;
	}

	public void setLedgerDocumentList(List<LedgerDocumentModel> ledgerDocumentList) {
		this.ledgerDocumentList = ledgerDocumentList;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getSacCode() {
		return sacCode;
	}

	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	
	public int getExcellDocumentId() {
		return excellDocumentId;
	}

	public void setExcellDocumentId(int excellDocumentId) {
		this.excellDocumentId = excellDocumentId;
	}

	
	public int getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(int creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getLedgerCode() {
		return LedgerCode;
	}

	public void setLedgerCode(String ledgerCode) {
		LedgerCode = ledgerCode;
	}

	public int getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(int customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public int getPriceId() {
		return priceId;
	}

	public void setPriceId(int priceId) {
		this.priceId = priceId;
	}

	public Boolean getSchemeOneId() {
		return schemeOneId;
	}

	public void setSchemeOneId(Boolean schemeOneId) {
		this.schemeOneId = schemeOneId;
	}

	public Boolean getSchemeTwoId() {
		return schemeTwoId;
	}

	public void setSchemeTwoId(Boolean schemeTwoId) {
		this.schemeTwoId = schemeTwoId;
	}

	public List<MultipleGstModel> getMultipleGstList() {
		return multipleGstList;
	}

	public void setMultipleGstList(List<MultipleGstModel> multipleGstList) {
		this.multipleGstList = multipleGstList;
	}

	@Override
	public String toString() {
		return "LedgerModel [ledgerId=" + ledgerId + ", ledgerName=" + ledgerName + ", mobileNumber=" + mobileNumber
				+ ", address=" + address + ", pinCode=" + pinCode + ", contactPersonName="
				+ contactPersonName + ", ledgerGroup=" + ledgerGroup + ", gstType=" + gstType + ", gstNumber="
				+ gstNumber + ", openingBalance=" + openingBalance + ", addedDateTime=" + addedDateTime
				+ ", modifiedDateTime=" + modifiedDateTime + ", deletedDateTime=" + deletedDateTime + "]";
	}

}
