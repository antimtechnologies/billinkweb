package acc.webservice.components.ledger;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_MULTIPLE_GST_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/ledgerData/")
public class LedgerController {

	@Autowired
	private LedgerService ledgerService;

	@RequestMapping(value = "getLedgerNameIdListData", method = RequestMethod.POST)
	public Map<String, Object> getBasicLedgerDetailsList(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		String ledgerName = ApplicationUtility.getStrValue(params, LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString());
		int start = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getBasicLedgerDetailsList(companyId, start, noOfRecord, ledgerName));
		return responseData;
	}

	@RequestMapping(value = "getLedgerListData", method = RequestMethod.POST)
	public Map<String, Object> getLedgerListData(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String ledgerName = ApplicationUtility.getStrValue(params, LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getLedgerListData(companyId, start, numberOfRecord, ledgerName));
		return responseData;
	}

	@RequestMapping(value = "getLedgerDetailsById", method = RequestMethod.POST)
	public Map<String, Object> getLedgerDetailsById(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getLedgerDetailsById(companyId, ledgerId));
		return responseData;
	}

	@RequestMapping(value = "saveLedgerData", method = RequestMethod.POST)
	public Map<String, Object> saveLedgerData(@RequestBody LedgerModel ledgerModel) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.saveLedgerData(ledgerModel));
		return responseData;
	}
	
	@RequestMapping(value = "getDiscount", method = RequestMethod.POST)
	public Map<String, Object> getDiscount(@RequestBody Map<String, Object> params) throws AccountingSofwareException {
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getDiscount(ledgerId));
		
		return responseData;
	}
	
	@RequestMapping(value = "getMultipleGstData", method = RequestMethod.POST)
	public Map<String, Object> getMultipleGstData(@RequestBody Map<String, Object> params) throws AccountingSofwareException {
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getMultipleGstData(ledgerId));
		return responseData;
	}
	
	
	@RequestMapping(value = "getMultipleGstDataspcndn", method = RequestMethod.POST)
	public Map<String, Object> getMultipleGstDataspcndn(@RequestBody Map<String, Object> params) throws AccountingSofwareException {
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getMultipleGstDataspcndn(ledgerId));
		return responseData;
	}
	
	
	@RequestMapping(value = "getMultipleGstDataByCityName", method = RequestMethod.POST)
	public Map<String, Object> getMultipleGstDataByCityName(@RequestBody Map<String, Object> params) throws AccountingSofwareException {
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		String cityName = ApplicationUtility.getStrValue(params, LEDGER_MULTIPLE_GST_COLUMN.CITY.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getMultipleGstDataByCityNameAndId(ledgerId, cityName));
		return responseData;
	}
	

	@RequestMapping(value = "updateLedgerData", method = RequestMethod.POST)
	public Map<String, Object> updateLedgerData(@RequestBody LedgerModel ledgerModel) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.updateLedgerData(ledgerModel));
		return responseData;
	}

	@RequestMapping(value = "deleteLedgerData", method = RequestMethod.POST)
	public Map<String, Object> deleteLedgerData(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int ledgerId = ApplicationUtility.getIntValue(params, LEDGER_DETAILS_COLUMN.LEDGER_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.deleteLedgerData(companyId, ledgerId, deletedBy));
		return responseData;
	}
	

	@RequestMapping(value="getallCredit",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getAllCreditLedger(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "http://localhost:8090/user/taxesledger/getallcreditcharget";
				
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}


	@RequestMapping(value="getallDebit",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getAllDebitLedger(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "http://localhost:8090/user/taxesledger/getalldebitcharge";
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}

	
	@RequestMapping(value="creditByLedger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getCreditByLedger(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
				if (request.getParameter("ledgerId") != null) {
					int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
					fooResourceUrl  = "http://localhost:8090/user/taxesledger/getallcreditchargebyledgerid?ledgerId="+ledgerId;
				}
		
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	@RequestMapping(value="debitByLedger",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getDebitByLedger(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
				if (request.getParameter("ledgerId") != null) {
					int ledgerId = Integer.parseInt(request.getParameter("ledgerId"));
					fooResourceUrl  = "http://localhost:8090/user/taxesledger/getalldebitchargebyledgerid?ledgerId="+ledgerId;
				}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	
	@RequestMapping(value = "getLedgerListDataForReport", method = RequestMethod.POST)
	public Map<String, Object> getLedgerDetailsList(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", ledgerService.getLedgerDetailsList(companyId));
		return responseData;
	}
}
