package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.state.StateModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_MULTIPLE_GST_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class LedgerDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<LedgerModel> getBasicLedgerDetailsList(int companyId, int start, int numberOfRecord, String ledgerName) {

		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PAN_NUMBER, LEDGER_DETAILS_COLUMN.CITY_NAME))
						.append(String.format(" LD.%s, LD.%s, LD.%s,  ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID, LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID ))
						.append(String.format(" LD.%s, LD.%s, SM.%s, SM.%s ", LEDGER_DETAILS_COLUMN.GST_TYPE, LEDGER_DETAILS_COLUMN.SAC_CODE, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" FROM %s LD ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
						.append(String.format(" LEFT JOIN %s SM ON LD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" WHERE LD.%s = 0 AND ( %s LIKE :%s OR  %s LIKE :%s ) ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_CODE,LEDGER_DETAILS_COLUMN.LEDGER_CODE))
						.append(String.format(" AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" ORDER BY LD.%s ASC ", LEDGER_DETAILS_COLUMN.LEDGER_NAME));

		Map<String, Object> parameters = new HashMap<>();
		if (numberOfRecord > 0) {
			sqlQueryToFetchData.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START, APPLICATION_GENERIC_ENUM.NO_OF_RECORD));
		}

		parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString(), "%" + ledgerName + "%");
		parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_CODE.toString(), "%" + ledgerName + "%");
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLedgerNameIdDetailsListExtractor());
	}

	private ResultSetExtractor<List<LedgerModel>> getLedgerNameIdDetailsListExtractor() {
		return new ResultSetExtractor<List<LedgerModel>>() {
			@Override
			public List<LedgerModel> extractData(ResultSet rs) throws SQLException {
				List<LedgerModel> ledgerList = new ArrayList<>();
				LedgerModel ledgerModel;
				while (rs.next()) {
					ledgerModel = new LedgerModel();
					ledgerModel.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
					ledgerModel.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
					ledgerModel.setPanNumber(rs.getString(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString()));
					ledgerModel.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));
					ledgerModel.setTaxCode(rs.getDouble(LEDGER_DETAILS_COLUMN.TAX_CODE.toString()));
					ledgerModel.setLedgerCharge(rs.getDouble(LEDGER_DETAILS_COLUMN.LEDGER_CHARGE.toString()));
					ledgerModel.setGstType(rs.getString(LEDGER_DETAILS_COLUMN.GST_TYPE.toString()));
					ledgerModel.setAddress(rs.getString(LEDGER_DETAILS_COLUMN.ADDRESS.toString()));
					ledgerModel.setGstNumber(rs.getString(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString()));
					ledgerModel.setCityName(rs.getString(LEDGER_DETAILS_COLUMN.CITY_NAME.toString()));
					ledgerModel.setPriceId(rs.getInt(LEDGER_DETAILS_COLUMN.PRICE_ID.toString()));
					ledgerModel.setSchemeOneId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString()));
					ledgerModel.setSchemeTwoId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString()));
					StateModel stateModel = new StateModel(rs);
					ledgerModel.setState(stateModel);
					ledgerList.add(ledgerModel);
				}
				return ledgerList;
			}
		};
	}

	List<LedgerModel> getLedgerListData(int companyId, int start, int numberOfRecord, String ledgerName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.MOBILE_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.PINCODE, LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME, LEDGER_DETAILS_COLUMN.LEDGER_GROUP, LEDGER_DETAILS_COLUMN.GST_TYPE))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.OPENING_BALANCE, LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME, LEDGER_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.SAC_CODE, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE, LEDGER_DETAILS_COLUMN.CITY_NAME, LEDGER_DETAILS_COLUMN.PAN_NUMBER,LEDGER_DETAILS_COLUMN.CREDIT_LIMIT,LEDGER_DETAILS_COLUMN.CREDIT_PERIOD))
						.append(String.format("LD.%s,LD.%s,LD.%s,LD.%s,LD.%s,LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_CODE,LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID,LEDGER_DETAILS_COLUMN.PRICE_ID,LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID,LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID,LEDGER_DETAILS_COLUMN.PeriodName))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
						.append(String.format(" FROM %s LD ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
						.append(String.format(" INNER JOIN %s AUM ON LD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, LEDGER_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s SM ON LD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" LEFT JOIN %s MUM ON LD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, LEDGER_DETAILS_COLUMN.LAST_MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE LD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s ) ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" ORDER BY LD.%s DESC LIMIT :start, :noOfRecord ", LEDGER_DETAILS_COLUMN.LEDGER_ID));


		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString(), "%" + ledgerName + "%");
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLedgerDetailsListExtractor());
	}
	
	private ResultSetExtractor<List<LedgerModel>> getLedgerDetailsListExtractor() {

		return new ResultSetExtractor<List<LedgerModel>>() {
			@Override
			public List<LedgerModel> extractData(ResultSet rs) throws SQLException {
				List<LedgerModel> ledgerList = new ArrayList<>();
				while (rs.next()) {
					ledgerList.add(new LedgerModel(rs));
				}
				return ledgerList;
			}
		};
	}
	
	List<MultipleGstModel> getMultipleGstData(int ledgerId){
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format("select %s,%s,%s,%s,%s,0 as %s ", LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST, 
										LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST_ID, LEDGER_MULTIPLE_GST_COLUMN.STATE, LEDGER_MULTIPLE_GST_COLUMN.CITY, LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,LEDGER_DETAILS_COLUMN.STATE_ID))
							.append(String.format("	from %s where %s=:%s",COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS,
									LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
		List<MultipleGstModel> multiplegstcitylist= namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getgstDetailsExtractor());
		
		return multiplegstcitylist;
	}
	List<MultipleGstModel> getMultipleGstDataspcndn(int ledgerId){
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format("select %s,%s,%s,%s,%s,asm.%s as %s ", LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST, 
										LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST_ID, LEDGER_MULTIPLE_GST_COLUMN.STATE, LEDGER_MULTIPLE_GST_COLUMN.CITY, LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,STATE_TABLE_COLUMN.STATE_ID,LEDGER_DETAILS_COLUMN.STATE_ID))
							.append(String.format("	from %s as aclmgd",COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS))
							.append(String.format("	left join %s as asm on asm.%s=aclmgd.%s ",DATABASE_TABLE.ACC_STATE_MASTER,STATE_TABLE_COLUMN.STATE_NAME,LEDGER_MULTIPLE_GST_COLUMN.STATE))
							.append(String.format("where %s=:%s",LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
		List<MultipleGstModel> multiplegstcitylist= namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getgstDetailsExtractor());
		
		// FOR ADD PRIMARY CITY IN GSTLIST CONVERT LEDGER DATA RESULTSET TO MULTIPLE GST RESULTSET
		sqlQueryToFetchData=new StringBuilder();
		sqlQueryToFetchData.append(String.format("select %s as %s,0 as %s,%s as %s,%s as %s,'' as %s,%s ",LEDGER_DETAILS_COLUMN.GST_NUMBER,LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST,
				LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST_ID,LEDGER_DETAILS_COLUMN.CITY_NAME,LEDGER_MULTIPLE_GST_COLUMN.CITY,LEDGER_DETAILS_COLUMN.STATE_ID,LEDGER_MULTIPLE_GST_COLUMN.STATE,LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,LEDGER_DETAILS_COLUMN.STATE_ID))
				.append(String.format("from %s where %s= :%s",COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS,LEDGER_DETAILS_COLUMN.LEDGER_ID,LEDGER_DETAILS_COLUMN.LEDGER_ID));
		multiplegstcitylist.addAll(namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getgstDetailsExtractor()));
		return multiplegstcitylist;
	}
	
	
	Double getDiscount(int ledgerId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format("select %s from %s ",PRICE_MASTER_COLUMN.DISCOUNT,DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS))
		.append(String.format("where %s in ",PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
		.append(String.format("(select %s from %s where %s= :%s)",LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID,COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS,
				LEDGER_DETAILS_COLUMN.LEDGER_ID,LEDGER_DETAILS_COLUMN.LEDGER_ID));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getNormalDiscount());
	}

	
	private ResultSetExtractor<Double> getNormalDiscount() {
		return new ResultSetExtractor<Double>() {
			@Override
			public Double extractData(ResultSet rs) throws SQLException, DataAccessException {
				Double normaldiscount = 0d;
				while(rs.next()) {
					normaldiscount = rs.getDouble(PRICE_MASTER_COLUMN.DISCOUNT.toString());
				}
				return normaldiscount;
			}
		};
	}
	
	
	
	private ResultSetExtractor<List<MultipleGstModel>> getgstDetailsExtractor() {

		return new ResultSetExtractor<List<MultipleGstModel>>() {
			@Override
			public List<MultipleGstModel> extractData(ResultSet rs) throws SQLException {
				List<MultipleGstModel> multipleGstDetailslist = new ArrayList<MultipleGstModel>();
				while (rs.next()) {
					multipleGstDetailslist.add(new MultipleGstModel(rs));
				}
				return multipleGstDetailslist;
			}
		};
	}
	
	
	List<MultipleGstModel> getMultipleGstByCityNameAndLedgerID(int ledgerId,String cityName){
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format("select %s,%s,%s,%s,%s,0 as %s ", LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST, 
										LEDGER_MULTIPLE_GST_COLUMN.MULTIPLE_GST_ID, LEDGER_MULTIPLE_GST_COLUMN.STATE, LEDGER_MULTIPLE_GST_COLUMN.CITY, LEDGER_MULTIPLE_GST_COLUMN.ADDRESS,LEDGER_DETAILS_COLUMN.STATE_ID))
							.append(String.format("	from %s where %s=:%s AND %s=:%s",COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS,
									LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID,LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID,LEDGER_MULTIPLE_GST_COLUMN.CITY,LEDGER_MULTIPLE_GST_COLUMN.CITY));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_MULTIPLE_GST_COLUMN.LEDGER_ID.toString(), ledgerId);
		parameters.put(LEDGER_MULTIPLE_GST_COLUMN.CITY.toString(), cityName);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getgstDetailsExtractorByCityName());
	}
	
	private ResultSetExtractor<List<MultipleGstModel>> getgstDetailsExtractorByCityName() {

		return new ResultSetExtractor<List<MultipleGstModel>>() {
			@Override
			public List<MultipleGstModel> extractData(ResultSet rs) throws SQLException {
				List<MultipleGstModel> multipleGstDetailslist = new ArrayList<MultipleGstModel>();
				while (rs.next()) {
					multipleGstDetailslist.add(new MultipleGstModel(rs));
				}
				return multipleGstDetailslist;
			}
		};
	}
	
	LedgerModel getLedgerDetailsById(int companyId, int ledgerId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.MOBILE_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.PINCODE, LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME, LEDGER_DETAILS_COLUMN.LEDGER_GROUP, LEDGER_DETAILS_COLUMN.GST_TYPE))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.OPENING_BALANCE, LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME, LEDGER_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.SAC_CODE, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE, LEDGER_DETAILS_COLUMN.CITY_NAME, LEDGER_DETAILS_COLUMN.PAN_NUMBER,LEDGER_DETAILS_COLUMN.CREDIT_LIMIT,LEDGER_DETAILS_COLUMN.CREDIT_PERIOD))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format("LD.%s,LD.%s,LD.%s,LD.%s,LD.%s,LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_CODE,LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID,LEDGER_DETAILS_COLUMN.PRICE_ID,LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID,LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID,LEDGER_DETAILS_COLUMN.PeriodName))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" FROM %s LD ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
						.append(String.format(" INNER JOIN %s AUM ON LD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, LEDGER_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s SM ON LD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" LEFT JOIN %s MUM ON LD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, LEDGER_DETAILS_COLUMN.LAST_MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE LD.%s = :%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLedgerDetailExtractor());
	}

	private ResultSetExtractor<LedgerModel> getLedgerDetailExtractor() {

		return new ResultSetExtractor<LedgerModel>() {
			@Override
			public LedgerModel extractData(ResultSet rs) throws SQLException {
				LedgerModel ledgerDetails = new LedgerModel();
				while (rs.next()) {
					ledgerDetails = new LedgerModel(rs);
				}
				return ledgerDetails;
			}
		};
	}

	LedgerModel saveLedgerData(LedgerModel ledgerModel) {
		StringBuilder sqlQueryCompanySave = new StringBuilder();
		sqlQueryCompanySave.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
							.append(String.format("( %s, %s, %s, %s, %s, ", LEDGER_DETAILS_COLUMN.SAC_CODE, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.MOBILE_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PINCODE))
							.append(String.format(" %s, %s, %s, %s, %s, ", LEDGER_DETAILS_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME, LEDGER_DETAILS_COLUMN.LEDGER_GROUP, LEDGER_DETAILS_COLUMN.GST_TYPE, LEDGER_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" %s, %s, %s, %s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.OPENING_BALANCE, LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.CITY_NAME))
							.append(String.format(" %s, %s, %s, %s, ", LEDGER_DETAILS_COLUMN.CREDIT_PERIOD, LEDGER_DETAILS_COLUMN.CREDIT_LIMIT, LEDGER_DETAILS_COLUMN.LEDGER_CODE, LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID))
							.append(String.format(" %s, %s, %s,%s, ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID, LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID,LEDGER_DETAILS_COLUMN.PeriodName,LEDGER_DETAILS_COLUMN.CREDIT_LIMIT))
							.append(String.format(" %s, %s, %s, %s, %s )", LEDGER_DETAILS_COLUMN.ADDED_BY, LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.PAN_NUMBER))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, :%s, :%s, ",LEDGER_DETAILS_COLUMN.SAC_CODE, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.MOBILE_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PINCODE))
							.append(String.format(" :%s, :%s, :%s, :%s, :%s, ", LEDGER_DETAILS_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME, LEDGER_DETAILS_COLUMN.LEDGER_GROUP, LEDGER_DETAILS_COLUMN.GST_TYPE, LEDGER_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" :%s, :%s, :%s, :%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.OPENING_BALANCE, LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.CITY_NAME))
							.append(String.format(" :%s, :%s, :%s, :%s, ", LEDGER_DETAILS_COLUMN.CREDIT_PERIOD, LEDGER_DETAILS_COLUMN.CREDIT_LIMIT, LEDGER_DETAILS_COLUMN.LEDGER_CODE, LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID))
							.append(String.format(" :%s, :%s, :%s,:%s,  ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID, LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID,LEDGER_DETAILS_COLUMN.PeriodName))
							.append(String.format(" :%s, :%s, :%s, :%s, :%s )", LEDGER_DETAILS_COLUMN.ADDED_BY, LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.PAN_NUMBER));

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString(), ledgerModel.getLedgerName())
				.addValue(LEDGER_DETAILS_COLUMN.SAC_CODE.toString(), ledgerModel.getSacCode())
				.addValue(LEDGER_DETAILS_COLUMN.MOBILE_NUMBER.toString(), ledgerModel.getMobileNumber())
				.addValue(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), ledgerModel.getCompanyId())
				.addValue(LEDGER_DETAILS_COLUMN.ADDRESS.toString(), ledgerModel.getAddress())
				.addValue(LEDGER_DETAILS_COLUMN.PINCODE.toString(), ledgerModel.getPinCode())
				.addValue(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString(), ledgerModel.getPanNumber())
				.addValue(LEDGER_DETAILS_COLUMN.STATE_ID.toString(), ledgerModel.getState() != null ? ledgerModel.getState().getStateId() : 0)
				.addValue(LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME.toString(), ledgerModel.getContactPersonName())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_GROUP.toString(), ledgerModel.getLedgerGroup())
				.addValue(LEDGER_DETAILS_COLUMN.CITY_NAME.toString(), ledgerModel.getCityName())
				.addValue(LEDGER_DETAILS_COLUMN.GST_TYPE.toString(), ledgerModel.getGstType())
				.addValue(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString(), ledgerModel.getGstNumber())
				.addValue(LEDGER_DETAILS_COLUMN.OPENING_BALANCE.toString(), ledgerModel.getOpeningBalance())
				.addValue(LEDGER_DETAILS_COLUMN.CREDIT_PERIOD.toString(), ledgerModel.getCreditPeriod())
				.addValue(LEDGER_DETAILS_COLUMN.CREDIT_LIMIT.toString(), ledgerModel.getCreditLimit())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_CODE.toString(), ledgerModel.getLedgerCode())
				.addValue(LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID.toString(), ledgerModel.getCustomerTypeId())
				.addValue(LEDGER_DETAILS_COLUMN.PRICE_ID.toString(), ledgerModel.getPriceId())
				.addValue(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString(), ledgerModel.getSchemeOneId())
				.addValue(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString(), ledgerModel.getSchemeTwoId())
				.addValue(LEDGER_DETAILS_COLUMN.PeriodName.toString(),ledgerModel.getPeriodName())
				.addValue(LEDGER_DETAILS_COLUMN.TAX_CODE.toString(), ledgerModel.getTaxCode())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_CHARGE.toString(), ledgerModel.getLedgerCharge())
				.addValue(LEDGER_DETAILS_COLUMN.ADDED_BY.toString(), ledgerModel.getAddedBy().getUserId())
				.addValue(LEDGER_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), ledgerModel.getExcellDocumentId())
				.addValue(LEDGER_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		ledgerModel.setLedgerId(holder.getKey().intValue());
		return ledgerModel;
	}

	LedgerModel updateLedgerData(LedgerModel ledgerModel) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.MOBILE_NUMBER, LEDGER_DETAILS_COLUMN.MOBILE_NUMBER))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.ADDRESS))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.PINCODE, LEDGER_DETAILS_COLUMN.PINCODE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.SAC_CODE, LEDGER_DETAILS_COLUMN.SAC_CODE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME, LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.LEDGER_GROUP, LEDGER_DETAILS_COLUMN.LEDGER_GROUP))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.GST_TYPE, LEDGER_DETAILS_COLUMN.GST_TYPE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.GST_NUMBER))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.OPENING_BALANCE, LEDGER_DETAILS_COLUMN.OPENING_BALANCE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.LEDGER_CHARGE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.TAX_CODE))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.CITY_NAME, LEDGER_DETAILS_COLUMN.CITY_NAME))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.PAN_NUMBER, LEDGER_DETAILS_COLUMN.PAN_NUMBER))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.PRICE_ID))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.CREDIT_PERIOD,LEDGER_DETAILS_COLUMN.CREDIT_PERIOD))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.CREDIT_LIMIT,LEDGER_DETAILS_COLUMN.CREDIT_LIMIT))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.LEDGER_CODE,LEDGER_DETAILS_COLUMN.LEDGER_CODE))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID,LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID,LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.OPENING_BALANCE,LEDGER_DETAILS_COLUMN.OPENING_BALANCE))
							.append(String.format(" %s = :%s, ",LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID,LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID))
							.append(String.format(" %s = :%s ",LEDGER_DETAILS_COLUMN.PeriodName,LEDGER_DETAILS_COLUMN.PeriodName))
							.append(String.format(" WHERE %s = :%s  ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString(), ledgerModel.getLedgerName())
				.addValue(LEDGER_DETAILS_COLUMN.SAC_CODE.toString(), ledgerModel.getSacCode())
				.addValue(LEDGER_DETAILS_COLUMN.MOBILE_NUMBER.toString(), ledgerModel.getMobileNumber())
				.addValue(LEDGER_DETAILS_COLUMN.ADDRESS.toString(), ledgerModel.getAddress())
				.addValue(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString(), ledgerModel.getPanNumber())
				.addValue(LEDGER_DETAILS_COLUMN.PINCODE.toString(), ledgerModel.getPinCode())
				.addValue(LEDGER_DETAILS_COLUMN.STATE_ID.toString(), ledgerModel.getState() != null && ledgerModel.getState().getStateId() > 0 ? ledgerModel.getState().getStateId() : 0)
				.addValue(LEDGER_DETAILS_COLUMN.CONTANCT_PERSON_NAME.toString(), ledgerModel.getContactPersonName())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_GROUP.toString(), ledgerModel.getLedgerGroup())
				.addValue(LEDGER_DETAILS_COLUMN.GST_TYPE.toString(), ledgerModel.getGstType())
				.addValue(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString(), ledgerModel.getGstNumber())
				.addValue(LEDGER_DETAILS_COLUMN.OPENING_BALANCE.toString(), ledgerModel.getOpeningBalance())
				.addValue(LEDGER_DETAILS_COLUMN.CITY_NAME.toString(), ledgerModel.getCityName())
//				.addValue(LEDGER_DETAILS_COLUMN.LAST_MODIFIED_BY.toString(), ledgerModel.getModifiedBy().getUserId())
//				.addValue(LEDGER_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerModel.getLedgerId())
				.addValue(LEDGER_DETAILS_COLUMN.PRICE_ID.toString(), ledgerModel.getPriceId())
				.addValue(LEDGER_DETAILS_COLUMN.CREDIT_PERIOD.toString(), ledgerModel.getCreditPeriod())
				.addValue(LEDGER_DETAILS_COLUMN.CREDIT_LIMIT.toString(), ledgerModel.getCreditLimit())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_CODE.toString(), ledgerModel.getLedgerCode())
				.addValue(LEDGER_DETAILS_COLUMN.CUSTOMER_TYPE_ID.toString(), ledgerModel.getCustomerTypeId())
				.addValue(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString(), ledgerModel.getSchemeOneId())
				.addValue(LEDGER_DETAILS_COLUMN.OPENING_BALANCE.toString(), ledgerModel.getOpeningBalance())
				.addValue(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString(), ledgerModel.getSchemeTwoId())
				.addValue(LEDGER_DETAILS_COLUMN.PeriodName.toString(), ledgerModel.getPeriodName())
				.addValue(LEDGER_DETAILS_COLUMN.TAX_CODE.toString(), ledgerModel.getTaxCode())
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_CHARGE.toString(), ledgerModel.getLedgerCharge())
				.addValue(LEDGER_DETAILS_COLUMN.IS_DELETED.toString(), 0)
				.addValue(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), ledgerModel.getCompanyId());

		ledgerModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return ledgerModel;
	}

	int deleteLedgerData(int companyId, int ledgerId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", LEDGER_DETAILS_COLUMN.DELETED_BY, LEDGER_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", LEDGER_DETAILS_COLUMN.DELETED_DATE_TIME, LEDGER_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId)
				.addValue(LEDGER_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(LEDGER_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(LEDGER_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);


		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int getLedgerIdByName(int companyId, String ledgerName ) {
		return getLedgerIdByParameter(companyId, LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString(), ledgerName);
	}

	int getLedgerIdByGSTNumber(int companyId, String gstNumber) {
		return getLedgerIdByParameter(companyId, LEDGER_DETAILS_COLUMN.GST_NUMBER.toString(), gstNumber);
	}

	private int getLedgerIdByParameter(int companyId, String parameterName, String parameterValue) {
		int ledgerId = 0;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		param.put(parameterName, parameterValue);
		param.put(LEDGER_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		try {
			StringBuilder query = new StringBuilder("SELECT ").append(LEDGER_DETAILS_COLUMN.LEDGER_ID).append( " from ")
					.append(COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS)
					.append(String.format(" WHERE %s = :%s", parameterName, parameterName))
					.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
					.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.IS_DELETED));
			ledgerId = namedParameterJdbcTemplate.queryForObject(query.toString(),param , Integer.class);
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return ledgerId;
	}
	
	
	

	LedgerModel getBasicLedgerDetailByLedgerId(int ledgerId, int companyId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE))
						.append(String.format(" LD.%s, LD.%s, LD.%s ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PAN_NUMBER))
						.append(String.format(" FROM %s LD ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
						.append(String.format(" WHERE LD.%s = 0 AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getBasicLedgerDetailByLedgerIdExtractor());
	}

	private ResultSetExtractor<LedgerModel> getBasicLedgerDetailByLedgerIdExtractor() {
		return new ResultSetExtractor<LedgerModel>() {
			@Override
			public LedgerModel extractData(ResultSet rs) throws SQLException {
				LedgerModel ledgerModel = new LedgerModel();
				while (rs.next()) {
					ledgerModel = new LedgerModel();
					ledgerModel.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
					ledgerModel.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
					ledgerModel.setPanNumber(rs.getString(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString()));
					ledgerModel.setGstNumber(rs.getString(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString()));
				}
				return ledgerModel;
			}
		};
	}

	
	
	
	int isLedgerWithSameGSTExist(int companyId, String gstNumber, int ledgerId) {
		StringBuilder query = new StringBuilder(String.format("SELECT COUNT(%s) AS %s FROM ", LEDGER_DETAILS_COLUMN.LEDGER_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
				.append(COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS)
				.append(String.format(" WHERE %s = :%s", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s ", LEDGER_DETAILS_COLUMN.IS_DELETED, LEDGER_DETAILS_COLUMN.IS_DELETED));
		
		Map<String, Object> parameters = new HashMap<>(); 
		if (ledgerId > 0) {
			query.append(String.format(" AND %s != :%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID));
			parameters.put(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
		}

		parameters.put(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString(), gstNumber);
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(LEDGER_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		return namedParameterJdbcTemplate.query(query.toString(), parameters, getUserCount());
	}
	
	private ResultSetExtractor<Integer> getUserCount() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				int totalCount = 0;
				while(rs.next()) {
					totalCount = rs.getInt(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString());
				}
				return totalCount;
			}
		};
	}

	
	List<LedgerModel> getLedgerDetailsList(int companyId) {

		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.TAX_CODE, LEDGER_DETAILS_COLUMN.LEDGER_CHARGE))
						.append(String.format(" LD.%s, LD.%s, LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PAN_NUMBER, LEDGER_DETAILS_COLUMN.CITY_NAME))
						.append(String.format(" LD.%s, LD.%s, LD.%s,  ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID, LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID ))
						.append(String.format(" LD.%s, LD.%s, SM.%s, SM.%s ", LEDGER_DETAILS_COLUMN.GST_TYPE, LEDGER_DETAILS_COLUMN.SAC_CODE, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
						.append(String.format(" FROM %s LD ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS))
						.append(String.format(" LEFT JOIN %s SM ON LD.%s = SM.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" WHERE LD.%s = 0  ", LEDGER_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND LD.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" ORDER BY LD.%s ASC ", LEDGER_DETAILS_COLUMN.LEDGER_NAME));

		Map<String, Object> parameters = new HashMap<>();
	

	
		parameters.put(LEDGER_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLedgerNameDetailsListExtractor());
	}

	private ResultSetExtractor<List<LedgerModel>> getLedgerNameDetailsListExtractor() {
		return new ResultSetExtractor<List<LedgerModel>>() {
			@Override
			public List<LedgerModel> extractData(ResultSet rs) throws SQLException {
				List<LedgerModel> ledgerList = new ArrayList<>();
				LedgerModel ledgerModel;
				while (rs.next()) {
					ledgerModel = new LedgerModel();
					ledgerModel.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
					ledgerModel.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
					ledgerModel.setPanNumber(rs.getString(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString()));
					ledgerModel.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));
					ledgerModel.setTaxCode(rs.getDouble(LEDGER_DETAILS_COLUMN.TAX_CODE.toString()));
					ledgerModel.setLedgerCharge(rs.getDouble(LEDGER_DETAILS_COLUMN.LEDGER_CHARGE.toString()));
					ledgerModel.setGstType(rs.getString(LEDGER_DETAILS_COLUMN.GST_TYPE.toString()));
					ledgerModel.setAddress(rs.getString(LEDGER_DETAILS_COLUMN.ADDRESS.toString()));
					ledgerModel.setGstNumber(rs.getString(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString()));
					ledgerModel.setCityName(rs.getString(LEDGER_DETAILS_COLUMN.CITY_NAME.toString()));
					ledgerModel.setPriceId(rs.getInt(LEDGER_DETAILS_COLUMN.PRICE_ID.toString()));
					ledgerModel.setSchemeOneId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString()));
					ledgerModel.setSchemeTwoId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString()));
					StateModel stateModel = new StateModel(rs);
					ledgerModel.setState(stateModel);
					ledgerList.add(ledgerModel);
				}
				return ledgerList;
			}
		};
	}
	
	
}
