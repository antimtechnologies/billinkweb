package acc.webservice.components.ledger;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.LEDGER_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class LedgerDocumentModel {

	private int documentUrlId;
	private boolean deleted;
	private String documentType;
	private String description;
	private String documentUrl;
	private LedgerModel ledgerModel;

	public LedgerDocumentModel() {
	}

	public LedgerDocumentModel(ResultSet rs) throws SQLException {

		setDocumentUrlId(rs.getInt(LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL_ID.toString()));
		setDocumentType(rs.getString(LEDGER_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString()));
		setDescription(rs.getString(LEDGER_DOCUMENT_COLUMN.DESCRIPTION.toString()));
		String documentURL = rs.getString(LEDGER_DOCUMENT_COLUMN.DOCUMENT_URL.toString());
		if (!ApplicationUtility.isNullEmpty(documentURL)) {
			setDocumentUrl(ApplicationUtility.getServerURL() + documentURL);
		}

	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LedgerModel getLedgerModel() {
		return ledgerModel;
	}

	public void setLedgerModel(LedgerModel ledgerModel) {
		this.ledgerModel = ledgerModel;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDocumentUrlId() {
		return documentUrlId;
	}

	public void setDocumentUrlId(int documentUrlId) {
		this.documentUrlId = documentUrlId;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
