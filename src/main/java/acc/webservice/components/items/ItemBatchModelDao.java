package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DOCUMENT_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;

@Repository
@Lazy
public class ItemBatchModelDao {

	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@SuppressWarnings("unchecked")
	int[] saveBatchData(List<ItemBatchModel> itemBatchModelList, int itemId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s, %s) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s , :%s , :%s ) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE));

		List<Map<String, Object>> itemBatchMapList = new ArrayList<>();
		Map<String, Object> itemBatchMap;
		for (ItemBatchModel itemBatchModel : itemBatchModelList) {
			itemBatchMap = new HashMap<>();
			itemBatchMap.put(ITEM_BATCH_COLUMN.BATCH_NO.toString(), itemBatchModel.getBatchNo());
			itemBatchMap.put(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString(), itemBatchModel.getMfgDate());
			itemBatchMap.put(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString(), itemBatchModel.getExpDate());
			itemBatchMap.put(ITEM_BATCH_COLUMN.SHELF_LIFE.toString(), itemBatchModel.getShelfLife());
			itemBatchMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
			itemBatchMapList.add(itemBatchMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), itemBatchMapList.toArray(new HashMap[0]));
	}
	
	
	ItemBatchModel saveItemBatch(ItemBatchModel itemBatchModel) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s, %s) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s , :%s , :%s ) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE));
			KeyHolder holder = new GeneratedKeyHolder();
			
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue(ITEM_BATCH_COLUMN.BATCH_NO.toString(), itemBatchModel.getBatchNo())
					.addValue(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString(), itemBatchModel.getMfgDate())
					.addValue(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString(), itemBatchModel.getExpDate())
					.addValue(ITEM_BATCH_COLUMN.SHELF_LIFE.toString(), itemBatchModel.getShelfLife())
					.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemBatchModel.getItemModel().getItemId());
					//.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemBatchModel.getItemModel().getItemId());
					

			namedParameterJdbcTemplate.update(strQuery.toString(), parameters, holder);
			itemBatchModel.setBatchId(holder.getKey().intValue());
			return itemBatchModel;
	}
	
		

	
	
//	public List<Foo> getByName(List<Integer> codes, String namePart) {
//	    String sql = "SELECT * FROM acc_item_batch_details " + 
//	    		" WHERE batchNo LIKE :batch1 " + 
//	    		" AND itemId = :itemId "; 
//	    		
//	    Map<String,Object> params = new HashMap<String,Object>();
//	    params.put("codes", codes);
//	    params.put("name", namePart+"%");
//	    return getSimpleJdbcTemplate().query(sql, new FooRowMapper(), params);
//	}
	
	
	public List<ItemBatchModel> findByBatchNoLikeByItemId(String batchNo,int itemId) {
		List<ItemBatchModel> batch = namedParameterJdbcTemplate.query("SELECT * FROM acc_item_batch_details WHERE batchNo LIKE :batchNo AND itemId = :itemId",
                new MapSqlParameterSource("batchNo", batchNo)
                , (resultSet, i) -> {
                    return toBatch(resultSet);
                });
		return null;
    }
	
	
	public ItemBatchModel checkuniqueandInsertBatch(ItemBatchModel itemBatchModel,int itemId) {
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("itemId", itemId);
		paramMap.put("batchNo", itemBatchModel.getBatchNo());
		List<ItemBatchModel> batch = namedParameterJdbcTemplate.query("SELECT * FROM acc_item_batch_details WHERE batchNo LIKE :batchNo AND itemId = :itemId",
				paramMap
                , (resultSet, i) -> {
                    return toBatch(resultSet);
                });
		if(batch.size() > 0) {
			return batch.get(0);
		}else {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
					.append(String.format(" ( %s, %s, %s, %s, %s) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE))
					.append(" VALUES ")
					.append(String.format(" ( :%s, :%s, :%s , :%s , :%s ) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE));
				KeyHolder holder = new GeneratedKeyHolder();
				
				SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(ITEM_BATCH_COLUMN.BATCH_NO.toString(), itemBatchModel.getBatchNo())
						.addValue(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString(), itemBatchModel.getMfgDate())
						.addValue(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString(), itemBatchModel.getExpDate())
						.addValue(ITEM_BATCH_COLUMN.SHELF_LIFE.toString(), itemBatchModel.getShelfLife())
						.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemBatchModel.getItemModel().getItemId());
						//.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemBatchModel.getItemModel().getItemId());
						

				namedParameterJdbcTemplate.update(strQuery.toString(), parameters, holder);
				itemBatchModel.setBatchId(holder.getKey().intValue());
				return itemBatchModel;
		}
	}

	 private ItemBatchModel toBatch(ResultSet resultSet) throws SQLException {
		 ItemBatchModel model = new ItemBatchModel();
		    model.setBatchId(resultSet.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
		    model.setBatchNo(resultSet.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
		    model.setMfgDate(resultSet.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
		    model.setExpDate(resultSet.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
	        return model;
	    }
	 
	 
		List<ItemBatchModel> getItemBatchList(int itemId) {
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append(String.format(" SELECT %s, %s, %s, %s ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE, ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.SHELF_LIFE))
				.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
				.append(String.format(" WHERE %s = :%s AND isDeleted = 0  ", ITEM_BATCH_COLUMN.ITEM_ID, ITEM_BATCH_COLUMN.ITEM_ID));

			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);

			return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemBatchResultExctractor());
		}

		private ResultSetExtractor<List<ItemBatchModel>> getItemBatchResultExctractor() {
			return new ResultSetExtractor<List<ItemBatchModel>>() {
				@Override
				public List<ItemBatchModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ItemBatchModel> itemBatchModelList = new ArrayList<>();
					ItemBatchModel itemBatchModel;
					while (rs.next()) {
						itemBatchModel = new ItemBatchModel();
						itemBatchModel.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
						itemBatchModel.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
						itemBatchModel.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
						itemBatchModel.setShelfLife(rs.getString(ITEM_BATCH_COLUMN.SHELF_LIFE.toString()));
						itemBatchModelList.add(itemBatchModel);
					}
					return itemBatchModelList;
				}
			};
		}
	 
		int deleteItemBatchData(int itemId) {
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append(String.format(" UPDATE %s SET isDeleted = ? ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS ))
					.append(String.format(" WHERE %s = ? ", ITEM_BATCH_COLUMN.ITEM_ID, ITEM_BATCH_COLUMN.ITEM_ID));

			return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, itemId });
		}
		
		// 15-05-2019 Start
	ItemBatchModel getOneBatch(int batchId) {
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append(String.format(" SELECT %s, %s, %s ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE, ITEM_BATCH_COLUMN.ITEM_EXP_DATE))
				.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
				.append(String.format(" WHERE %s = :%s  ", ITEM_BATCH_COLUMN.BATCH_ID, ITEM_BATCH_COLUMN.BATCH_ID));

			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put(ITEM_BATCH_COLUMN.BATCH_ID.toString(), batchId);

			return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getOneItemBatchResultExctractor());
		}

		private ResultSetExtractor<ItemBatchModel> getOneItemBatchResultExctractor() {
			return new ResultSetExtractor<ItemBatchModel>() {
				@Override
				public ItemBatchModel extractData(ResultSet rs) throws SQLException, DataAccessException {
					//List<ItemBatchModel> itemBatchModelList = new ArrayList<>();
					ItemBatchModel itemBatchModel = null;
					while (rs.next()) {
				 itemBatchModel = new ItemBatchModel();
				 		itemBatchModel.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
						itemBatchModel.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
						itemBatchModel.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
						//itemBatchModelList.add(itemBatchModel);
					}
					return itemBatchModel;
				}
			};
		}
	 
		
		ItemBatchModel saveBatch(String batchNo,String mfgDate,String expDate,String shelfLife,int itemId) {
			StringBuilder strQuery = new StringBuilder();
			ItemBatchModel itemBatchModel = new ItemBatchModel();
//				strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
//							.append(String.format(" ( %s, %s, %s, %s, %s) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE))
//							.append(" VALUES ")
//							.append(String.format(" ( :%s, :%s, :%s , 1, :%s ) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE));


			strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
					.append(String.format(" ( %s, %s, %s, %s, %s) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE))
					.append(" VALUES ")
					.append(String.format(" ( :%s, :%s, :%s , :%s , :%s ) ", ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE,ITEM_BATCH_COLUMN.ITEM_ID,ITEM_BATCH_COLUMN.SHELF_LIFE));
				KeyHolder holder = new GeneratedKeyHolder();
				
				SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(ITEM_BATCH_COLUMN.BATCH_NO.toString(), batchNo)
						.addValue(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString(), mfgDate)
						.addValue(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString(), expDate)
						.addValue(ITEM_BATCH_COLUMN.SHELF_LIFE.toString(), shelfLife)
						.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemId);
						//.addValue(ITEM_BATCH_COLUMN.ITEM_ID.toString(),itemBatchModel.getItemModel().getItemId());
						

				namedParameterJdbcTemplate.update(strQuery.toString(), parameters, holder);
				itemBatchModel.setBatchId(holder.getKey().intValue());
				itemBatchModel.setBatchNo(batchNo);
				itemBatchModel.setMfgDate(mfgDate);
				itemBatchModel.setExpDate(expDate);
				return itemBatchModel;
		}
		
		
		// 15-05-2019 End

	 
	 
}
