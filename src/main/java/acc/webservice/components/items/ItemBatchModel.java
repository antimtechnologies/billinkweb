package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.global.utils.DateUtility;

public class ItemBatchModel {

	private int batchId;
	private String batchNo;
	private String mfgDate,expDate,shelfLife;
	private boolean deleted;
	private ItemModel itemModel;
	
	public ItemBatchModel() {
		
	}
	public ItemBatchModel(ResultSet rs) throws SQLException {
		setBatchId(rs.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
		setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
		setShelfLife(rs.getString(ITEM_BATCH_COLUMN.SHELF_LIFE.toString()));	
		Timestamp fetchedMfgDateTime = rs.getTimestamp(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString());
		setMfgDate(DateUtility.converDateToUserString(fetchedMfgDateTime));
		Timestamp fetchedExpDateTime = rs.getTimestamp(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString());
		setExpDate(DateUtility.converDateToUserString(fetchedExpDateTime));
	
	}
	
	
	public int getBatchId() {
		return batchId;
	}
	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public ItemModel getItemModel() {
		return itemModel;
	}
	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getShelfLife() {
		return shelfLife;
	}
	public void setShelfLife(String shelfLife) {
		this.shelfLife = shelfLife;
	}
	
}
