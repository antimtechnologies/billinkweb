package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.ITEM_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class ItemDocumentModel {

	private int documentUrlId;
	private boolean deleted;
	private String documentType;
	private String description;
	private String documentUrl;
	private ItemModel itemModel;

	public ItemDocumentModel() {
	}

	public ItemDocumentModel(ResultSet rs) throws SQLException {

		setDocumentUrlId(rs.getInt(ITEM_DOCUMENT_COLUMN.DOCUMENT_URL_ID.toString()));
		setDocumentType(rs.getString(ITEM_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString()));
		setDescription(rs.getString(ITEM_DOCUMENT_COLUMN.DESCRIPTION.toString()));
		String documentURL = rs.getString(ITEM_DOCUMENT_COLUMN.DOCUMENT_URL.toString());
		if (!ApplicationUtility.isNullEmpty(documentURL)) {
			setDocumentUrl(ApplicationUtility.getServerURL() + documentURL);
		}

	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ItemModel getItemModel() {
		return itemModel;
	}

	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}


	public int getDocumentUrlId() {
		return documentUrlId;
	}

	public void setDocumentUrlId(int documentUrlId) {
		this.documentUrlId = documentUrlId;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
