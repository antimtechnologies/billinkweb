package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.ACC_CATEGORY_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_ALIAS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;

@Repository
@Lazy
public class ItemAliasDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@SuppressWarnings("unchecked")
	int[] saveitemAlias(List<ItemAliasModel> itemAliasList, int itemId ,int companyId) {
		
		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_COMPANY_ITEM_ALIAS_DETAIL))
				.append(String.format(" ( %s,%s,%s) ", ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME,ITEM_ALIAS_COLUMN.ITEM_ID,ITEM_ALIAS_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME,ITEM_ALIAS_COLUMN.ITEM_ID,ITEM_ALIAS_COLUMN.COMPANY_ID));

		List<Map<String, Object>> itemAliasMapList = new ArrayList<>();
		Map<String, Object> itemAliasMap;
		for (ItemAliasModel itemAliasModel : itemAliasList) {
			itemAliasMap = new HashMap<>();
			itemAliasMap.put(ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString(), itemAliasModel.getItemAliasName());
			itemAliasMap.put(ITEM_ALIAS_COLUMN.ITEM_ID.toString(), itemId);
			itemAliasMap.put(ITEM_ALIAS_COLUMN.COMPANY_ID.toString(), companyId);

			itemAliasMapList.add(itemAliasMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), itemAliasMapList.toArray(new HashMap[0]));
	}

	
	public List<ItemAliasModel> getItemAliasNameByCompanyId(int companyId) {
	
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ID.%s ", ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME))
						.append(String.format(" FROM %s ID ", DATABASE_TABLE.ACC_COMPANY_ITEM_ALIAS_DETAIL))
						.append(String.format(" WHERE ID.%s = :%s ", ITEM_ALIAS_COLUMN.COMPANY_ID, ITEM_ALIAS_COLUMN.COMPANY_ID));
					
		Map<String, Object> parameters = new HashMap<>();
	
		parameters.put(ITEM_ALIAS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemAliasDetailsExtractor());
	}
	
	

	private ResultSetExtractor<List<ItemAliasModel>> getItemAliasDetailsExtractor() {

		return new ResultSetExtractor<List<ItemAliasModel>>() {
			@Override
			public List<ItemAliasModel> extractData(ResultSet rs) throws SQLException {
				List<ItemAliasModel> itemAliasList = new ArrayList<>();
				ItemAliasModel itemAliasModel;
				while (rs.next()) {
					itemAliasModel = new ItemAliasModel();
					itemAliasModel.setItemAliasName(rs.getString(ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString()));
					itemAliasList.add(itemAliasModel);
				}
				return itemAliasList;
			}
		};
	}
	
	
	public List<ItemAliasModel> getItemAliasNameByItemId(int itemID) {
		
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ID.%s ", ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME))
						.append(String.format(" FROM %s ID ", DATABASE_TABLE.ACC_COMPANY_ITEM_ALIAS_DETAIL))
						.append(String.format(" WHERE ID.%s = :%s AND isDeleted = 0 ", ITEM_ALIAS_COLUMN.ITEM_ID, ITEM_ALIAS_COLUMN.ITEM_ID));
					
		Map<String, Object> parameters = new HashMap<>();
	
		parameters.put(ITEM_ALIAS_COLUMN.ITEM_ID.toString(), itemID);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemAliasByItemIdDetailsExtractor());
	}
	
	

	private ResultSetExtractor<List<ItemAliasModel>> getItemAliasByItemIdDetailsExtractor() {

		return new ResultSetExtractor<List<ItemAliasModel>>() {
			@Override
			public List<ItemAliasModel> extractData(ResultSet rs) throws SQLException {
				List<ItemAliasModel> itemAliasList = new ArrayList<>();
				ItemAliasModel itemAliasModel;
				while (rs.next()) {
					itemAliasModel = new ItemAliasModel();
					itemAliasModel.setItemAliasName(rs.getString(ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString()));
					itemAliasList.add(itemAliasModel);
				}
				return itemAliasList;
			}
		};
	}
	
	int deleteItemAliasData(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET isDeleted = ? ", DATABASE_TABLE.ACC_COMPANY_ITEM_ALIAS_DETAIL))
				.append(String.format(" WHERE %s = ? ", ITEM_ALIAS_COLUMN.ITEM_ID, ITEM_ALIAS_COLUMN.ITEM_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, itemId });
	}
	
	//23-05-2019 Start
	public List<ItemAliasModel> getItemAliasNameListByCompanyId(int companyId,String itemAliasName) {
		
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ID.%s ", ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME))
						.append(String.format(" FROM %s ID ", DATABASE_TABLE.ACC_COMPANY_ITEM_ALIAS_DETAIL))
						.append(String.format(" WHERE ID.%s = :%s  AND ID.%s = :%s", ITEM_ALIAS_COLUMN.COMPANY_ID, ITEM_ALIAS_COLUMN.COMPANY_ID,ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME,ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME));
					
		Map<String, Object> parameters = new HashMap<>();
	
		parameters.put(ITEM_ALIAS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString(), itemAliasName);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemAliasListExtractor());
	}
	
	

	private ResultSetExtractor<List<ItemAliasModel>> getItemAliasListExtractor() {

		return new ResultSetExtractor<List<ItemAliasModel>>() {
			@Override
			public List<ItemAliasModel> extractData(ResultSet rs) throws SQLException {
				List<ItemAliasModel> itemAliasList = new ArrayList<>();
				ItemAliasModel itemAliasModel;
				while (rs.next()) {
					itemAliasModel = new ItemAliasModel();
					itemAliasModel.setItemAliasName(rs.getString(ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString()));
					itemAliasList.add(itemAliasModel);
				}
				return itemAliasList;
			}
		};
	}
	//23-05-2019 End
	

}
