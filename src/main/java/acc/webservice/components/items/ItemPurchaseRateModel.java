package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.enums.DatabaseEnum.ITEM_PURCHASE_RATE_COLUMN;
import acc.webservice.global.utils.DateUtility;

public class ItemPurchaseRateModel {

	private int itemPurchaseRateId;
	private double itemPurchaseRate;
	private String wefPurchaseRateDate,wetPurachaseRateDate;
	private boolean deleted;
	private ItemModel itemModel;
	
	public ItemPurchaseRateModel() {
		
	}
	public ItemPurchaseRateModel(ResultSet rs) throws SQLException {
		setItemPurchaseRateId(rs.getInt(ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE_ID.toString()));
		setItemPurchaseRate(rs.getDouble(ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE.toString()));
	
		Timestamp fetchedWetDateTime = rs.getTimestamp(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString());
		setWetPurachaseRateDate(DateUtility.converDateToUserString(fetchedWetDateTime));
		
		Timestamp fetchedWefDateTime = rs.getTimestamp(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString());
		setWefPurchaseRateDate(DateUtility.converDateToUserString(fetchedWefDateTime));
	}
	
	public int getItemPurchaseRateId() {
		return itemPurchaseRateId;
	}
	public void setItemPurchaseRateId(int itemPurchaseRateId) {
		this.itemPurchaseRateId = itemPurchaseRateId;
	}
	public double getItemPurchaseRate() {
		return itemPurchaseRate;
	}
	public void setItemPurchaseRate(double itemPurchaseRate) {
		this.itemPurchaseRate = itemPurchaseRate;
	}
	public String getWefPurchaseRateDate() {
		return wefPurchaseRateDate;
	}
	public void setWefPurchaseRateDate(String wefPurchaseRateDate) {
		this.wefPurchaseRateDate = wefPurchaseRateDate;
	}
	public String getWetPurachaseRateDate() {
		return wetPurachaseRateDate;
	}
	public void setWetPurachaseRateDate(String wetPurachaseRateDate) {
		this.wetPurachaseRateDate = wetPurachaseRateDate;
	}
	public ItemModel getItemModel() {
		return itemModel;
	}
	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
	


}
