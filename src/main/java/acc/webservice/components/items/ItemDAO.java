package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.ACC_CATEGORY_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class ItemDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	Integer getItemCount(String itemName, int companyId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT COUNT(ID.%s) AS %s ", ITEM_DETAILS_COLUMN.ITEM_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), "%" + itemName + "%");
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemCount());
	}

	private ResultSetExtractor<Integer> getItemCount() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				int totalCount = 0;
				while(rs.next()) {
					totalCount = rs.getInt(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString());
				}
				return totalCount;
			}
		};
	}


	List<ItemModel> getItemBasicDetailList(int companyId, int start, int noOfRecord, String itemName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
//		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
//						.append(String.format(" ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
//						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
//						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
//						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
//						.append(String.format(" ORDER BY ID.%s ASC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
							.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL,ITEM_DETAILS_COLUMN.PURCHASE_RATE))
							.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR  %s LIKE :%s OR %s LIKE :%s)", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME,ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_EAN_NO))
							.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" ORDER BY ID.%s ASC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), "%" + itemName + "%");
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_SKU.toString(), "%" + itemName + "%");
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString(), "%" + itemName + "%");
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemBasicDetailsListExtractor());
	}

	private ResultSetExtractor<List<ItemModel>> getItemBasicDetailsListExtractor() {

		return new ResultSetExtractor<List<ItemModel>>() {
			@Override
			public List<ItemModel> extractData(ResultSet rs) throws SQLException {
				List<ItemModel> itemList = new ArrayList<>();
				ItemModel itemModel;
				while (rs.next()) {
					itemModel = new ItemModel();
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
					itemModel.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					itemModel.setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
					itemModel.setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
					itemModel.setProfilePhotoURL(ApplicationUtility.getServerURL() + rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));
					itemList.add(itemModel);
				}
				return itemList;
			}
		};
	}

	List<ItemModel> getItemListData(int companyId, String itemName, int start, int numberOfRecord) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.GST_TYPE))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.CESS, ITEM_DETAILS_COLUMN.MEASURE_UNIT, ITEM_DETAILS_COLUMN.QUANTITY, ITEM_DETAILS_COLUMN.PURCHASE_RATE))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ALIAS, ITEM_DETAILS_COLUMN.BOM_NAME, ITEM_DETAILS_COLUMN.BOM_ALIAS))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.ADDED_DATE_TIME, ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL,ITEM_DETAILS_COLUMN.ITEM_EAN_NO))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s,  ", ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS, ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS, ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG, ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s,ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH, ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT,ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER))
						.append(String.format(" ID.%s, ", ITEM_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" BM.%s, BM.%s, CM.%s, CM.%s ", ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME))
						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
						.append(String.format(" LEFT JOIN %s BM ON ID.%s = BM.%s AND BM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER, ITEM_DETAILS_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.IS_DELETED))
						.append(String.format(" AND BM.%s = :%s ", ACC_BRAND_MASTER_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" LEFT JOIN %s CM ON ID.%s = CM.%s AND CM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER, ITEM_DETAILS_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.IS_DELETED))
						.append(String.format(" AND CM.%s = :%s ", ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s AUM ON ID.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, ITEM_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON ID.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, ITEM_DETAILS_COLUMN.LAST_MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))						
						.append(String.format(" ORDER BY ID.%s DESC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), "%" + itemName + "%");
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemDetailsListExtractor());
	}

	private ResultSetExtractor<List<ItemModel>> getItemDetailsListExtractor() {

		return new ResultSetExtractor<List<ItemModel>>() {
			@Override
			public List<ItemModel> extractData(ResultSet rs) throws SQLException {
				List<ItemModel> itemList = new ArrayList<>();
				while (rs.next()) {
					itemList.add(new ItemModel(rs));
				}
				return itemList;
			}
		};
	}

	ItemModel getItemDetailsById(int companyId, int itemId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.GST_TYPE))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.CESS, ITEM_DETAILS_COLUMN.MEASURE_UNIT, ITEM_DETAILS_COLUMN.QUANTITY, ITEM_DETAILS_COLUMN.PURCHASE_RATE))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.ADDED_DATE_TIME, ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
						.append(String.format(" ID.%s, ID.%s, ID.%s,ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ALIAS, ITEM_DETAILS_COLUMN.BOM_NAME, ITEM_DETAILS_COLUMN.BOM_ALIAS,ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT))
						.append(String.format(" ID.%s, ID.%s, ID.%s,ID.%s,ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS, ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS,ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG,ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH))
						.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s, ",ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH,ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" BM.%s, BM.%s, CM.%s, CM.%s ", ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_NAME, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME))
						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
						.append(String.format(" LEFT JOIN %s BM ON ID.%s = BM.%s AND BM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_BRAND_MASTER, ITEM_DETAILS_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.BRAND_ID, ACC_BRAND_MASTER_COLUMN.IS_DELETED))
						.append(String.format(" LEFT JOIN %s CM ON ID.%s = CM.%s AND CM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER, ITEM_DETAILS_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.IS_DELETED))
						.append(String.format(" INNER JOIN %s AUM ON ID.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, ITEM_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON ID.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, ITEM_DETAILS_COLUMN.LAST_MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE ID.%s = :%s ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_ID))
						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_ID.toString(), itemId);
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemDetailsExtractor());
	}

	private ResultSetExtractor<ItemModel> getItemDetailsExtractor() {

		return new ResultSetExtractor<ItemModel>() {
			@Override
			public ItemModel extractData(ResultSet rs) throws SQLException {
				ItemModel itemDetails = new ItemModel();
				while (rs.next()) {
					itemDetails = new ItemModel(rs);
				}
				return itemDetails;
			}
		};
	}

	ItemModel saveItemData(ItemModel itemModel) {
		StringBuilder sqlQueryCompanySave = new StringBuilder();
		sqlQueryCompanySave.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format("( %s, %s, %s, ", ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.BRAND_ID, ITEM_DETAILS_COLUMN.CATEGORY_ID))
							.append(String.format(" %s, %s, %s, %s, ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.CESS, ITEM_DETAILS_COLUMN.MEASURE_UNIT))
							.append(String.format(" %s, %s, %s, %s, ", ITEM_DETAILS_COLUMN.QUANTITY, ITEM_DETAILS_COLUMN.PURCHASE_RATE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.GST_TYPE))
							.append(String.format(" %s, %s, %s, %s, ", ITEM_DETAILS_COLUMN.ADDED_BY, ITEM_DETAILS_COLUMN.ADDED_DATE_TIME, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL, ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s, %s, %s, ", ITEM_DETAILS_COLUMN.ITEM_ALIAS, ITEM_DETAILS_COLUMN.BOM_NAME, ITEM_DETAILS_COLUMN.BOM_ALIAS,ITEM_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" %s, %s, %s, %s, ", ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS,ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG))
							.append(String.format(" %s, %s, %s, ", ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH))
							.append(String.format(" %s, %s, ",  ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS, ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT))
							.append(String.format(" %s, %s,%s, %s,%s) ", ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH, ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT,ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.BRAND_ID, ITEM_DETAILS_COLUMN.CATEGORY_ID))
							.append(String.format(" :%s, :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.CESS, ITEM_DETAILS_COLUMN.MEASURE_UNIT))
							.append(String.format(" :%s, :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.QUANTITY, ITEM_DETAILS_COLUMN.PURCHASE_RATE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.GST_TYPE))
							.append(String.format(" :%s, :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.ADDED_BY, ITEM_DETAILS_COLUMN.ADDED_DATE_TIME, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL, ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.ITEM_ALIAS, ITEM_DETAILS_COLUMN.BOM_NAME, ITEM_DETAILS_COLUMN.BOM_ALIAS, ITEM_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" :%s, :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS,ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG))
							.append(String.format(" :%s, :%s, :%s, ", ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH)) 
							.append(String.format(":%s, :%s, :%s,:%s, :%s,:%s, :%s)",ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS, ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB,ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH, ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT, ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER));
		if (!ApplicationUtility.isNullEmpty(itemModel.getProfilePhotoURL())) {			
			itemModel.setProfilePhotoURL(itemModel.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			itemModel.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.ITEM_PROFILE_PHOTO_DEF_URL.toString());
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), itemModel.getItemName())
				.addValue(ITEM_DETAILS_COLUMN.BRAND_ID.toString(), itemModel.getBrand() == null || itemModel.getBrand().getBrandId() <= 0 ? null : itemModel.getBrand().getBrandId())
				.addValue(ITEM_DETAILS_COLUMN.CATEGORY_ID.toString(), itemModel.getCategory() == null || itemModel.getCategory().getCategoryId() <= 0 ? null : itemModel.getCategory().getCategoryId())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_ALIAS.toString(), itemModel.getItemAlias())
				.addValue(ITEM_DETAILS_COLUMN.BOM_NAME.toString(), itemModel.getBomName())
				.addValue(ITEM_DETAILS_COLUMN.BOM_ALIAS.toString(), itemModel.getBomAlias())
				.addValue(ITEM_DETAILS_COLUMN.HSN_CODE.toString(), itemModel.getHsnCode())
				.addValue(ITEM_DETAILS_COLUMN.TAX_CODE.toString(), itemModel.getTaxCode())
				.addValue(ITEM_DETAILS_COLUMN.CESS.toString(), itemModel.getCess())
				.addValue(ITEM_DETAILS_COLUMN.MEASURE_UNIT.toString(), itemModel.getMeasureUnit())
				.addValue(ITEM_DETAILS_COLUMN.QUANTITY.toString(), itemModel.getQuantity())
				.addValue(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString(), itemModel.getPurchaseRate())
				.addValue(ITEM_DETAILS_COLUMN.GST_TYPE.toString(), itemModel.getGstType())
				.addValue(ITEM_DETAILS_COLUMN.SELLING_RATE.toString(), itemModel.getSellRate())
				.addValue(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString(), itemModel.getProfilePhotoURL())
				.addValue(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), itemModel.getCompanyId())
				.addValue(ITEM_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), itemModel.getExcellDocumentId())
				.addValue(ITEM_DETAILS_COLUMN.ADDED_BY.toString(), itemModel.getAddedBy().getUserId())
				.addValue(ITEM_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString(), itemModel.getEanNo())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_SKU.toString(), itemModel.getSku())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS.toString(), itemModel.getInnerContains())
                .addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG.toString(), itemModel.getInnnerWeightKg())
				
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS.toString(), itemModel.getOuterContains())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT.toString(), itemModel.getOuterWeight())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL.toString(), itemModel.getInnerBoxSizeL())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL.toString(), itemModel.getOuterBoxSizeL())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB.toString(), itemModel.getInnerBoxSizeB())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB.toString(), itemModel.getOuterBoxSizeB())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH.toString(), itemModel.getInnerBoxSizeH())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH.toString(), itemModel.getOuterBoxSizeH())
				.addValue(ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER.toString(), itemModel.getCurrentItemNumber())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT.toString(), itemModel.getProductMeasurement());
				

		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		itemModel.setItemId(holder.getKey().intValue());
		return itemModel;
	}

	ItemModel updateItemData(ItemModel itemModel) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_ALIAS, ITEM_DETAILS_COLUMN.ITEM_ALIAS))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.BOM_NAME, ITEM_DETAILS_COLUMN.BOM_NAME))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.BOM_ALIAS, ITEM_DETAILS_COLUMN.BOM_ALIAS))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.BRAND_ID, ITEM_DETAILS_COLUMN.BRAND_ID))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.CATEGORY_ID, ITEM_DETAILS_COLUMN.CATEGORY_ID))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL, ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL, ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB, ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB, ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH, ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH, ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.HSN_CODE))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.CESS, ITEM_DETAILS_COLUMN.CESS))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.MEASURE_UNIT, ITEM_DETAILS_COLUMN.MEASURE_UNIT))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.QUANTITY, ITEM_DETAILS_COLUMN.QUANTITY))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.PURCHASE_RATE, ITEM_DETAILS_COLUMN.PURCHASE_RATE))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.GST_TYPE, ITEM_DETAILS_COLUMN.GST_TYPE))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.SELLING_RATE))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.LAST_MODIFIED_BY, ITEM_DETAILS_COLUMN.LAST_MODIFIED_BY))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME, ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME))
							.append(String.format("%s = :%s",  ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT,ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT))
							.append(String.format(" WHERE %s = :%s  ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_ID))
							.append(String.format(" AND %s = :%s  ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.IS_DELETED));

		if (!ApplicationUtility.isNullEmpty(itemModel.getProfilePhotoURL())) {			
			itemModel.setProfilePhotoURL(itemModel.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			itemModel.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.ITEM_PROFILE_PHOTO_DEF_URL.toString());
		}

		
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), itemModel.getItemName())
				.addValue(ITEM_DETAILS_COLUMN.BRAND_ID.toString(), itemModel.getBrand() == null || itemModel.getBrand().getBrandId() <= 0 ? null : itemModel.getBrand().getBrandId())
				.addValue(ITEM_DETAILS_COLUMN.CATEGORY_ID.toString(), itemModel.getCategory() == null || itemModel.getCategory().getCategoryId() <= 0 ? null : itemModel.getCategory().getCategoryId())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_ALIAS.toString(), itemModel.getItemAlias())
				.addValue(ITEM_DETAILS_COLUMN.BOM_NAME.toString(), itemModel.getBomName())
				.addValue(ITEM_DETAILS_COLUMN.BOM_ALIAS.toString(), itemModel.getBomAlias())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL.toString(), itemModel.getInnerBoxSizeL())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL.toString(), itemModel.getOuterBoxSizeL())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB.toString(), itemModel.getInnerBoxSizeB())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB.toString(), itemModel.getOuterBoxSizeB())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH.toString(), itemModel.getInnerBoxSizeH())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH.toString(), itemModel.getOuterBoxSizeH())
				
				.addValue(ITEM_DETAILS_COLUMN.HSN_CODE.toString(), itemModel.getHsnCode())
				.addValue(ITEM_DETAILS_COLUMN.TAX_CODE.toString(), itemModel.getTaxCode())
				.addValue(ITEM_DETAILS_COLUMN.CESS.toString(), itemModel.getCess())
				.addValue(ITEM_DETAILS_COLUMN.MEASURE_UNIT.toString(), itemModel.getMeasureUnit())
				.addValue(ITEM_DETAILS_COLUMN.QUANTITY.toString(), itemModel.getQuantity())
				.addValue(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString(), itemModel.getPurchaseRate())
				.addValue(ITEM_DETAILS_COLUMN.GST_TYPE.toString(), itemModel.getGstType())
				.addValue(ITEM_DETAILS_COLUMN.SELLING_RATE.toString(), itemModel.getSellRate())
				.addValue(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString(), itemModel.getProfilePhotoURL())
				.addValue(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), itemModel.getCompanyId())
				.addValue(ITEM_DETAILS_COLUMN.LAST_MODIFIED_BY.toString(), itemModel.getModifiedBy().getUserId())
				.addValue(ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT.toString(), itemModel.getProductMeasurement())
				.addValue(ITEM_DETAILS_COLUMN.ITEM_ID.toString(), itemModel.getItemId())
				.addValue(ITEM_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		itemModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return itemModel;
	}

	int deleteItemData(int companyId, int itemId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", ITEM_DETAILS_COLUMN.DELETED_BY, ITEM_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", ITEM_DETAILS_COLUMN.DELETED_DATE_TIME, ITEM_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_ID))
							.append(String.format(" AND %s = :%s  ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ITEM_DETAILS_COLUMN.ITEM_ID.toString(), itemId)
				.addValue(ITEM_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(ITEM_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(ITEM_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int getItemIdByName(int companyId, String itemName) {
		int itemId = 0;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		param.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), itemName);
		param.put(ITEM_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		try {
			StringBuilder query = new StringBuilder("SELECT ").append(ITEM_DETAILS_COLUMN.ITEM_ID).append(" from ")
					.append(COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS)
					.append(String.format(" WHERE %s = :%s ", ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
					.append(String.format(" AND %s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
					.append(String.format(" AND %s = :%s ", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.IS_DELETED));

			itemId = namedParameterJdbcTemplate.queryForObject(query.toString(), param, Integer.class);

		} catch (Exception e) {
//			e.printStackTrace();
		}
		return itemId;
	}


	int getMaxItemNumber(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT MAX(%s) AS maxItemNumber FROM %s ", ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER, COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
				.append(String.format("WHERE %s = 0 ", ITEM_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s  ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID));
	
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
				

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getMaxItemNumberExctractor());
	}

	private ResultSetExtractor<Integer> getMaxItemNumberExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int maxItemNumber = 0;
				while (rs.next()) {
					maxItemNumber = rs.getInt("maxItemNumber");
				}
				return maxItemNumber;
			}
		};
	}

// create 23-05-2019
	List<ItemModel> getItemList(int companyId, String itemName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
//		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
//						.append(String.format(" ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
//						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
//						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
//						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
//						.append(String.format(" ORDER BY ID.%s ASC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
							.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL,ITEM_DETAILS_COLUMN.PURCHASE_RATE))
							.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format(" WHERE ID.%s = 0 AND ID.%s = :%s  ", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME,ITEM_DETAILS_COLUMN.ITEM_NAME))
							.append(String.format(" AND ID.%s = "+companyId+" ", ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" ORDER BY ID.%s ASC ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		
		Map<String, Object> parameters = new HashMap<>();
		
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), itemName);
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemListExtractor());
	}

	private ResultSetExtractor<List<ItemModel>> getItemListExtractor() {

		return new ResultSetExtractor<List<ItemModel>>() {
			@Override
			public List<ItemModel> extractData(ResultSet rs) throws SQLException {
				List<ItemModel> itemList = new ArrayList<>();
				ItemModel itemModel;
				while (rs.next()) {
					itemModel = new ItemModel();
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
					itemModel.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					itemModel.setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
					itemModel.setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
					itemModel.setProfilePhotoURL(ApplicationUtility.getServerURL() + rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));
					itemList.add(itemModel);
				}
				return itemList;
			}
		};
	}
	
	
	List<ItemModel> getItemListByeanNo(int companyId, String itemName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
//		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
//						.append(String.format(" ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
//						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
//						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
//						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
//						.append(String.format(" ORDER BY ID.%s ASC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
							.append(String.format(" ID.%s, ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL,ITEM_DETAILS_COLUMN.PURCHASE_RATE))
							.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
							.append(String.format(" WHERE ID.%s = 0 AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_EAN_NO))
							.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" ORDER BY ID.%s ASC ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString(), itemName);
		parameters.put(ITEM_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemListByeanNoExtractor());
	}

	private ResultSetExtractor<List<ItemModel>> getItemListByeanNoExtractor() {

		return new ResultSetExtractor<List<ItemModel>>() {
			@Override
			public List<ItemModel> extractData(ResultSet rs) throws SQLException {
				List<ItemModel> itemList = new ArrayList<>();
				ItemModel itemModel;
				while (rs.next()) {
					itemModel = new ItemModel();
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
					itemModel.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					itemModel.setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
					itemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					itemModel.setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
					itemModel.setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
					itemModel.setProfilePhotoURL(ApplicationUtility.getServerURL() + rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));
					itemList.add(itemModel);
				}
				return itemList;
			}
		};
	}
 // 23-05-2019 End	
	
}
	