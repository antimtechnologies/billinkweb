package acc.webservice.components.items;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_ALIAS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/itemData/")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private ItemBatchModelDao itemBatchModelDao;

	@RequestMapping(value="getItemBasicDetailList", method=RequestMethod.POST)
	public Map<String, Object> getItemBasicDetailList(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		String itemName = ApplicationUtility.getStrValue(params, ITEM_DETAILS_COLUMN.ITEM_NAME.toString());
		int start = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getItemBasicDetailList(companyId, start, noOfRecord, itemName));
		return responseData;
	}
	
	
	@RequestMapping(value="getPurchaseRate", method=RequestMethod.POST)
	public Map<String, Object> getPurchaseRate(@RequestBody Map<String, Object> params) {
		int itemid = ApplicationUtility.getIntValue(params, ITEM_DETAILS_COLUMN.ITEM_ID.toString());
		int billdate=ApplicationUtility.getIntValue(params, PURCHASE_DETAILS_COLUMN.BILL_DATE.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getPurchaseRate(itemid,billdate));
		return responseData;
	}
	
	@RequestMapping(value="getMRPRate", method=RequestMethod.POST)
	public Map<String, Object> getMRPRate(@RequestBody Map<String, Object> params) {
		int itemid = ApplicationUtility.getIntValue(params, ITEM_DETAILS_COLUMN.ITEM_ID.toString());
		int billdate=ApplicationUtility.getIntValue(params, PURCHASE_DETAILS_COLUMN.BILL_DATE.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getMRPRate(itemid,billdate));
		return responseData;
	}
	
	

	@RequestMapping(value="getItemListData", method=RequestMethod.POST)
	public Map<String, Object> getItemListData(@RequestBody Map<String, Object> params) {
		int companyId 			= ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start 				= ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord 		= ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String itemName 		= ApplicationUtility.getStrValue(params, ITEM_DETAILS_COLUMN.ITEM_NAME.toString());

		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.getItemListData(companyId, itemName, start, numberOfRecord));
		
		if (start == 0) {
			response.put(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString(), itemService.getItemCount(itemName, companyId));
		}
		return response;
	}

	@RequestMapping(value="getItemDetailsById", method=RequestMethod.POST)
	public Map<String, Object> getItemDetailsById(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int itemId = ApplicationUtility.getIntValue(params, ITEM_DETAILS_COLUMN.ITEM_ID.toString());

		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.getItemDetailsById(companyId, itemId));
		return response;
	}

	@RequestMapping(value="saveItemData", method=RequestMethod.POST)
	public Map<String, Object> saveItemData(@RequestBody ItemModel itemModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.saveItemData(itemModel));
		return response;
	}

	@RequestMapping(value="updateItemData", method=RequestMethod.POST)
	public Map<String, Object> updateItemData(@RequestBody ItemModel itemModel) {
		Map<String, Object> response = new HashMap<>(); 
		response.put("status", "success");
		response.put("data", itemService.updateItemData(itemModel));
		return response;
	}

	@RequestMapping(value="deleteItemData", method=RequestMethod.POST)
	public Map<String, Object> deleteItemData(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int itemId = ApplicationUtility.getIntValue(params, ITEM_DETAILS_COLUMN.ITEM_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());

		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.deleteItemData(companyId, itemId, deletedBy));
		return response;
	}
	
	@RequestMapping(value="savebatchData", method=RequestMethod.POST)
	public Map<String, Object> saveBatchData(@RequestBody ItemBatchModel batchModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.saveItembatchdata(batchModel));
		return response;
	}
	
	@RequestMapping(value="getItemAliasNameById", method=RequestMethod.POST)
	public Map<String, Object> getItemAliasByCompanyId(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, ITEM_ALIAS_COLUMN.COMPANY_ID.toString());
	
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.getitemAliasNameByCompanyId(companyId));
		return response;
	}
	
	@RequestMapping(value="getItemMrpByItemId", method=RequestMethod.POST)
	public Map<String, Object> getItemMrpByItemId(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, ITEM_MRP_COLUMN.ITEM_ID.toString());
		String date = ApplicationUtility.getStrValue(params, ITEM_MRP_COLUMN.MRP_WET_DATE.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.getItemMrpbyItemIdAndDate(itemId,date));
		return response;
	}
	
	
	// 15-09-2019 Start
	@RequestMapping(value="getoneBatch", method=RequestMethod.POST)
	public Map<String, Object> getOneBatchDetail(@RequestBody Map<String, Object> params) {
		int batchId = ApplicationUtility.getIntValue(params, ITEM_BATCH_COLUMN.BATCH_ID.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.getOneBatch(batchId));
		return response;
	}
	
	@RequestMapping(value="saveBatch", method=RequestMethod.POST)
	public Map<String, Object> saveBatch(@RequestBody Map<String, Object> params) {
		//int companyId 			= ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		//int start 				= ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		//int numberOfRecord 		= ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String batchNo 		= ApplicationUtility.getStrValue(params, ITEM_BATCH_COLUMN.BATCH_NO.toString());
		String mfgDate 		= ApplicationUtility.getStrValue(params, ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString());
		String expDate 		= ApplicationUtility.getStrValue(params, ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString());
		String shelftLife 		= ApplicationUtility.getStrValue(params, ITEM_BATCH_COLUMN.SHELF_LIFE.toString());
		int itemID 		= ApplicationUtility.getIntValue(params, ITEM_BATCH_COLUMN.ITEM_ID.toString());
		
		
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemService.saveBatch(batchNo, mfgDate, expDate, shelftLife, itemID));
		return response;
	}
	
	// 185-09-2019 End
	
	// 23-05-2019 Start
	@RequestMapping(value="getItemList", method=RequestMethod.POST)
	public Map<String, Object> getItemList(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		String itemName = ApplicationUtility.getStrValue(params, ITEM_DETAILS_COLUMN.ITEM_NAME.toString());
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getIteList(companyId, itemName));
		return responseData;
	}
	
	@RequestMapping(value="getItemListByEanNo", method=RequestMethod.POST)
	public Map<String, Object> getItemListByEanNo(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		String itemName = ApplicationUtility.getStrValue(params, ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString());
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getIteListByEanNo(companyId, itemName));
		return responseData;
	}
	
	
	@RequestMapping(value="getItemAliasNameListByCompanyId", method=RequestMethod.POST)
	public Map<String, Object> getItemAliasNameListByCompanyId(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, ITEM_ALIAS_COLUMN.COMPANY_ID.toString());
		String itemAliasName = ApplicationUtility.getStrValue(params, ITEM_ALIAS_COLUMN.ITEM_ALIAS_NAME.toString());
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", itemService.getItemAliasNameListByCompanyId(companyId, itemAliasName));
		return responseData;
	}
	// 23-05-2019 End
	
}
