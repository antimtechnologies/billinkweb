package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;

@Repository
@Lazy
public class ItemMrpModelDao {

	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@SuppressWarnings("unchecked")
	int[] saveItemMrpData(List<ItemMrpModel> itemMrpModelList, int itemId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_MRP_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s) ", ITEM_MRP_COLUMN.ITEM_MRP, ITEM_MRP_COLUMN.MRP_WET_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE,ITEM_MRP_COLUMN.ITEM_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s , :%s) ", ITEM_MRP_COLUMN.ITEM_MRP, ITEM_MRP_COLUMN.MRP_WET_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE,ITEM_MRP_COLUMN.ITEM_ID));

		List<Map<String, Object>> itemMrpMapList = new ArrayList<>();
		Map<String, Object> itemMrpMap;
		for (ItemMrpModel itemMrpModel : itemMrpModelList) {
			itemMrpMap = new HashMap<>();
			itemMrpMap.put(ITEM_MRP_COLUMN.ITEM_MRP.toString(), itemMrpModel.getItemMrp());
			itemMrpMap.put(ITEM_MRP_COLUMN.MRP_WET_DATE.toString(), itemMrpModel.getWetDate());
			itemMrpMap.put(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString(), itemMrpModel.getWefDate());
			itemMrpMap.put(ITEM_MRP_COLUMN.ITEM_ID.toString(), itemId);
			itemMrpMapList.add(itemMrpMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), itemMrpMapList.toArray(new HashMap[0]));
	}
	
	
	List<ItemMrpModel> getItemMrpList(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s ", ITEM_MRP_COLUMN.ITEM_MRP, ITEM_MRP_COLUMN.MRP_WET_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_MRP_DETAILS))
			.append(String.format(" WHERE %s = :%s AND isDeleted = 0  ", ITEM_MRP_COLUMN.ITEM_ID, ITEM_MRP_COLUMN.ITEM_ID));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemMrpResultExctractor());
	}

	private ResultSetExtractor<List<ItemMrpModel>> getItemMrpResultExctractor() {
		return new ResultSetExtractor<List<ItemMrpModel>>() {
			@Override
			public List<ItemMrpModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemMrpModel> itemMrpModelList = new ArrayList<>();
				ItemMrpModel itemMrpModel;
				while (rs.next()) {
					itemMrpModel = new ItemMrpModel();
					itemMrpModel.setItemMrp(rs.getDouble(ITEM_MRP_COLUMN.ITEM_MRP.toString()));
					itemMrpModel.setWetDate(rs.getString(ITEM_MRP_COLUMN.MRP_WET_DATE.toString()));
					itemMrpModel.setWefDate(rs.getString(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString()));
					itemMrpModelList.add(itemMrpModel);
				}
				return itemMrpModelList;
			}
		};
	}
 
	int deleteItemMrpData(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET isDeleted = ? ", DATABASE_TABLE.ACC_ITEM_MRP_DETAILS))
				.append(String.format(" WHERE %s = ? ", ITEM_MRP_COLUMN.ITEM_ID, ITEM_MRP_COLUMN.ITEM_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, itemId });
	}
	
	List<ItemMrpModel> getItemMrpByItemIdAndDateList(int itemId,String date) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT MRP.%s, MRP.%s, MRP.%s ", ITEM_MRP_COLUMN.ITEM_MRP, ITEM_MRP_COLUMN.MRP_WET_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE))
			.append(String.format(" FROM %s AS MRP ", DATABASE_TABLE.ACC_ITEM_MRP_DETAILS))
			.append(String.format(" WHERE MRP.itemId = "+itemId+"  AND MRP.isDeleted = 0  AND  MRP.wefDate >= '"+date+"'  AND MRP.itemMrpId IN (SELECT itemMrpId  FROM  acc_item_mrp_details  WHERE wetDate <= '"+date+"') ", ITEM_MRP_COLUMN.ITEM_ID,ITEM_MRP_COLUMN.MRP_WEF_DATE,ITEM_MRP_COLUMN.MRP_ID,ITEM_MRP_COLUMN.MRP_ID,DATABASE_TABLE.ACC_ITEM_MRP_DETAILS,ITEM_MRP_COLUMN.MRP_WET_DATE));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_MRP_COLUMN.ITEM_ID.toString(), itemId);
		paramMap.put(ITEM_MRP_COLUMN.MRP_WET_DATE.toString(), date);
		paramMap.put(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString(), date);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemMrpByDateAndItemIdResultExctractor());
	}
	
	private ResultSetExtractor<List<ItemMrpModel>> getItemMrpByDateAndItemIdResultExctractor() {
		return new ResultSetExtractor<List<ItemMrpModel>>() {
			@Override
			public List<ItemMrpModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemMrpModel> itemMrpModelList = new ArrayList<>();
				ItemMrpModel itemMrpModel;
				while (rs.next()) {
					itemMrpModel = new ItemMrpModel();
					//itemMrpModel.setItemMrpId(rs.getInt(ITEM_MRP_COLUMN.MRP_ID.toString()));
					itemMrpModel.setItemMrp(rs.getDouble(ITEM_MRP_COLUMN.ITEM_MRP.toString()));
					itemMrpModel.setWetDate(rs.getString(ITEM_MRP_COLUMN.MRP_WET_DATE.toString()));
					itemMrpModel.setWefDate(rs.getString(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString()));
					itemMrpModelList.add(itemMrpModel);
				}
				return itemMrpModelList;
			}
		};
	}
}
