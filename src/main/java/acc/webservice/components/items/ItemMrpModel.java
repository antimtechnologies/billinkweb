package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.global.utils.DateUtility;

public class ItemMrpModel {

	private int itemMrpId;
	private double itemMrp;
	private boolean deleted;
	private String wefDate,wetDate;
	private ItemModel itemModel;
	
	
	public ItemMrpModel() {
		
	}
	public ItemMrpModel(ResultSet rs) throws SQLException {
		setItemMrpId(rs.getInt(ITEM_MRP_COLUMN.MRP_ID.toString()));
		setItemMrp(rs.getDouble(ITEM_MRP_COLUMN.ITEM_MRP.toString()));
	
		Timestamp fetchedWetDateTime = rs.getTimestamp(ITEM_MRP_COLUMN.MRP_WET_DATE.toString());
		setWetDate(DateUtility.converDateToUserString(fetchedWetDateTime));
		
		Timestamp fetchedWefDateTime = rs.getTimestamp(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString());
		setWefDate(DateUtility.converDateToUserString(fetchedWefDateTime));
	}
	
	
	
	public int getItemMrpId() {
		return itemMrpId;
	}
	public void setItemMrpId(int itemMrpId) {
		this.itemMrpId = itemMrpId;
	}
	public String getWefDate() {
		return wefDate;
	}
	public void setWefDate(String wefDate) {
		this.wefDate = wefDate;
	}
	public String getWetDate() {
		return wetDate;
	}
	public void setWetDate(String wetDate) {
		this.wetDate = wetDate;
	}
	public ItemModel getItemModel() {
		return itemModel;
	}
	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}
	public double getItemMrp() {
		return itemMrp;
	}
	public void setItemMrp(double itemMrp) {
		this.itemMrp = itemMrp;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
	
	
}
