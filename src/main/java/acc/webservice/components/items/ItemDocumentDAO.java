package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class ItemDocumentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<ItemDocumentModel> getItemDocumentList(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s, %s ", ITEM_DOCUMENT_COLUMN.DOCUMENT_URL_ID, ITEM_DOCUMENT_COLUMN.DOCUMENT_URL, ITEM_DOCUMENT_COLUMN.DOCUMENT_TYPE, ITEM_DOCUMENT_COLUMN.DESCRIPTION))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_DOCUMENT_DETAILS))
			.append(String.format(" WHERE %s = :%s AND %s = 0  ", ITEM_DOCUMENT_COLUMN.ITEM_ID, ITEM_DOCUMENT_COLUMN.ITEM_ID, ITEM_DOCUMENT_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_DOCUMENT_COLUMN.ITEM_ID.toString(), itemId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemDocumentResultExctractor());
	}

	private ResultSetExtractor<List<ItemDocumentModel>> getItemDocumentResultExctractor() {
		return new ResultSetExtractor<List<ItemDocumentModel>>() {
			@Override
			public List<ItemDocumentModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemDocumentModel> itemDocumentModelList = new ArrayList<>();
				while (rs.next()) {
					itemDocumentModelList.add(new ItemDocumentModel(rs));
				}
				return itemDocumentModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveItemDocumentData(List<ItemDocumentModel> itemDocumentModelList, int itemId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_DOCUMENT_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s ) ", ITEM_DOCUMENT_COLUMN.DOCUMENT_TYPE, ITEM_DOCUMENT_COLUMN.DESCRIPTION, ITEM_DOCUMENT_COLUMN.DOCUMENT_URL, ITEM_DOCUMENT_COLUMN.ITEM_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", ITEM_DOCUMENT_COLUMN.DOCUMENT_TYPE, ITEM_DOCUMENT_COLUMN.DESCRIPTION, ITEM_DOCUMENT_COLUMN.DOCUMENT_URL, ITEM_DOCUMENT_COLUMN.ITEM_ID));

		List<Map<String, Object>> itemDocumentURLMapList = new ArrayList<>();
		Map<String, Object> itemDocumentURLMap;
		for (ItemDocumentModel itemDocumentModel : itemDocumentModelList) {
			itemDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(itemDocumentModel.getDocumentUrl())) {			
				itemDocumentModel.setDocumentUrl(itemDocumentModel.getDocumentUrl().replace(ApplicationUtility.getServerURL(), ""));
			}

			itemDocumentURLMap.put(ITEM_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString(), itemDocumentModel.getDocumentType());
			itemDocumentURLMap.put(ITEM_DOCUMENT_COLUMN.DESCRIPTION.toString(), itemDocumentModel.getDescription());
			itemDocumentURLMap.put(ITEM_DOCUMENT_COLUMN.DOCUMENT_URL.toString(), itemDocumentModel.getDocumentUrl());
			itemDocumentURLMap.put(ITEM_DOCUMENT_COLUMN.ITEM_ID.toString(), itemId);
			itemDocumentURLMapList.add(itemDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), itemDocumentURLMapList.toArray(new HashMap[0]));
	}

	int deleteItemDocumentData(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", DATABASE_TABLE.ACC_ITEM_DOCUMENT_DETAILS, ITEM_DOCUMENT_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", ITEM_DOCUMENT_COLUMN.ITEM_ID, ITEM_DOCUMENT_COLUMN.ITEM_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, itemId });
	}

}
