package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.w3c.dom.ls.LSInput;

import acc.webservice.components.brand.ItemBrandModel;
import acc.webservice.components.itemcategory.ItemCategoryModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class ItemModel {

	private int itemId;
	private String itemName;
	private String itemAlias;
	private String bomName;
	private String bomAlias;
	private ItemCategoryModel category;
	private ItemBrandModel brand;
	private int hsnCode;
	private double taxCode;
	private double cess;
	private String measureUnit;
	private double quantity;
	private double purchaseRate;
	private double sellRate;
	private String profilePhotoURL;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private int companyId;
	private String gstType;
	private List<ItemDocumentModel> itemDocumentList;
	private int excellDocumentId,currentItemNumber;
	
	private String eanNo;
	private int innerContains,outerContains;
	private double innnerWeightKg,outerWeight;
	private String productMeasurement;
	private String sku;
	private int innerBoxSizeL,outerBoxSizeL,innerBoxSizeB,outerBoxSizeB,innerBoxSizeH,outerBoxSizeH;
	private List<ItemBatchModel> itemBatchList;
	private List<ItemMrpModel> itemMrpList;
	private List<ItemPurchaseRateModel> itemPurchaseRateList;
	private List<ItemAliasModel> itemAliasList;
		
	
	
	

	public ItemModel() {
	}

	public ItemModel(ResultSet rs) throws SQLException {
		setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
		setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
		setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
		setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
		setCess(rs.getDouble(ITEM_DETAILS_COLUMN.CESS.toString()));
		setMeasureUnit(rs.getString(ITEM_DETAILS_COLUMN.MEASURE_UNIT.toString()));
		setQuantity(rs.getDouble(ITEM_DETAILS_COLUMN.QUANTITY.toString()));
		setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
		setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
		setItemAlias(rs.getString(ITEM_DETAILS_COLUMN.ITEM_ALIAS.toString()));
		setBomName(rs.getString(ITEM_DETAILS_COLUMN.BOM_NAME.toString()));
		setBomAlias(rs.getString(ITEM_DETAILS_COLUMN.BOM_ALIAS.toString()));
		setEanNo(rs.getString(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString()));
		setInnerContains(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_INNER_CONTAINS.toString()));
		setOuterContains(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_OUTER_CONTAINS.toString()));
		setInnnerWeightKg(rs.getDouble(ITEM_DETAILS_COLUMN.ITEM_INNER_WEIGHT_KG.toString()));
		setOuterWeight(rs.getDouble(ITEM_DETAILS_COLUMN.ITEM_OUTER_WEIGHT.toString()));
		setProductMeasurement(rs.getString(ITEM_DETAILS_COLUMN.ITEM_PRODUCT_MEASUREMENT.toString()));
		setSku(rs.getString(ITEM_DETAILS_COLUMN.ITEM_SKU.toString()));
		
//		
		setInnerBoxSizeL(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEL.toString()));
		setOuterBoxSizeL(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEL.toString()));
		setInnerBoxSizeB(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEB.toString()));
		setOuterBoxSizeB(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEB.toString()));
		setInnerBoxSizeH(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_INNER_BOX_SIZEH.toString()));
		setOuterBoxSizeH(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_OUTER_BOX_SIZEH.toString()));
		
		
		setCurrentItemNumber(rs.getInt(ITEM_DETAILS_COLUMN.CURRENT_ITEM_NUMBER.toString()));
		setProfilePhotoURL(
				ApplicationUtility.getServerURL() + rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));
		setGstType(rs.getString(ITEM_DETAILS_COLUMN.GST_TYPE.toString()));
		UserModel objAddedBy = new UserModel();
		objAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		objAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(objAddedBy);

		Timestamp fetchedAddedDateTime = rs.getTimestamp(ITEM_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDateTime));

		UserModel objModifiedBy = new UserModel();
		objModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		objModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(objModifiedBy);

		Timestamp fetchedModifiedDateTime = rs.getTimestamp(ITEM_DETAILS_COLUMN.LAST_MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDateTime != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDateTime));
		}

		setCategory(new ItemCategoryModel(rs));
		setBrand(new ItemBrandModel(rs));
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemCategoryModel getCategory() {
		return category;
	}

	public void setCategory(ItemCategoryModel category) {
		this.category = category;
	}

	public ItemBrandModel getBrand() {
		return brand;
	}

	public void setBrand(ItemBrandModel brand) {
		this.brand = brand;
	}

	public int getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(int hsnCode) {
		this.hsnCode = hsnCode;
	}

	public double getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(double taxCode) {
		this.taxCode = taxCode;
	}

	public double getCess() {
		return cess;
	}

	public void setCess(double cess) {
		this.cess = cess;
	}

	public String getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getSellRate() {
		return sellRate;
	}

	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}

	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public List<ItemDocumentModel> getItemDocumentList() {
		return itemDocumentList;
	}

	public void setItemDocumentList(List<ItemDocumentModel> itemDocumentList) {
		this.itemDocumentList = itemDocumentList;
	}
	public String getItemAlias() {
		return itemAlias;
	}

	public void setItemAlias(String itemAlias) {
		this.itemAlias = itemAlias;
	}

	public String getBomName() {
		return bomName;
	}

	public void setBomName(String bomName) {
		this.bomName = bomName;
	}

	public String getBomAlias() {
		return bomAlias;
	}

	public void setBomAlias(String bomAlias) {
		this.bomAlias = bomAlias;
	}
	public int getExcellDocumentId() {
		return excellDocumentId;
	}

	public void setExcellDocumentId(int excellDocumentId) {
		this.excellDocumentId = excellDocumentId;
	}
	

	public String getEanNo() {
		return eanNo;
	}

	public void setEanNo(String eanNo) {
		this.eanNo = eanNo;
	}

	public int getInnerContains() {
		return innerContains;
	}

	public void setInnerContains(int innerContains) {
		this.innerContains = innerContains;
	}

	public int getOuterContains() {
		return outerContains;
	}

	public void setOuterContains(int outerContains) {
		this.outerContains = outerContains;
	}

	public double getInnnerWeightKg() {
		return innnerWeightKg;
	}

	public void setInnnerWeightKg(double innnerWeightKg) {
		this.innnerWeightKg = innnerWeightKg;
	}

	

	public double getOuterWeight() {
		return outerWeight;
	}

	public void setOuterWeight(double outerWeight) {
		this.outerWeight = outerWeight;
	}

	

	

	public int getInnerBoxSizeL() {
		return innerBoxSizeL;
	}

	public void setInnerBoxSizeL(int innerBoxSizeL) {
		this.innerBoxSizeL = innerBoxSizeL;
	}

	public int getOuterBoxSizeL() {
		return outerBoxSizeL;
	}

	public void setOuterBoxSizeL(int outerBoxSizeL) {
		this.outerBoxSizeL = outerBoxSizeL;
	}

	public int getInnerBoxSizeB() {
		return innerBoxSizeB;
	}

	public void setInnerBoxSizeB(int innerBoxSizeB) {
		this.innerBoxSizeB = innerBoxSizeB;
	}

	public int getOuterBoxSizeB() {
		return outerBoxSizeB;
	}

	public void setOuterBoxSizeB(int outerBoxSizeB) {
		this.outerBoxSizeB = outerBoxSizeB;
	}

	public int getInnerBoxSizeH() {
		return innerBoxSizeH;
	}

	public void setInnerBoxSizeH(int innerBoxSizeH) {
		this.innerBoxSizeH = innerBoxSizeH;
	}

	public int getOuterBoxSizeH() {
		return outerBoxSizeH;
	}

	public void setOuterBoxSizeH(int outerBoxSizeH) {
		this.outerBoxSizeH = outerBoxSizeH;
	}

	public String getProductMeasurement() {
		return productMeasurement;
	}

	public void setProductMeasurement(String productMeasurement) {
		this.productMeasurement = productMeasurement;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public List<ItemBatchModel> getItemBatchList() {
		return itemBatchList;
	}

	public void setItemBatchList(List<ItemBatchModel> itemBatchList) {
		this.itemBatchList = itemBatchList;
	}

	public List<ItemMrpModel> getItemMrpList() {
		return itemMrpList;
	}

	public void setItemMrpList(List<ItemMrpModel> itemMrpList) {
		this.itemMrpList = itemMrpList;
	}

	public List<ItemPurchaseRateModel> getItemPurchaseRateList() {
		return itemPurchaseRateList;
	}

	public void setItemPurchaseRateList(List<ItemPurchaseRateModel> itemPurchaseRateList) {
		this.itemPurchaseRateList = itemPurchaseRateList;
	}
	
	public List<ItemAliasModel> getItemAliasList() {
		return itemAliasList;
	}

	public void setItemAliasList(List<ItemAliasModel> itemAliasList) {
		this.itemAliasList = itemAliasList;
	}

	public int getCurrentItemNumber() {
		return currentItemNumber;
	}

	public void setCurrentItemNumber(int currentItemNumber) {
		this.currentItemNumber = currentItemNumber;
	}

	@Override
	public String toString() {
		return "ItemModel [itemId=" + itemId + ", itemName=" + itemName + ", category=" + category + ", brand=" + brand
				+ ", hsnCode=" + hsnCode + ", taxCode=" + taxCode + ", cess=" + cess + ", measureUnit=" + measureUnit
				+ ", quantity=" + quantity + ", purchaseRate=" + purchaseRate + ", sellRate=" + sellRate
				+ ", addedDateTime=" + addedDateTime + ", modifiedDateTime=" + modifiedDateTime + ", deletedDateTime="
				+ deletedDateTime + "]";
	}

}
