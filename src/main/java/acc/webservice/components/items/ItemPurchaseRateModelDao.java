package acc.webservice.components.items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_ALIAS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_PURCHASE_RATE_COLUMN;

@Repository
@Lazy
public class ItemPurchaseRateModelDao {

	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@SuppressWarnings("unchecked")
	int[] saveItemPurchaseRateData(List<ItemPurchaseRateModel> itemPurchaseRateModelList, int itemId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_ITEM_PURCHASE_RATE_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s) ", ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE, ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.ITEM_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s , :%s ) ", ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE, ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.ITEM_ID));

		List<Map<String, Object>> itemPurchaseRateMapList = new ArrayList<>();
		Map<String, Object> itemPurchaseRateMap;
		for (ItemPurchaseRateModel itemPurchaseRateModel : itemPurchaseRateModelList) {
			itemPurchaseRateMap = new HashMap<>();
			itemPurchaseRateMap.put(ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE.toString(), itemPurchaseRateModel.getItemPurchaseRate());
			itemPurchaseRateMap.put(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString(), itemPurchaseRateModel.getWetPurachaseRateDate());
			itemPurchaseRateMap.put(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString(), itemPurchaseRateModel.getWefPurchaseRateDate());
			itemPurchaseRateMap.put(ITEM_PURCHASE_RATE_COLUMN.ITEM_ID.toString(), itemId);
			itemPurchaseRateMapList.add(itemPurchaseRateMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), itemPurchaseRateMapList.toArray(new HashMap[0]));
	}
	
	List<ItemPurchaseRateModel> getItemPurchaseRateByItemId(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s ", ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE, ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_PURCHASE_RATE_DETAILS))
			.append(String.format(" WHERE %s = :%s AND isDeleted = 0  ", ITEM_PURCHASE_RATE_COLUMN.ITEM_ID, ITEM_PURCHASE_RATE_COLUMN.ITEM_ID));
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemPurchaseRateResultExctractor());
	}
	
	List<ItemPurchaseRateModel> getItemPurchaseRateByItemIdBilldate(int itemId,int converttimestamp) {
		
		StringBuilder primaryPrice = new StringBuilder();
		primaryPrice.append(String.format("select %s as %s,current_date() as %s,",ITEM_DETAILS_COLUMN.SELLING_RATE,ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE))
				.append(String.format("current_date() as %s from %s ",ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
				.append( String.format("where %s != 0 and %s= :%s",ITEM_DETAILS_COLUMN.SELLING_RATE,ITEM_DETAILS_COLUMN.ITEM_ID,ITEM_DETAILS_COLUMN.ITEM_ID));
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		List<ItemPurchaseRateModel> primaryPriceList=namedParameterJdbcTemplate.query(primaryPrice.toString(), paramMap, getItemPurchaseRateResultExctractor());
		
		if(primaryPriceList.size() > 0) {
		StringBuilder secondaryPrice = new StringBuilder();
		secondaryPrice.append(String.format(" SELECT %s, %s, %s ", ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE, ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_PURCHASE_RATE_DETAILS))
			.append(String.format(" WHERE %s = :%s AND UNIX_TIMESTAMP(%s) >= :%s AND UNIX_TIMESTAMP(%s) <= :%s and  isDeleted = 0  ", ITEM_PURCHASE_RATE_COLUMN.ITEM_ID, ITEM_PURCHASE_RATE_COLUMN.ITEM_ID,
					ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE , ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE,
					ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE,ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE));
		
		paramMap = new HashMap<>();
		paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString(), converttimestamp);
		paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString(), converttimestamp);

		List<ItemPurchaseRateModel> secondaryPriceList= namedParameterJdbcTemplate.query(secondaryPrice.toString(), paramMap, getItemPurchaseRateResultExctractor());
		if(secondaryPriceList.size()>0) {
			return secondaryPriceList;
		}
		}
		return primaryPriceList;
	}
	
	private ResultSetExtractor<List<ItemPurchaseRateModel>> getItemPurchaseRateResultExctractor() {
		return new ResultSetExtractor<List<ItemPurchaseRateModel>>() {
			@Override
			public List<ItemPurchaseRateModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemPurchaseRateModel> itemPurchaseRateModelList = new ArrayList<>();
				ItemPurchaseRateModel itemPurchaseRateModel;
				while (rs.next()) {
					itemPurchaseRateModel = new ItemPurchaseRateModel();
					itemPurchaseRateModel.setItemPurchaseRate(rs.getDouble(ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE.toString()));
					itemPurchaseRateModel.setWetPurachaseRateDate(rs.getString(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString()));
					itemPurchaseRateModel.setWefPurchaseRateDate(rs.getString(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString()));
					itemPurchaseRateModelList.add(itemPurchaseRateModel);
				}
				return itemPurchaseRateModelList;
			}
		};
	}
	
	List<ItemMrpModel> getItemMRPRateByItemIdBilldate(int itemId,int converttimestamp){

		StringBuilder primaryPrice = new StringBuilder();
		primaryPrice.append(String.format("select %s as %s,current_date() as %s,",ITEM_DETAILS_COLUMN.SELLING_RATE,ITEM_MRP_COLUMN.ITEM_MRP,ITEM_MRP_COLUMN.MRP_WEF_DATE))
				.append(String.format("current_date() as %s from %s ",ITEM_MRP_COLUMN.MRP_WET_DATE,COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
				.append( String.format("where %s != 0 and %s=:%s",ITEM_DETAILS_COLUMN.SELLING_RATE,ITEM_DETAILS_COLUMN.ITEM_ID,ITEM_DETAILS_COLUMN.ITEM_ID));
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		List<ItemMrpModel> primaryPriceList=namedParameterJdbcTemplate.query(primaryPrice.toString(), paramMap, getItemMRPResultExctractor());
		
		if(primaryPriceList.size() > 0) {
		StringBuilder secondaryPrice = new StringBuilder();
		secondaryPrice.append(String.format(" SELECT %s, %s, %s ", ITEM_MRP_COLUMN.ITEM_MRP, ITEM_MRP_COLUMN.MRP_WET_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_ITEM_MRP_DETAILS))
			.append(String.format(" WHERE %s = :%s AND UNIX_TIMESTAMP(%s) >= :%s AND UNIX_TIMESTAMP(%s) <= :%s and  isDeleted = 0  ", ITEM_MRP_COLUMN.ITEM_ID, ITEM_MRP_COLUMN.ITEM_ID,
					ITEM_MRP_COLUMN.MRP_WET_DATE , ITEM_MRP_COLUMN.MRP_WET_DATE,
					ITEM_MRP_COLUMN.MRP_WEF_DATE,ITEM_MRP_COLUMN.MRP_WEF_DATE));
		
		paramMap = new HashMap<>();
		paramMap.put(ITEM_MRP_COLUMN.ITEM_ID.toString(), itemId);
		paramMap.put(ITEM_MRP_COLUMN.MRP_WET_DATE.toString(), converttimestamp);
		paramMap.put(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString(), converttimestamp);

		List<ItemMrpModel> secondaryPriceList= namedParameterJdbcTemplate.query(secondaryPrice.toString(), paramMap, getItemMRPResultExctractor());
		if(secondaryPriceList.size()>0) {
			return secondaryPriceList;
		}
		}
		return primaryPriceList;
	}
	
	
	private ResultSetExtractor<List<ItemMrpModel>> getItemMRPResultExctractor() {
		return new ResultSetExtractor<List<ItemMrpModel>>() {
			@Override
			public List<ItemMrpModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemMrpModel> itemMRPRateModelList = new ArrayList<>();
				ItemMrpModel itemMRPModel;
				while (rs.next()) {
					itemMRPModel = new ItemMrpModel();
					itemMRPModel.setItemMrp(rs.getDouble(ITEM_MRP_COLUMN.ITEM_MRP.toString()));
					itemMRPModel.setWetDate(rs.getString(ITEM_MRP_COLUMN.MRP_WET_DATE.toString()));
					itemMRPModel.setWefDate(rs.getString(ITEM_MRP_COLUMN.MRP_WEF_DATE.toString()));
					itemMRPRateModelList.add(itemMRPModel);
				}
				return itemMRPRateModelList;
			}
		};
	}

	
	
	
	int deleteItemPurchaseRateData(int itemId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET isDeleted = ? ", DATABASE_TABLE.ACC_ITEM_PURCHASE_RATE_DETAILS))
				.append(String.format(" WHERE %s = ? ", ITEM_PURCHASE_RATE_COLUMN.ITEM_ID, ITEM_PURCHASE_RATE_COLUMN.ITEM_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, itemId });
	}
	
}
