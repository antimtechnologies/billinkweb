package acc.webservice.components.items;

import java.sql.ResultSet;

public class ItemAliasModel {
	
	private int itemAliasId;
	private String itemAliasName;
	private ItemModel itemModel;
	private int companyId;
	
	public ItemAliasModel() {
		
	}
	
	public ItemAliasModel(ResultSet rs) {
		
	}
	
	
	public int getItemAliasId() {
		return itemAliasId;
	}
	public void setItemAliasId(int itemAliasId) {
		this.itemAliasId = itemAliasId;
	}
	public String getItemAliasName() {
		return itemAliasName;
	}
	public void setItemAliasName(String itemAliasName) {
		this.itemAliasName = itemAliasName;
	}
	public ItemModel getItemModel() {
		return itemModel;
	}
	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
	

}
