package acc.webservice.components.items;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class ItemService {

	@Autowired
	private ItemDAO itemDAO;

	@Autowired
	private ItemDocumentDAO itemDocumentDAO;
	
	@Autowired
	private ItemBatchModelDao itemBatchModelDao;

	@Autowired
	private ItemMrpModelDao itemMrpModelDao;
	
	@Autowired
	private ItemAliasDao itemAliasDao;
	
	@Autowired
	private CompanyService companyService;
	
	

	
	
	
	@Autowired
	private ItemPurchaseRateModelDao itemPurchaseRateModelDao;
	
	Integer getItemCount(String itemName, int companyId) {
		return itemDAO.getItemCount(itemName, companyId);
	}

	public List<ItemModel> getItemListData(int companyId, String itemName, int start, int numberOfRecord) {
		return itemDAO.getItemListData(companyId, itemName, start, numberOfRecord);
	}

	public ItemModel getItemDetailsById(int companyId, int itemId) {
		ItemModel itemModel = itemDAO.getItemDetailsById(companyId, itemId);
		itemModel.setItemDocumentList(itemDocumentDAO.getItemDocumentList(itemId));
		itemModel.setItemBatchList(itemBatchModelDao.getItemBatchList(itemId));
		itemModel.setItemAliasList(itemAliasDao.getItemAliasNameByItemId(itemId));
		itemModel.setItemMrpList(itemMrpModelDao.getItemMrpList(itemId));
		itemModel.setItemPurchaseRateList(itemPurchaseRateModelDao.getItemPurchaseRateByItemId(itemId));
		return itemModel;
	}

	public ItemModel saveItemData(ItemModel itemModel) {
		ItemModel model = itemDAO.saveItemData(itemModel);
		
		if (ApplicationUtility.getSize(itemModel.getItemDocumentList()) > 0) {
			itemDocumentDAO.saveItemDocumentData(itemModel.getItemDocumentList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemBatchList()) > 0) {
			itemBatchModelDao.saveBatchData(itemModel.getItemBatchList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemMrpList()) > 0) {
			itemMrpModelDao.saveItemMrpData(itemModel.getItemMrpList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemPurchaseRateList()) > 0) {
			itemPurchaseRateModelDao.saveItemPurchaseRateData(itemModel.getItemPurchaseRateList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemAliasList()) > 0) {
			itemAliasDao.saveitemAlias(itemModel.getItemAliasList(), itemModel.getItemId(),itemModel.getCompanyId());
		}
		companyService.updateCompanyLastItemNumber(itemModel.getCompanyId(), itemDAO.getMaxItemNumber(itemModel.getCompanyId()));
		return model;
	}

	public ItemModel updateItemData(ItemModel itemModel) {
		itemDocumentDAO.deleteItemDocumentData(itemModel.getItemId());
		itemBatchModelDao.deleteItemBatchData(itemModel.getItemId());
		itemAliasDao.deleteItemAliasData(itemModel.getItemId());
		itemMrpModelDao.deleteItemMrpData(itemModel.getItemId());
		itemPurchaseRateModelDao.deleteItemPurchaseRateData(itemModel.getItemId());
		
		
		
		if (ApplicationUtility.getSize(itemModel.getItemDocumentList()) > 0) {
			itemDocumentDAO.saveItemDocumentData(itemModel.getItemDocumentList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemBatchList()) > 0) {
			itemBatchModelDao.saveBatchData(itemModel.getItemBatchList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemMrpList()) > 0) {
			itemMrpModelDao.saveItemMrpData(itemModel.getItemMrpList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemPurchaseRateList()) > 0) {
			itemPurchaseRateModelDao.saveItemPurchaseRateData(itemModel.getItemPurchaseRateList(), itemModel.getItemId());
		}
		if (ApplicationUtility.getSize(itemModel.getItemAliasList()) > 0) {
			itemAliasDao.saveitemAlias(itemModel.getItemAliasList(), itemModel.getItemId(),itemModel.getCompanyId());
		}
		return itemDAO.updateItemData(itemModel);
	}

	public int deleteItemData(int companyId, int itemId, int deletedBy) {
		itemDocumentDAO.deleteItemDocumentData(itemId);
		return itemDAO.deleteItemData(companyId, itemId, deletedBy);
	}

	
	public List<ItemPurchaseRateModel> getPurchaseRate(int itemId, int billdate) {
		
		return itemPurchaseRateModelDao.getItemPurchaseRateByItemIdBilldate(itemId,billdate);
	}
	
	public List<ItemMrpModel> getMRPRate(int itemId, int billdate) {
		
		return itemPurchaseRateModelDao.getItemMRPRateByItemIdBilldate(itemId,billdate);
	}
	
	public List<ItemModel> getItemBasicDetailList(int companyId, int start, int noOfRecord, String itemName) {
		return itemDAO.getItemBasicDetailList(companyId, start, noOfRecord, itemName);
	}

	public int getItemIdByName(int companyId, String itemName) {
		return itemDAO.getItemIdByName(companyId, itemName);
	}

	public ItemBatchModel saveItembatchdata(ItemBatchModel itemBatchModel) {
		
		return itemBatchModelDao.saveItemBatch(itemBatchModel);
	}
	
	public ItemBatchModel checkuniqueandInsertBatch(ItemBatchModel itemBatchModel,int itemId) {
		return itemBatchModelDao.checkuniqueandInsertBatch(itemBatchModel,itemId);
	}
	
	public ItemBatchModel saveBatch(String batchNo,String mfgDate,String expDate,String shelfLife,int itemId) {
		
		return itemBatchModelDao.saveBatch(batchNo,mfgDate,expDate,shelfLife,itemId);
	}
	
	public List<ItemAliasModel> getitemAliasNameByCompanyId(int companyId){
		return itemAliasDao.getItemAliasNameByCompanyId(companyId);
	}
	
	public List<ItemMrpModel> getItemMrpbyItemIdAndDate(int itemId,String date){
		return itemMrpModelDao.getItemMrpByItemIdAndDateList(itemId,date);
	}
	
	public ItemBatchModel getOneBatch(int batchId) {
		return itemBatchModelDao.getOneBatch(batchId);
	}
	
	// 23-05-2019 Start
	public List<ItemModel> getIteList(int companyId, String itemName) {
		return itemDAO.getItemList(companyId, itemName);
	}
	
	public List<ItemModel> getIteListByEanNo(int companyId, String itemEanNo) {
		return itemDAO.getItemListByeanNo(companyId, itemEanNo);
	}
	public List<ItemAliasModel> getItemAliasNameListByCompanyId(int companyId,String itemAliasName){
		return itemAliasDao.getItemAliasNameListByCompanyId(companyId,itemAliasName);
	}
	// 23-05-2109 End
}
