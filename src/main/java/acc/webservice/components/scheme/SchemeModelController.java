package acc.webservice.components.scheme;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.components.location.LocationModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_ONE_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_TWO_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/schemeData/")
public class SchemeModelController {

	
	@Autowired
	SchemeModelService schemeModelOneService;
	
	@RequestMapping(value="saveSchemeOneData", method=RequestMethod.POST)
	public Map<String, Object> saveSchemeOneData(@RequestBody SchemeMasterModelOne schemeMasterModelOne)
	{
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", schemeModelOneService.saveSchemeOneData(schemeMasterModelOne));
		return response;
	}
	

	@RequestMapping(value="overruledSchemaOne", method=RequestMethod.POST)
	public Map<String, Object> overruledSchemaOne(@RequestBody Map<String, Object> parameter) {
		Map<String, Object> response = new HashMap<>();
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int mrpfrom=ApplicationUtility.getIntValue(parameter, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString());
		response.put("status", "success");
		boolean flag=schemeModelOneService.overRuledSchemeOne(companyId,mrpfrom);
		if(flag) {
			response.put("data", "exists");
		}else {
			response.put("data", "None");
		}
		return response;
	}
	
	
	@RequestMapping(value="overruledSchemaTwo", method=RequestMethod.POST)
	public Map<String, Object> overruledSchemaTwo(@RequestBody Map<String, Object> parameter) {
		Map<String, Object> response = new HashMap<>();
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int itemid=ApplicationUtility.getIntValue(parameter, SCHEME_TWO_COLUMN.ITEM_ID.toString());
		int qty=ApplicationUtility.getIntValue(parameter, SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM.toString());
		response.put("status", "success");
		boolean flag=schemeModelOneService.overruledSchemaTwo(companyId, itemid, qty);
		if(flag) {
			response.put("data", "exists");
		}else {
			response.put("data", "None");
		}
		return response;
	}
	
	@RequestMapping(value="saveSchemeTwoData", method=RequestMethod.POST)
	public Map<String, Object> saveSchemeTwoData(@RequestBody SchemeMasterModelTwo schemeMasterModelTwo)
	{
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", schemeModelOneService.saveSchemeTwoData(schemeMasterModelTwo));
		return response;
	}
	
	@RequestMapping(value="getSchemeOneData",method=RequestMethod.POST)
	public Map<String,Object> getSchemeOneData(@RequestBody Map<String, Object> parameter){
		int start=ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		int companyId 			= ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		return Collections.singletonMap("data", schemeModelOneService.getSchemeOneListObj(start, noOfRecord,companyId));
	}
	
	
	@RequestMapping(value="getSchemeTwoData",method=RequestMethod.POST)
	public Map<String,Object> getSchemeTwoData(@RequestBody Map<String, Object> parameter){
		int start=ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		return Collections.singletonMap("data", schemeModelOneService.getSchemeTwoListObj(start, noOfRecord,companyId));
	}
	
	
	@RequestMapping(value="getSchemeOneDataByMrp",method=RequestMethod.POST)
	public Map<String,Object> getSchemeOneDataByMrp(@RequestBody Map<String, Object> parameter){
		String mrp=ApplicationUtility.getStrValue(parameter, "mrp");
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		return Collections.singletonMap("data", schemeModelOneService.getSchemeOneByMrp(companyId,mrp));
	}
	
	@RequestMapping(value = "getFreeItemByItemId", method = RequestMethod.POST)
	public Map<String, Object> getFreeItemById(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, SCHEME_TWO_COLUMN.ITEM_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getFreeItemDetailsByItemId(itemId));
		return responseData;
	}
	
	@RequestMapping(value = "getFreeItemNameByItemId", method = RequestMethod.POST)
	public Map<String, Object> getFreeItemNameById(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, SCHEME_TWO_COLUMN.ITEM_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getFreeNameByItemId(itemId));
		return responseData;
	}
	
	@RequestMapping(value = "getBatchItemDetailsByItemId", method = RequestMethod.POST)
	public Map<String, Object> getBatchItemDetilById(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, ITEM_BATCH_COLUMN.ITEM_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getItemBatch(itemId));
		return responseData;
	}
	
	
	@RequestMapping(value = "getBatchItemDetails", method = RequestMethod.POST)
	public Map<String, Object> getBatchItemDetilByItemID(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, ITEM_BATCH_COLUMN.ITEM_ID.toString());
		String batchNo = ApplicationUtility.getStrValue(params, ITEM_BATCH_COLUMN.BATCH_NO.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getItembatchDetials(itemId, batchNo));
		return responseData;
	}
	
	@RequestMapping(value = "getLastBatchItemDetails", method = RequestMethod.POST)
	public Map<String, Object> getLastAddBatch(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, ITEM_BATCH_COLUMN.ITEM_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getLastItembatchDetials(itemId));
		return responseData;
	}
	
	
	@RequestMapping(value = "getInvoiceAmountDiscount", method = RequestMethod.POST)
	public Map<String, Object> getInvoiceDiscount(@RequestBody Map<String, Object> params) {
		int amount = ApplicationUtility.getIntValue(params, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.getInvoiceAmountDiscount(amount));
		return responseData;
	}
	
	
	@RequestMapping(value="deleteSchemeOne", method=RequestMethod.POST)
	public Map<String, Object> deleteSchemeOne(@RequestBody Map<String, Object> parameter) {
		int schemeId = ApplicationUtility.getIntValue(parameter, SCHEME_ONE_COLUMN.SCHEME_ONE_ID.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.deleteSchemeOne(schemeId, companyId, deletedBy));
		return responseData;
	}
	@RequestMapping(value="deleteSchemeTwo", method=RequestMethod.POST)
	public Map<String, Object> deleteSchemeTwo(@RequestBody Map<String, Object> parameter) {
		int schemeId = ApplicationUtility.getIntValue(parameter, SCHEME_TWO_COLUMN.SCHEME_TWO_ID.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", schemeModelOneService.deleteSchemeTwo(schemeId, companyId, deletedBy));
		return responseData;
	}
}
