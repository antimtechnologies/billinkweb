package acc.webservice.components.scheme;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_ONE_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_TWO_COLUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class SchemeMasterModelTwoDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	
	SchemeMasterModelTwo saveSchemeTwo(SchemeMasterModelTwo schemeMasterModelTwo) {
		StringBuilder sqlQuerySchemeTwoSave = new StringBuilder();
		sqlQuerySchemeTwoSave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_SCHEME_TWO_DETAILS))
							 .append(String.format("( %s, %s, %s, %s,%s )", SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM,SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM,SCHEME_TWO_COLUMN.ITEM_ID,SCHEME_TWO_COLUMN.FREE_ITEM_ID,SCHEME_TWO_COLUMN.SCHEME_COMPANYID))
							 .append(" VALUES ")
							 .append(String.format("( :%s, :%s, :%s, :%s,:%s ) ", SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM,SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM,SCHEME_TWO_COLUMN.ITEM_ID,SCHEME_TWO_COLUMN.FREE_ITEM_ID,SCHEME_TWO_COLUMN.SCHEME_COMPANYID));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM.toString(), schemeMasterModelTwo.getTotalQtyOfItem())
					.addValue(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM.toString(), schemeMasterModelTwo.getTotalQtyOfFreeItem())
					.addValue(SCHEME_TWO_COLUMN.ITEM_ID.toString(), schemeMasterModelTwo.getItemId())
					.addValue(SCHEME_TWO_COLUMN.FREE_ITEM_ID.toString(), schemeMasterModelTwo.getFreeItemId())
					.addValue(SCHEME_TWO_COLUMN.SCHEME_COMPANYID.toString(), schemeMasterModelTwo.getCompanyId());
		
		namedParameterJdbcTemplate.update(sqlQuerySchemeTwoSave.toString(), parameters, holder);
		schemeMasterModelTwo.setSchemeTwoId(holder.getKey().intValue());
		return schemeMasterModelTwo;
	}
	
	public boolean overruledSchemaTwo(int companyId, int itemid,int qty) {
		StringBuilder sqlQuery = new StringBuilder(
				String.format("SELECT * FROM %s where %s=:%s and isdeleted=0 and %s = :%s and  %s = :%s ",  DATABASE_TABLE.ACC_SCHEME_TWO_DETAILS,
						SCHEME_TWO_COLUMN.SCHEME_COMPANYID, SCHEME_TWO_COLUMN.SCHEME_COMPANYID,
						 SCHEME_TWO_COLUMN.ITEM_ID,  SCHEME_TWO_COLUMN.ITEM_ID,
						 SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM,  SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM));

		SqlParameterSource parameters = new MapSqlParameterSource()

				.addValue(SCHEME_TWO_COLUMN.SCHEME_COMPANYID.toString(), companyId)
				.addValue(SCHEME_TWO_COLUMN.ITEM_ID.toString(), itemid)
				.addValue(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM.toString(), qty);
		SchemeMasterModelTwo schmeTwo = namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters,getFreeItemByItemIdExtractor());
		if (schmeTwo.getFreeItemId() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	SchemeMasterModelTwo getFreeItemByItemId(int itemId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT %s , %s , %s  ",SCHEME_TWO_COLUMN.FREE_ITEM_ID,SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM,SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM))
						.append(String.format(" FROM %s  ", DATABASE_TABLE.ACC_SCHEME_TWO_DETAILS))
						.append(String.format(" WHERE isDeleted = 0 and %s = :%s ", SCHEME_TWO_COLUMN.ITEM_ID, SCHEME_TWO_COLUMN.ITEM_ID));
						

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(SCHEME_TWO_COLUMN.ITEM_ID.toString(), itemId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getFreeItemByItemIdExtractor());
	}
	
	private ResultSetExtractor<SchemeMasterModelTwo> getFreeItemByItemIdExtractor() {
		return new ResultSetExtractor<SchemeMasterModelTwo>() {
			@Override
			public SchemeMasterModelTwo extractData(ResultSet rs) throws SQLException {
				SchemeMasterModelTwo schemeModel = new SchemeMasterModelTwo();
				while (rs.next()) {
					schemeModel = new SchemeMasterModelTwo();
					schemeModel.setFreeItemId(rs.getInt(SCHEME_TWO_COLUMN.FREE_ITEM_ID.toString()));
					schemeModel.setTotalQtyOfItem(rs.getInt(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM.toString()));
					schemeModel.setTotalQtyOfFreeItem(rs.getInt(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM.toString()));
				}
				return schemeModel;
			}
		};
	}
	
	
	ItemModel getFreeItemName(int itemId) {
		
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT %s ",ITEM_DETAILS_COLUMN.ITEM_NAME))
						.append(String.format(" FROM %s  ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
						.append(String.format(" WHERE %s = :%s ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_ID));
						

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_DETAILS_COLUMN.ITEM_ID.toString(), itemId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getFreeNameExtractor());
	}
	
	
	private ResultSetExtractor<ItemModel> getFreeNameExtractor() {
		return new ResultSetExtractor<ItemModel>() {
			@Override
			public ItemModel extractData(ResultSet rs) throws SQLException {
				ItemModel itemModel = new ItemModel();
				while (rs.next()) {
					itemModel = new ItemModel();
					itemModel.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
				}
				return itemModel;
			}
		};
	}
	
	List<ItemBatchModel> getItemBatchDetailsByItemID(int itemId) {
	
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT  %s, %s, %s, %s ",ITEM_BATCH_COLUMN.BATCH_ID,ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE))
						.append(String.format(" FROM %s  ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
						.append(String.format(" WHERE %s = :%s  AND isDeleted = 0", ITEM_BATCH_COLUMN.ITEM_ID, ITEM_BATCH_COLUMN.ITEM_ID));
						

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemBatchExtractor());
	}
	
	private ResultSetExtractor<List<ItemBatchModel>> getItemBatchExtractor() {
		return new ResultSetExtractor<List<ItemBatchModel>>() {
			@Override
			public List<ItemBatchModel> extractData(ResultSet rs) throws SQLException {
				List<ItemBatchModel> itembatchlist=new ArrayList<ItemBatchModel>();
				
				while (rs.next()) {
					ItemBatchModel itemBatchModel = new ItemBatchModel();
					itemBatchModel.setBatchId(rs.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
					itemBatchModel.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
					itemBatchModel.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
					itemBatchModel.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
					itembatchlist.add(itemBatchModel);
				}
				return itembatchlist;
			}
		};
	}
	
	
	List<ItemBatchModel> getItemBatchByItemId(int itemId,String batchNo){
		
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT %s, %s, %s  ", ITEM_BATCH_COLUMN.BATCH_NO,ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE))
							.append(String.format(" FROM %s  ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
							.append(String.format(" WHERE %s LIKE :%s OR '%%' = :%s ", ITEM_BATCH_COLUMN.BATCH_NO,ITEM_BATCH_COLUMN.BATCH_NO,ITEM_BATCH_COLUMN.BATCH_NO))
							.append(String.format(" AND %s = :%s ", ITEM_BATCH_COLUMN.ITEM_ID, ITEM_BATCH_COLUMN.ITEM_ID));
		
		Map<String, Object> parameters = new HashMap<>();
		
		parameters.put(ITEM_BATCH_COLUMN.BATCH_NO.toString(), "%" + batchNo + "%");
		parameters.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getItemBatchDetailsListExtractor());
	}
	
	private ResultSetExtractor<List<ItemBatchModel>> getItemBatchDetailsListExtractor() {
		return new ResultSetExtractor<List<ItemBatchModel>>() {
			@Override
			public List<ItemBatchModel> extractData(ResultSet rs) throws SQLException {
				List<ItemBatchModel> itemBatchList = new ArrayList<>();
				ItemBatchModel itemBatchModel;
				while (rs.next()) {
					itemBatchModel = new ItemBatchModel();
					itemBatchModel.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
					itemBatchModel.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
					itemBatchModel.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
					itemBatchList.add(itemBatchModel);
				}
				return itemBatchList;
			}
		};
	}
	
	// last insert recode get
	ItemBatchModel getlastAddItemBatchDetails(int itemId) {
		
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format("SELECT  %s, %s, %s, %s ",ITEM_BATCH_COLUMN.BATCH_ID,ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE))
						.append(String.format(" FROM %s  ", DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS))
						.append(String.format(" WHERE %s = :%s ", ITEM_BATCH_COLUMN.ITEM_ID, ITEM_BATCH_COLUMN.ITEM_ID));
						

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ITEM_BATCH_COLUMN.ITEM_ID.toString(), itemId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLastAddItemBatchExtractor());
	}
	
	private ResultSetExtractor<ItemBatchModel> getLastAddItemBatchExtractor() {
		return new ResultSetExtractor<ItemBatchModel>() {
			@Override
			public ItemBatchModel extractData(ResultSet rs) throws SQLException {
				ItemBatchModel itemBatchModel = new ItemBatchModel();
				if (rs.next()) {
					itemBatchModel = new ItemBatchModel();
					itemBatchModel.setBatchId(rs.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
					itemBatchModel.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
					itemBatchModel.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
					itemBatchModel.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
				}
				return itemBatchModel;
			}
		};
	}
	
	
	public int deleteSchemeTwo(int schemetwoId, int companyId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_SCHEME_TWO_DETAILS))
				.append(String.format(" %s = :%s, ", SCHEME_TWO_COLUMN.IS_DELETED,
						SCHEME_TWO_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", SCHEME_TWO_COLUMN.DELETED_BY,
						SCHEME_TWO_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", SCHEME_TWO_COLUMN.DELETED_DATE_TIME,
						SCHEME_TWO_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", SCHEME_TWO_COLUMN.SCHEME_TWO_ID,
						SCHEME_TWO_COLUMN.SCHEME_TWO_ID))
				.append(String.format(" AND %s = :%s  ", SCHEME_TWO_COLUMN.SCHEME_COMPANYID,
						SCHEME_TWO_COLUMN.SCHEME_COMPANYID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SCHEME_TWO_COLUMN.SCHEME_TWO_ID.toString(), schemetwoId)
				.addValue(SCHEME_TWO_COLUMN.IS_DELETED.toString(), 1)
				.addValue(SCHEME_TWO_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(SCHEME_TWO_COLUMN.SCHEME_COMPANYID.toString(), companyId)
				.addValue(SCHEME_TWO_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
	
}
