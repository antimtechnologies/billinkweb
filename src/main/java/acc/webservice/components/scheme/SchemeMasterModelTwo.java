package acc.webservice.components.scheme;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.SCHEME_TWO_COLUMN;

public class SchemeMasterModelTwo {

	private int schemeTwoId;
	private int totalQtyOfItem,totalQtyOfFreeItem,itemId,freeItemId;
	private String originalItem;
	private String freeItem;
	private int companyId;
	public SchemeMasterModelTwo() {}
	public SchemeMasterModelTwo(ResultSet rs) {
	}
	
	public SchemeMasterModelTwo(ResultSet rs,boolean hybrid) throws SQLException {
		
		setSchemeTwoId(rs.getInt(SCHEME_TWO_COLUMN.SCHEME_TWO_ID.toString()));
		setTotalQtyOfFreeItem(rs.getInt(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM.toString()));
		setTotalQtyOfItem(rs.getInt(SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM.toString()));
		setFreeItem(rs.getString("freeitem"));
		setOriginalItem(rs.getString("originalitem"));
		setCompanyId(rs.getInt(SCHEME_TWO_COLUMN.SCHEME_COMPANYID.toString()));
		
	}
	
	
	
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getOriginalItem() {
		return originalItem;
	}
	
	
	public void setOriginalItem(String originalItem) {
		this.originalItem = originalItem;
	}
	
	public String getFreeItem() {
		return freeItem;
	}
	public void setFreeItem(String freeItem) {
		this.freeItem = freeItem;
	}
	public int getSchemeTwoId() {
		return schemeTwoId;
	}
	public void setSchemeTwoId(int schemeTwoId) {
		this.schemeTwoId = schemeTwoId;
	}
	public int getTotalQtyOfItem() {
		return totalQtyOfItem;
	}
	public void setTotalQtyOfItem(int totalQtyOfItem) {
		this.totalQtyOfItem = totalQtyOfItem;
	}
	public int getTotalQtyOfFreeItem() {
		return totalQtyOfFreeItem;
	}
	public void setTotalQtyOfFreeItem(int totalQtyOfFreeItem) {
		this.totalQtyOfFreeItem = totalQtyOfFreeItem;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getFreeItemId() {
		return freeItemId;
	}
	public void setFreeItemId(int freeItemId) {
		this.freeItemId = freeItemId;
	}
	
	
	
}
