package acc.webservice.components.scheme;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.SCHEME_ONE_COLUMN;

public class SchemeMasterModelOne {

	private int schemeOneId;
	private double totalInvoiceMrpTo, totalInvoiceMrpFrom, totalMrpOnDiscount;
	private boolean isDeleted;
	private int companyId;

	public SchemeMasterModelOne() {
	}

	public SchemeMasterModelOne(ResultSet rs) throws SQLException {
		setSchemeOneId(rs.getInt(SCHEME_ONE_COLUMN.SCHEME_ONE_ID.toString()));
		setTotalInvoiceMrpTo(rs.getDouble(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString()));
		setTotalInvoiceMrpFrom(rs.getDouble(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString()));
		setTotalMrpOnDiscount(rs.getDouble(SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT.toString()));
		setCompanyId(rs.getInt(SCHEME_ONE_COLUMN.SCHEME_COMPANYID.toString()));
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getSchemeOneId() {
		return schemeOneId;
	}

	public void setSchemeOneId(int schemeOneId) {
		this.schemeOneId = schemeOneId;
	}

	public double getTotalInvoiceMrpTo() {
		return totalInvoiceMrpTo;
	}

	public void setTotalInvoiceMrpTo(double totalInvoiceMrpTo) {
		this.totalInvoiceMrpTo = totalInvoiceMrpTo;
	}

	public double getTotalInvoiceMrpFrom() {
		return totalInvoiceMrpFrom;
	}

	public void setTotalInvoiceMrpFrom(double totalInvoiceMrpFrom) {
		this.totalInvoiceMrpFrom = totalInvoiceMrpFrom;
	}

	public double getTotalMrpOnDiscount() {
		return totalMrpOnDiscount;
	}

	public void setTotalMrpOnDiscount(double totalMrpOnDiscount) {
		this.totalMrpOnDiscount = totalMrpOnDiscount;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
