package acc.webservice.components.scheme;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.location.LocationModel;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;


@Service
@Lazy
public class SchemeModelService {

	@Autowired
	SchemeModelOneDao schemeModelOneDao;
	
	
	
	@Autowired
	SchemeMasterModelTwoDao schemeMasterModelTwoDao;
	
	

	public SchemeMasterModelOne saveSchemeOneData(SchemeMasterModelOne schemeMasterModelOne) {
		return schemeModelOneDao.saveSchemeOne(schemeMasterModelOne);
	}
	
	public boolean overRuledSchemeOne(int companyId,int totalInvoiceamountFrom)
	{
		return schemeModelOneDao.overruledSchemaOne(companyId,totalInvoiceamountFrom);
	}
	
	public boolean overruledSchemaTwo(int companyId,int itemid,int qty) {
		return schemeMasterModelTwoDao.overruledSchemaTwo(companyId, itemid, qty);
	}
	
	public SchemeMasterModelTwo saveSchemeTwoData(SchemeMasterModelTwo schemeMasterModelTwo) {
		return schemeMasterModelTwoDao.saveSchemeTwo(schemeMasterModelTwo);
	}

	public SchemeMasterModelTwo getFreeItemDetailsByItemId(int itemId) {
		SchemeMasterModelTwo schemeModel = schemeMasterModelTwoDao.getFreeItemByItemId(itemId);
		return schemeModel;
	}
	
	
	
	public List<SchemeMasterModelOne> getSchemeOneListObj(int start, int totalrecords,int companyId) {

		return schemeModelOneDao.getSchemeOneListObj(start, totalrecords,companyId);
	}

	public List<SchemeMasterModelTwo> getSchemeTwoListObj(int start, int totalrecords,int companyId) {

		return schemeModelOneDao.getSchemeTwoListObj(start, totalrecords,companyId);
	}
	
	public List<SchemeMasterModelOne> getSchemeOneByMrp(int companyId,String mrp) {

		return schemeModelOneDao.getSchemeOneByMrp(companyId,mrp);
	}
	
	public ItemModel getFreeNameByItemId(int itemId) {
		ItemModel itemModel = schemeMasterModelTwoDao.getFreeItemName(itemId);
		return itemModel;
	}
	
	public List<ItemBatchModel> getItemBatch(int itemId) {
		List<ItemBatchModel> batchModel = schemeMasterModelTwoDao.getItemBatchDetailsByItemID(itemId);
		return batchModel;
	}
	
	public List<ItemBatchModel> getItembatchDetials(int itemId,String batchNo)
	{
		return schemeMasterModelTwoDao.getItemBatchByItemId(itemId, batchNo);
	}
	
	public ItemBatchModel getLastItembatchDetials(int itemId)
	{
		return schemeMasterModelTwoDao.getlastAddItemBatchDetails(itemId);
	}
	
	public List<SchemeMasterModelOne> getInvoiceAmountDiscount(int amount){
		return schemeModelOneDao.getInvoiceAmountDiscount(amount);
	}
	
	
	public int deleteSchemeOne(int schemeId, int companyId, int deletedBy) {
		return schemeModelOneDao.deleteSchemeOne(schemeId, companyId, deletedBy);
	}
	
	
	public int deleteSchemeTwo(int schemeId, int companyId, int deletedBy) {
		return schemeMasterModelTwoDao.deleteSchemeTwo(schemeId, companyId, deletedBy);
	}
	
	
}
