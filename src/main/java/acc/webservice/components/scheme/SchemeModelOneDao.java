package acc.webservice.components.scheme;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.items.ItemMrpModel;
import acc.webservice.components.location.LocationModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_ONE_COLUMN;
import acc.webservice.enums.DatabaseEnum.SCHEME_TWO_COLUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class SchemeModelOneDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	SchemeMasterModelOne saveSchemeOne(SchemeMasterModelOne schemeMasterModelOne) {
		StringBuilder sqlQuerySchemeOneSave = new StringBuilder();
		sqlQuerySchemeOneSave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS))
				.append(String.format("( %s, %s, %s,%s )", SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
						SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT,
						SCHEME_ONE_COLUMN.SCHEME_COMPANYID))
				.append(" VALUES ")
				.append(String.format("( :%s, :%s, :%s,:%s ) ", SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
						SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT,
						SCHEME_ONE_COLUMN.SCHEME_COMPANYID));

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString(), schemeMasterModelOne.getTotalInvoiceMrpTo())
				.addValue(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString(),
						schemeMasterModelOne.getTotalInvoiceMrpFrom())
				.addValue(SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT.toString(),
						schemeMasterModelOne.getTotalMrpOnDiscount())
				.addValue(SCHEME_ONE_COLUMN.SCHEME_COMPANYID.toString(), schemeMasterModelOne.getCompanyId());

		namedParameterJdbcTemplate.update(sqlQuerySchemeOneSave.toString(), parameters, holder);
		schemeMasterModelOne.setSchemeOneId(holder.getKey().intValue());
		return schemeMasterModelOne;
	}

	public boolean overruledSchemaOne(int companyId, int invoiceAmount) {
		StringBuilder sqlQuery = new StringBuilder(
				String.format("SELECT * FROM %s where %s=:%s and isdeleted=0 and %s >= :%s", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS,
						SCHEME_ONE_COLUMN.SCHEME_COMPANYID, SCHEME_ONE_COLUMN.SCHEME_COMPANYID,
						SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO));

		SqlParameterSource parameters = new MapSqlParameterSource()

				.addValue(SCHEME_ONE_COLUMN.SCHEME_COMPANYID.toString(), companyId)
				.addValue(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString(), invoiceAmount);
		List<SchemeMasterModelOne> schmeOneList = namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters,
				getSchemeOneResultSetExctractor());
		if (schmeOneList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	List<SchemeMasterModelOne> getInvoiceAmountDiscount(int amount) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT MRP.%s, MRP.%s, MRP.%s ", SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
				SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT))
				.append(String.format(" FROM %s AS MRP ", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS))
				.append(String.format(" WHERE MRP.isDeleted = 0  AND  MRP.totalInvoiceMrpTo >= '" + amount
						+ "'  AND MRP.totalInvoiceMrpFrom <= '" + amount + "' "));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString(), amount);
		paramMap.put(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString(), amount);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemDiscountResultExctractor());
	}

	List<SchemeMasterModelOne> getSchemeOneListObj(int start, int noOfRecord, int companyId) {

		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT %s, %s,%s,%s,%s ", SCHEME_ONE_COLUMN.SCHEME_ONE_ID,
				SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
				SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT, SCHEME_ONE_COLUMN.SCHEME_COMPANYID))
				.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS))
				.append(String.format(" where isDeleted = 0  and %s=%s", SCHEME_TWO_COLUMN.SCHEME_COMPANYID, companyId));
		;

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" order by %s desc LIMIT :%s, :%s ", SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
					APPLICATION_GENERIC_ENUM.START.toString(), APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getSchemeOneResultSetExctractor());
	}

	List<SchemeMasterModelTwo> getSchemeTwoListObj(int start, int noOfRecord, int companyId) {

		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT %s, %s,%s,%s,aiddf.%s as freeitem,aiddo.%s as originalitem,actd.%s ",
				SCHEME_TWO_COLUMN.SCHEME_TWO_ID, SCHEME_TWO_COLUMN.TOTAL_QTY_OF_FREE_ITEM,
				SCHEME_TWO_COLUMN.TOTAL_QTY_OF_ITEM, SCHEME_TWO_COLUMN.FREE_ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME,
				ITEM_DETAILS_COLUMN.ITEM_NAME, SCHEME_TWO_COLUMN.SCHEME_COMPANYID))
				.append(String.format(" FROM %s as actd ", DATABASE_TABLE.ACC_SCHEME_TWO_DETAILS))
				.append(String.format(" inner join %s as aiddf", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
				.append(String.format(" on aiddf.%s=actd.%s", ITEM_DETAILS_COLUMN.ITEM_ID,
						SCHEME_TWO_COLUMN.FREE_ITEM_ID))
				.append(String.format(" inner join %s as aiddo", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
				.append(String.format(" on aiddo.%s=actd.%s", ITEM_DETAILS_COLUMN.ITEM_ID, SCHEME_TWO_COLUMN.ITEM_ID))
				.append(String.format(" where actd.isDeleted = 0 and actd.%s=%s", SCHEME_TWO_COLUMN.SCHEME_COMPANYID, companyId));

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(),
					APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getSchemeTwoResultSetExctractor());
	}

	private ResultSetExtractor<List<SchemeMasterModelOne>> getSchemeOneResultSetExctractor() {
		return new ResultSetExtractor<List<SchemeMasterModelOne>>() {

			@Override
			public List<SchemeMasterModelOne> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SchemeMasterModelOne> schemeOneList = new ArrayList<>();
				while (rs.next()) {
					schemeOneList.add(new SchemeMasterModelOne(rs));
				}
				return schemeOneList;
			}
		};
	}

	private ResultSetExtractor<List<SchemeMasterModelTwo>> getSchemeTwoResultSetExctractor() {
		return new ResultSetExtractor<List<SchemeMasterModelTwo>>() {

			@Override
			public List<SchemeMasterModelTwo> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SchemeMasterModelTwo> schemeTwoList = new ArrayList<>();
				while (rs.next()) {
					schemeTwoList.add(new SchemeMasterModelTwo(rs, true));
				}
				return schemeTwoList;
			}
		};
	}

	private ResultSetExtractor<List<SchemeMasterModelOne>> getItemDiscountResultExctractor() {
		return new ResultSetExtractor<List<SchemeMasterModelOne>>() {
			@Override
			public List<SchemeMasterModelOne> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SchemeMasterModelOne> schemeModelList = new ArrayList<>();
				SchemeMasterModelOne schemeMasterModelOne;
				while (rs.next()) {
					schemeMasterModelOne = new SchemeMasterModelOne();
					schemeMasterModelOne
							.setTotalInvoiceMrpTo(rs.getInt(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString()));
					schemeMasterModelOne
							.setTotalInvoiceMrpFrom(rs.getInt(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString()));
					schemeMasterModelOne
							.setTotalMrpOnDiscount(rs.getInt(SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT.toString()));
					schemeModelList.add(schemeMasterModelOne);
				}
				return schemeModelList;
			}
		};
	}

	List<SchemeMasterModelOne> getSchemeOneByMrp(int companyId, String mrp) {
		float mrpflt = 0.0f;
		if (mrp != null && !mrp.isEmpty()) {
			try {
				mrpflt = Float.parseFloat(mrp);
			} catch (Exception e) {
				System.out.println(e);
				mrpflt = 0.0f;
			}
		}
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("SELECT %s, %s,%s,%s,%s ", SCHEME_ONE_COLUMN.SCHEME_ONE_ID,
				SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO,
				SCHEME_ONE_COLUMN.SCHEME_MRP_ON_DISCOUNT, SCHEME_ONE_COLUMN.SCHEME_COMPANYID))
				.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS))
				.append(String.format(" where isDeleted = 0 and %s=:%s and %s <= :%s and %s >= :%s limit 1",
						SCHEME_ONE_COLUMN.SCHEME_COMPANYID, SCHEME_ONE_COLUMN.SCHEME_COMPANYID,
						SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM,
						SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO, SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO));
		System.out.println(sqlQuery);

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(SCHEME_ONE_COLUMN.SCHEME_COMPANYID.toString(), companyId);
		parameters.put(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_FROM.toString(), mrpflt);
		parameters.put(SCHEME_ONE_COLUMN.SCHEME_TOTAL_MRP_TO.toString(), mrpflt);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getSchemeOneResultSetExctractor());
	}

	public int deleteSchemeOne(int locationId, int companyId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_SCHEME_ONE_DETAILS))
				.append(String.format(" %s = :%s, ", SCHEME_ONE_COLUMN.IS_DELETED,
						SCHEME_ONE_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", SCHEME_ONE_COLUMN.DELETED_BY,
						SCHEME_ONE_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", SCHEME_ONE_COLUMN.DELETED_DATE_TIME,
						SCHEME_ONE_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", SCHEME_ONE_COLUMN.SCHEME_ONE_ID,
						SCHEME_ONE_COLUMN.SCHEME_ONE_ID))
				.append(String.format(" AND %s = :%s  ", SCHEME_ONE_COLUMN.SCHEME_COMPANYID,
						SCHEME_ONE_COLUMN.SCHEME_COMPANYID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SCHEME_ONE_COLUMN.SCHEME_ONE_ID.toString(), locationId)
				.addValue(SCHEME_ONE_COLUMN.IS_DELETED.toString(), 1)
				.addValue(SCHEME_ONE_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(SCHEME_ONE_COLUMN.SCHEME_COMPANYID.toString(), companyId)
				.addValue(SCHEME_ONE_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
	
	
	

}
