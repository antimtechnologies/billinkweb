package acc.webservice.components.payment.sellpayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class SellPaymentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@SuppressWarnings("unchecked")
	List<SellPaymentModel> getSellPaymentListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();
		sqlQueryToFetchData.append(String.format(" SELECT SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL, SELL_PAYMENT_DETAILS_COLUMN.AMOUNT))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION, SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" LM.%s AS %s, LM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" SLM.%s AS %s, SLM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, "receiptInLedgerId", LEDGER_DETAILS_COLUMN.LEDGER_NAME, "receiptInLedgerName"))
						.append(String.format(" SM.%s, SM.%s ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME))
						.append(String.format(" FROM %s SPD ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON SPD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s LM ON SPD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" LEFT JOIN %s SLM ON SPD.%s = SLM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s AUM ON SPD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON SPD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON SPD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON SPD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE SPD.%s = 0 ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND SPD.%s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {					
					sqlQueryToFetchData.append(String.format(" AND SPD.%s IN (:statusList) ", SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString());
				
				if (isVerified == 0 || isVerified == 1) {					
					sqlQueryToFetchData.append(String.format(" AND SPD.%s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString());
				
				if (ledgerId > 0)  {					
					sqlQueryToFetchData.append(String.format(" AND SPD.%s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString())) {
				String expenseId = ApplicationUtility.getStrValue(filterParamter, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND SPD.%s LIKE :%s ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID));
				parameters.put(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), "%" + expenseId + "%");
			}

		}

		sqlQueryToFetchData.append(String.format(" ORDER BY SPD.%s DESC LIMIT :start, :noOfRecord ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID));
		parameters.put(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getSellDetailsListExtractor());
	}

	private ResultSetExtractor<List<SellPaymentModel>> getSellDetailsListExtractor() {

		return new ResultSetExtractor<List<SellPaymentModel>>() {
			@Override
			public List<SellPaymentModel> extractData(ResultSet rs) throws SQLException {
				List<SellPaymentModel> sellList = new ArrayList<>();
				while (rs.next()) {
					sellList.add(new SellPaymentModel(rs));
				}
				return sellList;
			}
		};
	}

	SellPaymentModel getSellPaymentDetailsById(int companyId, int sellId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL, SELL_PAYMENT_DETAILS_COLUMN.AMOUNT))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION, SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME))
						.append(String.format(" SPD.%s, SPD.%s, SPD.%s, ", SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" LM.%s AS %s, LM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" SLM.%s AS %s, SLM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, "receiptInLedgerId", LEDGER_DETAILS_COLUMN.LEDGER_NAME, "receiptInLedgerName"))
						.append(String.format(" SM.%s, SM.%s, SPM.%s, SPM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME, SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_MAPPING_ID, SELL_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE))
						.append(String.format(" sellMaster.%s, sellMaster.%s, sellMaster.%s, ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.ADDED_DATE_TIME))
						.append(String.format(" sellMaster.%s AS sellImageURL, sellMaster.%s, sellMaster.%s ", SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.SELL_BILL_DATE, SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT))
						.append(String.format(" FROM %s SPD ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON SPD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s LM ON SPD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" LEFT JOIN %s SLM ON SPD.%s = SLM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" INNER JOIN %s AUM ON SPD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON SPD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON SPD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON SPD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s SPM ON SPM.%s = SPD.%s AND SPM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_MAPPING, SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_MAPPING_COLUMN.IS_DELETED))
						.append(String.format(" AND SPM.%s = :%s ", SELL_PAYMENT_MAPPING_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))						
						.append(String.format(" LEFT JOIN %s sellMaster ON sellMaster.%s = SPM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS, SELL_DETAILS_COLUMN.SELL_ID, SELL_PAYMENT_MAPPING_COLUMN.SELL_ID))
						.append(String.format(" AND sellMaster.%s = :%s ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))						
						.append(String.format(" WHERE SPD.%s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
						.append(String.format(" AND SPD.%s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellId);
		parameters.put(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getSellPaymentDetailExctractor());
	}

	private ResultSetExtractor<SellPaymentModel> getSellPaymentDetailExctractor() {

		return new ResultSetExtractor<SellPaymentModel>() {
			@Override
			public SellPaymentModel extractData(ResultSet rs) throws SQLException {
				SellPaymentModel sellPaymentDetails = new SellPaymentModel();
				List<SellPaymentMappingModel> sellList = new ArrayList<>();
				while (rs.next()) {
					if (sellPaymentDetails.getSellPaymentId() < 1) {						
						sellPaymentDetails = new SellPaymentModel(rs);
					}
					
					if (rs.getInt(SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_MAPPING_ID.toString()) > 0) 
						sellList.add(new SellPaymentMappingModel(rs));
				}
				sellPaymentDetails.setSellList(sellList);
				return sellPaymentDetails;
			}
		};
	}

	SellPaymentModel publishSellPaymentData(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = getSellPaymentInsertQuery(sellPaymentModel);

		if (!ApplicationUtility.isNullEmpty(sellPaymentModel.getImageURL())) {			
			sellPaymentModel.setImageURL(sellPaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL.toString(), sellPaymentModel.getImageURL())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.AMOUNT.toString(), sellPaymentModel.getAmount())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellPaymentModel.getReferenceNumber1())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellPaymentModel.getReferenceNumber2())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION.toString(), sellPaymentModel.getDescription())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString(), sellPaymentModel.getLedgerData().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER.toString(), sellPaymentModel.getReceiptInLedger() == null || sellPaymentModel.getReceiptInLedger().getLedgerId() < 1 ? null : sellPaymentModel.getReceiptInLedger().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE.toString(), sellPaymentModel.getPaymentDate())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY.toString(), sellPaymentModel.getAddedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		sellPaymentModel.setSellPaymentId(holder.getKey().intValue());
		return sellPaymentModel;
	}

	SellPaymentModel saveSellPaymentData(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = getSellPaymentInsertQuery(sellPaymentModel);

		if (!ApplicationUtility.isNullEmpty(sellPaymentModel.getImageURL())) {			
			sellPaymentModel.setImageURL(sellPaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL.toString(), sellPaymentModel.getImageURL())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.AMOUNT.toString(), sellPaymentModel.getAmount())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION.toString(), sellPaymentModel.getDescription())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellPaymentModel.getReferenceNumber1())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellPaymentModel.getReferenceNumber2())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString(), sellPaymentModel.getLedgerData().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER.toString(), sellPaymentModel.getReceiptInLedger() == null || sellPaymentModel.getReceiptInLedger().getLedgerId() < 1 ? null : sellPaymentModel.getReceiptInLedger().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE.toString(), sellPaymentModel.getPaymentDate())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY.toString(), sellPaymentModel.getAddedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		sellPaymentModel.setSellPaymentId(holder.getKey().intValue());
		return sellPaymentModel;
	}

	private StringBuilder getSellPaymentInsertQuery(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format("( %s, %s, %s, %s, ", SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL, SELL_PAYMENT_DETAILS_COLUMN.AMOUNT, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s, %s, ", SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE))
							.append(String.format(" %s, %s, %s, ", SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" %s, %s, %s )", SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY, SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL, SELL_PAYMENT_DETAILS_COLUMN.AMOUNT, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s, :%s, ", SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE))
							.append(String.format(" :%s, :%s, :%s, ", SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" :%s, :%s, :%s )", SELL_PAYMENT_DETAILS_COLUMN.ADDED_BY, SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED));
		return sqlQuery;
	}

	SellPaymentModel updateSavedSellPaymentData(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = getSellPaymentUpdateQuery(sellPaymentModel);
		

		if (!ApplicationUtility.isNullEmpty(sellPaymentModel.getImageURL())) {			
			sellPaymentModel.setImageURL(sellPaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL.toString(), sellPaymentModel.getImageURL())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.AMOUNT.toString(), sellPaymentModel.getAmount())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION.toString(), sellPaymentModel.getDescription())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellPaymentModel.getReferenceNumber1())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellPaymentModel.getReferenceNumber2())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString(), sellPaymentModel.getLedgerData().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER.toString(), sellPaymentModel.getReceiptInLedger() == null || sellPaymentModel.getReceiptInLedger().getLedgerId() < 1 ? null : sellPaymentModel.getReceiptInLedger().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE.toString(), sellPaymentModel.getPaymentDate())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY.toString(), sellPaymentModel.getModifiedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentModel.getSellPaymentId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return sellPaymentModel;
	}
	
	SellPaymentModel updatePublishedSellPaymentData(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = getSellPaymentUpdateQuery(sellPaymentModel);

		if (!ApplicationUtility.isNullEmpty(sellPaymentModel.getImageURL())) {			
			sellPaymentModel.setImageURL(sellPaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL.toString(), sellPaymentModel.getImageURL())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.AMOUNT.toString(), sellPaymentModel.getAmount())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION.toString(), sellPaymentModel.getDescription())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellPaymentModel.getReferenceNumber1())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellPaymentModel.getReferenceNumber2())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID.toString(), sellPaymentModel.getLedgerData().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER.toString(), sellPaymentModel.getReceiptInLedger() == null || sellPaymentModel.getReceiptInLedger().getLedgerId() < 1 ? null : sellPaymentModel.getReceiptInLedger().getLedgerId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE.toString(), sellPaymentModel.getPaymentDate())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY.toString(), sellPaymentModel.getModifiedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentModel.getSellPaymentId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return sellPaymentModel;
	}

	private StringBuilder getSellPaymentUpdateQuery(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL, SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.AMOUNT, SELL_PAYMENT_DETAILS_COLUMN.AMOUNT))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION, SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID, SELL_PAYMENT_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER, SELL_PAYMENT_DETAILS_COLUMN.RECEIPT_IN_LEDGER))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE, SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_BY))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED));
		return sqlQuery;
	}


	int markSellPaymentAsVerified(SellPaymentModel sellPaymentModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY.toString(), sellPaymentModel.getVerifiedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentModel.getSellPaymentId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	int deletePublishedSellPaymentData(int companyId, int sellPaymentId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY, SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentId)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deleteSavedSellPaymentData(int companyId, int sellPaymentId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY, SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 1)
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentId)
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
			.addValue(SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int markSellPaymentAsDeleteAndVerified(SellPaymentModel sellPaymentModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s, ", SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_BY.toString(), sellPaymentModel.getVerifiedBy().getUserId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentModel.getSellPaymentId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	SellPaymentModel publishSavedSellPayment(SellPaymentModel sellPaymentModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" WHERE %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentModel.getSellPaymentId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), sellPaymentModel.getCompanyId())
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		sellPaymentModel.setStatus(status);
		return sellPaymentModel;
	}

	int getPendingSellPaymentCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ", SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_DETAILS))
				.append(String.format("WHERE %s != :%s AND %s = 0 ", SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID, SELL_PAYMENT_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID, SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s", SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(SELL_PAYMENT_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

}
