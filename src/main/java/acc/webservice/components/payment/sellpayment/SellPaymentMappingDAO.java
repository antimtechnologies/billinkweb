package acc.webservice.components.payment.sellpayment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.PAYMENT_STATUS_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_MAPPING_COLUMN;

@Repository
@Lazy
public class SellPaymentMappingDAO {
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	/**
	 * @param sellPaymentMappingList
	 * @param companyId
	 * @param sellPaymentId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	List<Map<String, Object>> saveSellPaymentMappingData(List<SellPaymentMappingModel> sellPaymentMappingList, int companyId, int sellPaymentId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_MAPPING))
				.append(String.format(" ( %s, %s, %s, %s ) ", SELL_PAYMENT_MAPPING_COLUMN.SELL_ID, SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_MAPPING_COLUMN.COMPANY_ID, SELL_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", SELL_PAYMENT_MAPPING_COLUMN.SELL_ID, SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_MAPPING_COLUMN.COMPANY_ID, SELL_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE));

		List<Map<String, Object>> sellPaymentList = new ArrayList<>();
		List<Map<String, Object>> sellPaymentStatusList = new ArrayList<>();
		Map<String, Object>  sellPaymentStatusMap;
		Map<String, Object> sellPaymentMap;
		for (SellPaymentMappingModel sellPaymentMapping : sellPaymentMappingList) {
			sellPaymentMap = new HashMap<>();
			sellPaymentMap.put(SELL_PAYMENT_MAPPING_COLUMN.SELL_ID.toString(), sellPaymentMapping.getSell().getSellId());
			sellPaymentMap.put(SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentId);
			sellPaymentMap.put(SELL_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE.toString(), sellPaymentMapping.isPaymentDone());
			sellPaymentMap.put(SELL_PAYMENT_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);

			sellPaymentList.add(sellPaymentMap);

			sellPaymentStatusMap = new HashMap<>();
			sellPaymentStatusMap.put(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellPaymentMapping.getSell().getSellId());
			sellPaymentStatusMap.put(SELL_DETAILS_COLUMN.PAYMENT_STATUS.toString(), sellPaymentMapping.isPaymentDone() ? PAYMENT_STATUS_ENUM.COMPLETE_PAYMENT_STATUS.status() : PAYMENT_STATUS_ENUM.PARTIAL_PAYMENT_STATUS.status());
			sellPaymentStatusMap.put(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
			sellPaymentStatusList.add(sellPaymentStatusMap);
		}

		namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellPaymentList.toArray(new HashMap[0]));
		return sellPaymentStatusList;
	}

	int deleteSellPaymentMapping(int companyId, int sellPaymentId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_MAPPING, SELL_PAYMENT_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_ID))
				.append(String.format(" AND %s = ? ", SELL_PAYMENT_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, sellPaymentId, companyId });
	}
}
