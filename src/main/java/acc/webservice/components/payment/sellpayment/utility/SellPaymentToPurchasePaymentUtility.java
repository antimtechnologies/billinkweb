package acc.webservice.components.payment.sellpayment.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentImageModel;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentMappingModel;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentService;
import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.payment.sellpayment.SellPaymentImageModel;
import acc.webservice.components.payment.sellpayment.SellPaymentMappingModel;
import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.sell.SellService;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.sell.utility.SellToPurchaseUtility;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class SellPaymentToPurchasePaymentUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private PurchasePaymentService purchasePaymentService;

	@Autowired
	private SellService sellService;

	@Autowired
	private SellToPurchaseUtility sellToPurchaseUtility;

	public PurchasePaymentModel saveSellPaymentDataForLedger(SellPaymentModel sellPaymentModel) throws AccountingSofwareException {
		PurchasePaymentModel purchasePaymentModel = new PurchasePaymentModel();

		LedgerModel ledgerBasicDetails = ledgerService.getBasicLedgerDetailByLedgerId(
				sellPaymentModel.getLedgerData().getLedgerId(), sellPaymentModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());
			CompanyModel purchasePaymentCompanyModel = companyService.getCompanyDetails(selectedLedgerCompanyId);
			
			if (purchasePaymentCompanyModel.isAllowB2BImport()) {				
				if (selectedLedgerCompanyId > 0) {
					purchasePaymentModel = createPurchasePaymentModelFromSellPaymentModel(sellPaymentModel, selectedLedgerCompanyId);
					purchasePaymentService.savePurchasePaymentData(purchasePaymentModel);
				}
			}
		}
		return purchasePaymentModel;
	}

	/**
	 * @param sellPaymentModel
	 * @param selectedLedgerCompanyId
	 * @return
	 * @throws AccountingSofwareException 
	 */
	private PurchasePaymentModel createPurchasePaymentModelFromSellPaymentModel(SellPaymentModel sellPaymentModel, int selectedLedgerCompanyId) throws AccountingSofwareException {
		PurchasePaymentModel purchasePaymentModel = new PurchasePaymentModel();
		purchasePaymentModel.setCompanyId(selectedLedgerCompanyId);
		purchasePaymentModel.setAmount(sellPaymentModel.getAmount());
		purchasePaymentModel.setImageURL(sellPaymentModel.getImageURL());
		purchasePaymentModel.setDescription(sellPaymentModel.getDescription());
		purchasePaymentModel.setPaymentDate(sellPaymentModel.getPaymentDate());
		CompanyModel sellCompanyDetail = companyService.getCompanyDetails(sellPaymentModel.getCompanyId());
		LedgerModel sellLedgerData = new LedgerModel();
		int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompanyId,
				sellCompanyDetail.getGstNumber());
		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompanyId, sellCompanyDetail);
		}
		sellLedgerData.setLedgerId(purchaseLedgerId);
		purchasePaymentModel.setLedgerData(sellLedgerData);

		List<PurchasePaymentImageModel> purchasePaymentImagList = new ArrayList<>();
		PurchasePaymentImageModel purchasePaymentImageModel;
		
		if (ApplicationUtility.getSize(sellPaymentModel.getImageURLList()) > 0) {			
			for (SellPaymentImageModel sellPaymentImageModel : sellPaymentModel.getImageURLList()) {
				purchasePaymentImageModel = new PurchasePaymentImageModel();
				purchasePaymentImageModel.setImageURL(sellPaymentImageModel.getImageURL());
				purchasePaymentImagList.add(purchasePaymentImageModel);
			}
		}

		purchasePaymentModel.setImageURLList(purchasePaymentImagList);

		List<SellPaymentMappingModel> sellPaymentMappingList = sellPaymentModel.getSellList();
		List<PurchasePaymentMappingModel> purchasePaymentMappingModelList = new ArrayList<>();
		PurchasePaymentMappingModel purchasePaymentMappingModel;
		PurchaseModel purchaseModel;
		SellModel sellModel;
		
		if (ApplicationUtility.getSize(sellPaymentMappingList) > 0) {			
			for (SellPaymentMappingModel sellPaymentMappingModel : sellPaymentMappingList) {
				purchasePaymentMappingModel = new PurchasePaymentMappingModel();
				sellModel = sellService.getSellDetailsById(sellPaymentModel.getCompanyId(), sellPaymentMappingModel.getSell().getSellId());

				if (sellModel.getSellDate() != null) {
					sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
				}

				sellModel.setCompanyId(sellPaymentModel.getCompanyId());
				purchaseModel = sellToPurchaseUtility.savePurchaseForSelectedLedger(sellModel);

				if (purchaseModel.getPurchaseId() > 0) {
					purchasePaymentMappingModel.setPurchase(purchaseModel);
					purchasePaymentMappingModel.setPaymentDone(sellPaymentMappingModel.isPaymentDone());
					purchasePaymentMappingModelList.add(purchasePaymentMappingModel);
				}
			}
		}

		purchasePaymentModel.setPurchaseList(purchasePaymentMappingModelList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		purchasePaymentModel.setAddedBy(userData);

		return purchasePaymentModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail) throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
