package acc.webservice.components.payment.sellpayment;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.global.utils.DateUtility;
import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.payment.sellpayment.utility.SellPaymentToPurchasePaymentUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.components.users.UserService;

@RestController
@RequestMapping("/sellPaymentData/")
public class SellPaymentController {

	@Autowired
	private SellPaymentService sellPaymentService;

	@Autowired
	private SellPaymentToPurchasePaymentUtility sellPaymentToPurchasePaymentUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getSellPaymentListData", method = RequestMethod.POST)
	public Map<String, Object> getSellPaymentListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				sellPaymentService.getSellPaymentListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getSellPaymentDetailsById", method = RequestMethod.POST)
	public Map<String, Object> getSellPaymentDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellPaymentId = ApplicationUtility.getIntValue(parameters,
				SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.getSellPaymentDetailsById(companyId, sellPaymentId));
		return responseData;
	}

	@RequestMapping(value = "publishSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> publishSellPaymentData(@RequestBody SellPaymentModel sellPaymentModel)
			throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.publishSellPaymentData(sellPaymentModel));
		try {
			sellPaymentToPurchasePaymentUtility.saveSellPaymentDataForLedger(sellPaymentModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "saveSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> saveSellPaymentData(@RequestBody SellPaymentModel sellPaymentModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.saveSellPaymentData(sellPaymentModel));
		return responseData;
	}

	@RequestMapping(value = "updateSavedSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedSellPaymentData(@RequestBody SellPaymentModel sellPaymentModel) {
		Map<String, Object> responseData = new HashMap<>();
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, sellPaymentModel.getPaymentDate())) {
					sellPaymentModel.setPaymentDate(DateUtility.convertDateFormat(sellPaymentModel.getPaymentDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.updateSavedSellPaymentData(sellPaymentModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedSellPaymentData(@RequestBody SellPaymentModel sellPaymentModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, sellPaymentModel.getPaymentDate())) {
			sellPaymentModel.setPaymentDate(DateUtility.convertDateFormat(sellPaymentModel.getPaymentDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.updatePublishedSellPaymentData(sellPaymentModel));
		return responseData;
	}

	@RequestMapping(value = "markSellPaymentAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markSellPaymentAsVerified(@RequestBody SellPaymentModel sellPaymentModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.markSellPaymentAsVerified(sellPaymentModel));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishedSellPaymentData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellPaymentId = ApplicationUtility.getIntValue(parameters,
				SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				sellPaymentService.deletePublishedSellPaymentData(companyId, sellPaymentId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedSellPaymentData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedSellPaymentData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellPaymentId = ApplicationUtility.getIntValue(parameters,
				SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.deleteSavedSellPaymentData(companyId, sellPaymentId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "publishSavedSellPayment", method = RequestMethod.POST)
	public Map<String, Object> publishSavedPurchasePayment(@RequestBody SellPaymentModel sellPaymentModel)
			throws AccountingSofwareException {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, sellPaymentModel.getPaymentDate())) {
				sellPaymentModel.setPaymentDate(DateUtility.convertDateFormat(sellPaymentModel.getPaymentDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellPaymentService.publishSavedSellPayment(sellPaymentModel));
		
		try {
				if (sellPaymentModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
					sellPaymentToPurchasePaymentUtility.saveSellPaymentDataForLedger(sellPaymentModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "getallsalesPaymentByDay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSalesPaymentDetailByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("ispaymentdone") != null) {
			if (request.getParameter("day") != null) {
				int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = "http://localhost:8090/user/salespaymentmapping/salespaymentbyday?ispaymentdone="
						+ ispaymentdone + "&day=" + day;
			}

		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "getallsalesPaymentByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSalesPaymentDetailByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("ispaymentdone") != null) {
			if (request.getParameter("month") != null) {
				int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = "http://localhost:8090/user/salespaymentmapping/salespaymentbymonth?ispaymentdone="
						+ ispaymentdone + "&month=" + month;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "sellPaymentByCompanyAndDay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSellPaymentDetailByCompanyAndDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("isPaymentdone") != null) {
				if (request.getParameter("day") != null) {
					int companyId = Integer.parseInt(request.getParameter("companyId"));
					int ispaymentdone = Integer.parseInt(request.getParameter("isPaymentdone"));
					int day = Integer.parseInt(request.getParameter("day"));
					fooResourceUrl = "http://localhost:8090/user/salespaymentmapping/findallsalespaymentbycompanyandday?companyId="
							+ companyId + "&isPaymentdone=" + ispaymentdone + "&day=" + day;
				}

			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "sellPaymentByCompanyAndMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSellPaymentDetailByCompanyAndMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("isPaymentdone") != null) {
				if (request.getParameter("month") != null) {
					int companyId = Integer.parseInt(request.getParameter("companyId"));
					int ispaymentdone = Integer.parseInt(request.getParameter("isPaymentdone"));
					int month = Integer.parseInt(request.getParameter("month"));
					fooResourceUrl = "http://localhost:8090/user/salespaymentmapping/findallsalespaymentbycompanyandmonth?companyId="
							+ companyId + "&isPaymentdone=" + ispaymentdone + "&month=" + month;
				}

			}
		}

		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

}
