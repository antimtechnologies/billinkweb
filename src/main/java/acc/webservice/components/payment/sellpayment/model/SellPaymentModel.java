package acc.webservice.components.payment.sellpayment.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.payment.sellpayment.SellPaymentImageModel;
import acc.webservice.components.payment.sellpayment.SellPaymentMappingModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class SellPaymentModel {

	private int sellPaymentId;
	private LedgerModel ledgerData;
	private LedgerModel receiptInLedger;
	private String imageURL;
	private double amount;
	private String description;
	private StatusModel status;
	private String paymentDate;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel verifiedBy;
	private String verifiedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private boolean isDeleted;
	private boolean isVerified;
	private int companyId;
	private List<SellPaymentMappingModel> sellList;
	private List<SellPaymentImageModel> imageURLList;
	private String referenceNumber1;
	private String referenceNumber2;

	public SellPaymentModel() {
	}

	public SellPaymentModel(ResultSet rs) throws SQLException {
		setSellPaymentId(rs.getInt(SELL_PAYMENT_DETAILS_COLUMN.SELL_PAYMENT_ID.toString()));

		LedgerModel fetchedLedger = new LedgerModel();
		fetchedLedger.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedger.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		setLedgerData(fetchedLedger);
		
		LedgerModel fetchedReceiptInLedger = new LedgerModel();
		fetchedReceiptInLedger.setLedgerId(rs.getInt("receiptInLedgerId"));
		fetchedReceiptInLedger.setLedgerName(rs.getString("receiptInLedgerName"));
		setReceiptInLedger(fetchedReceiptInLedger);
		
		setReferenceNumber1(rs.getString(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(SELL_PAYMENT_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));

		String imageURL = rs.getString(SELL_PAYMENT_DETAILS_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}

		setAmount(rs.getDouble(SELL_PAYMENT_DETAILS_COLUMN.AMOUNT.toString()));
		setDescription(rs.getString(SELL_PAYMENT_DETAILS_COLUMN.DESCRIPTION.toString()));
		setVerified(rs.getBoolean(SELL_PAYMENT_DETAILS_COLUMN.IS_VERIFIED.toString()));

		Timestamp fetchedPaymentDate = rs.getTimestamp(SELL_PAYMENT_DETAILS_COLUMN.PAYMENT_DATE.toString());
		if (fetchedPaymentDate != null) {
			setPaymentDate(DateUtility.converDateToUserString(fetchedPaymentDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(SELL_PAYMENT_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(SELL_PAYMENT_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerifiedDate = rs.getTimestamp(SELL_PAYMENT_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerifiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerifiedDate));
		}

		Timestamp fetchedDeleteDate = rs.getTimestamp(SELL_PAYMENT_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeleteDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeleteDate));
		}
		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		setStatus(new StatusModel(rs));
	}

	public int getSellPaymentId() {
		return sellPaymentId;
	}

	public void setSellPaymentId(int sellPaymentId) {
		this.sellPaymentId = sellPaymentId;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<SellPaymentMappingModel> getSellList() {
		return sellList;
	}

	public void setSellList(List<SellPaymentMappingModel> sellList) {
		this.sellList = sellList;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public List<SellPaymentImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<SellPaymentImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public LedgerModel getReceiptInLedger() {
		return receiptInLedger;
	}

	public void setReceiptInLedger(LedgerModel receiptInLedger) {
		this.receiptInLedger = receiptInLedger;
	}

	@Override
	public String toString() {
		return "PurchasePaymentModel [purchasePaymentId=" + sellPaymentId + ", imageURL=" + imageURL + ", amount="
				+ amount + ", description=" + description + ", status=" + status + ", paymentDate=" + paymentDate
				+ ", addedDateTime=" + addedDateTime + ", modifiedDateTime=" + modifiedDateTime + ", verifiedDateTime="
				+ verifiedDateTime + ", deletedDateTime=" + deletedDateTime + ", isDeleted=" + isDeleted + "]";
	}

}
