package acc.webservice.components.payment.sellpayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class SellPaymentImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<SellPaymentImageModel> getSellPaymentImageList(int companyId, int sellPaymentIsd) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentIsd);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCreditNoteImageResultExctractor());
	}

	private ResultSetExtractor<List<SellPaymentImageModel>> getCreditNoteImageResultExctractor() {
		return new ResultSetExtractor<List<SellPaymentImageModel>>() {
			@Override
			public List<SellPaymentImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SellPaymentImageModel> sellPaymentImageModelList = new ArrayList<>();
				while (rs.next()) {
					sellPaymentImageModelList.add(new SellPaymentImageModel(rs));
				}
				return sellPaymentImageModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteImageURLData(List<SellPaymentImageModel> sellPaymentImageModelList, int companyId, int sellPaymentIsd) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s) ", SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> sellPaymentImageURLMapList = new ArrayList<>();
		Map<String, Object> sellPaymentImageURLMap;
		for (SellPaymentImageModel sellPaymentImageModel : sellPaymentImageModelList) {
			sellPaymentImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(sellPaymentImageModel.getImageURL())) {			
				sellPaymentImageModel.setImageURL(sellPaymentImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			sellPaymentImageURLMap.put(SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL.toString(), sellPaymentImageModel.getImageURL());
			sellPaymentImageURLMap.put(SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID.toString(), sellPaymentIsd);
			sellPaymentImageURLMap.put(SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			sellPaymentImageURLMapList.add(sellPaymentImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellPaymentImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteImageURLData(int companyId, int sellPaymentIsd) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_PAYMENT_IMAGE_URL, SELL_PAYMENT_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.SELL_PAYMENT_ID ))
				.append(String.format(" AND %s = ? ", SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, SELL_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, sellPaymentIsd, companyId });
	}

}
