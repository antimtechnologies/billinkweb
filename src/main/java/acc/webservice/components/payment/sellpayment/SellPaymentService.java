package acc.webservice.components.payment.sellpayment;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.sell.SellService;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class SellPaymentService {
	
	@Autowired
	private SellPaymentDAO sellPaymentDAO;

	@Autowired
	private SellPaymentMappingDAO sellPaymentMappingDAO;

	@Autowired
	private SellPaymentImageDAO sellPaymentImageDAO;
	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private SellService sellService;

	public List<SellPaymentModel> getSellPaymentListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		return sellPaymentDAO.getSellPaymentListData(companyId, start, numberOfRecord, filterParamter);
	}

	public SellPaymentModel getSellPaymentDetailsById(int companyId, int sellPaymentId) {
		SellPaymentModel sellPaymentModel = sellPaymentDAO.getSellPaymentDetailsById(companyId, sellPaymentId);
		sellPaymentModel.setImageURLList(sellPaymentImageDAO.getSellPaymentImageList(companyId, sellPaymentId));
		return sellPaymentModel;
	}

	public SellPaymentModel saveSellPaymentData(SellPaymentModel sellPaymentModel) {
		validateSellPaymentManupulateData(sellPaymentModel);
		sellPaymentDAO.saveSellPaymentData(sellPaymentModel);
		List<Map<String, Object>> sellPaymentStatusMap = sellPaymentMappingDAO.saveSellPaymentMappingData(sellPaymentModel.getSellList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		sellService.updateSellPaymentStatus(sellPaymentStatusMap, sellPaymentModel.getCompanyId());
		
		if (ApplicationUtility.getSize(sellPaymentModel.getImageURLList()) > 0) {
			sellPaymentImageDAO.saveCreditNoteImageURLData(sellPaymentModel.getImageURLList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		}
		return sellPaymentModel;
	}

	public SellPaymentModel publishSellPaymentData(SellPaymentModel sellPaymentModel) {
		validateSellPaymentManupulateData(sellPaymentModel);
		sellPaymentDAO.publishSellPaymentData(sellPaymentModel);
		if(sellPaymentModel.getSellList() != null && sellPaymentModel.getSellList().size() >0) {
			List<Map<String, Object>> sellPaymentStatusMap = sellPaymentMappingDAO.saveSellPaymentMappingData(sellPaymentModel.getSellList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
			sellService.updateSellPaymentStatus(sellPaymentStatusMap, sellPaymentModel.getCompanyId());
			
		}
		if (ApplicationUtility.getSize(sellPaymentModel.getImageURLList()) > 0) {
			sellPaymentImageDAO.saveCreditNoteImageURLData(sellPaymentModel.getImageURLList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		}
		companyService.updateCompanySellPaymentCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 2), sellPaymentModel.getCompanyId());
		companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 0), sellPaymentModel.getCompanyId());
		return sellPaymentModel;
	}

	public SellPaymentModel updatePublishedSellPaymentData(SellPaymentModel sellPaymentModel) {
		validateSellPaymentManupulateData(sellPaymentModel);
		sellPaymentMappingDAO.deleteSellPaymentMapping(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		List<Map<String, Object>> sellPaymentStatusMap = sellPaymentMappingDAO.saveSellPaymentMappingData(sellPaymentModel.getSellList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		sellService.updateSellPaymentStatus(sellPaymentStatusMap, sellPaymentModel.getCompanyId());
		sellPaymentDAO.updatePublishedSellPaymentData(sellPaymentModel);

		if (ApplicationUtility.getSize(sellPaymentModel.getImageURLList()) > 0) {
			sellPaymentImageDAO.deleteCreditNoteImageURLData(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
			sellPaymentImageDAO.saveCreditNoteImageURLData(sellPaymentModel.getImageURLList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		}
		companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 0), sellPaymentModel.getCompanyId());
		return sellPaymentModel;
	}

	public SellPaymentModel updateSavedSellPaymentData(SellPaymentModel sellPaymentModel) {
		validateSellPaymentManupulateData(sellPaymentModel);
		sellPaymentMappingDAO.deleteSellPaymentMapping(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		List<Map<String, Object>> sellPaymentStatusMap = sellPaymentMappingDAO.saveSellPaymentMappingData(sellPaymentModel.getSellList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		sellService.updateSellPaymentStatus(sellPaymentStatusMap, sellPaymentModel.getCompanyId());

		if (ApplicationUtility.getSize(sellPaymentModel.getImageURLList()) > 0) {
			sellPaymentImageDAO.deleteCreditNoteImageURLData(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
			sellPaymentImageDAO.saveCreditNoteImageURLData(sellPaymentModel.getImageURLList(), sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
		}
		return sellPaymentDAO.updateSavedSellPaymentData(sellPaymentModel);
	}
	
	public int deletePublishedSellPaymentData(int companyId, int sellPaymentId, int deletedBy) {
		sellPaymentDAO.deletePublishedSellPaymentData(companyId, sellPaymentId, deletedBy);
		return companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(companyId, 0), companyId);
	}
	
	public int deleteSavedSellPaymentData(int companyId, int sellPaymentId, int deletedBy) {
		sellPaymentImageDAO.deleteCreditNoteImageURLData(companyId, sellPaymentId);
		sellPaymentDAO.deleteSavedSellPaymentData(companyId, sellPaymentId, deletedBy);
		return companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(companyId, 0), companyId);
	}

	public SellPaymentModel markSellPaymentAsVerified(SellPaymentModel sellPaymentModel) {
		if (sellPaymentModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			sellPaymentImageDAO.deleteCreditNoteImageURLData(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
			sellPaymentMappingDAO.deleteSellPaymentMapping(sellPaymentModel.getCompanyId(), sellPaymentModel.getSellPaymentId());
			sellPaymentDAO.markSellPaymentAsDeleteAndVerified(sellPaymentModel);
		} else {			
			sellPaymentDAO.markSellPaymentAsVerified(sellPaymentModel);
		}
		companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 0), sellPaymentModel.getCompanyId());
		return sellPaymentModel;
	}

	public SellPaymentModel publishSavedSellPayment(SellPaymentModel sellPaymentModel) {
		updateSavedSellPaymentData(sellPaymentModel);
		sellPaymentDAO.publishSavedSellPayment(sellPaymentModel);
		companyService.updateCompanySellPaymentCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 2), sellPaymentModel.getCompanyId());
		companyService.updateCompanySellPaymentPendingCount(sellPaymentDAO.getPendingSellPaymentCountData(sellPaymentModel.getCompanyId(), 0), sellPaymentModel.getCompanyId());
		return sellPaymentModel;
	}

	private void validateSellPaymentManupulateData(SellPaymentModel sellPaymentModel) {

		if (sellPaymentModel.getLedgerData() == null || sellPaymentModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save sell payment data.");
		}
	}
}
