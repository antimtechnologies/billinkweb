package acc.webservice.components.payment.sellpayment;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class SellPaymentImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private SellPaymentModel sellPaymentModel;

	public SellPaymentImageModel() {
	}

	public SellPaymentImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(SELL_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public SellPaymentModel getSellPaymentModel() {
		return sellPaymentModel;
	}

	public void setSellPaymentModel(SellPaymentModel sellPaymentModel) {
		this.sellPaymentModel = sellPaymentModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
