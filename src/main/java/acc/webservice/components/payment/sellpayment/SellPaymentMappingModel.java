package acc.webservice.components.payment.sellpayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_PAYMENT_MAPPING_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class SellPaymentMappingModel {

	private int sellPaymentMappingId;
	private SellModel sell;
	private SellPaymentModel payment;
	private boolean paymentDone;
	private boolean isDeleted;

	public SellPaymentMappingModel() {
	}

	public SellPaymentMappingModel(ResultSet rs) throws SQLException {
		setSellPaymentMappingId(rs.getInt(SELL_PAYMENT_MAPPING_COLUMN.SELL_PAYMENT_MAPPING_ID.toString()));
		SellModel fetchedSell = new SellModel();
		fetchedSell.setSellId(rs.getInt(SELL_DETAILS_COLUMN.SELL_ID.toString()));
		fetchedSell.setBillNumber(rs.getString(SELL_DETAILS_COLUMN.BILL_NUMBER.toString()));

		if (!ApplicationUtility.isNullEmpty(rs.getString("sellImageURL"))) {
			fetchedSell.setImageURL(ApplicationUtility.getServerURL() + rs.getString("sellImageURL"));
		}

		Timestamp fetchedSellDate = rs.getTimestamp(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString());
		if (fetchedSellDate != null) {
			fetchedSell.setSellDate(DateUtility.converDateToUserString(fetchedSellDate));
		}
		Timestamp fetchedAddedDate = rs.getTimestamp(SELL_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		fetchedSell.setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));
		fetchedSell.setTotalBillAmount(rs.getDouble(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));
		setPaymentDone(rs.getBoolean(SELL_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE.toString()));
		setSell(fetchedSell);
	}

	public int getSellPaymentMappingId() {
		return sellPaymentMappingId;
	}

	public void setSellPaymentMappingId(int sellPaymentMappingId) {
		this.sellPaymentMappingId = sellPaymentMappingId;
	}

	public SellModel getSell() {
		return sell;
	}

	public void setSell(SellModel sell) {
		this.sell = sell;
	}

	public SellPaymentModel getPayment() {
		return payment;
	}

	public void setPayment(SellPaymentModel payment) {
		this.payment = payment;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isPaymentDone() {
		return paymentDone;
	}

	public void setPaymentDone(boolean paymentDone) {
		this.paymentDone = paymentDone;
	}

}
