package acc.webservice.components.payment.purchasepayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_MAPPING_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class PurchasePaymentMappingModel {

	private int purchasePaymentMappingId;
	private PurchaseModel purchase;
	private PurchasePaymentModel payment;
	private boolean isPaymentDone;
	private boolean isDeleted;

	public PurchasePaymentMappingModel() {
	}

	public PurchasePaymentMappingModel(ResultSet rs) throws SQLException {
		setPurchasePaymentMappingId(rs.getInt(PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_MAPPING_ID.toString()));
		PurchaseModel fetchedPurchase = new PurchaseModel();
		fetchedPurchase.setPurchaseId(rs.getInt(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString()));
		fetchedPurchase.setBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString()));
		fetchedPurchase.setTotalBillAmount(rs.getDouble(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));

		if (!ApplicationUtility.isNullEmpty(rs.getString("purchaseImageURL"))) {
			fetchedPurchase.setImageURL(ApplicationUtility.getServerURL() + rs.getString("purchaseImageURL"));
		}

		Timestamp fetchedPurchaseDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString());
		if (fetchedPurchaseDate != null) {
			fetchedPurchase.setPurchaseDate(DateUtility.converDateToUserString(fetchedPurchaseDate));
		}
		Timestamp fetchedAddedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		fetchedPurchase.setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		setPaymentDone(rs.getBoolean(PURCHASE_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE.toString()));
		setPurchase(fetchedPurchase);
	}

	public int getPurchasePaymentMappingId() {
		return purchasePaymentMappingId;
	}

	public void setPurchasePaymentMappingId(int purchasePaymentMappingId) {
		this.purchasePaymentMappingId = purchasePaymentMappingId;
	}

	public PurchaseModel getPurchase() {
		return purchase;
	}

	public void setPurchase(PurchaseModel purchase) {
		this.purchase = purchase;
	}

	public PurchasePaymentModel getPayment() {
		return payment;
	}

	public void setPayment(PurchasePaymentModel payment) {
		this.payment = payment;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isPaymentDone() {
		return isPaymentDone;
	}

	public void setPaymentDone(boolean isPaymentDone) {
		this.isPaymentDone = isPaymentDone;
	}

}
