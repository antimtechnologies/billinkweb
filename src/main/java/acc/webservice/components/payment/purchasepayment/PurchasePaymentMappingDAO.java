package acc.webservice.components.payment.purchasepayment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.PAYMENT_STATUS_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_MAPPING_COLUMN;

@Repository
@Lazy
public class PurchasePaymentMappingDAO {
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	List<Map<String, Object>> savePurchasePaymentMappingData(List<PurchasePaymentMappingModel> purchasePaymentMappingList, int companyId, int purchasePaymentId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_MAPPING))
				.append(String.format(" ( %s, %s, %s, %s ) ", PURCHASE_PAYMENT_MAPPING_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", PURCHASE_PAYMENT_MAPPING_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE));

		List<Map<String, Object>> purchaseItemMappingList = new ArrayList<>();
		Map<String, Object> purchaseItemMap;

		List<Map<String, Object>> purchasePaymentStatusList = new ArrayList<>();
		Map<String, Object> purchasePaymentStatusMap;

		for (PurchasePaymentMappingModel purchasePaymentMapping : purchasePaymentMappingList) {
			purchaseItemMap = new HashMap<>();
			purchaseItemMap.put(PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_ID.toString(), purchasePaymentMapping.getPurchase().getPurchaseId());
			purchaseItemMap.put(PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_ID.toString(), purchasePaymentId);
			purchaseItemMap.put(PURCHASE_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE.toString(), purchasePaymentMapping.isPaymentDone());
			purchaseItemMap.put(PURCHASE_PAYMENT_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
			purchaseItemMappingList.add(purchaseItemMap);

			purchasePaymentStatusMap = new HashMap<>();
			purchasePaymentStatusMap.put(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchasePaymentMapping.getPurchase().getPurchaseId());
			purchasePaymentStatusMap.put(PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS.toString(), purchasePaymentMapping.isPaymentDone() ? PAYMENT_STATUS_ENUM.COMPLETE_PAYMENT_STATUS.status() : PAYMENT_STATUS_ENUM.PARTIAL_PAYMENT_STATUS.status());
			purchasePaymentStatusMap.put(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
			purchasePaymentStatusList.add(purchasePaymentStatusMap);
		}

		namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), purchaseItemMappingList.toArray(new HashMap[0]));
		return purchasePaymentStatusList;
	}

	int deletePurchaseItemMapping(int companyId, int purchasePaymentId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_MAPPING, PURCHASE_PAYMENT_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_ID ))
				.append(String.format(" AND %s = ? ", PURCHASE_PAYMENT_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, purchasePaymentId, companyId });
	}
}
