package acc.webservice.components.payment.purchasepayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class PurchasePaymentImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<PurchasePaymentImageModel> getPurchasePaymentImageList(int companyId, int purchasePaymentId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID.toString(), purchasePaymentId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCreditNoteImageResultExctractor());
	}

	private ResultSetExtractor<List<PurchasePaymentImageModel>> getCreditNoteImageResultExctractor() {
		return new ResultSetExtractor<List<PurchasePaymentImageModel>>() {
			@Override
			public List<PurchasePaymentImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PurchasePaymentImageModel> purchasePaymentImageModelList = new ArrayList<>();
				while (rs.next()) {
					purchasePaymentImageModelList.add(new PurchasePaymentImageModel(rs));
				}
				return purchasePaymentImageModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] savePurchasePaymentImageURLData(List<PurchasePaymentImageModel> purchasePaymentImageModelList, int companyId, int purchasePaymentIsd) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s) ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> purchasePaymentImageURLMapList = new ArrayList<>();
		Map<String, Object> purchasePaymentImageURLMap;
		for (PurchasePaymentImageModel purchasePaymentImageModel : purchasePaymentImageModelList) {
			purchasePaymentImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(purchasePaymentImageModel.getImageURL())) {			
				purchasePaymentImageModel.setImageURL(purchasePaymentImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			purchasePaymentImageURLMap.put(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL.toString(), purchasePaymentImageModel.getImageURL());
			purchasePaymentImageURLMap.put(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID.toString(), purchasePaymentIsd);
			purchasePaymentImageURLMap.put(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			purchasePaymentImageURLMapList.add(purchasePaymentImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), purchasePaymentImageURLMapList.toArray(new HashMap[0]));
	}

	int deletePurchasePaymentImageURLData(int companyId, int purchasePaymentIsd) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_IMAGE_URL, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.PURCHASE_PAYMENT_ID ))
				.append(String.format(" AND %s = ? ", PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, purchasePaymentIsd, companyId });
	}

}
