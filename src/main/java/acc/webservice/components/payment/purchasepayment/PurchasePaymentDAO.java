package acc.webservice.components.payment.purchasepayment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_DETAILS;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class PurchasePaymentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@SuppressWarnings("unchecked")
	List<PurchasePaymentModel> getPurchasePaymentListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		Map<String, Object> parameters = new HashMap<>();
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.IMAGE_URL, PURCHASE_PAYMENT_DETAILS.AMOUNT))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.DESCRIPTION, PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2, PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" LM.%s AS %s, LM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" PLM.%s AS %s, PLM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, "paymentFromId", LEDGER_DETAILS_COLUMN.LEDGER_NAME, "paymentFromName"))
						.append(String.format(" SM.%s, SM.%s ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME))
						.append(String.format(" FROM %s PPD ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON PPD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, PURCHASE_PAYMENT_DETAILS.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s LM ON PPD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_PAYMENT_DETAILS.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
						.append(String.format(" INNER JOIN %s AUM ON PPD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s PLM ON PPD.%s = PLM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER, LEDGER_DETAILS_COLUMN.LEDGER_ID))						
						.append(String.format(" LEFT JOIN %s MUM ON PPD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON PPD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON PPD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE PPD.%s = 0 ", PURCHASE_PAYMENT_DETAILS.IS_DELETED))
						.append(String.format(" AND PPD.%s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {					
					sqlQueryToFetchData.append(String.format(" AND PPD.%s IN (:statusList) ", PURCHASE_PAYMENT_DETAILS.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString());
				
				if (isVerified == 0 || isVerified == 1) {					
					sqlQueryToFetchData.append(String.format(" AND PPD.%s = :%s ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED));
					parameters.put(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter, PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString());
				
				if (ledgerId > 0)  {					
					sqlQueryToFetchData.append(String.format(" AND PPD.%s = :%s ", PURCHASE_PAYMENT_DETAILS.LEDGER_ID, PURCHASE_PAYMENT_DETAILS.LEDGER_ID));
					parameters.put(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString())) {
				String expenseId = ApplicationUtility.getStrValue(filterParamter, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND PPD.%s LIKE :%s ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID));
				parameters.put(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), "%" + expenseId + "%");
			}

		}

		sqlQueryToFetchData.append(String.format(" ORDER BY PPD.%s DESC LIMIT :start, :noOfRecord ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID));

		parameters.put(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getPurchaseDetailsListExtractor());
	}

	private ResultSetExtractor<List<PurchasePaymentModel>> getPurchaseDetailsListExtractor() {

		return new ResultSetExtractor<List<PurchasePaymentModel>>() {
			@Override
			public List<PurchasePaymentModel> extractData(ResultSet rs) throws SQLException {
				List<PurchasePaymentModel> purchaseList = new ArrayList<>();
				while (rs.next()) {
					purchaseList.add(new PurchasePaymentModel(rs));
				}
				return purchaseList;
			}
		};
	}

	PurchasePaymentModel getPurchasePaymentDetailsById(int companyId, int purchaseId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.IMAGE_URL, PURCHASE_PAYMENT_DETAILS.AMOUNT))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.DESCRIPTION, PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME))
						.append(String.format(" PPD.%s, PPD.%s, PPD.%s, ", PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2, PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" LM.%s AS %s, LM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" PLM.%s AS %s, PLM.%s AS %s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, "paymentFromId", LEDGER_DETAILS_COLUMN.LEDGER_NAME, "paymentFromName"))
						.append(String.format(" SM.%s, SM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME))
						.append(String.format(" PM.%s, PM.%s, PM.%s, PM.%s As purchaseImageURL, ", PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME, PURCHASE_DETAILS_COLUMN.PURCHASE_DATE, PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, PURCHASE_DETAILS_COLUMN.IMAGE_URL))						
						.append(String.format(" PM.%s, PM.%s, PPM.%s, PPM.%s ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_DETAILS_COLUMN.BILL_NUMBER, PURCHASE_PAYMENT_MAPPING_COLUMN.IS_PAYMENT_DONE, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_MAPPING_ID))
						.append(String.format(" FROM %s PPD ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
						.append(String.format(" INNER JOIN %s LM ON PPD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_PAYMENT_DETAILS.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
						.append(String.format(" INNER JOIN %s SM ON PPD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, PURCHASE_PAYMENT_DETAILS.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s AUM ON PPD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s PLM ON PPD.%s = PLM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON PPD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON PPD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON PPD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, PURCHASE_PAYMENT_DETAILS.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s PPM ON PPM.%s = PPD.%s AND PPM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_MAPPING, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.IS_DELETED))
						.append(String.format(" AND PPM.%s = :%s ", PURCHASE_PAYMENT_MAPPING_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
						.append(String.format(" LEFT JOIN %s PM ON PM.%s = PPM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS, PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_ID))
						.append(String.format(" AND PM.%s = :%s ", PURCHASE_DETAILS_COLUMN.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
						.append(String.format(" WHERE PPD.%s = :%s ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
						.append(String.format(" AND PPD.%s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchaseId);
		parameters.put(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getPurchaseDetailsExtractor());
	}

	private ResultSetExtractor<PurchasePaymentModel> getPurchaseDetailsExtractor() {

		return new ResultSetExtractor<PurchasePaymentModel>() {
			@Override
			public PurchasePaymentModel extractData(ResultSet rs) throws SQLException {
				PurchasePaymentModel purchasePaymentDetails = new PurchasePaymentModel();
				List<PurchasePaymentMappingModel> purchaseList = new ArrayList<>();
				while (rs.next()) {
					if (purchasePaymentDetails.getPurchasePaymentId() < 1) {						
						purchasePaymentDetails = new PurchasePaymentModel(rs);
					}
					
					if (rs.getInt(PURCHASE_PAYMENT_MAPPING_COLUMN.PURCHASE_PAYMENT_MAPPING_ID.toString()) > 0)
						purchaseList.add(new PurchasePaymentMappingModel(rs));
				}
				purchasePaymentDetails.setPurchaseList(purchaseList);
				return purchasePaymentDetails;
			}
		};
	}
	
	PurchasePaymentModel publishPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = getPurchasePaymentInsertQuery(purchasePaymentModel);
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IMAGE_URL.toString(), purchasePaymentModel.getImageURL())
				.addValue(PURCHASE_PAYMENT_DETAILS.AMOUNT.toString(), purchasePaymentModel.getAmount())
				.addValue(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString(), purchasePaymentModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER.toString(), purchasePaymentModel.getPaymentFromLedger() == null || purchasePaymentModel.getPaymentFromLedger().getLedgerId() < 1 ? null : purchasePaymentModel.getPaymentFromLedger().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1.toString(), purchasePaymentModel.getReferenceNumber1())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2.toString(), purchasePaymentModel.getReferenceNumber2())
				.addValue(PURCHASE_PAYMENT_DETAILS.DESCRIPTION.toString(), purchasePaymentModel.getDescription())
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE.toString(), purchasePaymentModel.getPaymentDate())
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.ADDED_BY.toString(), purchasePaymentModel.getAddedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		purchasePaymentModel.setPurchasePaymentId(holder.getKey().intValue());
		return purchasePaymentModel;
	}

	PurchasePaymentModel savePurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = getPurchasePaymentInsertQuery(purchasePaymentModel);

		if (!ApplicationUtility.isNullEmpty(purchasePaymentModel.getImageURL())) {			
			purchasePaymentModel.setImageURL(purchasePaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IMAGE_URL.toString(), purchasePaymentModel.getImageURL())
				.addValue(PURCHASE_PAYMENT_DETAILS.AMOUNT.toString(), purchasePaymentModel.getAmount())
				.addValue(PURCHASE_PAYMENT_DETAILS.DESCRIPTION.toString(), purchasePaymentModel.getDescription())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1.toString(), purchasePaymentModel.getReferenceNumber1())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2.toString(), purchasePaymentModel.getReferenceNumber2())
				.addValue(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString(), purchasePaymentModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER.toString(), purchasePaymentModel.getPaymentFromLedger() == null || purchasePaymentModel.getPaymentFromLedger().getLedgerId() < 1 ? null : purchasePaymentModel.getPaymentFromLedger().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE.toString(), purchasePaymentModel.getPaymentDate())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.ADDED_BY.toString(), purchasePaymentModel.getAddedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		purchasePaymentModel.setPurchasePaymentId(holder.getKey().intValue());
		return purchasePaymentModel;
	}

	private StringBuilder getPurchasePaymentInsertQuery(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format("( %s, %s, %s, %s, ", PURCHASE_PAYMENT_DETAILS.IMAGE_URL, PURCHASE_PAYMENT_DETAILS.AMOUNT, PURCHASE_PAYMENT_DETAILS.LEDGER_ID, PURCHASE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s, %s, ", PURCHASE_PAYMENT_DETAILS.DESCRIPTION, PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE))
							.append(String.format(" %s, %s, %s, ", PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2))
							.append(String.format(" %s, %s, %s )", PURCHASE_PAYMENT_DETAILS.ADDED_BY, PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, :%s, ", PURCHASE_PAYMENT_DETAILS.IMAGE_URL, PURCHASE_PAYMENT_DETAILS.AMOUNT, PURCHASE_DETAILS_COLUMN.LEDGER_ID, PURCHASE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s, :%s, ", PURCHASE_PAYMENT_DETAILS.DESCRIPTION, PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE))
							.append(String.format(" :%s, :%s, :%s, ", PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2))
							.append(String.format(" :%s, :%s, :%s )", PURCHASE_PAYMENT_DETAILS.ADDED_BY, PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED));
		return sqlQuery;
	}

	PurchasePaymentModel updatePublishedPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = getPurchasePaymentUpdateQuery(purchasePaymentModel);

		if (!ApplicationUtility.isNullEmpty(purchasePaymentModel.getImageURL())) {			
			purchasePaymentModel.setImageURL(purchasePaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IMAGE_URL.toString(), purchasePaymentModel.getImageURL())
				.addValue(PURCHASE_PAYMENT_DETAILS.AMOUNT.toString(), purchasePaymentModel.getAmount())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1.toString(), purchasePaymentModel.getReferenceNumber1())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2.toString(), purchasePaymentModel.getReferenceNumber2())
				.addValue(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString(), purchasePaymentModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER.toString(), purchasePaymentModel.getPaymentFromLedger() == null || purchasePaymentModel.getPaymentFromLedger().getLedgerId() < 1 ? null : purchasePaymentModel.getPaymentFromLedger().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.DESCRIPTION.toString(), purchasePaymentModel.getDescription())
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE.toString(), purchasePaymentModel.getPaymentDate())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.MODIFIED_BY.toString(), purchasePaymentModel.getModifiedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentModel.getPurchasePaymentId())
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		purchasePaymentModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		return purchasePaymentModel;
	}

	PurchasePaymentModel updateSavedPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = getPurchasePaymentUpdateQuery(purchasePaymentModel);

		if (!ApplicationUtility.isNullEmpty(purchasePaymentModel.getImageURL())) {			
			purchasePaymentModel.setImageURL(purchasePaymentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IMAGE_URL.toString(), purchasePaymentModel.getImageURL())
				.addValue(PURCHASE_PAYMENT_DETAILS.AMOUNT.toString(), purchasePaymentModel.getAmount())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1.toString(), purchasePaymentModel.getReferenceNumber1())
				.addValue(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2.toString(), purchasePaymentModel.getReferenceNumber2())
				.addValue(PURCHASE_PAYMENT_DETAILS.DESCRIPTION.toString(), purchasePaymentModel.getDescription())
				.addValue(PURCHASE_PAYMENT_DETAILS.LEDGER_ID.toString(), purchasePaymentModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER.toString(), purchasePaymentModel.getPaymentFromLedger() == null || purchasePaymentModel.getPaymentFromLedger().getLedgerId() < 1 ? null : purchasePaymentModel.getPaymentFromLedger().getLedgerId())
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE.toString(), purchasePaymentModel.getPaymentDate())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.MODIFIED_BY.toString(), purchasePaymentModel.getModifiedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentModel.getPurchasePaymentId())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		purchasePaymentModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		return purchasePaymentModel;
	}

	private StringBuilder getPurchasePaymentUpdateQuery(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IMAGE_URL, PURCHASE_PAYMENT_DETAILS.IMAGE_URL))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.AMOUNT, PURCHASE_PAYMENT_DETAILS.AMOUNT))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.DESCRIPTION, PURCHASE_PAYMENT_DETAILS.DESCRIPTION))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2, PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.STATUS_ID))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.LEDGER_ID, PURCHASE_PAYMENT_DETAILS.LEDGER_ID))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER, PURCHASE_PAYMENT_DETAILS.PAYMENT_FROM_LEDGER))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE, PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.MODIFIED_BY, PURCHASE_PAYMENT_DETAILS.MODIFIED_BY))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", PURCHASE_PAYMENT_DETAILS.IS_DELETED, PURCHASE_PAYMENT_DETAILS.IS_DELETED));
		return sqlQuery;
	}

	PurchasePaymentModel markPurchasePaymentAsVerified(PurchasePaymentModel purchasePaymentModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.VERIFIED_BY, PURCHASE_PAYMENT_DETAILS.VERIFIED_BY))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", PURCHASE_PAYMENT_DETAILS.IS_DELETED, PURCHASE_PAYMENT_DETAILS.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 1)
				.addValue(PURCHASE_PAYMENT_DETAILS.VERIFIED_BY.toString(), purchasePaymentModel.getVerifiedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentModel.getPurchasePaymentId())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		
		purchasePaymentModel.setVerified(true);
		purchasePaymentModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		return purchasePaymentModel;
	}

	int deletePublishedPurchasePaymentData(int companyId, int purchasePaymentId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.STATUS_ID))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.DELETED_BY, PURCHASE_PAYMENT_DETAILS.DELETED_BY))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s  ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentId)
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), companyId)
				.addValue(PURCHASE_PAYMENT_DETAILS.DELETED_BY.toString(), deletedBy)
				.addValue(PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deleteSavedPurchasePaymentData(int companyId, int purchasePaymentId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_DELETED, PURCHASE_PAYMENT_DETAILS.IS_DELETED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.DELETED_BY, PURCHASE_PAYMENT_DETAILS.DELETED_BY))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentId)
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), companyId)
				.addValue(PURCHASE_PAYMENT_DETAILS.DELETED_BY.toString(), deletedBy)
				.addValue(PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	PurchasePaymentModel markPurchasePaymentAsDeleteAndVerified(PurchasePaymentModel purchasePaymentModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, PURCHASE_PAYMENT_DETAILS.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.VERIFIED_BY, PURCHASE_PAYMENT_DETAILS.VERIFIED_BY))
							.append(String.format(" %s = :%s, ", PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME, PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.IS_DELETED, PURCHASE_PAYMENT_DETAILS.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString(), 1)
				.addValue(PURCHASE_PAYMENT_DETAILS.VERIFIED_BY.toString(), purchasePaymentModel.getVerifiedBy().getUserId())
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentModel.getPurchasePaymentId())
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 1);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		
		purchasePaymentModel.setVerified(true);
		purchasePaymentModel.setDeleted(true);
		purchasePaymentModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		return purchasePaymentModel;
	}

	PurchasePaymentModel publishSavedPurchasePayment(PurchasePaymentModel purchasePaymentModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
							.append(String.format(" %s = :%s ", PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.STATUS_ID))
							.append(String.format(" WHERE %s = :%s ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID))
							.append(String.format(" AND %s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID))
							.append(String.format(" AND %s = :%s ", PURCHASE_PAYMENT_DETAILS.IS_DELETED, PURCHASE_PAYMENT_DETAILS.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString(), purchasePaymentModel.getPurchasePaymentId())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchasePaymentModel.getCompanyId())
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		purchasePaymentModel.setStatus(status);
		return purchasePaymentModel;
	}

	int getPendingPurchasePaymentCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ", PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_PAYMENT_DETAILS))
				.append(String.format("WHERE %s != :%s ", PURCHASE_PAYMENT_DETAILS.STATUS_ID, PURCHASE_PAYMENT_DETAILS.STATUS_ID))
				.append(String.format(" AND %s = :%s ", PURCHASE_PAYMENT_DETAILS.COMPANY_ID, PURCHASE_PAYMENT_DETAILS.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", PURCHASE_PAYMENT_DETAILS.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_PAYMENT_DETAILS.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_PAYMENT_DETAILS.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

}
