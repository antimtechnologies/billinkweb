package acc.webservice.components.payment.purchasepayment;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import acc.webservice.components.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.global.utils.DateUtility;
import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.payment.purchasepayment.utility.PurchasePaymentToSellPaymentUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_DETAILS;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/purchasePaymentData/")
public class PurchasePaymentController {

	@Autowired
	private PurchasePaymentService purchasePaymentService;

	@Autowired
	private PurchasePaymentToSellPaymentUtility purchasePaymentToSellPaymentUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getPurchasePaymentListData", method = RequestMethod.POST)
	public Map<String, Object> getPurchasePaymentListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				purchasePaymentService.getPurchasePaymentListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getPurchasePaymentDetailsById", method = RequestMethod.POST)
	public Map<String, Object> getPurchasePaymentDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchasePaymentId = ApplicationUtility.getIntValue(parameters,
				PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.getPurchasePaymentDetailsById(companyId, purchasePaymentId));
		return responseData;
	}

	@RequestMapping(value = "publishPurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> publishPurchasePaymentData(@RequestBody PurchasePaymentModel purchasePaymentModel)
			throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.publishPurchasePaymentData(purchasePaymentModel));
		try {
			purchasePaymentToSellPaymentUtility.saveSellPaymentDataForLedger(purchasePaymentModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "savePurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> savePurchasePaymentData(@RequestBody PurchasePaymentModel purchasePaymentModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.savePurchasePaymentData(purchasePaymentModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedPurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedPurchasePaymentData(
			@RequestBody PurchasePaymentModel purchasePaymentModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				purchasePaymentModel.getPaymentDate())) {
			purchasePaymentModel.setPaymentDate(DateUtility.convertDateFormat(purchasePaymentModel.getPaymentDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.updatePublishedPurchasePaymentData(purchasePaymentModel));
		return responseData;
	}

	@RequestMapping(value = "updateSavedPurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedPurchasePaymentData(@RequestBody PurchasePaymentModel purchasePaymentModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				purchasePaymentModel.getPaymentDate())) {
			purchasePaymentModel.setPaymentDate(DateUtility.convertDateFormat(purchasePaymentModel.getPaymentDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.updateSavedPurchasePaymentData(purchasePaymentModel));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedPurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> deletePurchasePaymentData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchasePaymentId = ApplicationUtility.getIntValue(parameters,
				PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				purchasePaymentService.deletePublishedPurchasePaymentData(companyId, purchasePaymentId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedPurchasePaymentData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedPurchasePaymentData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchasePaymentId = ApplicationUtility.getIntValue(parameters,
				PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				purchasePaymentService.deleteSavedPurchasePaymentData(companyId, purchasePaymentId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "markPurchasePaymentAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markPurchasePaymentAsVerified(@RequestBody PurchasePaymentModel purchasePaymentModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.markPurchasePaymentAsVerified(purchasePaymentModel));
		return responseData;
	}

	@RequestMapping(value = "publishSavedPurchasePayment", method = RequestMethod.POST)
	public Map<String, Object> publishSavedPurchasePayment(@RequestBody PurchasePaymentModel purchasePaymentModel)
			throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchasePaymentService.publishSavedPurchasePayment(purchasePaymentModel));

		try {
			if (purchasePaymentModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
				purchasePaymentToSellPaymentUtility.saveSellPaymentDataForLedger(purchasePaymentModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "getallpurchasePaymentByDay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchasePaymentDetailByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("ispaymentdone") != null) {
			if (request.getParameter("day") != null) {
				int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = "http://localhost:8090/user/purchasepaymentmapping/purchasepaymentbyday?ispaymentdone="
						+ ispaymentdone + "&day=" + day;
			}

		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "getallpurchasePaymentByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchasePaymentDetailByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("ispaymentdone") != null) {
			if (request.getParameter("month") != null) {
				int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = "http://localhost:8090/user/purchasepaymentmapping/purchasepaymentbymonth?ispaymentdone="
						+ ispaymentdone + "&month=" + month;
			}

		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "purchasePaymentByCompanyAndDay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchasePaymentDetailByCompanyAndDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("ispaymentdone") != null) {
				if (request.getParameter("day") != null) {
					int companyId = Integer.parseInt(request.getParameter("companyId"));
					int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
					int day = Integer.parseInt(request.getParameter("day"));
					fooResourceUrl = "http://localhost:8090/user/purchasepaymentmapping/findallpurchasebycompanyandday?companyId="
							+ companyId + "&ispaymentdone=" + ispaymentdone + "&day=" + day;
				}

			}
		}

		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "purchasePaymentByCompanyAndMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchasePaymentDetailByCompanyAndMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("ispaymentdone") != null) {
				if (request.getParameter("month") != null) {
					int companyId = Integer.parseInt(request.getParameter("companyId"));
					int ispaymentdone = Integer.parseInt(request.getParameter("ispaymentdone"));
					int month = Integer.parseInt(request.getParameter("month"));
					fooResourceUrl = "http://localhost:8090/user/purchasepaymentmapping/findallpurchasebycompanyandmonth?companyId="
							+ companyId + "&ispaymentdone=" + ispaymentdone + "&month=" + month;
				}

			}
		}

		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}
}
