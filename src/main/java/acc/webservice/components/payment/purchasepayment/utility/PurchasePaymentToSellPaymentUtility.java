package acc.webservice.components.payment.purchasepayment.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentImageModel;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentMappingModel;
import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.payment.sellpayment.SellPaymentImageModel;
import acc.webservice.components.payment.sellpayment.SellPaymentMappingModel;
import acc.webservice.components.payment.sellpayment.SellPaymentService;
import acc.webservice.components.payment.sellpayment.model.SellPaymentModel;
import acc.webservice.components.purchase.PurchaseService;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.purchase.utility.PurchaseToSellUtility;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class PurchasePaymentToSellPaymentUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private SellPaymentService sellPaymentService;

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private PurchaseToSellUtility purchaseToSellUtility;

	public SellPaymentModel saveSellPaymentDataForLedger(PurchasePaymentModel purchasePaymentModel) throws AccountingSofwareException {
		SellPaymentModel sellPaymentModel = new SellPaymentModel();

		LedgerModel ledgerBasicDetails = ledgerService.getBasicLedgerDetailByLedgerId(
				purchasePaymentModel.getLedgerData().getLedgerId(), purchasePaymentModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());
			CompanyModel sellPaymentCompanyModel = companyService.getCompanyDetails(selectedLedgerCompanyId);
			
			if (sellPaymentCompanyModel.isAllowB2BImport()) {
				if (selectedLedgerCompanyId > 0) {
					sellPaymentModel = createSellPaymentModelFromPurchasePaymentModel(purchasePaymentModel, selectedLedgerCompanyId);
					sellPaymentService.saveSellPaymentData(sellPaymentModel);
				}				
			}
		}
		return sellPaymentModel;
	}

	/**
	 * @param purchasePaymentModel
	 * @param selectedLedgerCompanyId
	 * @return
	 * @throws AccountingSofwareException 
	 */
	private SellPaymentModel createSellPaymentModelFromPurchasePaymentModel(PurchasePaymentModel purchasePaymentModel, int selectedLedgerCompanyId) throws AccountingSofwareException {
		SellPaymentModel sellPaymentModel = new SellPaymentModel();
		sellPaymentModel.setCompanyId(selectedLedgerCompanyId);
		sellPaymentModel.setAmount(purchasePaymentModel.getAmount());
		sellPaymentModel.setImageURL(purchasePaymentModel.getImageURL());
		sellPaymentModel.setDescription(purchasePaymentModel.getDescription());
		sellPaymentModel.setPaymentDate(purchasePaymentModel.getPaymentDate());

		CompanyModel sellCompanyDetail = companyService.getCompanyDetails(purchasePaymentModel.getCompanyId());
		LedgerModel sellLedgerData = new LedgerModel();
		int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompanyId,
				sellCompanyDetail.getGstNumber());
		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompanyId, sellCompanyDetail);
		}
		sellLedgerData.setLedgerId(purchaseLedgerId);
		sellPaymentModel.setLedgerData(sellLedgerData);

		List<SellPaymentImageModel> sellImageURLList = new ArrayList<>();
		SellPaymentImageModel sellPaymentImageModel;
		
		if (ApplicationUtility.getSize(purchasePaymentModel.getImageURLList()) > 0) {			
			for (PurchasePaymentImageModel purchasePaymentImageModel : purchasePaymentModel.getImageURLList()) {
				sellPaymentImageModel = new SellPaymentImageModel();
				sellPaymentImageModel.setImageURL(purchasePaymentImageModel.getImageURL());
				sellImageURLList.add(sellPaymentImageModel);
			}
		}

		sellPaymentModel.setImageURLList(sellImageURLList);

		List<PurchasePaymentMappingModel> purchasePaymentMappingModels = purchasePaymentModel.getPurchaseList();
		List<SellPaymentMappingModel> sellPaymentMappingModelList = new ArrayList<>();
		PurchaseModel purchaseModel;
		SellPaymentMappingModel sellPaymentMappingModel;
		SellModel sellModel;

		if (ApplicationUtility.getSize(purchasePaymentMappingModels) > 0) {			
			for (PurchasePaymentMappingModel purchasePaymentMappingModel : purchasePaymentMappingModels) {
				sellPaymentMappingModel = new SellPaymentMappingModel();
				purchaseModel = purchaseService.getPurchaseDetailsById(purchasePaymentModel.getCompanyId(),
						purchasePaymentMappingModel.getPurchase().getPurchaseId());
				
				if (purchaseModel.getPurchaseDate() != null) {				
					purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
				}

				purchaseModel.setCompanyId(purchasePaymentModel.getCompanyId());
				sellModel = purchaseToSellUtility.saveSellDataForSelectedLedger(purchaseModel);
				
				if (sellModel.getSellId() > 0) {
					sellPaymentMappingModel.setSell(sellModel);
					sellPaymentMappingModel.setPaymentDone(purchasePaymentMappingModel.isPaymentDone());
					sellPaymentMappingModelList.add(sellPaymentMappingModel);
				}
			}
		}

		sellPaymentModel.setSellList(sellPaymentMappingModelList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		sellPaymentModel.setAddedBy(userData);

		return sellPaymentModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail) throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
