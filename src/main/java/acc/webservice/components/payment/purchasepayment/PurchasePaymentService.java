package acc.webservice.components.payment.purchasepayment;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.components.purchase.PurchaseService;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class PurchasePaymentService {
	
	@Autowired
	private PurchasePaymentDAO purchasePaymentDAO;

	@Autowired
	private PurchasePaymentMappingDAO purchasePaymentMappingDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private PurchasePaymentImageDAO purchasePaymentImageDAO;

	public List<PurchasePaymentModel> getPurchasePaymentListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		return purchasePaymentDAO.getPurchasePaymentListData(companyId, start, numberOfRecord, filterParamter);
	}

	public PurchasePaymentModel getPurchasePaymentDetailsById(int companyId, int purchasePaymentId) {
		PurchasePaymentModel purchasePaymentModel = purchasePaymentDAO.getPurchasePaymentDetailsById(companyId, purchasePaymentId);
		purchasePaymentModel.setImageURLList(purchasePaymentImageDAO.getPurchasePaymentImageList(companyId, purchasePaymentId));
		return purchasePaymentModel;
	}
	
	public PurchasePaymentModel publishPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		validatePurchasePaymentManupulateData(purchasePaymentModel);
		purchasePaymentDAO.publishPurchasePaymentData(purchasePaymentModel);
		List<Map<String, Object>> purchasePaymentStatusList = purchasePaymentMappingDAO.savePurchasePaymentMappingData(purchasePaymentModel.getPurchaseList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		purchaseService.updatePurchasePaymentStatus(purchasePaymentStatusList, purchasePaymentModel.getCompanyId());
		
		if (ApplicationUtility.getSize(purchasePaymentModel.getImageURLList()) > 0) {
			purchasePaymentImageDAO.savePurchasePaymentImageURLData(purchasePaymentModel.getImageURLList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		}
		companyService.updateCompanyPurchasePaymentCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 2), purchasePaymentModel.getCompanyId());
		companyService.updateCompanyPurchasePaymentPendingCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 0), purchasePaymentModel.getCompanyId());
		return purchasePaymentModel;
	}

	public PurchasePaymentModel savePurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		validatePurchasePaymentManupulateData(purchasePaymentModel);
		purchasePaymentDAO.savePurchasePaymentData(purchasePaymentModel);
		if(purchasePaymentModel.getPurchaseList() != null && purchasePaymentModel.getPurchaseList().size() > 0) {
			List<Map<String, Object>> purchasePaymentStatusList = purchasePaymentMappingDAO.savePurchasePaymentMappingData(purchasePaymentModel.getPurchaseList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
			purchaseService.updatePurchasePaymentStatus(purchasePaymentStatusList, purchasePaymentModel.getCompanyId());
						
		}

		if (ApplicationUtility.getSize(purchasePaymentModel.getImageURLList()) > 0) {
			purchasePaymentImageDAO.savePurchasePaymentImageURLData(purchasePaymentModel.getImageURLList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		}
		return purchasePaymentModel;
	}
	
	public PurchasePaymentModel updatePublishedPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		validatePurchasePaymentManupulateData(purchasePaymentModel);
		purchasePaymentMappingDAO.deletePurchaseItemMapping(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		List<Map<String, Object>> purchasePaymentStatusList = purchasePaymentMappingDAO.savePurchasePaymentMappingData(purchasePaymentModel.getPurchaseList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		purchaseService.updatePurchasePaymentStatus(purchasePaymentStatusList, purchasePaymentModel.getCompanyId());
		purchasePaymentDAO.updatePublishedPurchasePaymentData(purchasePaymentModel);

		if (ApplicationUtility.getSize(purchasePaymentModel.getImageURLList()) > 0) {
			purchasePaymentImageDAO.deletePurchasePaymentImageURLData(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
			purchasePaymentImageDAO.savePurchasePaymentImageURLData(purchasePaymentModel.getImageURLList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		}
		companyService.updateCompanyPurchasePaymentPendingCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 0), purchasePaymentModel.getCompanyId());
		return purchasePaymentModel;
	}

	public PurchasePaymentModel updateSavedPurchasePaymentData(PurchasePaymentModel purchasePaymentModel) {
		validatePurchasePaymentManupulateData(purchasePaymentModel);
		purchasePaymentMappingDAO.deletePurchaseItemMapping(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		List<Map<String, Object>> purchasePaymentStatusList = purchasePaymentMappingDAO.savePurchasePaymentMappingData(purchasePaymentModel.getPurchaseList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		purchaseService.updatePurchasePaymentStatus(purchasePaymentStatusList, purchasePaymentModel.getCompanyId());
		purchasePaymentDAO.updateSavedPurchasePaymentData(purchasePaymentModel);

		if (ApplicationUtility.getSize(purchasePaymentModel.getImageURLList()) > 0) {
			purchasePaymentImageDAO.deletePurchasePaymentImageURLData(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
			purchasePaymentImageDAO.savePurchasePaymentImageURLData(purchasePaymentModel.getImageURLList(), purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
		}
		return purchasePaymentModel;
	}

	public PurchasePaymentModel markPurchasePaymentAsVerified(PurchasePaymentModel purchasePaymentModel) {
		if (statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()) == purchasePaymentModel.getStatus().getStatusId()) {
			purchasePaymentImageDAO.deletePurchasePaymentImageURLData(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
			purchasePaymentMappingDAO.deletePurchaseItemMapping(purchasePaymentModel.getCompanyId(), purchasePaymentModel.getPurchasePaymentId());
			purchasePaymentDAO.markPurchasePaymentAsDeleteAndVerified(purchasePaymentModel);
		} else {	
			purchasePaymentDAO.markPurchasePaymentAsVerified(purchasePaymentModel);
		}
		companyService.updateCompanyPurchasePaymentPendingCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 0), purchasePaymentModel.getCompanyId());
		return purchasePaymentModel;
	}

	public int deletePublishedPurchasePaymentData(int companyId, int purchasePaymentId, int deletedBy) {
		purchasePaymentDAO.deletePublishedPurchasePaymentData(companyId, purchasePaymentId, deletedBy);
		return companyService.updateCompanyPurchasePaymentPendingCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(companyId, 0), companyId);
	}

	public int deleteSavedPurchasePaymentData(int companyId, int purchasePaymentId, int deletedBy) {
		purchasePaymentImageDAO.deletePurchasePaymentImageURLData(companyId, purchasePaymentId);
		return purchasePaymentDAO.deleteSavedPurchasePaymentData(companyId, purchasePaymentId, deletedBy);
	}

	public PurchasePaymentModel publishSavedPurchasePayment(PurchasePaymentModel purchasePaymentModel) {
		updateSavedPurchasePaymentData(purchasePaymentModel);
		purchasePaymentDAO.publishSavedPurchasePayment(purchasePaymentModel);
		companyService.updateCompanyPurchasePaymentCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 2), purchasePaymentModel.getCompanyId());
		companyService.updateCompanyPurchasePaymentPendingCount(purchasePaymentDAO.getPendingPurchasePaymentCountData(purchasePaymentModel.getCompanyId(), 0), purchasePaymentModel.getCompanyId());
		return purchasePaymentModel;
	}

	private void validatePurchasePaymentManupulateData(PurchasePaymentModel purchasePaymentModel) {

		if (purchasePaymentModel.getLedgerData() == null || purchasePaymentModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save purchase payment data.");
		}

	}
}
