package acc.webservice.components.payment.purchasepayment.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentImageModel;
import acc.webservice.components.payment.purchasepayment.PurchasePaymentMappingModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_DETAILS;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class PurchasePaymentModel {

	private int purchasePaymentId;
	private String imageURL;
	private double amount;
	private String description;
	private StatusModel status;
	private String paymentDate;
	private LedgerModel ledgerData;
	private LedgerModel paymentFromLedger;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel verifiedBy;
	private String verifiedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private boolean deleted;
	private boolean verified;
	private int companyId;
	private List<PurchasePaymentMappingModel> purchaseList;
	private List<PurchasePaymentImageModel> imageURLList;
	private String referenceNumber1;
	private String referenceNumber2;

	public PurchasePaymentModel() {
	}

	public PurchasePaymentModel(ResultSet rs) throws SQLException {
		setPurchasePaymentId(rs.getInt(PURCHASE_PAYMENT_DETAILS.PURCHASE_PAYMENT_ID.toString()));

		String imageURL = rs.getString(PURCHASE_PAYMENT_DETAILS.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
		setReferenceNumber1(rs.getString(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(PURCHASE_PAYMENT_DETAILS.REFERENCE_NUBER_2.toString()));
		setAmount(rs.getDouble(PURCHASE_PAYMENT_DETAILS.AMOUNT.toString()));
		setDescription(rs.getString(PURCHASE_PAYMENT_DETAILS.DESCRIPTION.toString()));
		setVerified(rs.getBoolean(PURCHASE_PAYMENT_DETAILS.IS_VERIFIED.toString()));

		Timestamp fetchedPaymentDate = rs.getTimestamp(PURCHASE_PAYMENT_DETAILS.PAYMENT_DATE.toString());
		if (fetchedPaymentDate != null) {
			setPaymentDate(DateUtility.converDateToUserString(fetchedPaymentDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(PURCHASE_PAYMENT_DETAILS.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(PURCHASE_PAYMENT_DETAILS.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerifiedDate = rs.getTimestamp(PURCHASE_PAYMENT_DETAILS.VERIFIED_DATE_TIME.toString());
		if (fetchedVerifiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerifiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(PURCHASE_PAYMENT_DETAILS.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		LedgerModel fetchedLedgerData = new LedgerModel();
		fetchedLedgerData.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedgerData.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		setLedgerData(fetchedLedgerData);

		LedgerModel fetchedPaymentFromLedger = new LedgerModel();
		fetchedPaymentFromLedger.setLedgerName(rs.getString("paymentFromName"));
		fetchedPaymentFromLedger.setLedgerId(rs.getInt("paymentFromId"));
		setPaymentFromLedger(fetchedPaymentFromLedger);

		setStatus(new StatusModel(rs));
	}

	public int getPurchasePaymentId() {
		return purchasePaymentId;
	}

	public void setPurchasePaymentId(int purchasePaymentId) {
		this.purchasePaymentId = purchasePaymentId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<PurchasePaymentMappingModel> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<PurchasePaymentMappingModel> purchaseList) {
		this.purchaseList = purchaseList;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public List<PurchasePaymentImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<PurchasePaymentImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public LedgerModel getPaymentFromLedger() {
		return paymentFromLedger;
	}

	public void setPaymentFromLedger(LedgerModel paymentFromLedger) {
		this.paymentFromLedger = paymentFromLedger;
	}

	@Override
	public String toString() {
		return "PurchasePaymentModel [purchasePaymentId=" + purchasePaymentId + ", imageURL=" + imageURL + ", amount="
				+ amount + ", description=" + description + ", status=" + status + ", paymentDate=" + paymentDate
				+ ", addedDateTime=" + addedDateTime + ", modifiedDateTime=" + modifiedDateTime + ", verifiedDateTime="
				+ verifiedDateTime + ", deletedDateTime=" + deletedDateTime + ", deleted=" + deleted + "]";
	}

}
