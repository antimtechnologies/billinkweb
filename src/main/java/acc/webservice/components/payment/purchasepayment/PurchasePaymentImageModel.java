package acc.webservice.components.payment.purchasepayment;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.payment.purchasepayment.model.PurchasePaymentModel;
import acc.webservice.enums.DatabaseEnum.PURCHASE_PAYMENT_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class PurchasePaymentImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private PurchasePaymentModel purchasePaymentModel;

	public PurchasePaymentImageModel() {
	}

	public PurchasePaymentImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(PURCHASE_PAYMENT_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public PurchasePaymentModel getPurchasePaymentModel() {
		return purchasePaymentModel;
	}

	public void setPurchasePaymentModel(PurchasePaymentModel purchasePaymentModel) {
		this.purchasePaymentModel = purchasePaymentModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
