package acc.webservice.components.buyer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class BuyerInfoModelService {

	@Autowired
	BuyerInfoModelDao buyerInfoModelDao;
	
	public BuyerInfoModel saveBuyerInfoData(BuyerInfoModel buyerInfoModel ) {
		return buyerInfoModelDao.saveBuyerModelData(buyerInfoModel);
	}
	
	public List<BuyerInfoModel> getBuyerInfoByledgerId(int ledgerId){
		return buyerInfoModelDao.getBuyerInfoByledgerId(ledgerId);
	}
	
	public BuyerInfoModel updateBuyerInfoData(BuyerInfoModel buyerInfoModel ) {
		return buyerInfoModelDao.updateBuyerInfoData(buyerInfoModel);
	}
	
	public int deleteLOneBuyerInfo(int buyerId ) {
		return buyerInfoModelDao.deleteLOneBuyerInfo(buyerId);
	}
	
}
