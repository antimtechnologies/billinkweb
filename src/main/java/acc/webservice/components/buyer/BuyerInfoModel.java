package acc.webservice.components.buyer;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.BUYER_DETAILS_COLUMN;

public class BuyerInfoModel {

	private int buyerId;
	private String buyerName,address,city,pan;
	private long buyerMobileNo,adharNo;
	private int stateId,ledgerId;
	
	
	public BuyerInfoModel() {}
	public BuyerInfoModel(ResultSet rs) throws SQLException {
		setBuyerId(rs.getInt(BUYER_DETAILS_COLUMN.BUYER_ID.toString()));
		setBuyerName(rs.getString(BUYER_DETAILS_COLUMN.BUYER_NAME.toString()));
		setAddress(rs.getString(BUYER_DETAILS_COLUMN.ADDRESS.toString()));
		setCity(rs.getString(BUYER_DETAILS_COLUMN.CITY.toString()));
		setPan(rs.getString(BUYER_DETAILS_COLUMN.PAN.toString()));
		setBuyerMobileNo(rs.getLong(BUYER_DETAILS_COLUMN.MOBILE_NO.toString()));
		setAdharNo(rs.getLong(BUYER_DETAILS_COLUMN.ADHAR_NO.toString()));
		setStateId(rs.getInt(BUYER_DETAILS_COLUMN.STATE_ID.toString()));
		setLedgerId(rs.getInt(BUYER_DETAILS_COLUMN.LEDGER_ID.toString()));
	}
	
	public int getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(int buyerId) {
		this.buyerId = buyerId;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public long getBuyerMobileNo() {
		return buyerMobileNo;
	}
	public void setBuyerMobileNo(long buyerMobileNo) {
		this.buyerMobileNo = buyerMobileNo;
	}
	public long getAdharNo() {
		return adharNo;
	}
	public void setAdharNo(long adharNo) {
		this.adharNo = adharNo;
	}
	
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public int getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}
	

}
