package acc.webservice.components.buyer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.items.ItemModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.enums.DatabaseEnum.BUYER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class BuyerInfoModelDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	BuyerInfoModel saveBuyerModelData(BuyerInfoModel buyerModel) {
		 StringBuilder sqlQueryCompanySave = new StringBuilder();
		 sqlQueryCompanySave.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_BUYERS_DETAILS))
							.append(String.format(" ( %s, %s, %s, %s, ", BUYER_DETAILS_COLUMN.BUYER_NAME, BUYER_DETAILS_COLUMN.MOBILE_NO,BUYER_DETAILS_COLUMN.PAN,BUYER_DETAILS_COLUMN.CITY))
							.append(String.format(" %s, %s, %s, %s) ", BUYER_DETAILS_COLUMN.ADDRESS, BUYER_DETAILS_COLUMN.ADHAR_NO,BUYER_DETAILS_COLUMN.STATE_ID,BUYER_DETAILS_COLUMN.LEDGER_ID))
							.append(" VALUES ")
							.append(String.format(" ( :%s, :%s, :%s , :%s, ", BUYER_DETAILS_COLUMN.BUYER_NAME, BUYER_DETAILS_COLUMN.MOBILE_NO,BUYER_DETAILS_COLUMN.PAN,BUYER_DETAILS_COLUMN.CITY))
							.append(String.format(" :%s, :%s, :%s , :%s ) ", BUYER_DETAILS_COLUMN.ADDRESS, BUYER_DETAILS_COLUMN.ADHAR_NO,BUYER_DETAILS_COLUMN.STATE_ID,BUYER_DETAILS_COLUMN.LEDGER_ID));

		KeyHolder holder = new GeneratedKeyHolder();
		System.err.println("LedgerID For dao ::"+buyerModel.getLedgerId());
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(BUYER_DETAILS_COLUMN.BUYER_NAME.toString(), buyerModel.getBuyerName())
				.addValue(BUYER_DETAILS_COLUMN.MOBILE_NO.toString(), buyerModel.getBuyerMobileNo())
				.addValue(BUYER_DETAILS_COLUMN.PAN.toString(), buyerModel.getPan())
				.addValue(BUYER_DETAILS_COLUMN.CITY.toString(), buyerModel.getCity())
				.addValue(BUYER_DETAILS_COLUMN.ADDRESS.toString(), buyerModel.getAddress())
				.addValue(BUYER_DETAILS_COLUMN.ADHAR_NO.toString(), buyerModel.getAdharNo())
				.addValue(BUYER_DETAILS_COLUMN.STATE_ID.toString(), buyerModel.getStateId())
				.addValue(BUYER_DETAILS_COLUMN.LEDGER_ID.toString(), buyerModel.getLedgerId());
		
		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		buyerModel.setBuyerId(holder.getKey().intValue());
		return buyerModel;
	}
	
	
	//23-05-2019 Start
	List<BuyerInfoModel> getBuyerInfoByledgerId(int ledgerId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
//		sqlQueryToFetchData.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
//						.append(String.format(" ID.%s, ID.%s, ID.%s ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
//						.append(String.format(" FROM %s ID ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS))
//						.append(String.format(" WHERE ID.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", ITEM_DETAILS_COLUMN.IS_DELETED, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME))
//						.append(String.format(" AND ID.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, ITEM_DETAILS_COLUMN.COMPANY_ID))
//						.append(String.format(" ORDER BY ID.%s ASC LIMIT :start, :noOfRecord ", ITEM_DETAILS_COLUMN.ITEM_NAME));

		sqlQueryToFetchData.append(String.format(" SELECT * FROM %s ", DATABASE_TABLE.ACC_BUYERS_DETAILS))
							.append(String.format(" WHERE %s = "+ledgerId+" ", BUYER_DETAILS_COLUMN.LEDGER_ID,BUYER_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" ORDER BY %s DESC ", BUYER_DETAILS_COLUMN.BUYER_ID));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(BUYER_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
		
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getBuyerInfoExtractor());
	}

	private ResultSetExtractor<List<BuyerInfoModel>> getBuyerInfoExtractor() {

		return new ResultSetExtractor<List<BuyerInfoModel>>() {
			@Override
			public List<BuyerInfoModel> extractData(ResultSet rs) throws SQLException {
				List<BuyerInfoModel> buyerInfoList = new ArrayList<>();
				//ItemModel itemModel;
				while (rs.next()) {
					buyerInfoList.add(new BuyerInfoModel(rs));
				}
				return buyerInfoList;
			}
		};
	}
	
	
	
	
	BuyerInfoModel updateBuyerInfoData(BuyerInfoModel buyerInfoModel) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_BUYERS_DETAILS))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.BUYER_NAME, BUYER_DETAILS_COLUMN.BUYER_NAME))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.ADDRESS, BUYER_DETAILS_COLUMN.ADDRESS))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.CITY, BUYER_DETAILS_COLUMN.CITY))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.PAN, BUYER_DETAILS_COLUMN.PAN))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.MOBILE_NO, BUYER_DETAILS_COLUMN.MOBILE_NO))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.ADHAR_NO, BUYER_DETAILS_COLUMN.ADHAR_NO))
							.append(String.format(" %s = :%s, ", BUYER_DETAILS_COLUMN.STATE_ID, BUYER_DETAILS_COLUMN.STATE_ID))
							.append(String.format(" %s = :%s ", BUYER_DETAILS_COLUMN.LEDGER_ID, BUYER_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" WHERE %s = :%s  ", BUYER_DETAILS_COLUMN.BUYER_ID, BUYER_DETAILS_COLUMN.BUYER_ID));
							

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(BUYER_DETAILS_COLUMN.BUYER_NAME.toString(), buyerInfoModel.getBuyerName())
				.addValue(BUYER_DETAILS_COLUMN.ADDRESS.toString(), buyerInfoModel.getAddress())
				.addValue(BUYER_DETAILS_COLUMN.CITY.toString(), buyerInfoModel.getCity())
				.addValue(BUYER_DETAILS_COLUMN.PAN.toString(), buyerInfoModel.getPan())
				.addValue(BUYER_DETAILS_COLUMN.MOBILE_NO.toString(), buyerInfoModel.getBuyerMobileNo())
				.addValue(BUYER_DETAILS_COLUMN.ADHAR_NO.toString(), buyerInfoModel.getAdharNo())
				.addValue(BUYER_DETAILS_COLUMN.STATE_ID.toString(), buyerInfoModel.getStateId())
				.addValue(BUYER_DETAILS_COLUMN.LEDGER_ID.toString(), buyerInfoModel.getLedgerId())
				.addValue(BUYER_DETAILS_COLUMN.BUYER_ID.toString(), buyerInfoModel.getBuyerId());

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return buyerInfoModel;
	}
	
	
	int deleteLOneBuyerInfo(int buyerId) {


		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" DELETE FROM %s ", DATABASE_TABLE.ACC_BUYERS_DETAILS))
							.append(String.format(" WHERE %s = :%s  ", BUYER_DETAILS_COLUMN.BUYER_ID, BUYER_DETAILS_COLUMN.BUYER_ID));
							

		SqlParameterSource parameters = new MapSqlParameterSource()
				
				.addValue(BUYER_DETAILS_COLUMN.BUYER_ID.toString(),buyerId);

		//namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
	
	// 23-05-2019 End
}
