package acc.webservice.components.buyer;

import java.util.HashMap;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.BUYER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_ALIAS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/buyerData/")
public class BuyerInfoModelController {
	
	@Autowired
	BuyerInfoModelService buyerInfoModelService;
	
	@RequestMapping(value="saveBuyerData", method=RequestMethod.POST)
	public Map<String, Object> saveBuyer(@RequestBody BuyerInfoModel buyerInfoModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", buyerInfoModelService.saveBuyerInfoData(buyerInfoModel));
		return response;
	}
	
	// 23-05-2019 Start
	@RequestMapping(value="getBuyerInfoByledgerId", method=RequestMethod.POST)
	public Map<String, Object> getBuyerInfoByledgerId(@RequestBody Map<String, Object> params) {
		int ledgerId = ApplicationUtility.getIntValue(params, BUYER_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", buyerInfoModelService.getBuyerInfoByledgerId(ledgerId));
		return responseData;
	}
	
	@RequestMapping(value="updateBuyerInfoData", method=RequestMethod.POST)
	public Map<String, Object> updateBuyerInfoData(@RequestBody BuyerInfoModel buyerInfoModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", buyerInfoModelService.updateBuyerInfoData(buyerInfoModel));
		return response;
	}
	
	@RequestMapping(value = "deleteLOneBuyerInfo", method = RequestMethod.POST)
	public Map<String, Object> deleteLOneBuyerInfo(@RequestBody Map<String, Object> params) {
		int buyerId = ApplicationUtility.getIntValue(params, BUYER_DETAILS_COLUMN.BUYER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", buyerInfoModelService.deleteLOneBuyerInfo(buyerId));
		return responseData;
	}
	// 23-05-2019 End
	
}
