package acc.webservice.components.utills;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ResponseListTemplate {
	
	private List<Errorclass> error=null;
	private List<?> Result=null;
	
	
	public List<Errorclass> getError() {
		return error;
	}
	public void setError(List<Errorclass> error) {
		this.error = error;
	}
	public List<?> getResult() {
		return Result;
	}
	public void setResult(List<?> result) {
		Result = result;
	}
	
	

}
