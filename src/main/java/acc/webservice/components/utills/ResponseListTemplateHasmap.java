package acc.webservice.components.utills;



import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ResponseListTemplateHasmap {
	
	private List<Errorclass> error=null;
	private HashMap<? , ?> Result=null;
	public List<Errorclass> getError() {
		return error;
	}
	public void setError(List<Errorclass> error) {
		this.error = error;
	}
	public HashMap<?, ?> getResult() {
		return Result;
	}
	public void setResult(HashMap<?, ?> result) {
		Result = result;
	}
	
		

}
