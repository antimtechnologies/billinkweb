package acc.webservice.components.utills;

import org.springframework.stereotype.Service;

@Service
public class Errorclass {
	 
	private int errorcode;
	private String errorstatus;
	
	
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public String getErrorstatus() {
		return errorstatus;
	}
	public void setErrorstatus(String errorstatus) {
		this.errorstatus = errorstatus;
	}
	       
	   
}
 