package acc.webservice.components.status;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusUtil {

	@Autowired
	private StatusDAO statusDAO;

	public String getStatusNameById(int statusId) {
		return statusDAO.getStatusNameById(statusId);
	}

	public int getStatusIdByName(String statusName) {
		return statusDAO.getStatusIdByName(statusName);
	}


	public List<Integer> getStatusIdList(List<String> statusList) {
		List<Integer> statusIdList = new ArrayList<>();
		for (String statusName : statusList) {
			statusIdList.add(getStatusIdByName(statusName));
		}
		return statusIdList;
	}
}
