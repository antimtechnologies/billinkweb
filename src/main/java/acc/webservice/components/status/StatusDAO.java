package acc.webservice.components.status;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;

@Repository
public class StatusDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static Map<String, Integer> statusNameIdMap = new HashMap<>();
	private static Map<Integer, String> statusIdNameMap = new HashMap<>();

	@PostConstruct
	private void inializeStatusMap() {
		String sqlQuery = String.format(" SELECT %s, %s FROM %s WHERE %s = 0 ", STATUS_TABLE_COLUMN.STATUS_ID,
				STATUS_TABLE_COLUMN.STATUS_NAME, DATABASE_TABLE.ACC_STATUS_MASTER, STATUS_TABLE_COLUMN.IS_DELETED);
		namedParameterJdbcTemplate.query(sqlQuery, getStatusResultSetExtractor());
	}

	private ResultSetExtractor<Map<String, Integer>> getStatusResultSetExtractor() {
		return new ResultSetExtractor<Map<String, Integer>>() {
			@Override
			public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
				int statusId;
				String statusName;
				while (rs.next()) {
					statusId = rs.getInt(STATUS_TABLE_COLUMN.STATUS_ID.toString());
					statusName = rs.getString(STATUS_TABLE_COLUMN.STATUS_NAME.toString());
					statusIdNameMap.put(statusId, statusName);
					statusNameIdMap.put(statusName, statusId);
				}
				return statusNameIdMap;
			}
		};
	}

	int getStatusIdByName(String statusName) {
		return statusNameIdMap.get(statusName);
	}

	String getStatusNameById(int statusId) {
		return statusIdNameMap.get(statusId);
	}
}
