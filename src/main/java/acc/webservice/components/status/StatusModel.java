package acc.webservice.components.status;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;

public class StatusModel {

	private int statusId;
	private String statusName;
	private boolean isDeleted;

	public StatusModel() {
	}

	public StatusModel(ResultSet rs) throws SQLException {
		setStatusId(rs.getInt(STATUS_TABLE_COLUMN.STATUS_ID.toString()));
		setStatusName(rs.getString(STATUS_TABLE_COLUMN.STATUS_NAME.toString()));
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
