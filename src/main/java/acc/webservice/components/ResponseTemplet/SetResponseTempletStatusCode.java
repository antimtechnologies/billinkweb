package acc.webservice.components.ResponseTemplet;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.utills.Errorclass;

@Service
public class SetResponseTempletStatusCode {
	
	List<Errorclass> errorlist;
	@Autowired
	Errorclass errorstatus;
	
	public List<Errorclass> FailsResponse(String errormsg) {
		errorlist  = new ArrayList<Errorclass>();
		errorstatus.setErrorcode(0);
		errorstatus.setErrorstatus(errormsg);
		errorlist.add(errorstatus);
		return errorlist;
		
	}
	
	public List<Errorclass> SucessResponse(String Errormsg,int errorCode) {
		errorlist = new ArrayList<Errorclass>();
		errorstatus.setErrorcode(errorCode);
		errorstatus.setErrorstatus(Errormsg);
		errorlist.add(errorstatus);
		return errorlist;
		
	}
}
