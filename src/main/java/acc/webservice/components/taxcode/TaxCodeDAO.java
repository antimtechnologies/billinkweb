package acc.webservice.components.taxcode;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.TAX_CODE_COLUMN;

@Repository
@Lazy
public class TaxCodeDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static final Logger logger = Logger.getLogger(TaxCodeDAO.class);

	List<TaxCodeModel> getTaxCodeList() {
		StringBuilder sqlQuery = new StringBuilder();
		logger.debug("TaxCodeDAO.getTaxCodeList() -- method called");
		sqlQuery.append(String.format("SELECT %s, %s ", TAX_CODE_COLUMN.TAX_CODE_ID, TAX_CODE_COLUMN.TAX_CODE))
				.append(String.format(" FROM %s WHERE %s = 0 ", DATABASE_TABLE.ACC_TAX_CODE_DETAILS, TAX_CODE_COLUMN.IS_DELETED))
				.append(String.format(" ORDER BY %s ASC ", TAX_CODE_COLUMN.TAX_CODE));
		logger.debug(" sql query to fetch : " + sqlQuery ); 
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), getTaxCodeModelResultSetExctractor());
	}
	
	List<Double> getTaxCodeValueList() {
		StringBuilder sqlQuery = new StringBuilder();
		logger.debug("TaxCodeDAO.getTaxCodeValueList() -- method called");

		sqlQuery.append(String.format("SELECT %s, %s ", TAX_CODE_COLUMN.TAX_CODE_ID, TAX_CODE_COLUMN.TAX_CODE))
				.append(String.format(" FROM %s WHERE %s = 0 ", DATABASE_TABLE.ACC_TAX_CODE_DETAILS, TAX_CODE_COLUMN.IS_DELETED))
				.append(String.format(" ORDER BY %s ASC ", TAX_CODE_COLUMN.TAX_CODE));

		logger.debug(" sql query to fetch : " + sqlQuery ); 
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), getTaxCodeValueResultSetExctractor());
	}

	private ResultSetExtractor<List<Double>> getTaxCodeValueResultSetExctractor() {
		logger.debug("TaxCodeDAO.getTaxCodeValueResultSetExctractor() -- method called");
		return new ResultSetExtractor<List<Double>>() {
			@Override
			public List<Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Double> taxCodeList = new ArrayList<>();
				while (rs.next()) {
					taxCodeList.add(rs.getDouble(TAX_CODE_COLUMN.TAX_CODE.toString()));
				}
				logger.debug("tax code list : " + taxCodeList);
				logger.debug("TaxCodeDAO.getTaxCodeValueResultSetExctractor() -- method end");
				return taxCodeList;
			}
		};
	}

	private ResultSetExtractor<List<TaxCodeModel>> getTaxCodeModelResultSetExctractor() {
		logger.debug("TaxCodeDAO.getTaxCodeModelResultSetExctractor() -- method called");

		return new ResultSetExtractor<List<TaxCodeModel>>() {
			@Override
			public List<TaxCodeModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<TaxCodeModel> taxCodeList = new ArrayList<>();
				while (rs.next()) {
					taxCodeList.add(new TaxCodeModel(rs));
				}
				logger.debug("tax code list : " + taxCodeList);
				logger.debug("TaxCodeDAO.getTaxCodeModelResultSetExctractor() -- method end");
				return taxCodeList;
			}
		};
	}

	public TaxCodeModel saveTaxCodeList(TaxCodeModel taxCodeModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_TAX_CODE_DETAILS))
				.append(String.format(" ( %s ) VALUES ( :%s ) ", TAX_CODE_COLUMN.TAX_CODE, TAX_CODE_COLUMN.TAX_CODE));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(TAX_CODE_COLUMN.TAX_CODE.toString(), taxCodeModel.getTaxCode());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		taxCodeModel.setTaxCodeId(holder.getKey().intValue());
		return taxCodeModel;
	}


	public int deleteTaxCodeList(int taxCodeId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s ", DATABASE_TABLE.ACC_TAX_CODE_DETAILS))
				.append(String.format(" SET %s = 1 ", TAX_CODE_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = :%s ", TAX_CODE_COLUMN.TAX_CODE_ID, TAX_CODE_COLUMN.TAX_CODE_ID));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(TAX_CODE_COLUMN.TAX_CODE_ID.toString(), taxCodeId);
		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

}
