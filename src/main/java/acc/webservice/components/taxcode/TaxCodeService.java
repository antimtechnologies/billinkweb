package acc.webservice.components.taxcode;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class TaxCodeService {

	@Autowired
	private TaxCodeDAO taxCodeDAO;

	private static final Logger logger = Logger.getLogger(TaxCodeService.class);

	public List<TaxCodeModel> getTaxCodeList() {
		logger.debug("TaxCodeService.getTaxCodeList() -- method called");
		return taxCodeDAO.getTaxCodeList();
	}
	
	public List<Double> getTaxCodeValueList() {
		return taxCodeDAO.getTaxCodeValueList();
	}

	public TaxCodeModel saveTaxCodeList(TaxCodeModel taxCodeModel) {
		return taxCodeDAO.saveTaxCodeList(taxCodeModel);
	}

	public int deleteTaxCodeList(int taxCodeId) {
		return taxCodeDAO.deleteTaxCodeList(taxCodeId);
	}

}
