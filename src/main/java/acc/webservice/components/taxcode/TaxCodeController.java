package acc.webservice.components.taxcode;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/taxcodedata/")
public class TaxCodeController {

	@Autowired
	private TaxCodeService taxCodeService;

	private static final Logger logger = Logger.getLogger(TaxCodeController.class);

	@RequestMapping(value="getTaxCodeList", method=RequestMethod.POST)
	public Map<String,Object> getTaxCodeList(@RequestBody Map<String, Object> parameter) {
		logger.debug("TaxCodeController.getTaxCodeList() -- method called");
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", taxCodeService.getTaxCodeList());
		return response;
	}

	@RequestMapping(value="getTaxCodeValueList", method=RequestMethod.POST)
	public Map<String,Object> getTaxCodeValueList(@RequestBody Map<String, Object> parameter) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", taxCodeService.getTaxCodeValueList());
		return response;
	}

	@RequestMapping(value="saveTaxCodeData", method=RequestMethod.POST)
	public Map<String,Object> saveTaxCodeList(@RequestBody TaxCodeModel taxCodeModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", taxCodeService.saveTaxCodeList(taxCodeModel));
		return response;
	}
	
	@RequestMapping(value="deleteTaxCodeData", method=RequestMethod.POST)
	public Map<String,Object> deleteTaxCodeList(@RequestBody TaxCodeModel taxCodeModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", taxCodeService.deleteTaxCodeList(taxCodeModel.getTaxCodeId()));
		return response;
	}

}
