package acc.webservice.components.taxcode;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.TAX_CODE_COLUMN;

public class TaxCodeModel {

	private double taxCode;
	private boolean isDeleted;
	private int taxCodeId;

	public TaxCodeModel() {
		
	}

	public TaxCodeModel(ResultSet rs) throws SQLException {
		setTaxCode(rs.getDouble(TAX_CODE_COLUMN.TAX_CODE.toString()));
		setTaxCodeId(rs.getInt(TAX_CODE_COLUMN.TAX_CODE_ID.toString()));
	}

	public int getTaxCodeId() {
		return taxCodeId;
	}

	public void setTaxCodeId(int taxCodeId) {
		this.taxCodeId = taxCodeId;
	}

	public double getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(double taxCode) {
		this.taxCode = taxCode;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "TaxCodeModel [taxCode=" + taxCode + "]";
	}

}
