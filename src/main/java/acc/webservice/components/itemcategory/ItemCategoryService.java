package acc.webservice.components.itemcategory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class ItemCategoryService {

	@Autowired
	private ItemCategoryDAO itemCategoryDAO;

	public List<ItemCategoryModel> getItemCategoryList(int companyId, int start, int noOfRecord, String categoryName) {
		return itemCategoryDAO.getItemCategoryList(companyId, start, noOfRecord, categoryName);
	}

	public ItemCategoryModel saveItemCategoryList(ItemCategoryModel itemCategoryModel) {
		return itemCategoryDAO.saveItemCategoryList(itemCategoryModel);
	}

	public ItemCategoryModel updateItemCategoryList(ItemCategoryModel itemCategoryModel) {
		return itemCategoryDAO.updateItemCategoryList(itemCategoryModel);
	}

	public int deleteItemCategoryList(int categoryId) {
		return itemCategoryDAO.deleteItemCategoryList(categoryId);
	}

}
