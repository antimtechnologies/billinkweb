package acc.webservice.components.itemcategory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_CATEGORY_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/categorydata/")
public class ItemCategoryController {

	@Autowired
	private ItemCategoryService itemCategoryService;

	@RequestMapping(value = "getItemCategoryList", method = RequestMethod.POST)
	public Map<String,Object> getItemCategoryList(@RequestBody Map<String, Object> requestData) {
		int companyId = ApplicationUtility.getIntValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String categoryName = ApplicationUtility.getStrValue(requestData, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME.toString());
		return Collections.singletonMap("data",itemCategoryService.getItemCategoryList(companyId, start, noOfRecord, categoryName));
	}

	@RequestMapping(value="saveItemCategoryData", method=RequestMethod.POST)
	public Map<String,Object> saveItemCategoryList(@RequestBody ItemCategoryModel itemCategoryModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemCategoryService.saveItemCategoryList(itemCategoryModel));
		return response;
	}

	@RequestMapping(value="updateItemCategoryData", method=RequestMethod.POST)
	public Map<String,Object> updateItemCategoryList(@RequestBody ItemCategoryModel itemCategoryModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemCategoryService.updateItemCategoryList(itemCategoryModel));
		return response;
	}
	
	@RequestMapping(value="deleteItemCategoryData", method=RequestMethod.POST)
	public Map<String,Object> deleteItemCategoryList(@RequestBody ItemCategoryModel itemCategoryModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", itemCategoryService.deleteItemCategoryList(itemCategoryModel.getCategoryId()));
		return response;
	}

}
