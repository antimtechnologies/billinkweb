package acc.webservice.components.itemcategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.ACC_CATEGORY_MASTER_COLUMN;

public class ItemCategoryModel {

	private int categoryId;
	private String categoryName;
	private boolean isDeleted;
	private int companyId;

	public ItemCategoryModel() {
	}

	public ItemCategoryModel(ResultSet rs) throws SQLException {
		setCategoryId(rs.getInt(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID.toString()));
		setCategoryName(rs.getString(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME.toString()));
		
	}
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "ItemCategoryModel [categoryId=" + categoryId + ", categoryName=" + categoryName + ", isDeleted="
				+ isDeleted + "]";
	}

}
