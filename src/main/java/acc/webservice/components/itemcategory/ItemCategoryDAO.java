package acc.webservice.components.itemcategory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_CATEGORY_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;

@Repository
@Lazy
public class ItemCategoryDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<ItemCategoryModel> getItemCategoryList(int companyId, int start, int noOfRecord, String categoryName) {
		
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT %s, %s", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME))
				.append(String.format(" FROM %s WHERE %s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER, ACC_CATEGORY_MASTER_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID, ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID))
				.append(String.format(" AND ( %s LIKE :%s OR '%%' = :%s ) ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME))
				.append(String.format(" ORDER BY %s ASC ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME));

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(), APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();

		parameters.put(ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		parameters.put(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME.toString(), "%" + categoryName + "%");

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getItemCategoryResultSetExctrator());

	}

	private ResultSetExtractor<List<ItemCategoryModel>> getItemCategoryResultSetExctrator() {
		return new ResultSetExtractor<List<ItemCategoryModel>>() {

			@Override
			public List<ItemCategoryModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemCategoryModel> itemCategoryList = new ArrayList<>();
				while (rs.next()) {
					itemCategoryList.add(new ItemCategoryModel(rs));
				}
				return itemCategoryList;
			}
		};
	}

	public ItemCategoryModel saveItemCategoryList(ItemCategoryModel itemCategoryModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER))
				.append(String.format(" ( %s, %s ) VALUES ( :%s, :%s ) ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME,ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME,ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME.toString(), itemCategoryModel.getCategoryName())
						.addValue(ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID.toString(), itemCategoryModel.getCompanyId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		itemCategoryModel.setCategoryId(holder.getKey().intValue());
		return itemCategoryModel;
	}

	public ItemCategoryModel updateItemCategoryList(ItemCategoryModel itemCategoryModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER))
				.append(String.format(" %s = :%s ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME))
				.append(String.format(" WHERE %s = :%s ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID))
				.append(String.format(" AND %s = :%s ", ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID, ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID));

		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_NAME.toString(), itemCategoryModel.getCategoryName());
		parameters.put(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID.toString(), itemCategoryModel.getCategoryId());
		parameters.put(ACC_CATEGORY_MASTER_COLUMN.COMPANY_ID.toString(), itemCategoryModel.getCompanyId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return itemCategoryModel;
	}

	public int deleteItemCategoryList(int categoryId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" DELETE FROM %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_CATEGORY_MASTER))
				.append(String.format(" WHERE %s = :%s ", ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID, ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_CATEGORY_MASTER_COLUMN.CATEGORY_ID.toString(), categoryId);
		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

}
