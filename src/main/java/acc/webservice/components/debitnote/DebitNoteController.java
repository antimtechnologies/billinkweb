package acc.webservice.components.debitnote;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import acc.webservice.global.utils.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.components.users.UserService;
import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.debitnote.utilty.DebitNoteToCreditNoteUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/debitNoteData/")
public class DebitNoteController {

	@Autowired
	private DebitNoteService debitNoteService;

	@Autowired
	private DebitNoteToCreditNoteUtility debitNoteToCreditNoteUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getDebitNoteListData", method = RequestMethod.POST)
	public Map<String, Object> getDebitNoteListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data",
				debitNoteService.getDebitNoteListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getDebitNoteDetailsById", method = RequestMethod.POST)
	Map<String, Object> getDebitNoteDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int debitNoteId = ApplicationUtility.getIntValue(parameters,
				DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.getDebitNoteDetailsById(companyId, debitNoteId));
		return responseData;
	}

	@RequestMapping(value = "publishDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> publishDebitNoteData(@RequestBody DebitNoteModel debitNoteModel)
			throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.publishDebitNoteData(debitNoteModel));

		try {
			debitNoteToCreditNoteUtility.saveDebitNoteForSelectedLedger(debitNoteModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "saveDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> saveDebitNoteData(@RequestBody DebitNoteModel debitNoteModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.saveDebitNoteData(debitNoteModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedDebitNoteData(@RequestBody DebitNoteModel debitNoteModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				debitNoteModel.getDebitNoteDate())) {
			debitNoteModel.setDebitNoteDate(DateUtility.convertDateFormat(debitNoteModel.getDebitNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.updatePublishedDebitNoteData(debitNoteModel));

		return responseData;
	}

	@RequestMapping(value = "updateSavedDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedDebitNoteData(@RequestBody DebitNoteModel debitNoteModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				debitNoteModel.getDebitNoteDate())) {
			debitNoteModel.setDebitNoteDate(DateUtility.convertDateFormat(debitNoteModel.getDebitNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.updateSavedDebitNoteData(debitNoteModel));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishedDebitNoteData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int debitNoteId = ApplicationUtility.getIntValue(parameters,
				DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.deletePublishedDebitNoteData(companyId, debitNoteId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedDebitNoteData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedDebitNoteData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int debitNoteId = ApplicationUtility.getIntValue(parameters,
				DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.deletePublishedDebitNoteData(companyId, debitNoteId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "markDebitNoteAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markDebitNoteAsVerified(@RequestBody DebitNoteModel debitNoteModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.markDebitNoteAsVerified(debitNoteModel));
		return responseData;
	}

	@RequestMapping(value = "publishSavedDebitNote", method = RequestMethod.POST)
	public Map<String, Object> publishSavedDebitNote(@RequestBody DebitNoteModel debitNoteModel)
			throws AccountingSofwareException {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				debitNoteModel.getDebitNoteDate())) {
			debitNoteModel.setDebitNoteDate(DateUtility.convertDateFormat(debitNoteModel.getDebitNoteDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", debitNoteService.publishSavedDebitNote(debitNoteModel));

//		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteDate()) > 0) {
//			debitNoteModel.setDebitNoteDate(DateUtility.convertDateFormat(debitNoteModel.getDebitNoteDate(),
//					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
//		}

		try {
			debitNoteToCreditNoteUtility.saveDebitNoteForSelectedLedger(debitNoteModel);
			if (debitNoteModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
				debitNoteToCreditNoteUtility.saveDebitNoteForSelectedLedger(debitNoteModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "getdebitDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getDebitDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;

		if (request.getParameter("companyId") != null && request.getParameter("ledgerId") != null
				&& request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = "http://localhost:8090/user/debitnotecharge/finddebitnotedetails?companyId=" + companyId;
		} else if (request.getParameter("companyId") != null && request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			String billNumber = request.getParameter("billNumber");
			fooResourceUrl = "http://localhost:8090/user/debitnotecharge/finddebitnotedetails?companyId=" + companyId
					+ "&billNumber" + billNumber;
		} else if (request.getParameter("companyId") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = "http://localhost:8090/user/debitnotecharge/finddebitnotedetails?companyId=" + companyId;
		} else if (request.getParameter("billNumber") != null) {
			String billNumber = request.getParameter("companyId");
			fooResourceUrl = "http://localhost:8090/user/debitnotecharge/finddebitnotedetails?billNumber=" + billNumber;
		} else {
			fooResourceUrl = "http://localhost:8090/user/debitnotecharge/finddebitnotedetails";
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;

	}
}
