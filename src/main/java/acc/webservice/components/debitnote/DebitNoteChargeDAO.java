package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_CHARGE_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;

@Lazy
@Repository
public class DebitNoteChargeDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveDebitNoteChargeMappingData(List<DebitNoteChargeModel> debitNoteChargeModel, int companyId, int debitNoteId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_CHARGE_DATA))
				.append(String.format(" ( %s, %s, %s, %s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE, DEBIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, DEBIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" %s, %s, %s, %s ) ", DEBIT_NOTE_CHARGE_DATA_COLUMN.I_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.S_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.C_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE, DEBIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, DEBIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s ) ", DEBIT_NOTE_CHARGE_DATA_COLUMN.I_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.S_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.C_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> debitNoteFrightMappingList = new ArrayList<>();
		Map<String, Object> debitNoteFrightMap;
		for (DebitNoteChargeModel debitNoteCharge : debitNoteChargeModel) {

			if (debitNoteCharge.getLedger() == null || debitNoteCharge.getLedger().getLedgerId() <= 0) {
				continue;
			}

			debitNoteFrightMap = new HashMap<>();
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE.toString(), debitNoteCharge.getDebitNoteCharge());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID.toString(), debitNoteCharge.getLedger().getLedgerId());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString(), debitNoteCharge.getDiscount());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE.toString(), debitNoteCharge.getTaxRate());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), debitNoteCharge.getTotalItemAmount());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString(), debitNoteCharge.getItemAmount());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.I_GST.toString(), debitNoteCharge.getIgst());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.S_GST.toString(), debitNoteCharge.getSgst());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.C_GST.toString(), debitNoteCharge.getCgst());
			debitNoteFrightMap.put(DEBIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), debitNoteCharge.isMeasureDiscountInAmount());

			debitNoteFrightMappingList.add(debitNoteFrightMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), debitNoteFrightMappingList.toArray(new HashMap[0]));
	}

	int deleteDebitNoteChargeMapping(int companyId, int debitNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_CHARGE_DATA, DEBIT_NOTE_CHARGE_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, debitNoteId, companyId });
	}

	List<DebitNoteChargeModel> getDebitNoteChargeList(int companyId, int debitNoteId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		
		sqlQueryToFetchData.append(String.format("SELECT SFM.%s, SFM.%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE, DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE_MAPPING_ID))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE, DEBIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT, DEBIT_NOTE_CHARGE_DATA_COLUMN.I_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.S_GST))
			.append(String.format(" SFM.%s, SFM.%s, ", DEBIT_NOTE_CHARGE_DATA_COLUMN.C_GST, DEBIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
			.append(String.format(" LD.%s, LD.%s, LD.%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.SAC_CODE))
			.append(String.format(" FROM %s SFM ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_CHARGE_DATA))
			.append(String.format(" INNER JOIN %s LD ON SFM.%s = LD.%s AND LD.%s = 0", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, DEBIT_NOTE_CHARGE_DATA_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.IS_DELETED))
			.append(String.format(" AND LD.%s = ? ", LEDGER_DETAILS_COLUMN.COMPANY_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID))
			.append(String.format(" WHERE SFM.%s = ? AND SFM.%s = 0 ", DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.IS_DELETED))
			.append(String.format(" AND SFM.%s = ? ", DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID, DEBIT_NOTE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().query(sqlQueryToFetchData.toString(), new Object[] { companyId, debitNoteId, companyId }, getDebitNoteChargeDetailsExtractor());

	}

	private ResultSetExtractor<List<DebitNoteChargeModel>> getDebitNoteChargeDetailsExtractor() {
		return new ResultSetExtractor<List<DebitNoteChargeModel>>() {
			@Override
			public List<DebitNoteChargeModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<DebitNoteChargeModel> debitNoteChargeChargeList = new ArrayList<>();
				while (rs.next()) {
					debitNoteChargeChargeList.add(new DebitNoteChargeModel(rs));
				}
				return debitNoteChargeChargeList;
			}
		};
	}

}
