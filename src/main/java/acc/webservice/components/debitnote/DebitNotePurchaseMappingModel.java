package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_PURCHASE_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class DebitNotePurchaseMappingModel {

	private int debitNotePurchaseMappingId;
	private DebitNoteModel debitNote;
	private PurchaseModel purchase;
	private boolean isDeleted;

	public DebitNotePurchaseMappingModel() {
	}

	public DebitNotePurchaseMappingModel(ResultSet rs) throws SQLException {
		setDebitNotePurchaseMappingId(rs.getInt(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_PURCHASE_MAPPING_ID.toString()));

		PurchaseModel fetchedPurchaseData = new PurchaseModel();
		fetchedPurchaseData.setPurchaseId(rs.getInt(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString()));
		fetchedPurchaseData.setBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString()));
		fetchedPurchaseData.setTotalBillAmount(rs.getDouble(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));

		if (!ApplicationUtility.isNullEmpty(rs.getString(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString()))) {			
			fetchedPurchaseData.setImageURL(ApplicationUtility.getServerURL() + rs.getString(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString()));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		fetchedPurchaseData.setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedPurchaseDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString());
		if (fetchedPurchaseDate != null) {
			fetchedPurchaseData.setPurchaseDate(DateUtility.converDateToUserString(fetchedPurchaseDate));
		}
	
		setPurchase(fetchedPurchaseData);
	}

	public int getDebitNotePurchaseMappingId() {
		return debitNotePurchaseMappingId;
	}

	public void setDebitNotePurchaseMappingId(int debitNotePurchaseMappingId) {
		this.debitNotePurchaseMappingId = debitNotePurchaseMappingId;
	}

	public DebitNoteModel getDebitNote() {
		return debitNote;
	}

	public void setDebitNote(DebitNoteModel debitNote) {
		this.debitNote = debitNote;
	}

	public PurchaseModel getPurchase() {
		return purchase;
	}

	public void setPurchase(PurchaseModel purchase) {
		this.purchase = purchase;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
