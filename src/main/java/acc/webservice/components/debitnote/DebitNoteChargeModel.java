package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_CHARGE_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;

public class DebitNoteChargeModel {

	private int debitNoteChargeMappingId;
	private double debitNoteCharge;
	private boolean deleted;
	private LedgerModel ledger;
	private DebitNoteModel debitNote;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private boolean measureDiscountInAmount;

	public DebitNoteChargeModel() {
	}

	public DebitNoteChargeModel(ResultSet rs) throws SQLException {
		setDebitNoteChargeMappingId(rs.getInt(DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE_MAPPING_ID.toString()));
		setDebitNoteCharge(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.DEBIT_NOTE_CHARGE.toString()));
		setDiscount(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setTaxRate(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setItemAmount(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString()));
		setCgst(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.C_GST.toString()));
		setIgst(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(DEBIT_NOTE_CHARGE_DATA_COLUMN.S_GST.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(DEBIT_NOTE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		LedgerModel fetchedLedger = new LedgerModel();
		fetchedLedger.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedger.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedger.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));
		setLedger(fetchedLedger);
	}

	public int getDebitNoteChargeMappingId() {
		return debitNoteChargeMappingId;
	}

	public void setDebitNoteChargeMappingId(int debitNoteChargeMappingId) {
		this.debitNoteChargeMappingId = debitNoteChargeMappingId;
	}

	public DebitNoteModel getDebitNote() {
		return debitNote;
	}

	public void setDebitNote(DebitNoteModel debitNote) {
		this.debitNote = debitNote;
	}

	public double getDebitNoteCharge() {
		return debitNoteCharge;
	}

	public void setDebitNoteCharge(double debitNoteCharge) {
		this.debitNoteCharge = debitNoteCharge;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LedgerModel getLedger() {
		return ledger;
	}

	public void setLedger(LedgerModel ledger) {
		this.ledger = ledger;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	@Override
	public String toString() {
		return "SellFreightChargeModel [sellFreightMappingId=" + debitNoteChargeMappingId + ", freightCharge="
				+ debitNoteCharge + "]";
	}

}
