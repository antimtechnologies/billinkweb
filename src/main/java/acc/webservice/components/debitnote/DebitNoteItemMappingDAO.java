package acc.webservice.components.debitnote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_ITEM_MAPPING_COLUMN;

@Repository
@Lazy
public class DebitNoteItemMappingDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveDebitNoteItemMappingData(List<DebitNoteItemMappingModel> debitNoteItemMappingList, int companyId, int debitNoteId) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_ITEM_MAPPING))
				.append(String.format(" ( %s, %s, %s, %s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY, DEBIT_NOTE_ITEM_MAPPING_COLUMN.PURCHASE_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" %s, %s, %s, %s, %s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT, DEBIT_NOTE_ITEM_MAPPING_COLUMN.I_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.C_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY, DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID))
				.append(String.format(" %s, %s, %s, %s, %s, %s )", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT, DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT,DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,DEBIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY, DEBIT_NOTE_ITEM_MAPPING_COLUMN.PURCHASE_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT, DEBIT_NOTE_ITEM_MAPPING_COLUMN.I_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.C_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY, DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s, :%s) ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT, DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT,DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,DEBIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> debitNoteItemList = new ArrayList<>();
		Map<String, Object> debitNoteItemMap;
		for (DebitNoteItemMappingModel debitNoteItem : debitNoteItemMappingList) {

			if(debitNoteItem.getItem() == null || debitNoteItem.getItem().getItemId() <= 0) {
				continue;
			}

			debitNoteItemMap = new HashMap<>();
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID.toString(), debitNoteItem.getItem().getItemId());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY.toString(), debitNoteItem.getQuantity());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.PURCHASE_RATE.toString(), debitNoteItem.getPurchaseRate());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT.toString(), debitNoteItem.getDiscount());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE.toString(), debitNoteItem.getTaxRate());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT.toString(), debitNoteItem.getItemAmount());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.C_GST.toString(), debitNoteItem.getCgst());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.S_GST.toString(), debitNoteItem.getSgst());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.I_GST.toString(), debitNoteItem.getIgst());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT.toString(), debitNoteItem.getTotalItemAmount());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), debitNoteItem.isMeasureDiscountInAmount());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID.toString(), debitNoteItem.getLocationId());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID.toString(), debitNoteItem.getBatchId());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_QUANTITY.toString(), debitNoteItem.getFreequantity());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.FREE_ITEM_ID.toString(), debitNoteItem.getFreeItemId());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString(), debitNoteItem.getAdditionaldiscount());
			debitNoteItemMap.put(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString(), debitNoteItem.isAddmeasureDiscountInAmount());
			debitNoteItemList.add(debitNoteItemMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), debitNoteItemList.toArray(new HashMap[0]));
	}

	int deleteDebitNoteItemMapping(int companyId, int debitNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_ITEM_MAPPING, DEBIT_NOTE_ITEM_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, debitNoteId, companyId });
	}

}
