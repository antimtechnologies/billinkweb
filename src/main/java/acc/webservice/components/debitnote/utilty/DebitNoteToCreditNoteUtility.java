package acc.webservice.components.debitnote.utilty;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.creditnote.CreditNoteChargeModel;
import acc.webservice.components.creditnote.CreditNoteImageModel;
import acc.webservice.components.creditnote.CreditNoteItemMappingModel;
import acc.webservice.components.creditnote.CreditNoteSellMappingModel;
import acc.webservice.components.creditnote.CreditNoteService;
import acc.webservice.components.creditnote.model.CreditNoteModel;
import acc.webservice.components.debitnote.DebitNoteChargeModel;
import acc.webservice.components.debitnote.DebitNoteImageModel;
import acc.webservice.components.debitnote.DebitNoteItemMappingModel;
import acc.webservice.components.debitnote.DebitNotePurchaseMappingModel;
import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.location.LocationModel;
import acc.webservice.components.location.LocationModelService;
import acc.webservice.components.purchase.PurchaseService;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.purchase.utility.PurchaseToSellUtility;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.enums.DatabaseEnum.CREDIT_NOTE_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class DebitNoteToCreditNoteUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private CreditNoteService creditNoteService;

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private PurchaseToSellUtility purchaseToSellUtility;

	@Autowired
	private LocationModelService locationService;

	public CreditNoteModel saveDebitNoteForSelectedLedger(DebitNoteModel debitNoteModel)
			throws AccountingSofwareException {
		CreditNoteModel creditNoteModel = new CreditNoteModel();

		if (ApplicationUtility.getSize(debitNoteModel.getBillNumber()) < 1) {
			return creditNoteModel;
		}

		LedgerModel ledgerBasicDetails = ledgerService.getBasicLedgerDetailByLedgerId(
				debitNoteModel.getLedgerData().getLedgerId(), debitNoteModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());

			CompanyModel selectedLedgerCompany = companyService.getCompanyDetails(selectedLedgerCompanyId);

			if (selectedLedgerCompany.isAllowB2BImport()) {
				if (selectedLedgerCompanyId > 0) {

					creditNoteModel = creditNoteService.getCreditNoteDetailsByBillNumber(selectedLedgerCompanyId,
							debitNoteModel.getBillNumber());

					if (creditNoteModel.getCreditNoteId() > 0) {
						return creditNoteModel;
					}

					creditNoteModel = createCreditNoteModelFromCreditNoteModel(debitNoteModel, selectedLedgerCompany);
					creditNoteService.saveCreditNoteData(creditNoteModel);
				}
			}
		}
		return creditNoteModel;
	}

	private CreditNoteModel createCreditNoteModelFromCreditNoteModel(DebitNoteModel debitNoteModel,
			CompanyModel selectedLedgerCompany) throws AccountingSofwareException {
		CreditNoteModel creditNoteModel = new CreditNoteModel();
		creditNoteModel.setCompanyId(selectedLedgerCompany.getCompanyId());
		creditNoteModel.setCurrentBillNumber(selectedLedgerCompany.getLastCreditNoteNumber() + 1);
		creditNoteModel.setBillNumberPrefix(selectedLedgerCompany.getBillNumberPrefix());
		creditNoteModel.setBillNumber(
				"" + selectedLedgerCompany.getBillNumberPrefix() + creditNoteModel.getCurrentBillNumber());
		creditNoteModel.setReferenceNumber1(debitNoteModel.getBillNumber());
		creditNoteModel.setDiscount(debitNoteModel.getDiscount());
		creditNoteModel.setGrosssTotalAmount(debitNoteModel.getGrosssTotalAmount());
		creditNoteModel.setCreditNoteDate(debitNoteModel.getDebitNoteDate());
		creditNoteModel.setSellMeasureDiscountInAmount(debitNoteModel.isPurchaseMeasureDiscountInAmount());
		creditNoteModel.setAmount(debitNoteModel.getAmount());
		creditNoteModel.setImageURL(debitNoteModel.getImageURL());
		creditNoteModel.setDescription(debitNoteModel.getDescription());

		creditNoteModel.setCityName(debitNoteModel.getCityName());
		creditNoteModel.setGstNumber(debitNoteModel.getGstNumber());
		creditNoteModel.setPoNo(debitNoteModel.getPoNumber());
		creditNoteModel.setLrNo(debitNoteModel.getLrNumber());
		creditNoteModel.seteWayBillNo(debitNoteModel.getLrNumber());

		CompanyModel sellCompanyDetail = companyService.getCompanyDetails(debitNoteModel.getCompanyId());
		LedgerModel sellLedgerData = new LedgerModel();
		int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompany.getCompanyId(),
				sellCompanyDetail.getGstNumber());

		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompany.getCompanyId(), sellCompanyDetail);
		}
		sellLedgerData.setLedgerId(purchaseLedgerId);
		creditNoteModel.setLedgerData(sellLedgerData);

		List<CreditNoteImageModel> creditNoteImageURLList = new ArrayList<>();
		CreditNoteImageModel creditNoteImageModel;

		if (ApplicationUtility.getSize(debitNoteModel.getImageURLList()) > 0) {
			for (DebitNoteImageModel debitNoteImageModel : debitNoteModel.getImageURLList()) {
				creditNoteImageModel = new CreditNoteImageModel();
				creditNoteImageModel.setImageURL(debitNoteImageModel.getImageURL());
				creditNoteImageURLList.add(creditNoteImageModel);
			}
		}

		creditNoteModel.setImageURLList(creditNoteImageURLList);
		List<CreditNoteItemMappingModel> creditNoteItemList = new ArrayList<>();
		CreditNoteItemMappingModel creditNoteItemMappingModel;

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteItemMappingList()) > 0) {
			for (DebitNoteItemMappingModel debitNoteItemModel : debitNoteModel.getDebitNoteItemMappingList()) {
				if (debitNoteItemModel.getItem() == null || debitNoteItemModel.getItem().getItemId() <= 0) {
					continue;
				}
				creditNoteItemMappingModel = getCreditNoteItemModelFromDebitNoteItemModel(debitNoteItemModel,
						debitNoteModel.getCompanyId(), selectedLedgerCompany.getCompanyId());

				creditNoteItemList.add(creditNoteItemMappingModel);
			}
		}
		creditNoteModel.setCreditNoteItemMappingList(creditNoteItemList);

		List<CreditNoteChargeModel> creditNoteChargeList = new ArrayList<>();
		CreditNoteChargeModel creditNoteChargeModel;

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteChargeModelList()) > 0) {
			for (DebitNoteChargeModel debitNoteChargeModel : debitNoteModel.getDebitNoteChargeModelList()) {
				if (debitNoteChargeModel.getLedger() == null || debitNoteChargeModel.getLedger().getLedgerId() <= 0) {
					continue;
				}
				creditNoteChargeModel = getCreditNoteChargeModelFromDebitNoteChargeModel(debitNoteChargeModel,
						debitNoteModel.getCompanyId(), selectedLedgerCompany.getCompanyId());
				creditNoteChargeList.add(creditNoteChargeModel);
			}
		}
		creditNoteModel.setCreditNoteChargeModelList(creditNoteChargeList);

		List<DebitNotePurchaseMappingModel> debitNotePurchaseList = debitNoteModel.getDebitNotePurchaseMappingList();
		List<CreditNoteSellMappingModel> creditNoteSellList = new ArrayList<>();
		PurchaseModel purchaseModel;
		CreditNoteSellMappingModel creditNoteSellModel;
		SellModel sellModel;

		if (ApplicationUtility.getSize(debitNotePurchaseList) > 0) {
			for (DebitNotePurchaseMappingModel debitNotePurchaseModel : debitNotePurchaseList) {
				creditNoteSellModel = new CreditNoteSellMappingModel();
				purchaseModel = purchaseService.getPurchaseDetailsById(debitNoteModel.getCompanyId(),
						debitNotePurchaseModel.getPurchase().getPurchaseId());

				if (purchaseModel.getPurchaseDate() != null) {
					purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(),
							DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
				}

				purchaseModel.setCompanyId(debitNoteModel.getCompanyId());
				sellModel = purchaseToSellUtility.saveSellDataForSelectedLedger(purchaseModel);

				if (sellModel.getSellId() > 0) {
					creditNoteSellModel.setSell(sellModel);
					creditNoteSellList.add(creditNoteSellModel);
				}
			}
		}

		creditNoteModel.setCreditNoteSellMappingList(creditNoteSellList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		creditNoteModel.setAddedBy(userData);

		return creditNoteModel;
	}

	private CreditNoteChargeModel getCreditNoteChargeModelFromDebitNoteChargeModel(
			DebitNoteChargeModel debitNoteChargeModel1, int purchaseCompanyId, int sellCompanyId)
			throws AccountingSofwareException {
		CreditNoteChargeModel creditNoteChargeModel = new CreditNoteChargeModel();
		int ledgerId = ledgerService.getLedgerIdByName(sellCompanyId,
				debitNoteChargeModel1.getLedger().getLedgerName());

		if (ledgerId < 1) {
			LedgerModel ledgerModel = ledgerService.getLedgerDetailsById(purchaseCompanyId,
					debitNoteChargeModel1.getLedger().getLedgerId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			ledgerModel.setAddedBy(userData);
			ledgerModel.setLedgerDocumentList(null);
			ledgerModel.setOpeningBalance(0);
			ledgerModel.setCompanyId(sellCompanyId);
			ledgerId = ledgerService.saveLedgerData(ledgerModel).getLedgerId();
		}

		LedgerModel ledgerData = new LedgerModel();
		ledgerData.setLedgerId(ledgerId);

		creditNoteChargeModel.setLedger(ledgerData);
		creditNoteChargeModel.setCgst(debitNoteChargeModel1.getCgst());
		creditNoteChargeModel.setDiscount(debitNoteChargeModel1.getDiscount());
		creditNoteChargeModel.setIgst(debitNoteChargeModel1.getIgst());
		creditNoteChargeModel.setItemAmount(debitNoteChargeModel1.getItemAmount());
		creditNoteChargeModel.setMeasureDiscountInAmount(debitNoteChargeModel1.isMeasureDiscountInAmount());
		creditNoteChargeModel.setCreditNoteCharge(debitNoteChargeModel1.getDebitNoteCharge());
		creditNoteChargeModel.setSgst(debitNoteChargeModel1.getSgst());
		creditNoteChargeModel.setTaxRate(debitNoteChargeModel1.getTaxRate());
		creditNoteChargeModel.setTotalItemAmount(debitNoteChargeModel1.getTotalItemAmount());
		return creditNoteChargeModel;
	}

	private CreditNoteItemMappingModel getCreditNoteItemModelFromDebitNoteItemModel(
			DebitNoteItemMappingModel debitNoteItemModel, int purchaseCompanyId, int sellCompanyId) {
		CreditNoteItemMappingModel creditNoteItemModel = new CreditNoteItemMappingModel();
		int itemId = itemService.getItemIdByName(sellCompanyId, debitNoteItemModel.getItem().getItemName());

		if (itemId < 1) {
			ItemModel itemModel = itemService.getItemDetailsById(purchaseCompanyId,
					debitNoteItemModel.getItem().getItemId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			itemModel.setAddedBy(userData);
			itemModel.setCompanyId(sellCompanyId);
			itemModel.setItemDocumentList(null);
			itemModel.setQuantity(0);
			itemModel.setBrand(null);
			itemModel.setCategory(null);
			itemId = itemService.saveItemData(itemModel).getItemId();
		}

		LocationModel locationModel = locationService.getLocationByCompanyLocationId(purchaseCompanyId,
				debitNoteItemModel.getLocationId());
		if (locationModel != null) {
			LocationModel locationSellData = locationService.chekcklocationUniqueInsert(sellCompanyId, locationModel);
			creditNoteItemModel.setLocationId(locationSellData.getLocationId());
		}
		ItemBatchModel batchModel = itemService.getOneBatch(debitNoteItemModel.getBatchId());
		if (batchModel != null) {
			ItemBatchModel batchSellData = itemService.checkuniqueandInsertBatch(batchModel, itemId);
			creditNoteItemModel.setBatchId(batchSellData.getBatchId());
		}

		ItemModel itemData = new ItemModel();
		itemData.setItemId(itemId);

		creditNoteItemModel.setItem(itemData);
		creditNoteItemModel.setCgst(debitNoteItemModel.getCgst());
		creditNoteItemModel.setDiscount(debitNoteItemModel.getDiscount());
		creditNoteItemModel.setIgst(debitNoteItemModel.getIgst());
		creditNoteItemModel.setItemAmount(debitNoteItemModel.getItemAmount());
		creditNoteItemModel.setMeasureDiscountInAmount(debitNoteItemModel.isMeasureDiscountInAmount());
		creditNoteItemModel.setSellRate(debitNoteItemModel.getPurchaseRate());
		creditNoteItemModel.setQuantity(debitNoteItemModel.getQuantity());
		creditNoteItemModel.setSgst(debitNoteItemModel.getSgst());
		creditNoteItemModel.setTaxRate(debitNoteItemModel.getTaxRate());
		creditNoteItemModel.setTotalItemAmount(debitNoteItemModel.getTotalItemAmount());
		return creditNoteItemModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail)
			throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
