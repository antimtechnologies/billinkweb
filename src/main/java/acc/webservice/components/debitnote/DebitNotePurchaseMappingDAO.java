package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_PURCHASE_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;

@Repository
@Lazy
public class DebitNotePurchaseMappingDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveDebitNotePurchaseMappingData(List<DebitNotePurchaseMappingModel> debitNotePurchaseMappingList, int companyId, int debitNoteId) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_PURCHASE_MAPPING))
				.append(String.format(" ( %s, %s, %s ) ", DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.PURCHASE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s ) ", DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.PURCHASE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID));

		List<Map<String, Object>> debitNotePurchaseList = new ArrayList<>();
		Map<String, Object> debitNotePurchaseMap;
		for (DebitNotePurchaseMappingModel debitNotePurchaseMapping : debitNotePurchaseMappingList) {
			debitNotePurchaseMap = new HashMap<>();
			debitNotePurchaseMap.put(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
			debitNotePurchaseMap.put(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
			debitNotePurchaseMap.put(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.PURCHASE_ID.toString(), debitNotePurchaseMapping.getPurchase().getPurchaseId());
			debitNotePurchaseList.add(debitNotePurchaseMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), debitNotePurchaseList.toArray(new HashMap[0]));
	}

	int deleteDebitNotePurchaseMapping(int companyId, int debitNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_PURCHASE_MAPPING, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, debitNoteId, companyId });
	}

	List<DebitNotePurchaseMappingModel> getDebitNotePurchaseMappingListForDebitNote(int debitNoteId, int companyId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT CNSD.%s, PD.%s, PD.%s,  ", DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_PURCHASE_MAPPING_ID, PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s ", PURCHASE_DETAILS_COLUMN.IMAGE_URL, PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME, PURCHASE_DETAILS_COLUMN.PURCHASE_DATE, PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT))
				.append(String.format(" FROM %s CNSD ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_PURCHASE_MAPPING))
				.append(String.format(" INNER JOIN %s PD ON PD.%s = CNSD.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS, PURCHASE_DETAILS_COLUMN.PURCHASE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.PURCHASE_ID))
				.append(String.format(" AND PD.%s = :%s ", PURCHASE_DETAILS_COLUMN.COMPANY_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID))
				.append(String.format(" WHERE PD.%s = 0 AND CNSD.%s = 0 AND CNSD.%s = :%s", PURCHASE_DETAILS_COLUMN.IS_DELETED, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.IS_DELETED, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID))
				;

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
		parameters.put(DEBIT_NOTE_PURCHASE_MAPPING_COLUMN.COMPANY_ID.toString(), companyId);
 		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getDebitNotePurchaseDetailExctractor());
	}

	private ResultSetExtractor<List<DebitNotePurchaseMappingModel>> getDebitNotePurchaseDetailExctractor() {
		return new ResultSetExtractor<List<DebitNotePurchaseMappingModel>>() {
			@Override
			public List<DebitNotePurchaseMappingModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<DebitNotePurchaseMappingModel> debitNotePurchaseList = new ArrayList<>();
				while (rs.next()) {
					debitNotePurchaseList.add(new DebitNotePurchaseMappingModel(rs));
				}

				return debitNotePurchaseList;
			}
		};
	}
	
}
