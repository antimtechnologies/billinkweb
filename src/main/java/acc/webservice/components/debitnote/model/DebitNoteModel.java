package acc.webservice.components.debitnote.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.debitnote.DebitNoteChargeModel;
import acc.webservice.components.debitnote.DebitNoteImageModel;
import acc.webservice.components.debitnote.DebitNoteItemMappingModel;
import acc.webservice.components.debitnote.DebitNotePurchaseMappingModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class DebitNoteModel {

	private int debitNoteId;
	private String imageURL;
	private double amount;
	private double grosssTotalAmount;
	private String description;
	private boolean deleted;
	private boolean verified;
	private String debitNoteDate;
	private String addedDateTime;
	private String modifiedDateTime;
	private String verifiedDateTime;
	private String deletedDateTime;
	private LedgerModel ledgerData;
	private StatusModel status;
	private UserModel addedBy;
	private String billNumber;
	private UserModel modifiedBy;
	private UserModel verifiedBy;
	private UserModel deletedBy;
	private double discount;
	private boolean purchaseMeasureDiscountInAmount;
	private List<DebitNoteItemMappingModel> debitNoteItemMappingList;
	private List<DebitNotePurchaseMappingModel> debitNotePurchaseMappingList;
	private List<DebitNoteImageModel> imageURLList;
	private List<DebitNoteChargeModel> debitNoteChargeModelList;
	private String referenceNumber1;
	private String referenceNumber2;
	private String poNumber;
	private String lrNumber;
	private String ewayBillNumber;
	private String cityName;
	private String gstNumber;
	private String originalInvoiceNumber;
	private String originalInvoiceDate;
	private int companyId;
	private String billNumberPrefix;
	private int currentBillNumber;

	public DebitNoteModel() {
	}

	public DebitNoteModel(ResultSet rs) throws SQLException {
		setBillNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString()));
		setDebitNoteId(rs.getInt(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString()));
		setAmount(rs.getDouble(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString()));
		setGrosssTotalAmount(rs.getDouble(DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString()));
		setPurchaseMeasureDiscountInAmount(
				rs.getBoolean(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setReferenceNumber1(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));
		setBillNumberPrefix(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString()));
		setCurrentBillNumber(rs.getInt(DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString()));
		setPurchaseMeasureDiscountInAmount(
				rs.getBoolean(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setPoNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString()));
		setLrNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString()));
		setEwayBillNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString()));
		setCityName(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString()));
		setGstNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString()));
		setOriginalInvoiceNumber(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString()));
		Timestamp fetchedInvoiceDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString());
		if (fetchedInvoiceDate != null) {
			setOriginalInvoiceDate(DateUtility.converDateToUserString(fetchedInvoiceDate));
		}

		String imageURL = rs.getString(DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}

		setDescription(rs.getString(DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString()));
		setAmount(rs.getDouble(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString()));
		setVerified(rs.getBoolean(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString()));
		setDiscount(rs.getDouble(DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString()));
		setPurchaseMeasureDiscountInAmount(
				rs.getBoolean(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		Timestamp fetchedCreditNoteDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE.toString());
		if (fetchedCreditNoteDate != null) {
			setDebitNoteDate(DateUtility.converDateToUserString(fetchedCreditNoteDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerfiedDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerfiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerfiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		LedgerModel fetchedLedgerData = new LedgerModel();
		fetchedLedgerData.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedgerData.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedgerData.setState(new StateModel(rs));
		setLedgerData(fetchedLedgerData);

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		setStatus(new StatusModel(rs));
	}

	public int getDebitNoteId() {
		return debitNoteId;
	}

	public void setDebitNoteId(int debitNoteId) {
		this.debitNoteId = debitNoteId;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDebitNoteDate() {
		return debitNoteDate;
	}

	public void setDebitNoteDate(String debitNoteDate) {
		this.debitNoteDate = debitNoteDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<DebitNoteItemMappingModel> getDebitNoteItemMappingList() {
		return debitNoteItemMappingList;
	}

	public void setDebitNoteItemMappingList(List<DebitNoteItemMappingModel> debitNoteItemMappingList) {
		this.debitNoteItemMappingList = debitNoteItemMappingList;
	}

	public List<DebitNotePurchaseMappingModel> getDebitNotePurchaseMappingList() {
		return debitNotePurchaseMappingList;
	}

	public void setDebitNotePurchaseMappingList(List<DebitNotePurchaseMappingModel> debitNotePurchaseMappingList) {
		this.debitNotePurchaseMappingList = debitNotePurchaseMappingList;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public boolean isPurchaseMeasureDiscountInAmount() {
		return purchaseMeasureDiscountInAmount;
	}

	public void setPurchaseMeasureDiscountInAmount(boolean purchaseMeasureDiscountInAmount) {
		this.purchaseMeasureDiscountInAmount = purchaseMeasureDiscountInAmount;
	}

	public List<DebitNoteImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<DebitNoteImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public List<DebitNoteChargeModel> getDebitNoteChargeModelList() {
		return debitNoteChargeModelList;
	}

	public void setDebitNoteChargeModelList(List<DebitNoteChargeModel> debitNoteChargeModelList) {
		this.debitNoteChargeModelList = debitNoteChargeModelList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public double getGrosssTotalAmount() {
		return grosssTotalAmount;
	}

	public void setGrosssTotalAmount(double grosssTotalAmount) {
		this.grosssTotalAmount = grosssTotalAmount;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getLrNumber() {
		return lrNumber;
	}

	public void setLrNumber(String lrNumber) {
		this.lrNumber = lrNumber;
	}

	public String getEwayBillNumber() {
		return ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getOriginalInvoiceNumber() {
		return originalInvoiceNumber;
	}

	public void setOriginalInvoiceNumber(String originalInvoiceNumber) {
		this.originalInvoiceNumber = originalInvoiceNumber;
	}

	public String getOriginalInvoiceDate() {
		return originalInvoiceDate;
	}

	public void setOriginalInvoiceDate(String originalInvoiceDate) {
		this.originalInvoiceDate = originalInvoiceDate;
	}

	public String getBillNumberPrefix() {
		return billNumberPrefix;
	}

	public void setBillNumberPrefix(String billNumberPrefix) {
		this.billNumberPrefix = billNumberPrefix;
	}

	public int getCurrentBillNumber() {
		return currentBillNumber;
	}

	public void setCurrentBillNumber(int currentBillNumber) {
		this.currentBillNumber = currentBillNumber;
	}

	@Override
	public String toString() {
		return "DebitNoteModel [debitNoteId=" + debitNoteId + ", imageURL=" + imageURL + ", amount=" + amount
				+ ", grosssTotalAmount=" + grosssTotalAmount + ", description=" + description + ", deleted=" + deleted
				+ ", verified=" + verified + ", debitNoteDate=" + debitNoteDate + ", addedDateTime=" + addedDateTime
				+ ", modifiedDateTime=" + modifiedDateTime + ", verifiedDateTime=" + verifiedDateTime
				+ ", deletedDateTime=" + deletedDateTime + ", ledgerData=" + ledgerData + ", status=" + status
				+ ", addedBy=" + addedBy + ", billNumber=" + billNumber + ", modifiedBy=" + modifiedBy + ", verifiedBy="
				+ verifiedBy + ", deletedBy=" + deletedBy + ", discount=" + discount
				+ ", purchaseMeasureDiscountInAmount=" + purchaseMeasureDiscountInAmount + ", debitNoteItemMappingList="
				+ debitNoteItemMappingList + ", debitNotePurchaseMappingList=" + debitNotePurchaseMappingList
				+ ", imageURLList=" + imageURLList + ", debitNoteChargeModelList=" + debitNoteChargeModelList
				+ ", referenceNumber1=" + referenceNumber1 + ", referenceNumber2=" + referenceNumber2 + ", poNumber="
				+ poNumber + ", lrNumber=" + lrNumber + ", ewayBillNumber=" + ewayBillNumber + ", cityName=" + cityName
				+ ", gstNumber=" + gstNumber + ", originalInvoiceNumber=" + originalInvoiceNumber
				+ ", originalInvoiceDate=" + originalInvoiceDate + ", companyId=" + companyId + ", billNumberPrefix="
				+ billNumberPrefix + ", currentBillNumber=" + currentBillNumber + "]";
	}

	

}
