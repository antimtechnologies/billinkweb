package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class DebitNoteDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private DebitNotePurchaseMappingDAO debitNotePurchaseMappingDAO;

	@SuppressWarnings("unchecked")
	List<DebitNoteModel> getDebitNoteListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();
		sqlQueryToFetchData
				.append(String.format(" SELECT DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL, DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE,
						DEBIT_NOTE_DETAILS_COLUMN.AMOUNT, DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))

				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" DND.%s, DND.%s, DND.%s,  ", DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME, DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" DND.%s, DND.%s, ",
						DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" FROM %s DND ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" INNER JOIN %s SM ON DND.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON DND.%s = LM.%s AND LM.%s = :%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON DND.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s DUM ON DND.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s MUM ON DND.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON DND.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" WHERE DND.%s = 0 ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND DND.%s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {
					sqlQueryToFetchData.append(
							String.format(" AND DND.%s IN (:statusList) ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter,
						DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString());

				if (isVerified == 0 || isVerified == 1) {
					sqlQueryToFetchData.append(String.format(" AND DND.%s = :%s ",
							DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED, DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter,
						DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString());

				if (ledgerId > 0) {
					sqlQueryToFetchData.append(String.format(" AND DND.%s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
							DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString())) {
				String sellId = ApplicationUtility.getStrValue(filterParamter,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND (DND.%s LIKE :%s OR DND.%s LIKE :%s) ",
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID));
				parameters.put(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), "%" + sellId + "%");
			}

		}

		sqlQueryToFetchData
				.append(String.format(" ORDER BY DND.%s DESC LIMIT :%s, :%s ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						APPLICATION_GENERIC_ENUM.START, APPLICATION_GENERIC_ENUM.NO_OF_RECORD));
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getDebitNoteDetailsListExtractor());
	}

	private ResultSetExtractor<List<DebitNoteModel>> getDebitNoteDetailsListExtractor() {

		return new ResultSetExtractor<List<DebitNoteModel>>() {
			@Override
			public List<DebitNoteModel> extractData(ResultSet rs) throws SQLException {
				List<DebitNoteModel> debitList = new ArrayList<>();
				while (rs.next()) {
					debitList.add(new DebitNoteModel(rs));
				}
				return debitList;
			}
		};
	}

	DebitNoteModel getDebitNoteDetailsByBillNumber(int companyId, String billNumber) {
		StringBuilder sqlQueryToFetchData = getDebitNoteDetailsQuery();
		sqlQueryToFetchData.append(String.format(" AND DND.%s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
				DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), billNumber);
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getDebitNoteDetailsExtractor(companyId));
	}

	private StringBuilder getDebitNoteDetailsQuery() {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData
				.append(String.format(" SELECT DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL, DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE,
						DEBIT_NOTE_DETAILS_COLUMN.AMOUNT, DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME, DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" DND.%s, DND.%s, ",
						DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" DND.%s, DND.%s, DND.%s, DND.%s, ", DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" DND.%s, DND.%s, DND.%s,  ", DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" IM.%s, IM.%s, IM.%s, PIM.%s, PIM.%s, PIM.%s, ", ITEM_DETAILS_COLUMN.HSN_CODE,
						ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ITEM_MAPPING_ID,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" PIM.%s, PIM.%s, PIM.%s, PIM.%s, PIM.%s, PIM.%s, ",
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.PURCHASE_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE, DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID, DEBIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID))
				.append(String.format(" PIM.%s, PIM.%s, PIM.%s, PIM.%s, ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.C_GST,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.S_GST, DEBIT_NOTE_ITEM_MAPPING_COLUMN.I_GST,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT))
				.append(String.format(" PIM.%s, PIM.%s  ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" FROM %s DND ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" INNER JOIN %s SM ON DND.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON DND.%s = LM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON DND.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s DUM ON DND.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s MUM ON DND.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON DND.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s PIM ON PIM.%s = DND.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_ITEM_MAPPING,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" AND PIM.%s = 0 AND PIM.%s = :%s ", DEBIT_NOTE_ITEM_MAPPING_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID, DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" LEFT JOIN %s IM ON IM.%s = PIM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS, ITEM_DETAILS_COLUMN.ITEM_ID,
						DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ID))
				.append(String.format(" AND IM.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" WHERE DND.%s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND DND.%s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		return sqlQueryToFetchData;
	}

	DebitNoteModel getDebitNoteDetailsById(int companyId, int debitNoteId) {

		StringBuilder sqlQueryToFetchData = getDebitNoteDetailsQuery();
		sqlQueryToFetchData.append(String.format(" AND DND.%s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
				DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getDebitNoteDetailsExtractor(companyId));
	}

	private ResultSetExtractor<DebitNoteModel> getDebitNoteDetailsExtractor(int companyId) {

		return new ResultSetExtractor<DebitNoteModel>() {
			@Override
			public DebitNoteModel extractData(ResultSet rs) throws SQLException {
				DebitNoteModel debitDetails = new DebitNoteModel();
				List<DebitNoteItemMappingModel> debitItemMappingList = new ArrayList<>();
				while (rs.next()) {
					if (debitDetails.getDebitNoteId() < 1) {
						debitDetails = new DebitNoteModel(rs);
						debitDetails.setDebitNotePurchaseMappingList(debitNotePurchaseMappingDAO
								.getDebitNotePurchaseMappingListForDebitNote(debitDetails.getDebitNoteId(), companyId));
					}

					if (rs.getInt(DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ITEM_MAPPING_ID.toString()) > 0) {
						debitItemMappingList.add(new DebitNoteItemMappingModel(rs));
					}
				}
				debitDetails.setDebitNoteItemMappingList(debitItemMappingList);
				return debitDetails;
			}
		};
	}

	DebitNoteModel publishDebitNoteData(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = getDebitNoteInsertQuery(debitNoteModel);

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), debitNoteModel.getImageURL())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), debitNoteModel.getDescription())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), debitNoteModel.getBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), debitNoteModel.getBillNumberPrefix())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						debitNoteModel.getCurrentBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), debitNoteModel.getReferenceNumber1())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), debitNoteModel.getReferenceNumber2())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE.toString(), debitNoteModel.getDebitNoteDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), debitNoteModel.getLedgerData().getLedgerId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), debitNoteModel.getAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						debitNoteModel.getGrosssTotalAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						debitNoteModel.isPurchaseMeasureDiscountInAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), debitNoteModel.getDiscount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY.toString(), debitNoteModel.getAddedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString(), debitNoteModel.getPoNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString(), debitNoteModel.getLrNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), debitNoteModel.getEwayBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), debitNoteModel.getCityName())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), debitNoteModel.getGstNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString(),
						debitNoteModel.getOriginalInvoiceNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString(),
						debitNoteModel.getOriginalInvoiceDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		debitNoteModel.setDebitNoteId(holder.getKey().intValue());
		return debitNoteModel;
	}

	DebitNoteModel saveDebitNoteData(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = getDebitNoteInsertQuery(debitNoteModel);
		KeyHolder holder = new GeneratedKeyHolder();

		if (!ApplicationUtility.isNullEmpty(debitNoteModel.getImageURL())) {
			debitNoteModel.setImageURL(debitNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), debitNoteModel.getImageURL())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), debitNoteModel.getDescription())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), debitNoteModel.getBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), debitNoteModel.getBillNumberPrefix())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						debitNoteModel.getCurrentBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), debitNoteModel.getReferenceNumber1())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), debitNoteModel.getReferenceNumber2())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE.toString(), debitNoteModel.getDebitNoteDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), debitNoteModel.getLedgerData().getLedgerId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), debitNoteModel.getAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						debitNoteModel.getGrosssTotalAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						debitNoteModel.isPurchaseMeasureDiscountInAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), debitNoteModel.getDiscount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY.toString(), debitNoteModel.getAddedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString(), debitNoteModel.getPoNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString(), debitNoteModel.getLrNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), debitNoteModel.getEwayBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), debitNoteModel.getCityName())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), debitNoteModel.getGstNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString(),
						debitNoteModel.getOriginalInvoiceNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString(),
						debitNoteModel.getOriginalInvoiceDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		debitNoteModel.setDebitNoteId(holder.getKey().intValue());
		return debitNoteModel;
	}

	private StringBuilder getDebitNoteInsertQuery(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format("( %s, %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE, DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID, DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" %s, %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.AMOUNT, DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" %s, %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" %s, %s, %s, ", DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" %s, %s, %s, %s,", DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" %s, %s, %s )", DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME, DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(" VALUES ")
				.append(String.format("( :%s, :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE, DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID, DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" :%s, :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.AMOUNT, DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" :%s, :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2, DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" :%s, :%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" :%s, :%s,:%s, :%s, ", DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER, DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE))
				.append(String.format(" :%s, :%s, :%s )", DEBIT_NOTE_DETAILS_COLUMN.ADDED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.ADDED_DATE_TIME, DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED));
		return sqlQuery;
	}

	DebitNoteModel updatePublishedDebitNoteData(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = getDebitNoteUpdateQuery(debitNoteModel);

		if (!ApplicationUtility.isNullEmpty(debitNoteModel.getImageURL())) {
			debitNoteModel.setImageURL(debitNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), debitNoteModel.getImageURL())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), debitNoteModel.getDescription())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), debitNoteModel.getBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), debitNoteModel.getBillNumberPrefix())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						debitNoteModel.getCurrentBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), debitNoteModel.getReferenceNumber1())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), debitNoteModel.getReferenceNumber2())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE.toString(), debitNoteModel.getDebitNoteDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), debitNoteModel.getLedgerData().getLedgerId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						debitNoteModel.isPurchaseMeasureDiscountInAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), debitNoteModel.getDiscount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), debitNoteModel.getAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						debitNoteModel.getGrosssTotalAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY.toString(), debitNoteModel.getModifiedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteModel.getDebitNoteId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.P_O_NUMBER.toString(), debitNoteModel.getPoNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.L_R_NUMBER.toString(), debitNoteModel.getLrNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), debitNoteModel.getEwayBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), debitNoteModel.getCityName())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), debitNoteModel.getGstNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_NUMBER.toString(),
						debitNoteModel.getOriginalInvoiceNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.ORIGINAL_INVOICE_DATE.toString(),
						debitNoteModel.getOriginalInvoiceDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return debitNoteModel;
	}

	DebitNoteModel updateSavedDebitNoteData(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = getDebitNoteUpdateQuery(debitNoteModel);

		if (!ApplicationUtility.isNullEmpty(debitNoteModel.getImageURL())) {
			debitNoteModel.setImageURL(debitNoteModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL.toString(), debitNoteModel.getImageURL())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION.toString(), debitNoteModel.getDescription())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE.toString(), debitNoteModel.getDebitNoteDate())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER.toString(), debitNoteModel.getBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), debitNoteModel.getBillNumberPrefix())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(),
						debitNoteModel.getCurrentBillNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), debitNoteModel.getReferenceNumber1())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), debitNoteModel.getReferenceNumber2())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID.toString(), debitNoteModel.getLedgerData().getLedgerId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.AMOUNT.toString(), debitNoteModel.getAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(),
						debitNoteModel.getGrosssTotalAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT.toString(),
						debitNoteModel.isPurchaseMeasureDiscountInAmount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT.toString(), debitNoteModel.getDiscount())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME.toString(), debitNoteModel.getCityName())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER.toString(), debitNoteModel.getGstNumber())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY.toString(), debitNoteModel.getModifiedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteModel.getDebitNoteId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return debitNoteModel;
	}

	private StringBuilder getDebitNoteUpdateQuery(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL,
						DEBIT_NOTE_DETAILS_COLUMN.IMAGE_URL))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION,
						DEBIT_NOTE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_1))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2,
						DEBIT_NOTE_DETAILS_COLUMN.REFERENCE_NUBER_2))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_DATE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID,
						DEBIT_NOTE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.AMOUNT))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT,
						DEBIT_NOTE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME,
						DEBIT_NOTE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_BY))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER,
						DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX,
						DEBIT_NOTE_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.MODIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED));
		return sqlQuery;
	}

	DebitNoteModel markDebitNoteAsVerified(DebitNoteModel debitNoteModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY.toString(), debitNoteModel.getVerifiedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteModel.getDebitNoteId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		debitNoteModel.setVerified(true);
		debitNoteModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		return debitNoteModel;
	}

	int deletePublishedDebitNoteData(int companyId, int debitNoteId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int markDebitNoteAsDeleteAndVerified(DebitNoteModel debitNoteModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_BY.toString(), debitNoteModel.getVerifiedBy().getUserId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteModel.getDebitNoteId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	int deleteSavedDebitNoteData(int companyId, int debitNoteId, int deletedBy) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME,
						DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	DebitNoteModel publishSavedDebitNote(DebitNoteModel debitNoteModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format(" %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" WHERE %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID,
						DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID))
				.append(String.format(" AND %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED,
						DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteModel.getDebitNoteId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), debitNoteModel.getCompanyId())
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		debitNoteModel.setStatus(status);
		return debitNoteModel;
	}

	int getPendingDebitNoteCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ",
				DEBIT_NOTE_DETAILS_COLUMN.DEBIT_NOTE_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format("WHERE %s != :%s AND %s = 0 ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID, DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", DEBIT_NOTE_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getDebitNotePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getDebitNotePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

	int getMaxBillNumber(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT MAX(%s) AS maxBillNumber FROM %s ",
				DEBIT_NOTE_DETAILS_COLUMN.CURRENT_BILL_NUMBER, COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE))
				.append(String.format("WHERE %s = 0 ", DEBIT_NOTE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s  ", DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID,
						DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s != :%s  ", DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID,
						DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(DEBIT_NOTE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()));

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getMaxBillNumberExctractor());
	}

	private ResultSetExtractor<Integer> getMaxBillNumberExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int maxBillNumber = 0;
				while (rs.next()) {
					maxBillNumber = rs.getInt("maxBillNumber");
				}
				return maxBillNumber;
			}
		};
	}

}
