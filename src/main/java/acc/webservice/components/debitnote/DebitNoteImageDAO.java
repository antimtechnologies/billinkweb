package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class DebitNoteImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<DebitNoteImageModel> getDebitNoteImageList(int companyId, int debitNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.IS_DELETED));
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getExpenseImageResultExctractor());
	}

	private ResultSetExtractor<List<DebitNoteImageModel>> getExpenseImageResultExctractor() {
		return new ResultSetExtractor<List<DebitNoteImageModel>>() {
			@Override
			public List<DebitNoteImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<DebitNoteImageModel> expenseImageModels = new ArrayList<>();
				while (rs.next()) {
					expenseImageModels.add(new DebitNoteImageModel(rs));
				}
				return expenseImageModels;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveDebitNoteImageURLData(List<DebitNoteImageModel> debitNoteImageModelList, int companyId, int debitNoteId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s ) ", DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s ) ", DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL, DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> debitNoteImageURLMapList = new ArrayList<>();
		Map<String, Object> debitNoteImageURLMap;
		for (DebitNoteImageModel debitNoteImageModel : debitNoteImageModelList) {
			debitNoteImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(debitNoteImageModel.getImageURL())) {			
				debitNoteImageModel.setImageURL(debitNoteImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			debitNoteImageURLMap.put(DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL.toString(), debitNoteImageModel.getImageURL());
			debitNoteImageURLMap.put(DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID.toString(), debitNoteId);
			debitNoteImageURLMap.put(DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			debitNoteImageURLMapList.add(debitNoteImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), debitNoteImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteDebitNoteImageURLData(int companyId, int debitNoteId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_DEBIT_NOTE_IMAGE_URL, DEBIT_NOTE_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.DEBIT_NOTE_ID ))
				.append(String.format(" AND %s = ? ", DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID, DEBIT_NOTE_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, debitNoteId, companyId });
	}

}
