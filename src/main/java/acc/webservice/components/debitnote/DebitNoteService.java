package acc.webservice.components.debitnote;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class DebitNoteService {

	@Autowired
	private DebitNoteDAO debitNoteDAO;

	@Autowired
	private DebitNoteItemMappingDAO debitNoteItemMappingDAO;

	@Autowired
	private DebitNoteImageDAO debitNoteImageDAO;

	@Autowired
	private DebitNotePurchaseMappingDAO debitNotePurchaseMappingDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private DebitNoteChargeDAO debitNoteChargeDAO;

	public List<DebitNoteModel> getDebitNoteListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {
		return debitNoteDAO.getDebitNoteListData(companyId, start, numberOfRecord, filterParamter);
	}

	DebitNoteModel getDebitNoteDetailsById(int companyId, int debitNoteId) {
		DebitNoteModel debitNoteModel = debitNoteDAO.getDebitNoteDetailsById(companyId, debitNoteId);
		debitNoteModel.setDebitNoteChargeModelList(debitNoteChargeDAO.getDebitNoteChargeList(companyId, debitNoteId));
		debitNoteModel.setImageURLList(debitNoteImageDAO.getDebitNoteImageList(companyId, debitNoteId));
		return debitNoteModel;
	}

	public DebitNoteModel getDebitNoteDetailsByBillNumber(int companyId, String billNumber) {
		DebitNoteModel debitNoteModel = debitNoteDAO.getDebitNoteDetailsByBillNumber(companyId, billNumber);
		debitNoteModel.setDebitNoteChargeModelList(
				debitNoteChargeDAO.getDebitNoteChargeList(companyId, debitNoteModel.getDebitNoteId()));
		debitNoteModel
				.setImageURLList(debitNoteImageDAO.getDebitNoteImageList(companyId, debitNoteModel.getDebitNoteId()));
		return debitNoteModel;
	}

	public DebitNoteModel publishDebitNoteData(DebitNoteModel debitNoteModel) {
		validateDebitNoteManupulateData(debitNoteModel);
		debitNoteDAO.publishDebitNoteData(debitNoteModel);
		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteItemMappingList()) > 0) {
			debitNoteItemMappingDAO.saveDebitNoteItemMappingData(debitNoteModel.getDebitNoteItemMappingList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNotePurchaseMappingList()) > 0) {
			debitNotePurchaseMappingDAO.saveDebitNotePurchaseMappingData(
					debitNoteModel.getDebitNotePurchaseMappingList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteChargeModelList()) > 0) {
			debitNoteChargeDAO.saveDebitNoteChargeMappingData(debitNoteModel.getDebitNoteChargeModelList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getImageURLList()) > 0) {
			debitNoteImageDAO.saveDebitNoteImageURLData(debitNoteModel.getImageURLList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}
		companyService.updateCompanyDebitNoteCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 2),
				debitNoteModel.getCompanyId());
		companyService.updateCompanyDebitNotePendingCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 0),
				debitNoteModel.getCompanyId());
		companyService.updateCompanyLastDebitNoteNumber(debitNoteModel.getCompanyId(),
				debitNoteDAO.getMaxBillNumber(debitNoteModel.getCompanyId()));
		return debitNoteModel;
	}

	public DebitNoteModel saveDebitNoteData(DebitNoteModel debitNoteModel) {
		validateDebitNoteManupulateData(debitNoteModel);
		debitNoteDAO.saveDebitNoteData(debitNoteModel);
		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteItemMappingList()) > 0) {
			debitNoteItemMappingDAO.saveDebitNoteItemMappingData(debitNoteModel.getDebitNoteItemMappingList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNotePurchaseMappingList()) > 0) {
			debitNotePurchaseMappingDAO.saveDebitNotePurchaseMappingData(
					debitNoteModel.getDebitNotePurchaseMappingList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteChargeModelList()) > 0) {
			debitNoteChargeDAO.saveDebitNoteChargeMappingData(debitNoteModel.getDebitNoteChargeModelList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getImageURLList()) > 0) {
			debitNoteImageDAO.saveDebitNoteImageURLData(debitNoteModel.getImageURLList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		companyService.updateCompanyLastDebitNoteNumber(debitNoteModel.getCompanyId(),
				debitNoteDAO.getMaxBillNumber(debitNoteModel.getCompanyId()));
		return debitNoteModel;
	}

	public DebitNoteModel updatePublishedDebitNoteData(DebitNoteModel debitNoteModel) {
		validateDebitNoteManupulateData(debitNoteModel);
		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteItemMappingList()) > 0) {
			debitNoteItemMappingDAO.deleteDebitNoteItemMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteItemMappingDAO.saveDebitNoteItemMappingData(debitNoteModel.getDebitNoteItemMappingList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNotePurchaseMappingList()) > 0) {
			debitNotePurchaseMappingDAO.deleteDebitNotePurchaseMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNotePurchaseMappingDAO.saveDebitNotePurchaseMappingData(
					debitNoteModel.getDebitNotePurchaseMappingList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteChargeModelList()) > 0) {
			debitNoteChargeDAO.deleteDebitNoteChargeMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteChargeDAO.saveDebitNoteChargeMappingData(debitNoteModel.getDebitNoteChargeModelList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getImageURLList()) > 0) {
			debitNoteImageDAO.deleteDebitNoteImageURLData(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteImageDAO.saveDebitNoteImageURLData(debitNoteModel.getImageURLList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		debitNoteDAO.updatePublishedDebitNoteData(debitNoteModel);
		companyService.updateCompanyDebitNotePendingCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 0),
				debitNoteModel.getCompanyId());
		companyService.updateCompanyLastDebitNoteNumber(debitNoteModel.getCompanyId(),
				debitNoteDAO.getMaxBillNumber(debitNoteModel.getCompanyId()));
		return debitNoteModel;
	}

	public DebitNoteModel updateSavedDebitNoteData(DebitNoteModel debitNoteModel) {
		validateDebitNoteManupulateData(debitNoteModel);
		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteItemMappingList()) > 0) {
			debitNoteItemMappingDAO.deleteDebitNoteItemMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteItemMappingDAO.saveDebitNoteItemMappingData(debitNoteModel.getDebitNoteItemMappingList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNotePurchaseMappingList()) > 0) {
			debitNotePurchaseMappingDAO.deleteDebitNotePurchaseMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNotePurchaseMappingDAO.saveDebitNotePurchaseMappingData(
					debitNoteModel.getDebitNotePurchaseMappingList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getDebitNoteChargeModelList()) > 0) {
			debitNoteChargeDAO.deleteDebitNoteChargeMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteChargeDAO.saveDebitNoteChargeMappingData(debitNoteModel.getDebitNoteChargeModelList(),
					debitNoteModel.getCompanyId(), debitNoteModel.getDebitNoteId());
		}

		if (ApplicationUtility.getSize(debitNoteModel.getImageURLList()) > 0) {
			debitNoteImageDAO.deleteDebitNoteImageURLData(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteImageDAO.saveDebitNoteImageURLData(debitNoteModel.getImageURLList(), debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
		}
		debitNoteModel = debitNoteDAO.updateSavedDebitNoteData(debitNoteModel);
		companyService.updateCompanyLastDebitNoteNumber(debitNoteModel.getCompanyId(),
				debitNoteDAO.getMaxBillNumber(debitNoteModel.getCompanyId()));
		return debitNoteModel;
	}

	public DebitNoteModel markDebitNoteAsVerified(DebitNoteModel debitNoteModel) {
		if (debitNoteModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			debitNoteItemMappingDAO.deleteDebitNoteItemMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNotePurchaseMappingDAO.deleteDebitNotePurchaseMapping(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteImageDAO.deleteDebitNoteImageURLData(debitNoteModel.getCompanyId(),
					debitNoteModel.getDebitNoteId());
			debitNoteDAO.markDebitNoteAsDeleteAndVerified(debitNoteModel);
		} else {
			debitNoteDAO.markDebitNoteAsVerified(debitNoteModel);
		}
		companyService.updateCompanyDebitNotePendingCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 0),
				debitNoteModel.getCompanyId());
		return debitNoteModel;
	}

	public int deletePublishedDebitNoteData(int companyId, int debitNoteId, int deletedBy) {
		debitNoteDAO.deletePublishedDebitNoteData(companyId, debitNoteId, deletedBy);
		companyService.updateCompanyLastDebitNoteNumber(companyId, debitNoteDAO.getMaxBillNumber(companyId));
		return companyService
				.updateCompanyDebitNotePendingCount(debitNoteDAO.getPendingDebitNoteCountData(companyId, 0), companyId);
	}

	public int deleteSavedDebitNoteData(int companyId, int debitNoteId, int deletedBy) {
		debitNoteImageDAO.deleteDebitNoteImageURLData(companyId, debitNoteId);
		debitNoteDAO.deleteSavedDebitNoteData(companyId, debitNoteId, deletedBy);
		debitNoteChargeDAO.deleteDebitNoteChargeMapping(companyId, debitNoteId);
		debitNoteItemMappingDAO.deleteDebitNoteItemMapping(companyId, debitNoteId);
		companyService.updateCompanyDebitNotePendingCount(debitNoteDAO.getPendingDebitNoteCountData(companyId, 0),
				companyId);
		return companyService.updateCompanyLastDebitNoteNumber(companyId, debitNoteDAO.getMaxBillNumber(companyId));
	}

	public DebitNoteModel publishSavedDebitNote(DebitNoteModel debitNoteModel) {
		updateSavedDebitNoteData(debitNoteModel);
		debitNoteDAO.publishSavedDebitNote(debitNoteModel);
		companyService.updateCompanyDebitNoteCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 2),
				debitNoteModel.getCompanyId());
		companyService.updateCompanyDebitNotePendingCount(
				debitNoteDAO.getPendingDebitNoteCountData(debitNoteModel.getCompanyId(), 0),
				debitNoteModel.getCompanyId());
		return debitNoteModel;
	}

	private void validateDebitNoteManupulateData(DebitNoteModel debitNoteModel) {

		if (debitNoteModel.getLedgerData() == null || debitNoteModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save debit note data.");
		}

	}

}
