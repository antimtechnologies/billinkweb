package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_ITEM_MAPPING_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;

public class DebitNoteItemMappingModel {

	private int debitNoteItemMappingId;
	private DebitNoteModel debitNote;
	private ItemModel item;
	private double quantity;
	private double purchaseRate;
	private double discount;
	private double taxRate;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private double totalItemAmount;
	private boolean measureDiscountInAmount;
	private int freequantity;
	private int freeItemId;
	private int locationId;
	private int batchId;
	private boolean isDeleted;
	private double additionaldiscount;
	private boolean addmeasureDiscountInAmount;

	public DebitNoteItemMappingModel() {
	}

	public DebitNoteItemMappingModel(ResultSet rs) throws SQLException {
		setQuantity(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.QUANTITY.toString()));
		setPurchaseRate(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.PURCHASE_RATE.toString()));
		setTaxRate(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setDiscount(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_DISCOUNT.toString()));
		setDebitNoteItemMappingId(rs.getInt(DEBIT_NOTE_ITEM_MAPPING_COLUMN.DEBIT_NOTE_ITEM_MAPPING_ID.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(DEBIT_NOTE_ITEM_MAPPING_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		setAdditionaldiscount(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
		setAddmeasureDiscountInAmount(rs.getBoolean(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setItemAmount(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.ITEM_AMOUNT.toString()));
		setIgst(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.S_GST.toString()));
		setCgst(rs.getDouble(DEBIT_NOTE_ITEM_MAPPING_COLUMN.C_GST.toString()));
		setLocationId(rs.getInt(DEBIT_NOTE_ITEM_MAPPING_COLUMN.LOCTION_ID.toString()));
		setBatchId(rs.getInt(DEBIT_NOTE_ITEM_MAPPING_COLUMN.BATCH_ID.toString()));
		ItemModel fetchedItemModel = new ItemModel();
		fetchedItemModel.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
		fetchedItemModel.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
		fetchedItemModel.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
		setItem(fetchedItemModel);
	}

	public int getDebitNoteItemMappingId() {
		return debitNoteItemMappingId;
	}

	public void setDebitNoteItemMappingId(int debitNoteItemMappingId) {
		this.debitNoteItemMappingId = debitNoteItemMappingId;
	}

	public DebitNoteModel getDebitNote() {
		return debitNote;
	}

	public void setDebitNote(DebitNoteModel debitNote) {
		this.debitNote = debitNote;
	}

	public ItemModel getItem() {
		return item;
	}

	public void setItem(ItemModel item) {
		this.item = item;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	public int getFreequantity() {
		return freequantity;
	}

	public void setFreequantity(int freequantity) {
		this.freequantity = freequantity;
	}

	public int getFreeItemId() {
		return freeItemId;
	}

	public void setFreeItemId(int freeItemId) {
		this.freeItemId = freeItemId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	public double getAdditionaldiscount() {
		return additionaldiscount;
	}

	public void setAdditionaldiscount(double additionaldiscount) {
		this.additionaldiscount = additionaldiscount;
	}

	public boolean isAddmeasureDiscountInAmount() {
		return addmeasureDiscountInAmount;
	}

	public void setAddmeasureDiscountInAmount(boolean addmeasureDiscountInAmount) {
		this.addmeasureDiscountInAmount = addmeasureDiscountInAmount;
	}
	
	
}
