package acc.webservice.components.debitnote;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.debitnote.model.DebitNoteModel;
import acc.webservice.enums.DatabaseEnum.DEBIT_NOTE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class DebitNoteImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private DebitNoteModel debitNoteModel;

	public DebitNoteImageModel() {
	}

	public DebitNoteImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(DEBIT_NOTE_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {			
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public DebitNoteModel getDebitNoteModel() {
		return debitNoteModel;
	}

	public void setDebitNoteModel(DebitNoteModel debitNoteModel) {
		this.debitNoteModel = debitNoteModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
