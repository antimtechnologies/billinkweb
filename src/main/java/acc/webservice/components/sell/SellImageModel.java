package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.sell.model.SellModel;
import acc.webservice.enums.DatabaseEnum.SELL_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class SellImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private SellModel sellModel;

	public SellImageModel() {
	}

	public SellImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(SELL_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(SELL_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public SellModel getSellModel() {
		return sellModel;
	}

	public void setSellModel(SellModel sellModel) {
		this.sellModel = sellModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
