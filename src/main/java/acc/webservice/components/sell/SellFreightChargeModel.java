package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_FREIGHT_DATA_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class SellFreightChargeModel {

	private int sellFreightMappingId;
	private double freightCharge;
	private boolean deleted;
	private LedgerModel ledger;
	private SellModel sell;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private double cgstPer;
	private double sgstPer;
	private double igstPer;
	private boolean measureDiscountInAmount;
	private int excellDocumentId;
	
	public SellFreightChargeModel() {		
	}

	public SellFreightChargeModel(ResultSet rs) throws SQLException {
		setSellFreightMappingId(rs.getInt(SELL_FREIGHT_DATA_COLUMN.SELL_FREIGHT_MAPPING_ID.toString()));
		setFreightCharge(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.FREIGHT_CHARGE.toString()));
		setDiscount(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setTaxRate(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		
		
		setTotalItemAmount(ApplicationUtility.round(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()), 2));
		setItemAmount(ApplicationUtility.round(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.ITEM_AMOUNT.toString()), 2));
		setCgst(ApplicationUtility.round(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.C_GST.toString()), 2));
		setIgst(ApplicationUtility.round(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.I_GST.toString()), 2));
		setSgst(ApplicationUtility.round(rs.getDouble(SELL_FREIGHT_DATA_COLUMN.S_GST.toString()), 2));
		setMeasureDiscountInAmount(rs.getBoolean(SELL_FREIGHT_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		LedgerModel fetchedLedger = new LedgerModel();
		fetchedLedger.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedger.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedger.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));

		setLedger(fetchedLedger);
	}

	public int getSellFreightMappingId() {
		return sellFreightMappingId;
	}

	public void setSellFreightMappingId(int sellFreightMappingId) {
		this.sellFreightMappingId = sellFreightMappingId;
	}

	public SellModel getSell() {
		return sell;
	}

	public void setSell(SellModel sell) {
		this.sell = sell;
	}

	public double getFreightCharge() {
		return freightCharge;
	}

	public void setFreightCharge(double freightCharge) {
		this.freightCharge = freightCharge;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LedgerModel getLedger() {
		return ledger;
	}

	public void setLedger(LedgerModel ledger) {
		this.ledger = ledger;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	public double getCgstPer() {
		return cgstPer;
	}

	public void setCgstPer(double cgstPer) {
		this.cgstPer = cgstPer;
	}

	public double getSgstPer() {
		return sgstPer;
	}

	public void setSgstPer(double sgstPer) {
		this.sgstPer = sgstPer;
	}

	public double getIgstPer() {
		return igstPer;
	}

	public void setIgstPer(double igstPer) {
		this.igstPer = igstPer;
	}
	public int getExcellDocumentId() {
		return excellDocumentId;
	}

	public void setExcellDocumentId(int excellDocumentId) {
		this.excellDocumentId = excellDocumentId;
	}

	@Override
	public String toString() {
		return "SellFreightChargeModel [freightCharge=" + freightCharge + ", deleted=" + deleted + ", ledger=" + ledger
				+ ", sell=" + sell + ", discount=" + discount + ", taxRate=" + taxRate + ", totalItemAmount="
				+ totalItemAmount + ", itemAmount=" + itemAmount + ", cgst=" + cgst + ", sgst=" + sgst + ", igst="
				+ igst + ", cgstPer=" + cgstPer + ", sgstPer=" + sgstPer + ", igstPer=" + igstPer
				+ ", measureDiscountInAmount=" + measureDiscountInAmount + "]";
	}


}
