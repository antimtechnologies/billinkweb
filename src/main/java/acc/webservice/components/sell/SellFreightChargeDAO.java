package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_FREIGHT_DATA_COLUMN;

@Lazy
@Repository
public class SellFreightChargeDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveSellFreightMappingData(List<SellFreightChargeModel> sellFreightModel, int companyId, int sellId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_FREIGHT_DATA))
				.append(String.format(" ( %s, %s, %s, %s, ", SELL_FREIGHT_DATA_COLUMN.SELL_ID, SELL_FREIGHT_DATA_COLUMN.FREIGHT_CHARGE, SELL_FREIGHT_DATA_COLUMN.LEDGER_ID, SELL_FREIGHT_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", SELL_FREIGHT_DATA_COLUMN.ITEM_DISCOUNT, SELL_FREIGHT_DATA_COLUMN.TAX_RATE, SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT, SELL_FREIGHT_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" %s, %s, %s, %s, %s ) ", SELL_FREIGHT_DATA_COLUMN.I_GST, SELL_FREIGHT_DATA_COLUMN.S_GST, SELL_FREIGHT_DATA_COLUMN.C_GST, SELL_FREIGHT_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT, SELL_FREIGHT_DATA_COLUMN.EXCELL_DOCUMENT_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", SELL_FREIGHT_DATA_COLUMN.SELL_ID, SELL_FREIGHT_DATA_COLUMN.FREIGHT_CHARGE, SELL_FREIGHT_DATA_COLUMN.LEDGER_ID, SELL_FREIGHT_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", SELL_FREIGHT_DATA_COLUMN.ITEM_DISCOUNT, SELL_FREIGHT_DATA_COLUMN.TAX_RATE, SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT, SELL_FREIGHT_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s  ) ", SELL_FREIGHT_DATA_COLUMN.I_GST, SELL_FREIGHT_DATA_COLUMN.S_GST, SELL_FREIGHT_DATA_COLUMN.C_GST, SELL_FREIGHT_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT,  SELL_FREIGHT_DATA_COLUMN.EXCELL_DOCUMENT_ID));

		List<Map<String, Object>> sellFrightMappingList = new ArrayList<>();
		Map<String, Object> sellFrightMap;
		for (SellFreightChargeModel sellFreight : sellFreightModel) {

			if (sellFreight.getLedger() == null || sellFreight.getLedger().getLedgerId() <= 0) {
				continue;
			}

			sellFrightMap = new HashMap<>();
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.FREIGHT_CHARGE.toString(), sellFreight.getFreightCharge());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.LEDGER_ID.toString(), sellFreight.getLedger().getLedgerId());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.SELL_ID.toString(), sellId);
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.ITEM_DISCOUNT.toString(), sellFreight.getDiscount());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.TAX_RATE.toString(), sellFreight.getTaxRate());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), sellFreight.getTotalItemAmount());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.ITEM_AMOUNT.toString(), sellFreight.getItemAmount());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.I_GST.toString(), sellFreight.getIgst());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.S_GST.toString(), sellFreight.getSgst());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.C_GST.toString(), sellFreight.getCgst());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellFreight.getExcellDocumentId());
			sellFrightMap.put(SELL_FREIGHT_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), sellFreight.isMeasureDiscountInAmount());

			sellFrightMappingList.add(sellFrightMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellFrightMappingList.toArray(new HashMap[0]));
	}

	int deleteSellFreightMapping(int companyId, int sellId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_FREIGHT_DATA, SELL_FREIGHT_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", SELL_FREIGHT_DATA_COLUMN.SELL_ID, SELL_FREIGHT_DATA_COLUMN.SELL_ID ))
				.append(String.format(" AND %s = ? ", SELL_FREIGHT_DATA_COLUMN.COMPANY_ID, SELL_FREIGHT_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, sellId, companyId });
	}

	List<SellFreightChargeModel> getSellFreightList(int companyId, int sellId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		
		sqlQueryToFetchData.append(String.format("SELECT SFM.%s, SFM.%s, ", SELL_FREIGHT_DATA_COLUMN.FREIGHT_CHARGE, SELL_FREIGHT_DATA_COLUMN.SELL_FREIGHT_MAPPING_ID))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", SELL_FREIGHT_DATA_COLUMN.ITEM_DISCOUNT, SELL_FREIGHT_DATA_COLUMN.TAX_RATE, SELL_FREIGHT_DATA_COLUMN.TOTAL_ITEM_AMOUNT))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", SELL_FREIGHT_DATA_COLUMN.ITEM_AMOUNT, SELL_FREIGHT_DATA_COLUMN.I_GST, SELL_FREIGHT_DATA_COLUMN.S_GST))
			.append(String.format(" SFM.%s, SFM.%s, ", SELL_FREIGHT_DATA_COLUMN.C_GST, SELL_FREIGHT_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
			.append(String.format(" LD.%s, LD.%s, LD.%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.SAC_CODE))
			.append(String.format(" FROM %s SFM ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_FREIGHT_DATA))
			.append(String.format(" INNER JOIN %s LD ON SFM.%s = LD.%s AND LD.%s = 0", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_FREIGHT_DATA_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.IS_DELETED))
			.append(String.format(" AND LD.%s = ? ", LEDGER_DETAILS_COLUMN.COMPANY_ID, SELL_FREIGHT_DATA_COLUMN.COMPANY_ID))
			.append(String.format(" WHERE SFM.%s = ? AND SFM.%s = 0 ", SELL_FREIGHT_DATA_COLUMN.SELL_ID, SELL_FREIGHT_DATA_COLUMN.IS_DELETED))
			.append(String.format(" AND SFM.%s = ? ", SELL_FREIGHT_DATA_COLUMN.COMPANY_ID, SELL_FREIGHT_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().query(sqlQueryToFetchData.toString(), new Object[] { companyId, sellId, companyId }, getSellFreightDetailsExtractor());

	}

	private ResultSetExtractor<List<SellFreightChargeModel>> getSellFreightDetailsExtractor() {
		return new ResultSetExtractor<List<SellFreightChargeModel>>() {
			@Override
			public List<SellFreightChargeModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SellFreightChargeModel> sellFreightChargeList = new ArrayList<>();
				while (rs.next()) {
					sellFreightChargeList.add(new SellFreightChargeModel(rs));
				}
				return sellFreightChargeList;
			}
		};
	}

}
