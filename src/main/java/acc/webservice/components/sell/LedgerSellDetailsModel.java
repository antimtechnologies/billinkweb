package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.enums.DatabaseEnum.LEDGER_PURCHASE_DETAILS;
import acc.webservice.enums.DatabaseEnum.LEDGER_SELL_DETAILS;
import acc.webservice.global.utils.DateUtility;

public class LedgerSellDetailsModel {

	private int sellId;
	private String sellBillDate;
	private String billNo;
	private int itemId;
	private String itemName;
	private double quantity;
	private String batchNo;
	private String mfgDate,expDate;
	
	public LedgerSellDetailsModel() {
		
	}
	
	public LedgerSellDetailsModel(ResultSet rs) throws SQLException {
		
		Timestamp fetchedPurchaseDate = rs.getTimestamp(LEDGER_SELL_DETAILS.SELL_DATE.toString());
		if (fetchedPurchaseDate != null) {
			setSellBillDate(DateUtility.converDateToUserString(fetchedPurchaseDate));
		}
		setSellId(rs.getInt(LEDGER_SELL_DETAILS.SELL_ID.toString()));
		setBillNo(rs.getString(LEDGER_SELL_DETAILS.BILL_NUMBER.toString()));
		setItemId(rs.getInt(LEDGER_SELL_DETAILS.ITEM_ID.toString()));
		setItemName(rs.getString(LEDGER_SELL_DETAILS.ITEM_NAME.toString()));
		setQuantity(rs.getDouble(LEDGER_SELL_DETAILS.QUANTITY.toString()));
		setBatchNo(rs.getString(LEDGER_SELL_DETAILS.BATCH_NO.toString()));
		Timestamp fetchedMfgDateTime = rs.getTimestamp(LEDGER_SELL_DETAILS.MFG_DATE.toString());
		setMfgDate(DateUtility.converDateToUserString(fetchedMfgDateTime));
		Timestamp fetchedExpDateTime = rs.getTimestamp(LEDGER_SELL_DETAILS.EXP_DATE.toString());
		setExpDate(DateUtility.converDateToUserString(fetchedExpDateTime));
	}
	
	
	public int getSellId() {
		return sellId;
	}
	public void setSellId(int sellId) {
		this.sellId = sellId;
	}
	
	
	public String getSellBillDate() {
		return sellBillDate;
	}

	public void setSellBillDate(String sellBillDate) {
		this.sellBillDate = sellBillDate;
	}

	
	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
	
	
}
