package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.SELL_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class SellImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<SellImageModel> getSellPaymentImageList(int companyId, int sellId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", SELL_IMAGE_URL_COLUMN.IMAGE_ID, SELL_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", SELL_IMAGE_URL_COLUMN.SELL_ID, SELL_IMAGE_URL_COLUMN.SELL_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", SELL_IMAGE_URL_COLUMN.COMPANY_ID, SELL_IMAGE_URL_COLUMN.COMPANY_ID, SELL_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(SELL_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(SELL_IMAGE_URL_COLUMN.SELL_ID.toString(), sellId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCreditNoteImageResultExctractor());
	}

	private ResultSetExtractor<List<SellImageModel>> getCreditNoteImageResultExctractor() {
		return new ResultSetExtractor<List<SellImageModel>>() {
			@Override
			public List<SellImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SellImageModel> sellImageModelList = new ArrayList<>();
				while (rs.next()) {
					sellImageModelList.add(new SellImageModel(rs));
				}
				return sellImageModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveSellImageURLData(List<SellImageModel> sellImageModelList, int companyId, int sellId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s) ", SELL_IMAGE_URL_COLUMN.SELL_ID, SELL_IMAGE_URL_COLUMN.IMAGE_URL, SELL_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", SELL_IMAGE_URL_COLUMN.SELL_ID, SELL_IMAGE_URL_COLUMN.IMAGE_URL, SELL_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> sellImageURLMapList = new ArrayList<>();
		Map<String, Object> sellImageURLMap;
		for (SellImageModel sellImageModel : sellImageModelList) {
			sellImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(sellImageModel.getImageURL())) {			
				sellImageModel.setImageURL(sellImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			sellImageURLMap.put(SELL_IMAGE_URL_COLUMN.IMAGE_URL.toString(), sellImageModel.getImageURL());
			sellImageURLMap.put(SELL_IMAGE_URL_COLUMN.SELL_ID.toString(), sellId);
			sellImageURLMap.put(SELL_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			sellImageURLMapList.add(sellImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteSaleImageURLData(int companyId, int sellId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_IMAGE_URL, SELL_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", SELL_IMAGE_URL_COLUMN.SELL_ID, SELL_IMAGE_URL_COLUMN.SELL_ID ))
				.append(String.format(" AND %s = ? ", SELL_IMAGE_URL_COLUMN.COMPANY_ID, SELL_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, sellId, companyId });
	}

}
