package acc.webservice.components.sell.utility;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.location.LocationModel;
import acc.webservice.components.location.LocationModelService;
import acc.webservice.components.purchase.PurchaseChargeModel;
import acc.webservice.components.purchase.PurchaseImageModel;
import acc.webservice.components.purchase.PurchaseItemModel;
import acc.webservice.components.purchase.PurchaseService;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.sell.SellFreightChargeModel;
import acc.webservice.components.sell.SellImageModel;
import acc.webservice.components.sell.SellItemModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class SellToPurchaseUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private LocationModelService locationService;

	public PurchaseModel savePurchaseForSelectedLedger(SellModel sellModel) throws AccountingSofwareException {

		PurchaseModel purchaseModel = new PurchaseModel();
		if (ApplicationUtility.getSize(sellModel.getBillNumber()) < 1) {
			return purchaseModel;
		}

		LedgerModel ledgerBasicDetails = ledgerService
				.getBasicLedgerDetailByLedgerId(sellModel.getLedgerData().getLedgerId(), sellModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());
			CompanyModel purchaseCompanyModel = companyService.getCompanyDetails(selectedLedgerCompanyId);

			if (purchaseCompanyModel.isAllowB2BImport()) {
				if (selectedLedgerCompanyId > 0) {
					CompanyModel sellCompanyDetail = companyService.getCompanyDetails(sellModel.getCompanyId());
					int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompanyId,
							sellCompanyDetail.getGstNumber());
					purchaseModel = purchaseService.getPurchaseDetailsByBillNumber(selectedLedgerCompanyId,
							sellModel.getBillNumber(), purchaseLedgerId);

					if (purchaseModel.getPurchaseId() > 0) {
						return purchaseModel;
					}

					purchaseModel = createPurcahseModelFromSellModel(sellModel, selectedLedgerCompanyId,
							sellCompanyDetail, purchaseLedgerId);
					purchaseService.savePurchaseData(purchaseModel);
				}
			}
		}
		return purchaseModel;
	}

	private PurchaseModel createPurcahseModelFromSellModel(SellModel sellModel, int selectedLedgerCompanyId,
			CompanyModel sellCompanyDetail, int purchaseLedgerId) throws AccountingSofwareException {

		PurchaseModel purchaseModel = new PurchaseModel();
		purchaseModel.setCompanyId(selectedLedgerCompanyId);
		purchaseModel.setBillNumber(sellModel.getBillNumber());
		purchaseModel.setDiscount(sellModel.getDiscount());
		purchaseModel.setGrosssTotalAmount(sellModel.getGrosssTotalAmount());
		purchaseModel.setPurchaseDate(sellModel.getSellDate());
		purchaseModel.setPurchaseMeasureDiscountInAmount(sellModel.isSellMeasureDiscountInAmount());
		purchaseModel.setTotalBillAmount(sellModel.getTotalBillAmount());
		purchaseModel.setImageURL(sellModel.getImageURL());
		purchaseModel.setPaymentStatus(sellModel.getPaymentStatus());
		purchaseModel.setDescription(sellModel.getDescription());
		purchaseModel.setStatus(sellModel.getStatus());
		purchaseModel.setReferenceNumber1(sellModel.getReferenceNumber1());
		purchaseModel.setReferenceNumber2(sellModel.getReferenceNumber2());
		purchaseModel.setPurchaseMeasureDiscountInAmount(sellModel.isSellMeasureDiscountInAmount());
		purchaseModel.setBillDate(Date.valueOf(sellModel.getSellDate()));
		purchaseModel.setPoNumber(sellModel.getPoNo());
		purchaseModel.setLrNumber(sellModel.getLrNo());
		purchaseModel.setEwayBillNumber(sellModel.geteWayBillNo());
		purchaseModel.setCityName(sellModel.getCityName());
		purchaseModel.setGstNumber(sellModel.getGstNumber());

		LedgerModel purchaseLedgerData = new LedgerModel();
		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompanyId, sellCompanyDetail);
		}
		purchaseLedgerData.setLedgerId(purchaseLedgerId);
		purchaseModel.setLedgerData(purchaseLedgerData);

		List<PurchaseImageModel> purchaseImageURLList = new ArrayList<>();
		PurchaseImageModel purchaseImageModel;
		if (ApplicationUtility.getSize(sellModel.getImageURLList()) > 0) {
			for (SellImageModel sellImageModel : sellModel.getImageURLList()) {
				purchaseImageModel = new PurchaseImageModel();
				purchaseImageModel.setImageURL(sellImageModel.getImageURL());
				purchaseImageURLList.add(purchaseImageModel);
			}
		}

		purchaseModel.setImageURLList(purchaseImageURLList);
		List<PurchaseItemModel> purchaseItemList = new ArrayList<>();
		PurchaseItemModel purchaseItemModel;
		if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {
			for (SellItemModel sellItemModel : sellModel.getSellItemModel()) {
				if (sellItemModel.getItem() == null || sellItemModel.getItem().getItemId() <= 0) {
					continue;
				}
				purchaseItemModel = getPurchaseItemModelFromSellItemModel(sellItemModel, sellModel.getCompanyId(),
						selectedLedgerCompanyId);
				purchaseItemList.add(purchaseItemModel);
			}
		}
		purchaseModel.setPurchaseItemModel(purchaseItemList);

		List<PurchaseChargeModel> purchaseChargeList = new ArrayList<>();
		PurchaseChargeModel purchaseChargeModel;
		if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
			for (SellFreightChargeModel sellFreightChargeModel : sellModel.getFreightList()) {
				if (sellFreightChargeModel.getLedger() == null
						|| sellFreightChargeModel.getLedger().getLedgerId() <= 0) {
					continue;
				}
				purchaseChargeModel = getPurchaseChargeModelFromSellFreightModel(sellFreightChargeModel,
						sellModel.getCompanyId(), selectedLedgerCompanyId);
				purchaseChargeList.add(purchaseChargeModel);
			}
		}
		purchaseModel.setPurchaseChargeList(purchaseChargeList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		purchaseModel.setAddedBy(userData);

		return purchaseModel;
	}

	private PurchaseChargeModel getPurchaseChargeModelFromSellFreightModel(
			SellFreightChargeModel sellFreightChargeModel, int sellCompanyId, int purchaseCompanyId)
			throws AccountingSofwareException {
		PurchaseChargeModel purchaseChargeModel = new PurchaseChargeModel();
		int ledgerId = ledgerService.getLedgerIdByName(purchaseCompanyId,
				sellFreightChargeModel.getLedger().getLedgerName());

		if (ledgerId < 1) {
			LedgerModel ledgerModel = ledgerService.getLedgerDetailsById(sellCompanyId,
					sellFreightChargeModel.getLedger().getLedgerId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			ledgerModel.setAddedBy(userData);
			ledgerModel.setLedgerDocumentList(null);
			ledgerModel.setOpeningBalance(0);
			ledgerModel.setCompanyId(purchaseCompanyId);
			ledgerId = ledgerService.saveLedgerData(ledgerModel).getLedgerId();
		}

		LedgerModel ledgerData = new LedgerModel();
		ledgerData.setLedgerId(ledgerId);

		purchaseChargeModel.setLedger(ledgerData);
		purchaseChargeModel.setCgst(sellFreightChargeModel.getCgst());
		purchaseChargeModel.setDiscount(sellFreightChargeModel.getDiscount());
		purchaseChargeModel.setIgst(sellFreightChargeModel.getIgst());
		purchaseChargeModel.setItemAmount(sellFreightChargeModel.getItemAmount());
		purchaseChargeModel.setMeasureDiscountInAmount(sellFreightChargeModel.isMeasureDiscountInAmount());
		purchaseChargeModel.setPurchaseCharge(sellFreightChargeModel.getFreightCharge());
		purchaseChargeModel.setSgst(sellFreightChargeModel.getSgst());
		purchaseChargeModel.setTaxRate(sellFreightChargeModel.getTaxRate());
		purchaseChargeModel.setTotalItemAmount(sellFreightChargeModel.getTotalItemAmount());
		return purchaseChargeModel;
	}

	private PurchaseItemModel getPurchaseItemModelFromSellItemModel(SellItemModel sellItemModel, int sellCompanyId,
			int purchaseCompanyId) {
		PurchaseItemModel purchaseItemModel = new PurchaseItemModel();
		int itemId = itemService.getItemIdByName(purchaseCompanyId, sellItemModel.getItem().getItemName());

		if (itemId < 1) {
			ItemModel itemModel = itemService.getItemDetailsById(sellCompanyId, sellItemModel.getItem().getItemId());

			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			itemModel.setAddedBy(userData);
			itemModel.setCompanyId(purchaseCompanyId);
			itemModel.setItemDocumentList(null);
			itemModel.setQuantity(0);
			itemModel.setBrand(null);
			itemModel.setCategory(null);
			itemId = itemService.saveItemData(itemModel).getItemId();
		}

		LocationModel locationModel = locationService.getLocationByCompanyLocationId(sellCompanyId,
				sellItemModel.getLocationId());
		if (locationModel != null) {
			LocationModel locationSellData = locationService.chekcklocationUniqueInsert(purchaseCompanyId,
					locationModel);
			sellItemModel.setLocationId(locationSellData.getLocationId());
		}
		ItemBatchModel batchModel = itemService.getOneBatch(sellItemModel.getBatchId());
		if (batchModel != null) {
			ItemBatchModel batchSellData = itemService.checkuniqueandInsertBatch(batchModel, itemId);

			sellItemModel.setBatchId(batchSellData.getBatchId());
		}
		ItemModel itemData = new ItemModel();
		itemData.setItemId(itemId);

		purchaseItemModel.setItem(itemData);
		purchaseItemModel.setCgst(sellItemModel.getCgst());
		purchaseItemModel.setDiscount(sellItemModel.getDiscount());
		purchaseItemModel.setIgst(sellItemModel.getIgst());
		purchaseItemModel.setItemAmount(sellItemModel.getItemAmount());
		purchaseItemModel.setMeasureDiscountInAmount(sellItemModel.isMeasureDiscountInAmount());
		purchaseItemModel.setPurchaseRate(sellItemModel.getSellRate());
		purchaseItemModel.setQuantity(sellItemModel.getQuantity());
		purchaseItemModel.setSgst(sellItemModel.getSgst());
		purchaseItemModel.setTaxRate(sellItemModel.getTaxRate());
		purchaseItemModel.setTotalItemAmount(sellItemModel.getTotalItemAmount());

		return purchaseItemModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail)
			throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
