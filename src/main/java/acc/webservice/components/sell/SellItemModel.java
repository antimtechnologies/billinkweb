package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_ITEM_DATA_COLUMN;

public class SellItemModel {

	private int sellItemMappingId;
	private double quantity,freeItemQty;
	private double sellRate;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private double cgstPer;
	private double sgstPer;
	private double igstPer;
	private ItemModel item;
	private SellModel sell;
	private boolean measureDiscountInAmount;
	private boolean deleted;
	private int excellDocumentId,freeItemId;
	private boolean addmeasureDiscountInAmount;
	private double additionaldiscount;
	
	private int locationId;
	
	private int batchId;
	public SellItemModel() {
	}

	public SellItemModel(ResultSet rs) throws SQLException {
		setQuantity(rs.getDouble(SELL_ITEM_DATA_COLUMN.QUANTITY.toString()));
		setSellRate(rs.getDouble(SELL_ITEM_DATA_COLUMN.SELL_RATE.toString()));
		setTaxRate(rs.getDouble(SELL_ITEM_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setDiscount(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setSellItemMappingId(rs.getInt(SELL_ITEM_DATA_COLUMN.SELL_ITEM_MAPPING_ID.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		setTotalItemAmount(ApplicationUtility.round(rs.getDouble(SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()), 2));
		setItemAmount(ApplicationUtility.round(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString()), 2));
		setIgst(ApplicationUtility.round(rs.getDouble(SELL_ITEM_DATA_COLUMN.I_GST.toString()), 2));
		setSgst(ApplicationUtility.round(rs.getDouble(SELL_ITEM_DATA_COLUMN.S_GST.toString()), 2));
		setCgst(ApplicationUtility.round(rs.getDouble(SELL_ITEM_DATA_COLUMN.C_GST.toString()), 2));
		setLocationId(rs.getInt(SELL_ITEM_DATA_COLUMN.LOCTION_ID.toString()));
		setBatchId(rs.getInt(SELL_ITEM_DATA_COLUMN.BATCH_ID.toString()));
		setFreeItemId(rs.getInt(SELL_ITEM_DATA_COLUMN.FREE_ITEM_ID.toString()));
		setFreeItemQty(rs.getDouble(SELL_ITEM_DATA_COLUMN.FREE_ITEM_QTY.toString()));
		setAdditionaldiscount(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
		setAddmeasureDiscountInAmount(rs.getBoolean(SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		ItemModel fetchedItem = new ItemModel();
		fetchedItem.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
		fetchedItem.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
		fetchedItem.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
		setItem(fetchedItem);
	}

	
	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	public int getSellItemMappingId() {
		return sellItemMappingId;
	}

	public void setSellItemMappingId(int sellItemMappingId) {
		this.sellItemMappingId = sellItemMappingId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getSellRate() {
		return sellRate;
	}

	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	public ItemModel getItem() {
		return item;
	}

	public void setItem(ItemModel item) {
		this.item = item;
	}

	public SellModel getSell() {
		return sell;
	}

	public void setSell(SellModel sell) {
		this.sell = sell;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.deleted = isDeleted;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	
	public double getCgstPer() {
		return cgstPer;
	}

	public void setCgstPer(double cgstPer) {
		this.cgstPer = cgstPer;
	}

	public double getSgstPer() {
		return sgstPer;
	}

	public void setSgstPer(double sgstPer) {
		this.sgstPer = sgstPer;
	}

	public double getIgstPer() {
		return igstPer;
	}

	public void setIgstPer(double igstPer) {
		this.igstPer = igstPer;
	}
	public int getExcellDocumentId() {
		return excellDocumentId;
	}

	public void setExcellDocumentId(int excellDocumentId) {
		this.excellDocumentId = excellDocumentId;
	}

	
	
	
	
	public boolean isAddmeasureDiscountInAmount() {
		return addmeasureDiscountInAmount;
	}

	public void setAddmeasureDiscountInAmount(boolean addmeasureDiscountInAmount) {
		this.addmeasureDiscountInAmount = addmeasureDiscountInAmount;
	}

	public double getAdditionaldiscount() {
		return additionaldiscount;
	}

	public void setAdditionaldiscount(double additionaldiscount) {
		this.additionaldiscount = additionaldiscount;
	}

	public double getFreeItemQty() {
		return freeItemQty;
	}

	public void setFreeItemQty(double freeItemQty) {
		this.freeItemQty = freeItemQty;
	}

	public int getFreeItemId() {
		return freeItemId;
	}

	public void setFreeItemId(int freeItemId) {
		this.freeItemId = freeItemId;
	}

	@Override
	public String toString() {
		return "SellItemModel [sellItemMappingId=" + sellItemMappingId + ", quantity=" + quantity + ", sellRate="
				+ sellRate + ", discount=" + discount + ", taxRate=" + taxRate + ", totalItemAmount=" + totalItemAmount
				+ ", itemAmount=" + itemAmount + ", cGST=" + cgst + ", sGST=" + sgst + ", iGST=" + igst
				+ ", measureDiscountInAmount=" + measureDiscountInAmount + ", deleted=" + deleted + "]";
	}

}
