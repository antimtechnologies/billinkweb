package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.ledger.LedgerPurchaseDetailsModel;

import acc.webservice.components.purchase.PurchaseItemModel;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBITNOTE_ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_PURCHASE_DETAILS;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_ITEM_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class SellDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@SuppressWarnings("unchecked")
	List<SellModel> getSellListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.DESCRIPTION))
						.append(String.format(" SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.SELL_BILL_DATE, SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, SELL_DETAILS_COLUMN.DISCOUNT, SELL_DETAILS_COLUMN.PAYMENT_STATUS, SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT))
						.append(String.format(" SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_DETAILS_COLUMN.FREIGHT_CHARGE, SELL_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
						.append(String.format(" SD.%s, SD.%s,SD.%s, ", SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX,SELL_DETAILS_COLUMN.DISPATCH_DATE))
						.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
						.append(String.format(" LM.%s, LM.%s,LM.%s, ", LEDGER_DETAILS_COLUMN.PRICE_ID, LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID,LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID ))
						.append(String.format(" SD.%s,SD.%s,SD.%s,SD.%s, ", SELL_DETAILS_COLUMN.PO_NO,SELL_DETAILS_COLUMN.LR_NO,SELL_DETAILS_COLUMN.E_WAY_BILL_NO,SELL_DETAILS_COLUMN.ACTUAL_WEIGHT))
						.append(String.format(" SD.%s,SD.%s, ", SELL_DETAILS_COLUMN.CITY_NAME,SELL_DETAILS_COLUMN.GST_NUMBER))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" LM.%s, LM.%s, LM.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PAN_NUMBER))
						.append(String.format(" LM.%s ", LEDGER_DETAILS_COLUMN.CITY_NAME))
						.append(String.format(" FROM %s SD ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON SD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, SELL_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s LM ON SD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s AUM ON SD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" LEFT JOIN %s MUM ON SD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON SD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON SD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE SD.%s = 0 ", SELL_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND SD.%s = :%s ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {					
					sqlQueryToFetchData.append(String.format(" AND SD.%s IN (:statusList) ", SELL_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(SELL_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter, SELL_DETAILS_COLUMN.IS_VERIFIED.toString());
				
				if (isVerified == 0 || isVerified == 1) {					
					sqlQueryToFetchData.append(String.format(" AND SD.%s = :%s ", SELL_DETAILS_COLUMN.IS_VERIFIED, SELL_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(SELL_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter, SELL_DETAILS_COLUMN.LEDGER_ID.toString());
				
				if (ledgerId > 0)  {					
					sqlQueryToFetchData.append(String.format(" AND SD.%s = :%s ", SELL_DETAILS_COLUMN.LEDGER_ID, SELL_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(SELL_DETAILS_COLUMN.SELL_ID.toString())) {
				String sellId = ApplicationUtility.getStrValue(filterParamter, SELL_DETAILS_COLUMN.SELL_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND (SD.%s LIKE :%s OR SD.%s LIKE :%s) ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.SELL_ID));
				parameters.put(SELL_DETAILS_COLUMN.SELL_ID.toString(), "%" + sellId + "%");
			}

			if (filterParamter.containsKey(SELL_DETAILS_COLUMN.PAYMENT_STATUS.toString())) {
				List<Integer> paymentStatuses = (List<Integer>) filterParamter.get(SELL_DETAILS_COLUMN.PAYMENT_STATUS.toString());
				sqlQueryToFetchData.append(String.format(" AND SD.%s IN (:%s) ", SELL_DETAILS_COLUMN.PAYMENT_STATUS, SELL_DETAILS_COLUMN.PAYMENT_STATUS));
				parameters.put(SELL_DETAILS_COLUMN.PAYMENT_STATUS.toString(), paymentStatuses);
			}
		}


		sqlQueryToFetchData.append(String.format(" ORDER BY SD.%s DESC LIMIT :%s, :%s ", SELL_DETAILS_COLUMN.SELL_ID, APPLICATION_GENERIC_ENUM.START, APPLICATION_GENERIC_ENUM.NO_OF_RECORD));

		parameters.put(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getSellDetailsListExtractor());
	}

	private ResultSetExtractor<List<SellModel>> getSellDetailsListExtractor() {

		return new ResultSetExtractor<List<SellModel>>() {
			@Override
			public List<SellModel> extractData(ResultSet rs) throws SQLException {
				List<SellModel> sellList = new ArrayList<>();
				while (rs.next()) {
					sellList.add(new SellModel(rs));
				}
				return sellList;
			}
		};
	}

	SellModel getSellDetailsByColumnNameValue(int companyId, String columnName, String columnValue) {
		StringBuilder sqlQueryToFetchData = getSellDetailsQuery(columnName);
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(columnName, columnValue);
		parameters.put(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getSellDetailsExtractor());
	}
	
	SellModel getSellDetailsForGivenLedgerBill(int companyId, String columnName, String columnValue, int ledgerId) {
			StringBuilder sqlQueryToFetchData = getSellDetailsQuery(columnName);
			sqlQueryToFetchData.append(String.format(" AND SD.%s = :%s ", SELL_DETAILS_COLUMN.LEDGER_ID, SELL_DETAILS_COLUMN.LEDGER_ID));

			Map<String, Object> parameters = new HashMap<>();
			parameters.put(columnName, columnValue);
			parameters.put(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
			parameters.put(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
			return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getSellDetailsExtractor());
	}
		
	private StringBuilder getSellDetailsQuery(String columnName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.DESCRIPTION))
						.append(String.format(" SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.SELL_BILL_DATE, SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, SELL_DETAILS_COLUMN.DISCOUNT, SELL_DETAILS_COLUMN.PAYMENT_STATUS, SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT))
						.append(String.format(" SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_DETAILS_COLUMN.FREIGHT_CHARGE, SELL_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" SD.%s, SD.%s, SD.%s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
						.append(String.format(" SD.%s, SD.%s,SD.%s, ", SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX,SELL_DETAILS_COLUMN.DISPATCH_DATE))
						.append(String.format(" SD.%s,SD.%s,SD.%s ,", SELL_DETAILS_COLUMN.PO_NO,SELL_DETAILS_COLUMN.LR_NO,SELL_DETAILS_COLUMN.E_WAY_BILL_NO))
						.append(String.format(" SD.%s, ", SELL_DETAILS_COLUMN.ACTUAL_WEIGHT))
						.append(String.format(" SD.%s,SD.%s, ", SELL_DETAILS_COLUMN.CITY_NAME,SELL_DETAILS_COLUMN.GST_NUMBER))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" LM.%s, LM.%s, LM.%s, ", LEDGER_DETAILS_COLUMN.GST_NUMBER, LEDGER_DETAILS_COLUMN.ADDRESS, LEDGER_DETAILS_COLUMN.PAN_NUMBER))
						.append(String.format(" LM.%s,LM.%s,LM.%s,LM.%s, ", LEDGER_DETAILS_COLUMN.CITY_NAME,LEDGER_DETAILS_COLUMN.PRICE_ID,LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID,LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID))
						.append(String.format(" IM.%s, IM.%s, IM.%s, SIM.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.ITEM_NAME, SELL_ITEM_DATA_COLUMN.QUANTITY))
						.append(String.format(" SIM.%s, SIM.%s, SIM.%s, ", SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT, SELL_ITEM_DATA_COLUMN.SELL_ITEM_MAPPING_ID, SELL_ITEM_DATA_COLUMN.SELL_RATE))
						.append(String.format(" SIM.%s, SIM.%s, SIM.%s, ", SELL_ITEM_DATA_COLUMN.TAX_RATE, SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT, SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
						.append(String.format(" SIM.%s, SIM.%s, SIM.%s, SIM.%s, SIM.%s, SIM.%s, SIM.%s, SIM.%s,  ", SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT, SELL_ITEM_DATA_COLUMN.I_GST, SELL_ITEM_DATA_COLUMN.S_GST, SELL_ITEM_DATA_COLUMN.C_GST,SELL_ITEM_DATA_COLUMN.BATCH_ID,SELL_ITEM_DATA_COLUMN.LOCTION_ID,SELL_ITEM_DATA_COLUMN.FREE_ITEM_ID,SELL_ITEM_DATA_COLUMN.FREE_ITEM_QTY))
						.append(String.format(" SIM.%s, SIM.%s ",SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
						.append(String.format(" FROM %s SD ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
						.append(String.format(" INNER JOIN %s SM ON SD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, SELL_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s LM ON SD.%s = LM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, SELL_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID))
						.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s AUM ON SD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
						.append(String.format(" LEFT JOIN %s MUM ON SD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON SD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON SD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s SIM ON SIM.%s = SD.%s AND SIM.%s = 0 ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_ITEM_DATA, SELL_ITEM_DATA_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID, SELL_ITEM_DATA_COLUMN.IS_DELETED))
						.append(String.format(" AND SIM.%s = :%s ", SELL_ITEM_DATA_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" LEFT JOIN %s IM ON IM.%s = SIM.%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS, ITEM_DETAILS_COLUMN.ITEM_ID, SELL_ITEM_DATA_COLUMN.ITEM_ID))
						.append(String.format(" AND IM.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" WHERE SD.%s = 0 ", SELL_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND SD.%s = :%s ", columnName, columnName));
		return sqlQueryToFetchData;


		
	}

	private ResultSetExtractor<SellModel> getSellDetailsExtractor() {

		return new ResultSetExtractor<SellModel>() {
			@Override
			public SellModel extractData(ResultSet rs) throws SQLException {
				SellModel sellDetails = new SellModel();
				List<SellItemModel> sellItemMappingList = new ArrayList<>();
				while (rs.next()) {
					if (sellDetails.getSellId() < 1) {						
						sellDetails = new SellModel(rs);
					}
					if (rs.getInt(SELL_ITEM_DATA_COLUMN.SELL_ITEM_MAPPING_ID.toString()) > 0) {						
						sellItemMappingList.add(new SellItemModel(rs));
					}

				}
				sellDetails.setSellItemModel(sellItemMappingList);
				return sellDetails;
			}
		};
	}

	SellModel saveSellData(SellModel sellModel) {
		StringBuilder sqlQuery = getSellInsertQuery(sellModel);

		if (!ApplicationUtility.isNullEmpty(sellModel.getImageURL())) {			
			sellModel.setImageURL(sellModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IMAGE_URL.toString(), sellModel.getImageURL())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), sellModel.getBillNumber())
				.addValue(SELL_DETAILS_COLUMN.DESCRIPTION.toString(), sellModel.getDescription())
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString(), sellModel.getSellDate())
				.addValue(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), sellModel.getLedgerData().getLedgerId())
				.addValue(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), sellModel.getTotalBillAmount())
				.addValue(SELL_DETAILS_COLUMN.DISCOUNT.toString(), sellModel.getDiscount())
				.addValue(SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT.toString(), sellModel.isSellMeasureDiscountInAmount())
				.addValue(SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), sellModel.getGrosssTotalAmount())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellModel.getReferenceNumber1())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellModel.getReferenceNumber2())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), sellModel.getBillNumberPrefix())
				.addValue(SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(), sellModel.getCurrentBillNumber())
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellModel.getExcellDocumentId())
				.addValue(SELL_DETAILS_COLUMN.ADDED_BY.toString(), sellModel.getAddedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.ORDER_ID.toString(), sellModel.getOrderId())
				.addValue(SELL_DETAILS_COLUMN.SUB_ORDER_ID.toString(), sellModel.getSubOrderId())
				.addValue(SELL_DETAILS_COLUMN.VOUCHER_TYPE.toString(), sellModel.getVoucherType())
				.addValue(SELL_DETAILS_COLUMN.DISPATCH_DATE.toString(), sellModel.getDispatchDate())
				.addValue(SELL_DETAILS_COLUMN.PO_NO.toString(), sellModel.getPoNo())
				.addValue(SELL_DETAILS_COLUMN.LR_NO.toString(), sellModel.getLrNo())
				.addValue(SELL_DETAILS_COLUMN.E_WAY_BILL_NO.toString(), sellModel.geteWayBillNo())
				.addValue(SELL_DETAILS_COLUMN.CITY_NAME.toString(), sellModel.getCityName())
				.addValue(SELL_DETAILS_COLUMN.GST_NUMBER.toString(), sellModel.getGstNumber())
				.addValue(SELL_DETAILS_COLUMN.ACTUAL_WEIGHT.toString(), sellModel.getActualWeight());

		sellModel.setAddedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		sellModel.setSellId(holder.getKey().intValue());
		return sellModel;
	}

	SellModel publishSellData(SellModel sellModel) {
		StringBuilder sqlQuery = getSellInsertQuery(sellModel);
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IMAGE_URL.toString(), sellModel.getImageURL())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), sellModel.getBillNumber())
				.addValue(SELL_DETAILS_COLUMN.DESCRIPTION.toString(), sellModel.getDescription())
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString(), sellModel.getSellDate())
				.addValue(SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT.toString(), sellModel.isSellMeasureDiscountInAmount())
				.addValue(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), sellModel.getLedgerData().getLedgerId())
				.addValue(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), sellModel.getTotalBillAmount())
				.addValue(SELL_DETAILS_COLUMN.DISCOUNT.toString(), sellModel.getDiscount())
				.addValue(SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellModel.getExcellDocumentId())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellModel.getReferenceNumber1())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellModel.getReferenceNumber2())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), sellModel.getBillNumberPrefix())
				.addValue(SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(), sellModel.getCurrentBillNumber())
				.addValue(SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), sellModel.getGrosssTotalAmount())
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.ADDED_BY.toString(), sellModel.getAddedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.ORDER_ID.toString(), sellModel.getOrderId())
				.addValue(SELL_DETAILS_COLUMN.SUB_ORDER_ID.toString(), sellModel.getSubOrderId())
				.addValue(SELL_DETAILS_COLUMN.VOUCHER_TYPE.toString(), sellModel.getVoucherType())
				.addValue(SELL_DETAILS_COLUMN.DISPATCH_DATE.toString(), sellModel.getDispatchDate())
				.addValue(SELL_DETAILS_COLUMN.PO_NO.toString(), sellModel.getPoNo())
				.addValue(SELL_DETAILS_COLUMN.LR_NO.toString(), sellModel.getLrNo())
				.addValue(SELL_DETAILS_COLUMN.E_WAY_BILL_NO.toString(), sellModel.geteWayBillNo())
				.addValue(SELL_DETAILS_COLUMN.CITY_NAME.toString(), sellModel.getCityName())
				.addValue(SELL_DETAILS_COLUMN.GST_NUMBER.toString(), sellModel.getGstNumber())
				.addValue(SELL_DETAILS_COLUMN.ACTUAL_WEIGHT.toString(), sellModel.getActualWeight());
			

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		sellModel.setSellId(holder.getKey().intValue());
		sellModel.setAddedDateTime(DateUtility.getCurrentDateTime());
		return sellModel;
	}

	private StringBuilder getSellInsertQuery(SellModel sellModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format("( %s, %s, %s, ", SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
							.append(String.format(" %s, %s, %s, ", SELL_DETAILS_COLUMN.DESCRIPTION, SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.SELL_BILL_DATE))
							.append(String.format(" %s, %s, %s, ", SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX, SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" %s, %s, %s, ", SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, SELL_DETAILS_COLUMN.DISCOUNT, SELL_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" %s, %s, %s, %s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT, SELL_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s, %s, ", SELL_DETAILS_COLUMN.ADDED_BY, SELL_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s, %s, %s, ", SELL_DETAILS_COLUMN.ORDER_ID, SELL_DETAILS_COLUMN.SUB_ORDER_ID, SELL_DETAILS_COLUMN.VOUCHER_TYPE))
							.append(String.format(" %s, %s, %s, %s, %s, ", SELL_DETAILS_COLUMN.DISPATCH_DATE, SELL_DETAILS_COLUMN.PO_NO, SELL_DETAILS_COLUMN.LR_NO,SELL_DETAILS_COLUMN.CITY_NAME,SELL_DETAILS_COLUMN.GST_NUMBER))
							.append(String.format(" %s, %s ) ", SELL_DETAILS_COLUMN.E_WAY_BILL_NO, SELL_DETAILS_COLUMN.ACTUAL_WEIGHT))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
							.append(String.format(" :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.DESCRIPTION, SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.SELL_BILL_DATE))
							.append(String.format(" :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX, SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, SELL_DETAILS_COLUMN.DISCOUNT, SELL_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" :%s, :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT, SELL_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.ADDED_BY, SELL_DETAILS_COLUMN.ADDED_DATE_TIME, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.ORDER_ID, SELL_DETAILS_COLUMN.SUB_ORDER_ID, SELL_DETAILS_COLUMN.VOUCHER_TYPE))
							.append(String.format(" :%s, :%s, :%s, :%s, :%s, ", SELL_DETAILS_COLUMN.DISPATCH_DATE, SELL_DETAILS_COLUMN.PO_NO, SELL_DETAILS_COLUMN.LR_NO,SELL_DETAILS_COLUMN.CITY_NAME,SELL_DETAILS_COLUMN.GST_NUMBER))
							.append(String.format(" :%s, :%s ) ", SELL_DETAILS_COLUMN.E_WAY_BILL_NO, SELL_DETAILS_COLUMN.ACTUAL_WEIGHT));
		return sqlQuery;
	}

	SellModel updateSavedSellData(SellModel sellModel) {
		StringBuilder sqlQuery = getSellUpdateQuery(sellModel);
		if (!ApplicationUtility.isNullEmpty(sellModel.getImageURL())) {			
			sellModel.setImageURL(sellModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IMAGE_URL.toString(), sellModel.getImageURL())
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), sellModel.getBillNumber())
				.addValue(SELL_DETAILS_COLUMN.DESCRIPTION.toString(), sellModel.getDescription())
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT.toString(), sellModel.isSellMeasureDiscountInAmount())
				.addValue(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString(), sellModel.getSellDate())
				.addValue(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), sellModel.getLedgerData().getLedgerId())
				.addValue(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), sellModel.getTotalBillAmount())
				.addValue(SELL_DETAILS_COLUMN.DISCOUNT.toString(), sellModel.getDiscount())
				.addValue(SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), sellModel.getGrosssTotalAmount())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellModel.getReferenceNumber1())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellModel.getReferenceNumber2())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), sellModel.getBillNumberPrefix())
				.addValue(SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(), sellModel.getCurrentBillNumber())
				.addValue(SELL_DETAILS_COLUMN.MODIFIED_BY.toString(), sellModel.getModifiedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellModel.getSellId())
				.addValue(SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellModel.getExcellDocumentId())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.DISPATCH_DATE.toString(), sellModel.getDispatchDate())
				.addValue(SELL_DETAILS_COLUMN.PO_NO.toString(), sellModel.getPoNo())
				.addValue(SELL_DETAILS_COLUMN.LR_NO.toString(), sellModel.getLrNo())
				.addValue(SELL_DETAILS_COLUMN.CITY_NAME.toString(), sellModel.getCityName())
				.addValue(SELL_DETAILS_COLUMN.GST_NUMBER.toString(), sellModel.getGstNumber())
				.addValue(SELL_DETAILS_COLUMN.E_WAY_BILL_NO.toString(), sellModel.geteWayBillNo())
				.addValue(SELL_DETAILS_COLUMN.ACTUAL_WEIGHT.toString(), sellModel.getActualWeight())
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		sellModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		if (!ApplicationUtility.isNullEmpty(sellModel.getImageURL())) {			
			sellModel.setImageURL(ApplicationUtility.getServerURL() + sellModel.getImageURL());
		}

		return sellModel;
	}

	SellModel updatePublishedSellData(SellModel sellModel) {
		StringBuilder sqlQuery = getSellUpdateQuery(sellModel);
		if (!ApplicationUtility.isNullEmpty(sellModel.getImageURL())) {			
			sellModel.setImageURL(sellModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IMAGE_URL.toString(), sellModel.getImageURL())
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), sellModel.getBillNumber())
				.addValue(SELL_DETAILS_COLUMN.DESCRIPTION.toString(), sellModel.getDescription())
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString(), sellModel.getSellDate())
				.addValue(SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT.toString(), sellModel.isSellMeasureDiscountInAmount())
				.addValue(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), sellModel.getLedgerData().getLedgerId())
				.addValue(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), sellModel.getTotalBillAmount())
				.addValue(SELL_DETAILS_COLUMN.DISCOUNT.toString(), sellModel.getDiscount())
				.addValue(SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), sellModel.getGrosssTotalAmount())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), sellModel.getReferenceNumber1())
				.addValue(SELL_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), sellModel.getReferenceNumber2())
				.addValue(SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString(), sellModel.getBillNumberPrefix())
				.addValue(SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString(), sellModel.getCurrentBillNumber())
				.addValue(SELL_DETAILS_COLUMN.MODIFIED_BY.toString(), sellModel.getModifiedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellModel.getSellId())
				.addValue(SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellModel.getExcellDocumentId())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
//				.addValue(SELL_DETAILS_COLUMN.DISPATCH_DATE.toString(), sellModel.getDispatchDate())
				.addValue(SELL_DETAILS_COLUMN.PO_NO.toString(), sellModel.getPoNo())
				.addValue(SELL_DETAILS_COLUMN.LR_NO.toString(), sellModel.getLrNo())
				.addValue(SELL_DETAILS_COLUMN.CITY_NAME.toString(), sellModel.getCityName())
				.addValue(SELL_DETAILS_COLUMN.GST_NUMBER.toString(), sellModel.getGstNumber())
				.addValue(SELL_DETAILS_COLUMN.E_WAY_BILL_NO.toString(), sellModel.geteWayBillNo())
				.addValue(SELL_DETAILS_COLUMN.ACTUAL_WEIGHT.toString(), sellModel.getActualWeight())
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		sellModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		if (!ApplicationUtility.isNullEmpty(sellModel.getImageURL())) {			
			sellModel.setImageURL(ApplicationUtility.getServerURL() + sellModel.getImageURL());
		}
		return sellModel;
	}

	private StringBuilder getSellUpdateQuery(SellModel sellModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IMAGE_URL, SELL_DETAILS_COLUMN.IMAGE_URL))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.BILL_NUMBER, SELL_DETAILS_COLUMN.BILL_NUMBER))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.DESCRIPTION, SELL_DETAILS_COLUMN.DESCRIPTION))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT, SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.SELL_BILL_DATE, SELL_DETAILS_COLUMN.SELL_BILL_DATE))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.LEDGER_ID, SELL_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.DISCOUNT, SELL_DETAILS_COLUMN.DISCOUNT))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT, SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IS_VERIFIED, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_1, SELL_DETAILS_COLUMN.REFERENCE_NUBER_1))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX, SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID, SELL_DETAILS_COLUMN.EXCELL_DOCUMENT_ID))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.REFERENCE_NUBER_2, SELL_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.MODIFIED_BY, SELL_DETAILS_COLUMN.MODIFIED_BY))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.CITY_NAME, SELL_DETAILS_COLUMN.CITY_NAME))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.PO_NO, SELL_DETAILS_COLUMN.PO_NO))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.LR_NO, SELL_DETAILS_COLUMN.LR_NO))
//							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.DISPATCH_DATE, SELL_DETAILS_COLUMN.DISPATCH_DATE))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.GST_NUMBER, SELL_DETAILS_COLUMN.GST_NUMBER))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.E_WAY_BILL_NO, SELL_DETAILS_COLUMN.E_WAY_BILL_NO))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.ACTUAL_WEIGHT, SELL_DETAILS_COLUMN.ACTUAL_WEIGHT))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME, SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.IS_DELETED, SELL_DETAILS_COLUMN.IS_DELETED));
		
		return sqlQuery;
	}

	int deleteSavedSellData(int companyId, int purchaseId, int deletedBy) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IS_DELETED, SELL_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.DELETED_BY, SELL_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), purchaseId)
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(SELL_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(SELL_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deletePublishedSellData(int companyId, int sellId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IS_VERIFIED, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.DELETED_BY, SELL_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.DELETED_DATE_TIME, SELL_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellId)
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(SELL_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(SELL_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int markSellAsVerified(SellModel sellModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IS_VERIFIED, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.VERIFIED_BY, SELL_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.IS_DELETED, SELL_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(SELL_DETAILS_COLUMN.VERIFIED_BY.toString(), sellModel.getVerifiedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellModel.getSellId())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
	}

	SellModel markSellAsDeleteAndVerified(SellModel sellModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.IS_VERIFIED, SELL_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.VERIFIED_BY, SELL_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s, ", SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME, SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.IS_DELETED, SELL_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(SELL_DETAILS_COLUMN.VERIFIED_BY.toString(), sellModel.getVerifiedBy().getUserId())
				.addValue(SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellModel.getSellId())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		sellModel.setVerified(true);
		sellModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return sellModel;
	}

	int getPendingSellCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ", SELL_DETAILS_COLUMN.SELL_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
				.append(String.format("WHERE %s != :%s AND %s = 0 ", SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", SELL_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()));

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

	public SellModel publishSavedSell(SellModel sellModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" WHERE %s = :%s ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s ", SELL_DETAILS_COLUMN.IS_DELETED, SELL_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.SELL_ID.toString(), sellModel.getSellId())
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), sellModel.getCompanyId())
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(SELL_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		sellModel.setStatus(status);
		return sellModel;
	}

	@SuppressWarnings("unchecked")
	public int[] updateSellPaymentStatus(List<Map<String, Object>> sellPaymentStatusList, int companyId) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
							.append(String.format(" %s = :%s ", SELL_DETAILS_COLUMN.PAYMENT_STATUS, SELL_DETAILS_COLUMN.PAYMENT_STATUS))
							.append(String.format(" WHERE %s = :%s ", SELL_DETAILS_COLUMN.SELL_ID, SELL_DETAILS_COLUMN.SELL_ID))
							.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellPaymentStatusList.toArray(new HashMap[0]));
	}

	int getMaxBillNumber(int companyId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT MAX(%s) AS maxBillNumber FROM %s ", SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER, COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS))
				.append(String.format("WHERE %s = 0 ", SELL_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s  ", SELL_DETAILS_COLUMN.COMPANY_ID, SELL_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s != :%s  ", SELL_DETAILS_COLUMN.STATUS_ID, SELL_DETAILS_COLUMN.STATUS_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(SELL_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(SELL_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()));

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getMaxBillNumberExctractor());
	}

	private ResultSetExtractor<Integer> getMaxBillNumberExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int maxBillNumber = 0;
				while (rs.next()) {
					maxBillNumber = rs.getInt("maxBillNumber");
				}
				return maxBillNumber;
			}
		};
	}
	
	// 14-05-2019 Strat
	List<LedgerSellDetailsModel> getledgerSellList(int ledgerID){
		StringBuilder sqlQueryToFetchData = new StringBuilder();
					  sqlQueryToFetchData.append(String.format("SELECT sell.sellId,sell.sellBillDate,sell.billNo,sid.itemId,sid.quantity,item.itemName,batch.batchNo,batch.mfgDate,batch.expDate"))
					  					 .append(String.format(" FROM acc_company_sell_details AS sell, acc_company_sell_item_data AS sid,acc_company_items_details AS item,acc_item_batch_details AS batch"))
					  					 .append(String.format(" WHERE sell.ledgerId = "+ledgerID+" AND sell.sellId IN(SELECT sid.sellId FROM acc_company_sell_item_data AS sid) AND sid.itemId IN( SELECT item.itemId FROM acc_company_items_details AS item) AND item.itemId IN (SELECT batch.itemId FROM acc_item_batch_details AS batch) AND sell.sellId = sid.sellId  AND item.itemId = sid.itemId AND item.itemId = batch.itemId AND batch.isDeleted = 0 GROUP BY sell.sellId"));
					  Map<String, Object> parameters = new HashMap<>();
					  parameters.put(SELL_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerID);
					  return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLegderSellDetailsExtractor());
	}
	private ResultSetExtractor<List<LedgerSellDetailsModel>> getLegderSellDetailsExtractor() {

		return new ResultSetExtractor<List<LedgerSellDetailsModel>>() {
			@Override
			public List<LedgerSellDetailsModel> extractData(ResultSet rs) throws SQLException {
				List<LedgerSellDetailsModel> ledgerSellList = new ArrayList<LedgerSellDetailsModel>();
				while (rs.next()) {
					ledgerSellList.add(new LedgerSellDetailsModel(rs));
				}
				return ledgerSellList;
			}
		};
	}
	
	
	List<SellItemModel> getledgerSellDetails(int sellID) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT *,item.itemName,item.hsnCode FROM acc_company_sell_item_data  AS sid,acc_company_items_details AS item WHERE sid.sellId = "+sellID+" AND sid.isDeleted = 0 AND sid.itemId IN( SELECT item.itemId FROM acc_company_items_details AS item)  AND sid.itemId = item.itemId "));
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(SELL_ITEM_DATA_COLUMN.SELL_ID.toString(), sellID);
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getledgerSellDetailsExctractor());
	}
	
	private ResultSetExtractor<List<SellItemModel>> getledgerSellDetailsExctractor() {
		return new ResultSetExtractor<List<SellItemModel>>() {
			@Override
			public List<SellItemModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SellItemModel> itemSellList = new ArrayList<>();
				//ItemModel itemModel = new ItemModel();
				while (rs.next()) {
					itemSellList.add(new SellItemModel(rs));
				}
				return itemSellList;
			}
		};
	}
	
	// 14-05-2019 End
	
	List<CreditNoteGetItem> getItemDataListForDebitNote(String itemName,String arr) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("SELECT ci.%s, ci.%s, ci.%s, ci.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
				.append(String.format(" ci.%s, ci.%s, ci.%s, ", ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
				.append(String.format(" cpi.%s,cpi.%s as %s,"
						+ "cpi.%s,cpi.%s,cpi.%s,cpi.%s,cpi.%s,cpi.%s,cpi.%s,cpi.%s,location.%s,location.%s,batch.%s,batch.%s,batch.%s,batch.%s ",SELL_ITEM_DATA_COLUMN.QUANTITY,SELL_ITEM_DATA_COLUMN.SELL_RATE,ITEM_DETAILS_COLUMN.PURCHASE_RATE
						,SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT,SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT,SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT
						,SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT,SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT
						,SELL_ITEM_DATA_COLUMN.I_GST,SELL_ITEM_DATA_COLUMN.C_GST,SELL_ITEM_DATA_COLUMN.S_GST
						,LOCATION_DETAILS_COLUMN.LOCATION_ID,LOCATION_DETAILS_COLUMN.LOCATION_NAME
						,ITEM_BATCH_COLUMN.BATCH_ID,ITEM_BATCH_COLUMN.BATCH_NO,ITEM_BATCH_COLUMN.ITEM_MFG_DATE,ITEM_BATCH_COLUMN.ITEM_EXP_DATE
						))
				
				.append(String.format(" FROM %s AS cpi ",COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_ITEM_DATA))
				.append(String.format("INNER JOIN %s AS cpd  ON cpd.%s = cpi.%s ",COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_DETAILS,SELL_DETAILS_COLUMN.SELL_ID,SELL_ITEM_DATA_COLUMN.SELL_ID))
				.append(String.format(" INNER JOIN %s AS ci ON ci.%s  = cpi.%s  ",COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS,ITEM_DETAILS_COLUMN.ITEM_ID,SELL_ITEM_DATA_COLUMN.ITEM_ID))
				.append(" left JOIN acc_location_details AS location ON location.locationId = cpi.locationId ")
				.append(" left JOIN acc_item_batch_details AS batch ON batch.batchId = cpi.batchId ")
				.append(String.format("WHERE cpi.%s IN ("+arr+") ",SELL_ITEM_DATA_COLUMN.SELL_ID))
				.append(String.format(" AND ( %s LIKE :%s OR  %s LIKE :%s OR %s LIKE :%s) GROUP BY ci.%s ORDER BY cpd.%s  ASC", ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME,ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_EAN_NO, ITEM_DETAILS_COLUMN.ITEM_EAN_NO,ITEM_DETAILS_COLUMN.ITEM_ID,SELL_DETAILS_COLUMN.SELL_BILL_DATE));		
		
		Map<String, Object> paramMap = new HashMap<>();
		//paramMap.put(ITEM_PURCHASE_RATE_COLUMN.ITEM_ID.toString(), itemId);
		//paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString(), date);
		//paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString(), date);
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), "%" + itemName + "%");
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_SKU.toString(), "%" + itemName + "%");
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString(), "%" + itemName + "%");
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemResultExctractor());
	}
	
	private ResultSetExtractor<List<CreditNoteGetItem>> getItemResultExctractor() {
		return new ResultSetExtractor<List<CreditNoteGetItem>>() {
			@Override
			public List<CreditNoteGetItem> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CreditNoteGetItem> creditNoteItemList = new ArrayList<>();
				CreditNoteGetItem creditNoteGetItem;
				while (rs.next()) {
					creditNoteGetItem = new CreditNoteGetItem();
					creditNoteGetItem.setItemName(rs.getString(DEBITNOTE_ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					creditNoteGetItem.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
					//creditNoteGetItem.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					creditNoteGetItem.setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
					creditNoteGetItem.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					creditNoteGetItem.setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
					creditNoteGetItem.setQuantity(rs.getDouble(SELL_ITEM_DATA_COLUMN.QUANTITY.toString()));
					
					
					//creditNoteGetItem.setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
					creditNoteGetItem.setProfilePhotoURL(ApplicationUtility.getServerURL() + rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));
					creditNoteGetItem.setItemAmount(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString()));
					creditNoteGetItem.setDiscount(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString()));
					creditNoteGetItem.setMeasureDiscountInAmount(rs.getBoolean(SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));
					creditNoteGetItem.setAdditionaldiscount(rs.getDouble(SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
					creditNoteGetItem.setAddmeasureDiscountInAmount(rs.getBoolean(SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
					creditNoteGetItem.setIgst(rs.getDouble(SELL_ITEM_DATA_COLUMN.I_GST.toString()));
					creditNoteGetItem.setSgst(rs.getDouble(SELL_ITEM_DATA_COLUMN.S_GST.toString()));
					creditNoteGetItem.setCgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.C_GST.toString()));
					creditNoteGetItem.setLocationId(rs.getInt(LOCATION_DETAILS_COLUMN.LOCATION_ID.toString()));
					creditNoteGetItem.setLocationName(rs.getString(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString()));
					creditNoteGetItem.setBatchId(rs.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
					creditNoteGetItem.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
					creditNoteGetItem.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
					creditNoteGetItem.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
					
					
					creditNoteItemList.add(creditNoteGetItem);
				}
				return creditNoteItemList;
			}
		};
	}
	
	
	
}
