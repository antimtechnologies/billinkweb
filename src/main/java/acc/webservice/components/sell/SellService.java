package acc.webservice.components.sell;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.purchase.DebitNoteGetItem;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.components.users.UserDAO;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.NumberToWordsConvertor;

@Service
@Lazy
public class SellService {

	@Autowired
	private SellDAO sellDAO;

	@Autowired
	private SellItemDAO sellItemDAO;

	@Autowired
	private SellFreightChargeDAO sellFreightChargeDAO;

	@Autowired
	private SellImageDAO sellImageDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private UserDAO userDao;

	public List<SellModel> getSellListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {
		return sellDAO.getSellListData(companyId, start, numberOfRecord, filterParamter);
	}

	public SellModel getSellDetailsById(int companyId, int sellId) {
		SellModel sellData = sellDAO.getSellDetailsByColumnNameValue(companyId, SELL_DETAILS_COLUMN.SELL_ID.toString(),
				String.valueOf(sellId));
		sellData.setFreightList(sellFreightChargeDAO.getSellFreightList(companyId, sellId));
		sellData.setImageURLList(sellImageDAO.getSellPaymentImageList(companyId, sellId));
		return sellData;
	}

	public SellModel getSellDetailsByBillNumber(int companyId, String billNumber) {
		SellModel sellData = sellDAO.getSellDetailsByColumnNameValue(companyId,
				SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), billNumber);
		if (sellData.getSellId() > 0) {
			sellData.setFreightList(sellFreightChargeDAO.getSellFreightList(companyId, sellData.getSellId()));
			sellData.setImageURLList(sellImageDAO.getSellPaymentImageList(companyId, sellData.getSellId()));
		}
		return sellData;
	}

	public SellModel getSellDetailsForGivenLedgerBill(int companyId, String billNumber, int purchaseLedgerId) {
		SellModel sellData = sellDAO.getSellDetailsForGivenLedgerBill(companyId,
				SELL_DETAILS_COLUMN.BILL_NUMBER.toString(), billNumber, purchaseLedgerId);
		if (sellData.getSellId() > 0) {

			sellData.setFreightList(sellFreightChargeDAO.getSellFreightList(companyId, sellData.getSellId()));
			sellData.setImageURLList(sellImageDAO.getSellPaymentImageList(companyId, sellData.getSellId()));
		}
		return sellData;

	}

	public SellModel publishSellData(SellModel sellModel) {
		validateSellManupulateData(sellModel);
		sellDAO.publishSellData(sellModel);
		if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {

			sellItemDAO.saveSellItemsMappingData(sellModel.getSellItemModel(), sellModel.getCompanyId(),
					sellModel.getSellId());

		}

		if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
			sellFreightChargeDAO.saveSellFreightMappingData(sellModel.getFreightList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getImageURLList()) > 0) {
			sellImageDAO.saveSellImageURLData(sellModel.getImageURLList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}
		companyService.updateCompanySellCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 2),
				sellModel.getCompanyId());
		companyService.updateCompanySellPendingCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 0),
				sellModel.getCompanyId());
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));

		return sellModel;
	}

	public SellModel saveSellData(SellModel sellModel) {
		validateSellManupulateData(sellModel);
		sellDAO.saveSellData(sellModel);

		if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {
			sellItemDAO.saveSellItemsMappingData(sellModel.getSellItemModel(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
			sellFreightChargeDAO.saveSellFreightMappingData(sellModel.getFreightList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getImageURLList()) > 0) {
			sellImageDAO.saveSellImageURLData(sellModel.getImageURLList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));
		return sellModel;
	}

	public SellModel updatePublishedSellData(SellModel sellModel) {
		validateSellManupulateData(sellModel);
		if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {
			sellItemDAO.deleteSellItemMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellItemDAO.saveSellItemsMappingData(sellModel.getSellItemModel(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
			sellFreightChargeDAO.deleteSellFreightMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellFreightChargeDAO.saveSellFreightMappingData(sellModel.getFreightList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getImageURLList()) > 0) {
			sellImageDAO.deleteSaleImageURLData(sellModel.getCompanyId(), sellModel.getSellId());
			sellImageDAO.saveSellImageURLData(sellModel.getImageURLList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		sellDAO.updatePublishedSellData(sellModel);
		companyService.updateCompanySellPendingCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 0),
				sellModel.getCompanyId());
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));
		return sellModel;
	}

	public SellModel updateSavedSellData(SellModel sellModel) {
		validateSellManupulateData(sellModel);
		if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {
			sellItemDAO.deleteSellItemMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellItemDAO.saveSellItemsMappingData(sellModel.getSellItemModel(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
			sellFreightChargeDAO.deleteSellFreightMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellFreightChargeDAO.saveSellFreightMappingData(sellModel.getFreightList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		if (ApplicationUtility.getSize(sellModel.getImageURLList()) > 0) {
			sellImageDAO.deleteSaleImageURLData(sellModel.getCompanyId(), sellModel.getSellId());
			sellImageDAO.saveSellImageURLData(sellModel.getImageURLList(), sellModel.getCompanyId(),
					sellModel.getSellId());
		}

		sellDAO.updateSavedSellData(sellModel);
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));
		return sellModel;
	}

	public int deleteSavedSellData(int companyId, int sellId, int deletedBy) {
		sellItemDAO.deleteSellItemMapping(companyId, sellId);
		sellFreightChargeDAO.deleteSellFreightMapping(companyId, sellId);
		sellImageDAO.deleteSaleImageURLData(companyId, sellId);
		companyService.updateCompanyLastBillNumber(companyId, sellDAO.getMaxBillNumber(companyId));
		return sellDAO.deleteSavedSellData(companyId, sellId, deletedBy);
	}

	public int deletePublishedSellData(int companyId, int sellId, int deletedBy) {
		sellDAO.deletePublishedSellData(companyId, sellId, deletedBy);
		sellFreightChargeDAO.deleteSellFreightMapping(companyId, sellId);
		companyService.updateCompanyLastBillNumber(companyId, sellDAO.getMaxBillNumber(companyId));
		return companyService.updateCompanySellPendingCount(sellDAO.getPendingSellCountData(companyId, 0), companyId);
	}

	public SellModel markSellAsVerified(SellModel sellModel) {
		if (sellModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			sellImageDAO.deleteSaleImageURLData(sellModel.getCompanyId(), sellModel.getSellId());
			sellItemDAO.deleteSellItemMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellFreightChargeDAO.deleteSellFreightMapping(sellModel.getCompanyId(), sellModel.getSellId());
			sellDAO.markSellAsDeleteAndVerified(sellModel);
		} else {
			sellDAO.markSellAsVerified(sellModel);
		}
		companyService.updateCompanySellPendingCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 0),
				sellModel.getCompanyId());
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));
		return sellModel;
	}

	public SellModel publishSavedSell(SellModel sellModel) {
		updateSavedSellData(sellModel);
		sellDAO.publishSavedSell(sellModel);
		companyService.updateCompanySellCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 2),
				sellModel.getCompanyId());
		companyService.updateCompanySellPendingCount(sellDAO.getPendingSellCountData(sellModel.getCompanyId(), 0),
				sellModel.getCompanyId());
		companyService.updateCompanyLastBillNumber(sellModel.getCompanyId(),
				sellDAO.getMaxBillNumber(sellModel.getCompanyId()));
		return sellModel;
	}

	public int[] updateSellPaymentStatus(List<Map<String, Object>> sellPaymentStatusMap, int companyId) {
		return sellDAO.updateSellPaymentStatus(sellPaymentStatusMap, companyId);
	}

	private void validateSellManupulateData(SellModel sellModel) {

		if (sellModel.getLedgerData() == null || sellModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save sell data.");
		}
	}

	@SuppressWarnings("rawtypes")
	public String getPrintHTMLString(SellModel sellModel) {
		Map<String, Object> hsnData = getHsnData(sellModel);
		double igstTotal = (double) hsnData.get("totalIgst");
		double sgstTotal = (double) hsnData.get("totalSgst");
		double cgstTotal = (double) hsnData.get("totalCgst");

		CompanyModel companyData = companyService.getCompanyDetails(sellModel.getCompanyId());
		LedgerModel ledgerData = ledgerService.getLedgerDetailsById(sellModel.getCompanyId(),
				sellModel.getLedgerData().getLedgerId());
		UserModel userDetails = userDao.getUserDetailsByUserName(sellModel.getAddedBy().getUserName());
		StringBuilder htmlStringForPrint = new StringBuilder();

		htmlStringForPrint.append("<div style='border:1px solid black; font-size: 11px;'>")
				.append("<div style='width: 100%; display: inline-flex;'> ")
				.append("<div style='width: 100px;height: 100px;margin: 5px;'> ")
				.append("<img style='max-width: 100px;max-height: 100px;margin-left: auto;margin-right: auto;display: block;' src='"+companyData.getProfilePhotoURL() + "' ")
				.append("> ").append("</div> ").append("<div style='text-align: center; width: 85%'> ")
				.append("<div style='font-size: 20px; font-weight: 700;'>").append(companyData.getCompanyName() + "</div>").append("<div style='font-size: 20px;'>INVOICE</div>")
								.append("<div style='font-size: 20px;'>ORIGINAL FOR RECIPIENT</div>").append("</div>").append("</div>")
								.append("<table border='1px' style = 'border-collapse:collapse; width:100%'>").append("<thead>")
								.append("<tr>").append("<th width ='35%'>").append("<b style='text-align: center'>Customer</b>")
								.append("</th>").append("<th width ='30%'>")
								.append("<b style='text-align: center; padding-top: 25px;'>Invoice</b>").append("</th>")
								.append("<th width ='35%'>").append("<b style='text-align: center'>Billing Office</b>").append("</th>")
								.append("</tr>").append("</thead>").append("<tbody>").append("<tr>").append("<td width ='35%'>")
								.append("<span>Bill-to/Sell-to Customer</span>").append("<br /> ").append("<span>Name:&nbsp;")
								.append(ledgerData.getLedgerName() == null ? "" : ledgerData.getLedgerName()).append("</span>")
								.append("<br />").append("<span>Address:&nbsp;")
								.append(ledgerData.getAddress() == null ? "" : ledgerData.getAddress()).append("</span>")
								.append("<br /> ").append("<span>City:&nbsp;")
								.append(ledgerData.getCityName() == null ? "" : ledgerData.getCityName()).append("</span>")
								.append("<br /> ").append("<span>PAN No:&nbsp;")
								.append(ledgerData.getPanNumber() == null ? "" : ledgerData.getPanNumber()).append("</span>")
								.append("<br /> ").append("<span>GSTN No:&nbsp;")
								.append(ledgerData.getGstNumber() == null ? "" : ledgerData.getGstNumber()).append("</span>")
								.append("<br /> ").append("<span>Place of supply:&nbsp;")
								.append(ledgerData.getState().getStateName() == null ? "" : ledgerData.getState().getStateName())
								.append("</span>").append("</td>").append("<td width ='30%' style='vertical-align: baseline;'>")
								.append("<span>Invoice No:&nbsp;")
								.append(sellModel.getBillNumber() != null ? sellModel.getBillNumber() : "").append("</span>")
								.append("<br />").append("<span>Invoice Date:&nbsp;")
								.append(sellModel.getSellDate() != null ? sellModel.getSellDate() : "").append("</span>")
								.append("<br />").append("<span>Reference:&nbsp;")
								.append(sellModel.getReferenceNumber1() != null ? sellModel.getReferenceNumber1() : "")
								.append("</span>").append("<br />").append("<span>Other Reference:&nbsp;")
								.append(sellModel.getReferenceNumber2() != null ? sellModel.getReferenceNumber2() : "")
								.append("</span>").append("</td>").append("<td width ='35%' style='vertical-align: baseline;'>")
								.append("<span>").append(companyData.getCompanyName()).append("</span>").append(" <br /> <span>")
								.append(companyData.getOfficeAddress()).append("</span>").append("<br /> <span>")
								.append(companyData.getCityName()).append("</span>").append("<br /> <span>")
								.append(companyData.getState().getStateName()).append("</span>").append("<br /> <span>GSTN:&nbsp;")
								.append(companyData.getGstNumber()).append("</span>").append("<br /> <span>Invoice By:&nbsp;")
								.append(userDetails.getFirstName() + " " + userDetails.getLastName()).append("</span>").append("</td>")
								.append("</tr>").append("</tbody>").append("</table>");
						htmlStringForPrint.append("<table border='1px' style = 'border-collapse:collapse;'> ").append("<tbody> ");
						if (ApplicationUtility.getSize(sellModel.getSellItemModel()) > 0) {
										htmlStringForPrint.append("<tr> ").append("<th colspan='11'> ")
												.append("<b style='text-align: center'>Item List</b> ").append("</th> ").append("</tr> ")
										.append("<tr>").append("<th>No</th> ").append("<th style='width: 20%;'>Item Name</th> ")
												.append("<th style='width: 8%'>Qty</th> ").append("<th>Sale Rate</th> ")
												.append("<th style='width: 10%;'>Disc.</th> ").append("<th>Amount</th> ")
												.append("<th style='width: 8%;'>Tax Rate</th> ").append("<th>SGST</th> ").append("<th>CGST</th> ")
												.append("<th>IGST</th> ").append("<th style='width: 10%;'>Amount</th> ").append("</tr> ");
										int count = 0;
										for (SellItemModel sellItemData : sellModel.getSellItemModel()) {
											count++;
											htmlStringForPrint.append("<tr> ").append("<td><span>" + count + "</span>.</td> ").append("<td> ")
											.append("<td align='right'><span>" + sellItemData.getSellRate() + "</span></td> ")
											.append("<td align='right'> ").append("<div class='input-group'> ")
											.append("<span>" + sellItemData.getDiscount() + "</span> ");
											if (sellItemData.isMeasureDiscountInAmount()) {
													htmlStringForPrint.append("<span ng-if='vm.sell.sellItemModel[$index].measureDiscountInAmount'>$</span> ");
											} else {
													htmlStringForPrint.append("<span ng-if='!vm.sell.sellItemModel[$index].measureDiscountInAmount'>%</span> ");
											}
											htmlStringForPrint.append("</div> ").append("</td> ")
																	.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellItemData.getItemAmount(), 2) + "</span></td> ")
																	.append("<td align='right'><span>" + (int) sellItemData.getTaxRate() + "%</span></td> ")
																	.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellItemData.getSgst(), 2) + "</span></td> ")
																	.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellItemData.getCgst(), 2) + "</span></td> ")
																	.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellItemData.getIgst(), 2) + "</span></td> ")
																	.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellItemData.getTotalItemAmount(), 2) + "</span></td> ")
																	.append("</tr> ");
														}
													}
											
													if (ApplicationUtility.getSize(sellModel.getFreightList()) > 0) {
														htmlStringForPrint.append("<tr> ").append("<th colspan='11'> ")
																.append("<b style='text-align: center'>Charge List</b> ").append("</th> ").append("</tr> ")
																.append("<tr> ").append("<th>No</th> ").append("<th style='width: 20%;'>Charge Description</th> ")
																.append("<th style='width: 8%'>Qty</th> ").append("<th>Rate</th> ")
																.append("<th style='width: 10%;'>Disc.</th> ").append("<th>Amount</th> ")
																.append("<th style='width: 8%;'>Tax Rate</th> ").append("<th>SGST</th> ").append("<th>CGST</th> ")
																.append("<th>IGST</th> ").append("<th style='width: 10%;'>Amount</th> ").append("</tr> ");
										
														int count = 0;
														for (SellFreightChargeModel sellFreightChargeModel : sellModel.getFreightList()) {
															count++;
															htmlStringForPrint.append("<tr>").append("<td><span>" + count + "</span>.</td> ")
																	.append("<td style=''><span>"
																			+ sellFreightChargeModel.getLedger().getLedgerName() + "</span></td> ")
																	.append("<td align='right'><span>" + sellFreightChargeModel.getFreightCharge()+ "</span></td> "
																	+ sellFreightChargeModel.getDiscount() + "</span>");
																	
																	if (sellFreightChargeModel.isMeasureDiscountInAmount()) {
																		htmlStringForPrint.append("<span ng-if='vm.sell.freightList[$index].measureDiscountInAmount'>$</span> ");
																	} else {
																		htmlStringForPrint.append("<span ng-if='!vm.sell.freightList[$index].measureDiscountInAmount'>%</span> ");
																	}
																	htmlStringForPrint.append("</td> ")
																							.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellFreightChargeModel.getItemAmount(), 2) + "</span></td> ")
																							.append("<td align='right'><span>" + (int) sellFreightChargeModel.getTaxRate()  + "%</span></td> ")
																							.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellFreightChargeModel.getSgst(), 2) + "</span></td> ")
																							.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellFreightChargeModel.getCgst(), 2) + "</span></td> ")
																							.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellFreightChargeModel.getIgst(), 2) + "</span></td> ")
																							.append("<td align='right'><span>" + ApplicationUtility.roundToFixValue(sellFreightChargeModel.getTotalItemAmount(), 2) + "</span></td> ")
																							.append("</tr> ");
																	}
														
													}
													
													htmlStringForPrint.append("<tr>").append("<td align='right' colspan='5'>")
													.append("<span style='text-align: center'>Gross Total</span>").append("</td>")
													.append("<td align='right'>")
													.append("<span>" + ApplicationUtility.roundToFixValue((sellModel.getGrosssTotalAmount() - sgstTotal - cgstTotal - igstTotal), 2) + "</span>")
													.append("</td>").append("<td>").append("</td>").append("<td align='right'>" + ApplicationUtility.roundToFixValue(sgstTotal, 2) + "</td>")
													.append("<td align='right'>" + ApplicationUtility.roundToFixValue(cgstTotal, 2) + "</td>")
													.append("<td align='right'>" + ApplicationUtility.roundToFixValue(igstTotal, 2) + "</td>").append("<td a	lign='right'>")
													.append("<span>" + ApplicationUtility.roundToFixValue(sellModel.getGrosssTotalAmount(), 2) + "</span>").append("</td>").append("</tr>")
													.append("<tr>").append("<td colspan='10' align='right'>Disc.</td>").append("<td align ='right'>")
													.append("<span>" + sellModel.getDiscount() + "</span>");
													if (sellModel.isSellMeasureDiscountInAmount()) {
														htmlStringForPrint.append("<span ng-if='vm.sell.sellMeasureDiscountInAmount'>$</span> ");
													} else {
														htmlStringForPrint.append("<span ng-if='!vm.sell.sellMeasureDiscountInAmount'>%</span> ");
													}
													htmlStringForPrint.append("</td>").append("</tr>").append("<tr>")
													.append("<td colspan='10' align='right'>Total</td>").append("<td align='right'>")
													.append("<span>" + ApplicationUtility.roundToFixValue(sellModel.getTotalBillAmount(), 2) + "</span>").append("</td>").append("</tr>");
													
													htmlStringForPrint.append("</tbody> ").append("</table> ");
													
													// For amount in words div
													htmlStringForPrint
															.append("<div style='border-bottom:1px solid black;'><span style='margin-left:10px;'>Total in words: ")
															.append(NumberToWordsConvertor.convertToWordsString(String.valueOf(sellModel.getTotalBillAmount())))
															.append("</span></div>");
													
													htmlStringForPrint.append("</br> ");
													
													/*
													 * .append("<table border = '1px' style = 'border-collapse:collapse;' width = '100%'> "
													 * )
													 * .append("<tr><td align = 'right'><span style='text-align: center'>Gross Total</span> "
													 * ) .append("<span ng-bind='vm.getGrossTotalAmount()'>" +
													 * sellModel.getGrosssTotalAmount() + "</span></td></tr> ")
													 * .append("<tr><td align = 'right'><span>Discount</span> <span> " +
													 * sellModel.getDiscount() + " </span> ");
													 * 
													 * 
													 * htmlStringForPrint.append("</td></tr> ") .append("<tr><td align = 'right'> ")
													 * .append("<span colspan='10'>Total</span> ") .append("<span>" +
													 * sellModel.getTotalBillAmount() +
													 * "</span></td></tr> ").append("</table> <br/>");
													 */
													if ((hsnData.get("totalIgst") != null && (double) hsnData.get("totalIgst") > 0)
															|| (hsnData.get("totalSgst") != null && (double) hsnData.get("totalSgst") > 0)
															|| (hsnData.get("totalCgst") != null && (double) hsnData.get("totalCgst") > 0)) {
														htmlStringForPrint.append("<table border = '1px' style = 'border-collapse:collapse;' width = '100%'>")
																.append(" <tr> <th>HSN/SAC Wise Tax Summary</th> <th>Tax Rate</th> <th>IGST</th> <th>SGST</th> <th>CGST</th> </tr> ");
													
														List hsnList = (List) hsnData.get("hsnList");
														if (ApplicationUtility.getSize(hsnList) > 0) {
															
															for (int i = 0; i < hsnList.size(); i++) {
											 					htmlStringForPrint.append("<tr>").append("<td >" + ((Map) hsnList.get(i)).get("hsnCode") + "</td>")
											 							.append("<td align='right'><span>" + (int) Math.round((double) ((Map) hsnList.get(i)).get("taxRate")) + "%</span></td>")
																		.append("<td align='right' >" + ApplicationUtility.roundToFixValue((double) ((Map) hsnList.get(i)).get("igst"), 2) + "</td>")
																		.append("<td align='right' >" + ApplicationUtility.roundToFixValue((double) ((Map) hsnList.get(i)).get("sgst"), 2) + "</td>")
																		.append("<td align='right' >" + ApplicationUtility.roundToFixValue((double) ((Map) hsnList.get(i)).get("cgst"), 2) + "</td>").append("</tr>");
														 					
														 				}
														}
															htmlStringForPrint.append("<tr>").append("<td colspan = '2'>Total</td>")
															.append("<td  align='right'>" + ApplicationUtility.roundToFixValue(igstTotal, 2) + "</td>")
															.append("<td  align='right'>" + ApplicationUtility.roundToFixValue(sgstTotal, 2) + "</td>")
															.append("<td  align='right'>" + ApplicationUtility.roundToFixValue(cgstTotal, 2) + "</td>").append("</tr>")
															.append("</table>");
												}
												htmlStringForPrint.append("<div style='width:100%'>")
												.append("<table  style = 'border-collapse:collapse;' width = '100%' >")
												.append("<tr style='height: 70px;'>")
												.append("<td width='65%' style='height: 40px;text-align: center;border-right: 1px solid;border-bottom: 1px solid;'></th>")
												.append("<td width='10%' style='height: 40px;'></th>")
												.append("<td width='25%' style='height: 40px;text-align: center;vertical-align:bottom;border-left: 1px solid;border-bottom: 1px solid;'>Authorized Signature</th>")
												.append("</tr>").append("</table>").append("</div> ");
												
												htmlStringForPrint.append("<div style='border-bottom:1px solid black;font-size: 12px;'>").append(
														"<p style='margin: 4px 0px 0px 2px;'>* This is Computerized Invoice and requires no Signature</p>")
												.append("<p style='margin: 4px 0px 4px 2px;'>* All Disputes subject to ")
												.append(companyData.getCityName()).append(" Jurisdiction Only.</p>").append("</div>")
												.append("<div style='font-size: 12px;'>")
												.append("<p style='margin: 4px 0px 4px 2px;'>Declaration: We declare that this invoice shows the actual price of the goods described and that all particular are true and correct</p>")
												.append("</div>");

												htmlStringForPrint.append("</div> ");
												htmlStringForPrint.append("</div> ");
												/*
												 * htmlStringForPrint.append("<br />") .append("<div>")
												 * .append("Invoice is Generated using Billink. Visit us: www.Billink.in or Contact us: 9099757928"
												 * ) .append("</div>");
												 */
												return htmlStringForPrint.toString();
												 	}

	public List<LedgerSellDetailsModel> getLedgerSellDetails(int ledgerID) {
		return sellDAO.getledgerSellList(ledgerID);
	}

	public List<SellItemModel> getledgerSellDetail(int sellID) {
		return sellDAO.getledgerSellDetails(sellID);

	}

	@SuppressWarnings("unlikely-arg-type")
	private Map<String, Object> getHsnData(SellModel sellModel) {
		Map<String, Object> hsnData = new HashMap<>();
		List<SellItemModel> sellItemModelList = sellModel.getSellItemModel();
		List<SellFreightChargeModel> freightList = sellModel.getFreightList();
		Map<String, Map<Double, Map<String, Double>>> hsnTaxCodeList = new HashMap<>();

		Double totalIGST = 0.0;
		Double totalSGST = 0.0;
		Double totalCGST = 0.0;

		if (ApplicationUtility.getSize(sellItemModelList) > 0) {
			for (int i = 0; i < sellItemModelList.size(); i++) {
				SellItemModel sellItemObj = sellItemModelList.get(i);
				if (sellItemObj.getItem() != null) {
					String hsnCode = sellItemObj.getItem().getHsnCode() == 0 ? ""
							: String.valueOf(sellItemObj.getItem().getHsnCode());
					if (hsnTaxCodeList.get(hsnCode) == null) {
						hsnTaxCodeList.put(hsnCode, new HashMap<>());
					}
					Map<Double, Map<String, Double>> hsnTaxCodeWiseList = hsnTaxCodeList.get(hsnCode);
					Map<String, Double> hsnTaxCodeAndTaxRateWiseList = hsnTaxCodeWiseList.get(sellItemObj.getTaxRate());

					if (hsnTaxCodeAndTaxRateWiseList != null && hsnTaxCodeAndTaxRateWiseList.size() > 0) {
						hsnTaxCodeAndTaxRateWiseList.put("igst",
								hsnTaxCodeAndTaxRateWiseList.get("igst") + sellItemObj.getIgst());
						hsnTaxCodeAndTaxRateWiseList.put("cgst",
								hsnTaxCodeAndTaxRateWiseList.get("cgst") + sellItemObj.getCgst());
						hsnTaxCodeAndTaxRateWiseList.put("sgst",
								hsnTaxCodeAndTaxRateWiseList.get("sgst") + sellItemObj.getSgst());
					} else {
						Map<String, Double> test1 = new HashMap<>();
						test1.put("igst", sellItemObj.getIgst());
						test1.put("cgst", sellItemObj.getCgst());
						test1.put("sgst", sellItemObj.getSgst());
						hsnTaxCodeWiseList.put(sellItemObj.getTaxRate(), test1);
						hsnTaxCodeList.get(hsnCode).put(sellItemObj.getTaxRate(), test1);
					}
					totalIGST += sellItemObj.getIgst();
					totalCGST += sellItemObj.getCgst();
					totalSGST += sellItemObj.getSgst();
				}
			}
		}
		if (ApplicationUtility.getSize(freightList) > 0) {
			for (int i = 0; i < freightList.size(); i++) {
				SellFreightChargeModel sellFreightChargeModel = freightList.get(i);
				if (sellFreightChargeModel.getLedger() != null) {

					sellFreightChargeModel.getLedger()
							.setSacCode((sellFreightChargeModel.getLedger().getSacCode() == null
									|| sellFreightChargeModel.getLedger().getSacCode() == "") ? "0"
											: sellFreightChargeModel.getLedger().getSacCode());

					if (hsnTaxCodeList.get(sellFreightChargeModel.getLedger().getSacCode()) == null) {
						hsnTaxCodeList.put(sellFreightChargeModel.getLedger().getSacCode() + "",
								new HashMap<Double, Map<String, Double>>());
					}
					Map<Double, Map<String, Double>> test = hsnTaxCodeList
							.get(sellFreightChargeModel.getLedger().getSacCode());
					Map<String, Double> freightAndTaxRateWiseList = test.get(sellFreightChargeModel.getTaxRate());
					if (freightAndTaxRateWiseList != null && freightAndTaxRateWiseList.size() > 0) {
						freightAndTaxRateWiseList.put("igst",
								freightAndTaxRateWiseList.get("igst") + sellFreightChargeModel.getIgst());
						freightAndTaxRateWiseList.put("cgst",
								freightAndTaxRateWiseList.get("cgst") + sellFreightChargeModel.getCgst());
						freightAndTaxRateWiseList.put("sgst",
								freightAndTaxRateWiseList.get("sgst") + sellFreightChargeModel.getSgst());
					} else {
						Map<String, Double> test1 = new HashMap<>();
						test1.put("igst", sellFreightChargeModel.getIgst());
						test1.put("cgst", sellFreightChargeModel.getCgst());
						test1.put("sgst", sellFreightChargeModel.getSgst());
						test.put(sellFreightChargeModel.getTaxRate(), test1);
						hsnTaxCodeList.get(sellFreightChargeModel.getLedger().getSacCode())
								.put(sellFreightChargeModel.getTaxRate(), test1);
					}
					totalIGST += sellFreightChargeModel.getIgst();
					totalCGST += sellFreightChargeModel.getCgst();
					totalSGST += sellFreightChargeModel.getSgst();
				}
			}
		}

		return hsnData;
	}

	public List<CreditNoteGetItem> getItemName(String itemName, String arr) {
		return sellDAO.getItemDataListForDebitNote(itemName, arr);
	}

}
