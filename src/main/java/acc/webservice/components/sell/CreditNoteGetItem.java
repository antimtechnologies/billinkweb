package acc.webservice.components.sell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import acc.webservice.components.brand.ItemBrandModel;
import acc.webservice.components.itemcategory.ItemCategoryModel;
import acc.webservice.components.items.ItemAliasModel;
import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemDocumentModel;
import acc.webservice.components.items.ItemMrpModel;
import acc.webservice.components.items.ItemPurchaseRateModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.DatabaseEnum.DEBITNOTE_ITEM_DETAILS_COLUMN;

public class CreditNoteGetItem {
	
	
	private int itemId;
	private String itemName;
	private String itemAlias;

	private int hsnCode;
	private double taxCode;

	private double quantity;
	private double sellRate;
	private String profilePhotoURL;
	private double discount;
	private boolean measureDiscountInAmount;
	private boolean addmeasureDiscountInAmount;
	private double additionaldiscount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private int locationId;
	private String locationName;
	private int batchId;
	private String batchNo;
	private String mfgDate,expDate;
	
	
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemAlias() {
		return itemAlias;
	}
	public void setItemAlias(String itemAlias) {
		this.itemAlias = itemAlias;
	}
	
	public int getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(int hsnCode) {
		this.hsnCode = hsnCode;
	}
	public double getTaxCode() {
		return taxCode;
	}
	public void setTaxCode(double taxCode) {
		this.taxCode = taxCode;
	}
	
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public double getSellRate() {
		return sellRate;
	}
	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}
	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}
	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}
	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}
	public boolean isAddmeasureDiscountInAmount() {
		return addmeasureDiscountInAmount;
	}
	public void setAddmeasureDiscountInAmount(boolean addmeasureDiscountInAmount) {
		this.addmeasureDiscountInAmount = addmeasureDiscountInAmount;
	}
	public double getAdditionaldiscount() {
		return additionaldiscount;
	}
	public void setAdditionaldiscount(double additionaldiscount) {
		this.additionaldiscount = additionaldiscount;
	}
	public double getItemAmount() {
		return itemAmount;
	}
	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}
	public double getCgst() {
		return cgst;
	}
	public void setCgst(double cgst) {
		this.cgst = cgst;
	}
	public double getSgst() {
		return sgst;
	}
	public void setSgst(double sgst) {
		this.sgst = sgst;
	}
	public double getIgst() {
		return igst;
	}
	public void setIgst(double igst) {
		this.igst = igst;
	}
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public int getBatchId() {
		return batchId;
	}
	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
	
	
	
	
	
	
	
}
