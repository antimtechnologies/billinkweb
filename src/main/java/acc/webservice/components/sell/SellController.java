package acc.webservice.components.sell;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.components.users.UserService;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.sell.utility.SellToPurchaseUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_ITEM_DATA_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@RestController
@RequestMapping("/sellData/")
public class SellController {

	@Autowired
	private SellService sellService;

	@Autowired
	private SellToPurchaseUtility sellToPurchaseUtility;

	private String ip = "http://localhost:8090/";

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getSellListData", method = RequestMethod.POST)
	public Map<String, Object> getSellListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getSellListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getSellDetailsById", method = RequestMethod.POST)
	Map<String, Object> getSellDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellId = ApplicationUtility.getIntValue(parameters, SELL_DETAILS_COLUMN.SELL_ID.toString());
		System.err.println("CompanyId: " + companyId);
		System.err.println("sellId: " + sellId);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getSellDetailsById(companyId, sellId));
		return responseData;
	}

	@RequestMapping(value = "saveSellData", method = RequestMethod.POST)
	public Map<String, Object> saveSellData(@RequestBody SellModel sellModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.saveSellData(sellModel));
		return responseData;
	}

	@RequestMapping(value = "publishSellData", method = RequestMethod.POST)
	public Map<String, Object> publishSellData(@RequestBody SellModel sellModel) throws AccountingSofwareException {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				sellModel.getSellDate())) {
			sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.publishSellData(sellModel));

		try {
			sellToPurchaseUtility.savePurchaseForSelectedLedger(sellModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "updateSavedSellData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedSellData(@RequestBody SellModel sellModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				sellModel.getSellDate())) {
			sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.updateSavedSellData(sellModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedSellData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedSellData(@RequestBody SellModel sellModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				sellModel.getSellDate())) {
			sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.updatePublishedSellData(sellModel));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedSellData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedSellData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellId = ApplicationUtility.getIntValue(parameters, SELL_DETAILS_COLUMN.SELL_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.deleteSavedSellData(companyId, sellId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedSellData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishSellData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int sellId = ApplicationUtility.getIntValue(parameters, SELL_DETAILS_COLUMN.SELL_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.deletePublishedSellData(companyId, sellId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "markSellAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markSellAsVerified(@RequestBody SellModel sellModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.markSellAsVerified(sellModel));
		return responseData;
	}

	@RequestMapping(value = "publishSavedSell", method = RequestMethod.POST)
	public Map<String, Object> publishSavedPurchase(@RequestBody SellModel sellModel)
			throws AccountingSofwareException {
		if (ApplicationUtility.getSize(sellModel.getSellDate()) > 0) {
			sellModel.setSellDate(DateUtility.convertDateFormat(sellModel.getSellDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.publishSavedSell(sellModel));

		try {
			if (sellModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
				sellToPurchaseUtility.savePurchaseForSelectedLedger(sellModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "getPrintHTMLString", method = RequestMethod.POST)
	public Map<String, Object> getPrintHTMLString(@RequestBody SellModel sellModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getPrintHTMLString(sellModel));
		return responseData;
	}

	@RequestMapping(value = "getsellDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSalesDetails(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;

		if (request.getParameter("companyId") != null && request.getParameter("ledgerId") != null
				&& request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = ip + "user/salesfreight/findsaledetails?companyId=" + companyId;
		} else if (request.getParameter("companyId") != null && request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			String billNumber = request.getParameter("billNumber");
			fooResourceUrl = ip + "user/salesfreight/findsaledetails?companyId=" + companyId + "&billNumber"
					+ billNumber;
		} else if (request.getParameter("companyId") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl = ip + "user/salesfreight/findsaledetails?companyId=" + companyId;
		} else if (request.getParameter("billNumber") != null) {
			String billNumber = request.getParameter("companyId");
			fooResourceUrl = ip + "user/salesfreight/findsaledetails?billNumber=" + billNumber;
		} else {
			fooResourceUrl = ip + "user/salesfreight/findsaledetails";
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;

	}

	@RequestMapping(value = "findbyday", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("day") != null) {
			int day = Integer.parseInt(request.getParameter("day"));
			fooResourceUrl = ip + "user/sales/findbyday?day=" + day;
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findbymonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("month") != null) {
			int month = Integer.parseInt(request.getParameter("month"));
			fooResourceUrl = ip + "user/sales/findbymonth?month=" + month;
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "getsellItemDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSellItemDetail(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;

		if (request.getParameter("userId") != null) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			fooResourceUrl = ip + "user/salesitemdata/?userId=" + userId;
		} else if (request.getParameter("totalItemAmount") != null) {
			Double totalItemAmount = Double.parseDouble(request.getParameter("totalItemAmount"));
			fooResourceUrl = ip + "user/salesitemdata/?totalItemAmount=" + totalItemAmount;
		} else if (request.getParameter("quantity") != null) {
			Double quantity = Double.parseDouble(request.getParameter("quantity"));
			fooResourceUrl = ip + "user/salesitemdata/?quantity=" + quantity;
		} else if (request.getParameter("purchaseRate") != null) {
			Double purchaseRate = Double.parseDouble(request.getParameter("purchaseRate"));
			fooResourceUrl = ip + "user/salesitemdata/?quantity=" + purchaseRate;
		} else {
			fooResourceUrl = ip + "user/salesitemdata/";
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;

	}

	@RequestMapping(value = "findtotalamountbycompanyandday", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalSellAmountByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("day") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = ip + "user/salesitemdata/findtotalamountbycompanyandday?companyId=" + companyId
						+ "&day=" + day;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findtotalamountbycompanyandmonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalSellAmountByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("month") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = ip + "user/salesitemdata/findtotalamountbycompanyandday?companyId=" + companyId
						+ "&month=" + month;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findAllPurchasebycompanyandday", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findAllSellDetailBycompanyAndByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("day") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = ip + "user/salesitemdata/findallsalesbycompanyandday?companyId=" + companyId + "&day="
						+ day;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findAllPurchasebycompanyandmonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findAllsellDetailByCompanyAndMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("month") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = ip + "user/salesitemdata/findallsalesbycompanyandmonth?companyId=" + companyId
						+ "&month=" + month;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	// 14-05-2019 Start
	@RequestMapping(value = "getLedgerSellDetails", method = RequestMethod.POST)
	public Map<String, Object> getLedgerSellDetails(@RequestBody Map<String, Object> parameters) {
		int ledgerId = ApplicationUtility.getIntValue(parameters, SELL_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getLedgerSellDetails(ledgerId));
		return responseData;
	}

	@RequestMapping(value = "getledgerItemSellList", method = RequestMethod.POST)
	public Map<String, Object> getledgerItemsellList(@RequestBody Map<String, Object> parameters) {
		int sellID = ApplicationUtility.getIntValue(parameters, SELL_ITEM_DATA_COLUMN.SELL_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getledgerSellDetail(sellID));
		return responseData;
	}

	// 14-05-2019 End
	@RequestMapping(value = "getItemNameForCredit", method = RequestMethod.POST)
	public Map<String, Object> getItemNameForDebit(@RequestBody Map<String, Object> parameters) {
		String itemName = ApplicationUtility.getStrValue(parameters, ITEM_DETAILS_COLUMN.ITEM_NAME.toString());
		String saleID = ApplicationUtility.getStrValue(parameters, SELL_ITEM_DATA_COLUMN.SELL_ID.toString());
		saleID = saleID + "0";
		// String val = parameters.values().toString().replace("[", "").replace("]",
		// "");
		// val=val.substring(0,val.length()-1);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", sellService.getItemName(itemName, saleID));
		return responseData;
	}

}
