package acc.webservice.components.sell.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.sell.SellFreightChargeModel;
import acc.webservice.components.sell.SellImageModel;
import acc.webservice.components.sell.SellItemModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class SellModel {

	private int sellId;
	private String imageURL;
	private String billNumber;
	private String description;
	private StatusModel status;
	private String sellDate;
	private LedgerModel ledgerData;
	private LedgerModel buyerLedgerData;
	private double totalBillAmount;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel verifiedBy;
	private String verifiedDateTime;
	private double grosssTotalAmount;
	private double discount;
	private boolean sellMeasureDiscountInAmount;
	private byte paymentStatus;
	private boolean deleted;
	private boolean verified;
	private UserModel deletedBy;
	private String deletedDateTime;
	private int companyId;
	private String referenceNumber1;
	private String referenceNumber2;
	private List<SellItemModel> sellItemModel;
	private List<SellFreightChargeModel> freightList;
	private double freightCharge;
	private List<SellImageModel> imageURLList;
	private String billNumberPrefix;
	private int currentBillNumber;
	private int excellDocumentId;
	private long orderId;
	private long subOrderId;
	private String voucherType;
	private String dispatchDate,poNo,lrNo,eWayBillNo;
	private double actualWeight;
	private String cityName;
	private String gstNumber;
	
	public SellModel() {
	}

	public SellModel(ResultSet rs) throws SQLException {
		setSellId(rs.getInt(SELL_DETAILS_COLUMN.SELL_ID.toString()));
		setReferenceNumber1(rs.getString(SELL_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(SELL_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));
		String imageURL = rs.getString(SELL_DETAILS_COLUMN.IMAGE_URL.toString());
		
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
		
		setCityName(rs.getString(SELL_DETAILS_COLUMN.CITY_NAME.toString()));
		setGstNumber(rs.getString(SELL_DETAILS_COLUMN.GST_NUMBER.toString()));
		setBillNumber(rs.getString(SELL_DETAILS_COLUMN.BILL_NUMBER.toString()));
		setDescription(rs.getString(SELL_DETAILS_COLUMN.DESCRIPTION.toString()));
		setTotalBillAmount(rs.getDouble(SELL_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));
		setGrosssTotalAmount(rs.getDouble(SELL_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString()));
		setPaymentStatus(rs.getByte(SELL_DETAILS_COLUMN.PAYMENT_STATUS.toString()));
		setDiscount(rs.getDouble(SELL_DETAILS_COLUMN.DISCOUNT.toString()));
		setFreightCharge(rs.getDouble(SELL_DETAILS_COLUMN.FREIGHT_CHARGE.toString()));
		setVerified(rs.getBoolean(SELL_DETAILS_COLUMN.IS_VERIFIED.toString()));
		setSellMeasureDiscountInAmount(rs.getBoolean(SELL_DETAILS_COLUMN.SELL_MEASURE_DISCOUNT_IN_AMONT.toString()));
		setBillNumberPrefix(rs.getString(SELL_DETAILS_COLUMN.BILL_NUMBER_PREFIX.toString()));
		setCurrentBillNumber(rs.getInt(SELL_DETAILS_COLUMN.CURRENT_BILL_NUMBER.toString()));
		setPoNo(rs.getString(SELL_DETAILS_COLUMN.PO_NO.toString()));
		seteWayBillNo(rs.getString(SELL_DETAILS_COLUMN.E_WAY_BILL_NO.toString()));
		setActualWeight(rs.getDouble(SELL_DETAILS_COLUMN.ACTUAL_WEIGHT.toString()));
		setLrNo(rs.getString(SELL_DETAILS_COLUMN.LR_NO.toString()));
		Timestamp fetchedDispatchDate = rs.getTimestamp(SELL_DETAILS_COLUMN.DISPATCH_DATE.toString());
		if (fetchedDispatchDate != null) {
			setDispatchDate(DateUtility.converDateToUserString(fetchedDispatchDate));
		}
		Timestamp fetchedSellDate = rs.getTimestamp(SELL_DETAILS_COLUMN.SELL_BILL_DATE.toString());
		if (fetchedSellDate != null) {
			setSellDate(DateUtility.converDateToUserString(fetchedSellDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(SELL_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(SELL_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerfiedDate = rs.getTimestamp(SELL_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerfiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerfiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		LedgerModel fetchedLedgerData = new LedgerModel();
		fetchedLedgerData.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedgerData.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedgerData.setState(new StateModel(rs));
		fetchedLedgerData.setGstNumber(rs.getString(LEDGER_DETAILS_COLUMN.GST_NUMBER.toString()));
		fetchedLedgerData.setAddress(rs.getString(LEDGER_DETAILS_COLUMN.ADDRESS.toString()));
		fetchedLedgerData.setPanNumber(rs.getString(LEDGER_DETAILS_COLUMN.PAN_NUMBER.toString()));
		fetchedLedgerData.setCityName(rs.getString(LEDGER_DETAILS_COLUMN.CITY_NAME.toString()));
		fetchedLedgerData.setPriceId(rs.getInt(LEDGER_DETAILS_COLUMN.PRICE_ID.toString()));
		fetchedLedgerData.setSchemeOneId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_ONE_ID.toString()));
		fetchedLedgerData.setSchemeTwoId(rs.getBoolean(LEDGER_DETAILS_COLUMN.SCHEME_TWO_ID.toString()));
		setLedgerData(fetchedLedgerData);

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		setStatus(new StatusModel(rs));
	}

	public int getSellId() {
		return sellId;
	}

	public void setSellId(int sellId) {
		this.sellId = sellId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public String getSellDate() {
		return sellDate;
	}

	public void setSellDate(String sellDate) {
		this.sellDate = sellDate;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public double getTotalBillAmount() {
		return totalBillAmount;
	}

	public void setTotalBillAmount(double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<SellItemModel> getSellItemModel() {
		return sellItemModel;
	}

	public void setSellItemModel(List<SellItemModel> sellItemModel) {
		this.sellItemModel = sellItemModel;
	}

	public double getFreightCharge() {
		return freightCharge;
	}

	public void setFreightCharge(double freightCharge) {
		this.freightCharge = freightCharge;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isSellMeasureDiscountInAmount() {
		return sellMeasureDiscountInAmount;
	}

	public void setSellMeasureDiscountInAmount(boolean sellMeasureDiscountInAmount) {
		this.sellMeasureDiscountInAmount = sellMeasureDiscountInAmount;
	}

	public List<SellFreightChargeModel> getFreightList() {
		return freightList;
	}

	public void setFreightList(List<SellFreightChargeModel> freightList) {
		this.freightList = freightList;
	}

	public List<SellImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<SellImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public double getGrosssTotalAmount() {
		return grosssTotalAmount;
	}

	public void setGrosssTotalAmount(double grosssTotalAmount) {
		this.grosssTotalAmount = grosssTotalAmount;
	}

	public String getBillNumberPrefix() {
		return billNumberPrefix;
	}

	public void setBillNumberPrefix(String billNumberPrefix) {
		this.billNumberPrefix = billNumberPrefix;
	}

	public int getCurrentBillNumber() {
		return currentBillNumber;
	}

	public void setCurrentBillNumber(int currentBillNumber) {
		this.currentBillNumber = currentBillNumber;
	}
	public LedgerModel getBuyerLedgerData() {
		return buyerLedgerData;
	}

	public void setBuyerLedgerData(LedgerModel buyerLedgerData) {
		this.buyerLedgerData = buyerLedgerData;
	}

	public int getExcellDocumentId() {
		return excellDocumentId;
	}

	public void setExcellDocumentId(int excellDocumentId) {
		this.excellDocumentId = excellDocumentId;
	}
	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getSubOrderId() {
		return subOrderId;
	}

	public void setSubOrderId(long subOrderId) {
		this.subOrderId = subOrderId;
	}
	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	


	public String getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(String dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public String geteWayBillNo() {
		return eWayBillNo;
	}

	public void seteWayBillNo(String eWayBillNo) {
		this.eWayBillNo = eWayBillNo;
	}

	public double getActualWeight() {
		return actualWeight;
	}

	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}

	@Override
	public String toString() {
		return "SellModel [sellId=" + sellId + ", imageURL=" + imageURL + ", billNumber=" + billNumber
				+ ", description=" + description + ", status=" + status + ", sellDate=" + sellDate + ", ledgerData="
				+ ledgerData + ", totalBillAmount=" + totalBillAmount + ", addedDateTime=" + addedDateTime
				+ ", modifiedDateTime=" + modifiedDateTime + ", verifiedDateTime=" + verifiedDateTime
				+ ", paymentStatus=" + paymentStatus + ", discount=" + discount + ", deleted=" + deleted
				+ ", deletedDateTime=" + deletedDateTime + "]";
	}

}
