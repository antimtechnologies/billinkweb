package acc.webservice.components.sell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.SELL_ITEM_DATA_COLUMN;

@Repository
@Lazy
public class SellItemDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] saveSellItemsMappingData(List<SellItemModel> sellItemList, int companyId, int sellId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_ITEM_DATA))
				.append(String.format(" ( %s, %s, %s, %s, %s, ", SELL_ITEM_DATA_COLUMN.ITEM_ID,
						SELL_ITEM_DATA_COLUMN.QUANTITY, SELL_ITEM_DATA_COLUMN.SELL_RATE,
						SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT, SELL_ITEM_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT,
						SELL_ITEM_DATA_COLUMN.I_GST, SELL_ITEM_DATA_COLUMN.C_GST, SELL_ITEM_DATA_COLUMN.S_GST))
				.append(String.format(" %s, %s, %s, %s , %s , %s , %s , %s , %s, %s, %s)", SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT,
						SELL_ITEM_DATA_COLUMN.SELL_ID, SELL_ITEM_DATA_COLUMN.TAX_RATE,
						SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT, SELL_ITEM_DATA_COLUMN.EXCELL_DOCUMENT_ID,
						SELL_ITEM_DATA_COLUMN.FREE_ITEM_ID, SELL_ITEM_DATA_COLUMN.FREE_ITEM_QTY,
						SELL_ITEM_DATA_COLUMN.BATCH_ID,SELL_ITEM_DATA_COLUMN.LOCTION_ID,
						SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, :%s, ", SELL_ITEM_DATA_COLUMN.ITEM_ID,
						SELL_ITEM_DATA_COLUMN.QUANTITY, SELL_ITEM_DATA_COLUMN.SELL_RATE,
						SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT, SELL_ITEM_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT,
						SELL_ITEM_DATA_COLUMN.I_GST, SELL_ITEM_DATA_COLUMN.C_GST, SELL_ITEM_DATA_COLUMN.S_GST))
				.append(String.format(" :%s, :%s, :%s, :%s, :%s, :%s, :%s, :%s ,:%s,:%s,:%s ) ", SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT,
						SELL_ITEM_DATA_COLUMN.SELL_ID, SELL_ITEM_DATA_COLUMN.TAX_RATE,
						SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT, SELL_ITEM_DATA_COLUMN.EXCELL_DOCUMENT_ID,
						SELL_ITEM_DATA_COLUMN.FREE_ITEM_ID, SELL_ITEM_DATA_COLUMN.FREE_ITEM_QTY,
						SELL_ITEM_DATA_COLUMN.BATCH_ID,SELL_ITEM_DATA_COLUMN.LOCTION_ID,
						SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> sellItemMappingList = new ArrayList<>();
		Map<String, Object> sellItemMap;
		for (SellItemModel sellItem : sellItemList) {

			if (sellItem.getItem() == null || sellItem.getItem().getItemId() <= 0) {
				continue;
			}
			System.err.println("Location ID :"+sellItem.getLocationId());
			sellItemMap = new HashMap<>();
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.ITEM_ID.toString(), sellItem.getItem().getItemId());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.QUANTITY.toString(), sellItem.getQuantity());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.SELL_RATE.toString(), sellItem.getSellRate());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString(), sellItem.getDiscount());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), sellItem.getTotalItemAmount());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(),
					sellItem.isMeasureDiscountInAmount());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.TAX_RATE.toString(), sellItem.getTaxRate());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString(), sellItem.getItemAmount());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.I_GST.toString(), sellItem.getIgst());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.S_GST.toString(), sellItem.getSgst());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.C_GST.toString(), sellItem.getCgst());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.EXCELL_DOCUMENT_ID.toString(), sellItem.getExcellDocumentId());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.SELL_ID.toString(), sellId);
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.FREE_ITEM_ID.toString(), sellItem.getFreeItemId());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.FREE_ITEM_QTY.toString(), sellItem.getFreeItemQty());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.BATCH_ID.toString(), sellItem.getBatchId());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.LOCTION_ID.toString(), sellItem.getLocationId());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString(), sellItem.getAdditionaldiscount());
			sellItemMap.put(SELL_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString(), sellItem.isAddmeasureDiscountInAmount());
			sellItemMappingList.add(sellItemMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), sellItemMappingList.toArray(new HashMap[0]));
	}

	int deleteSellItemMapping(int companyId, int sellId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_SELL_ITEM_DATA,
				SELL_ITEM_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", SELL_ITEM_DATA_COLUMN.SELL_ID, SELL_ITEM_DATA_COLUMN.SELL_ID))
				.append(String.format(" AND %s = ? ", SELL_ITEM_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(),
				new Object[] { 1, sellId, companyId });
	}

}
