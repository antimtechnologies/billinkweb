package acc.webservice.components.otherdocs;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.OTHER_DOCS_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class OtherDocsImageModel {

	private int imageURLId;
	private boolean deleted;
	private String imageURL;
	private OtherDocsModel otherDocsModel;

	public OtherDocsImageModel() {
	}

	public OtherDocsImageModel(ResultSet rs) throws SQLException {

		setImageURLId(rs.getInt(OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL_ID.toString()));
		String documentURL = rs.getString(OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(documentURL)) {
			setImageURL(ApplicationUtility.getServerURL() + documentURL);
		}

	}

	public int getImageURLId() {
		return imageURLId;
	}

	public void setImageURLId(int imageURLId) {
		this.imageURLId = imageURLId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public OtherDocsModel getOtherDocsModel() {
		return otherDocsModel;
	}

	public void setOtherDocsModel(OtherDocsModel otherDocsModel) {
		this.otherDocsModel = otherDocsModel;
	}

}
