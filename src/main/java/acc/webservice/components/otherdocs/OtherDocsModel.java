package acc.webservice.components.otherdocs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.OTHER_DOCS_COLUMN;
import acc.webservice.global.utils.DateUtility;

public class OtherDocsModel {

	private int otherDocsId;
	private String description;
	private String imageURL;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private int companyId;
	private List<OtherDocsImageModel> otherDocImageURLList;

	public OtherDocsModel(ResultSet rs) throws SQLException {
		UserModel objAddedBy = new UserModel();
		setOtherDocsId(rs.getInt(OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString()));
		setImageURL(rs.getString(OTHER_DOCS_COLUMN.IMAGE_URL.toString()));
		setDescription(rs.getString(OTHER_DOCS_COLUMN.DESCRPITION.toString()));

		objAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		objAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(objAddedBy);
		Timestamp fetchedAddedDateTime = rs.getTimestamp(OTHER_DOCS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDateTime));

		UserModel objModifiedBy = new UserModel();
		objModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		objModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(objModifiedBy);
		Timestamp fetchedModifiedDateTime = rs.getTimestamp(OTHER_DOCS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedModifiedDateTime != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedModifiedDateTime));
		}
	}

	public OtherDocsModel() {
	}

	public int getOtherDocsId() {
		return otherDocsId;
	}

	public void setOtherDocsId(int otherDocsId) {
		this.otherDocsId = otherDocsId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<OtherDocsImageModel> getOtherDocImageURLList() {
		return otherDocImageURLList;
	}

	public void setOtherDocImageURLList(List<OtherDocsImageModel> otherDocImageURLList) {
		this.otherDocImageURLList = otherDocImageURLList;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "OtherDocsModel [otherDocId=" + otherDocsId + ", description=" + description + ", imageURL=" + imageURL
				+ ", addedBy=" + addedBy + ", addedDateTime=" + addedDateTime + ", deletedBy=" + deletedBy
				+ ", deletedDateTime=" + deletedDateTime + ", companyId=" + companyId + "]";
	}

}
