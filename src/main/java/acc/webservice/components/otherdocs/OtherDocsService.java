package acc.webservice.components.otherdocs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class OtherDocsService {

	@Autowired
	private OtherDocsDAO otherDocsDAO;

	@Autowired
	private OtherDocsImageDAO otherDocsImageDAO;

	public List<OtherDocsModel> getOtherDocsListData(int companyId, int start, int numberOfRecord) {
		return otherDocsDAO.getOtherDocsListData(companyId, start, numberOfRecord);
	}

	public OtherDocsModel getOtherDocsDetailsById(int companyId, int otherDocsId) {
		OtherDocsModel otherDocsModel = otherDocsDAO.getOtherDocsDetailsById(companyId, otherDocsId);
		otherDocsModel.setOtherDocImageURLList(otherDocsImageDAO.getOtherDocImageURLList(otherDocsId));
		return otherDocsModel;
	}

	public OtherDocsModel saveOtherDocsData(OtherDocsModel otherDocsModel) {
		otherDocsDAO.saveOtherDocsData(otherDocsModel);
		if (ApplicationUtility.getSize(otherDocsModel.getOtherDocImageURLList()) > 0) {
			otherDocsImageDAO.saveOtherDocImageData(otherDocsModel.getOtherDocImageURLList(), otherDocsModel.getOtherDocsId());
		}

		return otherDocsModel;
	}

	public OtherDocsModel updateOtherDocsData(OtherDocsModel otherDocsModel) {

		otherDocsImageDAO.deleteOtherDocImageData(otherDocsModel.getOtherDocsId());
		if (ApplicationUtility.getSize(otherDocsModel.getOtherDocImageURLList()) > 0) {
			otherDocsImageDAO.saveOtherDocImageData(otherDocsModel.getOtherDocImageURLList(), otherDocsModel.getOtherDocsId());
		}
		return otherDocsDAO.updateOtherDocsData(otherDocsModel);
	}

	public int deleteOtherDocsData(int companyId, int otherDocsId, int deletedBy) {
		otherDocsImageDAO.deleteOtherDocImageData(otherDocsId);
		return otherDocsDAO.deleteOtherDocsData(companyId, otherDocsId, deletedBy);
	}
}
