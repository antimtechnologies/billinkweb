package acc.webservice.components.otherdocs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.OTHER_DOCS_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class OtherDocsDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<OtherDocsModel> getOtherDocsListData(int companyId, int start, int numberOfRecord) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT OD.%s, OD.%s, OD.%s, OD.%s, OD.%s, ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_COLUMN.IMAGE_URL, OTHER_DOCS_COLUMN.DESCRPITION, OTHER_DOCS_COLUMN.ADDED_DATE_TIME, OTHER_DOCS_COLUMN.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" FROM %s OD ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS))
						.append(String.format(" INNER JOIN %s AUM ON OD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, OTHER_DOCS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON OD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, OTHER_DOCS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE OD.%s = :%s ", OTHER_DOCS_COLUMN.COMPANY_ID, OTHER_DOCS_COLUMN.COMPANY_ID))
						.append(String.format(" AND OD.%s = :%s ", OTHER_DOCS_COLUMN.IS_DELETED, OTHER_DOCS_COLUMN.IS_DELETED))
						.append(String.format(" ORDER BY OD.%s DESC LIMIT :start, :noOfRecord ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(OTHER_DOCS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(OTHER_DOCS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getOtherDocsDetailsListExtractor());
	}

	private ResultSetExtractor<List<OtherDocsModel>> getOtherDocsDetailsListExtractor() {

		return new ResultSetExtractor<List<OtherDocsModel>>() {
			@Override
			public List<OtherDocsModel> extractData(ResultSet rs) throws SQLException {
				List<OtherDocsModel> otherDocList = new ArrayList<>();
				while (rs.next()) {
					otherDocList.add(new OtherDocsModel(rs));
				}
				return otherDocList;
			}
		};
	}

	OtherDocsModel getOtherDocsDetailsById(int companyId, int otherDocId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT OD.%s, OD.%s, OD.%s, OD.%s, OD.%s, ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_COLUMN.IMAGE_URL, OTHER_DOCS_COLUMN.DESCRPITION, OTHER_DOCS_COLUMN.ADDED_DATE_TIME, OTHER_DOCS_COLUMN.DELETED_DATE_TIME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" FROM %s OD ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS))
						.append(String.format(" INNER JOIN %s AUM ON OD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, OTHER_DOCS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON OD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, OTHER_DOCS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE OD.%s = :%s ", OTHER_DOCS_COLUMN.COMPANY_ID, OTHER_DOCS_COLUMN.COMPANY_ID))
						.append(String.format(" AND OD.%s = :%s ", OTHER_DOCS_COLUMN.IS_DELETED, OTHER_DOCS_COLUMN.IS_DELETED))
						.append(String.format(" AND OD.%s = :%s ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_COLUMN.OTHER_DOCS_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString(), otherDocId);
		parameters.put(OTHER_DOCS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(OTHER_DOCS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getOtherDocsDetailExtractor());
	}

	private ResultSetExtractor<OtherDocsModel> getOtherDocsDetailExtractor() {

		return new ResultSetExtractor<OtherDocsModel>() {
			@Override
			public OtherDocsModel extractData(ResultSet rs) throws SQLException {
				OtherDocsModel otherDocDetails = new OtherDocsModel();
				while (rs.next()) {
					otherDocDetails = new OtherDocsModel(rs);
				}
				return otherDocDetails;
			}
		};
	}

	OtherDocsModel saveOtherDocsData(OtherDocsModel otherDocModel) {
		StringBuilder sqlQueryCompanySave = new StringBuilder();
		sqlQueryCompanySave.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS))
							.append(String.format("( %s, %s, %s, ", OTHER_DOCS_COLUMN.DESCRPITION, OTHER_DOCS_COLUMN.IMAGE_URL, OTHER_DOCS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s )", OTHER_DOCS_COLUMN.ADDED_BY, OTHER_DOCS_COLUMN.ADDED_DATE_TIME))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, ",OTHER_DOCS_COLUMN.DESCRPITION, OTHER_DOCS_COLUMN.IMAGE_URL, OTHER_DOCS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s )", OTHER_DOCS_COLUMN.ADDED_BY, OTHER_DOCS_COLUMN.ADDED_DATE_TIME));

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(OTHER_DOCS_COLUMN.DESCRPITION.toString(), otherDocModel.getDescription())
				.addValue(OTHER_DOCS_COLUMN.COMPANY_ID.toString(), otherDocModel.getCompanyId())
				.addValue(OTHER_DOCS_COLUMN.IMAGE_URL.toString(), otherDocModel.getImageURL())
				.addValue(OTHER_DOCS_COLUMN.ADDED_BY.toString(), otherDocModel.getAddedBy().getUserId())
				.addValue(OTHER_DOCS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		otherDocModel.setOtherDocsId(holder.getKey().intValue());
		return otherDocModel;
	}

	OtherDocsModel updateOtherDocsData(OtherDocsModel otherDocModel) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS))
							.append(String.format(" %s = :%s, ", OTHER_DOCS_COLUMN.DESCRPITION, OTHER_DOCS_COLUMN.DESCRPITION))
							.append(String.format(" %s = :%s ", OTHER_DOCS_COLUMN.IMAGE_URL, OTHER_DOCS_COLUMN.IMAGE_URL))
							.append(String.format(" WHERE %s = :%s  ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_COLUMN.OTHER_DOCS_ID))
							.append(String.format(" AND %s = :%s ", OTHER_DOCS_COLUMN.COMPANY_ID, OTHER_DOCS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", OTHER_DOCS_COLUMN.IS_DELETED, OTHER_DOCS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(OTHER_DOCS_COLUMN.DESCRPITION.toString(), otherDocModel.getDescription())
				.addValue(OTHER_DOCS_COLUMN.IMAGE_URL.toString(), otherDocModel.getImageURL())
				.addValue(OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString(), otherDocModel.getOtherDocsId())
				.addValue(OTHER_DOCS_COLUMN.IS_DELETED.toString(), 0)
				.addValue(OTHER_DOCS_COLUMN.COMPANY_ID.toString(), otherDocModel.getCompanyId());

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
		return otherDocModel;
	}

	int deleteOtherDocsData(int companyId, int otherDocId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS))
							.append(String.format(" %s = :%s, ", OTHER_DOCS_COLUMN.IS_DELETED, OTHER_DOCS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", OTHER_DOCS_COLUMN.DELETED_BY, OTHER_DOCS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", OTHER_DOCS_COLUMN.DELETED_DATE_TIME, OTHER_DOCS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", OTHER_DOCS_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_COLUMN.OTHER_DOCS_ID))
							.append(String.format(" AND %s = :%s ", OTHER_DOCS_COLUMN.COMPANY_ID, OTHER_DOCS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString(), otherDocId)
				.addValue(OTHER_DOCS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(OTHER_DOCS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(OTHER_DOCS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(OTHER_DOCS_COLUMN.COMPANY_ID.toString(), companyId);


		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

}
