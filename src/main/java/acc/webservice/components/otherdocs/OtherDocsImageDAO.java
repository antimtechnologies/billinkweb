package acc.webservice.components.otherdocs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.OTHER_DOCS_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class OtherDocsImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<OtherDocsImageModel> getOtherDocImageURLList(int otherDocsId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s ", OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL_ID, OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL))
			.append(String.format(" FROM %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s AND %s = 0  ", OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID.toString(), otherDocsId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getOtherDocImageResultExctractor());
	}

	private ResultSetExtractor<List<OtherDocsImageModel>> getOtherDocImageResultExctractor() {
		return new ResultSetExtractor<List<OtherDocsImageModel>>() {
			@Override
			public List<OtherDocsImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<OtherDocsImageModel> otherDocsDocumentModelList = new ArrayList<>();
				while (rs.next()) {
					otherDocsDocumentModelList.add(new OtherDocsImageModel(rs));
				}
				return otherDocsDocumentModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveOtherDocImageData(List<OtherDocsImageModel> otherDocsDocumentModelList, int otherDocsId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS_IMAGE_URL))
				.append(String.format(" ( %s, %s ) ", OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL, OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s ) ", OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL, OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID));

		List<Map<String, Object>> otherDocsDocumentURLMapList = new ArrayList<>();
		Map<String, Object> otherDocsDocumentURLMap;
		for (OtherDocsImageModel otherDocsDocumentModel : otherDocsDocumentModelList) {
			otherDocsDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(otherDocsDocumentModel.getImageURL())) {			
				otherDocsDocumentModel.setImageURL(otherDocsDocumentModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			otherDocsDocumentURLMap.put(OTHER_DOCS_IMAGE_URL_COLUMN.IMAGE_URL.toString(), otherDocsDocumentModel.getImageURL());
			otherDocsDocumentURLMap.put(OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID.toString(), otherDocsId);
			otherDocsDocumentURLMapList.add(otherDocsDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), otherDocsDocumentURLMapList.toArray(new HashMap[0]));
	}

	int deleteOtherDocImageData(int otherDocsId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_OTHER_DOCS_IMAGE_URL, OTHER_DOCS_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID, OTHER_DOCS_IMAGE_URL_COLUMN.OTHER_DOCS_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, otherDocsId });
	}

}
