package acc.webservice.components.otherdocs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.OTHER_DOCS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/otherDocsData/")
public class OtherDocsController {

	@Autowired
	private OtherDocsService otherDocsService;

	@RequestMapping(value = "getOtherDocsListData", method = RequestMethod.POST)
	public Map<String, Object> getOtherDocsListData(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", otherDocsService.getOtherDocsListData(companyId, start, numberOfRecord));
		return responseData;
	}

	@RequestMapping(value = "getOtherDocsDetailsById", method = RequestMethod.POST)
	public Map<String, Object> getOtherDocsDetailsById(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int otherDocsId = ApplicationUtility.getIntValue(params, OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", otherDocsService.getOtherDocsDetailsById(companyId, otherDocsId));
		return responseData;
	}

	@RequestMapping(value = "saveOtherDocsData", method = RequestMethod.POST)
	public Map<String, Object> saveOtherDocsData(@RequestBody OtherDocsModel otherDocsModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", otherDocsService.saveOtherDocsData(otherDocsModel));
		return responseData;
	}

	@RequestMapping(value = "updateOtherDocsData", method = RequestMethod.POST)
	public Map<String, Object> updateOtherDocsData(@RequestBody OtherDocsModel otherDocsModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", otherDocsService.updateOtherDocsData(otherDocsModel));
		return responseData;
	}

	@RequestMapping(value = "deleteOtherDocsData", method = RequestMethod.POST)
	public Map<String, Object> deleteOtherDocsData(@RequestBody Map<String, Object> params) {
		int companyId = ApplicationUtility.getIntValue(params, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int otherDocsId = ApplicationUtility.getIntValue(params, OTHER_DOCS_COLUMN.OTHER_DOCS_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(params, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", otherDocsService.deleteOtherDocsData(companyId, otherDocsId, deletedBy));
		return responseData;
	}

}
