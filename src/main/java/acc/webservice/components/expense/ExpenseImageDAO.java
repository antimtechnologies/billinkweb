package acc.webservice.components.expense;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.EXPENSE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class ExpenseImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<ExpenseImageModel> getExpenseImageList(int companyId, int expenseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", EXPENSE_IMAGE_URL_COLUMN.IMAGE_ID, EXPENSE_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID, EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID, EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID, EXPENSE_IMAGE_URL_COLUMN.IS_DELETED));
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID.toString(), expenseId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getExpenseImageResultExctractor());
	}

	private ResultSetExtractor<List<ExpenseImageModel>> getExpenseImageResultExctractor() {
		return new ResultSetExtractor<List<ExpenseImageModel>>() {
			@Override
			public List<ExpenseImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ExpenseImageModel> expenseImageModels = new ArrayList<>();
				while (rs.next()) {
					expenseImageModels.add(new ExpenseImageModel(rs));
				}
				return expenseImageModels;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveExpenseImageURLData(List<ExpenseImageModel> expenseImageModels, int companyId, int expenseId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s ) ", EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID, EXPENSE_IMAGE_URL_COLUMN.IMAGE_URL, EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s ) ", EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID, EXPENSE_IMAGE_URL_COLUMN.IMAGE_URL, EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> expenseImageURLMapList = new ArrayList<>();
		Map<String, Object> expenseImageObj;
		for (ExpenseImageModel expenseImageModel : expenseImageModels) {
			expenseImageObj = new HashMap<>();
			
			if (!ApplicationUtility.isNullEmpty(expenseImageModel.getImageURL())) {			
				expenseImageModel.setImageURL(expenseImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}
			expenseImageObj.put(EXPENSE_IMAGE_URL_COLUMN.IMAGE_URL.toString(), expenseImageModel.getImageURL());
			expenseImageObj.put(EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID.toString(), expenseId);
			expenseImageObj.put(EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			expenseImageURLMapList.add(expenseImageObj);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), expenseImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteExpenseImageURL(int companyId, int expenseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_IMAGE_URL, EXPENSE_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID, EXPENSE_IMAGE_URL_COLUMN.EXPENSE_ID ))
				.append(String.format(" AND %s = ? ", EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID, EXPENSE_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, expenseId, companyId });
	}

}
