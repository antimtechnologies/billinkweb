package acc.webservice.components.expense;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.expense.model.ExpenseModel;
import acc.webservice.enums.DatabaseEnum.EXPENSE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class ExpenseImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private ExpenseModel expenseModel;

	public ExpenseImageModel() {
	}

	public ExpenseImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(EXPENSE_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(EXPENSE_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {			
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public ExpenseModel getExpenseModel() {
		return expenseModel;
	}

	public void setExpenseModel(ExpenseModel expenseModel) {
		this.expenseModel = expenseModel;
	}

	@Override
	public String toString() {
		return "ExpenseImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
