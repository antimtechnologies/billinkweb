package acc.webservice.components.expense;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.global.utils.DateUtility;
import acc.webservice.components.expense.model.ExpenseModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.EXPENSE_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/expenseData/")
public class ExpenseController {

	@Autowired
	private ExpenseService expenseService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getExpenseListData", method = RequestMethod.POST)
	public Map<String, Object> getExpenseListData(@RequestBody Map<String, Object> parameters) {

		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.getExpenseListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getExpenseDetailsById", method = RequestMethod.POST)
	Map<String, Object> getExpenseDetailsById(@RequestBody Map<String, Object> parameters) {

		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int expenseId = ApplicationUtility.getIntValue(parameters, EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.getExpenseDetailsById(companyId, expenseId));
		return responseData;
	}

	@RequestMapping(value = "saveExpenseData", method = RequestMethod.POST)
	public Map<String, Object> saveExpenseData(@RequestBody ExpenseModel expenseModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.saveExpenseData(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "publishExpenseData", method = RequestMethod.POST)
	public Map<String, Object> publishExpenseData(@RequestBody ExpenseModel expenseModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.publishExpenseData(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "updateSavedExpenseData", method = RequestMethod.POST)
	public Map<String, Object> updateExpenseData(@RequestBody ExpenseModel expenseModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				expenseModel.getExpenseInvoiceDate())) {
			expenseModel.setExpenseInvoiceDate(DateUtility.convertDateFormat(expenseModel.getExpenseInvoiceDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.updateSavedExpenseData(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedExpenseData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedExpenseData(@RequestBody ExpenseModel expenseModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				expenseModel.getExpenseInvoiceDate())) {
			expenseModel.setExpenseInvoiceDate(DateUtility.convertDateFormat(expenseModel.getExpenseInvoiceDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.updatePublishedExpenseData(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedExpenseData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedExpenseData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int expenseId = ApplicationUtility.getIntValue(parameters, EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.deleteSavedExpenseData(companyId, expenseId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedExpenseData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishExpenseData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int expenseId = ApplicationUtility.getIntValue(parameters, EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters,
				APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.deletePublishedExpenseData(companyId, expenseId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "markExpenseAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markExpenseAsVerified(@RequestBody ExpenseModel expenseModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.markExpenseAsVerified(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "publishSavedExpense", method = RequestMethod.POST)
	public Map<String, Object> publishSavedExpense(@RequestBody ExpenseModel expenseModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT,
				expenseModel.getExpenseInvoiceDate())) {
			expenseModel.setExpenseInvoiceDate(DateUtility.convertDateFormat(expenseModel.getExpenseInvoiceDate(),
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", expenseService.publishSavedExpense(expenseModel));
		return responseData;
	}

	@RequestMapping(value = "getExpenseByDay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getExpenseByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("day") != null) {
			int day = Integer.parseInt(request.getParameter("day"));
			fooResourceUrl = "http://localhost:8090/user/expense/findbyday?day=" + day;
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "getExpenseByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getExpenseByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("month") != null) {
			int month = Integer.parseInt(request.getParameter("month"));
			fooResourceUrl = "http://localhost:8090/user/expense/findbymonth?month=" + month;
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findtotalamountbycompanyandday", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalExpenseAmountByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("day") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = "http://localhost:8090/user/expense/findtotalamountbycompanyandday?companyId="
						+ companyId + "&day=" + day;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findtotalamountbycompanyandmonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalPurchaseAmountByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("month") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = "http://localhost:8090/user/expense/findtotalamountbycompanyandmonth?companyId="
						+ companyId + "&month=" + month;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findAllExpensebycompanyandday", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findallPurchasetByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("day") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
				fooResourceUrl = "http://localhost:8090/user/expense/findallbycomapnyidandday?companyId=" + companyId
						+ "&day=" + day;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

	@RequestMapping(value = "findAllExpensebycompanyandmonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findallPurchaseByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if (request.getParameter("month") != null) {
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
				fooResourceUrl = "http://localhost:8090/user/expense/findallbycomapnyidandmonth?companyId=" + companyId
						+ "&month=" + month;
			}
		}
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
		return response;
	}

}
