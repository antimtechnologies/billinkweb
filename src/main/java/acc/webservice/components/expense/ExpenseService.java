package acc.webservice.components.expense;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.expense.model.ExpenseModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class ExpenseService {

	@Autowired
	private ExpenseDAO expenseDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ExpenseImageDAO expenseImageDAO;

	public List<ExpenseModel> getExpenseListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {
		return expenseDAO.getExpenseListData(companyId, start, numberOfRecord, filterParamter);
	}

	ExpenseModel getExpenseDetailsById(int companyId, int expenseId) {
		ExpenseModel expenseModel = expenseDAO.getExpenseDetailsById(companyId, expenseId);
		expenseModel.setImageURLList(expenseImageDAO.getExpenseImageList(companyId, expenseId));
		return expenseModel;
	}

	public ExpenseModel saveExpenseData(ExpenseModel expenseModel) {
		validateExpenseManupulateData(expenseModel);
		expenseDAO.saveExpenseData(expenseModel);

		if (ApplicationUtility.getSize(expenseModel.getImageURLList()) > 0) {
			expenseImageDAO.saveExpenseImageURLData(expenseModel.getImageURLList(), expenseModel.getCompanyId(),
					expenseModel.getExpenseId());
		}
		return expenseModel;
	}

	public ExpenseModel publishExpenseData(ExpenseModel expenseModel) {
		validateExpenseManupulateData(expenseModel);
		expenseDAO.publishExpenseData(expenseModel);
		if (ApplicationUtility.getSize(expenseModel.getImageURLList()) > 0) {
			expenseImageDAO.saveExpenseImageURLData(expenseModel.getImageURLList(), expenseModel.getCompanyId(),
					expenseModel.getExpenseId());
		}
		companyService.updateCompanyExpenseCount(expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 2),
				expenseModel.getCompanyId());
		companyService.updateCompanyExpensePendingCount(
				expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 0), expenseModel.getCompanyId());
		return expenseModel;
	}

	public ExpenseModel updateSavedExpenseData(ExpenseModel expenseModel) {
		validateExpenseManupulateData(expenseModel);
		if (ApplicationUtility.getSize(expenseModel.getImageURLList()) > 0) {
			expenseImageDAO.deleteExpenseImageURL(expenseModel.getCompanyId(), expenseModel.getExpenseId());
			expenseImageDAO.saveExpenseImageURLData(expenseModel.getImageURLList(), expenseModel.getCompanyId(),
					expenseModel.getExpenseId());
		}
		return expenseDAO.updateSavedExpenseData(expenseModel);
	}

	public ExpenseModel updatePublishedExpenseData(ExpenseModel expenseModel) {
		validateExpenseManupulateData(expenseModel);
		if (ApplicationUtility.getSize(expenseModel.getImageURLList()) > 0) {
			expenseImageDAO.deleteExpenseImageURL(expenseModel.getCompanyId(), expenseModel.getExpenseId());
			expenseImageDAO.saveExpenseImageURLData(expenseModel.getImageURLList(), expenseModel.getCompanyId(),
					expenseModel.getExpenseId());
		}
		expenseDAO.updatePublishedExpenseData(expenseModel);
		companyService.updateCompanyExpensePendingCount(
				expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 0), expenseModel.getCompanyId());
		return expenseModel;
	}

	public int deletePublishedExpenseData(int companyId, int expenseId, int deletedBy) {
		expenseDAO.deletePublishedExpenseData(companyId, expenseId, deletedBy);
		return companyService.updateCompanyExpensePendingCount(expenseDAO.getPendingExpenseCountData(companyId, 0),
				companyId);
	}

	public int deleteSavedExpenseData(int companyId, int expenseId, int deletedBy) {
		expenseImageDAO.deleteExpenseImageURL(companyId, expenseId);
		return expenseDAO.deleteSavedExpenseData(companyId, expenseId, deletedBy);
	}

	public ExpenseModel markExpenseAsVerified(ExpenseModel expenseModel) {

		if (expenseModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			expenseImageDAO.deleteExpenseImageURL(expenseModel.getCompanyId(), expenseModel.getExpenseId());
			expenseDAO.markExpenseAsDeleteAndVerified(expenseModel);
		} else {
			expenseDAO.markExpenseAsVerified(expenseModel);
		}

		companyService.updateCompanyExpensePendingCount(
				expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 0), expenseModel.getCompanyId());
		return expenseModel;
	}

	public ExpenseModel publishSavedExpense(ExpenseModel expenseModel) {
		updateSavedExpenseData(expenseModel);
		expenseDAO.publishSavedExpense(expenseModel);
		companyService.updateCompanyExpenseCount(expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 2),
				expenseModel.getCompanyId());
		companyService.updateCompanyExpensePendingCount(
				expenseDAO.getPendingExpenseCountData(expenseModel.getCompanyId(), 0), expenseModel.getCompanyId());
		return expenseModel;
	}

	private void validateExpenseManupulateData(ExpenseModel expenseModel) {

		if (expenseModel.getLedgerData() == null || expenseModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save expense data.");
		}

		if (expenseModel.getExpenseData() == null || expenseModel.getExpenseData().getLedgerId() < 1) {
			throw new InvalidParameterException("Expense ledger data is not passed to save expense data.");
		}

	}

}
