package acc.webservice.components.expense.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.expense.ExpenseImageModel;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.EXPENSE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class ExpenseModel {

	private int expenseId;
	private LedgerModel ledgerData;
	private LedgerModel expenseData;
	private String imageURL;
	private String description;
	private String expenseInvoiceDate;
	private double expenseAmount;
	private StatusModel status;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel verifiedBy;
	private String verifiedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private boolean verified;
	private boolean deleted;
	private int companyId;
	private List<ExpenseImageModel> imageURLList;
	private String referenceNumber1;
	private String referenceNumber2;

	public ExpenseModel() {
	}

	public ExpenseModel(ResultSet rs) throws SQLException {

		setExpenseId(rs.getInt(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString()));
		setExpenseAmount(rs.getDouble(EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT.toString()));
		setReferenceNumber1(rs.getString(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		ledgerModel.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		setLedgerData(ledgerModel);

		LedgerModel expenseData = new LedgerModel();
		expenseData.setLedgerId(rs.getInt("expenseLedgerId"));
		expenseData.setLedgerName(rs.getString("expenseLedgerName"));
		setExpenseData(expenseData);

		String imageURL = rs.getString(EXPENSE_DETAILS_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}

		setDescription(rs.getString(EXPENSE_DETAILS_COLUMN.DESCRIPTION.toString()));
		setVerified(rs.getBoolean(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString()));
		Timestamp fetchedExpenseDate = rs.getTimestamp(EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE.toString());
		if (fetchedExpenseDate != null) {
			setExpenseInvoiceDate(DateUtility.converDateToUserString(fetchedExpenseDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerfiedDate = rs.getTimestamp(EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerfiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerfiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		setStatus(new StatusModel(rs));
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public int getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(int expenseId) {
		this.expenseId = expenseId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public String getExpenseInvoiceDate() {
		return expenseInvoiceDate;
	}

	public void setExpenseInvoiceDate(String expenseInvoiceDate) {
		this.expenseInvoiceDate = expenseInvoiceDate;
	}

	public double getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<ExpenseImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<ExpenseImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public LedgerModel getExpenseData() {
		return expenseData;
	}

	public void setExpenseData(LedgerModel expenseData) {
		this.expenseData = expenseData;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	@Override
	public String toString() {
		return "ExpenseModel [expenseId=" + expenseId + ", imageURL=" + imageURL + ", description=" + description
				+ ", status=" + status + ", expenseInvoiceDate=" + expenseInvoiceDate + ", expenseAmount="
				+ expenseAmount + ", addedDateTime=" + addedDateTime + ", modifiedDateTime=" + modifiedDateTime
				+ ", verifiedDateTime=" + verifiedDateTime + ", deletedDateTime=" + deletedDateTime + ", isDeleted="
				+ deleted + "]";
	}

}
