package acc.webservice.components.expense;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.expense.model.ExpenseModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.EXPENSE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.SELL_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class ExpenseDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@SuppressWarnings("unchecked")
	List<ExpenseModel> getExpenseListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData.append(String.format(" SELECT ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.IMAGE_URL, EXPENSE_DETAILS_COLUMN.DESCRIPTION))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE, EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME, EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME, EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2))
						.append(String.format(" LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" ELD.%s AS expenseLedgerId, ELD.%s AS expenseLedgerName, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME))
						.append(String.format(" FROM %s ED ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
						.append(String.format(" INNER JOIN %s LD ON ED.%s = LD.%s AND LD.%s = :%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, EXPENSE_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s ELD ON ED.%s = ELD.%s AND ELD.%s = :%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s SM ON ED.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, EXPENSE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s AUM ON ED.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON ED.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON ED.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON ED.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE ED.%s = 0 ", EXPENSE_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND ED.%s = :%s ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {					
					sqlQueryToFetchData.append(String.format(" AND ED.%s IN (:statusList) ", EXPENSE_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter, EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString());
				
				if (isVerified == 0 || isVerified == 1) {					
					sqlQueryToFetchData.append(String.format(" AND ED.%s = :%s ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, EXPENSE_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter, EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString());
				
				if (ledgerId > 0)  {					
					sqlQueryToFetchData.append(String.format(" AND ED.%s = :%s ", EXPENSE_DETAILS_COLUMN.LEDGER_ID, EXPENSE_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString())) {
				String expenseId = ApplicationUtility.getStrValue(filterParamter, EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND ED.%s LIKE :%s ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID));
				parameters.put(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), "%" + expenseId + "%");
			}

		}

		sqlQueryToFetchData.append(String.format(" ORDER BY ED.%s DESC LIMIT :%s, :%s ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, APPLICATION_GENERIC_ENUM.START.toString(), APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		parameters.put(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getExpenseDetailsListExtractor());
	}

	private ResultSetExtractor<List<ExpenseModel>> getExpenseDetailsListExtractor() {

		return new ResultSetExtractor<List<ExpenseModel>>() {
			@Override
			public List<ExpenseModel> extractData(ResultSet rs) throws SQLException {
				List<ExpenseModel> purchaseList = new ArrayList<>();
				while (rs.next()) {
					purchaseList.add(new ExpenseModel(rs));
				}
				return purchaseList;
			}
		};
	}

	ExpenseModel getExpenseDetailsById(int companyId, int purchaseId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.IMAGE_URL, EXPENSE_DETAILS_COLUMN.DESCRIPTION))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE, EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME, EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME, EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
						.append(String.format(" ED.%s, ED.%s, ED.%s, ", EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2))
						.append(String.format(" LD.%s, LD.%s, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME))
						.append(String.format(" ELD.%s AS expenseLedgerId, ELD.%s AS expenseLedgerName, ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME)).append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
						.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
						.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
						.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
						.append(String.format(" SM.%s, SM.%s ", STATUS_TABLE_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_NAME))
						.append(String.format(" FROM %s ED ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
						.append(String.format(" INNER JOIN %s LD ON ED.%s = LD.%s AND LD.%s = :%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, EXPENSE_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s ELD ON ED.%s = ELD.%s AND ELD.%s = :%s ", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
						.append(String.format(" INNER JOIN %s SM ON ED.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER, EXPENSE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
						.append(String.format(" INNER JOIN %s AUM ON ED.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s MUM ON ED.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s VUM ON ED.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, EXPENSE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" LEFT JOIN %s DUM ON ED.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS, SELL_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
						.append(String.format(" WHERE ED.%s = :%s AND ED.%s = :%s ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED))
						.append(String.format(" AND ED.%s = :%s ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), purchaseId);
		parameters.put(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 0);
		parameters.put(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getExpenseDetailsExtractor());
	}

	private ResultSetExtractor<ExpenseModel> getExpenseDetailsExtractor() {

		return new ResultSetExtractor<ExpenseModel>() {
			@Override
			public ExpenseModel extractData(ResultSet rs) throws SQLException {
				ExpenseModel expenseDetails = new ExpenseModel();
				while (rs.next()) {
					expenseDetails = new ExpenseModel(rs);
				}
				return expenseDetails;
			}
		};
	}

	ExpenseModel saveExpenseData(ExpenseModel expenseModel) {
		String sqlQuery = getExpenseInsertQuery(expenseModel);

		if (!ApplicationUtility.isNullEmpty(expenseModel.getImageURL())) {			
			expenseModel.setImageURL(expenseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IMAGE_URL.toString(), expenseModel.getImageURL())
				.addValue(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString(), expenseModel.getLedgerData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), expenseModel.getReferenceNumber1())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), expenseModel.getReferenceNumber2())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID.toString(), expenseModel.getExpenseData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE.toString(), expenseModel.getExpenseInvoiceDate())
				.addValue(EXPENSE_DETAILS_COLUMN.DESCRIPTION.toString(), expenseModel.getDescription())
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT.toString(), expenseModel.getExpenseAmount())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(EXPENSE_DETAILS_COLUMN.ADDED_BY.toString(), expenseModel.getAddedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		expenseModel.setExpenseId(holder.getKey().intValue());
		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()));
		status.setStatusName(STATUS_TEXT.UNPUBLISHED.toString());
		expenseModel.setStatus(status);
		return expenseModel;
	}


	ExpenseModel publishExpenseData(ExpenseModel expenseModel) {
		String sqlQuery = getExpenseInsertQuery(expenseModel);

		if (!ApplicationUtility.isNullEmpty(expenseModel.getImageURL())) {			
			expenseModel.setImageURL(expenseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IMAGE_URL.toString(), expenseModel.getImageURL())
				.addValue(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString(), expenseModel.getLedgerData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), expenseModel.getReferenceNumber1())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), expenseModel.getReferenceNumber2())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID.toString(), expenseModel.getExpenseData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE.toString(), expenseModel.getExpenseInvoiceDate())
				.addValue(EXPENSE_DETAILS_COLUMN.DESCRIPTION.toString(), expenseModel.getDescription())
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT.toString(), expenseModel.getExpenseAmount())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(EXPENSE_DETAILS_COLUMN.ADDED_BY.toString(), expenseModel.getAddedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		expenseModel.setExpenseId(holder.getKey().intValue());
		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		expenseModel.setStatus(status);
		return expenseModel;
	}

	private String getExpenseInsertQuery(ExpenseModel expenseModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format("( %s, %s, %s, ", EXPENSE_DETAILS_COLUMN.IMAGE_URL, EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE, EXPENSE_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" %s, %s, %s, ", EXPENSE_DETAILS_COLUMN.DESCRIPTION, EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s, %s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s, %s, ", EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" %s, %s, %s )", EXPENSE_DETAILS_COLUMN.ADDED_BY, EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME, EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, ", EXPENSE_DETAILS_COLUMN.IMAGE_URL, EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE, EXPENSE_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" :%s, :%s, :%s, ", EXPENSE_DETAILS_COLUMN.DESCRIPTION, EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s, :%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" :%s, :%s, ", EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" :%s, :%s, :%s )", EXPENSE_DETAILS_COLUMN.ADDED_BY, EXPENSE_DETAILS_COLUMN.ADDED_DATE_TIME, EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID));
		return sqlQuery.toString();
	}

	ExpenseModel updateSavedExpenseData(ExpenseModel expenseModel) {
		StringBuilder sqlQuery = getUpdateExpenseQuery(expenseModel);

		if (!ApplicationUtility.isNullEmpty(expenseModel.getImageURL())) {			
			expenseModel.setImageURL(expenseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IMAGE_URL.toString(), expenseModel.getImageURL())
				.addValue(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString(), expenseModel.getLedgerData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), expenseModel.getReferenceNumber1())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), expenseModel.getReferenceNumber2())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID.toString(), expenseModel.getExpenseData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE.toString(), expenseModel.getExpenseInvoiceDate())
				.addValue(EXPENSE_DETAILS_COLUMN.DESCRIPTION.toString(), expenseModel.getDescription())
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT.toString(), expenseModel.getExpenseAmount())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(EXPENSE_DETAILS_COLUMN.MODIFIED_BY.toString(), expenseModel.getModifiedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseModel.getExpenseId())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		expenseModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return expenseModel;
	}

	ExpenseModel updatePublishedExpenseData(ExpenseModel expenseModel) {
		StringBuilder sqlQuery = getUpdateExpenseQuery(expenseModel);

		if (!ApplicationUtility.isNullEmpty(expenseModel.getImageURL())) {			
			expenseModel.setImageURL(expenseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IMAGE_URL.toString(), expenseModel.getImageURL())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), expenseModel.getReferenceNumber1())
				.addValue(EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), expenseModel.getReferenceNumber2())
				.addValue(EXPENSE_DETAILS_COLUMN.LEDGER_ID.toString(), expenseModel.getLedgerData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID.toString(), expenseModel.getExpenseData().getLedgerId())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE.toString(), expenseModel.getExpenseInvoiceDate())
				.addValue(EXPENSE_DETAILS_COLUMN.DESCRIPTION.toString(), expenseModel.getDescription())
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT.toString(), expenseModel.getExpenseAmount())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.MODIFIED_BY.toString(), expenseModel.getModifiedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseModel.getExpenseId())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		expenseModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return expenseModel;
	}

	private StringBuilder getUpdateExpenseQuery(ExpenseModel expenseModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IMAGE_URL, EXPENSE_DETAILS_COLUMN.IMAGE_URL))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE, EXPENSE_DETAILS_COLUMN.EXPENSE_INVOICE_DATE))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.DESCRIPTION, EXPENSE_DETAILS_COLUMN.DESCRIPTION))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT, EXPENSE_DETAILS_COLUMN.EXPENSE_AMOUNT))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.LEDGER_ID, EXPENSE_DETAILS_COLUMN.LEDGER_ID))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_1))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2, EXPENSE_DETAILS_COLUMN.REFERENCE_NUBER_2))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_LEDGER_ID))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.MODIFIED_BY, EXPENSE_DETAILS_COLUMN.MODIFIED_BY))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME, EXPENSE_DETAILS_COLUMN.MODIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED));
		return sqlQuery;
	}

	int deletePublishedExpenseData(int companyId, int expenseId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.DELETED_BY, EXPENSE_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME, EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseId)
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deleteSavedExpenseData(int companyId, int expenseId, int deletedBy) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.DELETED_BY, EXPENSE_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME, EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseId)
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(EXPENSE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(EXPENSE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	ExpenseModel markExpenseAsVerified(ExpenseModel expenseModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.VERIFIED_BY, EXPENSE_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME, EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(EXPENSE_DETAILS_COLUMN.VERIFIED_BY.toString(), expenseModel.getVerifiedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseModel.getExpenseId())
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		expenseModel.setVerified(true);
		UserModel userModel = new UserModel();
		userModel.setUserId(expenseModel.getVerifiedBy().getUserId());
		expenseModel.setVerifiedBy(userModel);
		expenseModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		return expenseModel;
	}

	ExpenseModel markExpenseAsDeleteAndVerified(ExpenseModel expenseModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, EXPENSE_DETAILS_COLUMN.IS_VERIFIED))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.VERIFIED_BY, EXPENSE_DETAILS_COLUMN.VERIFIED_BY))
							.append(String.format(" %s = :%s, ", EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME, EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));


		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(EXPENSE_DETAILS_COLUMN.VERIFIED_BY.toString(), expenseModel.getVerifiedBy().getUserId())
				.addValue(EXPENSE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseModel.getExpenseId())
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		expenseModel.setVerified(true);
		UserModel userModel = new UserModel();
		userModel.setUserId(expenseModel.getVerifiedBy().getUserId());
		expenseModel.setVerifiedBy(userModel);
		expenseModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		expenseModel.setDeleted(true);
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return expenseModel;
	}

	ExpenseModel publishSavedExpense(ExpenseModel expenseModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
							.append(String.format(" %s = :%s ", EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.STATUS_ID))
							.append(String.format(" WHERE %s = :%s ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, EXPENSE_DETAILS_COLUMN.EXPENSE_ID))
							.append(String.format(" AND %s = :%s  ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" AND %s = :%s ", EXPENSE_DETAILS_COLUMN.IS_DELETED, EXPENSE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.EXPENSE_ID.toString(), expenseModel.getExpenseId())
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), expenseModel.getCompanyId())
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		expenseModel.setStatus(status);
		return expenseModel;
	}

	int getPendingExpenseCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ", EXPENSE_DETAILS_COLUMN.EXPENSE_ID, COMPANY_RELATED_TABLE.ACC_COMPANY_EXPENSE_DETAILS))
				.append(String.format("WHERE %s != :%s AND %s = 0 ", EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.STATUS_ID, EXPENSE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s ", EXPENSE_DETAILS_COLUMN.COMPANY_ID, EXPENSE_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", EXPENSE_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(EXPENSE_DETAILS_COLUMN.STATUS_ID.toString(), statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(EXPENSE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

}
