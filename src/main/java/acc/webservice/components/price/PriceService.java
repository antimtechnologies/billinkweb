package acc.webservice.components.price;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class PriceService {

	@Autowired
	private PriceDao priceDao;
	
	public PriceModel savePrice(PriceModel priceModel) {
		return	priceDao.savePriceMaster(priceModel);
	}
	
	public PriceModel findByCustomerType(int customerTypeId){
		return priceDao.findByCustomerType(customerTypeId);
	}
	
	public HashMap<String, Map<String, String>> findAllPriseMaster(){
		return priceDao.findByAllpriceMaster();
	}
	
	public List<CustomerPriceModel> getCustomerPriceList( int start, int noOfRecord,int companyId) {
		return priceDao.getCustomerPriceList(start, noOfRecord,companyId);
	}
	
	public int deleteCustomerPriceList(int priceId,int companyId,int deletedBy) {
		return priceDao.deleteCustomerPriceList(priceId, companyId,deletedBy);
	}
}
