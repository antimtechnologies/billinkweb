package acc.webservice.components.price;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_BRAND_MASTER_COLUMN;
import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class PriceDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	// final String SELECT_BY_ID_QUERY = "SELECT priceId,discount FROM
	// acc_company_price_master WHERE customerTypeId =id";

	final String SELECT_ALL_QUERY = "SELECT * FROM acc_company_price_master";

	PriceModel savePriceMaster(PriceModel priceModel) {
		StringBuilder sqlQueryPriceMasterSave = new StringBuilder();
		sqlQueryPriceMasterSave.append(String.format("Insert INTO %s", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS))
				.append(String.format("( %s, ", PRICE_MASTER_COLUMN.DISCOUNT))
				.append(String.format(" %s, ", PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
				.append(String.format(" %s, ", PRICE_MASTER_COLUMN.COMPANY_ID))
				.append(String.format(" %s )", PRICE_MASTER_COLUMN.IS_DELETED)).append(" VALUES ")
				.append(String.format("( :%s, ", PRICE_MASTER_COLUMN.DISCOUNT))
				.append(String.format(" :%s, ", PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
				.append(String.format(" :%s, ", PRICE_MASTER_COLUMN.COMPANY_ID))
				.append(String.format(" :%s )", PRICE_MASTER_COLUMN.IS_DELETED))
				.append(String.format("ON DUPLICATE KEY UPDATE isDeleted=0"));

		KeyHolder holder = new GeneratedKeyHolder();

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PRICE_MASTER_COLUMN.DISCOUNT.toString(), priceModel.getDiscount())
				.addValue(PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID.toString(), priceModel.getCustomerTypeId())
				.addValue(PRICE_MASTER_COLUMN.COMPANY_ID.toString(), priceModel.getCompanyId())
				.addValue(PRICE_MASTER_COLUMN.IS_DELETED.toString(), priceModel.getIsDeleted());

		namedParameterJdbcTemplate.update(sqlQueryPriceMasterSave.toString(), parameters, holder);
//		priceModel.setPriceId(holder.getKey().intValue());
		return priceModel;
	}

	List<CustomerPriceModel> getCustomerPriceList(int start, int noOfRecord, int companyId) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("SELECT PMD.%s,PMD.%s, PMD.%s, PMD.%s,CTD.%s ", PRICE_MASTER_COLUMN.PRICE_ID,PRICE_MASTER_COLUMN.DISCOUNT,
				PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID, PRICE_MASTER_COLUMN.IS_DELETED,
				CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE))
				.append(String.format(" FROM %s PMD", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS))
				.append(String.format(" LEFT JOIN %s CTD ON PMD.%s = CTD.%s", DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS,
						PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID, CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE_ID))
				.append(String.format(" WHERE PMD.isdeleted=0 and CTD.isdeleted=0 and PMD.%s = :%s and PMD.%s = 0", PRICE_MASTER_COLUMN.COMPANY_ID,
						PRICE_MASTER_COLUMN.COMPANY_ID, ACC_BRAND_MASTER_COLUMN.IS_DELETED));

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(),
					APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(PRICE_MASTER_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getCustomerPriceResultSetExctractor());
	}

	private ResultSetExtractor<List<CustomerPriceModel>> getCustomerPriceResultSetExctractor() {
		return new ResultSetExtractor<List<CustomerPriceModel>>() {

			@Override
			public List<CustomerPriceModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<CustomerPriceModel> priceList = new ArrayList<>();
				while (rs.next()) {
					priceList.add(new CustomerPriceModel(rs));
				}
				return priceList;
			}
		};
	}

	public PriceModel findByCustomerType(int customerYupeId) {
		List<PriceModel> persons = namedParameterJdbcTemplate.query(
				"SELECT * FROM acc_company_price_master  WHERE customerTypeId = :id",
				new MapSqlParameterSource("id", customerYupeId), (resultSet, i) -> {
					return toPerson(resultSet);
				});

		if (persons.size() == 1) {
			return persons.get(0);
		}
		return null;
	}

	private PriceModel toPerson(ResultSet resultSet) throws SQLException {
		PriceModel person = new PriceModel();
		person.setPriceId(resultSet.getInt("priceId"));
		person.setDiscount(resultSet.getDouble("discount"));
		person.setCustomerTypeId(resultSet.getInt("customerTypeId"));
		person.setIsDeleted(resultSet.getInt("isDeleted"));
		return person;
	}

//	   public List<?> findByAllpriceMaster() {
//	        List<?> persons = namedParameterJdbcTemplate.query("SELECT * FROM acc_company_price_master",
//	                (resultSet, i) -> {
//	                    return toPersonlist(resultSet);
//	                });
//	         return persons;
//             }

//	   public List<?> findByAllpriceMaster() {
//			StringBuilder sqlQueryPriceMasterSave = new StringBuilder();
//			
//			
////			sqlQueryPriceMasterSave.append(String.format(" SELECT ID.%s, ID.%s, ID.%s, ID.%s, ID.%s, ", PRICE_MASTER_COLUMN.PRICE_ID, PRICE_MASTER_COLUMN.DISCOUNT, PRICE_MASTER_COLUMN.PRICE_ID, ITEM_DETAILS_COLUMN.TAX_CODE, ITEM_DETAILS_COLUMN.GST_TYPE))
////			.append(String.format(" FROM %s ID ", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS))
////			.append(String.format(" LEFT JOIN %s BM ON ID.%s = BM.%s AND BM.%s = 0 ", DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS, PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID, .BRAND_ID, ACC_BRAND_MASTER_COLUMN.IS_DELETED))
////			
////			.append(String.format(" WHERE ID.%s = :%s ", PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID, PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
////			.append(String.format(" AND ID.%s = 0 ", PRICE_MASTER_COLUMN.IS_DELETED, PRICE_MASTER_COLUMN.IS_DELETED));
//			
//			
////			sqlQueryPriceMasterSave.append(String.format("SELECT %s,%s,%s,%s ", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS,DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS))
////					.append(String.format("( %s, ", PRICE_MASTER_COLUMN.DISCOUNT))
////					.append(String.format(" %s, ", PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
////					.append(String.format(" %s )", PRICE_MASTER_COLUMN.IS_DELETED)).append(" VALUES ")
////					.append(String.format("( :%s, ", PRICE_MASTER_COLUMN.DISCOUNT))
////					.append(String.format(" :%s, ", PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID))
////					.append(String.format(" :%s )", PRICE_MASTER_COLUMN.IS_DELETED));
//
////			KeyHolder holder = new GeneratedKeyHolder();
////
////			SqlParameterSource parameters = new MapSqlParameterSource()
////					.addValue(PRICE_MASTER_COLUMN.DISCOUNT.toString(), priceModel.getDiscount())
////					.addValue(PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID.toString(), priceModel.getCustomerTypeId())
////					.addValue(PRICE_MASTER_COLUMN.IS_DELETED.toString(), priceModel.getIsDeleted());
////
////			namedParameterJdbcTemplate.update(sqlQueryPriceMasterSave.toString(), parameters, holder);
////			priceModel.setPriceId(holder.getKey().intValue());
////			return priceModel;
//			return null;
//		}

//	   	private List<?> toPersonlist(ResultSet resultSet) throws SQLException {
//			 PriceModel person = new PriceModel();
//		        person.setPriceId(resultSet.getInt("priceId"));
//		        person.setDiscount(resultSet.getDouble("discount"));
//		        person.setCustomerTypeId(resultSet.getInt("customerTypeId"));
//		        person.setIsDeleted(resultSet.getInt("isDeleted"));
//		        
//		        return person;
//		  }

	public HashMap<String, Map<String, String>> findByAllpriceMaster() {
		StringBuilder sqlQueryPriceMasterSave = new StringBuilder();
		sqlQueryPriceMasterSave
				.append(String.format(" SELECT ID.%s, ID.%s, CS.%s ", PRICE_MASTER_COLUMN.PRICE_ID,
						PRICE_MASTER_COLUMN.DISCOUNT, CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE))
				.append(String.format(" FROM %s ID, %s CS ", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS,
						DATABASE_TABLE.ACC_CUSTOMER_TYPE_DETAILS))
				.append(String.format(" WHERE ID.customerTypeId = CS.customerTypeId AND ID.isDeleted = 0 "));

		Map<String, Object> paramMap = new HashMap<>();
		return namedParameterJdbcTemplate.query(sqlQueryPriceMasterSave.toString(), paramMap,
				getPriceResultExctractor());

	}

	private ResultSetExtractor<HashMap<String, Map<String, String>>> getPriceResultExctractor() {
		return new ResultSetExtractor<HashMap<String, Map<String, String>>>() {
			@Override
			public HashMap<String, Map<String, String>> extractData(ResultSet rs)
					throws SQLException, DataAccessException {

				HashMap<String, Map<String, String>> hspList = new HashMap<>();
				while (rs.next()) {
					Map<String, String> mapList = new HashMap<>();
					mapList.put("customerType", rs.getString(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString()));
					mapList.put("Discount", rs.getString(PRICE_MASTER_COLUMN.DISCOUNT.toString()));
					mapList.put("PriceId", rs.getString(PRICE_MASTER_COLUMN.PRICE_ID.toString()));
					hspList.put(rs.getString(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString()), mapList);
				}
				return hspList;
			}
		};
	}

	public int deleteCustomerPriceList(int priceId, int companyId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_PRICE_MASTER_DETAILS))
				.append(String.format(" %s = :%s, ", PRICE_MASTER_COLUMN.IS_DELETED,
						PRICE_MASTER_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", PRICE_MASTER_COLUMN.DELETED_BY,
						PRICE_MASTER_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", PRICE_MASTER_COLUMN.DELETED_DATE_TIME,
						PRICE_MASTER_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", PRICE_MASTER_COLUMN.PRICE_ID,
						PRICE_MASTER_COLUMN.PRICE_ID))
				.append(String.format(" AND %s = :%s  ", PRICE_MASTER_COLUMN.COMPANY_ID,
						PRICE_MASTER_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PRICE_MASTER_COLUMN.PRICE_ID.toString(), priceId)
				.addValue(PRICE_MASTER_COLUMN.IS_DELETED.toString(), 1)
				.addValue(PRICE_MASTER_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(PRICE_MASTER_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(PRICE_MASTER_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

}
