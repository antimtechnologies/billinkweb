package acc.webservice.components.price;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.components.ResponseTemplet.SetResponseTemplet;
import acc.webservice.components.ResponseTemplet.setTemplateHasmap;
import acc.webservice.components.utills.ResponseListTemplate;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/pricemaster/")
public class PriceController {

	@Autowired
	private PriceService priceService;
	
	@Autowired
	ResponseListTemplate template;
	
	@Autowired
	setTemplateHasmap templateHshMap;
	
	@Autowired
	SetResponseTemplet setStatus;
	
	@RequestMapping(value="savepricedata",method = RequestMethod.POST)
	public Map<String,Object> savePriceMaster(@RequestBody PriceModel priceModel) {
		
		Map<String,Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("data", priceService.savePrice(priceModel));
		return response;
	}
	
	@RequestMapping(value="findbycustomertype",method= RequestMethod.GET)
	public ResponseListTemplate findByCustomerType(HttpServletRequest request) {
		if (request.getParameter("customerTypeId") != null) {
			PriceModel model = priceService.findByCustomerType(Integer.parseInt(request.getParameter("customerTypeId")));
			List<PriceModel> list = new ArrayList<>();
			list.add(model);
			template.setError(setStatus.SucessResponse("SuccessFully get"));
			template.setResult(list);
			return template;
		} else {
			template.setError(setStatus.FailsResponse("Please Enter customer type id"));
			template.setResult(null);
			return template;
		}
	}
	
	@RequestMapping(value="getCustomerPriceList", method=RequestMethod.POST)
	public Map<String,Object> getItemBrandList(@RequestBody Map<String, Object> parameter) {
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		
		return Collections.singletonMap("data", priceService.getCustomerPriceList(start, noOfRecord,companyId));
	}
	
	@RequestMapping(value="getAllPriceList",method= RequestMethod.GET)
	public setTemplateHasmap getAllpriceMaster(){
		HashMap<String, Map<String, String>> list = priceService.findAllPriseMaster();
		if (list.size() != 0) {
			templateHshMap.setError(setStatus.SucessResponse("SuccessFully get"));
			templateHshMap.setResult(list);
			return templateHshMap;
		} else {
			templateHshMap.setError(setStatus.FailsResponse("No recored Found"));
			templateHshMap.setResult(null);
			return templateHshMap;
		}
	}
	
	
	@RequestMapping(value="deleteCustomerPrice",method= RequestMethod.POST)
	public Map<String,Object> deleteCustomerPriceList(@RequestBody Map<String, Object> parameter){
		int priceId = ApplicationUtility.getIntValue(parameter, PRICE_MASTER_COLUMN.PRICE_ID.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, PRICE_MASTER_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", priceService.deleteCustomerPriceList(priceId, companyId, deletedBy));
		return responseData;
	}
	
	
	
}
