package acc.webservice.components.price;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;;
public class PriceModel {

	private int priceId;
	
	private Double discount;
	
	private int customerTypeId;

	private int isDeleted;
	
	
	private int companyId;
	
	public PriceModel() {
		// TODO Auto-generated constructor stub
	}
	
	public PriceModel(ResultSet rs) throws SQLException {
		setPriceId(rs.getInt(PRICE_MASTER_COLUMN.PRICE_ID.toString()));
		setDiscount(rs.getDouble(PRICE_MASTER_COLUMN.DISCOUNT.toString()));
		setCustomerTypeId(rs.getInt(PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID.toString()));
		setIsDeleted(rs.getInt(PRICE_MASTER_COLUMN.IS_DELETED.toString()));
		setCompanyId(rs.getInt(PRICE_MASTER_COLUMN.COMPANY_ID.toString()));
	}
	
	
	
	
	
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getPriceId() {
		return priceId;
	}

	public void setPriceId(int priceId) {
		this.priceId = priceId;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public int getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(int customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}
