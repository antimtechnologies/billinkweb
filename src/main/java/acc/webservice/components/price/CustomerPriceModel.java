package acc.webservice.components.price;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.CUSTOMER_TYPE_COLUMN;
import acc.webservice.enums.DatabaseEnum.PRICE_MASTER_COLUMN;

public class CustomerPriceModel {

	private Double discount;

	private int customerTypeId;

	private int isDeleted;

	private String customerType;
	
	private int priceId;

	public CustomerPriceModel() {
		// TODO Auto-generated constructor stub
	}

	public CustomerPriceModel(ResultSet rs) throws SQLException {
		setPriceId(rs.getInt(PRICE_MASTER_COLUMN.PRICE_ID.toString()));
		setCustomerType(rs.getString(CUSTOMER_TYPE_COLUMN.CUSTOMER_TYPE.toString()));
		setCustomerTypeId(rs.getInt(PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID.toString()));
		setIsDeleted(rs.getInt(PRICE_MASTER_COLUMN.IS_DELETED.toString()));
		setDiscount(rs.getDouble(PRICE_MASTER_COLUMN.DISCOUNT.toString()));
		// setCustomerTypeId(rs.getInt(PRICE_MASTER_COLUMN.CUSTOMER_TYPE_ID.toString()));
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public int getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(int customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getPriceId() {
		return priceId;
	}

	public void setPriceId(int priceId) {
		this.priceId = priceId;
	}
	
	

}
