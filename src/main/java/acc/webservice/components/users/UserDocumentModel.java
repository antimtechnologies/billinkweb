package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.USER_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class UserDocumentModel {

	private int documentUrlId;
	private boolean deleted;
	private String documentType;
	private String description;
	private String documentUrl;
	private UserModel userModel;

	public UserDocumentModel() {
	}

	public UserDocumentModel(ResultSet rs) throws SQLException {

		setDocumentUrlId(rs.getInt(USER_DOCUMENT_COLUMN.DOCUMENT_URL_ID.toString()));
		setDocumentType(rs.getString(USER_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString()));
		setDescription(rs.getString(USER_DOCUMENT_COLUMN.DESCRIPTION.toString()));
		String documentURL = rs.getString(USER_DOCUMENT_COLUMN.DOCUMENT_URL.toString());
		if (!ApplicationUtility.isNullEmpty(documentURL)) {
			setDocumentUrl(ApplicationUtility.getServerURL() + documentURL);
		}

	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDocumentUrlId() {
		return documentUrlId;
	}

	public void setDocumentUrlId(int documentUrlId) {
		this.documentUrlId = documentUrlId;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

}
