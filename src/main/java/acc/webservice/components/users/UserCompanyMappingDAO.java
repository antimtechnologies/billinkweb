package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_COMPANY_MAPPING_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Repository
@Lazy
public class UserCompanyMappingDAO {

	private final Logger logger = Logger.getLogger(UserCompanyMappingDAO.class);

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<CompanyModel> getUserCompanyList(int userId) {
		logger.debug("UserCompanyMappingDAO.getUserCompanyList() -- method start");
		logger.debug("parameters are { userId : " + userId + " } ");

		StringBuilder strQueryToFetchCompanyList = new StringBuilder();
		strQueryToFetchCompanyList.append(String.format("SELECT CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL))
			.append(String.format(" CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX, COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER ))
			.append(String.format(" CD.%s, CD.%s, CD.%s, ", COMPANY_TABLE_COLUMN.OFFICE_ADDRESS, COMPANY_TABLE_COLUMN.PAN_NUMBER, COMPANY_TABLE_COLUMN.GST_NUMBER ))
			.append(String.format(" SM.%s, SM.%s ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
			.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_COMPANY_DETAILS))
			.append(String.format(" LEFT JOIN %s SM ON SM.%s = CD.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, COMPANY_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
			.append(String.format(" INNER JOIN %s UCM ON CD.companyId = UCM.companyId ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING, COMPANY_TABLE_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID))
			.append(String.format(" WHERE CD.%s = 0 AND UCM.%s = 0 AND UCM.%s = ? ", COMPANY_TABLE_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.USER_ID));

		logger.debug("Sql query" + strQueryToFetchCompanyList);
		logger.debug("UserCompanyMappingDAO.getUserCompanyList() -- method end");
		return namedParameterJdbcTemplate.getJdbcOperations().query(strQueryToFetchCompanyList.toString(), new Object[] { userId }, resultSetExctractorUserCompanyList());
	}

	private ResultSetExtractor<List<CompanyModel>> resultSetExctractorUserCompanyList() {

		return new ResultSetExtractor<List<CompanyModel>>() {

			@Override
			public List<CompanyModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				CompanyModel companyData = null;
				List<CompanyModel> userCompanyList = new ArrayList<>();

				while(rs.next()) {
					companyData = new CompanyModel();
					companyData.setCompanyId(rs.getInt(COMPANY_TABLE_COLUMN.COMPANY_ID.toString()));
					companyData.setCompanyName(rs.getString(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString()));
					companyData.setBillNumberPrefix(rs.getString(COMPANY_TABLE_COLUMN.BILL_NUMBER_PREFIX.toString()));
					companyData.setLastBillNumber(rs.getInt(COMPANY_TABLE_COLUMN.LAST_BILL_NUMBER.toString()));
					companyData.setGstNumber(rs.getString(COMPANY_TABLE_COLUMN.GST_NUMBER.toString()));
					companyData.setPanNumber(rs.getString(COMPANY_TABLE_COLUMN.PAN_NUMBER.toString()));
					companyData.setOfficeAddress(rs.getString(COMPANY_TABLE_COLUMN.OFFICE_ADDRESS.toString()));
					companyData.setProfilePhotoURL(ApplicationUtility.getServerURL()+rs.getString(COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL.toString()));

					companyData.setState(new StateModel(rs));
					userCompanyList.add(companyData);
				}
				return userCompanyList;
			}
		}; 
	}

	@SuppressWarnings("unchecked")
	void saveUserCompanyMappingData(List<CompanyModel> companyList, int userId) {
		String sqlQuery = String.format("INSERT INTO %s (%s, %s) VALUES (:%s, :%s) ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID, USER_COMPANY_MAPPING_COLUMN.COMPANY_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID);
		List<Map<String, Object>> companyDataList = new ArrayList<>();
		Map<String, Object> companyData = null;

		for (CompanyModel companyModel : companyList) {
			companyData = new HashMap<>();
			companyData.put(USER_COMPANY_MAPPING_COLUMN.COMPANY_ID.toString(), companyModel.getCompanyId());
			companyData.put(USER_COMPANY_MAPPING_COLUMN.USER_ID.toString(), userId);
			companyDataList.add(companyData);
		}
		namedParameterJdbcTemplate.batchUpdate(sqlQuery, companyDataList.toArray(new HashMap[0]));
	}

	int deleteUserCompanyData(int userId) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_COMPANY_MAPPING))
							.append(String.format(" %s = :%s ", USER_COMPANY_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MAPPING_COLUMN.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", USER_COMPANY_MAPPING_COLUMN.USER_ID, USER_COMPANY_MAPPING_COLUMN.USER_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_COMPANY_MAPPING_COLUMN.USER_ID.toString(), userId)
				.addValue(USER_COMPANY_MAPPING_COLUMN.IS_DELETED.toString(), 1);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
}
