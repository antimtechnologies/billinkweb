package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.roles.RoleModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ROLE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class UserDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger logger = Logger.getLogger(UserDAO.class);
	static int SYSTEM_USER_ID = 0;

	@PostConstruct
	private void inializeStatusMap() {
		String sqlQuery = String.format(" SELECT %s FROM %s WHERE %s = 0 AND %s = '" + ApplicationUtility.SYSTEM_USER_NAME + "'", USER_TABLE_COLOUMN.USER_ID,
				DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.USER_NAME);
		namedParameterJdbcTemplate.query(sqlQuery, getStatusResultSetExtractor());
	}

	private ResultSetExtractor<Integer> getStatusResultSetExtractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					SYSTEM_USER_ID = rs.getInt(USER_TABLE_COLOUMN.USER_ID.toString());
				}
				return SYSTEM_USER_ID;
			}
		};
	}

	Integer getUserCount(String userName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData.append(String.format(" SELECT COUNT(CD.%s) AS %s ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
						.append(String.format(" FROM %s CD ", DATABASE_TABLE.ACC_USER_DETAILS))
						.append(String.format(" WHERE CD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s )", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME))
						.append(String.format(" AND CD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(USER_TABLE_COLOUMN.USER_NAME.toString(), "%" + userName + "%");

		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getUserCount());
	}

	private ResultSetExtractor<Integer> getUserCount() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				int totalCount = 0;
				while(rs.next()) {
					totalCount = rs.getInt(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString());
				}
				return totalCount;
			}
		};
	}

	public UserModel getBasicUserDetailForUserName(String userName) {
		logger.debug("UserDAO.getUserDetailsByUserName() -- method start");
		logger.debug("parameters are : { userName : " + userName + " }");

		StringBuilder strQueryToGetuserData = new StringBuilder();
		strQueryToGetuserData.append(String.format(" SELECT UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.FIRST_NAME, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.IS_USER_LOGGE_IN, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL,USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED, USER_TABLE_COLOUMN.CITY_NAME))
							.append(String.format(" RM.%s, RM.%s ", ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_NAME))
							.append(String.format(" FROM %s UD ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" INNER JOIN %s RM ON UD.%s = RM.%s AND RM.%s = 0 ", DATABASE_TABLE.ACC_ROLE_MASTER, USER_TABLE_COLOUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" WHERE UD.%s = ? AND UD.%s = 0 ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.IS_DELETED))
							.append(String.format(" AND UD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN));

		logger.debug("sql query : " + strQueryToGetuserData );
		logger.debug("UserDAO.getUserDetailsByUserName() -- method end");
		return namedParameterJdbcTemplate.getJdbcOperations().query(strQueryToGetuserData.toString(), new Object[] {userName}, getUserBasicDetails());
	}

	private ResultSetExtractor<UserModel> getUserBasicDetails() {
		return new ResultSetExtractor<UserModel>() {
			@Override
			public UserModel extractData(ResultSet rs) throws SQLException, DataAccessException {
				UserModel userDetails = new UserModel();
				while (rs.next()) {
					userDetails.setUserId(rs.getInt(USER_TABLE_COLOUMN.USER_ID.toString()));
					userDetails.setUserName(rs.getString(USER_TABLE_COLOUMN.USER_NAME.toString()));
					userDetails.setFirstName(rs.getString(USER_TABLE_COLOUMN.FIRST_NAME.toString()));
					userDetails.setLastName(rs.getString(USER_TABLE_COLOUMN.LAST_NAME.toString()));
					userDetails.setProfilePhotoURL(rs.getString(USER_TABLE_COLOUMN.PROFILE_PHOTO_URL.toString()));
					userDetails.setMultiLoginAllowed(rs.getBoolean(USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED.toString()));
					userDetails.setUserLoggedIn(rs.getBoolean(USER_TABLE_COLOUMN.IS_USER_LOGGE_IN.toString()));
					userDetails.setDesignation(rs.getString(USER_TABLE_COLOUMN.DESIGNATION.toString()));
					RoleModel role = new RoleModel(rs);
					userDetails.setRole(role);
				}
				return userDetails;
			}
		};
	}

	public boolean validateUserCredential(String userName, String password) {
		logger.debug("UserDAO.validateUserCredential() -- method start");
		logger.debug("parameters are : { userName : " + userName + ", password : " + password + " }");
		StringBuilder strQueryToValidateUser = new StringBuilder();
		strQueryToValidateUser.append(String.format(" SELECT COUNT(%s) AS UserCount FROM %s ", USER_TABLE_COLOUMN.USER_ID, DATABASE_TABLE.ACC_USER_DETAILS))
								.append(String.format(" WHERE %s = :%s AND %s = SHA1(:%s) AND %s = :%s ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.PASSWORD, USER_TABLE_COLOUMN.PASSWORD,USER_TABLE_COLOUMN.IS_DELETED,USER_TABLE_COLOUMN.IS_DELETED));

		logger.debug("sql query : " + strQueryToValidateUser);
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(USER_TABLE_COLOUMN.USER_NAME.toString(), userName);
		paramMap.put(USER_TABLE_COLOUMN.PASSWORD.toString(), password);
		paramMap.put(USER_TABLE_COLOUMN.IS_DELETED.toString(), 0);
		logger.debug("UserDAO.validateUserCredential() -- method end");
		return namedParameterJdbcTemplate.queryForObject(strQueryToValidateUser.toString(), paramMap, Integer.class) > 0;
	}

	public UserModel getUserDetailsByUserName(String userName) {
		logger.debug("UserDAO.getUserDetailsByUserName() -- method start");
		logger.debug("parameters are : { userName : " + userName + " }");
		StringBuilder strQueryToGetuserData = getUserDetailQuery();				
		strQueryToGetuserData.append(String.format(" WHERE UD.%s = ? AND UD.%s = 0 ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.IS_DELETED))
							.append(String.format(" AND UD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN));

		logger.debug("sql query : " + strQueryToGetuserData );
		logger.debug("UserDAO.getUserDetailsByUserName() -- method end");
		return namedParameterJdbcTemplate.getJdbcOperations().query(strQueryToGetuserData.toString(), new Object[] {userName}, getUsersData());
	}

	public UserModel getUserDetailsById(int userId) {
		StringBuilder strQueryToGetuserData = getUserDetailQuery();				
		strQueryToGetuserData.append(String.format(" WHERE UD.%s = ? AND UD.%s = 0 ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.IS_DELETED))
								.append(String.format(" AND UD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN));

		logger.debug("sql query : " + strQueryToGetuserData );
		logger.debug("UserDAO.getUserDetailsByUserName() -- method end");
		return namedParameterJdbcTemplate.getJdbcOperations().query(strQueryToGetuserData.toString(), new Object[] {userId}, getUsersData());
	}

	private StringBuilder getUserDetailQuery() {
		StringBuilder strQueryToGetuserData = new StringBuilder();
		strQueryToGetuserData.append(String.format(" SELECT UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.FIRST_NAME))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.MOBILE_NUMBER, USER_TABLE_COLOUMN.EMAIL_ID, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.FULL_ADDRESS, USER_TABLE_COLOUMN.PIN_CODE, USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.ADDED_BY, USER_TABLE_COLOUMN.ADDED_DATE_TIME, USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE, USER_TABLE_COLOUMN.CITY_NAME))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY, USER_TABLE_COLOUMN.IS_USER_LOGGE_IN, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" SM.%s, SM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
							.append(String.format(" RM.%s, RM.%s, CD.%s, CD.%s, CD.%s ", ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_NAME, COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" FROM %s UD ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" LEFT JOIN %s SM ON SM.%s = UD.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, USER_TABLE_COLOUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" INNER JOIN %s RM ON UD.%s = RM.%s AND RM.%s = 0 ", DATABASE_TABLE.ACC_ROLE_MASTER, USER_TABLE_COLOUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" LEFT JOIN %s CD ON UD.%s = CD.%s ", DATABASE_TABLE.ACC_COMPANY_DETAILS, USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID));
		return strQueryToGetuserData;
	}

	private ResultSetExtractor<UserModel> getUsersData() {
		return new ResultSetExtractor<UserModel>() {
			@Override
			public UserModel extractData(ResultSet rs) throws SQLException, DataAccessException {
				UserModel userDetails = new UserModel();
				while (rs.next()) {
					userDetails = new UserModel(rs);
				}
				return userDetails;
			}
		};
	}

	public UserModel saveUserData(UserModel userData) {
		StringBuilder sqlQueryCompanySave = new StringBuilder();
		sqlQueryCompanySave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format("( %s, %s, %s, %s, %s, ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.PASSWORD, USER_TABLE_COLOUMN.FIRST_NAME, USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" %s, %s, %s, %s, ", USER_TABLE_COLOUMN.ROLE_ID, USER_TABLE_COLOUMN.MOBILE_NUMBER, USER_TABLE_COLOUMN.EMAIL_ID, USER_TABLE_COLOUMN.FULL_ADDRESS))
							.append(String.format(" %s, %s, %s, %s, ", USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED, USER_TABLE_COLOUMN.PIN_CODE, USER_TABLE_COLOUMN.STATE_ID, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" %s, %s, %s, %s, %s )", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY, USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE, USER_TABLE_COLOUMN.ADDED_BY, USER_TABLE_COLOUMN.CITY_NAME))
							.append(" VALUES ")
							.append(String.format("( :%s, SHA1(:%s), :%s, :%s, :%s, ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.PASSWORD, USER_TABLE_COLOUMN.FIRST_NAME, USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" :%s, :%s, :%s, :%s, ", USER_TABLE_COLOUMN.ROLE_ID, USER_TABLE_COLOUMN.MOBILE_NUMBER, USER_TABLE_COLOUMN.EMAIL_ID, USER_TABLE_COLOUMN.FULL_ADDRESS))
							.append(String.format(" :%s, :%s, :%s, :%s, ", USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED, USER_TABLE_COLOUMN.PIN_CODE, USER_TABLE_COLOUMN.STATE_ID, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" :%s, :%s, :%s, :%s, :%s )", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY, USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE, USER_TABLE_COLOUMN.ADDED_BY, USER_TABLE_COLOUMN.CITY_NAME));

		if (!ApplicationUtility.isNullEmpty(userData.getProfilePhotoURL())) {			
			userData.setProfilePhotoURL(userData.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			userData.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.USER_PROFILE_PHOTO_DEF_URL.toString());
		}
		
		if (!ApplicationUtility.isNullEmpty(userData.getUserLicenceExpiryDate())) {
			userData.setUserLicenceExpiryDate(DateUtility.convertDateFormat(userData.getUserLicenceExpiryDate(), DateUtility.DEFAULT_USER_DATE_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.USER_NAME.toString(), userData.getUserName())
				.addValue(USER_TABLE_COLOUMN.PASSWORD.toString(), userData.getPassword())
				.addValue(USER_TABLE_COLOUMN.CITY_NAME.toString(), userData.getCityName())
				.addValue(USER_TABLE_COLOUMN.FIRST_NAME.toString(), userData.getFirstName())
				.addValue(USER_TABLE_COLOUMN.LAST_NAME.toString(), userData.getLastName())
				.addValue(USER_TABLE_COLOUMN.ROLE_ID.toString(), userData.getRole().getRoleId())
				.addValue(USER_TABLE_COLOUMN.MOBILE_NUMBER.toString(), userData.getMobileNumber())
				.addValue(USER_TABLE_COLOUMN.FULL_ADDRESS.toString(), userData.getFullAddress())
				.addValue(USER_TABLE_COLOUMN.EMAIL_ID.toString(), userData.getEmailId())
				.addValue(USER_TABLE_COLOUMN.DESIGNATION.toString(), userData.getDesignation())
				.addValue(USER_TABLE_COLOUMN.PIN_CODE.toString(), userData.getPincode())
				.addValue(USER_TABLE_COLOUMN.STATE_ID.toString(), userData.getState().getStateId())
				.addValue(USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED.toString(), userData.isMultiLoginAllowed())
				.addValue(USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID.toString(), (userData.getDefaultCompany() == null || userData.getDefaultCompany().getCompanyId() == 0) ? null : userData.getDefaultCompany().getCompanyId())
				.addValue(USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY.toString(), userData.getNoOfAllowedCompany())
				.addValue(USER_TABLE_COLOUMN.PROFILE_PHOTO_URL.toString(), userData.getProfilePhotoURL())
				.addValue(USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE.toString(), userData.getUserLicenceExpiryDate())
				.addValue(USER_TABLE_COLOUMN.ADDED_BY.toString(), userData.getAddedBy().getUserId());

		namedParameterJdbcTemplate.update(sqlQueryCompanySave.toString(), parameters, holder);
		userData.setUserId(holder.getKey().intValue());
		return userData;
	}

	public List<UserModel> getUserDetails(int start, int noOfRecord, String userName) {
		logger.debug("UserDAO.getUserDetails() -- method start");

		StringBuilder strQueryToGetuserData = new StringBuilder();
		strQueryToGetuserData.append(String.format(" SELECT UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.FIRST_NAME, USER_TABLE_COLOUMN.CITY_NAME))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.MOBILE_NUMBER, USER_TABLE_COLOUMN.EMAIL_ID, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.FULL_ADDRESS, USER_TABLE_COLOUMN.PIN_CODE, USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED))
							.append(String.format(" UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.ADDED_BY, USER_TABLE_COLOUMN.ADDED_DATE_TIME, USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE))
							.append(String.format(" UD.%s, UD.%s, UD.%s, UD.%s, ", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY, USER_TABLE_COLOUMN.IS_USER_LOGGE_IN, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" SM.%s, SM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME ))
							.append(String.format(" RM.%s, RM.%s, CD.%s, CD.%s, CD.%s ", ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_NAME, COMPANY_TABLE_COLUMN.COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_NAME, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" FROM %s UD ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" LEFT JOIN %s SM ON SM.%s = UD.%s AND SM.%s = 0 ", DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, USER_TABLE_COLOUMN.STATE_ID, STATE_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" INNER JOIN %s RM ON UD.%s = RM.%s AND RM.%s = 0 ", DATABASE_TABLE.ACC_ROLE_MASTER, USER_TABLE_COLOUMN.ROLE_ID, ROLE_TABLE_COLUMN.ROLE_ID, ROLE_TABLE_COLUMN.IS_DELETED))
							.append(String.format(" LEFT JOIN %s CD ON UD.%s = CD.%s ", DATABASE_TABLE.ACC_COMPANY_DETAILS, USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, COMPANY_TABLE_COLUMN.COMPANY_ID))
							.append(String.format(" WHERE UD.%s = 0 AND ( %s LIKE :%s OR '%%' = :%s ) ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME ))
							.append(String.format(" AND UD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN))
							.append(String.format(" ORDER BY UD.%s ASC LIMIT :start, :noOfRecord ", USER_TABLE_COLOUMN.USER_NAME));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		parameters.put(USER_TABLE_COLOUMN.USER_NAME.toString(), "%" + userName + "%");

		logger.debug("sql query : " + strQueryToGetuserData );
		logger.debug("UserDAO.getUserDetailsByUserName() -- method end");

		return namedParameterJdbcTemplate.query(strQueryToGetuserData.toString(), parameters, getUsersDataList());
	}

	private ResultSetExtractor<List<UserModel>> getUsersDataList() {
		return new ResultSetExtractor<List<UserModel>>() {
			@Override
			public List<UserModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<UserModel> userDetailList = new ArrayList<>();
				UserModel userDetails = null;
				while (rs.next()) {
					userDetails = new UserModel(rs);
					userDetailList.add(userDetails);
				}
				return userDetailList;
			}
		};
	}

	UserModel updateUserData(UserModel userData) {
		StringBuilder sqlQueryUpdate = new StringBuilder();
		sqlQueryUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.FIRST_NAME, USER_TABLE_COLOUMN.FIRST_NAME))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.LAST_NAME, USER_TABLE_COLOUMN.LAST_NAME))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.ROLE_ID, USER_TABLE_COLOUMN.ROLE_ID))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.MOBILE_NUMBER, USER_TABLE_COLOUMN.MOBILE_NUMBER))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.EMAIL_ID, USER_TABLE_COLOUMN.EMAIL_ID))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.FULL_ADDRESS, USER_TABLE_COLOUMN.FULL_ADDRESS))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.PIN_CODE, USER_TABLE_COLOUMN.PIN_CODE))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.CITY_NAME, USER_TABLE_COLOUMN.CITY_NAME))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED, USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY, USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.PROFILE_PHOTO_URL, USER_TABLE_COLOUMN.PROFILE_PHOTO_URL))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.DESIGNATION, USER_TABLE_COLOUMN.DESIGNATION))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE, USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE))
							.append(String.format(" %s = :%s ", USER_TABLE_COLOUMN.STATE_ID, USER_TABLE_COLOUMN.STATE_ID))
							.append(String.format(" WHERE %s = :%s  ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_ID))
							.append(String.format(" AND %s = :%s  ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.IS_DELETED));

		if (!ApplicationUtility.isNullEmpty(userData.getProfilePhotoURL())) {			
			userData.setProfilePhotoURL(userData.getProfilePhotoURL().replace(ApplicationUtility.getServerURL(), ""));
		} else {
			userData.setProfilePhotoURL(APPLICATION_GENERIC_ENUM.USER_PROFILE_PHOTO_DEF_URL.toString());
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.FIRST_NAME.toString(), userData.getFirstName())
				.addValue(USER_TABLE_COLOUMN.LAST_NAME.toString(), userData.getLastName())
				.addValue(USER_TABLE_COLOUMN.ROLE_ID.toString(), userData.getRole().getRoleId())
				.addValue(USER_TABLE_COLOUMN.CITY_NAME.toString(), userData.getCityName())
				.addValue(USER_TABLE_COLOUMN.MOBILE_NUMBER.toString(), userData.getMobileNumber())
				.addValue(USER_TABLE_COLOUMN.EMAIL_ID.toString(), userData.getEmailId())
				.addValue(USER_TABLE_COLOUMN.FULL_ADDRESS.toString(), userData.getFullAddress())
				.addValue(USER_TABLE_COLOUMN.PIN_CODE.toString(), userData.getPincode())
				.addValue(USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED.toString(), userData.isMultiLoginAllowed())
				.addValue(USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID.toString(), (userData.getDefaultCompany() == null || userData.getDefaultCompany().getCompanyId() == 0) ? null : userData.getDefaultCompany().getCompanyId())
				.addValue(USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY.toString(), userData.getNoOfAllowedCompany())
				.addValue(USER_TABLE_COLOUMN.PROFILE_PHOTO_URL.toString(), userData.getProfilePhotoURL())
				.addValue(USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE.toString(), userData.getUserLicenceExpiryDate())
				.addValue(USER_TABLE_COLOUMN.DESIGNATION.toString(), userData.getDesignation())
				.addValue(USER_TABLE_COLOUMN.STATE_ID.toString(), userData.getState().getStateId())
				.addValue(USER_TABLE_COLOUMN.USER_ID.toString(), userData.getUserId())
				.addValue(USER_TABLE_COLOUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryUpdate.toString(), parameters);
		return userData;
	}

	int deleteUserData(int userId, int deletedBy) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", USER_TABLE_COLOUMN.DELETED_BY, USER_TABLE_COLOUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", USER_TABLE_COLOUMN.DELETED_DATE_TIME, USER_TABLE_COLOUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", USER_TABLE_COLOUMN.USER_ID, USER_TABLE_COLOUMN.USER_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.USER_ID.toString(), userId)
				.addValue(USER_TABLE_COLOUMN.IS_DELETED.toString(), 1)
				.addValue(USER_TABLE_COLOUMN.DELETED_BY.toString(), deletedBy)
				.addValue(USER_TABLE_COLOUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int changeUserPassword(String oldPassword, String newPassword, String userName) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" %s = SHA1(:%s) ", USER_TABLE_COLOUMN.PASSWORD, USER_TABLE_COLOUMN.PASSWORD))
							.append(String.format(" WHERE %s = SHA1(:oldPassword)  ", USER_TABLE_COLOUMN.PASSWORD))
							.append(String.format(" AND %s = :%s  ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME))
							.append(String.format(" AND %s = :%s  ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.USER_NAME.toString(), userName)
				.addValue(USER_TABLE_COLOUMN.IS_DELETED.toString(), 0)
				.addValue("oldPassword", oldPassword)
				.addValue(USER_TABLE_COLOUMN.PASSWORD.toString(), newPassword);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int resetUserPassword(String newPassword, String userName) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" %s = SHA1(:%s) ", USER_TABLE_COLOUMN.PASSWORD, USER_TABLE_COLOUMN.PASSWORD))
							.append(String.format(" WHERE %s = :%s  ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME))
							.append(String.format(" AND %s = :%s  ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.USER_NAME.toString(), userName)
				.addValue(USER_TABLE_COLOUMN.IS_DELETED.toString(), 0)
				.addValue(USER_TABLE_COLOUMN.PASSWORD.toString(), newPassword);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int updateUserDefaultCompany(String userName, int companyId) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" %s =  :%s ", USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID, USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID))
							.append(String.format(" WHERE %s = :%s  ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.USER_NAME))
							.append(String.format(" AND %s = :%s  ", USER_TABLE_COLOUMN.IS_DELETED, USER_TABLE_COLOUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_TABLE_COLOUMN.USER_NAME.toString(), userName)
				.addValue(USER_TABLE_COLOUMN.IS_DELETED.toString(), 0)
				.addValue(USER_TABLE_COLOUMN.DEFAULT_COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int isUserExistWithSameUserName(String userName, int userId) {
		logger.debug("UserDAO.getUserDetailsByUserName() -- method start");
		logger.debug("parameters are : { userName : " + userName + " }");

		StringBuilder strQueryToGetuserData = new StringBuilder();
		strQueryToGetuserData.append(String.format(" SELECT COUNT(UD.%s) AS %s ", USER_TABLE_COLOUMN.USER_ID, APPLICATION_GENERIC_ENUM.TOTAL_COUNT))
							.append(String.format(" FROM %s UD ", DATABASE_TABLE.ACC_USER_DETAILS))
							.append(String.format(" WHERE UD.%s = ? AND UD.%s = 0 ", USER_TABLE_COLOUMN.USER_NAME, USER_TABLE_COLOUMN.IS_DELETED))
							.append(String.format(" AND UD.%s = 0 ", USER_TABLE_COLOUMN.IS_HIDDEN));

		Object[] params = new Object[] {userName};
		if (userId > 0) {
			strQueryToGetuserData.append(String.format(" AND UD.%s != ? ", USER_TABLE_COLOUMN.USER_ID));
			params = new Object[] {userName, userId};
		}

		logger.debug("sql query : " + strQueryToGetuserData );
		logger.debug("UserDAO.getUserDetailsByUserName() -- method end");
		return namedParameterJdbcTemplate.getJdbcOperations().query(strQueryToGetuserData.toString(), params, getUserCount());
	}

}

