package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.USER_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class UserDocumentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<UserDocumentModel> getUserDocumentList(int userId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s, %s, %s ", USER_DOCUMENT_COLUMN.DOCUMENT_URL_ID, USER_DOCUMENT_COLUMN.DOCUMENT_URL, USER_DOCUMENT_COLUMN.DOCUMENT_TYPE, USER_DOCUMENT_COLUMN.DESCRIPTION))
			.append(String.format(" FROM %s ", DATABASE_TABLE.ACC_USER_DOCUMENT_DETAILS))
			.append(String.format(" WHERE %s = :%s AND %s = 0  ", USER_DOCUMENT_COLUMN.USER_ID, USER_DOCUMENT_COLUMN.USER_ID, USER_DOCUMENT_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(USER_DOCUMENT_COLUMN.USER_ID.toString(), userId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getUserDocumentResultExctractor());
	}

	private ResultSetExtractor<List<UserDocumentModel>> getUserDocumentResultExctractor() {
		return new ResultSetExtractor<List<UserDocumentModel>>() {
			@Override
			public List<UserDocumentModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<UserDocumentModel> userDocumentModelList = new ArrayList<>();
				while (rs.next()) {
					userDocumentModelList.add(new UserDocumentModel(rs));
				}
				return userDocumentModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveUserDocumentData(List<UserDocumentModel> userDocumentModelList, int userId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_USER_DOCUMENT_DETAILS))
				.append(String.format(" ( %s, %s, %s, %s ) ", USER_DOCUMENT_COLUMN.DOCUMENT_TYPE, USER_DOCUMENT_COLUMN.DESCRIPTION, USER_DOCUMENT_COLUMN.DOCUMENT_URL, USER_DOCUMENT_COLUMN.USER_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", USER_DOCUMENT_COLUMN.DOCUMENT_TYPE, USER_DOCUMENT_COLUMN.DESCRIPTION, USER_DOCUMENT_COLUMN.DOCUMENT_URL, USER_DOCUMENT_COLUMN.USER_ID));

		List<Map<String, Object>> userDocumentURLMapList = new ArrayList<>();
		Map<String, Object> userDocumentURLMap;
		for (UserDocumentModel userDocumentModel : userDocumentModelList) {
			userDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(userDocumentModel.getDocumentUrl())) {			
				userDocumentModel.setDocumentUrl(userDocumentModel.getDocumentUrl().replace(ApplicationUtility.getServerURL(), ""));
			}
			userDocumentURLMap.put(USER_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString(), userDocumentModel.getDocumentType());
			userDocumentURLMap.put(USER_DOCUMENT_COLUMN.DESCRIPTION.toString(), userDocumentModel.getDescription());
			userDocumentURLMap.put(USER_DOCUMENT_COLUMN.DOCUMENT_URL.toString(), userDocumentModel.getDocumentUrl());
			userDocumentURLMap.put(USER_DOCUMENT_COLUMN.USER_ID.toString(), userId);
			userDocumentURLMapList.add(userDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), userDocumentURLMapList.toArray(new HashMap[0]));
	}

	int deleteUserDocumentData(int userId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", DATABASE_TABLE.ACC_USER_DOCUMENT_DETAILS, USER_DOCUMENT_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", USER_DOCUMENT_COLUMN.USER_ID, USER_DOCUMENT_COLUMN.USER_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, userId });
	}

}
