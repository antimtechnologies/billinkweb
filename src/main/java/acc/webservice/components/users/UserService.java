package acc.webservice.components.users;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.enums.ROLES_TEXT;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;


@Service
@Lazy
public class UserService {

	private final Logger logger = Logger.getLogger(UserService.class);

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private UserCompanyMappingDAO userCompanyMappingDAO;

	@Autowired
	private UserCompanyMenuMappingDAO userCompanyMenuMappingDAO;

	@Autowired
	private UserDocumentDAO userDocumentDAO;

	public Integer getUserCount(String userName) {
		return userDAO.getUserCount(userName);
	}

	public UserModel getBasicUserDetailForUserName(String userName) {
		return userDAO.getBasicUserDetailForUserName(userName);
	}

	public boolean validateUserCredential(String userName, String password) {
		logger.debug("UserService.validateUserCredential() -- method start");
		logger.debug("Parameters are : userName : " + userName + ", password : " + password);
		return userDAO.validateUserCredential(userName, password);
	}

	public UserModel getUserDetailsByUserName(String userName) {
		logger.debug("UserService.getUserDetailsByUserName() -- method start");
		logger.debug("parameters are : Username : " + userName);
		UserModel userData = userDAO.getUserDetailsByUserName(userName);

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userData.getRole().getRoleName())) {
			userData.setCompanyList(userCompanyMappingDAO.getUserCompanyList(userData.getUserId()));
		}

		if (ROLES_TEXT.VENDOR_ROLE.toString().equalsIgnoreCase(userData.getRole().getRoleName())) {
			userData.setCompanyList(userCompanyMenuMappingDAO.getCompanyMenuMappingListForUser(userData));
		}

		userData.setUserDocumentList(userDocumentDAO.getUserDocumentList(userData.getUserId()));
		logger.debug("UserService.getUserDetailsByUserName() -- method end");
		return userData;
	}

	public UserModel saveUserData(UserModel userData) throws AccountingSofwareException {
		if(isUserNameAlreadyExist(userData.getUserName(), userData.getUserId())) {
			throw new AccountingSofwareException("User name already exist.");
		}

		userDAO.saveUserData(userData);
		if (ApplicationUtility.getSize(userData.getCompanyList()) > 0) {			
			userCompanyMappingDAO.saveUserCompanyMappingData(userData.getCompanyList(), userData.getUserId());
			userCompanyMenuMappingDAO.saveUserCompanyMenuMappingData(userData.getCompanyList(), userData.getUserId());
		}
	
		if (ApplicationUtility.getSize(userData.getUserDocumentList()) > 0) {
			userDocumentDAO.saveUserDocumentData(userData.getUserDocumentList(), userData.getUserId());
		}
		return userData;
	}

	private boolean isUserNameAlreadyExist(String userName, int userId) {
		return userDAO.isUserExistWithSameUserName(userName, userId) > 0;
	}

	public UserModel updateUserData(UserModel userData) throws AccountingSofwareException {

		if(isUserNameAlreadyExist(userData.getUserName(), userData.getUserId())) {
			throw new AccountingSofwareException("User name already exist.");
		}

		if (ApplicationUtility.getSize(userData.getCompanyList()) > 0) {			
			userCompanyMappingDAO.deleteUserCompanyData(userData.getUserId());
			userCompanyMenuMappingDAO.deleteUserCompanyMenuData(userData.getUserId());
			userCompanyMappingDAO.saveUserCompanyMappingData(userData.getCompanyList(), userData.getUserId());
			userCompanyMenuMappingDAO.saveUserCompanyMenuMappingData(userData.getCompanyList(), userData.getUserId());
		}

		userDocumentDAO.deleteUserDocumentData(userData.getUserId());
		if (ApplicationUtility.getSize(userData.getUserDocumentList()) > 0) {
			userDocumentDAO.saveUserDocumentData(userData.getUserDocumentList(), userData.getUserId());
		}

		return userDAO.updateUserData(userData);
	}

	public int deleteUserData(int userId, int deletedBy) {
		userCompanyMenuMappingDAO.deleteUserCompanyMenuData(userId);
		userCompanyMappingDAO.deleteUserCompanyData(userId);
		userDocumentDAO.deleteUserDocumentData(userId);
		return userDAO.deleteUserData(userId, deletedBy);
	}

	public List<UserModel> getUserDetails(int start, int noOfRecord, String userName) {
		return userDAO.getUserDetails(start, noOfRecord, userName);
	}

	public int changeUserPassword(String oldPassword, String newPassword, String userName) {
		return userDAO.changeUserPassword(oldPassword, newPassword, userName);
	}

	public int resetUserPassword(String newPassword, String userName) {
		return userDAO.resetUserPassword(newPassword, userName);
	}

	public UserModel getUserDetailsById(int userId) {
		logger.debug("UserService.getUserDetailsById() -- method start");
		logger.debug("parameters are : Username : " + userId);
		UserModel userData = userDAO.getUserDetailsById(userId);

		if (!ROLES_TEXT.ADMIN_ROLE.toString().equalsIgnoreCase(userData.getRole().getRoleName())) {
			userData.setCompanyList(userCompanyMappingDAO.getUserCompanyList(userData.getUserId()));
		}

		if (ROLES_TEXT.VENDOR_ROLE.toString().equalsIgnoreCase(userData.getRole().getRoleName())) {
			userData.setCompanyList(userCompanyMenuMappingDAO.getCompanyMenuMappingListForUser(userData));
		}
		userData.setUserDocumentList(userDocumentDAO.getUserDocumentList(userId));
		logger.debug("UserService.getUserDetailsById() -- method end");
		return userData;
	}

	public int updateUserDefaultCompany(String userName, int companyId) {
		return userDAO.updateUserDefaultCompany(userName, companyId);
	}

	public static int getSystemUserId() {
		return UserDAO.SYSTEM_USER_ID;
	}

}
