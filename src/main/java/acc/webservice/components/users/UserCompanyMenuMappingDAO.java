package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.menu.MenuModel;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.MENU_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_COMPANY_MENU_MAPPING_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Repository
@Lazy
public class UserCompanyMenuMappingDAO {

	private static final Logger logger = Logger.getLogger(UserCompanyMenuMappingDAO.class);

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<CompanyModel> getCompanyMenuMappingListForUser(UserModel userData) {

		logger.debug("UserCompanyMenuMappingDAO.getCompanyMenuMappingListForUser() -- method start");
		logger.debug("parameters are : { userId : " + userData + " } ");
		StringBuilder strQuryToFetchCompanyMenuData = new StringBuilder();
		List<MenuModel> menuList;

		strQuryToFetchCompanyMenuData.append(String.format(" SELECT MD.%s, MD.%s, ", MENU_DETAILS_COLUMN.MENU_ID, MENU_DETAILS_COLUMN.MENU_NAME))
				.append(String.format(" CASE WHEN UCM.%s IS NULL THEN 0 ELSE 1  END AS isMenuSelected ", USER_COMPANY_MENU_MAPPING_COLUMN.USER_COMPANY_MENU_MAPPING_ID))
				.append(String.format(" FROM %s MD ", DATABASE_TABLE.ACC_MENU_DETAILS))
				.append(String.format(" LEFT JOIN %s UCM ", DATABASE_TABLE.ACC_USER_COMPANY_MENU_MAPPING))
				.append(String.format(" ON MD.%s = UCM.%s AND UCM.%s = ? ", MENU_DETAILS_COLUMN.MENU_ID,  USER_COMPANY_MENU_MAPPING_COLUMN.MENU_ID, USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID))
				.append(String.format(" AND UCM.%s = ? AND UCM.%s = 0 ", USER_COMPANY_MENU_MAPPING_COLUMN.COMPANY_ID,  USER_COMPANY_MENU_MAPPING_COLUMN.IS_DELETED))
				.append(String.format(" WHERE MD.%s = 0 ", MENU_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" ORDER BY MD.%s ", MENU_DETAILS_COLUMN.DISPLAY_INDEX));

		List<CompanyModel> companyList = userData.getCompanyList();
		List<CompanyModel> outputCompanyList = new ArrayList<>();
		if (ApplicationUtility.getSize(companyList) <= 0) {
			return companyList;
		}

		for (CompanyModel companyModel : companyList) {
			menuList = namedParameterJdbcTemplate.getJdbcOperations().query(strQuryToFetchCompanyMenuData.toString(), new Object[] { userData.getUserId(), companyModel.getCompanyId() }, resultSetExctractorToFetchCompanMenuData());
			companyModel.setUserCompanyMenuList(menuList);
			outputCompanyList.add(companyModel);
		}

		return outputCompanyList;		
	}

	private ResultSetExtractor<List<MenuModel>> resultSetExctractorToFetchCompanMenuData() {
		return new ResultSetExtractor<List<MenuModel>>() {
			@Override
			public List<MenuModel> extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<MenuModel> menuList = new ArrayList<>();
				MenuModel menuDetails = null;

				while (rs.next()) {
					menuDetails = new MenuModel(rs);
					menuDetails.setSelected(rs.getBoolean("isMenuSelected"));
					menuList.add(menuDetails);
				}

				return menuList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	void saveUserCompanyMenuMappingData(List<CompanyModel> companyList, int userId) {
		List<Map<String, Object>> userCompantMenuMappingDataList = new ArrayList<>();
		Map<String, Object> userCompantMenuMappingData = new HashMap<>();
		List<MenuModel> menuList;
		for (CompanyModel companyModel : companyList) {
			menuList = companyModel.getUserCompanyMenuList();

			if (menuList == null) {
				continue;
			}
			for (MenuModel menu : menuList) {
				
				if (!menu.isSelected()) {
					continue;
				}
				userCompantMenuMappingData = new HashMap<>();

				userCompantMenuMappingData.put(USER_COMPANY_MENU_MAPPING_COLUMN.COMPANY_ID.toString(), companyModel.getCompanyId());
				userCompantMenuMappingData.put(USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID.toString(), userId);
				userCompantMenuMappingData.put(USER_COMPANY_MENU_MAPPING_COLUMN.MENU_ID.toString(), menu.getMenuId());

				userCompantMenuMappingDataList.add(userCompantMenuMappingData);
			}
		}

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" INSERT INTO %s ", DATABASE_TABLE.ACC_USER_COMPANY_MENU_MAPPING))
				.append(String.format(" ( %s, %s, %s ) ", USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID, USER_COMPANY_MENU_MAPPING_COLUMN.COMPANY_ID, USER_COMPANY_MENU_MAPPING_COLUMN.MENU_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s ) ", USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID, USER_COMPANY_MENU_MAPPING_COLUMN.COMPANY_ID, USER_COMPANY_MENU_MAPPING_COLUMN.MENU_ID));

		if (ApplicationUtility.getSize(userCompantMenuMappingDataList) > 0)
			namedParameterJdbcTemplate.batchUpdate(sqlQuery.toString(), userCompantMenuMappingDataList.toArray(new HashMap[0]));
	}

	int deleteUserCompanyMenuData(int userId) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_USER_COMPANY_MENU_MAPPING))
							.append(String.format(" %s = :%s ", USER_COMPANY_MENU_MAPPING_COLUMN.IS_DELETED, USER_COMPANY_MENU_MAPPING_COLUMN.IS_DELETED))
							.append(String.format(" WHERE %s = :%s  ", USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID, USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(USER_COMPANY_MENU_MAPPING_COLUMN.USER_ID.toString(), userId)
				.addValue(USER_COMPANY_MENU_MAPPING_COLUMN.IS_DELETED.toString(), 1);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

}

