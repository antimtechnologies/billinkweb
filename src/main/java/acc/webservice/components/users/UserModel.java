package acc.webservice.components.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.roles.RoleModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class UserModel {

	private int userId;
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private RoleModel role;
	private long mobileNumber;
	private String emailId;
	private String fullAddress;
	private int pincode;
	private StateModel state;
	private boolean isMultiLoginAllowed;
	private CompanyModel defaultCompany;
	private short noOfAllowedCompany;
	private List<CompanyModel> companyList;
	private String profilePhotoURL;
	private UserModel addedBy;
	private String addedDateTime;
	private String userLicenceExpiryDate;
	private boolean isUserLoggedIn;
	private String designation;
	private String cityName;
	private List<UserDocumentModel> userDocumentList;

	public UserModel() {
	}

	public UserModel(ResultSet rs) throws SQLException {
		setUserId(rs.getInt(USER_TABLE_COLOUMN.USER_ID.toString()));
		setCityName(rs.getString(USER_TABLE_COLOUMN.CITY_NAME.toString()));
		setUserName(rs.getString(USER_TABLE_COLOUMN.USER_NAME.toString()));
		setFirstName(rs.getString(USER_TABLE_COLOUMN.FIRST_NAME.toString()));
		setLastName(rs.getString(USER_TABLE_COLOUMN.LAST_NAME.toString()));
		setMobileNumber(rs.getLong(USER_TABLE_COLOUMN.MOBILE_NUMBER.toString()));
		setEmailId(rs.getString(USER_TABLE_COLOUMN.EMAIL_ID.toString()));
		setFullAddress(rs.getString(USER_TABLE_COLOUMN.FULL_ADDRESS.toString()));
		setPincode(rs.getInt(USER_TABLE_COLOUMN.PIN_CODE.toString()));
		setMultiLoginAllowed(rs.getBoolean(USER_TABLE_COLOUMN.IS_MULTI_LOGGEDIN_ALLOWED.toString()));
		setNoOfAllowedCompany(rs.getShort(USER_TABLE_COLOUMN.NO_OF_ALLOWED_COMPANY.toString()));
		setProfilePhotoURL(
				ApplicationUtility.getServerURL() + rs.getString(USER_TABLE_COLOUMN.PROFILE_PHOTO_URL.toString()));
		setDesignation(rs.getString(USER_TABLE_COLOUMN.DESIGNATION.toString()));

		Timestamp fetchedAddedDateTime = rs.getTimestamp(USER_TABLE_COLOUMN.ADDED_DATE_TIME.toString());
		if (fetchedAddedDateTime != null) {
			setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDateTime));
		}

		Timestamp fetchedUserLicenceExpiryDate = rs
				.getTimestamp(USER_TABLE_COLOUMN.USER_LICENCE_EXPIRY_DATE.toString());
		if (fetchedUserLicenceExpiryDate != null) {
			setUserLicenceExpiryDate(DateUtility.converDateToUserString(fetchedUserLicenceExpiryDate,
					DateUtility.DEFAULT_USER_DATE_TIME_FORMAT));
		}

		CompanyModel defaultCompanyModel = new CompanyModel();
		defaultCompanyModel.setCompanyId(rs.getInt(COMPANY_TABLE_COLUMN.COMPANY_ID.toString()));
		defaultCompanyModel.setCompanyName(rs.getString(COMPANY_TABLE_COLUMN.COMPANY_NAME.toString()));
		defaultCompanyModel.setProfilePhotoURL(
				ApplicationUtility.getServerURL() + rs.getString(COMPANY_TABLE_COLUMN.PROFILE_PHOTO_URL.toString()));

		setDefaultCompany(defaultCompanyModel);
		setState(new StateModel(rs));
		setRole(new RoleModel(rs));
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public boolean isMultiLoginAllowed() {
		return isMultiLoginAllowed;
	}

	public void setMultiLoginAllowed(boolean isMultiLoginAllowed) {
		this.isMultiLoginAllowed = isMultiLoginAllowed;
	}

	public short getNoOfAllowedCompany() {
		return noOfAllowedCompany;
	}

	public void setNoOfAllowedCompany(short noOfAllowedCompany) {
		this.noOfAllowedCompany = noOfAllowedCompany;
	}

	public RoleModel getRole() {
		return role;
	}

	public void setRole(RoleModel role) {
		this.role = role;
	}

	public StateModel getState() {
		return state;
	}

	public void setState(StateModel state) {
		this.state = state;
	}

	public CompanyModel getDefaultCompany() {
		return defaultCompany;
	}

	public void setDefaultCompany(CompanyModel defaultCompany) {
		this.defaultCompany = defaultCompany;
	}

	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}

	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}

	public List<CompanyModel> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<CompanyModel> companyList) {
		this.companyList = companyList;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public String getUserLicenceExpiryDate() {
		return userLicenceExpiryDate;
	}

	public void setUserLicenceExpiryDate(String userLicenceExpiryDate) {
		this.userLicenceExpiryDate = userLicenceExpiryDate;
	}

	public boolean isUserLoggedIn() {
		return isUserLoggedIn;
	}

	public void setUserLoggedIn(boolean isUserLoggedIn) {
		this.isUserLoggedIn = isUserLoggedIn;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<UserDocumentModel> getUserDocumentList() {
		return userDocumentList;
	}

	public void setUserDocumentList(List<UserDocumentModel> userDocumentList) {
		this.userDocumentList = userDocumentList;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public String toString() {
		return "UserModel [userId=" + userId + ", userName=" + userName + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", role =" + role + ", mobileNumber=" + mobileNumber
				+ ", emailId=" + emailId + ", fullAddress=" + fullAddress + ", pincode=" + pincode
				+ ", isMultiLoginAllowed=" + isMultiLoginAllowed + ", defaultCompany=" + defaultCompany
				+ ", noOfAllowedCompany=" + noOfAllowedCompany + "]";
	}

}
