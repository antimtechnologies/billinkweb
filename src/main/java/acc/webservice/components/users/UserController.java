package acc.webservice.components.users;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.exception.model.AuthenticationException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/userData/")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value="validateUserCredential", method=RequestMethod.POST)
    public Map<String, Object> validateUserCredential(@RequestBody Map<String, Object> parameter, HttpServletRequest request) throws AuthenticationException {
		Map<String, Object> responseData = new HashMap<>();

		String userName = ApplicationUtility.getStrValue(parameter, USER_TABLE_COLOUMN.USER_NAME.toString());
		String password = ApplicationUtility.getStrValue(parameter, USER_TABLE_COLOUMN.PASSWORD.toString());

		if (userService.validateUserCredential(userName, password)) {
			UserModel userModel = userService.getBasicUserDetailForUserName(userName);
			responseData.put("status", "success");
			responseData.put("data", userModel);
			HttpSession session = request.getSession();
			session.setAttribute(USER_TABLE_COLOUMN.USER_NAME.toString(), userName);			
			session.setAttribute(USER_TABLE_COLOUMN.USER_ID.toString(), userModel.getUserId());
			return responseData;
		}
		
		throw new AuthenticationException("Invalid credential data.");
    }

	@RequestMapping(value="getUserDetailsByUserName", method=RequestMethod.POST)
	public Map<String, Object> getUserDetailsByUserName(@RequestBody Map<String, Object> parameter, HttpServletRequest request) throws AuthenticationException {
		HttpSession session = request.getSession();
		String userName = ApplicationUtility.getStrValue(parameter, USER_TABLE_COLOUMN.USER_NAME.toString());
		
		if ("".equals(userName)) {
			userName = (String) session.getAttribute(USER_TABLE_COLOUMN.USER_NAME.toString());
		}

		if (ApplicationUtility.getSize(userName) < 1) {
			throw new AuthenticationException("Invalid user data found.");
		}

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", userService.getUserDetailsByUserName(userName));
		return responseData;
	}

	@RequestMapping(value="getUserDetailsById", method=RequestMethod.POST)
	public Map<String, Object> getUserDetailsById(@RequestBody Map<String, Object> parameter, HttpServletRequest request) {
		int userId = ApplicationUtility.getIntValue(parameter, USER_TABLE_COLOUMN.USER_ID.toString());
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", userService.getUserDetailsById(userId));
		return responseData;
	}

	@RequestMapping(value="getUserDetails", method=RequestMethod.POST)
	public Map<String, Object> getUserDetails(@RequestBody Map<String, Object> parameter) {
		int start = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		String userName = ApplicationUtility.getStrValue(parameter, USER_TABLE_COLOUMN.USER_NAME.toString());

		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", userService.getUserDetails(start, noOfRecord, userName));
		
		if (start == 0) {
			responseData.put(APPLICATION_GENERIC_ENUM.TOTAL_COUNT.toString(), userService.getUserCount(userName));
		}
		return responseData;
	}

	@RequestMapping(value="saveUserData", method=RequestMethod.POST)
	public Map<String,Object> saveUserData(@RequestBody UserModel userData) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", userService.saveUserData(userData));
		return responseData;
	}

	@RequestMapping(value="updateUserData", method=RequestMethod.POST)
	public Map<String, Object> updateUserData(@RequestBody UserModel userData) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", userService.updateUserData(userData));
		return responseData;
	}

	@RequestMapping(value="deleteUserData", method=RequestMethod.POST)
	public Map<String, Object> deleteUserData(@RequestBody Map<String,Object> requestData) {
		int userId = ApplicationUtility.getIntValue(requestData, USER_TABLE_COLOUMN.USER_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(requestData, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		userService.deleteUserData(userId, deletedBy);
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		return responseData;
	}

	@RequestMapping(value="changeUserPassword", method=RequestMethod.POST)
	public Map<String, Object> changeUserPassword(@RequestBody Map<String,Object> requestData) {
		String oldPassword = ApplicationUtility.getStrValue(requestData, "oldPassword");
		String newPassword = ApplicationUtility.getStrValue(requestData, "newPassword");
		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("updateCount", userService.changeUserPassword(oldPassword, newPassword, userName));
		return responseData;

	}

	@RequestMapping(value="resetUserPassword", method=RequestMethod.POST)
	public Map<String, Object> resetUserPassword(@RequestBody Map<String,Object> requestData) {
		String newPassword = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.PASSWORD.toString());
		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("updateCount", userService.resetUserPassword(newPassword, userName));
		return responseData;
	}

	@RequestMapping(value="logOut", method=RequestMethod.POST)
	public void logOut(@RequestBody Map<String,Object> requestData, HttpServletRequest request) {
		request.getSession().invalidate();
	}


	@RequestMapping(value="updateUserDefaultCompany", method=RequestMethod.POST)
	public Map<String, Object> updateUserDefaultCompany(@RequestBody Map<String,Object> requestData, HttpServletRequest request) throws AuthenticationException {
		
		HttpSession session = request.getSession();
		String userName = ApplicationUtility.getStrValue(requestData, USER_TABLE_COLOUMN.USER_NAME.toString());

		if ("".equals(userName)) {
			userName = (String) session.getAttribute(USER_TABLE_COLOUMN.USER_NAME.toString());
		}

		if ("".equals(userName.trim())) {
			throw new AuthenticationException("User is not authenticate to perform action");
		}

		int companyId = ApplicationUtility.getIntValue(requestData, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("updateCount", userService.updateUserDefaultCompany(userName, companyId));
		return responseData;		
	}
}
