package acc.webservice.components.exceldocument;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_EXCEL_DOCUMENT;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/excelUploadData/")
public class ExcelDocumentController {

	@Autowired
	private ExcelDocumentService excelDocumentService;

	@RequestMapping(value="getExcelDocumentList", method=RequestMethod.POST)
	public Map<String,Object> getExcelDocumentList(@RequestBody Map<String, Object> parameter) {
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());

		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", excelDocumentService.getExcelDocumentList(companyId, start, noOfRecord));
		return response;

	}
	
	@RequestMapping(value="getExcelUploadDetailsById", method=RequestMethod.POST)
	public Map<String,Object> getExcelUploadDetailsById(@RequestBody Map<String, Object> parameter) {
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int excelDocumentId = ApplicationUtility.getIntValue(parameter, ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString());

		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", excelDocumentService.getExcelUploadDetailsById(companyId, excelDocumentId));
		return response;

	}
	@RequestMapping(value="saveExcelDocumentData", method=RequestMethod.POST)
	public Map<String,Object> saveExcelDocumentList(@RequestBody ExcelDocumentModel excelDocumentModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", excelDocumentService.saveExcelDocumentList(excelDocumentModel));
		return response;
	}
	
	@RequestMapping(value="deleteExcelDocumentData", method=RequestMethod.POST)
	public Map<String,Object> deleteExcelDocumentList(@RequestBody ExcelDocumentModel excelDocumentModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", excelDocumentService.deleteExcelDocumentList(excelDocumentModel));
		return response;
	}

	@RequestMapping(value="runExcelDocumentData", method=RequestMethod.POST)
	public Map<String,Object> runExcelDocumentData(@RequestBody ExcelDocumentModel excelDocumentModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", excelDocumentService.runExcelDocumentData(excelDocumentModel));
		return response;
	}


}
