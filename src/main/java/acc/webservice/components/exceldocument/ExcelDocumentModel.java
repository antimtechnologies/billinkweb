package acc.webservice.components.exceldocument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_EXCEL_DOCUMENT;
import acc.webservice.global.utils.DateUtility;

public class ExcelDocumentModel {

	private int documentId;
	private int companyId;
	private String documentStatus;
	private UserModel addedBy;
	private UserModel deletedBy;
	private UserModel lastRunBy;
	private String documentURL;
	private String errorDocumentURL;
	private String documentType;
	private String addedDateTime;
	private String lastRunDateTime;
	private String deletedDateTime;
	private String description;
	private boolean isDeleted;
	
	public ExcelDocumentModel() {
	}

	public ExcelDocumentModel(ResultSet resultSet) throws SQLException {
		setDocumentId(resultSet.getInt(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString()));
		setCompanyId(resultSet.getInt(ACC_EXCEL_DOCUMENT.COMPANY_ID.toString()));
		setDocumentStatus(resultSet.getString(ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS.toString()));

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(resultSet.getInt(ACC_EXCEL_DOCUMENT.ADDED_BY.toString()));
		fetchedAddedBy.setUserName(resultSet.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(resultSet.getInt(ACC_EXCEL_DOCUMENT.DELETED_BY.toString()));
		fetchedDeletedBy.setUserName(resultSet.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		UserModel fetchedLastRunBy = new UserModel();
		fetchedLastRunBy.setUserId(resultSet.getInt(ACC_EXCEL_DOCUMENT.LASTRUN_BY.toString()));
		fetchedLastRunBy.setUserName(resultSet.getString("lastRunByUserName"));
		setLastRunBy(fetchedLastRunBy);

		Timestamp deletedDateTime = resultSet.getTimestamp(ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME.toString());
		if (deletedDateTime != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(deletedDateTime));
		}

		Timestamp addedDateTime = resultSet.getTimestamp(ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME.toString());
		if (addedDateTime != null) {
			setAddedDateTime(DateUtility.converDateToUserString(addedDateTime));
		}
		
		Timestamp lastRunDateTime = resultSet.getTimestamp(ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME.toString());
		if (lastRunDateTime != null) {
			setLastRunDateTime(DateUtility.converDateToUserString(lastRunDateTime));
		}

		setDocumentURL(resultSet.getString(ACC_EXCEL_DOCUMENT.DOCUMENT_URL.toString()));
		setErrorDocumentURL(resultSet.getString(ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL.toString()));
		setDocumentType(resultSet.getString(ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE.toString()));
		setDescription(resultSet.getString(ACC_EXCEL_DOCUMENT.DESCRIPTION.toString()));

	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public UserModel getLastRunBy() {
		return lastRunBy;
	}

	public void setLastRunBy(UserModel lastRunBy) {
		this.lastRunBy = lastRunBy;
	}

	public String getDocumentURL() {
		return documentURL;
	}

	public void setDocumentURL(String documentURL) {
		this.documentURL = documentURL;
	}

	public String getErrorDocumentURL() {
		return errorDocumentURL;
	}

	public void setErrorDocumentURL(String errorDocumentURL) {
		this.errorDocumentURL = errorDocumentURL;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public String getLastRunDateTime() {
		return lastRunDateTime;
	}

	public void setLastRunDateTime(String lastRunDateTime) {
		this.lastRunDateTime = lastRunDateTime;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
