package acc.webservice.components.exceldocument;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.excel.CronJobScheduler;
import acc.webservice.enums.ApplicationGenericEnum.EXCELL_DOCUMENT_STATUS_ENUM;

@Service
@Lazy
public class ExcelDocumentService {

	@Autowired
	private ExcelDocumentDAO excelDocumentDAO;

	@Autowired
	private CronJobScheduler cronJobScheduler;

	@Autowired
	private ServletContext servletContext;

	
	public List<ExcelDocumentModel> getExcelDocumentList(int companyId, int start, int noOfRecord) {
		return excelDocumentDAO.getExcelDocumentList(companyId, start, noOfRecord);
	}

	public ExcelDocumentModel saveExcelDocumentList(ExcelDocumentModel excelDocumentModel) {
		excelDocumentModel.setDocumentStatus(EXCELL_DOCUMENT_STATUS_ENUM.PENDING_STATUS.status());
		ExcelDocumentModel saveExcelDocument = excelDocumentDAO.saveExcelDocumentList(excelDocumentModel);
		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split(servletContext.getContextPath() + "/WEB-INF/classes/");
			saveExcelDocument.setDocumentURL(pathArr[0]+saveExcelDocument.getDocumentURL());
			cronJobScheduler.scheduleExcellSheet(saveExcelDocument);
		} catch (UnsupportedEncodingException e) {
			
		}
		return saveExcelDocument;
	}

	public ExcelDocumentModel updateExcelDocumentWhileRun(ExcelDocumentModel excelDocumentModel) {
		excelDocumentModel.setDocumentStatus(EXCELL_DOCUMENT_STATUS_ENUM.PENDING_STATUS.status());
		return excelDocumentDAO.updateExcelDocumentWhileRun(excelDocumentModel);
	}

	public int deleteExcelDocumentList(ExcelDocumentModel excelDocumentModel) {
		return excelDocumentDAO.deleteExcelDocumentList(excelDocumentModel);
	}
	public ExcelDocumentModel updateExcelDocumentWhileError(ExcelDocumentModel excelDocumentModel) {
		return excelDocumentDAO.updateExcelDocumentWhileError(excelDocumentModel);
	}
	
	public ExcelDocumentModel updateExcelDocumentWhileSuccess(ExcelDocumentModel excelDocumentModel) {
		return excelDocumentDAO.updateExcelDocumentWhileSuccess(excelDocumentModel);
	}

	public ExcelDocumentModel runExcelDocumentData(ExcelDocumentModel excelDocumentModel) {
		ExcelDocumentModel saveExcelDocument = excelDocumentDAO.getExcelDocumentDetailsByDocumentId(excelDocumentModel.getCompanyId(), excelDocumentModel.getDocumentId());
		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split(servletContext.getContextPath() + "/WEB-INF/classes/");
			saveExcelDocument.setDocumentURL(pathArr[0]+saveExcelDocument.getDocumentURL());
			cronJobScheduler.scheduleExcellSheet(saveExcelDocument);
		} catch (UnsupportedEncodingException e) {
			
		}
		return saveExcelDocument;
	}

	public ExcelDocumentModel getExcelUploadDetailsById(int companyId, int excelDocumentId) {
		return excelDocumentDAO.getExcelDocumentDetailsByDocumentId(companyId, excelDocumentId);
	}


	
}
