package acc.webservice.components.exceldocument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.ApplicationGenericEnum.EXCELL_DOCUMENT_STATUS_ENUM;
import acc.webservice.enums.DatabaseEnum.ACC_EXCEL_DOCUMENT;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class ExcelDocumentDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<ExcelDocumentModel> getExcelDocumentList(int companyId, int start, int noOfRecord) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.DESCRIPTION))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_URL, ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL, ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS, ACC_EXCEL_DOCUMENT.ADDED_BY, ACC_EXCEL_DOCUMENT.DELETED_BY))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.LASTRUN_BY, ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME, ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME))
				.append(String.format(" EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME, ACC_EXCEL_DOCUMENT.IS_DELETED))
				.append(String.format(" AUD.%s AS %s, DUD.%s AS %s, RUD.%s AS lastRunByUserName ", USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME, USER_TABLE_COLOUMN.USER_NAME, 
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME, USER_TABLE_COLOUMN.USER_NAME))
				.append(String.format(" FROM %s AS EDD ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
				.append(String.format(" INNER JOIN %s AS AUD ON AUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.ADDED_BY))
				.append(String.format(" LEFT JOIN %s AS RUD ON RUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.LASTRUN_BY))
				.append(String.format(" LEFT JOIN %s AS DUD ON DUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.DELETED_BY))
				.append(String.format(" WHERE EDD.%s = 0 AND EDD.%s = :%s ", ACC_EXCEL_DOCUMENT.IS_DELETED, ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.COMPANY_ID))
				.append(String.format(" ORDER BY EDD.%s DESC ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID));

		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(), APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
		parameters.put(ACC_EXCEL_DOCUMENT.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getExcelDocumentModelResultSetExctractor());
	}
	
	private ResultSetExtractor<List<ExcelDocumentModel>> getExcelDocumentModelResultSetExctractor() {
		return new ResultSetExtractor<List<ExcelDocumentModel>>() {

			@Override
			public List<ExcelDocumentModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ExcelDocumentModel> excelDocumentList = new ArrayList<>();
				while (rs.next()) {
					excelDocumentList.add(new ExcelDocumentModel(rs));
				}
				return excelDocumentList;
			}
		};
	}

	ExcelDocumentModel getExcelDocumentDetailsByDocumentId(int companyId, int documentId) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.DESCRIPTION))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_URL, ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL, ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS, ACC_EXCEL_DOCUMENT.ADDED_BY, ACC_EXCEL_DOCUMENT.DELETED_BY))
				.append(String.format(" EDD.%s, EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.LASTRUN_BY, ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME, ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME))
				.append(String.format(" EDD.%s, EDD.%s, ", ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME, ACC_EXCEL_DOCUMENT.IS_DELETED))
				.append(String.format(" AUD.%s AS %s, DUD.%s AS %s, RUD.%s AS lastRunByUserName ", USER_TABLE_COLOUMN.USER_NAME, APPLICATION_GENERIC_ENUM.ADDED_BY_NAME, USER_TABLE_COLOUMN.USER_NAME, 
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME, USER_TABLE_COLOUMN.USER_NAME))
				.append(String.format(" FROM %s AS EDD ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
				.append(String.format(" INNER JOIN %s AS AUD ON AUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.ADDED_BY))
				.append(String.format(" LEFT JOIN %s AS RUD ON RUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.LASTRUN_BY))
				.append(String.format(" LEFT JOIN %s AS DUD ON DUD.%s = EDD.%s ", DATABASE_TABLE.ACC_USER_DETAILS, USER_TABLE_COLOUMN.USER_ID, ACC_EXCEL_DOCUMENT.DELETED_BY))
				.append(String.format(" WHERE EDD.%s = 0 AND EDD.%s = :%s ", ACC_EXCEL_DOCUMENT.IS_DELETED, ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.DOCUMENT_ID))
				.append(String.format(" AND EDD.%s = :%s ", ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.COMPANY_ID));
			

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString(), documentId);
		parameters.put(ACC_EXCEL_DOCUMENT.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getExcelDocumentDetailsResultSetExctractor());
	}
	private ResultSetExtractor<ExcelDocumentModel> getExcelDocumentDetailsResultSetExctractor() {
		return new ResultSetExtractor<ExcelDocumentModel>() {

			@Override
			public ExcelDocumentModel extractData(ResultSet rs) throws SQLException, DataAccessException {
				ExcelDocumentModel excelDocumentModel = new ExcelDocumentModel();
				while (rs.next()) {
					excelDocumentModel = new ExcelDocumentModel(rs);
				}
				return excelDocumentModel;
			}
		};
	}


	public ExcelDocumentModel saveExcelDocumentList(ExcelDocumentModel excelDocumentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
				.append(String.format(" (%s, %s, %s,  ", ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.DESCRIPTION, ACC_EXCEL_DOCUMENT.DOCUMENT_URL))
				.append(String.format("  %s, %s, %s,  ", ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL, ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE, ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS))
				.append(String.format("  %s, %s ) ", ACC_EXCEL_DOCUMENT.ADDED_BY, ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME))
				.append(" VALUES ")
				.append(String.format(" (:%s, :%s, :%s, ", ACC_EXCEL_DOCUMENT.COMPANY_ID, ACC_EXCEL_DOCUMENT.DESCRIPTION, ACC_EXCEL_DOCUMENT.DOCUMENT_URL))
				.append(String.format("  :%s, :%s, :%s, ", ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL, ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE, ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS))
				.append(String.format("  :%s, :%s )", ACC_EXCEL_DOCUMENT.ADDED_BY, ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME));
		
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
						.addValue(ACC_EXCEL_DOCUMENT.COMPANY_ID.toString(), excelDocumentModel.getCompanyId())
						.addValue(ACC_EXCEL_DOCUMENT.DESCRIPTION.toString(), excelDocumentModel.getDescription())
						.addValue(ACC_EXCEL_DOCUMENT.DOCUMENT_URL.toString(), excelDocumentModel.getDocumentURL())
						.addValue(ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL.toString(), excelDocumentModel.getErrorDocumentURL())
						.addValue(ACC_EXCEL_DOCUMENT.DOCUMENT_TYPE.toString(), excelDocumentModel.getDocumentType())
						.addValue(ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS.toString(), excelDocumentModel.getDocumentStatus())
						.addValue(ACC_EXCEL_DOCUMENT.ADDED_BY.toString(), excelDocumentModel.getAddedBy().getUserId())
						.addValue(ACC_EXCEL_DOCUMENT.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		excelDocumentModel.setDocumentId(holder.getKey().intValue());
		return excelDocumentModel;
	}

	public ExcelDocumentModel updateExcelDocumentWhileRun(ExcelDocumentModel excelDocumentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS, ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.LASTRUN_BY, ACC_EXCEL_DOCUMENT.LASTRUN_BY))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME, ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME))
		.append(String.format(" WHERE %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.DOCUMENT_ID));
		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS.toString(), excelDocumentModel.getDocumentStatus());
		parameters.put(ACC_EXCEL_DOCUMENT.LASTRUN_BY.toString(), excelDocumentModel.getLastRunBy());
		parameters.put(ACC_EXCEL_DOCUMENT.LAST_RUN_DATE_TIME.toString(), DateUtility.getCurrentDateTime());
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString(), excelDocumentModel.getDocumentId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return excelDocumentModel;
	}

	public int deleteExcelDocumentList(ExcelDocumentModel excelDocumentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
		.append(String.format(" %s = :%s, ", ACC_EXCEL_DOCUMENT.IS_DELETED, ACC_EXCEL_DOCUMENT.IS_DELETED))
		.append(String.format(" %s = :%s, ", ACC_EXCEL_DOCUMENT.DELETED_BY, ACC_EXCEL_DOCUMENT.DELETED_BY))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME, ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME))
		.append(String.format(" WHERE %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.DOCUMENT_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_EXCEL_DOCUMENT.IS_DELETED.toString(), 1);
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString(), excelDocumentModel.getDocumentId());
		parameters.put(ACC_EXCEL_DOCUMENT.DELETED_BY.toString(), excelDocumentModel.getDeletedBy().getUserId());
		parameters.put(ACC_EXCEL_DOCUMENT.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());
		
		return namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);

	}
	public ExcelDocumentModel updateExcelDocumentWhileError(ExcelDocumentModel excelDocumentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
		.append(String.format(" %s = :%s, ", ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL, ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS, ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS))
		.append(String.format(" WHERE %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.DOCUMENT_ID));
		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_EXCEL_DOCUMENT.ERROR_DOCUMENT_URL.toString(), excelDocumentModel.getErrorDocumentURL());
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS.toString(), EXCELL_DOCUMENT_STATUS_ENUM.ERROR_STATUS.status());
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString(), excelDocumentModel.getDocumentId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return excelDocumentModel;
	}
	public ExcelDocumentModel updateExcelDocumentWhileSuccess(ExcelDocumentModel excelDocumentModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", DATABASE_TABLE.ACC_EXCELL_DOCUMENT))
		.append(String.format(" %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS, ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS))
		.append(String.format(" WHERE %s = :%s ", ACC_EXCEL_DOCUMENT.DOCUMENT_ID, ACC_EXCEL_DOCUMENT.DOCUMENT_ID));
		
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_STATUS.toString(), EXCELL_DOCUMENT_STATUS_ENUM.COMPLETE_STATUS.status());
		parameters.put(ACC_EXCEL_DOCUMENT.DOCUMENT_ID.toString(), excelDocumentModel.getDocumentId());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return excelDocumentModel;
	}

}
