package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.enums.DatabaseEnum.PURCHASE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class PurchaseImageModel {

	private int imageId;
	private String imageURL;
	private boolean deleted;
	private PurchaseModel purchaseModel;

	public PurchaseImageModel() {
	}

	public PurchaseImageModel(ResultSet rs) throws SQLException {
		setImageId(rs.getInt(PURCHASE_IMAGE_URL_COLUMN.IMAGE_ID.toString()));
		String imageURL = rs.getString(PURCHASE_IMAGE_URL_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public PurchaseModel getPurchaseModel() {
		return purchaseModel;
	}

	public void setPurchaseModel(PurchaseModel purchaseModel) {
		this.purchaseModel = purchaseModel;
	}

	@Override
	public String toString() {
		return "CreditNoteImageModel [imageId=" + imageId + ", imageURL=" + imageURL + ", deleted=" + deleted + "]";
	}

}
