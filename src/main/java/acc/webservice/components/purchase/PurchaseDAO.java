package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.items.ItemMrpModel;
import acc.webservice.components.items.ItemPurchaseRateModel;
import acc.webservice.components.ledger.LedgerPurchaseDetailsModel;
import acc.webservice.components.ledger.MultipleGstModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.DEBITNOTE_ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_BATCH_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_PURCHASE_RATE_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_MULTIPLE_GST_COLUMN;
import acc.webservice.enums.DatabaseEnum.LEDGER_PURCHASE_DETAILS;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATE_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.STATUS_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.USER_TABLE_COLOUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class PurchaseDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private StatusUtil statusUtil;

	@SuppressWarnings("unchecked")
	List<PurchaseModel> getPurchaseListData(int companyId, int start, int numberOfRecord,
			Map<String, Object> filterParamter) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		Map<String, Object> parameters = new HashMap<>();

		sqlQueryToFetchData
				.append(String.format(" SELECT PD.%s, PD.%s, PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.IMAGE_URL, PURCHASE_DETAILS_COLUMN.BILL_NUMBER,
						PURCHASE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_DATE,
						PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, PURCHASE_DETAILS_COLUMN.DISCOUNT,
						PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME, PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME, PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s,PD.%s,PD.%s, ",
						PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1, PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2,
						PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT, PURCHASE_DETAILS_COLUMN.P_O_NUMBER,
						PURCHASE_DETAILS_COLUMN.BILL_DATE))
				.append(String.format(" PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						PURCHASE_DETAILS_COLUMN.L_R_NUMBER))
				.append(String.format(" PD.%s, PD.%s,PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.BILL_DATE,
						PURCHASE_DETAILS_COLUMN.P_O_NUMBER, PURCHASE_DETAILS_COLUMN.CITY_NAME,
						PURCHASE_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" FROM %s PD ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" INNER JOIN %s SM ON PD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						PURCHASE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON PD.%s = LM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON PD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s MUM ON PD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON PD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s DUM ON PD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" WHERE PD.%s = 0 ", PURCHASE_DETAILS_COLUMN.IS_DELETED)).append(String.format(
						" AND PD.%s = :%s ", PURCHASE_DETAILS_COLUMN.COMPANY_ID, PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		if (ApplicationUtility.getSize(filterParamter) > 0) {
			if (filterParamter.containsKey("statusList")) {
				List<String> statusList = (List<String>) filterParamter.get("statusList");
				if (ApplicationUtility.getSize(statusList) > 0) {
					sqlQueryToFetchData
							.append(String.format(" AND PD.%s IN (:statusList) ", PURCHASE_DETAILS_COLUMN.STATUS_ID));
					parameters.put("statusList", statusUtil.getStatusIdList(statusList));
				}
			}

			if (filterParamter.containsKey(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString())) {
				int isVerified = ApplicationUtility.getIntValue(filterParamter,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString());

				if (isVerified == 0 || isVerified == 1) {
					sqlQueryToFetchData.append(String.format(" AND PD.%s = :%s ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED,
							PURCHASE_DETAILS_COLUMN.IS_VERIFIED));
					parameters.put(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), isVerified);
				}
			}

			if (filterParamter.containsKey(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString())) {
				int ledgerId = ApplicationUtility.getIntValue(filterParamter,
						PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString());

				if (ledgerId > 0) {
					sqlQueryToFetchData.append(String.format(" AND PD.%s = :%s ", PURCHASE_DETAILS_COLUMN.LEDGER_ID,
							PURCHASE_DETAILS_COLUMN.LEDGER_ID));
					parameters.put(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
				}
			}

			if (filterParamter.containsKey(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString())) {
				String sellId = ApplicationUtility.getStrValue(filterParamter,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString());
				sqlQueryToFetchData.append(String.format(" AND (PD.%s LIKE :%s OR PD.%s LIKE :%s) ",
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.BILL_NUMBER, PURCHASE_DETAILS_COLUMN.PURCHASE_ID));
				parameters.put(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), "%" + sellId + "%");
			}

			if (filterParamter.containsKey(PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS.toString())) {
				List<Integer> paymentStatuses = (List<Integer>) filterParamter
						.get(PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS.toString());
				sqlQueryToFetchData.append(String.format(" AND PD.%s IN (:%s) ", PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS,
						PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS));
				parameters.put(PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS.toString(), paymentStatuses);
			}

		}

		sqlQueryToFetchData
				.append(String.format(" ORDER BY PD.%s DESC LIMIT :%s, :%s ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						APPLICATION_GENERIC_ENUM.START, APPLICATION_GENERIC_ENUM.NO_OF_RECORD));
		parameters.put(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), numberOfRecord);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getPurchaseDetailsListExtractor());
	}

	private ResultSetExtractor<List<PurchaseModel>> getPurchaseDetailsListExtractor() {

		return new ResultSetExtractor<List<PurchaseModel>>() {
			@Override
			public List<PurchaseModel> extractData(ResultSet rs) throws SQLException {
				List<PurchaseModel> purchaseList = new ArrayList<>();
				while (rs.next()) {
					purchaseList.add(new PurchaseModel(rs));
				}
				return purchaseList;
			}
		};
	}

	private StringBuilder getPurchaseDetailsQuery(String columnName) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData
				.append(String.format(" SELECT PD.%s, PD.%s, PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.IMAGE_URL, PURCHASE_DETAILS_COLUMN.BILL_NUMBER,
						PURCHASE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" PD.%s, PD.%s, PD.%s, %s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_DATE,
						PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT, PURCHASE_DETAILS_COLUMN.DISCOUNT,
						PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s, PD.%s,PD.%s, ",
						PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME, PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME, PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED, PURCHASE_DETAILS_COLUMN.BILL_DATE))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s,PD.%s,",
						PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1, PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2,
						PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT, PURCHASE_DETAILS_COLUMN.P_O_NUMBER))
				.append(String.format(" PD.%s, PD.%s, PD.%s, PD.%s, PD.%s, ", PURCHASE_DETAILS_COLUMN.L_R_NUMBER,
						PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER, PURCHASE_DETAILS_COLUMN.BILL_DATE,
						PURCHASE_DETAILS_COLUMN.CITY_NAME, PURCHASE_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" AUM.%s AS %s, AUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.ADDED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.ADDED_BY_NAME))
				.append(String.format(" MUM.%s AS %s, MUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME))
				.append(String.format(" VUM.%s AS %s, VUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME))
				.append(String.format(" DUM.%s AS %s, DUM.%s AS %s, ", USER_TABLE_COLOUMN.USER_ID,
						APPLICATION_GENERIC_ENUM.DELETED_BY_ID, USER_TABLE_COLOUMN.USER_NAME,
						APPLICATION_GENERIC_ENUM.DELETED_BY_NAME))
				.append(String.format(" SM.%s, SM.%s, LM.%s, LM.%s, ", STATUS_TABLE_COLUMN.STATUS_ID,
						STATUS_TABLE_COLUMN.STATUS_NAME, LEDGER_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_NAME))
				.append(String.format(" IM.%s, IM.%s, IM.%s, PIM.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID,
						ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE,
						PURCHASE_ITEM_DATA_COLUMN.QUANTITY))
				.append(String.format(" CSM.%s, CSM.%s, ", STATE_TABLE_COLUMN.STATE_ID, STATE_TABLE_COLUMN.STATE_NAME))
				.append(String.format(" PIM.%s, PIM.%s, PIM.%s, PIM.%s, PIM.%s, ",
						PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ITEM_MAPPING_ID,
						PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE, PURCHASE_ITEM_DATA_COLUMN.LOCTION_ID,
						PURCHASE_ITEM_DATA_COLUMN.BATCH_ID))
				.append(String.format(" PIM.%s, PIM.%s, PIM.%s, PIM.%s, PIM.%s, ", PURCHASE_ITEM_DATA_COLUMN.TAX_RATE,
						PURCHASE_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_QUANTITY, PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_ID))
				.append(String.format(" PIM.%s, PIM.%s, PIM.%s, PIM.%s, ", PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.I_GST, PURCHASE_ITEM_DATA_COLUMN.S_GST,
						PURCHASE_ITEM_DATA_COLUMN.C_GST))
				.append(String.format(" PIM.%s, PIM.%s ",
						PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT))
				.append(String.format(" FROM %s PD ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" INNER JOIN %s SM ON PD.%s = SM.%s ", DATABASE_TABLE.ACC_STATUS_MASTER,
						PURCHASE_DETAILS_COLUMN.STATUS_ID, STATUS_TABLE_COLUMN.STATUS_ID))
				.append(String.format(" INNER JOIN %s LM ON PD.%s = LM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_DETAILS_COLUMN.LEDGER_ID,
						LEDGER_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" AND LM.%s = :%s ", LEDGER_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" INNER JOIN %s AUM ON PD.%s = AUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.ADDED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s CSM ON CSM.%s = LM.%s AND CSM.%s = 0 ",
						DATABASE_TABLE.ACC_STATE_MASTER, STATE_TABLE_COLUMN.STATE_ID, LEDGER_DETAILS_COLUMN.STATE_ID,
						STATE_TABLE_COLUMN.IS_DELETED))
				.append(String.format(" LEFT JOIN %s MUM ON PD.%s = MUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.MODIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s VUM ON PD.%s = VUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.VERIFIED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s DUM ON PD.%s = DUM.%s ", DATABASE_TABLE.ACC_USER_DETAILS,
						PURCHASE_DETAILS_COLUMN.DELETED_BY, USER_TABLE_COLOUMN.USER_ID))
				.append(String.format(" LEFT JOIN %s PIM ON PIM.%s = PD.%s AND PIM.%s = 0 ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_ITEM_DATA, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_ITEM_DATA_COLUMN.IS_DELETED))
				.append(String.format(" AND PIM.%s = :%s ", PURCHASE_ITEM_DATA_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" LEFT JOIN %s IM ON IM.%s = PIM.%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS, ITEM_DETAILS_COLUMN.ITEM_ID,
						PURCHASE_ITEM_DATA_COLUMN.ITEM_ID))
				.append(String.format(" AND IM.%s = :%s ", ITEM_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" WHERE PD.%s = :%s AND PD.%s = 0 ", columnName, columnName,
						PURCHASE_DETAILS_COLUMN.IS_DELETED));
		return sqlQueryToFetchData;

	}

	PurchaseModel getPurchaseDetailsByColumnNameValue(int companyId, String columnName, String columnValue) {
		StringBuilder sqlQueryToFetchData = getPurchaseDetailsQuery(columnName);

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(columnName, columnValue);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getPurchaseDetailsExtractor());
	}

	

	private ResultSetExtractor<PurchaseModel> getPurchaseDetailsExtractor() {

		return new ResultSetExtractor<PurchaseModel>() {
			@Override
			public PurchaseModel extractData(ResultSet rs) throws SQLException {
				PurchaseModel purchaseDetails = new PurchaseModel();
				List<PurchaseItemModel> purchaseItemMappingList = new ArrayList<>();
				while (rs.next()) {
					if (purchaseDetails.getPurchaseId() < 1) {
						purchaseDetails = new PurchaseModel(rs);
					}

					if (rs.getInt(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ITEM_MAPPING_ID.toString()) > 0) {
						purchaseItemMappingList.add(new PurchaseItemModel(rs));
					}
				}
				purchaseDetails.setPurchaseItemModel(purchaseItemMappingList);
				return purchaseDetails;
			}
		};
	}

	PurchaseModel publishPurchaseData(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = getPurchaseInsertQuery(purchaseModel);
		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(purchaseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString(), purchaseModel.getImageURL())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString(), purchaseModel.getBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.DESCRIPTION.toString(), purchaseModel.getDescription())
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString(), purchaseModel.getPurchaseDate())
				.addValue(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), purchaseModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), purchaseModel.getTotalBillAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), purchaseModel.getGrosssTotalAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.DISCOUNT.toString(), purchaseModel.getDiscount())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), purchaseModel.getReferenceNumber1())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), purchaseModel.getReferenceNumber2())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT.toString(),
						purchaseModel.isPurchaseMeasureDiscountInAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_DATE.toString(), purchaseModel.getBillDate())
				.addValue(PURCHASE_DETAILS_COLUMN.P_O_NUMBER.toString(), purchaseModel.getPoNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString(), purchaseModel.getLrNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), purchaseModel.getEwayBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.CITY_NAME.toString(), purchaseModel.getCityName())
				.addValue(PURCHASE_DETAILS_COLUMN.GST_NUMBER.toString(), purchaseModel.getGstNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.ADDED_BY.toString(), purchaseModel.getAddedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		purchaseModel.setPurchaseId(holder.getKey().intValue());
		return purchaseModel;
	}

	PurchaseModel savePurchaseData(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = getPurchaseInsertQuery(purchaseModel);

		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(purchaseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString(), purchaseModel.getImageURL())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString(), purchaseModel.getBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.DESCRIPTION.toString(), purchaseModel.getDescription())
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString(), purchaseModel.getPurchaseDate())
				.addValue(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), purchaseModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), purchaseModel.getTotalBillAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), purchaseModel.getGrosssTotalAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.DISCOUNT.toString(), purchaseModel.getDiscount())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), purchaseModel.getReferenceNumber1())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), purchaseModel.getReferenceNumber2())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT.toString(),
						purchaseModel.isPurchaseMeasureDiscountInAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.ADDED_BY.toString(), purchaseModel.getAddedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_DATE.toString(), purchaseModel.getBillDate())
				.addValue(PURCHASE_DETAILS_COLUMN.P_O_NUMBER.toString(), purchaseModel.getPoNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString(), purchaseModel.getLrNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), purchaseModel.getEwayBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.CITY_NAME.toString(), purchaseModel.getCityName())
				.addValue(PURCHASE_DETAILS_COLUMN.GST_NUMBER.toString(), purchaseModel.getGstNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters, holder);
		purchaseModel.setPurchaseId(holder.getKey().intValue());
		return purchaseModel;
	}

	private StringBuilder getPurchaseInsertQuery(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("INSERT INTO %s ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format("( %s, %s, %s, %s, ", PURCHASE_DETAILS_COLUMN.IMAGE_URL,
						PURCHASE_DETAILS_COLUMN.BILL_NUMBER, PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, ", PURCHASE_DETAILS_COLUMN.DESCRIPTION,
						PURCHASE_DETAILS_COLUMN.STATUS_ID, PURCHASE_DETAILS_COLUMN.PURCHASE_DATE))
				.append(String.format(" %s, %s, %s, ", PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT,
						PURCHASE_DETAILS_COLUMN.DISCOUNT, PURCHASE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s, %s, %s, ", PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2, PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" %s, %s, %s,", PURCHASE_DETAILS_COLUMN.ADDED_BY,
						PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME, PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s, %s, %s,%s,", PURCHASE_DETAILS_COLUMN.BILL_DATE,
						PURCHASE_DETAILS_COLUMN.P_O_NUMBER, PURCHASE_DETAILS_COLUMN.L_R_NUMBER,
						PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String
						.format(" %s, %s)", PURCHASE_DETAILS_COLUMN.CITY_NAME, PURCHASE_DETAILS_COLUMN.GST_NUMBER))
				.append(" VALUES ")
				.append(String.format("( :%s, :%s, :%s, :%s, ", PURCHASE_DETAILS_COLUMN.IMAGE_URL,
						PURCHASE_DETAILS_COLUMN.BILL_NUMBER, PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, ", PURCHASE_DETAILS_COLUMN.DESCRIPTION,
						PURCHASE_DETAILS_COLUMN.STATUS_ID, PURCHASE_DETAILS_COLUMN.PURCHASE_DATE))
				.append(String.format(" :%s, :%s, :%s, ", PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT,
						PURCHASE_DETAILS_COLUMN.DISCOUNT, PURCHASE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" :%s, :%s, :%s, ", PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2, PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" :%s, :%s, :%s,", PURCHASE_DETAILS_COLUMN.ADDED_BY,
						PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME, PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" :%s, :%s, :%s, :%s,", PURCHASE_DETAILS_COLUMN.BILL_DATE,
						PURCHASE_DETAILS_COLUMN.P_O_NUMBER, PURCHASE_DETAILS_COLUMN.L_R_NUMBER,
						PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" :%s, :%s)", PURCHASE_DETAILS_COLUMN.CITY_NAME,
						PURCHASE_DETAILS_COLUMN.GST_NUMBER));
		return sqlQuery;
	}

	private StringBuilder getPurchaseUpdateQuery(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IMAGE_URL,
						PURCHASE_DETAILS_COLUMN.IMAGE_URL))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.BILL_NUMBER,
						PURCHASE_DETAILS_COLUMN.BILL_NUMBER))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT,
						PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.DESCRIPTION,
						PURCHASE_DETAILS_COLUMN.DESCRIPTION))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.STATUS_ID,
						PURCHASE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.PURCHASE_DATE,
						PURCHASE_DETAILS_COLUMN.PURCHASE_DATE))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.LEDGER_ID,
						PURCHASE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT,
						PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT,
						PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2,
						PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.DISCOUNT,
						PURCHASE_DETAILS_COLUMN.DISCOUNT))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.L_R_NUMBER,
						PURCHASE_DETAILS_COLUMN.L_R_NUMBER))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER,
						PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.CITY_NAME,
						PURCHASE_DETAILS_COLUMN.CITY_NAME))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.GST_NUMBER,
						PURCHASE_DETAILS_COLUMN.GST_NUMBER))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.MODIFIED_BY,
						PURCHASE_DETAILS_COLUMN.MODIFIED_BY))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.IS_DELETED,
						PURCHASE_DETAILS_COLUMN.IS_DELETED));
		return sqlQuery;
	}

	PurchaseModel updateSavedPurchaseData(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = getPurchaseUpdateQuery(purchaseModel);

		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(purchaseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString(), purchaseModel.getImageURL())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString(), purchaseModel.getBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.DESCRIPTION.toString(), purchaseModel.getDescription())
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString(), purchaseModel.getPurchaseDate())
				.addValue(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), purchaseModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT.toString(),
						purchaseModel.isPurchaseMeasureDiscountInAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), purchaseModel.getTotalBillAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), purchaseModel.getGrosssTotalAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.DISCOUNT.toString(), purchaseModel.getDiscount())
				.addValue(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString(), purchaseModel.getLrNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), purchaseModel.getEwayBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.CITY_NAME.toString(), purchaseModel.getCityName())
				.addValue(PURCHASE_DETAILS_COLUMN.GST_NUMBER.toString(), purchaseModel.getGstNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), purchaseModel.getReferenceNumber1())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), purchaseModel.getReferenceNumber2())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.MODIFIED_BY.toString(), purchaseModel.getModifiedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseModel.getPurchaseId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		purchaseModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);

		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(ApplicationUtility.getServerURL() + purchaseModel.getImageURL());
		}
		return purchaseModel;
	}

	PurchaseModel updatePublishedPurchaseData(PurchaseModel purchaseModel) {
		StringBuilder sqlQuery = getPurchaseUpdateQuery(purchaseModel);

		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(purchaseModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString(), purchaseModel.getImageURL())
				.addValue(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString(), purchaseModel.getBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.DESCRIPTION.toString(), purchaseModel.getDescription())
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.MODIFIED.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString(), purchaseModel.getPurchaseDate())
				.addValue(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), purchaseModel.getLedgerData().getLedgerId())
				.addValue(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString(), purchaseModel.getTotalBillAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString(), purchaseModel.getGrosssTotalAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.DISCOUNT.toString(), purchaseModel.getDiscount())
				.addValue(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString(), purchaseModel.getLrNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString(), purchaseModel.getEwayBillNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.CITY_NAME.toString(), purchaseModel.getCityName())
				.addValue(PURCHASE_DETAILS_COLUMN.GST_NUMBER.toString(), purchaseModel.getGstNumber())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT.toString(),
						purchaseModel.isPurchaseMeasureDiscountInAmount())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString(), purchaseModel.getReferenceNumber1())
				.addValue(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString(), purchaseModel.getReferenceNumber2())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.MODIFIED_BY.toString(), purchaseModel.getModifiedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseModel.getPurchaseId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		purchaseModel.setModifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);

		if (!ApplicationUtility.isNullEmpty(purchaseModel.getImageURL())) {
			purchaseModel.setImageURL(ApplicationUtility.getServerURL() + purchaseModel.getImageURL());
		}

		return purchaseModel;
	}

	PurchaseModel markPurchaseAsVerified(PurchaseModel purchaseModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.VERIFIED_BY,
						PURCHASE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.IS_DELETED,
						PURCHASE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(PURCHASE_DETAILS_COLUMN.VERIFIED_BY.toString(), purchaseModel.getVerifiedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseModel.getPurchaseId())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		purchaseModel.setVerified(true);
		purchaseModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		return purchaseModel;
	}

	PurchaseModel markPurchaseAsDeleteAndVerified(PurchaseModel purchaseModel) {

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.VERIFIED_BY,
						PURCHASE_DETAILS_COLUMN.VERIFIED_BY))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.IS_DELETED,
						PURCHASE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = :%s  ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 1)
				.addValue(PURCHASE_DETAILS_COLUMN.VERIFIED_BY.toString(), purchaseModel.getVerifiedBy().getUserId())
				.addValue(PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseModel.getPurchaseId())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 1);

		purchaseModel.setVerified(true);
		purchaseModel.setVerifiedDateTime(DateUtility.getCurrentDateTime());
		namedParameterJdbcTemplate.update(sqlQuery.toString(), parameters);
		return purchaseModel;
	}

	int deletePublishPurchaseData(int companyId, int purchaseId, int deletedBy) {

		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate
				.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.STATUS_ID,
						PURCHASE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.DELETED_BY,
						PURCHASE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED,
						PURCHASE_DETAILS_COLUMN.IS_VERIFIED))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseId)
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString(), 0)
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int deleteSavedPurchaseData(int companyId, int purchaseId, int deletedBy) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate
				.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.IS_DELETED,
						PURCHASE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" %s = :%s, ", PURCHASE_DETAILS_COLUMN.DELETED_BY,
						PURCHASE_DETAILS_COLUMN.DELETED_BY))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME,
						PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME))
				.append(String.format(" WHERE %s = :%s  ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseId)
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(PURCHASE_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime())
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}

	int getPendingPurchaseCountData(int companyId, int isVerified) {
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append(String.format("SELECT COUNT(%s) AS pendingCount FROM %s ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
				COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format("WHERE %s != :%s AND %s = 0 ", PURCHASE_DETAILS_COLUMN.STATUS_ID,
						PURCHASE_DETAILS_COLUMN.STATUS_ID, PURCHASE_DETAILS_COLUMN.IS_DELETED))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		if (isVerified == 1 || isVerified == 0) {
			sqlQuery.append(String.format(" AND %s = %s ", PURCHASE_DETAILS_COLUMN.IS_VERIFIED, isVerified));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.UNPUBLISHED.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters, getPurchasePendingCountExctractor());
	}

	private ResultSetExtractor<Integer> getPurchasePendingCountExctractor() {
		return new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException {
				int pendingCount = 0;
				while (rs.next()) {
					pendingCount = rs.getInt("pendingCount");
				}
				return pendingCount;
			}
		};
	}

	// get ledgerWise PurchaseList 13-05-2019 Start

//	List<PurchaseModel> getledgerPurchaseList(int ledgerID){
//		StringBuilder sqlQueryToFetchData = new StringBuilder();
//					  sqlQueryToFetchData.append(String.format("select %s,%s,%s ",PURCHASE_DETAILS_COLUMN.PURCHASE_ID,PURCHASE_DETAILS_COLUMN.PURCHASE_DATE,PURCHASE_DETAILS_COLUMN.BILL_NUMBER))
//					  					 .append(String.format("from %s where %s=:%s ",COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS,PURCHASE_DETAILS_COLUMN.LEDGER_ID,PURCHASE_DETAILS_COLUMN.LEDGER_ID));
//					  Map<String, Object> parameters = new HashMap<>();
//					  parameters.put(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerID);
//					  return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters, getLegderPurchseDetailsExtractor());
//	}
//	
//	private ResultSetExtractor<List<PurchaseModel>> getLegderPurchseDetailsExtractor() {
//
//		return new ResultSetExtractor<List<PurchaseModel>>() {
//			@Override
//			public List<PurchaseModel> extractData(ResultSet rs) throws SQLException {
//				List<PurchaseModel> ledgerPurchaseList = new ArrayList<PurchaseModel>();
//				while (rs.next()) {
//					PurchaseModel purchaseModel = new PurchaseModel();
//					purchaseModel.setPurchaseId(rs.getInt(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString()));
//					purchaseModel.setPurchaseDate(rs.getString(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString()));
//					purchaseModel.setBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString()));
//					ledgerPurchaseList.add(purchaseModel);
//				}
//				return ledgerPurchaseList;
//			}
//		};
//	}
	// Second
	List<LedgerPurchaseDetailsModel> getledgerPurchaseList(int ledgerID) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		sqlQueryToFetchData
				.append(String.format(
						"select pd.%s, pd.%s, pd.%s, pid.%s, item.%s, pid.%s, batch.%s, batch.%s, batch.%s ",
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID, PURCHASE_DETAILS_COLUMN.PURCHASE_DATE,
						PURCHASE_DETAILS_COLUMN.BILL_NUMBER, LEDGER_PURCHASE_DETAILS.ITEM_ID,
						LEDGER_PURCHASE_DETAILS.ITEM_NAME, PURCHASE_ITEM_DATA_COLUMN.QUANTITY,
						ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE, ITEM_BATCH_COLUMN.ITEM_EXP_DATE))
				.append(String.format("from %s AS pd , %s AS pid, %s AS item,  %s AS batch  where pd.%s=:%s ",
						COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS,
						COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_ITEM_DATA,
						COMPANY_RELATED_TABLE.ACC_COMPANY_ITEM_DETAILS, DATABASE_TABLE.ACC_ITEM_BATCH_DETAILS,
						PURCHASE_DETAILS_COLUMN.LEDGER_ID, PURCHASE_DETAILS_COLUMN.LEDGER_ID))
				.append(String.format(
						"AND pd.purchaseId IN ( SELECT pid.purchaseId FROM acc_company_purchase_item_data AS pid) AND pid.itemId IN( SELECT item.itemId FROM acc_company_items_details AS item) AND item.itemId IN (SELECT batch.itemId FROM acc_item_batch_details AS batch)  AND pd.purchaseId = pid.purchaseId AND item.itemId = pid.itemId AND item.itemId = batch.itemId AND batch.isDeleted = 0 GROUP BY pd.purchaseId"));
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerID);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getLegderPurchseDetailsExtractor());
	}

	private ResultSetExtractor<List<LedgerPurchaseDetailsModel>> getLegderPurchseDetailsExtractor() {

		return new ResultSetExtractor<List<LedgerPurchaseDetailsModel>>() {
			@Override
			public List<LedgerPurchaseDetailsModel> extractData(ResultSet rs) throws SQLException {
				List<LedgerPurchaseDetailsModel> ledgerPurchaseList = new ArrayList<LedgerPurchaseDetailsModel>();
				while (rs.next()) {
//					PurchaseModel purchaseModel = new PurchaseModel();
//					purchaseModel.setPurchaseId(rs.getInt(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString()));
//					purchaseModel.setPurchaseDate(rs.getString(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString()));
//					purchaseModel.setBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString()));
//					ledgerPurchaseList.add(purchaseModel);

					ledgerPurchaseList.add(new LedgerPurchaseDetailsModel(rs));
				}
				return ledgerPurchaseList;
			}
		};
	}
	// end

	List<ItemPurchaseRateModel> getItemMrpByItemIdAndDateList(int itemId, String date) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT MRP.%s, MRP.%s, MRP.%s ", ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE,
				ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE, ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE))
				.append(String.format(" FROM %s AS MRP ", DATABASE_TABLE.ACC_ITEM_PURCHASE_RATE_DETAILS))
				.append(String.format(" WHERE MRP.itemId = " + itemId
						+ "  AND MRP.isDeleted = 0  AND  MRP.wefPurchaseRateDate >= '" + date
						+ "'  AND MRP.itemPurchaseRateId IN (SELECT itemPurchaseRateId  FROM  acc_item_purchase_rate_details  WHERE wetPurachaseRateDate <= '"
						+ date + "') "));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ITEM_PURCHASE_RATE_COLUMN.ITEM_ID.toString(), itemId);
		paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString(), date);
		paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString(), date);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap,
				getItemMrpByDateAndItemIdResultExctractor());
	}

	private ResultSetExtractor<List<ItemPurchaseRateModel>> getItemMrpByDateAndItemIdResultExctractor() {
		return new ResultSetExtractor<List<ItemPurchaseRateModel>>() {
			@Override
			public List<ItemPurchaseRateModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ItemPurchaseRateModel> itemMrpModelList = new ArrayList<>();
				ItemPurchaseRateModel itemMrpModel;
				while (rs.next()) {
					itemMrpModel = new ItemPurchaseRateModel();
					// itemMrpModel.setItemMrpId(rs.getInt(ITEM_MRP_COLUMN.MRP_ID.toString()));
					itemMrpModel.setItemPurchaseRate(rs.getDouble(ITEM_PURCHASE_RATE_COLUMN.PURCHASE_RATE.toString()));
					itemMrpModel.setWetPurachaseRateDate(
							rs.getString(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString()));
					itemMrpModel.setWetPurachaseRateDate(
							rs.getString(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString()));
					itemMrpModelList.add(itemMrpModel);
				}
				return itemMrpModelList;
			}
		};
	}

	// get ledgerWise PurchaseList 13-05-2019 End

	PurchaseModel publishSavedPurchase(PurchaseModel purchaseModel) {
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate
				.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.STATUS_ID,
						PURCHASE_DETAILS_COLUMN.STATUS_ID))
				.append(String.format(" WHERE %s = :%s ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID))
				.append(String.format(" AND %s = :%s ", PURCHASE_DETAILS_COLUMN.IS_DELETED,
						PURCHASE_DETAILS_COLUMN.IS_DELETED));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), purchaseModel.getPurchaseId())
				.addValue(PURCHASE_DETAILS_COLUMN.STATUS_ID.toString(),
						statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()))
				.addValue(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), purchaseModel.getCompanyId())
				.addValue(PURCHASE_DETAILS_COLUMN.IS_DELETED.toString(), 0);

		namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);

		StatusModel status = new StatusModel();
		status.setStatusId(statusUtil.getStatusIdByName(STATUS_TEXT.NEW.toString()));
		status.setStatusName(STATUS_TEXT.NEW.toString());
		purchaseModel.setStatus(status);
		return purchaseModel;
	}

	// ,PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT
	List<DebitNoteGetItem> getItemDataListForDebitNote(String itemName, String arr) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format("SELECT ci.%s, ci.%s, ci.%s, ci.%s, ", ITEM_DETAILS_COLUMN.ITEM_ID,
				ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.HSN_CODE, ITEM_DETAILS_COLUMN.TAX_CODE))
				.append(String.format(" ci.%s, ci.%s, ci.%s, ", ITEM_DETAILS_COLUMN.HSN_CODE,
						ITEM_DETAILS_COLUMN.SELLING_RATE, ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL))
				.append(String.format(" cpi.%s,cpi.%s as %s,"
						+ "cpi.%s,cpi.%s,cpi.%s ,cpi.%s,cpi.%s,cpi.%s,cpi.%s,cpi.%s,location.%s,location.%s,batch.%s,batch.%s,batch.%s,batch.%s ",
						PURCHASE_ITEM_DATA_COLUMN.QUANTITY, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE,
						ITEM_DETAILS_COLUMN.PURCHASE_RATE, PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT,
						PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,
						PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT,
						PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.I_GST,
						PURCHASE_ITEM_DATA_COLUMN.C_GST, PURCHASE_ITEM_DATA_COLUMN.S_GST,
						LOCATION_DETAILS_COLUMN.LOCATION_ID, LOCATION_DETAILS_COLUMN.LOCATION_NAME,
						ITEM_BATCH_COLUMN.BATCH_ID, ITEM_BATCH_COLUMN.BATCH_NO, ITEM_BATCH_COLUMN.ITEM_MFG_DATE,
						ITEM_BATCH_COLUMN.ITEM_EXP_DATE

				))

				.append(" FROM acc_company_purchase_item_data AS cpi ")
				.append("INNER JOIN acc_company_purchase_details AS cpd  ON cpd.purchaseId = cpi.purchaseId ")
				.append(" INNER JOIN acc_company_items_details AS ci ON ci.itemId  = cpi.itemId  ")
				.append(" left JOIN acc_location_details AS location ON location.locationId = cpi.locationId ")
				.append(" left JOIN acc_item_batch_details AS batch ON batch.batchId = cpi.batchId ")
				.append("WHERE cpi.purchaseId IN (" + arr + ") ")
				.append(String.format(
						" AND ( %s LIKE :%s OR  %s LIKE :%s OR %s LIKE :%s) and cpi.isDeleted=0 GROUP BY ci.itemId ORDER BY cpd.purchaseDate  ASC",
						ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_NAME, ITEM_DETAILS_COLUMN.ITEM_SKU,
						ITEM_DETAILS_COLUMN.ITEM_SKU, ITEM_DETAILS_COLUMN.ITEM_EAN_NO,
						ITEM_DETAILS_COLUMN.ITEM_EAN_NO));// GROUP BY ci.itemId ORDER BY cpd.purchaseDate ASC");

		Map<String, Object> paramMap = new HashMap<>();
		// paramMap.put(ITEM_PURCHASE_RATE_COLUMN.ITEM_ID.toString(), itemId);
		// paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString(),
		// date);
		// paramMap.put(ITEM_PURCHASE_RATE_COLUMN.WEF_PURCHASE_RATE_DATE.toString(),
		// date);
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_NAME.toString(), "%" + itemName + "%");
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_SKU.toString(), "%" + itemName + "%");
		paramMap.put(ITEM_DETAILS_COLUMN.ITEM_EAN_NO.toString(), "%" + itemName + "%");

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getItemResultExctractor());
	}

	private ResultSetExtractor<List<DebitNoteGetItem>> getItemResultExctractor() {
		return new ResultSetExtractor<List<DebitNoteGetItem>>() {
			@Override
			public List<DebitNoteGetItem> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<DebitNoteGetItem> debitNoteGetItemList = new ArrayList<>();
				DebitNoteGetItem debitNoteGetItem;
				while (rs.next()) {
					debitNoteGetItem = new DebitNoteGetItem();
					debitNoteGetItem.setItemName(rs.getString(DEBITNOTE_ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					debitNoteGetItem.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
					// debitNoteGetItem.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
					debitNoteGetItem.setSellRate(rs.getDouble(ITEM_DETAILS_COLUMN.SELLING_RATE.toString()));
					debitNoteGetItem.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
					debitNoteGetItem.setTaxCode(rs.getDouble(ITEM_DETAILS_COLUMN.TAX_CODE.toString()));
					debitNoteGetItem.setQuantity(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.QUANTITY.toString()));
					debitNoteGetItem.setPurchaseRate(rs.getDouble(ITEM_DETAILS_COLUMN.PURCHASE_RATE.toString()));
					debitNoteGetItem.setProfilePhotoURL(ApplicationUtility.getServerURL()
							+ rs.getString(ITEM_DETAILS_COLUMN.PROFILE_PHOTO_URL.toString()));

					debitNoteGetItem.setItemAmount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString()));
					debitNoteGetItem.setDiscount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString()));
					debitNoteGetItem.setMeasureDiscountInAmount(
							rs.getBoolean(PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));
					debitNoteGetItem.setAdditionaldiscount(
							rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
					debitNoteGetItem.setAddmeasureDiscountInAmount(
							rs.getBoolean(PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
					debitNoteGetItem.setIgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.I_GST.toString()));
					debitNoteGetItem.setSgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.S_GST.toString()));
					debitNoteGetItem.setCgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.C_GST.toString()));
					debitNoteGetItem.setLocationId(rs.getInt(LOCATION_DETAILS_COLUMN.LOCATION_ID.toString()));
					debitNoteGetItem.setLocationName(rs.getString(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString()));
					debitNoteGetItem.setBatchId(rs.getInt(ITEM_BATCH_COLUMN.BATCH_ID.toString()));
					debitNoteGetItem.setBatchNo(rs.getString(ITEM_BATCH_COLUMN.BATCH_NO.toString()));
					debitNoteGetItem.setMfgDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_MFG_DATE.toString()));
					debitNoteGetItem.setExpDate(rs.getString(ITEM_BATCH_COLUMN.ITEM_EXP_DATE.toString()));
					debitNoteGetItemList.add(debitNoteGetItem);
				}
				return debitNoteGetItemList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	public int[] updatePurchasePaymentStatus(List<Map<String, Object>> purchasePaymentStatusList, int companyId) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(String.format(" UPDATE %s SET ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_DETAILS))
				.append(String.format(" %s = :%s ", PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS,
						PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS))
				.append(String.format(" WHERE %s = :%s ", PURCHASE_DETAILS_COLUMN.PURCHASE_ID,
						PURCHASE_DETAILS_COLUMN.PURCHASE_ID))
				.append(String.format(" AND %s = :%s  ", PURCHASE_DETAILS_COLUMN.COMPANY_ID,
						PURCHASE_DETAILS_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(),
				purchasePaymentStatusList.toArray(new HashMap[0]));
	}

	PurchaseModel getPurchaseDetailsForGivenLedgerBill(int companyId, String columnName, String columnValue,
			int ledgerId) {
		StringBuilder sqlQueryToFetchData = getPurchaseDetailsQuery(columnName);

		sqlQueryToFetchData.append(String.format(" AND PD.%s = :%s ", PURCHASE_DETAILS_COLUMN.LEDGER_ID,
				PURCHASE_DETAILS_COLUMN.LEDGER_ID));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PURCHASE_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
		parameters.put(columnName, columnValue);
		parameters.put(PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString(), ledgerId);
		return namedParameterJdbcTemplate.query(sqlQueryToFetchData.toString(), parameters,
				getPurchaseDetailsExtractor());
	}

}
