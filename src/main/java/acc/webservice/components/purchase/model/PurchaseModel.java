package acc.webservice.components.purchase.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.purchase.PurchaseChargeModel;
import acc.webservice.components.purchase.PurchaseImageModel;
import acc.webservice.components.purchase.PurchaseItemModel;
import acc.webservice.components.state.StateModel;
import acc.webservice.components.status.StatusModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

public class PurchaseModel {

	private int purchaseId;
	private String imageURL;
	private String billNumber;
	private String description;
	private StatusModel status;
	private String purchaseDate;
	private LedgerModel ledgerData;
	private double totalBillAmount;
	private double grosssTotalAmount;
	private UserModel addedBy;
	private String addedDateTime;
	private UserModel modifiedBy;
	private String modifiedDateTime;
	private UserModel verifiedBy;
	private String verifiedDateTime;
	private UserModel deletedBy;
	private String deletedDateTime;
	private double discount;
	private byte paymentStatus;
	private boolean deleted;
	private boolean verified;
	private int companyId;
	private boolean purchaseMeasureDiscountInAmount;
	private List<PurchaseItemModel> purchaseItemModel;
	private String referenceNumber1;
	private String referenceNumber2;
	private Date billDate;
	private String poNumber;
	private String lrNumber;
	private String ewayBillNumber;
	private String cityName;
	private String gstNumber;
	private List<PurchaseImageModel> imageURLList;
	private List<PurchaseChargeModel> purchaseChargeList;

	public PurchaseModel() {
	}

	public PurchaseModel(ResultSet rs) throws SQLException {
		setPurchaseId(rs.getInt(PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString()));

		String imageURL = rs.getString(PURCHASE_DETAILS_COLUMN.IMAGE_URL.toString());
		if (!ApplicationUtility.isNullEmpty(imageURL)) {
			setImageURL(ApplicationUtility.getServerURL() + imageURL);
		}

	
		
		setReferenceNumber1(rs.getString(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_1.toString()));
		setReferenceNumber2(rs.getString(PURCHASE_DETAILS_COLUMN.REFERENCE_NUBER_2.toString()));
		setBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString()));
		setDescription(rs.getString(PURCHASE_DETAILS_COLUMN.DESCRIPTION.toString()));
		setTotalBillAmount(rs.getDouble(PURCHASE_DETAILS_COLUMN.TOTAL_BILL_AMOUNT.toString()));
		setGrosssTotalAmount(rs.getDouble(PURCHASE_DETAILS_COLUMN.GROSS_TOTAL_AMOUNT.toString()));
		setPaymentStatus(rs.getByte(PURCHASE_DETAILS_COLUMN.PAYMENT_STATUS.toString()));
		setDiscount(rs.getDouble(PURCHASE_DETAILS_COLUMN.DISCOUNT.toString()));
		setVerified(rs.getBoolean(PURCHASE_DETAILS_COLUMN.IS_VERIFIED.toString()));
		
		setCityName(rs.getString(PURCHASE_DETAILS_COLUMN.CITY_NAME.toString()));
		setGstNumber(rs.getString(PURCHASE_DETAILS_COLUMN.GST_NUMBER.toString()));
		setPoNumber(rs.getString(PURCHASE_DETAILS_COLUMN.P_O_NUMBER.toString()));
		setLrNumber(rs.getString(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString()));
		setEwayBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString()));
		
			setBillDate(rs.getDate(PURCHASE_DETAILS_COLUMN.BILL_DATE.toString()));
		
	
		//setLrNumber(rs.getString(PURCHASE_DETAILS_COLUMN.L_R_NUMBER.toString()));
	  //	setEwayBillNumber(rs.getString(PURCHASE_DETAILS_COLUMN.E_WAY_BILL_NUMBER.toString()));
		
		setPurchaseMeasureDiscountInAmount(
				rs.getBoolean(PURCHASE_DETAILS_COLUMN.PURCHASE_MEASURE_DISCOUNT_IN_AMONT.toString()));

		Timestamp fetchedPurchaseDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.PURCHASE_DATE.toString());
		if (fetchedPurchaseDate != null) {
			setPurchaseDate(DateUtility.converDateToUserString(fetchedPurchaseDate));
		}

		Timestamp fetchedAddedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.ADDED_DATE_TIME.toString());
		setAddedDateTime(DateUtility.converDateToUserString(fetchedAddedDate));

		Timestamp fetchedModifiedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.MODIFIED_DATE_TIME.toString());
		if (fetchedModifiedDate != null) {
			setModifiedDateTime(DateUtility.converDateToUserString(fetchedModifiedDate));
		}

		Timestamp fetchedVerfiedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.VERIFIED_DATE_TIME.toString());
		if (fetchedVerfiedDate != null) {
			setVerifiedDateTime(DateUtility.converDateToUserString(fetchedVerfiedDate));
		}

		Timestamp fetchedDeletedDate = rs.getTimestamp(PURCHASE_DETAILS_COLUMN.DELETED_DATE_TIME.toString());
		if (fetchedDeletedDate != null) {
			setDeletedDateTime(DateUtility.converDateToUserString(fetchedDeletedDate));
		}

		LedgerModel fetchedLedgerData = new LedgerModel();
		fetchedLedgerData.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedgerData.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedgerData.setState(new StateModel(rs));
		setLedgerData(fetchedLedgerData);

		UserModel fetchedAddedBy = new UserModel();
		fetchedAddedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.ADDED_BY_ID.toString()));
		fetchedAddedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.ADDED_BY_NAME.toString()));
		setAddedBy(fetchedAddedBy);

		UserModel fetchedModifiedBy = new UserModel();
		fetchedModifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.MODIFIED_BY_ID.toString()));
		fetchedModifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.MODIFIED_BY_NAME.toString()));
		setModifiedBy(fetchedModifiedBy);

		UserModel fetchedVerifiedBy = new UserModel();
		fetchedVerifiedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.VERIFIED_BY_ID.toString()));
		fetchedVerifiedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.VERIFIED_BY_NAME.toString()));
		setVerifiedBy(fetchedVerifiedBy);

		UserModel fetchedDeletedBy = new UserModel();
		fetchedDeletedBy.setUserId(rs.getInt(APPLICATION_GENERIC_ENUM.DELETED_BY_ID.toString()));
		fetchedDeletedBy.setUserName(rs.getString(APPLICATION_GENERIC_ENUM.DELETED_BY_NAME.toString()));
		setDeletedBy(fetchedDeletedBy);

		setStatus(new StatusModel(rs));
	}

	public int getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}


	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusModel getStatus() {
		return status;
	}

	public void setStatus(StatusModel status) {
		this.status = status;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public LedgerModel getLedgerData() {
		return ledgerData;
	}

	public void setLedgerData(LedgerModel ledgerData) {
		this.ledgerData = ledgerData;
	}

	public double getTotalBillAmount() {
		return totalBillAmount;
	}

	public void setTotalBillAmount(double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}

	public UserModel getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(UserModel addedBy) {
		this.addedBy = addedBy;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public UserModel getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserModel modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateTime() {
		return modifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public UserModel getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(UserModel verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	public byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public UserModel getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(UserModel deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDeletedDateTime() {
		return deletedDateTime;
	}

	public void setDeletedDateTime(String deletedDateTime) {
		this.deletedDateTime = deletedDateTime;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public List<PurchaseItemModel> getPurchaseItemModel() {
		return purchaseItemModel;
	}

	public void setPurchaseItemModel(List<PurchaseItemModel> purchaseItemModel) {
		this.purchaseItemModel = purchaseItemModel;
	}

	public boolean isPurchaseMeasureDiscountInAmount() {
		return purchaseMeasureDiscountInAmount;
	}

	public void setPurchaseMeasureDiscountInAmount(boolean purchaseMeasureDiscountInAmount) {
		this.purchaseMeasureDiscountInAmount = purchaseMeasureDiscountInAmount;
	}

	public List<PurchaseImageModel> getImageURLList() {
		return imageURLList;
	}

	public void setImageURLList(List<PurchaseImageModel> imageURLList) {
		this.imageURLList = imageURLList;
	}

	public List<PurchaseChargeModel> getPurchaseChargeList() {
		return purchaseChargeList;
	}

	public void setPurchaseChargeList(List<PurchaseChargeModel> purchaseChargeList) {
		this.purchaseChargeList = purchaseChargeList;
	}

	public String getReferenceNumber1() {
		return referenceNumber1;
	}

	public void setReferenceNumber1(String referenceNumber1) {
		this.referenceNumber1 = referenceNumber1;
	}

	public String getReferenceNumber2() {
		return referenceNumber2;
	}

	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}

	public double getGrosssTotalAmount() {
		return grosssTotalAmount;
	}

	public void setGrosssTotalAmount(double grosssTotalAmount) {
		this.grosssTotalAmount = grosssTotalAmount;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getLrNumber() {
		return lrNumber;
	}

	public void setLrNumber(String lrNumber) {
		this.lrNumber = lrNumber;
	}

	public String getEwayBillNumber() {
		return ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}
	
	

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	@Override
	public String toString() {
		return "PurchaseModel [purchaseId=" + purchaseId + ", imageURL=" + imageURL + ", billNumber=" + billNumber
				+ ", description=" + description + ", status=" + status + ", purchaseDate=" + purchaseDate
				+ ", ledgerData=" + ledgerData + ", totalBillAmount=" + totalBillAmount + ", addedDateTime="
				+ addedDateTime + ", modifiedDateTime=" + modifiedDateTime + ", verifiedDateTime=" + verifiedDateTime
				+ ", paymentStatus=" + paymentStatus + ", discount=" + discount + ", deleted=" + deleted
				+ ", deletedDateTime=" + deletedDateTime + "]";
	}

}
