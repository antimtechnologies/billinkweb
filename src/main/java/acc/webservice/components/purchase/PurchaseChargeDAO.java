package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_CHARGE_DATA_COLUMN;

@Lazy
@Repository
public class PurchaseChargeDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] savePurchaseChargeMappingData(List<PurchaseChargeModel> purchaseChargeModel, int companyId, int purchaseId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_CHARGE_DATA))
				.append(String.format(" ( %s, %s, %s, %s, ", PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID, PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE, PURCHASE_CHARGE_DATA_COLUMN.LEDGER_ID, PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", PURCHASE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_CHARGE_DATA_COLUMN.TAX_RATE, PURCHASE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, PURCHASE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" %s, %s, %s, %s ) ", PURCHASE_CHARGE_DATA_COLUMN.I_GST, PURCHASE_CHARGE_DATA_COLUMN.S_GST, PURCHASE_CHARGE_DATA_COLUMN.C_GST, PURCHASE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s, ", PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID, PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE, PURCHASE_CHARGE_DATA_COLUMN.LEDGER_ID, PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", PURCHASE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_CHARGE_DATA_COLUMN.TAX_RATE, PURCHASE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT, PURCHASE_CHARGE_DATA_COLUMN.ITEM_AMOUNT))
				.append(String.format(" :%s, :%s, :%s, :%s ) ", PURCHASE_CHARGE_DATA_COLUMN.I_GST, PURCHASE_CHARGE_DATA_COLUMN.S_GST, PURCHASE_CHARGE_DATA_COLUMN.C_GST, PURCHASE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> purchaseFrightMappingList = new ArrayList<>();
		Map<String, Object> purchaseFrightMap;
		for (PurchaseChargeModel purchaseCharge : purchaseChargeModel) {

			if (purchaseCharge.getLedger() == null || purchaseCharge.getLedger().getLedgerId() <= 0) {
				continue;
			}

			purchaseFrightMap = new HashMap<>();
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE.toString(), purchaseCharge.getPurchaseCharge());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.LEDGER_ID.toString(), purchaseCharge.getLedger().getLedgerId());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID.toString(), purchaseId);
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString(), purchaseCharge.getDiscount());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.TAX_RATE.toString(), purchaseCharge.getTaxRate());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), purchaseCharge.getTotalItemAmount());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString(), purchaseCharge.getItemAmount());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.I_GST.toString(), purchaseCharge.getIgst());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.S_GST.toString(), purchaseCharge.getSgst());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.C_GST.toString(), purchaseCharge.getCgst());
			purchaseFrightMap.put(PURCHASE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), purchaseCharge.isMeasureDiscountInAmount());

			purchaseFrightMappingList.add(purchaseFrightMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), purchaseFrightMappingList.toArray(new HashMap[0]));
	}

	int deletePurchaseChargeMapping(int companyId, int purchaseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_CHARGE_DATA, PURCHASE_CHARGE_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID, PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID ))
				.append(String.format(" AND %s = ? ", PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID, PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, purchaseId, companyId });
	}

	List<PurchaseChargeModel> getPurchaseChargeList(int companyId, int purchaseId) {
		StringBuilder sqlQueryToFetchData = new StringBuilder();
		
		sqlQueryToFetchData.append(String.format("SELECT SFM.%s, SFM.%s, ", PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE, PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE_MAPPING_ID))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", PURCHASE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_CHARGE_DATA_COLUMN.TAX_RATE, PURCHASE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT))
			.append(String.format(" SFM.%s, SFM.%s, SFM.%s, ", PURCHASE_CHARGE_DATA_COLUMN.ITEM_AMOUNT, PURCHASE_CHARGE_DATA_COLUMN.I_GST, PURCHASE_CHARGE_DATA_COLUMN.S_GST))
			.append(String.format(" SFM.%s, SFM.%s, ", PURCHASE_CHARGE_DATA_COLUMN.C_GST, PURCHASE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT))
			.append(String.format(" LD.%s, LD.%s, LD.%s ", LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_NAME, LEDGER_DETAILS_COLUMN.SAC_CODE))
			.append(String.format(" FROM %s SFM ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_CHARGE_DATA))
			.append(String.format(" INNER JOIN %s LD ON SFM.%s = LD.%s AND LD.%s = 0", COMPANY_RELATED_TABLE.ACC_COMPANY_LEDGER_DETAILS, PURCHASE_CHARGE_DATA_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.LEDGER_ID, LEDGER_DETAILS_COLUMN.IS_DELETED))
			.append(String.format(" AND LD.%s = ? ", LEDGER_DETAILS_COLUMN.COMPANY_ID, PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID))
			.append(String.format(" WHERE SFM.%s = ? AND SFM.%s = 0 ", PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_ID, PURCHASE_CHARGE_DATA_COLUMN.IS_DELETED))
			.append(String.format(" AND SFM.%s = ? ", PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID, PURCHASE_CHARGE_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().query(sqlQueryToFetchData.toString(), new Object[] { companyId, purchaseId, companyId }, getPurchaseChargeDetailsExtractor());

	}

	private ResultSetExtractor<List<PurchaseChargeModel>> getPurchaseChargeDetailsExtractor() {
		return new ResultSetExtractor<List<PurchaseChargeModel>>() {
			@Override
			public List<PurchaseChargeModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PurchaseChargeModel> purchaseChargeChargeList = new ArrayList<>();
				while (rs.next()) {
					purchaseChargeChargeList.add(new PurchaseChargeModel(rs));
				}
				return purchaseChargeChargeList;
			}
		};
	}

}
