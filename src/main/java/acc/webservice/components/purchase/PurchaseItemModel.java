package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.items.ItemModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;

public class PurchaseItemModel {

	private int purchaseItemMappingId;
	private double quantity;
	private double purchaseRate;
	private double discount;
	private double taxRate;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private double totalItemAmount;
	private boolean measureDiscountInAmount;
	private ItemModel item;
	private PurchaseModel purchase;
	private boolean deleted;
	private int freequantity;
	private int freeItemId;
	private int locationId;
	private double additionaldiscount;
	private boolean addmeasureDiscountInAmount;
	private int batchId;

	public PurchaseItemModel() {
	}

	
	public PurchaseItemModel(ResultSet rs) throws SQLException {
		setQuantity(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.QUANTITY.toString()));
		setPurchaseRate(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE.toString()));
		setTaxRate(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setDiscount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setPurchaseItemMappingId(rs.getInt(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ITEM_MAPPING_ID.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		setItemAmount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString()));
		setIgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.S_GST.toString()));
		setCgst(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.C_GST.toString()));
		setLocationId(rs.getInt(PURCHASE_ITEM_DATA_COLUMN.LOCTION_ID.toString()));
		setBatchId(rs.getInt(PURCHASE_ITEM_DATA_COLUMN.BATCH_ID.toString()));
		setAdditionaldiscount(rs.getDouble(PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString()));
		setAddmeasureDiscountInAmount(rs.getBoolean(PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString()));
		ItemModel fetchedItem = new ItemModel();
		fetchedItem.setItemId(rs.getInt(ITEM_DETAILS_COLUMN.ITEM_ID.toString()));
		fetchedItem.setItemName(rs.getString(ITEM_DETAILS_COLUMN.ITEM_NAME.toString()));
		fetchedItem.setHsnCode(rs.getInt(ITEM_DETAILS_COLUMN.HSN_CODE.toString()));
		setItem(fetchedItem);
	}

	public int getPurchaseItemMappingId() {
		return purchaseItemMappingId;
	}

	public void setPurchaseItemMappingId(int purchaseItemMappingId) {
		this.purchaseItemMappingId = purchaseItemMappingId;
	}

	
	public int getFreequantity() {
		return freequantity;
	}

	public void setFreequantity(int freequantity) {
		this.freequantity = freequantity;
	}

	public int getFreeItemId() {
		return freeItemId;
	}

	public void setFreeItemId(int freeItemId) {
		this.freeItemId = freeItemId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public ItemModel getItem() {
		return item;
	}

	public void setItem(ItemModel item) {
		this.item = item;
	}

	public PurchaseModel getPurchase() {
		return purchase;
	}

	public void setPurchase(PurchaseModel purchase) {
		this.purchase = purchase;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.deleted = isDeleted;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	
	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	
	
	
	public double getAdditionaldiscount() {
		return additionaldiscount;
	}


	public void setAdditionaldiscount(double additionaldiscount) {
		this.additionaldiscount = additionaldiscount;
	}


	public boolean isAddmeasureDiscountInAmount() {
		return addmeasureDiscountInAmount;
	}


	public void setAddmeasureDiscountInAmount(boolean addmeasureDiscountInAmount) {
		this.addmeasureDiscountInAmount = addmeasureDiscountInAmount;
	}


	@Override
	public String toString() {
		return "PurchaseItemModel [purchaseItemMappingId=" + purchaseItemMappingId + ", quantity=" + quantity
				+ ", purchaseRate=" + purchaseRate + ", discount=" + discount + ", itemAmount=" + itemAmount
				+ ", taxRate=" + taxRate + ", cGST=" + cgst + ", sGST=" + sgst + ", iGST=" + igst + ", totalItemAmount="
				+ totalItemAmount + ", deleted=" + deleted + ", measureDiscountInAmount=" + measureDiscountInAmount
				+ "]";
	}

}
