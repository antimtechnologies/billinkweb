package acc.webservice.components.purchase.utility;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyModel;
import acc.webservice.components.company.CompanyService;
import acc.webservice.components.items.ItemBatchModel;
import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemService;
import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.ledger.LedgerService;
import acc.webservice.components.location.LocationModel;
import acc.webservice.components.location.LocationModelService;
import acc.webservice.components.purchase.PurchaseChargeModel;
import acc.webservice.components.purchase.PurchaseImageModel;
import acc.webservice.components.purchase.PurchaseItemModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.sell.SellFreightChargeModel;
import acc.webservice.components.sell.SellImageModel;
import acc.webservice.components.sell.SellItemModel;
import acc.webservice.components.sell.SellService;
import acc.webservice.components.sell.model.SellModel;
import acc.webservice.components.users.UserModel;
import acc.webservice.components.users.UserService;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@Service
public class PurchaseToSellUtility {

	@Autowired
	private LedgerService ledgerService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private SellService sellService;

	@Autowired
	private LocationModelService locationService;

	public SellModel saveSellDataForSelectedLedger(PurchaseModel purchaseModel) throws AccountingSofwareException {
		SellModel sellModel = new SellModel();

		if (ApplicationUtility.getSize(purchaseModel.getBillNumber()) < 1) {
			return sellModel;
		}

		LedgerModel ledgerBasicDetails = ledgerService.getBasicLedgerDetailByLedgerId(
				purchaseModel.getLedgerData().getLedgerId(), purchaseModel.getCompanyId());
		if (ApplicationUtility.getSize(ledgerBasicDetails.getGstNumber()) > 0) {
			int selectedLedgerCompanyId = companyService.getCompanyIdByGSTNumber(ledgerBasicDetails.getGstNumber());
			CompanyModel sellCompanyModel = companyService.getCompanyDetails(selectedLedgerCompanyId);

			if (sellCompanyModel.isAllowB2BImport()) {
				if (selectedLedgerCompanyId > 0) {
					sellModel = sellService.getSellDetailsByBillNumber(selectedLedgerCompanyId,
							purchaseModel.getBillNumber());
					CompanyModel sellCompanyDetail = companyService.getCompanyDetails(purchaseModel.getCompanyId());
					int purchaseLedgerId = ledgerService.getLedgerIdByGSTNumber(selectedLedgerCompanyId, sellCompanyDetail.getGstNumber());
					sellModel = sellService.getSellDetailsForGivenLedgerBill(selectedLedgerCompanyId, purchaseModel.getBillNumber(), purchaseLedgerId);
					 
					if (sellModel.getSellId() > 0) {
						return sellModel;
					}

					
					sellModel = createSellModelFromPurcahseModel(purchaseModel, selectedLedgerCompanyId, sellCompanyModel, sellCompanyDetail, purchaseLedgerId);
 					
					sellService.saveSellData(sellModel);
				}
			}
		}
		return sellModel;
	}

	private SellModel createSellModelFromPurcahseModel(PurchaseModel purchaseModel, int selectedLedgerCompanyId, CompanyModel sellCompanyModel, CompanyModel sellCompanyDetail, int purchaseLedgerId) throws AccountingSofwareException {
 		SellModel sellModel = new SellModel();
		sellModel.setCompanyId(selectedLedgerCompanyId);
		sellModel.setBillNumber(purchaseModel.getBillNumber());
		sellModel.setDiscount(purchaseModel.getDiscount());
		sellModel.setGrosssTotalAmount(purchaseModel.getGrosssTotalAmount());
		sellModel.setSellDate(purchaseModel.getPurchaseDate());
		sellModel.setSellMeasureDiscountInAmount(purchaseModel.isPurchaseMeasureDiscountInAmount());
		sellModel.setTotalBillAmount(purchaseModel.getTotalBillAmount());
		sellModel.setImageURL(purchaseModel.getImageURL());
		sellModel.setPaymentStatus(purchaseModel.getPaymentStatus());
		sellModel.setBillNumberPrefix(sellCompanyModel.getBillNumberPrefix());
		sellModel.setDescription(purchaseModel.getDescription());
		sellModel.setStatus(purchaseModel.getStatus());
		sellModel.setReferenceNumber1(purchaseModel.getReferenceNumber1());
		sellModel.setReferenceNumber2(purchaseModel.getReferenceNumber2());
		sellModel.setSellMeasureDiscountInAmount(purchaseModel.isPurchaseMeasureDiscountInAmount());
		sellModel.setSellDate(purchaseModel.getBillDate().toString());
		sellModel.setPoNo(purchaseModel.getPoNumber());
		sellModel.setLrNo(purchaseModel.getLrNumber());
		sellModel.seteWayBillNo(purchaseModel.getEwayBillNumber());
		sellModel.setCityName(purchaseModel.getCityName());
		sellModel.setGstNumber(purchaseModel.getGstNumber());

		String nextBillNumber = purchaseModel.getBillNumber();
		if (ApplicationUtility.getSize(sellCompanyModel.getBillNumberPrefix()) > 0) {
			nextBillNumber = purchaseModel.getBillNumber().replaceAll(sellCompanyModel.getBillNumberPrefix(), "");
		}
		int currentBillNumber = 0;
		try {
			currentBillNumber = Integer.parseInt(nextBillNumber);
		} catch (NumberFormatException e) {
			e.printStackTrace(System.out);
		}
		sellModel.setCurrentBillNumber(currentBillNumber);
		
		LedgerModel sellLedgerData = new LedgerModel();
		
		if (purchaseLedgerId < 1) {
			purchaseLedgerId = savePurchaseLedgerData(selectedLedgerCompanyId, sellCompanyDetail);
		}

		sellLedgerData.setLedgerId(purchaseLedgerId);
		sellModel.setLedgerData(sellLedgerData);

		List<SellImageModel> sellImageURLList = new ArrayList<>();
		SellImageModel sellImageModel;
		if (ApplicationUtility.getSize(purchaseModel.getImageURLList()) > 0) {
			for (PurchaseImageModel purchaseImageModel : purchaseModel.getImageURLList()) {
				sellImageModel = new SellImageModel();
				sellImageModel.setImageURL(purchaseImageModel.getImageURL());
				sellImageURLList.add(sellImageModel);
			}
		}
		sellModel.setImageURLList(sellImageURLList);

		List<SellItemModel> sellItemList = new ArrayList<>();
		SellItemModel sellItemModel;

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseItemModel()) > 0) {
			for (PurchaseItemModel purchaseItemModel : purchaseModel.getPurchaseItemModel()) {
				if (purchaseItemModel.getItem() == null || purchaseItemModel.getItem().getItemId() <= 0) {
					continue;
				}
				sellItemModel = getSellItemModelFromPurchaseItemModel(purchaseItemModel, purchaseModel.getCompanyId(),
						selectedLedgerCompanyId);
				sellItemList.add(sellItemModel);
			}
		}
		sellModel.setSellItemModel(sellItemList);

		List<SellFreightChargeModel> sellChargeModelList = new ArrayList<>();
		SellFreightChargeModel sellChargeModel;
		if (ApplicationUtility.getSize(purchaseModel.getPurchaseChargeList()) > 0) {
			for (PurchaseChargeModel purchaseChargeModel : purchaseModel.getPurchaseChargeList()) {
				if (purchaseChargeModel.getLedger() == null || purchaseChargeModel.getLedger().getLedgerId() <= 0) {
					continue;
				}
				sellChargeModel = getSellFreightModelFromPurchaseChargeModel(purchaseChargeModel,
						purchaseModel.getCompanyId(), selectedLedgerCompanyId);
				sellChargeModelList.add(sellChargeModel);
			}
		}
		sellModel.setFreightList(sellChargeModelList);
		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		sellModel.setAddedBy(userData);

		return sellModel;
	}

	private SellFreightChargeModel getSellFreightModelFromPurchaseChargeModel(PurchaseChargeModel purchaseChargeModel,
			int purchaseCompanyId, int sellCompanyId) throws AccountingSofwareException {
		SellFreightChargeModel sellChargeModel = new SellFreightChargeModel();
		int ledgerId = ledgerService.getLedgerIdByName(sellCompanyId, purchaseChargeModel.getLedger().getLedgerName());

		if (ledgerId < 1) {
			LedgerModel ledgerModel = ledgerService.getLedgerDetailsById(purchaseCompanyId,
					purchaseChargeModel.getLedger().getLedgerId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			ledgerModel.setAddedBy(userData);
			ledgerModel.setLedgerDocumentList(null);
			ledgerModel.setOpeningBalance(0);
			ledgerModel.setCompanyId(sellCompanyId);
			ledgerId = ledgerService.saveLedgerData(ledgerModel).getLedgerId();
		}

		LedgerModel ledgerData = new LedgerModel();
		ledgerData.setLedgerId(ledgerId);

		sellChargeModel.setLedger(ledgerData);
		sellChargeModel.setCgst(purchaseChargeModel.getCgst());
		sellChargeModel.setDiscount(purchaseChargeModel.getDiscount());
		sellChargeModel.setIgst(purchaseChargeModel.getIgst());
		sellChargeModel.setItemAmount(purchaseChargeModel.getItemAmount());
		sellChargeModel.setMeasureDiscountInAmount(purchaseChargeModel.isMeasureDiscountInAmount());
		sellChargeModel.setFreightCharge(purchaseChargeModel.getPurchaseCharge());
		sellChargeModel.setSgst(purchaseChargeModel.getSgst());
		sellChargeModel.setTaxRate(purchaseChargeModel.getTaxRate());
		sellChargeModel.setTotalItemAmount(purchaseChargeModel.getTotalItemAmount());
		return sellChargeModel;
	}

	private SellItemModel getSellItemModelFromPurchaseItemModel(PurchaseItemModel purchaseItemModel,
			int purchaseCompanyId, int sellCompanyId) {
		SellItemModel sellItemModel = new SellItemModel();
		int itemId = itemService.getItemIdByName(sellCompanyId, purchaseItemModel.getItem().getItemName());

		if (itemId < 1) {
			ItemModel itemModel = itemService.getItemDetailsById(purchaseCompanyId,
					purchaseItemModel.getItem().getItemId());
			UserModel userData = new UserModel();
			userData.setUserId(UserService.getSystemUserId());
			itemModel.setAddedBy(userData);
			itemModel.setCompanyId(sellCompanyId);
			itemModel.setItemDocumentList(null);
			itemModel.setQuantity(0);
			itemModel.setBrand(null);
			itemModel.setCategory(null);
			itemId = itemService.saveItemData(itemModel).getItemId();
		}

		LocationModel locationModel = locationService.getLocationByCompanyLocationId(purchaseCompanyId,
				purchaseItemModel.getLocationId());
		if (locationModel != null) {
			LocationModel locationSellData = locationService.chekcklocationUniqueInsert(sellCompanyId, locationModel);
			sellItemModel.setLocationId(locationSellData.getLocationId());
		}
		ItemBatchModel batchModel = itemService.getOneBatch(purchaseItemModel.getBatchId());
		if (batchModel != null) {
			ItemBatchModel batchSellData = itemService.checkuniqueandInsertBatch(batchModel, itemId);
			sellItemModel.setBatchId(batchSellData.getBatchId());
		}
		ItemModel itemData = new ItemModel();
		itemData.setItemId(itemId);

		sellItemModel.setItem(itemData);
		sellItemModel.setCgst(purchaseItemModel.getCgst());
		sellItemModel.setDiscount(purchaseItemModel.getDiscount());
		sellItemModel.setIgst(purchaseItemModel.getIgst());
		sellItemModel.setItemAmount(purchaseItemModel.getItemAmount());
		sellItemModel.setMeasureDiscountInAmount(purchaseItemModel.isMeasureDiscountInAmount());
		sellItemModel.setSellRate(purchaseItemModel.getPurchaseRate());
		sellItemModel.setQuantity(purchaseItemModel.getQuantity());
		sellItemModel.setSgst(purchaseItemModel.getSgst());
		sellItemModel.setTaxRate(purchaseItemModel.getTaxRate());
		sellItemModel.setTotalItemAmount(purchaseItemModel.getTotalItemAmount());
		
		
		return sellItemModel;
	}

	private int savePurchaseLedgerData(int selectedLedgerCompanyId, CompanyModel sellCompanyDetail)
			throws AccountingSofwareException {
		LedgerModel ledgerModel = new LedgerModel();
		ledgerModel.setLedgerName(sellCompanyDetail.getCompanyName());
		ledgerModel.setCompanyId(selectedLedgerCompanyId);
		ledgerModel.setCityName(sellCompanyDetail.getCityName());
		ledgerModel.setState(sellCompanyDetail.getState());
		ledgerModel.setGstNumber(sellCompanyDetail.getGstNumber());
		ledgerModel.setMobileNumber(sellCompanyDetail.getMobileNumber());
		ledgerModel.setPanNumber(sellCompanyDetail.getPanNumber());
		ledgerModel.setPinCode(sellCompanyDetail.getPinCode());

		UserModel userData = new UserModel();
		userData.setUserId(UserService.getSystemUserId());
		ledgerModel.setAddedBy(userData);
		return ledgerService.saveLedgerData(ledgerModel).getLedgerId();
	}

}
