package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.components.items.ItemModel;
import acc.webservice.components.items.ItemPurchaseRateModel;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_PURCHASE_RATE_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;

@Repository
@Lazy
public class PurchaseItemDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@SuppressWarnings("unchecked")
	int[] savePurchaseItemsMappingData(List<PurchaseItemModel> purchaseItemList, int companyId, int purchaseId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_ITEM_DATA))
				.append(String.format(" ( %s, %s, %s, %s, %s, ", PURCHASE_ITEM_DATA_COLUMN.ITEM_ID, PURCHASE_ITEM_DATA_COLUMN.QUANTITY, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE, PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" %s, %s, %s, %s, ", PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.I_GST, PURCHASE_ITEM_DATA_COLUMN.S_GST, PURCHASE_ITEM_DATA_COLUMN.C_GST))
				.append(String.format(" %s, %s, %s, %s ,%s ,%s, %s, %s,%s,%s)", PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID, PURCHASE_ITEM_DATA_COLUMN.TAX_RATE, PURCHASE_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT , PURCHASE_ITEM_DATA_COLUMN.LOCTION_ID,
						PURCHASE_ITEM_DATA_COLUMN.BATCH_ID, PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_QUANTITY,PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_ID,PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT))
				.append("VALUES")
				.append(String.format(" ( :%s, :%s, :%s, :%s, :%s, ", PURCHASE_ITEM_DATA_COLUMN.ITEM_ID, PURCHASE_ITEM_DATA_COLUMN.QUANTITY, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE, PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.COMPANY_ID))
				.append(String.format(" :%s, :%s, :%s, :%s, ", PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.I_GST, PURCHASE_ITEM_DATA_COLUMN.S_GST, PURCHASE_ITEM_DATA_COLUMN.C_GST))
				.append(String.format(" :%s, :%s, :%s, :%s ,:%s ,:%s,:%s,:%s,:%s,:%s) ", PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID, PURCHASE_ITEM_DATA_COLUMN.TAX_RATE, PURCHASE_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT, PURCHASE_ITEM_DATA_COLUMN.LOCTION_ID,
						PURCHASE_ITEM_DATA_COLUMN.BATCH_ID, PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_QUANTITY,PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_ID,PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT,PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT));

		List<Map<String, Object>> purchaseItemMappingList = new ArrayList<>();
		Map<String, Object> purchaseItemMap;
		for (PurchaseItemModel purchaseItem : purchaseItemList) {

			if (purchaseItem.getItem() == null || purchaseItem.getItem().getItemId() <= 0) {
				continue;
			}

			purchaseItemMap = new HashMap<>();
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.ITEM_ID.toString(), purchaseItem.getItem().getItemId());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.QUANTITY.toString(), purchaseItem.getQuantity());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_RATE.toString(), purchaseItem.getPurchaseRate());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.ITEM_DISCOUNT.toString(), purchaseItem.getDiscount());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString(), purchaseItem.getTotalItemAmount());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString(), purchaseItem.isMeasureDiscountInAmount());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.TAX_RATE.toString(), purchaseItem.getTaxRate());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.ITEM_AMOUNT.toString(), purchaseItem.getTaxRate());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.I_GST.toString(), purchaseItem.getIgst());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.C_GST.toString(), purchaseItem.getCgst());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.S_GST.toString(), purchaseItem.getSgst());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID.toString(), purchaseId);
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.COMPANY_ID.toString(), companyId);
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.LOCTION_ID.toString(), purchaseItem.getLocationId());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_QUANTITY.toString(), purchaseItem.getFreequantity());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.FREE_ITEM_ID.toString(), purchaseItem.getFreeItemId());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.BATCH_ID.toString(), purchaseItem.getBatchId());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT.toString(), purchaseItem.isAddmeasureDiscountInAmount());
			purchaseItemMap.put(PURCHASE_ITEM_DATA_COLUMN.ITEM_ADDITIONAL_DISCOUNT.toString(), purchaseItem.getAdditionaldiscount());
			purchaseItemMappingList.add(purchaseItemMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), purchaseItemMappingList.toArray(new HashMap[0]));
	}

	int deletePurchaseItemMapping(int companyId, int purchaseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_ITEM_DATA, PURCHASE_ITEM_DATA_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID ))
				.append(String.format(" AND %s = ? ", PURCHASE_ITEM_DATA_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, purchaseId, companyId });
	}
	//13-05-2019 Start
//	List<PurchaseItemModel> getledgerPurchseDetails(int purchaseID) {
//		StringBuilder sqlQuery = new StringBuilder();
//		sqlQuery.append(String.format(" SELECT * FROM acc_company_purchase_item_data  AS pdi WHERE pdi.purchaseId = "+purchaseID+" AND pdi.isDeleted = 0 "));
//		Map<String, Object> paramMap = new HashMap<>();
//		paramMap.put(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID.toString(), purchaseID);
//		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getledgerPurchseDetailsExctractor());
//	}
//	
//	private ResultSetExtractor<List<PurchaseItemModel>> getledgerPurchseDetailsExctractor() {
//		return new ResultSetExtractor<List<PurchaseItemModel>>() {
//			@Override
//			public List<PurchaseItemModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
//				List<PurchaseItemModel> itemPurchaseList = new ArrayList<>();
//				while (rs.next()) {
//					itemPurchaseList.add(new PurchaseItemModel(rs));
//				}
//				return itemPurchaseList;
//			}
//		};
//	}
	
	
	List<PurchaseItemModel> getledgerPurchseDetails(int purchaseID) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT *,item.itemName,item.hsnCode FROM acc_company_purchase_item_data  AS pdi,acc_company_items_details AS item  WHERE pdi.purchaseId = "+purchaseID+" AND pdi.isDeleted = 0  AND pdi.itemId IN( SELECT item.itemId FROM acc_company_items_details AS item)  AND pdi.itemId = item.itemId "));
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID.toString(), purchaseID);
		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getledgerPurchseDetailsExctractor());
	}
	
	private ResultSetExtractor<List<PurchaseItemModel>> getledgerPurchseDetailsExctractor() {
		return new ResultSetExtractor<List<PurchaseItemModel>>() {
			@Override
			public List<PurchaseItemModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PurchaseItemModel> itemPurchaseList = new ArrayList<>();
				//ItemModel itemModel = new ItemModel();
				while (rs.next()) {
					itemPurchaseList.add(new PurchaseItemModel(rs));
					
				}
				return itemPurchaseList;
			}
		};
	}
	//13-05-2019 End
}
