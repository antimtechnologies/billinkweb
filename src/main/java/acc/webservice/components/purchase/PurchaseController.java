package acc.webservice.components.purchase;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import acc.webservice.components.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import acc.webservice.global.utils.DateUtility;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.purchase.utility.PurchaseToSellUtility;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_MRP_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_PURCHASE_RATE_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_ITEM_DATA_COLUMN;
import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/purchaseData/")
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private PurchaseToSellUtility purchaseToSellUtility;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getPurchaseListData", method = RequestMethod.POST)
	public Map<String, Object> getPurchaseListData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.START.toString());
		int numberOfRecord = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		
		Map<String, Object> filterParamter = new HashMap<>();
		if (parameters.containsKey("filterData")) {
			filterParamter = (Map<String, Object>) parameters.get("filterData");
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.getPurchaseListData(companyId, start, numberOfRecord, filterParamter));
		return responseData;
	}

	@RequestMapping(value = "getPurchaseDetailsById", method = RequestMethod.POST)
	public Map<String, Object> getPurchaseDetailsById(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchaseId = ApplicationUtility.getIntValue(parameters, PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.getPurchaseDetailsById(companyId, purchaseId));
		return responseData;
	}

	@RequestMapping(value = "savePurchaseData", method = RequestMethod.POST)
	public Map<String, Object> savePurchaseData(@RequestBody PurchaseModel purchaseModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.savePurchaseData(purchaseModel));
		return responseData;
	}

	@RequestMapping(value = "publishPurchaseData", method = RequestMethod.POST)
	public Map<String, Object> publishPurchaseData(@RequestBody PurchaseModel purchaseModel) throws AccountingSofwareException {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.publishPurchaseData(purchaseModel));
		try {			
			purchaseToSellUtility.saveSellDataForSelectedLedger(purchaseModel);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}
		return responseData;
	}

	@RequestMapping(value = "updateSavedPurchaseData", method = RequestMethod.POST)
	public Map<String, Object> updateSavedPurchaseData(@RequestBody PurchaseModel purchaseModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, purchaseModel.getPurchaseDate())) {
				purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.updateSavedPurchaseData(purchaseModel));
		return responseData;
	}

	@RequestMapping(value = "updatePublishedPurchaseData", method = RequestMethod.POST)
	public Map<String, Object> updatePublishedPurchaseData(@RequestBody PurchaseModel purchaseModel) {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, purchaseModel.getPurchaseDate())) {
			purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.updatePublishedPurchaseData(purchaseModel));
		return responseData;
	}

	@RequestMapping(value = "deleteSavedPurchaseData", method = RequestMethod.POST)
	public Map<String, Object> deleteSavedPurchaseData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchaseId = ApplicationUtility.getIntValue(parameters, PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.deleteSavedPurchaseData(companyId, purchaseId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "deletePublishedPurchaseData", method = RequestMethod.POST)
	public Map<String, Object> deletePublishedPurchaseData(@RequestBody Map<String, Object> parameters) {
		int companyId = ApplicationUtility.getIntValue(parameters, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int purchaseId = ApplicationUtility.getIntValue(parameters, PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameters, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.deletePublishPurchaseData(companyId, purchaseId, deletedBy));
		return responseData;
	}

	@RequestMapping(value = "markPurchaseAsVerified", method = RequestMethod.POST)
	public Map<String, Object> markPurchaseAsVerified(@RequestBody PurchaseModel purchaseModel) {
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.markPurchaseAsVerified(purchaseModel));
		return responseData;
	}

	@RequestMapping(value = "publishSavedPurchase", method = RequestMethod.POST)
	public Map<String, Object> publishSavedPurchase(@RequestBody PurchaseModel purchaseModel) throws AccountingSofwareException {
		if (ApplicationUtility.isDateInGivenFormat(DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, purchaseModel.getPurchaseDate())) {
			purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
 		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.publishSavedPurchase(purchaseModel));

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseDate()) > 0) {
			purchaseModel.setPurchaseDate(DateUtility.convertDateFormat(purchaseModel.getPurchaseDate(), DateUtility.DEFAULT_USER_DATE_TIME_FORMAT, DateUtility.DEFAULT_DATA_BASE_DATE_FORMAT));
		}
		try {
			
			if (purchaseModel.getAddedBy().getUserId() != UserService.getSystemUserId()) {
				purchaseToSellUtility.saveSellDataForSelectedLedger(purchaseModel);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new AccountingSofwareException("Your data saved successfully. But got error while saving B2B data.");
		}

		return responseData;
	}

	
	@RequestMapping(value="getpurchaseDetails",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchase(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("companyId") != null && request.getParameter("ledgerId") != null && request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl  = "http://localhost:8090/user/purchasecharge/findpurchasedetails?companyId="+companyId;
		}
		else if(request.getParameter("companyId") != null && request.getParameter("billNumber") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			String billNumber = request.getParameter("billNumber");
			fooResourceUrl  = "http://localhost:8090/user/purchasecharge/findpurchasedetails?companyId="+companyId+"&billNumber"+billNumber;
		}
		else if (request.getParameter("companyId") != null) {
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			fooResourceUrl  = "http://localhost:8090/user/purchasecharge/findpurchasedetails?companyId="+companyId;
		}
		else if(request.getParameter("billNumber") != null){
			String billNumber = request.getParameter("companyId");
			fooResourceUrl  = "http://localhost:8090/user/purchasecharge/findpurchasedetails?billNumber="+billNumber;	
		}
		else {
			fooResourceUrl  = "http://localhost:8090/user/purchasecharge/findpurchasedetails";
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	
	}

	
	
	@RequestMapping(value="findbyday",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if(request.getParameter("day") != null){
			int day = Integer.parseInt(request.getParameter("day"));
		 fooResourceUrl  = "http://localhost:8090/user/purchase/findbyday?day="+day;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	@RequestMapping(value="findbymonth",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if(request.getParameter("month") != null){
			int month = Integer.parseInt(request.getParameter("month"));
		 fooResourceUrl  = "http://localhost:8090/user/purchase/findbymonth?month="+month;
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	

	@RequestMapping(value="getpurchaseItemDetails",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getPurchaseItemDetail(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		
		if(request.getParameter("userId") != null ) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/?userId="+userId;
		} 
		else if(request.getParameter("totalItemAmount") != null ){
			Double totalItemAmount = Double.parseDouble(request.getParameter("totalItemAmount")); 
			fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/?totalItemAmount="+totalItemAmount;
		}
		else if(request.getParameter("quantity") != null ){
			Double quantity = Double.parseDouble(request.getParameter("quantity")); 
			fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/?quantity="+quantity;
		}
		else if(request.getParameter("purchaseRate") != null ){
			Double purchaseRate = Double.parseDouble(request.getParameter("purchaseRate")); 
			fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/?quantity="+purchaseRate;
		}
		else {
			fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/";
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	
	
	@RequestMapping(value="findtotalamountbycompanyandday",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalPurchaseAmountByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if(request.getParameter("day") != null){
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
			 fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/findtotalamountbycompanyandday?companyId="+companyId+"&day="+day;
			}
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	@RequestMapping(value="findtotalamountbycompanyandmonth",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findTotalPurchaseAmountByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if(request.getParameter("month") != null){
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
			 fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/findtotalamountbycompanyandmonth?companyId="+companyId+"&month="+month;
			}
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	@RequestMapping(value="findAllPurchasebycompanyandday",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findallPurchasetByDay(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if(request.getParameter("day") != null){
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int day = Integer.parseInt(request.getParameter("day"));
			 fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/findallpurchasebycompanyandday?companyId="+companyId+"&day="+day;
			}
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	@RequestMapping(value="findAllPurchasebycompanyandmonth",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> findallPurchaseByMonth(HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = null;
		if (request.getParameter("companyId") != null) {
			if(request.getParameter("month") != null){
				int companyId = Integer.parseInt(request.getParameter("companyId"));
				int month = Integer.parseInt(request.getParameter("month"));
			 fooResourceUrl  = "http://localhost:8090/user/purchaseitemdata/findallpurchasebycompanyandmonth?companyId="+companyId+"&month="+month;
			}
		}
		ResponseEntity<String> response  = restTemplate.getForEntity(fooResourceUrl , String.class);
		return response;
	}
	
	//Get LedgerPurchase Details 13-05-2019 Start
	@RequestMapping(value = "getLedgerPurchaseDetails", method = RequestMethod.POST)
	public Map<String, Object> getLedgerPurchaseDetails(@RequestBody Map<String, Object> parameters) {
		int ledgerId = ApplicationUtility.getIntValue(parameters, PURCHASE_DETAILS_COLUMN.LEDGER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.getledgerPurchaseList(ledgerId));
		return responseData;
	}
	
	@RequestMapping(value="getItemPurchaserateByItemId", method=RequestMethod.POST)
	public Map<String, Object> getItemMrpByItemId(@RequestBody Map<String, Object> params) {
		int itemId = ApplicationUtility.getIntValue(params, ITEM_PURCHASE_RATE_COLUMN.ITEM_ID.toString());
		String date = ApplicationUtility.getStrValue(params, ITEM_PURCHASE_RATE_COLUMN.WET_PURCHASE_RATE_DATE.toString());
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", purchaseService.getPurchaseRate(itemId,date));
		return response;
	}
	
	@RequestMapping(value = "getledgerItemPurchaseList", method = RequestMethod.POST)
	public Map<String, Object> getledgerItemPurchaseList(@RequestBody Map<String, Object> parameters) {
		int purchseID = ApplicationUtility.getIntValue(parameters, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.getPurchaseItemList(purchseID));
		return responseData;
	}
	// Get LedgerPurchase Details 13-05-2019 End
	@RequestMapping(value = "getItemNameForDebit", method = RequestMethod.POST)
	public Map<String, Object> getItemNameForDebit(@RequestBody Map<String, Object> parameters) {
		String itemName = ApplicationUtility.getStrValue(parameters, ITEM_DETAILS_COLUMN.ITEM_NAME.toString());
		String purchseID = ApplicationUtility.getStrValue(parameters, PURCHASE_ITEM_DATA_COLUMN.PURCHASE_ID.toString());
		purchseID = purchseID+"0";
		//String val = parameters.values().toString().replace("[", "").replace("]", "");
		//val=val.substring(0,val.length()-1);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", purchaseService.getItemName(itemName,purchseID));
		return responseData;
	}
	
	
}
