package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.PURCHASE_IMAGE_URL_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class PurchaseImageDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	List<PurchaseImageModel> getSellPaymentImageList(int companyId, int purchaseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" SELECT %s, %s FROM %s", PURCHASE_IMAGE_URL_COLUMN.IMAGE_ID, PURCHASE_IMAGE_URL_COLUMN.IMAGE_URL, COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_IMAGE_URL))
			.append(String.format(" WHERE %s = :%s ", PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID, PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID))
			.append(String.format(" AND %s = :%s AND %s = 0  ", PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_IMAGE_URL_COLUMN.IS_DELETED));

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
		paramMap.put(PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID.toString(), purchaseId);

		return namedParameterJdbcTemplate.query(sqlQuery.toString(), paramMap, getCreditNoteImageResultExctractor());
	}

	private ResultSetExtractor<List<PurchaseImageModel>> getCreditNoteImageResultExctractor() {
		return new ResultSetExtractor<List<PurchaseImageModel>>() {
			@Override
			public List<PurchaseImageModel> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PurchaseImageModel> purchaseImageModelList = new ArrayList<>();
				while (rs.next()) {
					purchaseImageModelList.add(new PurchaseImageModel(rs));
				}
				return purchaseImageModelList;
			}
		};
	}

	@SuppressWarnings("unchecked")
	int[] saveCreditNoteImageURLData(List<PurchaseImageModel> purchaseImageModelList, int companyId, int purchaseId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_IMAGE_URL))
				.append(String.format(" ( %s, %s, %s) ", PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID, PURCHASE_IMAGE_URL_COLUMN.IMAGE_URL, PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s) ", PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID, PURCHASE_IMAGE_URL_COLUMN.IMAGE_URL, PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID));

		List<Map<String, Object>> purchaseImageURLMapList = new ArrayList<>();
		Map<String, Object> purchaseImageURLMap;
		for (PurchaseImageModel purchaseImageModel : purchaseImageModelList) {
			purchaseImageURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(purchaseImageModel.getImageURL())) {			
				purchaseImageModel.setImageURL(purchaseImageModel.getImageURL().replace(ApplicationUtility.getServerURL(), ""));
			}

			purchaseImageURLMap.put(PURCHASE_IMAGE_URL_COLUMN.IMAGE_URL.toString(), purchaseImageModel.getImageURL());
			purchaseImageURLMap.put(PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID.toString(), purchaseId);
			purchaseImageURLMap.put(PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID.toString(), companyId);
			purchaseImageURLMapList.add(purchaseImageURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), purchaseImageURLMapList.toArray(new HashMap[0]));
	}

	int deleteCreditNoteImageURLData(int companyId, int purchaseId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", COMPANY_RELATED_TABLE.ACC_COMPANY_PURCHASE_IMAGE_URL, PURCHASE_IMAGE_URL_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID, PURCHASE_IMAGE_URL_COLUMN.PURCHASE_ID ))
				.append(String.format(" AND %s = ? ", PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID, PURCHASE_IMAGE_URL_COLUMN.COMPANY_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, purchaseId, companyId });
	}

}
