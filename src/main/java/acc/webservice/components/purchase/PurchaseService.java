package acc.webservice.components.purchase;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.company.CompanyService;
import acc.webservice.components.items.ItemPurchaseRateModel;
import acc.webservice.components.ledger.LedgerPurchaseDetailsModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.components.status.StatusUtil;
import acc.webservice.enums.DatabaseEnum.PURCHASE_DETAILS_COLUMN;
import acc.webservice.enums.STATUS_TEXT;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class PurchaseService {

	@Autowired
	private PurchaseDAO purchaseDAO;

	@Autowired
	private PurchaseItemDAO purchaseItemDAO;

	@Autowired
	private PurchaseImageDAO purchaseImageDAO;

	@Autowired
	private StatusUtil statusUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private PurchaseChargeDAO purchaseChargeDAO;

	public List<PurchaseModel> getPurchaseListData(int companyId, int start, int numberOfRecord, Map<String, Object> filterParamter) {
		return purchaseDAO.getPurchaseListData(companyId, start, numberOfRecord, filterParamter);
	}

	public PurchaseModel getPurchaseDetailsById(int companyId, int purchaseId) {
		PurchaseModel purchaseModel = purchaseDAO.getPurchaseDetailsByColumnNameValue(companyId, PURCHASE_DETAILS_COLUMN.PURCHASE_ID.toString(), String.valueOf(purchaseId));
		purchaseModel.setPurchaseChargeList(purchaseChargeDAO.getPurchaseChargeList(companyId, purchaseId));
		purchaseModel.setImageURLList(purchaseImageDAO.getSellPaymentImageList(companyId, purchaseId));
		return purchaseModel;
	}

	public PurchaseModel getPurchaseDetailsByBillNumber(int companyId, String billNumber,int ledgerId) {
		PurchaseModel purchaseModel = purchaseDAO.getPurchaseDetailsForGivenLedgerBill(companyId, PURCHASE_DETAILS_COLUMN.BILL_NUMBER.toString(), billNumber, ledgerId);
 		
		if (purchaseModel.getPurchaseId() > 0) {			
			purchaseModel.setPurchaseChargeList(purchaseChargeDAO.getPurchaseChargeList(companyId, purchaseModel.getPurchaseId()));
			purchaseModel.setImageURLList(purchaseImageDAO.getSellPaymentImageList(companyId, purchaseModel.getPurchaseId()));
		}
		return purchaseModel;
	}

	public PurchaseModel savePurchaseData(PurchaseModel purchaseModel) {
		validatePurchaseManupulateData(purchaseModel);
		purchaseDAO.savePurchaseData(purchaseModel);
		if (ApplicationUtility.getSize(purchaseModel.getPurchaseItemModel()) > 0) {
			purchaseItemDAO.savePurchaseItemsMappingData(purchaseModel.getPurchaseItemModel(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseChargeList()) > 0) {
			purchaseChargeDAO.savePurchaseChargeMappingData(purchaseModel.getPurchaseChargeList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getImageURLList()) > 0) {
			purchaseImageDAO.saveCreditNoteImageURLData(purchaseModel.getImageURLList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}
		return purchaseModel;
	}

	public PurchaseModel publishPurchaseData(PurchaseModel purchaseModel) {
		validatePurchaseManupulateData(purchaseModel);
		purchaseDAO.publishPurchaseData(purchaseModel);
		if (ApplicationUtility.getSize(purchaseModel.getPurchaseItemModel()) > 0) {
			purchaseItemDAO.savePurchaseItemsMappingData(purchaseModel.getPurchaseItemModel(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseChargeList()) > 0) {
			purchaseChargeDAO.savePurchaseChargeMappingData(purchaseModel.getPurchaseChargeList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getImageURLList()) > 0) {
			purchaseImageDAO.saveCreditNoteImageURLData(purchaseModel.getImageURLList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}
		companyService.updateCompanyPurchaseCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 2), purchaseModel.getCompanyId());
		companyService.updateCompanyPurchasePendingCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 0), purchaseModel.getCompanyId());
		return purchaseModel;
	}

	public PurchaseModel updateSavedPurchaseData(PurchaseModel purchaseModel) {
		validatePurchaseManupulateData(purchaseModel);
		if (ApplicationUtility.getSize(purchaseModel.getPurchaseItemModel()) > 0) {
			purchaseItemDAO.deletePurchaseItemMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseItemDAO.savePurchaseItemsMappingData(purchaseModel.getPurchaseItemModel(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getImageURLList()) > 0) {
			purchaseImageDAO.deleteCreditNoteImageURLData(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseImageDAO.saveCreditNoteImageURLData(purchaseModel.getImageURLList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseChargeList()) > 0) {
			purchaseChargeDAO.deletePurchaseChargeMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseChargeDAO.savePurchaseChargeMappingData(purchaseModel.getPurchaseChargeList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		return purchaseDAO.updateSavedPurchaseData(purchaseModel);
	}

	public PurchaseModel updatePublishedPurchaseData(PurchaseModel purchaseModel) {
		validatePurchaseManupulateData(purchaseModel);
		if (ApplicationUtility.getSize(purchaseModel.getPurchaseItemModel()) > 0) {
			purchaseItemDAO.deletePurchaseItemMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseItemDAO.savePurchaseItemsMappingData(purchaseModel.getPurchaseItemModel(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getImageURLList()) > 0) {
			purchaseImageDAO.deleteCreditNoteImageURLData(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseImageDAO.saveCreditNoteImageURLData(purchaseModel.getImageURLList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		if (ApplicationUtility.getSize(purchaseModel.getPurchaseChargeList()) > 0) {
			purchaseChargeDAO.deletePurchaseChargeMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseChargeDAO.savePurchaseChargeMappingData(purchaseModel.getPurchaseChargeList(), purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
		}

		purchaseDAO.updatePublishedPurchaseData(purchaseModel);
		companyService.updateCompanyPurchasePendingCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 0), purchaseModel.getCompanyId());
		return purchaseModel;
	}

	public int deleteSavedPurchaseData(int companyId, int purchaseId, int deletedBy) {
		purchaseItemDAO.deletePurchaseItemMapping(companyId, purchaseId);
		purchaseImageDAO.deleteCreditNoteImageURLData(companyId, purchaseId);
		return purchaseDAO.deleteSavedPurchaseData(companyId, purchaseId, deletedBy);
	}

	public int deletePublishPurchaseData(int companyId, int purchaseId, int deletedBy) {
		purchaseDAO.deletePublishPurchaseData(companyId, purchaseId, deletedBy);
		return companyService.updateCompanyPurchasePendingCount(purchaseDAO.getPendingPurchaseCountData(companyId, 0), companyId);
	}

	public PurchaseModel markPurchaseAsVerified(PurchaseModel purchaseModel) {
		if (purchaseModel.getStatus().getStatusId() == statusUtil.getStatusIdByName(STATUS_TEXT.DELETED.toString())) {
			purchaseItemDAO.deletePurchaseItemMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseItemDAO.deletePurchaseItemMapping(purchaseModel.getCompanyId(), purchaseModel.getPurchaseId());
			purchaseDAO.markPurchaseAsDeleteAndVerified(purchaseModel);
		} else {
			purchaseDAO.markPurchaseAsVerified(purchaseModel);
		}

		companyService.updateCompanyPurchasePendingCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 0), purchaseModel.getCompanyId());
		return purchaseModel;
	}

	public PurchaseModel publishSavedPurchase(PurchaseModel purchaseModel) {
		updateSavedPurchaseData(purchaseModel);
		purchaseDAO.publishSavedPurchase(purchaseModel);
		companyService.updateCompanyPurchaseCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 2), purchaseModel.getCompanyId());
		companyService.updateCompanyPurchasePendingCount(purchaseDAO.getPendingPurchaseCountData(purchaseModel.getCompanyId(), 0), purchaseModel.getCompanyId());
		return purchaseModel;
	}

	public int[] updatePurchasePaymentStatus(List<Map<String, Object>> purchasePaymentStatusList, int companyId) {
		return purchaseDAO.updatePurchasePaymentStatus(purchasePaymentStatusList, companyId);
	}

	private void validatePurchaseManupulateData(PurchaseModel purchaseModel) {		
		if (purchaseModel.getLedgerData() == null || purchaseModel.getLedgerData().getLedgerId() < 1) {
			throw new InvalidParameterException("Ledger data is not passed to save purchase data.");
		}
	}
	
	public List<LedgerPurchaseDetailsModel> getledgerPurchaseList(int ledgerID){
		return purchaseDAO.getledgerPurchaseList(ledgerID);
	}
	
	public List<ItemPurchaseRateModel> getPurchaseRate(int itemId,String date){
		return purchaseDAO.getItemMrpByItemIdAndDateList(itemId, date);
	}
	
	public List<PurchaseItemModel> getPurchaseItemList(int purchaseID){
		return purchaseItemDAO.getledgerPurchseDetails(purchaseID);
	}
	
	public List<DebitNoteGetItem> getItemName(String itemName,String arr){
		return purchaseDAO.getItemDataListForDebitNote(itemName,arr);
	}
	
	
	
}

