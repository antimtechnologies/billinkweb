package acc.webservice.components.purchase;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.components.ledger.LedgerModel;
import acc.webservice.components.purchase.model.PurchaseModel;
import acc.webservice.enums.DatabaseEnum.LEDGER_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.PURCHASE_CHARGE_DATA_COLUMN;

public class PurchaseChargeModel {

	private int purchaseChargeMappingId;
	private double purchaseCharge;
	private boolean deleted;
	private LedgerModel ledger;
	private PurchaseModel purchase;
	private double discount;
	private double taxRate;
	private double totalItemAmount;
	private double itemAmount;
	private double cgst;
	private double sgst;
	private double igst;
	private boolean measureDiscountInAmount;

	public PurchaseChargeModel() {
	}

	public PurchaseChargeModel(ResultSet rs) throws SQLException {
		setPurchaseChargeMappingId(rs.getInt(PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE_MAPPING_ID.toString()));
		setPurchaseCharge(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.PURCHASE_CHARGE.toString()));
		setDiscount(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.ITEM_DISCOUNT.toString()));
		setTaxRate(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.TAX_RATE.toString()));
		setTotalItemAmount(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.TOTAL_ITEM_AMOUNT.toString()));
		setItemAmount(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.ITEM_AMOUNT.toString()));
		setCgst(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.C_GST.toString()));
		setIgst(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.I_GST.toString()));
		setSgst(rs.getDouble(PURCHASE_CHARGE_DATA_COLUMN.S_GST.toString()));
		setMeasureDiscountInAmount(rs.getBoolean(PURCHASE_CHARGE_DATA_COLUMN.MEASURE_DISCOUNT_IN_AMOUNT.toString()));

		LedgerModel fetchedLedger = new LedgerModel();
		fetchedLedger.setLedgerId(rs.getInt(LEDGER_DETAILS_COLUMN.LEDGER_ID.toString()));
		fetchedLedger.setLedgerName(rs.getString(LEDGER_DETAILS_COLUMN.LEDGER_NAME.toString()));
		fetchedLedger.setSacCode(rs.getString(LEDGER_DETAILS_COLUMN.SAC_CODE.toString()));

		setLedger(fetchedLedger);
	}

	public int getPurchaseChargeMappingId() {
		return purchaseChargeMappingId;
	}

	public void setPurchaseChargeMappingId(int purchaseChargeMappingId) {
		this.purchaseChargeMappingId = purchaseChargeMappingId;
	}

	public PurchaseModel getPurchase() {
		return purchase;
	}

	public void setPurchase(PurchaseModel purchase) {
		this.purchase = purchase;
	}

	public double getPurchaseCharge() {
		return purchaseCharge;
	}

	public void setPurchaseCharge(double purchaseCharge) {
		this.purchaseCharge = purchaseCharge;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LedgerModel getLedger() {
		return ledger;
	}

	public void setLedger(LedgerModel ledger) {
		this.ledger = ledger;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(double totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public boolean isMeasureDiscountInAmount() {
		return measureDiscountInAmount;
	}

	public void setMeasureDiscountInAmount(boolean measureDiscountInAmount) {
		this.measureDiscountInAmount = measureDiscountInAmount;
	}

	@Override
	public String toString() {
		return "SellFreightChargeModel [sellFreightMappingId=" + purchaseChargeMappingId + ", freightCharge="
				+ purchaseCharge + "]";
	}

}
