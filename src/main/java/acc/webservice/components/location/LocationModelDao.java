package acc.webservice.components.location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import acc.webservice.components.price.PriceModel;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_RELATED_TABLE;
import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DOCUMENT_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DOCUMENT_COLUMN;
import acc.webservice.global.utils.DateUtility;

@Repository
@Lazy
public class LocationModelDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	
	LocationModel saveLocationdata(LocationModel locationModel) {
		StringBuilder sqlQueryLocationSave = new StringBuilder();
		sqlQueryLocationSave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_LOCATION_DETAILS))
							.append(String.format(" ( %s ,%s, %s, %s ,", LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.ADDRESS, LOCATION_DETAILS_COLUMN.GODOWN_GST, LOCATION_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" %s ,%s, %s, %s,", LOCATION_DETAILS_COLUMN.CITY,LOCATION_DETAILS_COLUMN.EMAIL, LOCATION_DETAILS_COLUMN.PINCODE, LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE))
							.append(String.format(" %s ,%s )", LOCATION_DETAILS_COLUMN.CONTACT_NUMBER,LOCATION_DETAILS_COLUMN.STATE_ID))
							.append(" VALUES ")
							.append(String.format("( :%s, :%s, :%s, :%s ,", LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.ADDRESS, LOCATION_DETAILS_COLUMN.GODOWN_GST, LOCATION_DETAILS_COLUMN.COMPANY_ID))
							.append(String.format(" :%s , :%s, :%s, :%s,", LOCATION_DETAILS_COLUMN.CITY,LOCATION_DETAILS_COLUMN.EMAIL, LOCATION_DETAILS_COLUMN.PINCODE, LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE))
							.append(String.format(" :%s ,:%s )", LOCATION_DETAILS_COLUMN.CONTACT_NUMBER,LOCATION_DETAILS_COLUMN.STATE_ID));

		

		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString(), locationModel.getLocationName())
				.addValue(LOCATION_DETAILS_COLUMN.ADDRESS.toString(), locationModel.getAddress())
				.addValue(LOCATION_DETAILS_COLUMN.GODOWN_GST.toString(), locationModel.getGoDownGst())
				.addValue(LOCATION_DETAILS_COLUMN.CITY.toString(), locationModel.getCity())
				.addValue(LOCATION_DETAILS_COLUMN.EMAIL.toString(), locationModel.getEmail())
				.addValue(LOCATION_DETAILS_COLUMN.PINCODE.toString(), locationModel.getPinCode())
				.addValue(LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE.toString(), locationModel.getLocationIncharge())
				.addValue(LOCATION_DETAILS_COLUMN.CONTACT_NUMBER.toString(), locationModel.getContactNumber())
				.addValue(LOCATION_DETAILS_COLUMN.STATE_ID.toString(), locationModel.getStateId())
				.addValue(LOCATION_DETAILS_COLUMN.COMPANY_ID.toString(), locationModel.getCompanyId());
				

		namedParameterJdbcTemplate.update(sqlQueryLocationSave.toString(), parameters, holder);
		locationModel.setLocationId(holder.getKey().intValue());
		return locationModel;
	}
	
	public boolean chekcklocationUnique(LocationModel locationModel) {
		StringBuilder sqlQuery = new StringBuilder(String.format("SELECT * FROM acc_location_details where isdeleted=0 and %s=:%s and %s=:%s",LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.COMPANY_ID,LOCATION_DETAILS_COLUMN.COMPANY_ID));
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString(),locationModel.getLocationName())
				.addValue(LOCATION_DETAILS_COLUMN.COMPANY_ID.toString(),locationModel.getCompanyId());
		List<LocationModel> location = namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters,(resultSet, i) -> {
            return toLocation(resultSet);
        });
		if (location.size() > 0) {
			return true;
		}else {
		return false;}
	}
	
	public LocationModel chekcklocationUniqueInsert(int companyId,LocationModel locationModel) {
		StringBuilder sqlQuery = new StringBuilder(String.format("SELECT * FROM acc_location_details where isdeleted=0 and %s=:%s and %s=:%s",LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.COMPANY_ID,LOCATION_DETAILS_COLUMN.COMPANY_ID));
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString(),locationModel.getLocationName())
				.addValue(LOCATION_DETAILS_COLUMN.COMPANY_ID.toString(),companyId);
		List<LocationModel> location = namedParameterJdbcTemplate.query(sqlQuery.toString(), parameters,(resultSet, i) -> {
            return toLocation(resultSet);
        });
		if (location.size() > 0) {
			return location.get(0);
		}else {
			StringBuilder sqlQueryLocationSave = new StringBuilder();
			sqlQueryLocationSave.append(String.format("INSERT INTO %s ", DATABASE_TABLE.ACC_LOCATION_DETAILS))
								.append(String.format(" ( %s ,%s, %s, %s ,", LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.ADDRESS, LOCATION_DETAILS_COLUMN.GODOWN_GST, LOCATION_DETAILS_COLUMN.COMPANY_ID))
								.append(String.format(" %s ,%s, %s, %s,", LOCATION_DETAILS_COLUMN.CITY,LOCATION_DETAILS_COLUMN.EMAIL, LOCATION_DETAILS_COLUMN.PINCODE, LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE))
								.append(String.format(" %s ,%s )", LOCATION_DETAILS_COLUMN.CONTACT_NUMBER,LOCATION_DETAILS_COLUMN.STATE_ID))
								.append(" VALUES ")
								.append(String.format("( :%s, :%s, :%s, :%s ,", LOCATION_DETAILS_COLUMN.LOCATION_NAME,LOCATION_DETAILS_COLUMN.ADDRESS, LOCATION_DETAILS_COLUMN.GODOWN_GST, LOCATION_DETAILS_COLUMN.COMPANY_ID))
								.append(String.format(" :%s , :%s, :%s, :%s,", LOCATION_DETAILS_COLUMN.CITY,LOCATION_DETAILS_COLUMN.EMAIL, LOCATION_DETAILS_COLUMN.PINCODE, LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE))
								.append(String.format(" :%s ,:%s )", LOCATION_DETAILS_COLUMN.CONTACT_NUMBER,LOCATION_DETAILS_COLUMN.STATE_ID));

			

			KeyHolder holder = new GeneratedKeyHolder();
			 parameters = new MapSqlParameterSource()
					.addValue(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString(), locationModel.getLocationName())
					.addValue(LOCATION_DETAILS_COLUMN.ADDRESS.toString(), locationModel.getAddress())
					.addValue(LOCATION_DETAILS_COLUMN.GODOWN_GST.toString(), locationModel.getGoDownGst())
					.addValue(LOCATION_DETAILS_COLUMN.CITY.toString(), locationModel.getCity())
					.addValue(LOCATION_DETAILS_COLUMN.EMAIL.toString(), locationModel.getEmail())
					.addValue(LOCATION_DETAILS_COLUMN.PINCODE.toString(), locationModel.getPinCode())
					.addValue(LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE.toString(), locationModel.getLocationIncharge())
					.addValue(LOCATION_DETAILS_COLUMN.CONTACT_NUMBER.toString(), locationModel.getContactNumber())
					.addValue(LOCATION_DETAILS_COLUMN.STATE_ID.toString(), locationModel.getStateId())
					.addValue(LOCATION_DETAILS_COLUMN.COMPANY_ID.toString(), companyId);
					

			namedParameterJdbcTemplate.update(sqlQueryLocationSave.toString(), parameters, holder);
			locationModel.setLocationId(holder.getKey().intValue());
			return locationModel;
		}
	}
	
	public List<LocationModel> findByLocationByCompanyId(int companyId) {
        List<LocationModel> location = namedParameterJdbcTemplate.query("SELECT * FROM acc_location_details WHERE isdeleted=0 and companyId = :id ",
                new MapSqlParameterSource("id", companyId), (resultSet, i) -> {
                    return toLocation(resultSet);
                });

        return location;
    }
	
	
	public LocationModel findByLocationByCompanyLocationId(int companyId,int locationId) {
		
		StringBuilder locationquery=new StringBuilder("SELECT * FROM acc_location_details WHERE isdeleted=0 and companyId = :id and locationId=:locationId");
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("locationId", locationId)
				.addValue("id", companyId);
		
		List<LocationModel> location = namedParameterJdbcTemplate.query(locationquery.toString(),parameters, (resultSet, i) -> {
            return toLocation(resultSet);
        });
		if(location.size()>0) {
			return location.get(0);
		}else {
			return null;
		}
		
    }
	
	
	public List<LocationModel> LocationList(int start,int noOfRecord,int companyId) {
		StringBuilder sqlQuery = new StringBuilder(String.format("SELECT * FROM acc_location_details where isdeleted=0 and %s=%s",LOCATION_DETAILS_COLUMN.COMPANY_ID,companyId));
		if (noOfRecord > 0) {
			sqlQuery.append(String.format(" LIMIT :%s, :%s ", APPLICATION_GENERIC_ENUM.START.toString(),
					APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString()));
		}

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(APPLICATION_GENERIC_ENUM.START.toString(), start);
		parameters.put(APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString(), noOfRecord);
        List<LocationModel> location = namedParameterJdbcTemplate.query(sqlQuery.toString(),parameters, (resultSet, i) -> {
                    return toLocation(resultSet);
                });

        return location;
    }
	
	
	
	public int deleteLocation(int locationId,int companyId,int deletedBy) {
		
		StringBuilder sqlQueryCompanyUpdate = new StringBuilder();
		sqlQueryCompanyUpdate.append(String.format(" UPDATE %s SET ",DATABASE_TABLE.ACC_LOCATION_DETAILS))
							.append(String.format(" %s = :%s, ", LOCATION_DETAILS_COLUMN.IS_DELETED,LOCATION_DETAILS_COLUMN.IS_DELETED))
							.append(String.format(" %s = :%s, ", LOCATION_DETAILS_COLUMN.DELETED_BY, LOCATION_DETAILS_COLUMN.DELETED_BY))
							.append(String.format(" %s = :%s ", LOCATION_DETAILS_COLUMN.DELETED_DATE_TIME, LOCATION_DETAILS_COLUMN.DELETED_DATE_TIME))
							.append(String.format(" WHERE %s = :%s  ", LOCATION_DETAILS_COLUMN.LOCATION_ID, LOCATION_DETAILS_COLUMN.LOCATION_ID))
							.append(String.format(" AND %s = :%s  ", LOCATION_DETAILS_COLUMN.COMPANY_ID, LOCATION_DETAILS_COLUMN.COMPANY_ID));

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LOCATION_DETAILS_COLUMN.LOCATION_ID.toString(), locationId)
				.addValue(LOCATION_DETAILS_COLUMN.IS_DELETED.toString(), 1)
				.addValue(LOCATION_DETAILS_COLUMN.DELETED_BY.toString(), deletedBy)
				.addValue(LOCATION_DETAILS_COLUMN.COMPANY_ID.toString(), companyId)
				.addValue(LOCATION_DETAILS_COLUMN.DELETED_DATE_TIME.toString(), DateUtility.getCurrentDateTime());

		return namedParameterJdbcTemplate.update(sqlQueryCompanyUpdate.toString(), parameters);
	}
	
	int deleteLocationDocumentData(int locationId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(String.format(" UPDATE %s SET %s = ? ", DATABASE_TABLE.ACC_LOCATION_DOCUMENT_DETIALS, LOCATION_DOCUMENT_COLUMN.IS_DELETED))
				.append(String.format(" WHERE %s = ? ", LOCATION_DOCUMENT_COLUMN.LOCATION_ID, LOCATION_DOCUMENT_COLUMN.LOCATION_ID));

		return namedParameterJdbcTemplate.getJdbcOperations().update(sqlQuery.toString(), new Object[] { 1, locationId });
	}

	 private LocationModel toLocation(ResultSet resultSet) throws SQLException {
		 LocationModel model = new  LocationModel();
		 model.setLocationId(resultSet.getInt("locationId"));
		 model.setLocationName(resultSet.getString("locationName"));
		 model.setAddress(resultSet.getString("address"));
		 model.setGoDownGst(resultSet.getString("goDownGst"));
		 model.setCity(resultSet.getString("city"));
		 model.setEmail(resultSet.getString("email"));
		 model.setLocationIncharge(resultSet.getString("locationIncharge"));
		 model.setPinCode(resultSet.getString("pinCode"));
		 model.setContactNumber(resultSet.getLong("contactNumber"));
		 model.setStateId(resultSet.getInt("stateId"));
		 model.setCompanyId(resultSet.getInt("companyId"));
		 return model;
	    }
	
}
