package acc.webservice.components.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import acc.webservice.enums.DatabaseEnum.DATABASE_TABLE;
import acc.webservice.enums.DatabaseEnum.LOCATION_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@Lazy
@Repository
public class LocationDocumentModelDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	
	@SuppressWarnings("unchecked")
	int[] saveLocationDocumentData(List<LocationDocumentModel> locationDocumentList, int locationId) {

		StringBuilder strQuery = new StringBuilder();

		strQuery.append(String.format("INSERT INTO %s",	DATABASE_TABLE.ACC_LOCATION_DOCUMENT_DETIALS))
				.append(String.format(" ( %s, %s, %s, %s ) ", LOCATION_DOCUMENT_COLUMN.DOCUMENT_TYPE, LOCATION_DOCUMENT_COLUMN.DESCRIPTION, LOCATION_DOCUMENT_COLUMN.DOCUMENT_URL, LOCATION_DOCUMENT_COLUMN.LOCATION_ID))
				.append(" VALUES ")
				.append(String.format(" ( :%s, :%s, :%s, :%s ) ", LOCATION_DOCUMENT_COLUMN.DOCUMENT_TYPE, LOCATION_DOCUMENT_COLUMN.DESCRIPTION, LOCATION_DOCUMENT_COLUMN.DOCUMENT_URL, LOCATION_DOCUMENT_COLUMN.LOCATION_ID));

		List<Map<String, Object>> locationDocumentURLMapList = new ArrayList<>();
		Map<String, Object> locationDocumentURLMap;
		for (LocationDocumentModel locationDocumentModel : locationDocumentList) {
			locationDocumentURLMap = new HashMap<>();

			if (!ApplicationUtility.isNullEmpty(locationDocumentModel.getDocumentUrl())) {			
				locationDocumentModel.setDocumentUrl(locationDocumentModel.getDocumentUrl().replace(ApplicationUtility.getServerURL(), ""));
			}

			locationDocumentURLMap.put(LOCATION_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString(), locationDocumentModel.getDocumentType());
			locationDocumentURLMap.put(LOCATION_DOCUMENT_COLUMN.DESCRIPTION.toString(), locationDocumentModel.getDescription());
			locationDocumentURLMap.put(LOCATION_DOCUMENT_COLUMN.DOCUMENT_URL.toString(), locationDocumentModel.getDocumentUrl());
			locationDocumentURLMap.put(LOCATION_DOCUMENT_COLUMN.LOCATION_ID.toString(), locationId);
			locationDocumentURLMapList.add(locationDocumentURLMap);
		}

		return namedParameterJdbcTemplate.batchUpdate(strQuery.toString(), locationDocumentURLMapList.toArray(new HashMap[0]));
	}
}
