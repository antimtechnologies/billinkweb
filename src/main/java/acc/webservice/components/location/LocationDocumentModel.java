package acc.webservice.components.location;

import java.sql.ResultSet;
import java.sql.SQLException;

import acc.webservice.enums.DatabaseEnum.LOCATION_DOCUMENT_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

public class LocationDocumentModel {

	
	private int documentUrlId;
	private boolean deleted;
	private String documentType;
	private String description;
	private String documentUrl;
	private LocationModel locationModel;
	
	public LocationDocumentModel() {
		
	}
	public LocationDocumentModel(ResultSet rs) throws SQLException {
		setDocumentUrlId(rs.getInt(LOCATION_DOCUMENT_COLUMN.DOCUMENT_URL_ID.toString()));
		setDocumentType(rs.getString(LOCATION_DOCUMENT_COLUMN.DOCUMENT_TYPE.toString()));
		setDescription(rs.getString(LOCATION_DOCUMENT_COLUMN.DESCRIPTION.toString()));
		String documentURL = rs.getString(LOCATION_DOCUMENT_COLUMN.DOCUMENT_URL.toString());
		
		if (!ApplicationUtility.isNullEmpty(documentURL)) {
			setDocumentUrl(ApplicationUtility.getServerURL() + documentURL);
		}
	}
	public int getDocumentUrlId() {
		return documentUrlId;
	}
	public void setDocumentUrlId(int documentUrlId) {
		this.documentUrlId = documentUrlId;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDocumentUrl() {
		return documentUrl;
	}
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}
	public LocationModel getLocationModel() {
		return locationModel;
	}
	public void setLocationModel(LocationModel locationModel) {
		this.locationModel = locationModel;
	}
	
	
	
	
}
