package acc.webservice.components.location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;

public class LocationModel {
	
	private int locationId;
	private String locationName,address,goDownGst,city,pinCode,locationIncharge,email;
	private int companyId , stateId;
	private Long contactNumber;
	
	private List<LocationDocumentModel> locationDocumentList;
	
	
	public LocationModel() {}
	public LocationModel(ResultSet rs) throws SQLException {	
		setLocationId(rs.getInt(LOCATION_DETAILS_COLUMN.LOCATION_ID.toString()));
		
		//setGoDownAddress(rs.getString(LOCATION_DETAILS_COLUMN.GODOWN_ADDRESS.toString()));
		//setGoDownName(rs.getString(LOCATION_DETAILS_COLUMN.GODOWN_NAME.toString()));
		setGoDownGst(rs.getString(LOCATION_DETAILS_COLUMN.GODOWN_GST.toString()));
		setLocationName(rs.getString(LOCATION_DETAILS_COLUMN.LOCATION_NAME.toString()));
		setAddress(rs.getString(LOCATION_DETAILS_COLUMN.ADDRESS.toString()));
		setCity(rs.getString(LOCATION_DETAILS_COLUMN.CITY.toString()));
		setEmail(rs.getString(LOCATION_DETAILS_COLUMN.EMAIL.toString()));
		setPinCode(rs.getString(LOCATION_DETAILS_COLUMN.PINCODE.toString()));
		setLocationIncharge(rs.getString(LOCATION_DETAILS_COLUMN.LOCATION_IN_CHARGE.toString()));
		setContactNumber(rs.getLong(LOCATION_DETAILS_COLUMN.CONTACT_NUMBER.toString()));
	}
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	
	public String getGoDownGst() {
		return goDownGst;
	}
	public void setGoDownGst(String goDownGst) {
		this.goDownGst = goDownGst;
	}
	public List<LocationDocumentModel> getLocationDocumentList() {
		return locationDocumentList;
	}
	public void setLocationDocumentList(List<LocationDocumentModel> locationDocumentList) {
		this.locationDocumentList = locationDocumentList;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getLocationIncharge() {
		return locationIncharge;
	}
	public void setLocationIncharge(String locationIncharge) {
		this.locationIncharge = locationIncharge;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
}
