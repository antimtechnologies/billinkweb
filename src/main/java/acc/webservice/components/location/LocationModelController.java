package acc.webservice.components.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acc.webservice.components.ResponseTemplet.SetResponseTemplet;
import acc.webservice.components.price.PriceModel;
import acc.webservice.components.utills.ResponseListTemplate;
import acc.webservice.enums.ApplicationGenericEnum.APPLICATION_GENERIC_ENUM;
import acc.webservice.enums.DatabaseEnum.COMPANY_TABLE_COLUMN;
import acc.webservice.enums.DatabaseEnum.ITEM_DETAILS_COLUMN;
import acc.webservice.enums.DatabaseEnum.LOCATION_DETAILS_COLUMN;
import acc.webservice.global.utils.ApplicationUtility;

@RestController
@RequestMapping("/locationData/")
public class LocationModelController {

	@Autowired
	LocationModelService locationModelService;
	
	@Autowired
	ResponseListTemplate template;
	
	@Autowired
	SetResponseTemplet setStatus;
	
	@RequestMapping(value="savelocation", method=RequestMethod.POST)
	public Map<String, Object> savelocationData(@RequestBody LocationModel locationModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		response.put("data", locationModelService.saveLocationData(locationModel));
		return response;
	}
	
	
	@RequestMapping(value="checklocationunique", method=RequestMethod.POST)
	public Map<String, Object> chekcklocationUnique(@RequestBody LocationModel locationModel) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "success");
		boolean flag=locationModelService.chekcklocationUnique(locationModel);
		if(flag) {
			response.put("data", "exists");
		}else {
			response.put("data", "None");
		}
		return response;
	}
	
	@RequestMapping(value="getLocationList", method=RequestMethod.POST)
	public Map<String, Object> getLocationList(@RequestBody Map<String, Object> parameter) {
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int start=ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.START.toString());
		int noOfRecord = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.NO_OF_RECORD.toString());
		
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", locationModelService.getLocationList(start,noOfRecord,companyId));
		return responseData;
	}
	
	
	
	@RequestMapping(value="deleteLocation", method=RequestMethod.POST)
	public Map<String, Object> deleteLocation(@RequestBody Map<String, Object> parameter) {
		int locationId = ApplicationUtility.getIntValue(parameter, LOCATION_DETAILS_COLUMN.LOCATION_ID.toString());
		int companyId = ApplicationUtility.getIntValue(parameter, COMPANY_TABLE_COLUMN.COMPANY_ID.toString());
		int deletedBy = ApplicationUtility.getIntValue(parameter, APPLICATION_GENERIC_ENUM.LOGGE_IN_USER_ID.toString());
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("status", "success");
		responseData.put("data", locationModelService.deleteLocation(locationId, companyId, deletedBy));
		return responseData;
	}
	

	@RequestMapping(value="findbycompanyId",method= RequestMethod.GET)
	public ResponseListTemplate findByCustomerType(HttpServletRequest request) {
		if (request.getParameter("companyId") != null) {
			List<LocationModel> list = locationModelService.getLocationByCompanyId(Integer.parseInt(request.getParameter("companyId")));
			
			if (list.size() != 0) {
				template.setError(setStatus.SucessResponse("SuccessFully get!!"));
				template.setResult(list);
				return template;
			} else {
				template.setError(setStatus.FailsResponse("No recored found!!"));
				template.setResult(list);
				return template;
			}
			
		} else {
			template.setError(setStatus.FailsResponse("Please Enter company id"));
			template.setResult(null);
			return template;
		}
	}
}
