package acc.webservice.components.location;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import acc.webservice.components.items.ItemModel;
import acc.webservice.global.utils.ApplicationUtility;

@Service
@Lazy
public class LocationModelService {

	
	@Autowired
	LocationModelDao locationModelDao;
	
	@Autowired
	LocationDocumentModelDao locationDocumentModelDao;
	
	public boolean chekcklocationUnique(LocationModel locationModel) {
		return locationModelDao.chekcklocationUnique(locationModel);
		
		
	}
	
	public LocationModel saveLocationData(LocationModel locationModel) {
		LocationModel model = locationModelDao.saveLocationdata(locationModel);
		if (ApplicationUtility.getSize(locationModel.getLocationDocumentList()) > 0) {
			locationDocumentModelDao.saveLocationDocumentData(locationModel.getLocationDocumentList(), locationModel.getLocationId());
		}
		return model;
	}
	
	public List<LocationModel> getLocationByCompanyId(int companyId) {
		return locationModelDao.findByLocationByCompanyId(companyId);
	}
	
	public LocationModel chekcklocationUniqueInsert(int companyId,LocationModel locationMode) {
		return locationModelDao.chekcklocationUniqueInsert(companyId,locationMode);
	}
	
	public LocationModel getLocationByCompanyLocationId(int companyId,int locationId) {
		return locationModelDao.findByLocationByCompanyLocationId(companyId,locationId);
	}
	
	public List<LocationModel> getLocationList(int start,int noOfRecord,int companyId) {
		return locationModelDao.LocationList(start,noOfRecord,companyId);
	}
	
	
	public int deleteLocation(int locationId,int companyId,int deletedBy) {
		locationModelDao.deleteLocationDocumentData(locationId);
		return locationModelDao.deleteLocation(locationId,companyId,deletedBy);
	}
	
}
