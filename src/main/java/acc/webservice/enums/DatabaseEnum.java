package acc.webservice.enums;

public class DatabaseEnum {

	public enum DATABASE_TABLE {
		ACC_STATE_MASTER("acc_state_master"),
		ACC_COMPANY_DETAILS("acc_company_details"),
		ACC_USER_DETAILS("acc_users_details"),
		ACC_TAX_CODE_DETAILS("acc_taxcode_details"),
		ACC_ROLE_MASTER("acc_roles_master"),
		ACC_USER_COMPANY_MAPPING("acc_user_company_mapping"),
		ACC_USER_COMPANY_MENU_MAPPING("acc_user_company_menu_mapping"),
		ACC_MENU_DETAILS("acc_menus_details"),
		ACC_STATUS_MASTER("acc_status_master"),
		ACC_COMPANY_DOCUMENT_DETAILS("acc_company_document_details"),
		ACC_USER_DOCUMENT_DETAILS("acc_users_document_details"),
		ACC_SCHEME_ONE_DETAILS("acc_scheme_one_details"),
		ACC_SCHEME_TWO_DETAILS("acc_scheme_two_details"),
		ACC_ITEM_BATCH_DETAILS("acc_item_batch_details"),
		ACC_LOCATION_DETAILS("acc_location_details"),
		ACC_ITEM_MRP_DETAILS("acc_item_mrp_details"),
		ACC_LOCATION_DOCUMENT_DETIALS("acc_location_document_details"),
		ACC_LEDGER_DOCUMENT_DETAILS("acc_ledger_document_details"),
		ACC_ITEM_DOCUMENT_DETAILS("acc_item_document_details"),
		ACC_CUSTOMER_TYPE_DETAILS("acc_customer_type_details"),
		ACC_PRICE_MASTER_DETAILS("acc_company_price_master"),
		ACC_ITEM_PURCHASE_RATE_DETAILS("acc_item_purchase_rate_details"),
		ACC_EXCELL_DOCUMENT("acc_excell_document_details"),
		ACC_COMPANY_ITEM_ALIAS_DETAIL("acc_company_item_alias_detail"),
		ACC_BUYERS_DETAILS("acc_buyers_details");
		private String tableName;

		private DATABASE_TABLE(String tableName) {
			this.tableName = tableName;
		}

		@Override
		public String toString() {
			return this.tableName;
		}
	}
	
	public enum ITEM_PURCHASE_RATE_COLUMN {
		PURCHASE_RATE_ID("itemPurchaseRateId"),
		PURCHASE_RATE("itemPurchaseRate"),
		WEF_PURCHASE_RATE_DATE("wefPurchaseRateDate"),
		WET_PURCHASE_RATE_DATE("wetPurachaseRateDate"),
		ITEM_ID("itemId");
		private String columnName;
		private ITEM_PURCHASE_RATE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum ITEM_MRP_COLUMN {
		MRP_ID("itemMrpId"),
		ITEM_MRP("itemMrp"),
		MRP_WEF_DATE("wefDate"),
		MRP_WET_DATE("wetDate"),
		ITEM_ID("itemId");
		private String columnName;
		private ITEM_MRP_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum BUYER_DETAILS_COLUMN {
		BUYER_ID("buyerId"),
		BUYER_NAME("buyerName"),
		ADDRESS("address"),
		CITY("city"),
		PAN("pan"),
		MOBILE_NO("buyerMobileNo"),
		ADHAR_NO("adharNo"),
		STATE_ID("stateId"),
		LEDGER_ID("ledgerId");
		
		private String columnName;
		private BUYER_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum ITEM_BATCH_COLUMN {
		BATCH_ID("batchId"),
		BATCH_NO("batchNo"),
		ITEM_MFG_DATE("mfgDate"),
		ITEM_EXP_DATE("expDate"),
		SHELF_LIFE("shelfLife"),
		ITEM_ID("itemId");

		private String columnName;
		private ITEM_BATCH_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum ITEM_ALIAS_COLUMN {
		ITEM_ALIAS_ID("itemAliasId"),
		ITEM_ALIAS_NAME("itemAliasName"),
		COMPANY_ID("companyId"),
		ITEM_ID("itemId");

		private String columnName;
		private ITEM_ALIAS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum COMPANY_RELATED_TABLE {
		ACC_COMPANY_LEDGER_DETAILS("acc_company_ledgers_details"),
		ACC_COMPANY_BRAND_MASTER("acc_company_brand_master"),
		ACC_COMPANY_BUSINESS_INDUSTRY("acc_company_business_industry"),
		ACC_COMPANY_CATEGORY_MASTER("acc_company_category_master"),
		ACC_COMPANY_ITEM_DETAILS("acc_company_items_details"),
		ACC_COMPANY_PURCHASE_DETAILS("acc_company_purchase_details"),
		ACC_COMPANY_PURCHASE_ITEM_DATA("acc_company_purchase_item_data"),
		ACC_COMPANY_EXPENSE_DETAILS("acc_company_expense_details"),
		ACC_COMPANY_PURCHASE_PAYMENT_DETAILS("acc_company_purchase_payment_details"),
		ACC_COMPANY_PURCHASE_PAYMENT_MAPPING("acc_company_purchase_payment_mapping"),
		ACC_COMPANY_SELL_DETAILS("acc_company_sell_details"),
		ACC_COMPANY_SELL_ITEM_DATA("acc_company_sell_item_data"),
		ACC_COMPANY_SELL_FREIGHT_DATA("acc_company_sell_freight_data"),
		ACC_COMPANY_SELL_PAYMENT_DETAILS("acc_company_sell_payment_details"),
		ACC_COMPANY_SELL_PAYMENT_MAPPING("acc_company_sell_payment_mapping"),
		ACC_COMPANY_CREDIT_NOTE("acc_company_creditnote"),
		ACC_COMPANY_CREDIT_NOTE_ITEM_MAPPING("acc_company_creditnote_item_mapping"),
		ACC_COMPANY_CREDIT_SELL_MAPPING("acc_company_creditnote_sell_mapping"),
		ACC_COMPANY_DEBIT_NOTE("acc_company_debitnote"),
		ACC_COMPANY_DEBIT_NOTE_ITEM_MAPPING("acc_company_debitnote_item_mapping"),
		ACC_COMPANY_DEBIT_PURCHASE_MAPPING("acc_company_debitnote_purchase_mapping"),
		ACC_COMPANY_PURCHASE_IMAGE_URL("acc_company_purchase_image_url"),
		ACC_COMPANY_PURCHASE_PAYMENT_IMAGE_URL("acc_company_purchase_payment_image_url"),
		ACC_COMPANY_EXPENSE_IMAGE_URL("acc_company_expense_image_url"),
		ACC_COMPANY_CREDIT_NOTE_IMAGE_URL("acc_company_creditnote_image_url"),
		ACC_COMPANY_DEBIT_NOTE_IMAGE_URL("acc_company_debitnote_image_url"),
		ACC_COMPANY_SELL_IMAGE_URL("acc_company_sell_image_url"),
		ACC_COMPANY_SELL_PAYMENT_IMAGE_URL("acc_company_sell_payment_image_url"),
		ACC_COMPANY_PURCHASE_CHARGE_DATA("acc_company_purchase_charge_data"),
		ACC_COMPANY_CREDIT_NOTE_CHARGE_DATA("acc_company_creditnote_charge_data"),
		ACC_COMPANY_DEBIT_NOTE_CHARGE_DATA("acc_company_debitnote_charge_data"),
		ACC_COMPANY_OTHER_DOCS("acc_company_other_docs"),
		ACC_COMPANY_OTHER_DOCS_IMAGE_URL("acc_company_other_docs_image_url"),
		ACC_COMPANY_LEDGER_MULTIPLE_GST_DETAILS("acc_company_ledger_multiple_gst_details");

		private String tableName;

		private COMPANY_RELATED_TABLE(String tableName) {
			this.tableName = tableName;
		}

		public String toString() {
			return this.tableName;
		}

	}
	public enum SCHEME_ONE_COLUMN {
		SCHEME_ONE_ID("schemeOneId"),
		SCHEME_TOTAL_MRP_TO("totalInvoiceMrpTo"),
		SCHEME_TOTAL_MRP_FROM("totalInvoiceMrpFrom"),
		SCHEME_MRP_ON_DISCOUNT("totalMrpOnDiscount"),
		SCHEME_COMPANYID("companyid"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deleteddatetime");
		private String columnName;
		private SCHEME_ONE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum SCHEME_TWO_COLUMN {
		SCHEME_TWO_ID("schemeTwoId"),
		TOTAL_QTY_OF_ITEM("totalQtyOfItem"),
		TOTAL_QTY_OF_FREE_ITEM("totalQtyOfFreeItem"),
		ITEM_ID("itemId"),
		FREE_ITEM_ID("freeItemId"),
		SCHEME_COMPANYID("companyid"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deleteddatetime");;
		private String columnName;
		private SCHEME_TWO_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum STATE_TABLE_COLUMN {
		STATE_ID("stateId"),
		STATE_NAME("stateName"),
		IS_DELETED("isDeleted");

		private String columnName;

		private STATE_TABLE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
// for customer type 
	
	public enum CUSTOMER_TYPE_COLUMN {
		CUSTOMER_TYPE_ID("customerTypeId"),
		CUSTOMER_TYPE("customerType"),
		COMPANY_ID("companyId"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deleteddatetime");

		private String columnName;

		private CUSTOMER_TYPE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	// for price  master table  
	
		public enum PRICE_MASTER_COLUMN {
			PRICE_ID("priceId"),
			DISCOUNT("discount"),
			CUSTOMER_TYPE_ID("customerTypeId"),
			COMPANY_ID("companyId"),
			IS_DELETED("isdeleted"),
			DELETED_DATE_TIME("deleteddatetime"),
			DELETED_BY("deletedBy");

			private String columnName;

			private PRICE_MASTER_COLUMN(String columnName) {
				this.columnName = columnName;
			}

			@Override
			public String toString() {
				return this.columnName;
			}
		}
	public enum STATUS_TABLE_COLUMN {
		STATUS_ID("statusId"),
		STATUS_NAME("statusName"),
		IS_DELETED("isDeleted");

		private String columnName;

		private STATUS_TABLE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum COMPANY_TABLE_COLUMN {
		COMPANY_ID("companyId"),
		COMPANY_NAME("companyName"),
		GST_NUMBER("gstNumber"),
		COMPANY_TYPE("companyType"),
		MOBILE_NUMBER("mobileNumber"),
		EMAIL_ID("emailId"),
		LANDLINE_NUMBER("landlineNumber"),
		PAN_NUMBER("panNumber"),
		OFFICE_ADDRESS("officeAddress"),
		PIN_CODE("pinCode"),
		STATE_ID("stateId"),
		IS_DELETED("isDeleted"),
		PROFILE_PHOTO_URL("profilePhotoURL"),
		AADHAR_NUMBER("aadharNumber"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		PURCHASE_PENDING_COUNT("purchasePendingCount"),
		PURCHASE_PROCESS_COUNT("purchaseProcessCount"),
		SELL_PENDING_COUNT("sellPendingCount"),
		SELL_PROCESS_COUNT("sellProcessCount"),
		CREDIT_NOTE_PENDING_COUNT("creditNotePendingCount"),
		CREDIT_NOTE_PROCESS_COUNT("creditNoteProcessCount"),
		DEBIT_NOTE_PENDING_COUNT("debitNotePendingCount"),
		DEBIT_NOTE_PROCESS_COUNT("debitNoteProcessCount"),
		EXPENSE_PENDING_COUNT("expensePendingCount"),
		EXPENSE_PROCESS_COUNT("expenseProcessCount"),
		PURCHASE_PAYMENT_PENDING_COUNT("purchasePaymentPendingCount"),
		PURCHASE_PAYMENT_PROCESS_COUNT("purchasePaymentProcessCount"),
		SELL_PAYMENT_PENDING_COUNT("sellPaymentPendingCount"),
		CITY_NAME("cityName"),
		SELL_PAYMENT_PROCESS_COUNT("sellPaymentProcessCount"),
		BILL_NUMBER_PREFIX("billNumberPrefix"),
		LAST_BILL_NUMBER("lastBillNumber"),
		LAST_ITEM_NUMBER("lastItemNumber"),
		ALLOW_B2B_IMPORT("allowB2BImport"),
		LAST_CREDIT_NOTE_NUMBER("lastCreditNoteNumber"),
		LAST_DEBIT_NOTE_NUMBER("lastDebitNoteNumber"),
		SKU_PREFIX("skuprefix");
		private String columnName;

		private COMPANY_TABLE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum USER_TABLE_COLOUMN {
		USER_ID("userId"),
		USER_NAME("userName"),
		PASSWORD("password"),
		FIRST_NAME("firstName"),
		LAST_NAME("lastName"),
		ROLE_ID("roleId"),
		MOBILE_NUMBER("mobileNumber"),
		EMAIL_ID("emailId"),
		FULL_ADDRESS("fullAddress"),
		PIN_CODE("pincode"),
		STATE_ID("stateId"),
		DESIGNATION("designation"),
		IS_MULTI_LOGGEDIN_ALLOWED("isMultiLoginAllowed"),
		DEFAULT_COMPANY_ID("defaultCompanyId"),
		NO_OF_ALLOWED_COMPANY("noOfAllowedCompany"),
		IS_USER_LOGGE_IN("isUserLoggedIn"),
		IS_DELETED("isDeleted"),
		PROFILE_PHOTO_URL("profilePhotoURL"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		USER_LICENCE_EXPIRY_DATE("userLicenceExpiryDate"),
		DELETED_BY("deletedBy"),
		CITY_NAME("cityName"),
		DELETED_DATE_TIME("deletedDateTime"),
		IS_HIDDEN("isHidden");

		private String columnName;

		private USER_TABLE_COLOUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum ROLE_TABLE_COLUMN {
		ROLE_ID("roleId"),
		ROLE_NAME("roleName"),
		IS_DELETED("isDeleted");

		private String columnName;

		private ROLE_TABLE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum USER_COMPANY_MAPPING_COLUMN {
		COMPANY_ID("companyId"),
		USER_ID("userId"),
		USER_COMPANY_MAPPING_ID("userCompanyMappingId"),
		IS_DELETED("isDeleted");

		private String columnName;

		private USER_COMPANY_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum USER_COMPANY_MENU_MAPPING_COLUMN {
		COMPANY_ID("companyId"),
		USER_ID("userId"),
		MENU_ID("menuId"),
		USER_COMPANY_MENU_MAPPING_ID("userCompanyMenuMappingId"),
		IS_DELETED("isDeleted");

		private String columnName;
		private USER_COMPANY_MENU_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum MENU_DETAILS_COLUMN {
		MENU_ID("menuId"),
		MENU_NAME("menuName"),
		DISPLAY_INDEX("displayIndex"),
		IS_DELETED("isDeleted");

		private String columnName;
		private MENU_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum LEDGER_MULTIPLE_GST_COLUMN {
		MULTIPLE_GST_ID("multipleGstId"),
		MULTIPLE_GST("multipleGst"),
		CITY("city"),
		STATE("state"),
		ADDRESS("address"),
		LEDGER_ID("ledgerId");
		
		private String columnName;
		private LEDGER_MULTIPLE_GST_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	
	public enum ITEM_DETAILS_COLUMN {
		ITEM_ID("itemId"),
		COMPANY_ID("companyId"),
		ITEM_NAME("itemName"),
		BRAND_ID("brandId"),
		CATEGORY_ID("categoryId"),
		HSN_CODE("hsnCode"),
		TAX_CODE("taxCode"),
		CESS("cess"),
		MEASURE_UNIT("measureUnit"),
		QUANTITY("quantity"),
		GST_TYPE("gstType"),
		PURCHASE_RATE("purchaseRate"),
		SELLING_RATE("sellingPrice"),
		PROFILE_PHOTO_URL("profilePhotoURL"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTIme"),
		LAST_MODIFIED_BY("lastModifiedBy"),
		LAST_MODIFIED_DATE_TIME("lastModifiedDateTime"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deleteBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		ITEM_ALIAS("itemAlias"),
		BOM_NAME("bomName"),
		BOM_ALIAS("bomAlias"),
		EXCELL_DOCUMENT_ID("excellDocumentId"),
		ITEM_SKU("sku"),
		ITEM_EAN_NO("eanNo"),
		ITEM_INNER_CONTAINS("innerContains"),
		ITEM_OUTER_CONTAINS("outerContains"),
		ITEM_INNER_WEIGHT_KG("innnerWeightKg"),
		ITEM_OUTER_WEIGHT("outerWeight"),
		ITEM_INNER_BOX_SIZEL("innerBoxSizeL"),
		ITEM_INNER_BOX_SIZEB("innerBoxSizeB"),
		ITEM_INNER_BOX_SIZEH("innerBoxSizeH"),
		ITEM_OUTER_BOX_SIZEL("outerBoxSizeL"),
		ITEM_OUTER_BOX_SIZEB("outerBoxSizeB"),
		ITEM_OUTER_BOX_SIZEH("outerBoxSizeH"),
		CURRENT_ITEM_NUMBER("currentItemNumber"),
		ITEM_PRODUCT_MEASUREMENT("productMeasurement");

		private String columnName;
		private ITEM_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum LOCATION_DETAILS_COLUMN{
		LOCATION_ID("locationId"),
		ADDRESS("address"),
		GODOWN_GST("goDownGst"),
		LOCATION_NAME("locationName"),
		CITY("city"),
		PINCODE("pinCode"),
		LOCATION_IN_CHARGE("locationIncharge"),
		EMAIL("email"),
		CONTACT_NUMBER("contactNumber"),
		STATE_ID("stateId"),
		COMPANY_ID("companyId"),
		IS_DELETED("isdeleted"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deleteddatetime");
		private String columnName;
		private LOCATION_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum LOCATION_DOCUMENT_COLUMN{
		LOCATION_ID("locationId"),
		DOCUMENT_URL_ID("documentUrlId"),
		DOCUMENT_TYPE("doucmentType"),
		DESCRIPTION("descrpition"),
		DOCUMENT_URL("documentUrl"),
		IS_DELETED("isDeleted");

		private String columnName;

		private LOCATION_DOCUMENT_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum ACC_BRAND_MASTER_COLUMN {
		BRAND_ID("brandId"),
		COMPANY_ID("companyId"),
		IS_DELETED("isDeleted"),
		BRAND_NAME("brandName");

		private String columnName;
		private ACC_BRAND_MASTER_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum ACC_CATEGORY_MASTER_COLUMN {
		CATEGORY_ID("categoryId"),
		COMPANY_ID("companyId"),
		IS_DELETED("isDeleted"),
		CATEGORY_NAME("categoryName");

		private String columnName;
		private ACC_CATEGORY_MASTER_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum LEDGER_DETAILS_COLUMN {

		LEDGER_ID("ledgerId"),
		COMPANY_ID("companyId"),
		LEDGER_NAME("ledgerName"),
		MOBILE_NUMBER("mobileNumber"),
		ADDRESS("address"),
		PINCODE("pinCode"),
		SAC_CODE("sacCode"),
		STATE_ID("stateId"),
		CONTANCT_PERSON_NAME("contactPersonName"),
		LEDGER_GROUP("ledgerGroup"),
		GST_TYPE("gstType"),
		GST_NUMBER("gstNumber"),
		OPENING_BALANCE("openingBalance"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		LAST_MODIFIED_BY("lastModifyBy"),
		LAST_MODIFIED_DATE_TIME("lastModifyDateTime"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		TAX_CODE("taxCode"),
		LEDGER_CHARGE("ledgerCharge"),
		CITY_NAME("cityName"),
		IS_DELETED("isDeleted"), 
		PAN_NUMBER("panNumber"),
		CREDIT_PERIOD("creditPeriod"),
		CREDIT_LIMIT("creditLimit"),
		LEDGER_CODE("LedgerCode"),
		CUSTOMER_TYPE_ID("customerTypeId"),
		PRICE_ID("priceId"),
		SCHEME_ONE_ID("schemeOneId"),
		SCHEME_TWO_ID("schemeTwoId"),
		CURRENT_LEDGER_NUMBER("currentLedgerNumber"),
		EXCELL_DOCUMENT_ID("excellDocumentId"),
		PeriodName("periodName");

		private String columnName;
		private LEDGER_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum EXPENSE_DETAILS_COLUMN {

		EXPENSE_ID("expenseId"),
		COMPANY_ID("companyId"),
		LEDGER_ID("ledgerId"),
		EXPENSE_LEDGER_ID("expenseLedgerId"),
		IMAGE_URL("imageURL"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		EXPENSE_INVOICE_DATE("expenseInvoiceDate"),
		EXPENSE_AMOUNT("expenseAmount"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		DELETED_BY("deletedBy"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		DELETED_DATE_TIME("deletedDateTime"),
		IS_DELETED("isDeleted");

		private String columnName;

		private EXPENSE_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_DETAILS_COLUMN {

		PURCHASE_ID("purchaseId"),
		COMPANY_ID("companyId"),
		IMAGE_URL("imageURL"),
		BILL_NUMBER("billNumber"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		PURCHASE_DATE("purchaseDate"),
		LEDGER_ID("ledgerId"),
		TOTAL_BILL_AMOUNT("totalBillAmount"),
		PURCHASE_MEASURE_DISCOUNT_IN_AMONT("purchaseMeasureDiscountInAmount"),
		PAYMENT_STATUS("paymentStatus"),
		DISCOUNT("discount"),
		GROSS_TOTAL_AMOUNT("grossTotalAmount"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		BILL_DATE("billDate"),
		P_O_NUMBER("poNumber"),
		L_R_NUMBER("lrNumber"),
		CITY_NAME("cityName"),
		GST_NUMBER("gstNumber"),
		E_WAY_BILL_NUMBER("ewayBillNumber"),
		IS_DELETED("isDeleted");

		private String columnName;

		private PURCHASE_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_ITEM_DATA_COLUMN {

		PURCHASE_ITEM_MAPPING_ID("purchaseItemMappingId"),
		COMPANY_ID("companyId"),
		ITEM_ID("itemId"),
		QUANTITY("quantity"),
		PURCHASE_RATE("purchaseRate"),
		TAX_RATE("taxRate"),
		ITEM_AMOUNT("itemAmount"),
		C_GST("cGST"),
		S_GST("sGST"),
		I_GST("iGST"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		ITEM_DISCOUNT("itemDiscount"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		PURCHASE_ID("purchaseId"),
		LOCTION_ID("locationId"),
		BATCH_ID("batchId"),
		FREE_ITEM_QUANTITY("freequantity"),
		FREE_ITEM_ID("freeItemId"),
		IS_DELETED("isDeleted"),
		ITEM_ADDITIONAL_DISCOUNT("additionalitemDiscount"),
		ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT("addmeasureDiscountInAmount");

		private String columnName;

		private PURCHASE_ITEM_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	//13-05-2019
	public enum LEDGER_PURCHASE_DETAILS {

		PURCHASE_ID("purchaseId"),
		PURCHASE_DATE("purchaseDate"),
		BILL_NUMBER("billNumber"),
		ITEM_ID("itemId"),
		ITEM_NAME("itemName"),
		QUANTITY("quantity"),
		BATCH_NO("batchNo"),
		MFG_DATE("mfgDate"),
		EXP_DATE("expDate");
		
		private String columnName;

		private LEDGER_PURCHASE_DETAILS(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum LEDGER_SELL_DETAILS {

		SELL_ID("sellId"),
		SELL_DATE("sellBillDate"),
		BILL_NUMBER("billNo"),
		ITEM_ID("itemId"),
		ITEM_NAME("itemName"),
		QUANTITY("quantity"),
		BATCH_NO("batchNo"),
		MFG_DATE("mfgDate"),
		EXP_DATE("expDate");
		
		private String columnName;

		private LEDGER_SELL_DETAILS(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	//
	
	
	
	public enum PURCHASE_PAYMENT_DETAILS {

		PURCHASE_PAYMENT_ID("purchasePaymentId"),
		COMPANY_ID("companyId"),
		IMAGE_URL("imageURL"),
		AMOUNT("amount"),
		LEDGER_ID("ledgerId"),
		PAYMENT_FROM_LEDGER("paymentFromLedger"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		PAYMENT_DATE("paymentDate"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deletedBy"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		DELETED_DATE_TIME("deletedDateTime");

		private String columnName;

		private PURCHASE_PAYMENT_DETAILS(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_PAYMENT_MAPPING_COLUMN {

		PURCHASE_PAYMENT_MAPPING_ID("purchasePaymenMappingId"),
		COMPANY_ID("companyId"),
		PURCHASE_PAYMENT_ID("purchasePaymentId"),
		PURCHASE_ID("purchaseId"),
		IS_PAYMENT_DONE("isPaymentDone"),
		IS_DELETED("isDeleted");

		private String columnName;

		private PURCHASE_PAYMENT_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum SELL_DETAILS_COLUMN {

		SELL_ID("sellId"),
		COMPANY_ID("companyId"),
		IMAGE_URL("imageURL"),
		BILL_NUMBER("billNo"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		SELL_BILL_DATE("sellBillDate"),
		LEDGER_ID("ledgerId"),
		TOTAL_BILL_AMOUNT("totalBillAmount"),
		PAYMENT_STATUS("paymentStatus"),
		SELL_MEASURE_DISCOUNT_IN_AMONT("sellMeasureDiscountInAmount"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		DISCOUNT("discount"),
		FREIGHT_CHARGE("freightCharge"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		IS_DELETED("isDeleted"), 
		GROSS_TOTAL_AMOUNT("grossTotalAmount"), 
		BILL_NUMBER_PREFIX("billNumberPrefix"),
		CURRENT_BILL_NUMBER("currentBillNumber"),
		BUYER_LEDGER_ID("buyerLedgerId"),
		EXCELL_DOCUMENT_ID("excellDocumentId"),
		ORDER_ID("orderId"),
		SUB_ORDER_ID("subOrderId"),
		VOUCHER_TYPE("voucherType"),
		DISPATCH_DATE("dispatchDate"),
		PO_NO("poNo"),
		LR_NO("lrNo"),
		CITY_NAME("cityName"),
		GST_NUMBER("gstNumber"),
		E_WAY_BILL_NO("eWayBillNo"),
		ACTUAL_WEIGHT("actualWeight");

		private String columnName;

		private SELL_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum SELL_ITEM_DATA_COLUMN {

		SELL_ITEM_MAPPING_ID("sellItemMappingId"),
		COMPANY_ID("companyId"),
		ITEM_ID("itemId"),
		QUANTITY("quantity"),
		SELL_RATE("sellRate"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		ITEM_AMOUNT("itemAmount"),
		C_GST("cGST"),
		S_GST("sGST"),
		I_GST("iGST"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		SELL_ID("sellId"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		IS_DELETED("isDeleted"),
		LOCTION_ID("locationId"),
		BATCH_ID("batchId"),
		EXCELL_DOCUMENT_ID("excellDocumentId"),
		FREE_ITEM_ID("freeItemId"),
		FREE_ITEM_QTY("freeItemQty"),
		ITEM_ADDITIONAL_DISCOUNT("additionaldiscount"),
		ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT("addmeasureDiscountInAmount");;
		
		private String columnName;

		private SELL_ITEM_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum SELL_FREIGHT_DATA_COLUMN {

		SELL_FREIGHT_MAPPING_ID("sellFreightMappingId"),
		COMPANY_ID("companyId"),
		FREIGHT_CHARGE("freightCharge"),
		SELL_ID("sellId"),
		LEDGER_ID("ledgerId"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		ITEM_AMOUNT("itemAmount"),
		I_GST("iGST"),
		S_GST("sGST"),
		C_GST("cGST"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		IS_DELETED("isDeleted"),
		EXCELL_DOCUMENT_ID("excellDocumentId");

		private String columnName;

		private SELL_FREIGHT_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum DEBIT_NOTE_CHARGE_DATA_COLUMN {

		DEBIT_NOTE_CHARGE_MAPPING_ID("debitnoteChargeMappingId"),
		COMPANY_ID("companyId"),
		DEBIT_NOTE_CHARGE("debitnoteCharge"),
		DEBIT_NOTE_ID("debitnoteId"),
		LEDGER_ID("ledgerId"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		ITEM_AMOUNT("itemAmount"),
		I_GST("iGST"),
		S_GST("sGST"),
		C_GST("cGST"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		IS_DELETED("isDeleted");

		private String columnName;

		private DEBIT_NOTE_CHARGE_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum CREDIT_NOTE_CHARGE_DATA_COLUMN {

		CREDIT_NOTE_CHARGE_MAPPING_ID("creditnoteChargeMappingId"),
		COMPANY_ID("companyId"),
		CREDIT_NOTE_CHARGE("creditnoteCharge"),
		CREDIT_NOTE_ID("creditnoteId"),
		LEDGER_ID("ledgerId"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		ITEM_AMOUNT("itemAmount"),
		I_GST("iGST"),
		S_GST("sGST"),
		C_GST("cGST"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		IS_DELETED("isDeleted");

		private String columnName;

		private CREDIT_NOTE_CHARGE_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_CHARGE_DATA_COLUMN {

		PURCHASE_CHARGE_MAPPING_ID("purchaseChargeMappingId"),
		COMPANY_ID("companyId"),
		PURCHASE_CHARGE("purchaseCharge"),
		PURCHASE_ID("purchaseId"),
		LEDGER_ID("ledgerId"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		ITEM_AMOUNT("itemAmount"),
		I_GST("iGST"),
		S_GST("sGST"),
		C_GST("cGST"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		IS_DELETED("isDeleted");

		private String columnName;

		private PURCHASE_CHARGE_DATA_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum SELL_PAYMENT_DETAILS_COLUMN {

		SELL_PAYMENT_ID("sellPaymentId"),
		COMPANY_ID("companyId"),
		IMAGE_URL("imageURL"),
		AMOUNT("amount"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		PAYMENT_DATE("paymentDate"),
		LEDGER_ID("ledgerId"),
		RECEIPT_IN_LEDGER("receiptInLedger"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		IS_DELETED("isDeleted"),
		DELETED_BY("deletedBy"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		DELETED_DATE_TIME("deletedDateTime");

		private String columnName;

		private SELL_PAYMENT_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum SELL_PAYMENT_MAPPING_COLUMN {

		SELL_PAYMENT_MAPPING_ID("sellPaymentMappingId"),
		COMPANY_ID("companyId"),
		SELL_PAYMENT_ID("sellPaymentId"),
		SELL_ID("sellId"),
		IS_PAYMENT_DONE("isPaymentDone"),
		IS_DELETED("isDeleted");

		private String columnName;

		private SELL_PAYMENT_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum CREDIT_NOTE_DETAILS_COLUMN {

		CREDIT_NOTE_ID("creditNoteId"),
		COMPANY_ID("companyId"),
		LEDGER_ID("ledgerId"),
		IMAGE_URL("imageURL"),
		AMOUNT("amount"),
		GROSS_TOTAL_AMOUNT("grossTotalAmount"),
		BILL_NUMBER("billNumber"),
		CREDIT_NOTE_DATE("creditNoteDate"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		DISCOUNT("discount"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		SELL_MEASURE_DISCOUNT_IN_AMOUNT("sellMeasureDiscountInAmount"),
		IS_DELETED("isDeleted"),
		BILL_DATE("billDate"),
		P_O_NUMBER("poNo"),
		L_R_NUMBER("lrNo"),
		CITY_NAME("cityName"),
		GST_NUMBER("gstNumber"),
		E_WAY_BILL_NUMBER("eWayBillNo"),
		ORIGINAL_INVOICE_NUMBER("originalInvoiceNumber"),
		ORIGINAL_INVOICE_DATE("originalInvoiceDate"),
		BILL_NUMBER_PREFIX("billNumberPrefix"),
		CURRENT_BILL_NUMBER("currentBillNumber");

		private String columnName;

		private CREDIT_NOTE_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum CREDIT_NOTE_ITEM_MAPPING_COLUMN {

		CREDIT_NOTE_ITEM_MAPPING_ID("creditNoteItemMappingId"),
		COMPANY_ID("companyId"),
		CREDIT_NOTE_ID("creditNotesId"),
		ITEM_ID("itemId"),
		QUANTITY("quantity"),
		SELL_RATE("sellRate"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		ITEM_AMOUNT("itemAmount"),
		C_GST("cGST"),
		S_GST("sGST"),
		I_GST("iGST"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		LOCTION_ID("locationId"),
		BATCH_ID("batchId"),
		FREE_ITEM_QUANTITY("freequantity"),
		FREE_ITEM_ID("freeItemId"),
		ITEM_ADDITIONAL_DISCOUNT("additionaldiscount"),
		ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT("addmeasureDiscountInAmount"),
		IS_DELETED("isDeleted");

		private String columnName;

		private CREDIT_NOTE_ITEM_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum CREDIT_NOTE_SELL_MAPPING_COLUMN {

		CREDIT_NOTE_SELL_MAPPING_ID("creditNoteSellMappingId"),
		COMPANY_ID("companyId"),
		CREDIT_NOTE_ID("creditNoteId"),
		SELL_ID("sellId"),
		IS_DELETED("isDeleted");

		private String columnName;

		private CREDIT_NOTE_SELL_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum DEBIT_NOTE_DETAILS_COLUMN {

		DEBIT_NOTE_ID("debitNoteId"),
		COMPANY_ID("companyId"),
		LEDGER_ID("ledgerId"),
		IMAGE_URL("imageURL"),
		AMOUNT("amount"),
		GROSS_TOTAL_AMOUNT("grossTotalAmount"),
		BILL_NUMBER("billNumber"),
		DEBIT_NOTE_DATE("debitNoteDate"),
		DESCRIPTION("description"),
		STATUS_ID("statusId"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		MODIFIED_BY("modifiedBy"),
		MODIFIED_DATE_TIME("modifiedDateTime"),
		VERIFIED_BY("verifiedBy"),
		VERIFIED_DATE_TIME("verifiedDateTime"),
		IS_VERIFIED("isVerified"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		DISCOUNT("discount"),
		REFERENCE_NUBER_1("referenceNumber1"),
		REFERENCE_NUBER_2("referenceNumber2"),
		P_O_NUMBER("poNumber"),
		L_R_NUMBER("lrNumber"),
		CITY_NAME("cityName"),
		GST_NUMBER("gstNumber"),
		E_WAY_BILL_NUMBER("ewayBillNumber"),
		ORIGINAL_INVOICE_NUMBER("originalInvoiceNumber"),
		ORIGINAL_INVOICE_DATE("originalInvoiceDate"),
		PURCHASE_MEASURE_DISCOUNT_IN_AMOUNT("purchaseMeasureDiscountInAmount"),
		IS_DELETED("isDeleted"),
		BILL_NUMBER_PREFIX("billNumberPrefix"),
		CURRENT_BILL_NUMBER("currentBillNumber");
		

		private String columnName;

		private DEBIT_NOTE_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum DEBIT_NOTE_ITEM_MAPPING_COLUMN {

		DEBIT_NOTE_ITEM_MAPPING_ID("debitNoteItemMappingId"),
		COMPANY_ID("companyId"),
		DEBIT_NOTE_ID("debitNoteId"),
		ITEM_ID("itemId"),
		QUANTITY("quantity"),
		PURCHASE_RATE("purchaseRate"),
		ITEM_DISCOUNT("itemDiscount"),
		TAX_RATE("taxRate"),
		ITEM_AMOUNT("itemAmount"),
		C_GST("cGST"),
		S_GST("sGST"),
		I_GST("iGST"),
		TOTAL_ITEM_AMOUNT("totalItemAmount"),
		MEASURE_DISCOUNT_IN_AMOUNT("measureDiscountInAmount"),
		LOCTION_ID("locationId"),
		BATCH_ID("batchId"),
		FREE_ITEM_QUANTITY("freequantity"),
		FREE_ITEM_ID("freeItemId"),
		ITEM_ADDITIONAL_DISCOUNT("additionaldiscount"),
		ADDITIONAL_MEASURE_DISCOUNT_IN_AMOUNT("addmeasureDiscountInAmount"),
		IS_DELETED("isDeleted");

		private String columnName;

		private DEBIT_NOTE_ITEM_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum DEBIT_NOTE_PURCHASE_MAPPING_COLUMN {

		DEBIT_NOTE_PURCHASE_MAPPING_ID("debitNotePurchaseMappingId"),
		COMPANY_ID("companyId"),
		DEBIT_NOTE_ID("debitNoteId"),
		PURCHASE_ID("purchaseId"),
		IS_DELETED("isDeleted");

		private String columnName;

		private DEBIT_NOTE_PURCHASE_MAPPING_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum EXPENSE_IMAGE_URL_COLUMN {
		EXPENSE_ID("expenseId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private EXPENSE_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_IMAGE_URL_COLUMN {
		PURCHASE_ID("purchaseId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private PURCHASE_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum SELL_IMAGE_URL_COLUMN {
		SELL_ID("sellId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private SELL_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum CREDIT_NOTE_IMAGE_URL_COLUMN {
		CREDIT_NOTE_ID("creditNoteId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private CREDIT_NOTE_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum DEBIT_NOTE_IMAGE_URL_COLUMN {
		DEBIT_NOTE_ID("debitNoteId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private DEBIT_NOTE_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum SELL_PAYMENT_IMAGE_URL_COLUMN {
		SELL_PAYMENT_ID("sellPaymentId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private SELL_PAYMENT_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum PURCHASE_PAYMENT_IMAGE_URL_COLUMN {
		PURCHASE_PAYMENT_ID("purchasePaymentId"),
		COMPANY_ID("companyId"),
		IMAGE_ID("imageId"),
		IMAGE_URL("imageURL"),
		IS_DELETED("isDeleted");

		private String columnName;

		private PURCHASE_PAYMENT_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum COMPANY_DOCUMENT_COLUMN {

		COMPANY_ID("companyId"),
		DOCUMENT_URL_ID("documentUrlId"),
		DOCUMENT_TYPE("doucmentType"),
		DESCRIPTION("descrpition"),
		DOCUMENT_URL("documentUrl"),
		IS_DELETED("isDeleted");

		private String columnName;

		private COMPANY_DOCUMENT_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}


	public enum USER_DOCUMENT_COLUMN {

		USER_ID("userId"),
		DOCUMENT_URL_ID("documentUrlId"),
		DOCUMENT_TYPE("doucmentType"),
		DESCRIPTION("descrpition"),
		DOCUMENT_URL("documentUrl"),
		IS_DELETED("isDeleted");

		private String columnName;

		private USER_DOCUMENT_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}


	public enum LEDGER_DOCUMENT_COLUMN {

		LEDGER_ID("ledgerId"),
		DOCUMENT_URL_ID("documentUrlId"),
		DOCUMENT_TYPE("doucmentType"),
		DESCRIPTION("descrpition"),
		DOCUMENT_URL("documentUrl"),
		IS_DELETED("isDeleted");

		private String columnName;

		private LEDGER_DOCUMENT_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum ITEM_DOCUMENT_COLUMN {

		ITEM_ID("itemId"),
		DOCUMENT_URL_ID("documentUrlId"),
		DOCUMENT_TYPE("doucmentType"),
		DESCRIPTION("descrpition"),
		DOCUMENT_URL("documentUrl"),
		IS_DELETED("isDeleted");

		private String columnName;

		private ITEM_DOCUMENT_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum TAX_CODE_COLUMN {

		TAX_CODE_ID("taxCodeId"),
		TAX_CODE("taxCode"),
		IS_DELETED("isDeleted");

		private String columnName;

		private TAX_CODE_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum BUSINESS_INDUSTRY_COLUMN {
		BUSINESS_INDUSTRY_ID("businessIndustryId"),
		BUSINESS_INDUSTRY("businessIndustry"),
		COMPANY_ID("companyId"),
		IS_DELETED("isDeleted");

		private String columnName;

		private BUSINESS_INDUSTRY_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}

	public enum OTHER_DOCS_COLUMN {
		OTHER_DOCS_ID("otherDocsId"),
		IMAGE_URL("imageURL"),
		DESCRPITION("description"),
		ADDED_BY("addedBy"),
		ADDED_DATE_TIME("addedDateTime"),
		DELETED_BY("deletedBy"),
		DELETED_DATE_TIME("deletedDateTime"),
		IS_DELETED("isDeleted"),
		COMPANY_ID("companyId");
		
		private String columnName;

		private OTHER_DOCS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	public enum OTHER_DOCS_IMAGE_URL_COLUMN {
		OTHER_DOCS_ID("otherDocsId"),
		IMAGE_URL("imageURL"),
		IMAGE_URL_ID("imageURLId"),
		IS_DELETED("isDeleted");

		private String columnName;

		private OTHER_DOCS_IMAGE_URL_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
	public enum ACC_EXCEL_DOCUMENT {
		DOCUMENT_ID("documentId"),
		COMPANY_ID("companyId"),
		DOCUMENT_URL("documentURL"),
		DESCRIPTION("description"),
		ERROR_DOCUMENT_URL("errorDocumentURL"),
		DOCUMENT_TYPE("documentType"),
		DOCUMENT_STATUS("documentStatus"),
		ADDED_BY("addedBy"),
		DELETED_BY("deletedBy"),
		LASTRUN_BY("lastRunBy"),
		ADDED_DATE_TIME("addedDateTime"),
		LAST_RUN_DATE_TIME("lastRunDateTime"),
		DELETED_DATE_TIME("deletedDateTime"),
		IS_DELETED("isDeleted");

		private String columnName;
		private ACC_EXCEL_DOCUMENT(String columnName) {
			this.columnName = columnName;
		}
		@Override
		public String toString() {
			return this.columnName;
		}
	}
	
	
	public enum DEBITNOTE_ITEM_DETAILS_COLUMN {

		PURCHASE_ID("purchaseId"),
		ITEM_ID("itemId"),
		ITEM_NAME("itemName"),
		QUNTITY("quantity"),
		PURCHASE_RATE("purchaseRate"),
		TAX_RATE("taxRate");

		private String columnName;

		private DEBITNOTE_ITEM_DETAILS_COLUMN(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return this.columnName;
		}
	}
}
