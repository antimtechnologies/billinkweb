package acc.webservice.enums;

public enum ROLES_TEXT {

	ADMIN_ROLE("Admin"),
	EMPLOYEE_ROLE("Employee"),
	VENDOR_ROLE("Vendor");

	private String roleName;
	private ROLES_TEXT(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return this.roleName;
	}
}
