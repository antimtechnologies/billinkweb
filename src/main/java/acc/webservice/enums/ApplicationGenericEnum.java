package acc.webservice.enums;

public class ApplicationGenericEnum {

	public static final String PORT_NUMBER = "8080";

	public enum APPLICATION_GENERIC_ENUM {
		ADDED_BY_ID("addedById"),
		ADDED_BY_NAME("addedByName"),
		MODIFIED_BY_ID("modifiedById"),
		MODIFIED_BY_NAME("modifiedByName"),
		VERIFIED_BY_ID("verifiedById"),
		VERIFIED_BY_NAME("verifiedByName"),
		DELETED_BY_ID("deletedById"),
		DELETED_BY_NAME("deletedByName"), 
		LOGGE_IN_USER_ID("loggedInUserId"),
		START("start"),
		NO_OF_RECORD("noOfRecord"), 
		TOTAL_COUNT("totalCount"),
		SORT_PARAM("sortParam"),
		TOTAL_PENDING_COUNT("totalPendingCount"),
		COMPANY_PROFILE_PHOTO_DEF_URL("/resources/images/company/no-image-photo.jpg"), 
		ITEM_PROFILE_PHOTO_DEF_URL("/resources/images/item/no-image-photo.jpg"), 
		USER_PROFILE_PHOTO_DEF_URL("/resources/images/user/no-image-photo.jpg");
		private String stringName;

		private APPLICATION_GENERIC_ENUM(String stringName) {
			this.stringName = stringName;
		}

		@Override
		public String toString() {
			return this.stringName;
		}
	}

	public enum PAYMENT_STATUS_ENUM {

		PENDING_PAYMENT_STATUS(0),
		PARTIAL_PAYMENT_STATUS(1),
		COMPLETE_PAYMENT_STATUS(2);

		private int paymentStatus;

		private PAYMENT_STATUS_ENUM(int paymentStatus) {
			this.paymentStatus = paymentStatus;
		}

		public int status() {
			return this.paymentStatus;
		}

	}
	
	public enum EXCELL_DOCUMENT_STATUS_ENUM {

		PENDING_STATUS("Pending"),
		COMPLETE_STATUS("Complete"),
		ERROR_STATUS("Error");

		private String status;

		private EXCELL_DOCUMENT_STATUS_ENUM(String status) {
			this.status = status;
		}

		public String status() {
			return this.status;
		}

	}

}
