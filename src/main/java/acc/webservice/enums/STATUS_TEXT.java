package acc.webservice.enums;

public enum STATUS_TEXT {
	NEW("New"),
	MODIFIED("Modified"),
	DELETED("Deleted"),
	UNPUBLISHED("UnPublished");

	private String statusName;
	private STATUS_TEXT(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public String toString() {
		return this.statusName;
	}
}
