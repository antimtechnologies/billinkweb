package acc.webservice.global.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtility {

	private DateUtility() { }

	public static final String DEFAULT_DATA_BASE_TIME_ZONE = "IST";
	public static final String DEFAULT_DATA_BASE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_USER_DATE_TIME_FORMAT = "MMM dd, yyyy HH:mm";
	public static final String DEFAULT_USER_DATE_FORMAT = "dd-MM-yyyy";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

	public static String converDateToDataBaseString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATA_BASE_DATE_FORMAT);
		return sdf.format(date);
	}

	public static String converDateToUserString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_USER_DATE_TIME_FORMAT);
		return sdf.format(date);
	}

	public static String converDateToUserString(Date date, String strFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
		return sdf.format(date);
	}

	public static String getCurrentDateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATA_BASE_DATE_FORMAT);
		Date date = new Date();
		sdf.setTimeZone(TimeZone.getTimeZone(DEFAULT_DATA_BASE_TIME_ZONE));
		return sdf.format(date);
	}

	public static long getCurrentDateTimeInLong() {
		return new Date().getTime();
	}

	public static String convertDateFormat(String strDate, String sourceFormat, String destinationFormat) {
		SimpleDateFormat sourceFormatter = new SimpleDateFormat(sourceFormat);
		SimpleDateFormat destinationFormatter = new SimpleDateFormat(destinationFormat);

		try {
			Date date = sourceFormatter.parse(strDate);
			return destinationFormatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String convertDateIntoFormat(String strDate, String sourceFormat, String destinationFormat) throws ParseException {
		SimpleDateFormat sourceFormatter = new SimpleDateFormat(sourceFormat);
		SimpleDateFormat destinationFormatter = new SimpleDateFormat(destinationFormat);
		Date date = sourceFormatter.parse(strDate);
		return destinationFormatter.format(date);
	}
}
