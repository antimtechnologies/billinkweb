package acc.webservice.global.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Map;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import acc.webservice.enums.ApplicationGenericEnum;

public class ApplicationUtility {

	public static final boolean IS_SERVER_ENV = false;
	public static final String SYSTEM_USER_NAME = "System_User";

	public static boolean isNullEmpty(Map<String, Object> obj) {
		if (obj == null) {
			return true;
		}
		return obj.isEmpty();
	}

	public static boolean isNullEmpty(String strValue) {
		if (strValue == null) {
			return true;
		}
		return "".equals(strValue.trim()) || strValue.trim().length() == 0;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static String roundToFixValue(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		double value1 = (double) tmp / factor;
		DecimalFormat df = new DecimalFormat("#0.00");
		return df.format(value1);

	}

	public static boolean isDateInGivenFormat(String format, String value) {

		if (getSize(value) < 1) {
			return false;
		}

		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		if (date == null) {
			return false;
		}

		return true;
	}

	public static int getIntValue(Map<String, Object> parameter, String key) {
		return getIntValue(parameter, key, 0);
	}

	public static int getIntValue(Map<String, Object> parameter, String key, int defaultValue) {

		if (isNullEmpty(parameter)) {
			return defaultValue;
		}

		Object value = parameter.get(key);
		if (value instanceof String) {
			return getIntData(value, defaultValue);
		} else if (value instanceof Integer) {
			return (Integer) value;
		}
		return defaultValue;
	}

	private static int getIntData(Object value, int defaultValue) {
		String strValue = (String) value;
		try {
			return Integer.parseInt(strValue);
		} catch (RuntimeException e) {
		}
		return defaultValue;
	}

	public static String getStrValue(Map<String, Object> parameter, String key) {
		return getStrValue(parameter, key, "");
	}

	public static String getStrValue(Map<String, Object> parameter, String key, String defaultValue) {
		if (isNullEmpty(parameter)) {
			return defaultValue;
		}

		String value = String.valueOf(parameter.get(key));
		if (value == null || "null".equalsIgnoreCase(value)) {
			return defaultValue;
		}

		return value;
	}

	@SuppressWarnings("rawtypes")
	public static int getSize(Object obj) {
		if (obj == null) {
			return 0;
		}

		if (obj instanceof Collection) {
			return ((Collection) obj).size();
		}

		if (obj instanceof Map) {
			return ((Map) obj).size();
		}

		if (obj instanceof CharSequence) {
			return ((CharSequence) obj).length();
		}

		return 0;
	}

	public static String getServerIPAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "";
		}
	}

	public static String getServerURL() {
		if (!IS_SERVER_ENV) {
			return "http://" + getServerIPAddress() + ":" + ApplicationGenericEnum.PORT_NUMBER;
		} else {
			return "http://billink.in";
		}

	}

}
