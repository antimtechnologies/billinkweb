package acc.webservice.global.exception.exceptionenum;

public interface IAccountingSoftwareExceptionInterface {

	public int getErrorCode();
	public String getErrorMessage();

}
