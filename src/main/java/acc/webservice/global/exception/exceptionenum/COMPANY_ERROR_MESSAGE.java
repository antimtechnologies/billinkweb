package acc.webservice.global.exception.exceptionenum;

public enum COMPANY_ERROR_MESSAGE implements IAccountingSoftwareExceptionInterface {
	ERROR_SAVINGS_COMPANY_DATA(-1001, "Error while saving company data");

	private int errorCode;
	private String errorMessage;

	@Override
	public int getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String getErrorMessage() {
		return this.errorMessage;
	}

	COMPANY_ERROR_MESSAGE(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
