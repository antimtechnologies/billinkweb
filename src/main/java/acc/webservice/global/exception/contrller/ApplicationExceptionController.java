package acc.webservice.global.exception.contrller;

import java.security.InvalidParameterException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import acc.webservice.global.exception.model.AccountingSofwareException;
import acc.webservice.global.exception.model.AuthenticationException;
import acc.webservice.global.exception.model.ErrorInfo;

@ControllerAdvice
public class ApplicationExceptionController {

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ErrorInfo notFoundException(final Exception e) {
		e.printStackTrace(System.out);
		return new ErrorInfo("Error while performing operation. contact admin for further assistance.", HttpStatus.INTERNAL_SERVER_ERROR.value(), "error");
	}

	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(InvalidParameterException.class)
	@ResponseBody
	public ErrorInfo invalidParametedException(final Exception e) {
		e.printStackTrace(System.out);
		return new ErrorInfo(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY.value(), "error");
	}
	
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(AccountingSofwareException.class)
	@ResponseBody
	public ErrorInfo accountingSofwareException(final Exception e) {
		e.printStackTrace(System.out);
		return new ErrorInfo(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY.value(), "error");
	}
	
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	@ExceptionHandler(AuthenticationException.class)
	@ResponseBody
	public ErrorInfo authenticationException(final Exception e) {
		e.printStackTrace(System.out);
		return new ErrorInfo(e.getMessage(), HttpStatus.FORBIDDEN.value(), "error");
	}

}
