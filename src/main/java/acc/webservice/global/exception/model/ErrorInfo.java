package acc.webservice.global.exception.model;

public class ErrorInfo {

	private String errMsg;
	private int errorCode;
	private String status;

	public ErrorInfo() {

	}

	public ErrorInfo(String errMsg, int errorCode, String status) {
		this.errMsg = errMsg;
		this.errorCode = errorCode;
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
