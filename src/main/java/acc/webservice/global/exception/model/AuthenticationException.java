package acc.webservice.global.exception.model;

public class AuthenticationException  extends Exception {

	private static final long serialVersionUID = 222323232323232323L;

	
	public AuthenticationException(String errorMessage, Throwable e) {
		super(errorMessage, e);
	}

	public AuthenticationException(String errorMessage) {
		super(errorMessage);
	}
	
}
