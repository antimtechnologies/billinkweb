package acc.webservice.global.exception.model;

import acc.webservice.global.exception.exceptionenum.IAccountingSoftwareExceptionInterface;

public class AccountingSofwareException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8888888888888888L;

	public AccountingSofwareException(IAccountingSoftwareExceptionInterface accountingSoftwareExceptionInterface) {
		super(accountingSoftwareExceptionInterface.getErrorMessage());
	}

	public AccountingSofwareException(IAccountingSoftwareExceptionInterface accountingSoftwareExceptionInterface, Throwable e) {
		super(accountingSoftwareExceptionInterface.getErrorMessage(), e);
	}

	public AccountingSofwareException(String errorMessage) {
		super(errorMessage);
	}
	
}
