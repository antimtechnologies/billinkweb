package acc.webservice.global.imagehandler;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@RestController
public class UploadImageController {

	@Autowired
	private ServletContext servletContext;

	@Autowired
	UploadImageService uploadImageService;

	@RequestMapping(value = "saveImage", method = RequestMethod.POST)
	public String saveImage(@RequestParam Map<String, Object> requestData) throws UnknownHostException {
		return uploadImageService.uploadImage(requestData);
	}

	@RequestMapping(value = "uploadFile", method = RequestMethod.POST)
	public Map<String, Object> continueFileUpload(HttpServletRequest request, HttpServletResponse response) {
		MultipartHttpServletRequest mRequest;
		String imagePath = new String();
		String extension = new String();
		List<String> imageURLList = new ArrayList<>();

		try {
			mRequest = (MultipartHttpServletRequest) request;
			Map<String, String[]> requestMap = mRequest.getParameterMap();
			for (String string : requestMap.keySet()) {
				System.out.println("Key Name : " + string);
				System.out.println("Value : " + requestMap.get(string));
			}

			MultiValueMap<String, MultipartFile> filesMap = mRequest.getMultiFileMap();
			System.out.println("FILE START");
			for (String string : filesMap.keySet()) {
				System.out.println("Key Name : " + string);
				System.out.println("Value : " + requestMap.get(string));
			}

			String[] imageTypeList = requestMap.get("imageType");
			String imageType = imageTypeList[0];

			if (imageTypeList == null || imageTypeList.length == 0) {
				return null;
			}

			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split(servletContext.getContextPath() + "/WEB-INF/classes/");

			List<MultipartFile> fileList = filesMap.get("file[]");

			for (MultipartFile mFile : fileList) {
				imagePath = "/resources/images/" + imageType + "/" + imageType + "_" + Math.random() + "_"
						+ DateUtility.getCurrentDateTimeInLong();
				String fileName = pathArr[0] + imagePath;
				File outputFile = new File(fileName);
				outputFile.getParentFile().mkdirs();

				if (!ApplicationUtility.IS_SERVER_ENV) {
					fileName = fileName.substring(1, fileName.length());
				}

				extension = FilenameUtils.getExtension(mFile.getOriginalFilename());
				System.out.println("original file name : " + mFile.getOriginalFilename());
				System.out.println("file extension : " + extension);
				System.out.println(" new file name : " + fileName);
				fileName = fileName + "." + extension;
				Path objPath = Paths.get(fileName);
				Files.deleteIfExists(objPath);
				InputStream in = mFile.getInputStream();
				Files.copy(in, objPath);

				imageURLList.add(imagePath + "." + extension);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Object> responseObj = new HashMap<>();

		responseObj.put("status", "success");
		responseObj.put("imagePath", "");
		if (ApplicationUtility.getSize(imageURLList) > 0) {
			responseObj.put("imagePath", imageURLList.get(0));
		}

		responseObj.put("imageURLList", imageURLList);
		return responseObj;
	}

	@RequestMapping(value = "/uploadPdfFile", method = RequestMethod.POST)
	public @ResponseBody String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("pdfType") String pdfType) {
		if (!file.isEmpty()) {
			try {

				String path = this.getClass().getClassLoader().getResource("").getPath();
				String fullPath = URLDecoder.decode(path, "UTF-8");
				String pathArr[] = fullPath.split(servletContext.getContextPath() + "/WEB-INF/classes/");
				String fileName = "/resources/pdf/" + pdfType + "/" + pdfType + "_" + Math.random() + "_" + DateUtility.getCurrentDateTimeInLong() + ".pdf";
				String fullFileName = pathArr[0] + fileName;
				File outputFile = new File(fullFileName);
				outputFile.getParentFile().mkdirs();
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(outputFile));
				stream.write(bytes);
				stream.close();
				return ApplicationUtility.getServerURL() + fileName;
			} catch (Exception e) {
				return "You failed to upload " + pdfType + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + pdfType + " because the file was empty.";
		}
	}

}
