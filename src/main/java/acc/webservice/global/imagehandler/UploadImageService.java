package acc.webservice.global.imagehandler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import acc.webservice.global.utils.ApplicationUtility;
import acc.webservice.global.utils.DateUtility;

@Service
public class UploadImageService {

	@Autowired
	private ServletContext servletContext;

	private static final Logger logger = Logger.getLogger(UploadImageService.class);

	public String uploadImage(Map<String, Object> requestData) throws UnknownHostException {
		List<String> urlList = new ArrayList<>();	
		Gson gson = new Gson();
		String imageType 	= ApplicationUtility.getStrValue(requestData, "imageType");
		String imagePath    = ""; 
		FileOutputStream imageOutFile = null;
		int imageCount = ApplicationUtility.getIntValue(requestData, "imageCount");
		for(int i=0;i<imageCount;i++) {
			try {
				imagePath    = "/resources/images/" +imageType + "/" + imageType + "_" + Math.random()+ "_" + DateUtility.getCurrentDateTimeInLong() + ".jpg";
				byte imageByteArray[] =  Base64.decodeBase64(ApplicationUtility.getStrValue(requestData, "image"+i));
				String path = this.getClass().getClassLoader().getResource("").getPath();
				String fullPath = URLDecoder.decode(path, "UTF-8");
				String pathArr[] = fullPath.split(servletContext.getContextPath()+"/WEB-INF/classes/");
				String fileName = pathArr[0]+imagePath;
				File outputFile = new File(fileName);
				outputFile.getParentFile().mkdirs();
				imageOutFile = new FileOutputStream(fileName);
				imageOutFile.write(imageByteArray);
				urlList.add(ApplicationUtility.getServerURL()+imagePath);
	 		} catch (IOException e) {
	 			logger.error(e);
			}finally {
				try {
					imageOutFile.close();
				} catch (IOException e) {
				}
			}
		};
		return gson.toJson(urlList).toString();
	}

}
